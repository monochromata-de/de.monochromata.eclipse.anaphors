package de.monochromata.function;

public interface ThrowingBiFunction<I1, I2, O, T extends Throwable> {
	O apply(I1 input1, I2 input2) throws T;
}