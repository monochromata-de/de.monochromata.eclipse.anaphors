package de.monochromata.function;

public interface ThrowingBiConsumer<I1, I2, T extends Throwable> {
	void accept(I1 input1, I2 input2) throws T;
}