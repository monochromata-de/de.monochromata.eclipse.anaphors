package de.monochromata.function;

public interface ThrowingFunction<I, O, T extends Throwable> {
    O apply(I input) throws T;
}