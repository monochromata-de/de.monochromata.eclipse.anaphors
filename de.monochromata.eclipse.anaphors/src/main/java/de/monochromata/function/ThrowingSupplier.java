package de.monochromata.function;

public interface ThrowingSupplier<R, T extends Throwable> {
	R get() throws T;
}