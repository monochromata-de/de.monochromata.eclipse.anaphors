package de.monochromata.function;

public interface ThrowingConsumer<I, T extends Throwable> {
    void accept(I input) throws T;
}