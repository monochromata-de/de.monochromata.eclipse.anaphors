package de.monochromata.function;

public interface SextupleFunction<A, B, C, D, E, F, G> {
    G apply(A a, B b, C c, D d, E e, F f);
}