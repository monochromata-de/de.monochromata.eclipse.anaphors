package de.monochromata.function;

public interface ThrowingRunnable<T extends Throwable> {
	void run() throws T;
}