package de.monochromata.function;

public interface QuintupleFunction<A, B, C, D, E, F> {
    F apply(A a, B b, C c, D d, E e);
}