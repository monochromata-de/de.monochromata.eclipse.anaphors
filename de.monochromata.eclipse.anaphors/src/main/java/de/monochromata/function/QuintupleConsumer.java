package de.monochromata.function;

public interface QuintupleConsumer<A, B, C, D, E> {
    void accept(A a, B b, C c, D d, E e);
}