package de.monochromata.function;

public interface SextupleConsumer<A, B, C, D, E, F> {
    void accept(A a, B b, C c, D d, E e, F f);
}