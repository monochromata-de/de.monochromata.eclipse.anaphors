package de.monochromata.event;

/**
 * @param <E>
 *            the type of events - often an enum
 */
public class RunnableNotifier<E> extends Notifier<E, Runnable> {

    public void fire(final E event) {
        fire(event, Runnable::run);
    }

}
