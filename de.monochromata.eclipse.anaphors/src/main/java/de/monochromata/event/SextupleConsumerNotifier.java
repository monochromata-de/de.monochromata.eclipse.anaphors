package de.monochromata.event;

import de.monochromata.function.SextupleConsumer;

/**
 * @param <E>
 *            the type of events - often an enum
 * @param <A1>
 *            the type of the first argument presented to listeners
 * @param <A2>
 *            the type of the second argument presented to listeners
 * @param <A3>
 *            the type of the third argument presented to listeners
 * @param <A4>
 *            the type of the fourth argument presented to listeners
 * @param <A5>
 *            the type of the fifth argument presented to listeners
 * @param <A6>
 *            the type of the sixth argument presented to listeners
 */
public class SextupleConsumerNotifier<E, A1, A2, A3, A4, A5, A6>
        extends Notifier<E, SextupleConsumer<A1, A2, A3, A4, A5, A6>> {

    public void fire(final E event, final A1 arg1, final A2 arg2, final A3 arg3, final A4 arg4, final A5 arg5,
            final A6 arg6) {
        fire(event, listener -> listener.accept(arg1, arg2, arg3, arg4, arg5, arg6));
    }

}
