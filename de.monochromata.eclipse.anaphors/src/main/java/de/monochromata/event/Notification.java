package de.monochromata.event;

/**
 * @param <E>
 *            the type of events - often an enum
 * @param <L>
 *            the functional interface used to listen to events
 */
public interface Notification<E, L> {

    void addListenerForAllEvents(final Class<E> events, final L listener);

    void addListener(final E event, final L listener);

    void removeListenerForAllEvents(final Class<E> events, final L listener);

    void removeListener(final E event, final L listener);

}
