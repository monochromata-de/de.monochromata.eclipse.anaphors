package de.monochromata.event;

import static java.util.Arrays.stream;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * @param <E> the type of events - often an enum
 * @param <L> the functional interface used to listen to events
 */
public abstract class Notifier<E, L> implements Notification<E, L> {

	protected final Map<E, List<L>> listenersByEvent = new HashMap<>();

	@Override
	public synchronized void addListenerForAllEvents(final Class<E> events, final L listener) {
		stream(events.getEnumConstants()).forEach(enumConstant -> addListener(enumConstant, listener));
	}

	@Override
	public synchronized void addListener(final E event, final L listener) {
		listenersByEvent.computeIfAbsent(event, unused -> new ArrayList<>()).add(listener);
	}

	@Override
	public synchronized void removeListenerForAllEvents(final Class<E> events, final L listener) {
		stream(events.getEnumConstants()).forEach(enumConstant -> removeListener(enumConstant, listener));
	}

	@Override
	public synchronized void removeListener(final E event, final L listener) {
		listenersByEvent.computeIfAbsent(event, unused -> new ArrayList<>()).remove(listener);
	}

	protected synchronized void fire(final E event, final Consumer<L> fireOneListener) {
		final List<L> listeners = listenersByEvent.get(event);
		if (listeners != null) {
			listeners.forEach(listener -> fireOneListener.accept(listener));
		}
	}

}
