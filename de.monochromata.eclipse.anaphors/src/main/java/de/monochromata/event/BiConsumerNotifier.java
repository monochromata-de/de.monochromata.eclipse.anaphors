package de.monochromata.event;

import java.util.function.BiConsumer;

/**
 * @param <E>
 *            the type of events - often an enum
 * @param <A1>
 *            the type of the first argument presented to listeners
 * @param <A2>
 *            the type of the second argument presented to listeners
 */
public class BiConsumerNotifier<E, A1, A2> extends Notifier<E, BiConsumer<A1, A2>> {

    public void fire(final E event, final A1 arg1, final A2 arg2) {
        fire(event, listener -> listener.accept(arg1, arg2));
    }

}
