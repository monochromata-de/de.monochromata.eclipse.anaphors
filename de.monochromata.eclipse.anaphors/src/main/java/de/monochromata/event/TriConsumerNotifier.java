package de.monochromata.event;

import de.monochromata.function.TriConsumer;

/**
 * @param <E>
 *            the type of events - often an enum
 * @param <A1>
 *            the type of the first argument presented to listeners
 * @param <A2>
 *            the type of the second argument presented to listeners
 * @param <A3>
 *            the type of the third argument presented to listeners
 */
public class TriConsumerNotifier<E, A1, A2, A3> extends Notifier<E, TriConsumer<A1, A2, A3>> {

    public void fire(final E event, final A1 arg1, final A2 arg2, final A3 arg3) {
        fire(event, listener -> listener.accept(arg1, arg2, arg3));
    }

}
