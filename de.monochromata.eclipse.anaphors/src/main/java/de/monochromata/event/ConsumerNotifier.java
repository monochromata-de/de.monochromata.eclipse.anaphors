package de.monochromata.event;

import java.util.function.Consumer;

/**
 * @param <E>
 *            the type of events - often an enum
 * @param <A>
 *            the type of argument presented to listeners
 */
public class ConsumerNotifier<E, A> extends Notifier<E, Consumer<A>> {

    public void fire(final E event, final A arg) {
        fire(event, listener -> listener.accept(arg));
    }

}
