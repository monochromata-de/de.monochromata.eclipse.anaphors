package de.monochromata.eclipse.persp;

import static de.monochromata.eclipse.persp.PerspectivationPosition.PERSPECTIVATION_CATEGORY;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadLocationException;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.UnaryOperator;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentExtension;
import org.eclipse.jface.text.IDocumentExtension.IReplace;

import de.monochromata.eclipse.persp.PerspectivationPositionUpdater.PerspectivationDeletionStrategy;
import de.monochromata.eclipse.position.DoublyLinkedPositionUpdater;
import de.monochromata.function.TriConsumer;

public class PerspectivationPositionUpdater
        extends DoublyLinkedPositionUpdater<PerspectivationPosition, PerspectivationDeletionStrategy> {

    public PerspectivationPositionUpdater(final PerspectivationDocument perspectivationDocument) {
        super(PERSPECTIVATION_CATEGORY, createPreDeleteListener(PERSPECTIVATION_CATEGORY, perspectivationDocument),
                new PostNotificationReplaceDeletionStrategy());
    }

    protected static TriConsumer<PerspectivationPosition, IDocument, PerspectivationDeletionStrategy> createPreDeleteListener(
            final String positionCategory, final PerspectivationDocument perspectivationDocument) {
        final TriConsumer<PerspectivationPosition, PerspectivationDocument, PerspectivationDeletionStrategy> removePerspectivations = removePerspectivations();
        final BiConsumer<PerspectivationPosition, IDocument> maintainChain = maintainChainBeforeDeletion(positionCategory);
        return (position, document, deletionStrategy) -> {
            removePerspectivations.accept(position, perspectivationDocument, deletionStrategy);
            maintainChain.accept(position, document);
        };
    }

    protected static TriConsumer<PerspectivationPosition, PerspectivationDocument, PerspectivationDeletionStrategy> removePerspectivations() {
        return (position, perspectivationDocument, deletionStrategy) -> {
            inlineAllRelatedToLowerCaseIntoOriginDocument(position, perspectivationDocument, deletionStrategy);
            removeAllRelatedHiddenRegions(position, perspectivationDocument, deletionStrategy);
        };
    }

    protected static void inlineAllRelatedToLowerCaseIntoOriginDocument(final PerspectivationPosition position,
            final PerspectivationDocument perspectivationDocument,
            final PerspectivationDeletionStrategy deletionStrategy) {
        inlineToLowerCaseIntoOriginDocument(position, perspectivationDocument, deletionStrategy);
        if (!position.hasSiblings()) {
            inlineToLowerCaseIntoOriginDocumentRecursively(position, PerspectivationPosition::getPrevious,
                    perspectivationDocument, deletionStrategy);
        }
        inlineToLowerCaseIntoOriginDocumentRecursively(position, PerspectivationPosition::getNext,
                perspectivationDocument, deletionStrategy);
    }

    protected static void inlineToLowerCaseIntoOriginDocumentRecursively(final PerspectivationPosition position,
            final UnaryOperator<PerspectivationPosition> accessor,
            final PerspectivationDocument perspectivationDocument,
            final PerspectivationDeletionStrategy deletionStrategy) {
        final PerspectivationPosition subjectPosition = accessor.apply(position);
        if (subjectPosition != null) {
            inlineToLowerCaseIntoOriginDocument(subjectPosition, perspectivationDocument, deletionStrategy);
            inlineToLowerCaseIntoOriginDocumentRecursively(subjectPosition, accessor, perspectivationDocument,
                    deletionStrategy);
        }
    }

    protected static void inlineToLowerCaseIntoOriginDocumentRecursively(final PerspectivationPosition position,
            final Function<PerspectivationPosition, List<PerspectivationPosition>> accessor,
            final PerspectivationDocument perspectivationDocument,
            final PerspectivationDeletionStrategy deletionStrategy) {
        final List<PerspectivationPosition> subjectPositions = accessor.apply(position);
        subjectPositions.forEach(subjectPosition -> {
            inlineToLowerCaseIntoOriginDocument(subjectPosition, perspectivationDocument, deletionStrategy);
            inlineToLowerCaseIntoOriginDocumentRecursively(subjectPosition, accessor, perspectivationDocument,
                    deletionStrategy);
        });
    }

    protected static void inlineToLowerCaseIntoOriginDocument(final PerspectivationPosition position,
            final PerspectivationDocument perspectivationDocument,
            final PerspectivationDeletionStrategy deletionStrategy) {
        final List<ToLowerCasePosition> positionsToInline = position
                .getToLowerCasePositions()
                .stream()
                .filter(toLowerCase -> !toLowerCase.isDeleted)
                .collect(toList());
        if (!positionsToInline.isEmpty()) {
            deletionStrategy.inlineToLowerCaseIntoOriginDocument(positionsToInline, perspectivationDocument);
        }
    }

    protected static void removeAllRelatedHiddenRegions(final PerspectivationPosition position,
            final PerspectivationDocument perspectivationDocument,
            final PerspectivationDeletionStrategy deletionStrategy) {
        removeHiddenRegionsFromOriginDocument(position, perspectivationDocument, deletionStrategy);
        if (!position.hasSiblings()) {
            removeHiddenRegionsFromOriginDocumentRecursively(position, PerspectivationPosition::getPrevious,
                    perspectivationDocument, deletionStrategy);
        }
        removeHiddenRegionsFromOriginDocumentRecursively(position, PerspectivationPosition::getNext,
                perspectivationDocument, deletionStrategy);
    }

    protected static void removeHiddenRegionsFromOriginDocumentRecursively(final PerspectivationPosition position,
            final UnaryOperator<PerspectivationPosition> accessor,
            final PerspectivationDocument perspectivationDocument,
            final PerspectivationDeletionStrategy deletionStrategy) {
        final PerspectivationPosition subjectPosition = accessor.apply(position);
        if (subjectPosition != null) {
            removeHiddenRegionsFromOriginDocument(subjectPosition, perspectivationDocument, deletionStrategy);
            removeHiddenRegionsFromOriginDocumentRecursively(subjectPosition, accessor, perspectivationDocument,
                    deletionStrategy);
        }
    }

    protected static void removeHiddenRegionsFromOriginDocumentRecursively(final PerspectivationPosition position,
            final Function<PerspectivationPosition, List<PerspectivationPosition>> accessor,
            final PerspectivationDocument perspectivationDocument,
            final PerspectivationDeletionStrategy deletionStrategy) {
        final List<PerspectivationPosition> subjectPositions = accessor.apply(position);
        subjectPositions.forEach(subjectPosition -> {
            removeHiddenRegionsFromOriginDocument(subjectPosition, perspectivationDocument, deletionStrategy);
            removeHiddenRegionsFromOriginDocumentRecursively(subjectPosition, accessor, perspectivationDocument,
                    deletionStrategy);
        });
    }

    protected static void removeHiddenRegionsFromOriginDocument(final PerspectivationPosition position,
            final PerspectivationDocument perspectivationDocument,
            final PerspectivationDeletionStrategy deletionStrategy) {
        final List<HidePosition> positionsToRemove = position
                .getHidePositions()
                .stream()
                .filter(hidePosition -> !hidePosition.isDeleted)
                .collect(toList());
        if (!positionsToRemove.isEmpty()) {
            deletionStrategy.removeHiddenRegionsFromOriginDocument(positionsToRemove, perspectivationDocument);
        }
    }

    public static interface PerspectivationDeletionStrategy {

        void inlineToLowerCaseIntoOriginDocument(List<ToLowerCasePosition> positionsToInline,
                PerspectivationDocument perspectivationDocument);

        void removeHiddenRegionsFromOriginDocument(List<HidePosition> positionsToRemove,
                PerspectivationDocument perspectivationDocument);

    }

    public static class PostNotificationReplaceDeletionStrategy implements PerspectivationDeletionStrategy {

        @Override
        public void inlineToLowerCaseIntoOriginDocument(final List<ToLowerCasePosition> positionsToInline,
                final PerspectivationDocument perspectivationDocument) {
            registerPostNotificationReplace(perspectivationDocument,
                    (document, unused) -> perspectivationDocument
                            .inlineToLowerCaseIntoOriginDocument(positionsToInline));
        }

        @Override
        public void removeHiddenRegionsFromOriginDocument(final List<HidePosition> positionsToRemove,
                final PerspectivationDocument perspectivationDocument) {
            registerPostNotificationReplace(perspectivationDocument, (document, unused) -> wrapBadLocationException(
                    () -> perspectivationDocument
                            .removeHiddenRangesFromOriginDocument(positionsToRemove)));
        }

        protected void registerPostNotificationReplace(final PerspectivationDocument perspectivationDocument,
                final IReplace replaceOperation) {
            // A modification of the origin document is registered as a post notification
            // replace operation, because when the current method is executed, there is
            // already a change of the image and origin documents going on. Changing the
            // origin document would start another change before the first change has been
            // completed, leading to a NullPointerException. Hence, the change is added
            // to the list of post notification replaces and thereby executed sequentially
            // instead of recursively.
            ((IDocumentExtension) perspectivationDocument.getMasterDocument()).registerPostNotificationReplace(null,
                    replaceOperation);
        }
    }
}
