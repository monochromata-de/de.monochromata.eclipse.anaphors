package de.monochromata.eclipse.persp;

import static de.monochromata.anaphors.ast.chain.ChainTraversal.getAnaphorElementsForRelatedExpressionElement;
import static de.monochromata.anaphors.ast.chain.ChainTraversal.getRelatedExpressionElements;
import static de.monochromata.anaphors.perspectivation.Underspecification.underspecifyAnaphor;
import static de.monochromata.anaphors.perspectivation.Underspecification.underspecifyRelatedExpression;
import static de.monochromata.eclipse.anaphors.position.ExistingPerspectivation.getExistingAnaphorPerspectivation;
import static de.monochromata.eclipse.anaphors.position.ExistingPerspectivation.getExistingRelatedExpressionPerspectivation;
import static de.monochromata.eclipse.persp.ConditionCreation.getCondition;
import static de.monochromata.eclipse.persp.PerspectivationPositionEnabling.configureRelatedExpressionPerspectivation;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.builder;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.anaphors.PublicChainElement;
import org.eclipse.jdt.core.dom.anaphors.PublicDom;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpressions;
import org.eclipse.jdt.internal.corext.refactoring.code.context.ChainResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.code.context.SharedAnaphorResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.code.context.SuccessfulChainResolutionContext;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.text.edits.TextEdit;

import de.monochromata.anaphors.perspectivation.strategy.PerspectivationConfiguration;

public interface PerspectivationPositionCreation {

	/**
	 * A type used to mark returned perspectivation positions.
	 */
	enum ReturnType {
		Existing, New
	}

	Function<PublicRelatedExpressions, BiFunction<CompilationUnit, Predicate<DocumentEvent>, Function<PublicChainElement, PerspectivationPosition>>> PERSPECTIVIZE_RELATED_EXPRESSION = relatedExpressionsSpi -> (
			compilationUnitNode, condition) -> relatedExpressionElement -> {
				final var anaphorChainElements = getAnaphorElementsForRelatedExpressionElement(
						relatedExpressionElement);
				final var anaphorParts = anaphorChainElements.stream().map(anaphorElement -> anaphorElement.anaphor)
						.collect(toList());
				return underspecifyRelatedExpression(relatedExpressionElement.relatedExpression, anaphorParts,
						compilationUnitNode, condition, relatedExpressionsSpi);
			};

	Function<PublicDom, BiFunction<CompilationUnit, Predicate<DocumentEvent>, Function<PublicChainElement, Function<PublicChainElement, PerspectivationPosition>>>> PERSPECTIVIZE_ANAPHOR = anaphorsSpi -> (
			compilationUnitNode,
			condition) -> relatedExpressionElement -> anaphorElement -> underspecifyAnaphor(
					relatedExpressionElement.relatedExpression, anaphorElement.anaphor, compilationUnitNode, condition,
					anaphorsSpi);

	Function<PerspectivationConfiguration, UnaryOperator<PerspectivationPosition>> CONFIGURE_RELATED_EXPRESSION_PERSPECTIVATION = perspectivationConfiguration -> relatedExpressionPosition -> {
		configureRelatedExpressionPerspectivation(relatedExpressionPosition,
				/* No document is required, because the condition has not yet been met */ null,
				perspectivationConfiguration);
		return relatedExpressionPosition;
	};

	Function<PerspectivationConfiguration, UnaryOperator<PerspectivationPosition>> CONFIGURE_ANAPHOR_PERSPECTIVATION = perspectivationConfiguration -> anaphorPosition -> {
		PerspectivationPositionEnabling.configureAnaphorPerspectivation(anaphorPosition,
				/* No document is required, because the condition has not yet been met */ null,
				perspectivationConfiguration);
		return anaphorPosition;
	};

	static List<PerspectivationPosition> getPositions(
			final List<Pair<PublicChainElement, PerspectivationPosition>> perspectivationPositions) {
		return perspectivationPositions.stream().map(Pair::getRight).collect(toList());
	}

	static List<Pair<PublicChainElement, PerspectivationPosition>> createPerspectivationPositions(final TextEdit edit,
			final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
			final List<ChainResolutionContext> chainResolutionContexts,
			final PerspectivationConfiguration perspectivationConfiguration,
			final List<PerspectivationPosition> positionsToRemove,
			final List<Pair<PerspectivationPosition, PerspectivationPosition>> positionsToLink,
			final PublicRelatedExpressions relatedExpressionsSpi, final PublicDom anaphorsSpi) {
		final List<SuccessfulChainResolutionContext> successfulContexts = chainResolutionContexts.stream()
				.filter(context -> context instanceof SuccessfulChainResolutionContext)
				.map(context -> (SuccessfulChainResolutionContext) context).collect(toList());
		if (successfulContexts.isEmpty()) {
			return emptyList();
		}
		final Predicate<DocumentEvent> condition = getCondition(edit);
		final Stream.Builder<Pair<PublicChainElement, PerspectivationPosition>> positionsToAdd = builder();
		final var perspectivizeRelatedExpression = PERSPECTIVIZE_RELATED_EXPRESSION.apply(relatedExpressionsSpi)
				.apply(sharedAnaphorResolutionContext.compilationUnitNode, condition)
				.andThen(CONFIGURE_RELATED_EXPRESSION_PERSPECTIVATION.apply(perspectivationConfiguration));
		final var prePerspectivizeAnaphor = PERSPECTIVIZE_ANAPHOR.apply(anaphorsSpi)
				.apply(sharedAnaphorResolutionContext.compilationUnitNode, condition);
		final Function<PublicChainElement, Function<PublicChainElement, PerspectivationPosition>> perspectivizeAnaphor = relatedExpressionElement -> anaphorElement -> CONFIGURE_ANAPHOR_PERSPECTIVATION
				.apply(perspectivationConfiguration)
				.apply(prePerspectivizeAnaphor.apply(relatedExpressionElement).apply(anaphorElement));
		final var getExistingRelatedExpressionPerspectivation = getExistingRelatedExpressionPerspectivation(
				positionsToRemove);
		final var getExistingAnaphorPerspectivation = getExistingAnaphorPerspectivation(positionsToRemove);
		successfulContexts.stream()
				.forEach(context -> createPerspectivationPositions(context, positionsToAdd, positionsToLink,
						getExistingRelatedExpressionPerspectivation, perspectivizeRelatedExpression,
						getExistingAnaphorPerspectivation, perspectivizeAnaphor));
		return positionsToAdd.build().collect(toList());
	}

	private static void createPerspectivationPositions(final SuccessfulChainResolutionContext chainResolutionContext,
			final Stream.Builder<Pair<PublicChainElement, PerspectivationPosition>> positionsToAdd,
			final List<Pair<PerspectivationPosition, PerspectivationPosition>> positionsToLink,
			final Function<PublicChainElement, PerspectivationPosition> getExistingRelatedExpressionPerspectivation,
			final Function<PublicChainElement, PerspectivationPosition> perspectivizeRelatedExpression,
			final Function<PublicChainElement, PerspectivationPosition> getExistingAnaphorPerspectivation,
			final Function<PublicChainElement, Function<PublicChainElement, PerspectivationPosition>> perspectivizeAnaphor) {
		getRelatedExpressionElements(chainResolutionContext.anaphorChainRoot)
				.forEach(relatedExpressionElement -> createPerspectivationPositions(relatedExpressionElement,
						positionsToAdd, positionsToLink, getExistingRelatedExpressionPerspectivation,
						perspectivizeRelatedExpression, getExistingAnaphorPerspectivation, perspectivizeAnaphor));
	}

	static void createPerspectivationPositions(final PublicChainElement relatedExpressionElement,
			final Stream.Builder<Pair<PublicChainElement, PerspectivationPosition>> positionsToAdd,
			final List<Pair<PerspectivationPosition, PerspectivationPosition>> positionsToLink,
			final Function<PublicChainElement, PerspectivationPosition> getExistingRelatedExpressionPerspectivation,
			final Function<PublicChainElement, PerspectivationPosition> perspectivizeRelatedExpression,
			final Function<PublicChainElement, PerspectivationPosition> getExistingAnaphorPerspectivation,
			final Function<PublicChainElement, Function<PublicChainElement, PerspectivationPosition>> perspectivizeAnaphor) {
		final var relatedExpressionPerspectivation = perspectivize(relatedExpressionElement,
				getExistingRelatedExpressionPerspectivation, perspectivizeRelatedExpression, positionsToAdd);
		perspectivizeAnaphors(relatedExpressionElement, getExistingAnaphorPerspectivation, perspectivizeAnaphor,
				positionsToAdd).forEach(collectPositionsToLink(positionsToLink, relatedExpressionPerspectivation));
	}

	static Stream<Pair<ReturnType, PerspectivationPosition>> perspectivizeAnaphors(
			final PublicChainElement relatedExpressionElement,
			final Function<PublicChainElement, PerspectivationPosition> getExistingAnaphorPerspectivation,
			final Function<PublicChainElement, Function<PublicChainElement, PerspectivationPosition>> perspectivizeAnaphor,
			final Stream.Builder<Pair<PublicChainElement, PerspectivationPosition>> positionsToAdd) {
		return getAnaphorElementsForRelatedExpressionElement(relatedExpressionElement).stream()
				.map(anaphorElement -> perspectivize(anaphorElement, getExistingAnaphorPerspectivation,
						perspectivizeAnaphor.apply(relatedExpressionElement), positionsToAdd));
	}

	static Pair<ReturnType, PerspectivationPosition> perspectivize(final PublicChainElement chainElement,
			final Function<PublicChainElement, PerspectivationPosition> getExistingPerspectivation,
			final Function<PublicChainElement, PerspectivationPosition> perspectivize,
			final Stream.Builder<Pair<PublicChainElement, PerspectivationPosition>> positionsToAdd) {
		final var existingPerspectivation = getExistingPerspectivation.apply(chainElement);
		if (existingPerspectivation != null) {
			return new ImmutablePair<>(ReturnType.Existing, existingPerspectivation);
		}
		final var newPerspectivation = perspectivize.apply(chainElement);
		positionsToAdd.add(new ImmutablePair<>(chainElement, newPerspectivation));
		return new ImmutablePair<>(ReturnType.New, newPerspectivation);
	}

	static Consumer<Pair<ReturnType, PerspectivationPosition>> collectPositionsToLink(
			final List<Pair<PerspectivationPosition, PerspectivationPosition>> positionsToLink,
			final Pair<ReturnType, PerspectivationPosition> relatedExpressionPerspectivation) {
		return anaphorPerspectivation -> {
			if (relatedExpressionPerspectivation.getLeft() != ReturnType.Existing
					|| anaphorPerspectivation.getLeft() != ReturnType.Existing) {
				positionsToLink.add(new ImmutablePair<>(relatedExpressionPerspectivation.getRight(),
						anaphorPerspectivation.getRight()));
			}
		};
	}

}
