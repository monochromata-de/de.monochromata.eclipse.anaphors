package de.monochromata.eclipse.persp;

import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadLocationException;
import static java.util.Arrays.stream;

import org.eclipse.jface.text.IDocument;

import de.monochromata.anaphors.perspectivation.strategy.PerspectivationConfiguration;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;
import de.monochromata.eclipse.position.PositionQuerying;

public interface PerspectivationPositionEnabling {

    static void configurePerspectivation(final IDocument originDocument,
            final PerspectivationDocument imageDocument,
            final PerspectivationConfiguration perspectivationConfiguration) {
        stream(PositionQuerying.getPositions(ANAPHORA_CATEGORY, originDocument))
                .filter(position -> position instanceof AbstractAnaphoraPositionForRepresentation)
                .map(position -> (AbstractAnaphoraPositionForRepresentation) position)
                .forEach(position -> configurePerspectivation(position, imageDocument, perspectivationConfiguration));
    }

    static void configurePerspectivation(final AbstractAnaphoraPositionForRepresentation anaphoraPosition,
            final PerspectivationDocument document,
            final PerspectivationConfiguration perspectivationConfiguration) {
        if (anaphoraPosition instanceof PositionForRelatedExpression) {
            configureRelatedExpressionPerspectivation((PositionForRelatedExpression) anaphoraPosition, document,
                    perspectivationConfiguration);
        } else if (anaphoraPosition instanceof PositionForAnaphor) {
            configureAnaphorPerspectivation((PositionForAnaphor) anaphoraPosition, document,
                    perspectivationConfiguration);
        } else {
            throw new IllegalArgumentException(
                    "Unknown subtype of " + AbstractAnaphoraPositionForRepresentation.class.getName() + ": "
                            + anaphoraPosition.getClass().getName());
        }
    }

    static void configureRelatedExpressionPerspectivation(final PositionForRelatedExpression relatedExpressionPosition,
            final PerspectivationDocument document,
            final PerspectivationConfiguration perspectivationConfiguration) {
        configureRelatedExpressionPerspectivation(relatedExpressionPosition.perspectivationPosition, document,
                perspectivationConfiguration);
    }

    static void configureRelatedExpressionPerspectivation(final PerspectivationPosition relatedExpressionPosition,
            final PerspectivationDocument document,
            final PerspectivationConfiguration perspectivationConfiguration) {
        wrapBadLocationException(() -> relatedExpressionPosition
                .setEnabled(perspectivationConfiguration.underspecifyRelatedExpression, document));
    }

    static void configureAnaphorPerspectivation(final PositionForAnaphor anaphorPosition,
            final PerspectivationDocument document,
            final PerspectivationConfiguration perspectivationConfiguration) {
        configureAnaphorPerspectivation(anaphorPosition.perspectivationPosition, document,
                perspectivationConfiguration);
    }

    static void configureAnaphorPerspectivation(final PerspectivationPosition anaphorPosition,
            final PerspectivationDocument document,
            final PerspectivationConfiguration perspectivationConfiguration) {
        wrapBadLocationException(
                () -> anaphorPosition.setEnabled(perspectivationConfiguration.underspecifyAnaphor, document));
    }

}
