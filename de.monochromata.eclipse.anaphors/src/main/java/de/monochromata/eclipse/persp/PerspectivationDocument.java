package de.monochromata.eclipse.persp;

import static de.monochromata.eclipse.persp.HidePosition.HIDE_CATEGORY;
import static de.monochromata.eclipse.persp.PerspectivationPosition.PERSPECTIVATION_CATEGORY;
import static de.monochromata.eclipse.persp.ToLowerCasePosition.TO_LOWER_CASE_CATEGORY;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadLocationException;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadPositionCategoryException;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadPositionCategoryExceptionFunction;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapPositionExceptions;
import static java.lang.Character.isUpperCase;
import static java.util.Arrays.stream;
import static java.util.Optional.empty;
import static java.util.Optional.of;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPositionCategoryException;
import org.eclipse.jface.text.DefaultLineTracker;
import org.eclipse.jface.text.DefaultPositionUpdater;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ILineTracker;
import org.eclipse.jface.text.IPositionUpdater;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextStore;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.projection.ProjectionDocument;

import de.monochromata.eclipse.position.PositionQuerying;

public class PerspectivationDocument extends ProjectionDocument {

    protected final AbstractDocument originDocument;
    protected final IPositionUpdater toLowerCaseUpdater;
    protected final IPositionUpdater hideUpdater;
    protected final IPositionUpdater perspectivationUpdater;

    public PerspectivationDocument(final AbstractDocument originDocument) {
        super(originDocument);
        this.originDocument = originDocument;

        originDocument.addPositionCategory(TO_LOWER_CASE_CATEGORY);
        toLowerCaseUpdater = new DefaultPositionUpdater(TO_LOWER_CASE_CATEGORY);
        originDocument.addPositionUpdater(toLowerCaseUpdater);

        originDocument.addPositionCategory(HIDE_CATEGORY);
        hideUpdater = new DefaultPositionUpdater(HIDE_CATEGORY);
        originDocument.addPositionUpdater(hideUpdater);

        // Note: It is important that the updater for PERSPECTIVATION_CATEGORY is added
        // after the updaters for TO_LOWER_CASE_CATEGORY and HIDE_CATEGORY because the
        // updater for PERSPECTIVATION_CATEGORY accesses positions from the other two
        // categories.
        originDocument.addPositionCategory(PERSPECTIVATION_CATEGORY);
        perspectivationUpdater = new PerspectivationPositionUpdater(this);
        originDocument.addPositionUpdater(perspectivationUpdater);

        final ITextStore store = new PerspectivationTextStore(originDocument, this, getDocumentInformationMapping());
        final ILineTracker tracker = new DefaultLineTracker();
        setTextStore(store);
        setLineTracker(tracker);
        tracker.set(store.get(0, store.getLength()));

        originDocument.addDocumentListener(new PerspectivationPositionNotifier(this));
    }

    @Override
    public void dispose() {
        removeUpdaterAndCategory(toLowerCaseUpdater, TO_LOWER_CASE_CATEGORY);
        removeUpdaterAndCategory(hideUpdater, HIDE_CATEGORY);
        removeUpdaterAndCategory(perspectivationUpdater, PERSPECTIVATION_CATEGORY);
        super.dispose();
    }

    protected void removeUpdaterAndCategory(final IPositionUpdater positionUpdater, final String positionCategory) {
        originDocument.removePositionUpdater(positionUpdater);
        try {
            originDocument.removePositionCategory(positionCategory);
        } catch (final BadPositionCategoryException x) {
            // allow multiple dispose calls
        }

    }

    /**
     * If the character at the given offset is not lower case, adds a position at
     * the given offset that converts the character to lower case.
     *
     * @param imageOffset
     *            the image offset to turn to lower case
     * @return an optional containing a {@link ToLowerCasePosition} if the character
     *         was previously in upper case and lower-casing was added and is to be
     *         removed by {@link #removeLowerCase(int)}, an empty optional
     *         otherwise.
     * @throws BadLocationException
     *             If the given offset and length do not describe a valid position
     *             in this document
     * @see #removeLowerCase(int)
     */
    public Optional<ToLowerCasePosition> toLowerCase(final int imageOffset) throws BadLocationException {
        return wrapBadPositionCategoryExceptionFunction(imageOffset, off -> {
            requireNonNegativeOffset(off);
            if (shouldNotAddToLowerCase(off)) {
                return empty();
            }
            return of(addToLowerCase(off));
        });
    }

    protected boolean shouldNotAddToLowerCase(final int imageOffset)
            throws BadLocationException, BadPositionCategoryException {
        if (Character.isLowerCase(getChar(imageOffset))) {
            return true;
        }
        final int originOffset = getDocumentInformationMapping().toOriginOffset(imageOffset);
        if (originDocument.getPositions(TO_LOWER_CASE_CATEGORY, originOffset, 1, false, false).length > 0) {
            return true;
        }
        return false;
    }

    protected ToLowerCasePosition addToLowerCase(final int imageOffset)
            throws BadLocationException, BadPositionCategoryException {
        final int originOffset = getDocumentInformationMapping().toOriginOffset(imageOffset);
        final ToLowerCasePosition position = new ToLowerCasePosition(originOffset);
        originDocument.addPosition(TO_LOWER_CASE_CATEGORY, position);
        return position;
    }

    /**
     * Removes the given range from the perspectivation document. The range is
     * specified in coordinates of the origin document. Besides invoking
     * {@link #removeMasterDocumentRange(int, int)}, this method adds a
     * {@link HidePosition} so the range removed from the perspectivation document
     * can later be either (a) removed from the origin document, too, or (b) be
     * added to the image document again.
     *
     * @return HidePosition the added position
     * @throws BadLocationException
     *             if the given range exceeds the boundaries of the master document
     * @see #removeHiddenRangesFromOriginDocument(int,int)
     * @see #addMasterDocumentRange(HidePosition)
     * @see #removeMasterDocumentRange(int, int)
     */
    public HidePosition hideOriginDocumentRange(final int offsetInOrigin, final int lengthInOrigin)
            throws BadLocationException {
        removeMasterDocumentRange(offsetInOrigin, lengthInOrigin);
        return wrapBadPositionCategoryException(
                () -> addHidePosition(offsetInOrigin, lengthInOrigin));
    }

    protected HidePosition addHidePosition(final int offsetInOrigin, final int lengthInOrigin)
            throws BadLocationException, BadPositionCategoryException {
        final HidePosition position = new HidePosition(offsetInOrigin, lengthInOrigin);
        originDocument.addPosition(HIDE_CATEGORY, position);
        return position;
    }

    /**
     * Adds the range of text covered by the given {@link HidePosition} to the image
     * document and removes the given position instance from the origin document.
     *
     * @throws BadLocationException
     *             if the given position exceeds the boundaries of the master
     *             document
     * @see #hideOriginDocumentRange(int, int)
     * @see #removeHiddenRangeFromOriginDocument(HidePosition)
     */
    public void addMasterDocumentRange(final HidePosition hidePosition) {
        final int offset = hidePosition.offset;
        final int length = hidePosition.length;
        wrapBadPositionCategoryException(
                () -> originDocument.removePosition(HIDE_CATEGORY, hidePosition));
        if (length > 0) {
            wrapBadLocationException(() -> addMasterDocumentRange(offset, length));
        }
    }

    /**
     * Ensures that the given master document range is part of this perspectivation
     * document.
     * <p>
     * Note that hidden regions will not be shown by this method. Use
     * {@link #addMasterDocumentRange(HidePosition)} instead with a
     * {@link HidePosition} contained in this document. This limitation is imposed
     * because {@link #addMasterDocumentRange(int, int)} is invoked to e.g. ensure a
     * method is expanded when its signature is edited. Even an expanded method body
     * should not show hidden code, though.
     */
    @Override
    public void addMasterDocumentRange(final int offsetInMaster, final int lengthInMaster) throws BadLocationException {
        final Position[] hidePositions = PositionQuerying.getPositions(HIDE_CATEGORY, originDocument);
        if (hidePositions.length == 0) {
            super.addMasterDocumentRange(offsetInMaster, lengthInMaster);
        } else {
            addMasterDocumentRangeExcludingHidePositions(offsetInMaster, lengthInMaster, hidePositions);
        }
    }

    protected void addMasterDocumentRangeExcludingHidePositions(final int rangeOffset, final int rangeLength,
            final Position[] hidePositions) throws BadLocationException {
        int endOfLastSubRange = rangeOffset;
        for (final Position hidePosition : hidePositions) {
            addRangeBeforeHide(endOfLastSubRange, hidePosition);
            endOfLastSubRange = hidePosition.offset + hidePosition.length;
        }
        addRangeAfterLastHide(rangeOffset, rangeLength, endOfLastSubRange);
    }

    protected void addRangeBeforeHide(final int endOfLastSubRange, final Position hidePosition)
            throws BadLocationException {
        final int length = hidePosition.offset - endOfLastSubRange;
        if (length > 0) {
            super.addMasterDocumentRange(endOfLastSubRange, length);
        }
    }

    protected void addRangeAfterLastHide(final int rangeOffset, final int rangeLength, final int endOfLastSubRange)
            throws BadLocationException {
        final int rangeEnd = rangeOffset + rangeLength;
        if (endOfLastSubRange < rangeEnd) {
            final int length = rangeEnd - endOfLastSubRange;
            super.addMasterDocumentRange(endOfLastSubRange, length);
        }
    }

    /**
     * Remove the given {@link HidePosition}s and the ranges of text that they cover
     * from the origin document.
     *
     * @throws BadLocationException
     *             if any of the given positions exceeds the boundaries of the
     *             master document
     * @see #hideOriginDocumentRange(int, int)
     * @see #addMasterDocumentRange(HidePosition)
     */
    public void removeHiddenRangesFromOriginDocument(final List<HidePosition> hidePositions)
            throws BadLocationException {
        hidePositions
                .stream()
                .sorted(Comparator.comparingInt(position -> -position.offset)) // sort in reverse
                .forEach(this::removeHiddenRangeFromOriginDocument);
    }

    /**
     * Remove the {@link HidePosition} and the range of text that it covers from the
     * origin document. Nothing happens if the position has already been deleted.
     *
     * @throws BadLocationException
     *             if the given position exceeds the boundaries of the master
     *             document
     * @see #hideOriginDocumentRange(int, int)
     * @see #addMasterDocumentRange(HidePosition)
     */
    public void removeHiddenRangeFromOriginDocument(final HidePosition positionToRemove) {
        if (!positionToRemove.isDeleted) {
            final IDocument originDocument = getMasterDocument();
            wrapBadLocationException(
                    () -> originDocument.replace(positionToRemove.offset, positionToRemove.length, ""));
            wrapPositionExceptions(() -> originDocument.removePosition(HIDE_CATEGORY, positionToRemove));
            positionToRemove.delete();
        }
    }

    @Override
    public void masterDocumentAboutToBeChanged(final DocumentEvent masterEvent) {
        super.masterDocumentAboutToBeChanged(masterEvent);
        // Remove the lower-casing transformations from edited regions
        removeToLowerCaseRelativeToOrigin(masterEvent);
    }

    /**
     * Removes a lower-casing transformation previously added via
     * {@link #toLowerCase(int)}.
     *
     * @param imageOffset
     *            the image offset to remove the lower-casing transformation from
     * @return {@literal true} if a lower-casing transformation at the given offset
     *         has been removed, {@literal false} otherwise.
     * @throws BadLocationException
     *             If the given offset and length do not describe a valid position
     *             in this document
     * @see #toLowerCase(int)
     */
    public boolean removeToLowerCase(final int imageOffset) throws BadLocationException {
        return removeToLowerCase(imageOffset, 1);
    }

    /**
     * Removes all lower-casing transformations previously added via
     * {@link #toLowerCase(int)} in the given region.
     *
     * @return {@literal true} if a lower-casing transformation at the given offset
     *         has been removed, {@literal false} otherwise.
     * @throws BadLocationException
     *             If the given offset and length do not describe a valid position
     *             in this document
     * @see #toLowerCase(int)
     */
    public boolean removeToLowerCase(final int imageOffset, final int imageLength) {
        try {
            final IRegion imageRegion = new Region(imageOffset, imageLength);
            final IRegion originRegion = getDocumentInformationMapping().toOriginRegion(imageRegion);
            if (originRegion != null) {
                return removeToLowerCaseRelativeToOrigin(originRegion.getOffset(), originRegion.getLength());
            }
            return false;
        } catch (final BadLocationException e) {
            throw new IllegalStateException(e);
        }
    }

    public void removeToLowerCaseRelativeToOrigin(final DocumentEvent originEvent) {
        removeToLowerCaseRelativeToOrigin(originEvent.fOffset, originEvent.fLength);
    }

    protected boolean removeToLowerCaseRelativeToOrigin(final int originOffset, final int originLength) {
        return wrapPositionExceptions(() -> {
            requireNonNegativeOffset(originOffset);
            requireOffsetNotGreaterThanLength(originOffset + originLength, originDocument);
            final Position[] positions = originDocument.getPositions(TO_LOWER_CASE_CATEGORY, originOffset, originLength,
                    false, false);
            if (positions.length == 0) {
                return false;
            }
            removeToLowerCases(positions);
            return true;
        });
    }

    public void removeToLowerCases(final List<ToLowerCasePosition> positions) {
        positions.forEach(this::removeToLowerCase);
    }

    protected void removeToLowerCases(final Position[] positions) {
        stream(positions).forEach(this::removeToLowerCase);
    }

    /**
     * Removes the given lower-casing. Nothing happens if the given position has
     * already been deleted.
     *
     * @see Position#isDeleted
     */
    public void removeToLowerCase(final Position position) {
        if (!position.isDeleted) {
            wrapBadPositionCategoryException(position,
                    pos -> originDocument.removePosition(TO_LOWER_CASE_CATEGORY, pos));
            position.delete();
        }
    }

    /**
     * @see #inlineToLowerCaseIntoOriginDocument(Position)
     */
    public void inlineToLowerCaseIntoOriginDocument(final List<ToLowerCasePosition> positions) {
        positions.forEach(this::inlineToLowerCaseIntoOriginDocument);
    }

    /**
     * Removes the positions from the origin document and converts the character at
     * the position in the origin document to lower case. Nothing happens if the
     * given position has already been deleted.
     */
    public void inlineToLowerCaseIntoOriginDocument(final ToLowerCasePosition position) {
        if (!position.isDeleted) {
            wrapPositionExceptions(() -> {
                final char charAtOffset = originDocument.get(position.offset, 1).charAt(0);
                if (isUpperCase(charAtOffset)) {
                    originDocument.replace(position.offset, 1, "" + Character.toLowerCase(charAtOffset));
                }
                originDocument.removePosition(TO_LOWER_CASE_CATEGORY, position);
                position.delete();
            });
        }
    }

    public Position[] getToLowerCases(final int imageOffset, final int imageLength) throws BadLocationException {
        return wrapBadPositionCategoryExceptionFunction(imageOffset, imageLength,
                (off, len) -> {
                    final IRegion imageRegion = getDocumentInformationMapping()
                            .toOriginRegion(new Region(imageOffset, imageLength));
                    return originDocument.getPositions(TO_LOWER_CASE_CATEGORY, imageRegion.getOffset(),
                            imageRegion.getLength(), false, false);
                });
    }

    public boolean getToLowerCaseAt(final int imageOffset) throws BadLocationException {
        return getToLowerCases(imageOffset, 1).length > 0;
    }

    protected void requireNonNegativeOffset(final int offset) throws BadLocationException {
        if (offset < 0) {
            throw new BadLocationException("offset " + offset + " < 0");
        }
    }

    protected void requireOffsetNotGreaterThanLength(final int offset, final IDocument document)
            throws BadLocationException {
        final int length = document.getLength();
        if (offset > length) {
            throw new BadLocationException("offset=" + offset + " > length=" + length);
        }
    }

}
