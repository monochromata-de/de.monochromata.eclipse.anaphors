package de.monochromata.eclipse.persp;

import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.projection.Fragment;

/**
 * Represents a region in the origin document that is hidden from the image
 * document. It is used to remove hidden regions from the origin document
 * without relying on the {@link Fragment} positions of the origin document.
 * (Fragment is an internal class that is public for testing only.)
 */
public class HidePosition extends Position {

    public static final String HIDE_CATEGORY = "__HideCategory"; //$NON-NLS-1$

    public HidePosition(final int offset, final int length) {
        super(offset, length);
    }

}
