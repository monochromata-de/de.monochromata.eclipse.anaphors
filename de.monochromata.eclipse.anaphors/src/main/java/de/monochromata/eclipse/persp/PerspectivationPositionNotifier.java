package de.monochromata.eclipse.persp;

import static de.monochromata.eclipse.persp.PerspectivationPosition.PERSPECTIVATION_CATEGORY;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadLocationException;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapPositionExceptions;
import static java.util.Arrays.stream;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPositionCategoryException;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocumentListener;

/**
 * Like {@link PerspectivationPosition} instances, this listener should only be
 * added to master documents of {@link PerspectivationDocument}s or instances of
 * subtypes.
 * <p>
 * This listener is added to the master document instead of to the
 * perspectivation document because it reacts to
 * {@link #documentChanged(DocumentEvent)}, not
 * {@link #documentAboutToBeChanged(DocumentEvent)}, i.e. when informed about an
 * event that removed text, the removal has already happened and the event might
 * contain a range (from before the removal) that exceeds the length of the
 * document after removal. When the listener was registered with the
 * perspectivation document, the range in the document event needed to be
 * translated to an origin range - which at times did yield a
 * {@link BadLocationException}. Since the range from the document event is only
 * required to check the condition, not be actually access the document, no
 * translation is required now that both document event and condition refer to
 * origin coordinates.
 */
public class PerspectivationPositionNotifier implements IDocumentListener {

    private final PerspectivationDocument perspectivationDocument;

    public PerspectivationPositionNotifier(final PerspectivationDocument perspectivationDocument) {
        this.perspectivationDocument = perspectivationDocument;
    }

    @Override
    public void documentAboutToBeChanged(final DocumentEvent originEvent) {
    }

    @Override
    public void documentChanged(final DocumentEvent originEvent) {
        wrapPositionExceptions(() -> documentChanged0(originEvent));
    }

    protected void documentChanged0(final DocumentEvent originEvent)
            throws BadLocationException, BadPositionCategoryException {
        stream(originEvent.fDocument.getPositions(PERSPECTIVATION_CATEGORY))
                .map(position -> (PerspectivationPosition) position)
                .filter(PerspectivationPosition::yetToBePerspectivated)
                .forEach(position -> wrapBadLocationException(
                        () -> position.notify(originEvent, perspectivationDocument)));
    }
}