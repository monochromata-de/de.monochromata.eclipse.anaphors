package de.monochromata.eclipse.persp;

import org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitEditor;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.swt.custom.ST;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.texteditor.AbstractTextEditor;
import org.eclipse.ui.texteditor.ITextEditorActionDefinitionIds;
import org.eclipse.ui.texteditor.TextNavigationAction;

/**
 * A class to fix issues with the existing editors that arise from omitting
 * parts of lines.
 * <p>
 * To be used in conjunction with {@link PerspectivationViewer} or subclasses.
 */
public class CompilationUnitPerspectivationEditor extends CompilationUnitEditor {

    /**
     * A copy of {@link LineEndAction} that uses line information from the image
     * document instead of line information from the origin document because
     * perspectivations can hide code that is shorter than a single line.
     *
     * @see LineEndAction
     */
    protected class PerspectivationSafeLineEndAction extends TextNavigationAction {

        /**
         * boolean flag which tells if the text up to the line end should be selected.
         */
        private final boolean fDoSelect;

        /**
         * Create a new line end action.
         *
         * @param textWidget
         *            the styled text widget
         * @param doSelect
         *            a boolean flag which tells if the text up to the line end should
         *            be selected
         */
        public PerspectivationSafeLineEndAction(final StyledText textWidget, final boolean doSelect) {
            super(textWidget, ST.LINE_END);
            fDoSelect = doSelect;
        }

        /**
         * Computes the offset of the line end position.
         *
         * @param document
         *            the document where to compute the line end position
         * @param line
         *            the line to determine the end position of
         * @param length
         *            the length of the line
         * @param offset
         *            the caret position in the document
         * @return the offset of the line end
         * @since 3.4 protected, was added in 3.3 as private method
         */
        protected int getLineEndPosition(/* final IDocument document, */final String line, final int length/*
                                                                                                            * , final
                                                                                                            * int offset
                                                                                                            */) {
            int index = length - 1;
            while (index > -1 && Character.isWhitespace(line.charAt(index))) {
                index--;
            }
            index++;

            /*
             * LinkedModeModel model= LinkedModeModel.getModel(document, offset); if (model
             * != null) { LinkedPosition linkedPosition= model.findPosition(new
             * LinkedPosition(document, offset, 0)); if (linkedPosition != null) { int
             * linkedPositionEnd= linkedPosition.getOffset() + linkedPosition.getLength();
             * int lineOffset; try { lineOffset=
             * document.getLineInformationOfOffset(offset).getOffset(); if (offset !=
             * linkedPositionEnd && linkedPositionEnd - lineOffset < index) index=
             * linkedPositionEnd - lineOffset; } catch (BadLocationException e) { //should
             * not happen } } }
             */
            return index;
        }

        @Override
        public void run() {
            boolean isSmartHomeEndEnabled = false;
            final IPreferenceStore store = getPreferenceStore();
            if (store != null) {
                isSmartHomeEndEnabled = store.getBoolean(AbstractTextEditor.PREFERENCE_NAVIGATION_SMART_HOME_END);
            }

            final StyledText st = getSourceViewer().getTextWidget();
            if (st == null || st.isDisposed()) {
                return;
            }
            final int caretOffset = st.getCaretOffset();
            final int lineNumber = st.getLineAtOffset(caretOffset);
            final int lineOffset = st.getOffsetAtLine(lineNumber);

            int lineLength;
            // int caretOffsetInDocument;
            final IDocument document = getSourceViewer().getDocument();

            try {
                // caretOffsetInDocument= widgetOffset2ModelOffset(getSourceViewer(),
                // caretOffset);
                // lineLength=
                // document.getLineInformationOfOffset(caretOffsetInDocument).getLength();
                lineLength = ((PerspectivationViewer) getSourceViewer()).getLineLengthInImageDocument(caretOffset);
            } catch (final BadLocationException ex) {
                return;
            }
            int lineEndOffset = lineOffset + lineLength;

            final int delta = lineEndOffset - st.getCharCount();
            if (delta > 0) {
                lineEndOffset -= delta;
                lineLength -= delta;
            }

            String line = ""; //$NON-NLS-1$
            if (lineLength > 0) {
                line = st.getText(lineOffset, lineEndOffset - 1);
            }

            // Remember current selection
            final Point oldSelection = st.getSelection();

            // The new caret position
            int newCaretOffset = -1;

            if (isSmartHomeEndEnabled) {
                // Compute the line end offset
                final int i = getLineEndPosition(/* document, */line, lineLength/* , caretOffsetInDocument */);

                if (caretOffset - lineOffset == i) {
                    // to end of line
                    newCaretOffset = lineEndOffset;
                } else {
                    // to end of text
                    newCaretOffset = lineOffset + i;
                }

            } else {

                if (caretOffset < lineEndOffset) {
                    // to end of line
                    newCaretOffset = lineEndOffset;
                }

            }

            if (newCaretOffset == -1) {
                newCaretOffset = caretOffset;
            } else {
                st.setCaretOffset(newCaretOffset);
            }

            st.setCaretOffset(newCaretOffset);
            if (fDoSelect) {
                if (caretOffset < oldSelection.y) {
                    st.setSelection(oldSelection.y, newCaretOffset);
                } else {
                    st.setSelection(oldSelection.x, newCaretOffset);
                }
            } else {
                st.setSelection(newCaretOffset);
            }

            fireSelectionChanged(oldSelection);
        }
    }

    @Override
    protected void createNavigationActions() {
        super.createNavigationActions();

        final StyledText textWidget = getSourceViewer().getTextWidget();

        IAction action = new PerspectivationSafeLineEndAction(textWidget, false);
        action.setActionDefinitionId(ITextEditorActionDefinitionIds.LINE_END);
        setAction(ITextEditorActionDefinitionIds.LINE_END, action);

        action = new PerspectivationSafeLineEndAction(textWidget, true);
        action.setActionDefinitionId(ITextEditorActionDefinitionIds.SELECT_LINE_END);
        setAction(ITextEditorActionDefinitionIds.SELECT_LINE_END, action);
    }

}
