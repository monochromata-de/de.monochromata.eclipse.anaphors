package de.monochromata.eclipse.persp;

import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadLocationException;
import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.Position;

import de.monochromata.anaphors.perspectivation.Perspectivation;
import de.monochromata.eclipse.position.DoublyLinkedPosition;

/**
 * Position added to the master document of {@link PerspectivationDocument}s to
 * represent future or currently performed perspectivations.
 * <p>
 * The positions are added to the master document instead of the slave document,
 * because they are easier to retrieve from there because regions are removed
 * from the image document which makes it harder to retrieve all positions on
 * these removed regions.
 * <p>
 * Positions are used instead of annotations. Annotations provide the
 * possibility of display to users and interaction with users which is not
 * required here.
 */
public class PerspectivationPosition extends Position implements DoublyLinkedPosition<PerspectivationPosition> {

    public static final String PERSPECTIVATION_CATEGORY = PerspectivationPosition.class.getName();

    /**
     * The condition that determines after which {@link DocumentEvent} the
     * perspectivation is to be performed (typically the last event in a series of
     * events emitted by a source code modification/refactoring). The condition is
     * necessary
     * <ul>
     * <li>because perspectivation yields {@link DocumentEvent}s itself but it
     * should not trigger itself recursively, and
     * <li>because perspectivation can only be performed against a known basis, e.g.
     * it is not possible to remove a master document range before it has been
     * inserted.
     * </ul>
     */
    public final Predicate<DocumentEvent> condition;
    public final List<Perspectivation> perspectivations;
    private PerspectivationPosition previous;
    private final List<PerspectivationPosition> next = new ArrayList<>();
    private boolean isEnabled = true;
    private boolean conditionHasBeenMet = false;
    private boolean isPerspectivated = false;
    private PerspectivationDocument documentUsedForPerspectivation;
    private final List<ToLowerCasePosition> toLowerCasePositions = new ArrayList<>();
    private final List<HidePosition> hidePositions = new ArrayList<>();

    /**
     *
     * @param condition
     *            See the {@link #condition} field JavaDoc
     */
    public PerspectivationPosition(final Predicate<DocumentEvent> condition,
            final List<Perspectivation> perspectivations) {
        this.condition = condition;
        this.perspectivations = perspectivations;
    }

    /**
     *
     * @param condition
     *            See the {@link #condition} field JavaDoc
     */
    public PerspectivationPosition(final int offset, final Predicate<DocumentEvent> condition,
            final List<Perspectivation> perspectivations) {
        super(offset);
        this.condition = condition;
        this.perspectivations = perspectivations;
    }

    /**
     * For testing: Create a position without perspectivations that is applied to
     * the first event received.
     */
    public PerspectivationPosition(final int offset, final int length) {
        this(offset, length, unused -> true, emptyList());
    }

    /**
     *
     * @param condition
     *            See the {@link #condition} field JavaDoc
     */
    public PerspectivationPosition(final int offset, final int length, final Predicate<DocumentEvent> condition,
            final List<Perspectivation> perspectivations) {
        super(offset, length);
        this.condition = condition;
        this.perspectivations = perspectivations;
    }

    public List<ToLowerCasePosition> getToLowerCasePositions() {
        return unmodifiableList(toLowerCasePositions);
    }

    public List<HidePosition> getHidePositions() {
        return unmodifiableList(hidePositions);
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(final boolean isEnabled, final PerspectivationDocument document)
            throws BadLocationException {
        if (conditionHasBeenMet) {
            togglePerspectivation(isEnabled, document);
        }
        this.isEnabled = isEnabled;
    }

    protected void togglePerspectivation(final boolean isEnabled, final PerspectivationDocument document)
            throws BadLocationException {
        if (conditionHasBeenMet && !this.isEnabled && isEnabled) {
            doPerspectivation(isEnabled, document);
        } else if (conditionHasBeenMet && this.isEnabled && !isEnabled) {
            undoPerspectivation(document);
        }
    }

    public boolean conditionHasBeenMet() {
        return conditionHasBeenMet;
    }

    public void notify(final DocumentEvent originEvent, final PerspectivationDocument document)
            throws BadLocationException {
        if (!isPerspectivated && condition.test(originEvent)) {
            conditionHasBeenMet = true;
        }
        if (isEnabled) {
            documentUsedForPerspectivation = document;
            doPerspectivation(isEnabled, documentUsedForPerspectivation);
        }
    }

    protected void doPerspectivation(final boolean isEnabled, final PerspectivationDocument document)
            throws BadLocationException {
        if (!isPerspectivated && isEnabled && conditionHasBeenMet) {
            applyPerspectivations(document);
            isPerspectivated = true;
        }
    }

    protected void applyPerspectivations(final PerspectivationDocument document)
            throws BadLocationException {
        perspectivations.forEach(perspectivation -> apply(perspectivation, document));
    }

    protected void apply(final Perspectivation perspectivation,
            final PerspectivationDocument document) {
        if (perspectivation instanceof Perspectivation.Hide) {
            apply((Perspectivation.Hide) perspectivation, document);
        } else if (perspectivation instanceof Perspectivation.ToLowerCase) {
            apply((Perspectivation.ToLowerCase) perspectivation, document);
        } else {
            throw new IllegalArgumentException(
                    "Unknown type of perspectivation: " + perspectivation.getClass().getName());
        }
    }

    protected void apply(final Perspectivation.Hide hide,
            final PerspectivationDocument document) {
        wrapBadLocationException(
                () -> {
                    final HidePosition hidePosition = document.hideOriginDocumentRange(
                            offset + hide.originOffsetRelativeToPositionOffset,
                            hide.length);
                    hidePositions.add(hidePosition);
                });
    }

    protected void apply(final Perspectivation.ToLowerCase toLowerCase,
            final PerspectivationDocument document) {
        wrapBadLocationException(
                () -> {
                    final int imageOffset = document.getDocumentInformationMapping().toImageOffset(
                            offset + toLowerCase.originOffsetRelativeToPositionOffset);
                    final Optional<ToLowerCasePosition> optionalToLowerCase = document.toLowerCase(imageOffset);
                    optionalToLowerCase.orElseThrow(() -> new IllegalStateException("no ToLowerCasePosition added"));
                    optionalToLowerCase.ifPresent(toLowerCasePosition -> toLowerCasePositions.add(toLowerCasePosition));
                });
    }

    public boolean isPerspectivated() {
        return isPerspectivated;
    }

    public boolean yetToBePerspectivated() {
        return !isPerspectivated();
    }

    public void undoPerspectivation() throws BadLocationException {
        undoPerspectivation(documentUsedForPerspectivation);
        if (isEnabled && isPerspectivated()) {
            documentUsedForPerspectivation = null;
        }
    }

    protected void undoPerspectivation(final PerspectivationDocument document) throws BadLocationException {
        if (isEnabled && isPerspectivated()) {
            removeToLowerCases(document);
            addMasterDocumentRanges(document);
            isPerspectivated = false;
        }
    }

    protected void removeToLowerCases(final PerspectivationDocument document) throws BadLocationException {
        toLowerCasePositions.forEach(document::removeToLowerCase);
        toLowerCasePositions.clear();
    }

    protected void addMasterDocumentRanges(final PerspectivationDocument document) throws BadLocationException {
        hidePositions.forEach(document::addMasterDocumentRange);
        hidePositions.clear();
    }

    @Override
    public PerspectivationPosition getPrevious() {
        return previous;
    }

    @Override
    public void setPrevious(final PerspectivationPosition previous) {
        this.previous = previous;
    }

    @Override
    public List<PerspectivationPosition> getNext() {
        return next;
    }

    @Override
    public void addNext(final PerspectivationPosition next) {
        this.next.add(next);
    }

    @Override
    public void addNext(final List<PerspectivationPosition> next) {
        this.next.addAll(next);
    }

    @Override
    public void removeNext(final PerspectivationPosition next) {
        this.next.remove(next);
    }

    @Override
    public void removeNext(final List<PerspectivationPosition> next) {
        this.next.removeAll(next);
    }

}
