package de.monochromata.eclipse.persp;

import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadLocationException;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadLocationExceptionFunction;
import static java.util.Arrays.stream;
import static java.util.Objects.requireNonNull;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentInformationMapping;
import org.eclipse.jface.text.IDocumentInformationMappingExtension;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextStore;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.Region;

/**
 * Modelled after {@code org.eclipse.jface.text.ProjectionTextStore} but able to
 * replace text in the master document.
 */
public class PerspectivationTextStore implements ITextStore {

    /**
     * Implementation of {@link IRegion} that can be reused by setting the offset
     * and the length.
     */
    private static class ReusableRegion implements IRegion {

        private int fOffset;
        private int fLength;

        @Override
        public int getLength() {
            return fLength;
        }

        @Override
        public int getOffset() {
            return fOffset;
        }

        /**
         * Updates this region.
         *
         * @param offset
         *            the new offset
         * @param length
         *            the new length
         */
        public void update(final int offset, final int length) {
            fOffset = offset;
            fLength = length;
        }
    }

    /** The master document */
    private final IDocument fMasterDocument;
    private final PerspectivationDocument fSlaveDocument;
    /** The document information mapping */
    private final IDocumentInformationMapping fMapping;
    private final IDocumentInformationMappingExtension fMappingExtension;
    /** Internal region used for querying the mapping. */
    private final ReusableRegion fReusableRegion = new ReusableRegion();

    public PerspectivationTextStore(final IDocument masterDocument, final PerspectivationDocument slaveDocument,
            final IDocumentInformationMapping mapping) {
        fMasterDocument = masterDocument;
        fSlaveDocument = slaveDocument;
        fMapping = mapping;
        fMappingExtension = (IDocumentInformationMappingExtension) mapping;
    }

    @Override
    public void set(final String contents) {
        wrapBadLocationException(contents, this::setWithException);
    }

    protected void setWithException(final String contents) throws BadLocationException {
        final IRegion masterRegion = requireNonNull(fMapping.getCoverage(), "master region is null");
        fMasterDocument.replace(masterRegion.getOffset(), masterRegion.getLength(), contents);
    }

    @Override
    public void replace(final int offset, final int length, final String text) {
        wrapBadLocationException(offset, length, text, this::replaceWithException);
    }

    protected void replaceWithException(final int offset, final int length, final String text)
            throws BadLocationException {
        fReusableRegion.update(offset, length);
        final IRegion masterRegion = fMapping.toOriginRegion(fReusableRegion);
        fMasterDocument.replace(masterRegion.getOffset(), masterRegion.getLength(), text);
    }

    @Override
    public int getLength() {
        return fMappingExtension.getImageLength();
    }

    @Override
    public char get(final int offset) {
        final char charFromMaster = getFromMaster(offset);
        return getToLowerCaseAt(offset) ? Character.toLowerCase(charFromMaster) : charFromMaster;
    }

    protected boolean getToLowerCaseAt(final int offset) {
        return wrapBadLocationExceptionFunction(offset, off -> {
            return fSlaveDocument.getToLowerCaseAt(off);
        });
    }

    protected char getFromMaster(final int offset) {
        return wrapBadLocationExceptionFunction(offset, off -> {
            final int originOffset = fMapping.toOriginOffset(off);
            return fMasterDocument.getChar(originOffset);
        });
    }

    @Override
    public String get(final int offset, final int length) {
        return wrapBadLocationExceptionFunction(offset, length, (off, len) -> {
            final StringBuilder builder = new StringBuilder();
            addMasterContent(off, len, builder);
            applyLowerCasing(off, len, builder);
            return builder.toString();
        });
    }

    protected void addMasterContent(final Integer off, final Integer len, final StringBuilder builder)
            throws BadLocationException {
        final IRegion[] fragments = fMappingExtension.toExactOriginRegions(new Region(off, len));
        for (final IRegion fragment : fragments) {
            builder.append(fMasterDocument.get(fragment.getOffset(), fragment.getLength()));
        }
    }

    protected void applyLowerCasing(final Integer offset, final Integer length, final StringBuilder builder)
            throws BadLocationException {
        stream(fSlaveDocument.getToLowerCases(offset, length))
                .forEach(toLowerCase -> toLowerCase(builder, offset, toLowerCase));
    }

    protected void toLowerCase(final StringBuilder builder, final int firstOffsetInBuilder,
            final Position toLowerCase) {
        final int imageOffset = wrapBadLocationException(
                () -> fSlaveDocument.getDocumentInformationMapping().toImageOffset(toLowerCase.offset));
        final int charOffset = imageOffset - firstOffsetInBuilder;
        final char lowerCasedChar = Character.toLowerCase(builder.charAt(charOffset));
        builder.replace(charOffset, charOffset + 1, Character.toString(lowerCasedChar));
    }

}
