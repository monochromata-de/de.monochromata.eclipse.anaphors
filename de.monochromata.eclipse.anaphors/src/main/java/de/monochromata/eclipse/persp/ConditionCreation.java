package de.monochromata.eclipse.persp;

import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadLocationException;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.text.edits.CopySourceEdit;
import org.eclipse.text.edits.CopyTargetEdit;
import org.eclipse.text.edits.InsertEdit;
import org.eclipse.text.edits.TextEdit;

public interface ConditionCreation {

	static Predicate<DocumentEvent> getCondition(final TextEdit textEdit) {
		final var memory = new AtomicReference<Predicate<DocumentEvent>>();
		return documentEvent -> {
			final var condition = memoize(memory, () -> createCondition(textEdit));
			return condition.test(documentEvent);
		};
	}

	private static Predicate<DocumentEvent> memoize(final AtomicReference<Predicate<DocumentEvent>> memory,
			final Supplier<Predicate<DocumentEvent>> factory) {
		final var memoized = memory.get();
		if (memoized != null) {
			return memoized;
		}
		final var newValue = factory.get();
		memory.set(newValue);
		return newValue;
	}

	static Predicate<DocumentEvent> createCondition(final TextEdit textEdit) {
		final TextEdit[] relevantEdits = getRelevantEdits(textEdit);
		if (relevantEdits != null) {
			if (hasInsertEdit(relevantEdits)) {
				return createConditionForInsertEdit(relevantEdits);
			} else if (hasCopyTargetEdit(relevantEdits)) {
				return createConditionForCopyTargetEdit(relevantEdits);
			}
		}
		throw new IllegalArgumentException("Could not create condition for " + textEdit);
	}

	static TextEdit[] getRelevantEdits(final TextEdit textEdit) {
		// The first child is used to delete text.
		// The next child is optional and add imports if present.
		// The last child inserts text that shall be perspectivated.
		if (textEdit.getChildrenSize() < 2) {
			return null;
		}
		final TextEdit lastSecondLevelEdit = textEdit.getChildren()[textEdit.getChildrenSize() - 1];
		if (lastSecondLevelEdit.hasChildren()) {
			return lastSecondLevelEdit.getChildren();
		}
		return null;
	}

	static boolean hasInsertEdit(final TextEdit[] relevantEdits) {
		return relevantEdits.length >= 1 && relevantEdits[0] instanceof InsertEdit;
	}

	static Predicate<DocumentEvent> createConditionForInsertEdit(final TextEdit[] relevantEdits) {
		return originEvent -> checkConditionForInsertEdit(relevantEdits, originEvent);
	}

	static boolean checkConditionForInsertEdit(final TextEdit[] relevantEdits, final DocumentEvent originEvent) {
		final InsertEdit insertEdit = (InsertEdit) relevantEdits[0];
		return originEvent.fOffset == insertEdit.getOffset() && originEvent.fLength == insertEdit.getLength() // assumed
																												// to be
																												// always
																												// 0
				&& originEvent.fText.equals(insertEdit.getText());
	}

	static boolean hasCopyTargetEdit(final TextEdit[] relevantEdits) {
		return relevantEdits.length >= 2 && relevantEdits[0] instanceof CopySourceEdit
				&& relevantEdits[1] instanceof CopyTargetEdit;
	}

	static Predicate<DocumentEvent> createConditionForCopyTargetEdit(final TextEdit[] relevantEdits) {
		return originEvent -> checkConditionForCopyTargetEdit(relevantEdits, originEvent);
	}

	static boolean checkConditionForCopyTargetEdit(final TextEdit[] relevantEdits, final DocumentEvent originEvent) {
		final CopyTargetEdit copyTargetEdit = (CopyTargetEdit) relevantEdits[1];
		final CopySourceEdit copySourceEdit = copyTargetEdit.getSourceEdit();
		return originEvent.fOffset == copyTargetEdit.getOffset()
				&& originEvent.getLength() == copyTargetEdit.getLength() // assumed to be always 0
				&& originEvent.fText.equals(getEditText(copySourceEdit, originEvent));
	}

	static String getEditText(final CopySourceEdit copySourceEdit, final DocumentEvent originEvent) {
		return wrapBadLocationException(
				() -> originEvent.getDocument().get(copySourceEdit.getOffset(), copySourceEdit.getLength()));
	}
}
