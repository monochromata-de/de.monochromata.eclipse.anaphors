package de.monochromata.eclipse.persp;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.projection.ProjectionDocumentManager;

public class PerspectivationDocumentManager extends ProjectionDocumentManager {

    public PerspectivationDocumentManager() {
    }

    /**
     * @return a PerspectivationDocument for the given master (with the correct
     *         return type)
     */
    @Override
    public PerspectivationDocument createSlaveDocument(final IDocument master) {
        return (PerspectivationDocument) super.createSlaveDocument(master);
    }

    @Override
    protected PerspectivationDocument createProjectionDocument(final IDocument master) {
        return new PerspectivationDocument((AbstractDocument) master);
    }

}
