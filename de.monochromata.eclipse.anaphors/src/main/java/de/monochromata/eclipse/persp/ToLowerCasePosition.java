package de.monochromata.eclipse.persp;

import org.eclipse.jface.text.Position;

/**
 * Represents the conversion of a single-character to lower-case.
 */
public class ToLowerCasePosition extends Position {

	public static final String TO_LOWER_CASE_CATEGORY = "__toLowerCaseCategory"; //$NON-NLS-1$

	/**
	 * Creates a new instance at the given offset, with length {@literal 1}.
	 */
	public ToLowerCasePosition(final int offset) {
		super(offset, 1);
	}

}
