package de.monochromata.eclipse.persp;

/**
 * An extension of {@link org.eclipse.jface.text.projection.ProjectionDocument}
 * that not only permits regions of source code from a master document to be
 * omitted in a slave document, but also allows contents in the slave document
 * that are not present in the master document.
 */