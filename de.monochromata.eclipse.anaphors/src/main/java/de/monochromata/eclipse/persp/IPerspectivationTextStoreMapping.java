package de.monochromata.eclipse.persp;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IRegion;

/**
 * The mapping interface provided to {@link PerspectivationTextStore}. Modelled
 * after {@code org.eclipse.jface.text.projection.IMinimalMapping}.
 */
public interface IPerspectivationTextStoreMapping {

	/**
	 * @see org.eclipse.jface.text.IDocumentInformationMapping#getCoverage()
	 */
	IRegion getCoverage();

	/**
	 * @see org.eclipse.jface.text.IDocumentInformationMapping#toOriginRegion(
	 *      IRegion)
	 */
	IRegion toOriginRegion(IRegion region) throws BadLocationException;

	/**
	 * @see org.eclipse.jface.text.IDocumentInformationMapping#toOriginOffset(int)
	 */
	int toOriginOffset(int offset) throws BadLocationException;

	/**
	 * @see org.eclipse.jface.text.IDocumentInformationMappingExtension#
	 *      toExactOriginRegions(IRegion)
	 */
	IRegion[] toExactOriginRegions(IRegion region) throws BadLocationException;

	/**
	 * @see org.eclipse.jface.text.IDocumentInformationMappingExtension#
	 *      getImageLength()
	 */
	int getImageLength();

}
