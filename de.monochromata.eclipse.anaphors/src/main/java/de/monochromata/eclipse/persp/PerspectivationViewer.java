package de.monochromata.eclipse.persp;

import org.eclipse.jdt.internal.ui.javaeditor.JavaSourceViewer;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.source.IOverviewRuler;
import org.eclipse.jface.text.source.IVerticalRuler;
import org.eclipse.swt.widgets.Composite;

public class PerspectivationViewer extends JavaSourceViewer {

    public PerspectivationViewer(final Composite parent, final IVerticalRuler verticalRuler,
            final IOverviewRuler overviewRuler,
            final boolean showAnnotationsOverview, final int styles, final IPreferenceStore store) {
        super(parent, verticalRuler, overviewRuler, showAnnotationsOverview, styles, store);
    }

    public int getLineLengthInImageDocument(final int offsetInImage) throws BadLocationException {
        return ((PerspectivationDocument) getVisibleDocument()).getLineInformationOfOffset(offsetInImage).getLength();
    }

}
