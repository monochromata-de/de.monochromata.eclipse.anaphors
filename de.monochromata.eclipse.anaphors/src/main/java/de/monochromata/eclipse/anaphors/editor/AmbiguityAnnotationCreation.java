package de.monochromata.eclipse.anaphors.editor;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.compiler.IProblem;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jdt.internal.compiler.problem.DefaultProblem;
import org.eclipse.jdt.internal.compiler.problem.ProblemSeverities;
import org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitDocumentProvider.ProblemAnnotation;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.cog.Resolution;
import de.monochromata.eclipse.anaphors.ProblemIds;

public interface AmbiguityAnnotationCreation {

	static Pair<Annotation, Position> createProblemAnnotationForResolutions(final CompilationUnit cu,
			final Position definiteExpressionPosition,
			final List<Resolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>> resolutions) {
		final List<PublicAnaphora> anaphoraRelations = createAnaphoraRelations(resolutions);
		return createProblemAnnotationForAnaphoraRelations(cu, definiteExpressionPosition, anaphoraRelations);
	}

	static Pair<Annotation, Position> createProblemAnnotationForAnaphoraRelations(final CompilationUnit cu,
			final Position definiteExpressionPosition, final List<PublicAnaphora> anaphoraRelations) {
		final String details = createDetails(anaphoraRelations);
		return createAnnotationAndPosition(cu, definiteExpressionPosition, details);
	}

	static Pair<Annotation, Position> createAnnotationAndPosition(final CompilationUnit cu,
			final Position definiteExpressionPosition, final String details) {
		final Annotation annotation = createAnnotation(cu, definiteExpressionPosition, details);
		final Position position = new Position(definiteExpressionPosition.offset, definiteExpressionPosition.length);
		return new ImmutablePair<>(annotation, position);
	}

	@SuppressWarnings("restriction")
	static Annotation createAnnotation(final CompilationUnit cu, final Position definiteExpressionPosition,
			final String details) {
		final IProblem problem = createProblem(cu, definiteExpressionPosition, details);
		// TODO: Do not use the JDT-internal ProblemAnnotation
		final ICompilationUnit typeRoot = (ICompilationUnit) cu.getTypeRoot();
		return new ProblemAnnotation(problem, typeRoot);
	}

	static IProblem createProblem(final CompilationUnit cu, final Position definiteExpressionPosition,
			final String details) {
		final int start = definiteExpressionPosition.offset;
		final int end = start + definiteExpressionPosition.length;
		final int lineNumber = cu.getLineNumber(start);
		final int columNumber = cu.getColumnNumber(start);
		final char[] originatingFileName = cu.getTypeRoot().getResource().getFullPath().toOSString().toCharArray();
		return createProblem(details, start, end, lineNumber, columNumber, originatingFileName);
	}

	@SuppressWarnings("restriction")
	static IProblem createProblem(final String details, final int start, final int end, final int lineNumber,
			final int columNumber, final char[] originatingFileName) {
		return new DefaultProblem(originatingFileName,
				"Ambiguous reference (more than 1 potential referents found):\n" + details,
				/* id */ ProblemIds.AnaphorRelated + 1, /* arguments */ new String[0], ProblemSeverities.Error, start,
				end, lineNumber, columNumber);
	}

	static List<PublicAnaphora> createAnaphoraRelations(
			final List<Resolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>> resolutions) {
		return resolutions.stream().map(resolution -> resolution.getPreliminaryAnaphora()).collect(Collectors.toList());
	}

	static String createDetails(final List<PublicAnaphora> anaphoraRelations) {
		return anaphoraRelations.stream().map(ASTBasedAnaphora::getReferenceDescription)
				.collect(Collectors.joining("\n"));
	}

}
