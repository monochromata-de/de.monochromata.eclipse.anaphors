package de.monochromata.eclipse.anaphors.position;

import org.eclipse.jdt.core.compiler.IProblem;
import org.eclipse.jface.text.Position;

public class KnownProblemPosition extends Position {

    public static final String KNOWN_PROBLEM_CATEGORY = KnownProblemPosition.class.getName();

    public final int problemID;
    public final String problemMessage;

    public KnownProblemPosition(final IProblem knownProblem) {
        this(knownProblem.getSourceStart(), knownProblem.getSourceEnd() - knownProblem.getSourceStart() + 1,
                knownProblem.getID(), knownProblem.getMessage());
    }

    public KnownProblemPosition(final int offset, final int length,
            final int problemID, final String problemMessage) {
        super(offset, length);
        this.problemID = problemID;
        this.problemMessage = problemMessage;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + problemID;
        result = prime * result + ((problemMessage == null) ? 0 : problemMessage.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final KnownProblemPosition other = (KnownProblemPosition) obj;
        if (problemID != other.problemID) {
            return false;
        }
        if (problemMessage == null) {
            if (other.problemMessage != null) {
                return false;
            }
        } else if (!problemMessage.equals(other.problemMessage)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "KnownProblemPosition [problemID=" + problemID + ", problemMessage=" + problemMessage + ", offset="
                + offset + ", length=" + length + ", isDeleted=" + isDeleted + "]";
    }

}
