package de.monochromata.eclipse.anaphors.editor.update;

import static java.util.Collections.singletonList;

import java.util.List;
import java.util.function.Consumer;

import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.persp.PerspectivationPosition;

public interface AnaphoraDeletion {

	static void deletePositionIn(final AbstractAnaphoraPositionForRepresentation anaphoraPosition,
			final Consumer<PerspectivationPosition> deletePerspectivationPosition,
			final Consumer<AbstractAnaphoraPositionForRepresentation> deleteAnaphoraPosition) {
		deletePositionsIn(singletonList(anaphoraPosition), deletePerspectivationPosition, deleteAnaphoraPosition);
	}

	static void deletePositionsIn(final List<? extends AbstractAnaphoraPositionForRepresentation> anaphoraPositions,
			final Consumer<PerspectivationPosition> deletePerspectivationPosition,
			final Consumer<AbstractAnaphoraPositionForRepresentation> deleteAnaphoraPosition) {
		anaphoraPositions.stream().peek(
				anaphoraPosition -> deletePerspectivationPosition.accept(anaphoraPosition.perspectivationPosition))
				.forEach(deleteAnaphoraPosition);
	}

	static Consumer<PerspectivationPosition> delete(
			final Consumer<PerspectivationPosition> maintainPerspectivationBeforeDeletion,
			final List<PerspectivationPosition> perspectivationPositionsToDelete,
			final Consumer<PerspectivationPosition> perspectivationDeletionStrategy) {
		return position -> {
			maintainPerspectivationBeforeDeletion.accept(position);
			perspectivationPositionsToDelete.add(position);
			perspectivationDeletionStrategy.accept(position);
		};
	}

	static Consumer<AbstractAnaphoraPositionForRepresentation> delete(
			final Consumer<AbstractAnaphoraPositionForRepresentation> maintainAnaphorasBeforeDeletion,
			final List<AbstractAnaphoraPositionForRepresentation> anaphoraPositionsToDelete) {
		return position -> {
			maintainAnaphorasBeforeDeletion.accept(position);
			anaphoraPositionsToDelete.add(position);
		};
	}

}
