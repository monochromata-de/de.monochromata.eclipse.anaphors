package de.monochromata.eclipse.anaphors.editor.update;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jface.text.Position;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.KindComposition;
import de.monochromata.anaphors.ast.RelatedExpressionPart;

public interface AnaphoraComparing {

	static <R extends Position, A extends Position> boolean isEqual(final Triple<R, Anaphora, A> oldAnaphora,
			final Triple<Position, Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>, Position> newAnaphora) {
		return isEqualRaw(anaphoraToKindAndAnaphor(oldAnaphora), astBasedAnaphoraToKindAndAnaphor(newAnaphora));
	}

	static <R extends Position, A extends Position> Triple<R, Pair<String, String>, A> anaphoraToKindAndAnaphor(
			final Triple<R, Anaphora, A> oldAnaphora) {
		final Pair<String, String> kindAndAnaphor = new ImmutablePair<>(oldAnaphora.getMiddle().kind,
				oldAnaphora.getMiddle().anaphor);
		return new ImmutableTriple<>(oldAnaphora.getLeft(), kindAndAnaphor, oldAnaphora.getRight());
	}

	static Triple<Position, Pair<String, String>, Position> astBasedAnaphoraToKindAndAnaphor(
			final Triple<Position, Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>, Position> newAnaphora) {
		final String kind = KindComposition.getKind(newAnaphora.getMiddle().getLeft(),
				newAnaphora.getMiddle().getRight());
		final Pair<String, String> kindAndAnaphor = new ImmutablePair<>(kind,
				newAnaphora.getMiddle().getRight().getAnaphor());
		return new ImmutableTriple<>(newAnaphora.getLeft(), kindAndAnaphor, newAnaphora.getRight());
	}

	/**
	 *
	 * @param oldAnaphora related expression position, pair of kind and anaphor, and
	 *                    anaphor position
	 * @param newAnaphora related expression position, pair of kind and anaphor, and
	 *                    anaphor position
	 * @return
	 */
	static <R extends Position, A extends Position> boolean isEqualRaw(
			final Triple<R, Pair<String, String>, A> oldAnaphora,
			final Triple<Position, Pair<String, String>, Position> newAnaphora) {
		if (!offsetAndLengthAreEqual(oldAnaphora.getLeft(), newAnaphora.getLeft())) {
			return false;
		}
		if (!oldAnaphora.getMiddle().getLeft().equals(newAnaphora.getMiddle().getLeft())) {
			return false;
		}
		if (!oldAnaphora.getMiddle().getRight().equals(newAnaphora.getMiddle().getRight())) {
			return false;
		}
		if (!offsetAndLengthAreEqual(oldAnaphora.getRight(), newAnaphora.getRight())) {
			return false;
		}
		return true;
	}

	static boolean offsetAndLengthAreEqual(final Position position1, final Position position2) {
		return position1.offset == position2.offset && position1.length == position2.length;
	}
}
