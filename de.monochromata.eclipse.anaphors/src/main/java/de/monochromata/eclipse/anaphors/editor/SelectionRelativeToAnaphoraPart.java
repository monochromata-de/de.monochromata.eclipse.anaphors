package de.monochromata.eclipse.anaphors.editor;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;

public class SelectionRelativeToAnaphoraPart {

    public enum Relation {
        SelectionHasNonZeroLength, OutsideAnaphoraPart, AtBeginningOfAnaphoraPart, InsideAnaphoraPart, AtEndOfAnaphoraPart;
    }

    public final SelectionRelativeToAnaphoraPart.Relation relationBetweenSelectionAndAnaphoraPart;
    public final int distanceBetweenSelectionAndAnaphoraPart;

    public SelectionRelativeToAnaphoraPart(
            final SelectionRelativeToAnaphoraPart.Relation relationBetweenSelectionAndAnaphoraPart,
            final int distanceBetweenSelectionAndAnaphoraPart) {
        this.relationBetweenSelectionAndAnaphoraPart = relationBetweenSelectionAndAnaphoraPart;
        this.distanceBetweenSelectionAndAnaphoraPart = distanceBetweenSelectionAndAnaphoraPart;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + distanceBetweenSelectionAndAnaphoraPart;
        result = prime * result + ((relationBetweenSelectionAndAnaphoraPart == null) ? 0
                : relationBetweenSelectionAndAnaphoraPart.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SelectionRelativeToAnaphoraPart other = (SelectionRelativeToAnaphoraPart) obj;
        if (distanceBetweenSelectionAndAnaphoraPart != other.distanceBetweenSelectionAndAnaphoraPart) {
            return false;
        }
        if (relationBetweenSelectionAndAnaphoraPart != other.relationBetweenSelectionAndAnaphoraPart) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return relationBetweenSelectionAndAnaphoraPart + ":" + distanceBetweenSelectionAndAnaphoraPart;
    }

    public static Pair<Pair<? extends Position, SelectionRelativeToAnaphoraPart>, List<Pair<? extends Position, SelectionRelativeToAnaphoraPart>>> createForPairWithClosestPosition(
            final int selectionOffset, final int selectionLength,
            final List<? extends Position> positions) {
        final List<Pair<? extends Position, SelectionRelativeToAnaphoraPart>> allPositionsAndSelections = createPositionsAndSelections(
                selectionOffset, selectionLength, positions);
        final Pair<? extends Position, SelectionRelativeToAnaphoraPart> closestPositionAndSelection = selectByClosestPosition(
                allPositionsAndSelections.stream(), System.err::println);
        return new ImmutablePair<>(closestPositionAndSelection, allPositionsAndSelections);
    }

    protected static List<Pair<? extends Position, SelectionRelativeToAnaphoraPart>> createPositionsAndSelections(
            final int selectionOffset, final int selectionLength, final List<? extends Position> positions) {
        return positions
                .stream()
                .map(position -> new ImmutablePair<>(position,
                        create(selectionOffset, selectionLength, position)))
                .collect(toList());
    }

    public static Pair<? extends Position, SelectionRelativeToAnaphoraPart> selectByClosestPosition(
            final Stream<Pair<? extends Position, SelectionRelativeToAnaphoraPart>> positionsAndSelections) {
        return selectByClosestPosition(positionsAndSelections, System.err::println);
    }

    public static Pair<? extends Position, SelectionRelativeToAnaphoraPart> selectByClosestPosition(
            final Stream<Pair<? extends Position, SelectionRelativeToAnaphoraPart>> positionsAndSelections,
            final Consumer<String> logger) {
        return positionsAndSelections
                .reduce((input1, input2) -> getCloser(input1, input2, logger))
                .orElseThrow(() -> new IllegalArgumentException("empty stream"));
    }

    private static Pair<? extends Position, SelectionRelativeToAnaphoraPart> getCloser(
            final Pair<? extends Position, SelectionRelativeToAnaphoraPart> input1,
            final Pair<? extends Position, SelectionRelativeToAnaphoraPart> input2,
            final Consumer<String> logger) {
        final int distance1 = input1.getRight().distanceBetweenSelectionAndAnaphoraPart;
        final int distance2 = input2.getRight().distanceBetweenSelectionAndAnaphoraPart;
        final int comparison = compareSignedAndUnsigned(distance1, distance2);
        if (comparison < 0) {
            return input1;
        } else if (comparison == 0) {
            logger.accept("choosing second of two equally close inputs: " + input1 + ", " + input2);
        } // implies comparison >= 0
        return input2;
    }

    private static int compareSignedAndUnsigned(final int distance1, final int distance2) {
        if (distance1 >= 0 && distance2 >= 0) {
            final int signedComparison = Integer.compare(distance1, distance2);
            if (signedComparison != 0) {
                return signedComparison;
            }
        }
        // implies distance1 == distance2
        return Integer.compareUnsigned(distance1, distance2);
    }

    public static SelectionRelativeToAnaphoraPart create(
            final int selectionOffset, final int selectionLength,
            final Position position) {
        final int anaphoraPartOffset = position.offset;
        final int distanceToAnaphoraPartOffset = selectionOffset - anaphoraPartOffset;

        return createRelativeSelectionForAnaphor(selectionOffset, selectionLength, position,
                distanceToAnaphoraPartOffset);
    }

    protected static SelectionRelativeToAnaphoraPart createRelativeSelectionForAnaphor(final int selectionOffset,
            final int selectionLength, final Position anaphorPosition,
            final int distanceToAnaphoraPartOffset) {
        return create(selectionOffset, selectionLength, anaphorPosition, distanceToAnaphoraPartOffset);
    }

    protected static boolean selectionCoversAnaphoraPart(
            final SelectionRelativeToAnaphoraPart relativeSelectionForRelatedExpression) {
        final Relation relation = relativeSelectionForRelatedExpression.relationBetweenSelectionAndAnaphoraPart;
        return selectionCoversAnaphoraPart(relation);
    }

    protected static boolean selectionCoversAnaphoraPart(final Relation relation) {
        return relation != Relation.SelectionHasNonZeroLength
                && relation != Relation.OutsideAnaphoraPart;
    }

    protected static SelectionRelativeToAnaphoraPart create(final int selectionOffset,
            final int selectionLength, final Position position,
            final int distanceToAnaphoraPartOffset) {
        return create(selectionOffset, selectionLength, position.offset, position.length,
                distanceToAnaphoraPartOffset);
    }

    protected static SelectionRelativeToAnaphoraPart create(final int selectionOffset,
            final int selectionLength, final int anaphoraPartOffset, final int anaphoraPartLength,
            final int distanceToAnaphoraPartOffset) {
        if (selectionLength != 0) {
            // This case is not handled yet because it required further cases to be
            // distinguished, e.g. a selection overlapping with an anaphora part and its
            // surrounding.
            return new SelectionRelativeToAnaphoraPart(
                    SelectionRelativeToAnaphoraPart.Relation.SelectionHasNonZeroLength,
                    distanceToAnaphoraPartOffset);
        }
        final SelectionRelativeToAnaphoraPart.Relation relation = getRelation(selectionOffset,
                anaphoraPartOffset, anaphoraPartLength);
        return new SelectionRelativeToAnaphoraPart(relation, distanceToAnaphoraPartOffset);
    }

    public static SelectionRelativeToAnaphoraPart.Relation getRelation(final int selectionOffset,
            final int anaphoraPartOffset, final int anaphoraPartLength) {
        final int endOfAnaphoraPart = anaphoraPartOffset + anaphoraPartLength;
        if (selectionOffset < anaphoraPartOffset) {
            return Relation.OutsideAnaphoraPart;
        } else if (selectionOffset == anaphoraPartOffset) {
            return Relation.AtBeginningOfAnaphoraPart;
        } else if (selectionOffset > anaphoraPartOffset
                && selectionOffset < endOfAnaphoraPart) {
            return Relation.InsideAnaphoraPart;
        } else if (selectionOffset == endOfAnaphoraPart) {
            return Relation.AtEndOfAnaphoraPart;
        } else {
            return Relation.OutsideAnaphoraPart;
        }
    }

}