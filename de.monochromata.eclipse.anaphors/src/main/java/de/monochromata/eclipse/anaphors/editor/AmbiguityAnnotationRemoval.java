package de.monochromata.eclipse.anaphors.editor;

import static java.util.stream.Collectors.toList;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitDocumentProvider;
import org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitDocumentProvider.ProblemAnnotation;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.text.source.IAnnotationModelExtension2;

import de.monochromata.eclipse.anaphors.ProblemIds;
import de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPosition;

public interface AmbiguityAnnotationRemoval {

    static List<Pair<Annotation, Position>> getAnnotationsToRemove(final IAnnotationModel annotationModel,
            final MemberTrackingPosition trackingPosition) {
        final Iterator<Annotation> iter = getAnnotationsOverlappingPosition(annotationModel, trackingPosition);
        final Stream.Builder<Pair<Annotation, Position>> annotationsToRemove = Stream.builder();
        while (iter.hasNext()) {
            final Annotation annotation = iter.next();
            if (AmbiguityAnnotationRemoval.isAnaphorRelated(annotation)) {
                annotationsToRemove.add(new ImmutablePair<>(annotation, annotationModel.getPosition(annotation)));
            }
        }
        return annotationsToRemove.build().collect(toList());
    }

    static Iterator<Annotation> getAnnotationsOverlappingPosition(final IAnnotationModel annotationModel,
            final MemberTrackingPosition trackingPosition) {
        return ((IAnnotationModelExtension2) annotationModel).getAnnotationIterator(trackingPosition.getOffset(),
                trackingPosition.getLength(), /* canStartBefore */ true, /* canEndAfter */ true);
    }

    @SuppressWarnings("restriction")
    static boolean isAnaphorRelated(final Annotation annotation) {
        if (!(annotation instanceof CompilationUnitDocumentProvider.ProblemAnnotation)) {
            return false;
        }
        final ProblemAnnotation problemAnnotation = (CompilationUnitDocumentProvider.ProblemAnnotation) annotation;
        final int problemId = problemAnnotation.getId();
        System.err.println("problemId=" + problemId + " -> " + (problemId & ProblemIds.AnaphorRelated) + " == "
                + ProblemIds.AnaphorRelated);
        return (problemId & ProblemIds.AnaphorRelated) == ProblemIds.AnaphorRelated;
    }

}
