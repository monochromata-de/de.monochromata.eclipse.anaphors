package de.monochromata.eclipse.anaphors.editor.update;

import static de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPositionCreation.createAndAddMemberTrackingPosition;
import static de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPositionCreation.createAndAddMemberTrackingPositions;
import static de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPositionRemoval.removeAllMemberTrackingPositions;
import static de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPositionRemoval.removeMemberTrackingPosition;
import static de.monochromata.eclipse.anaphors.editor.update.job.ResolveAnaphorsJobCreation.createAndScheduleJobIfThereAreMembersToBeReResolved;
import static de.monochromata.eclipse.anaphors.editor.update.marking.AnaphorMarking.collectTrackingPositions;
import static de.monochromata.eclipse.anaphors.editor.update.marking.ProblemMarking.collectProblemMembersToReResolve;
import static de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent.NotCreatedBecauseAnotherJobIsAlreadyRunning;
import static de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent.TriggeredByElementChange;
import static de.monochromata.eclipse.anaphors.swt.SWTAccess.runSync;
import static de.monochromata.eclipse.anaphors.swt.SWTAccess.supplySync;
import static java.util.Arrays.stream;
import static java.util.Optional.empty;
import static org.eclipse.jdt.core.ElementChangedEvent.POST_RECONCILE;
import static org.eclipse.jdt.core.IJavaElementDelta.ADDED;
import static org.eclipse.jdt.core.IJavaElementDelta.REMOVED;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.ElementChangedEvent;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IElementChangedListener;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaElementDelta;
import org.eclipse.jdt.core.IMember;
import org.eclipse.jdt.core.compiler.IProblem;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.refactoring.descriptors.ElaborateAnaphorDescriptor;
import org.eclipse.jdt.internal.corext.refactoring.code.ResolveAnaphorsRefactoring;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.viewers.ISelection;

import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.editor.update.job.ResolveAnaphorsJob;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionPerspectivationEvent;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionProgressEvent;
import de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;

/**
 * A listener that triggers anaphor resolution on
 * {@link ElementChangedEvent#POST_RECONCILE} events based on {@link IProblem}s.
 * <p>
 * This rather global listener is used instead of one on e.g. the annotation
 * model, because the annotation model does not contain {@link IProblem}s
 * accessible via the API. The {@link IProblems} are required because they
 * contain source range information.
 */
public class AnaphoraResolvingElementListener implements IElementChangedListener {

	protected ElaborateAnaphorDescriptor refactoringDescriptor = new ElaborateAnaphorDescriptor();

	protected final Consumer<Job> anaphorResolutionQueue;
	protected final Supplier<Boolean> anaphorResolutionQueueHasJobs;
	protected final CognitiveEditor editor;
	protected final AnaphoraReresolvingDocumentListener anaphoraReresolvingListener;
	protected final BiConsumer<AnaphorResolutionPerspectivationEvent, List<? extends Position>> anaphorResolutionPerspectivationListener;
	protected final BiConsumer<AnaphorResolutionProgressEvent, List<? extends AbstractAnaphoraPositionForRepresentation>> anaphorResolutionProgressListener;
	protected final BiConsumer<AnaphorResolutionEvent.EventType, AnaphorResolutionEvent> anaphorResolutionListener;
	protected final BiConsumer<ResolveAnaphorsJobEvent, Pair<Optional<ResolveAnaphorsJob>, Optional<Exception>>> resolveAnaphorsJobListener;
	protected final Supplier<Boolean> editorHasInput;
	protected final Supplier<ICompilationUnit> editorCompilationUnit;
	protected final Supplier<IDocument> getOriginDocument;
	protected final BiConsumer<CompilationUnit, ICompilationUnit> createAndScheduleJob;
	protected final Supplier<IAnnotationModel> annotationModelSupplier;
	protected final Supplier<ISelection> originSelectionSupplier;
	protected final Consumer<ISelection> originSelectionSetter;
	protected final List<MemberTrackingPosition> memberTrackingPositionsToReResolve = new ArrayList<>();
	protected ICompilationUnit currentlyTrackedCompilationUnit;

	public AnaphoraResolvingElementListener(final Consumer<Job> anaphorResolutionQueue,
			final Supplier<Boolean> anaphorResolutionQueueHasJobs, final CognitiveEditor editor,
			final AnaphoraReresolvingDocumentListener anaphoraReresolvingListener,
			final BiConsumer<AnaphorResolutionPerspectivationEvent, List<? extends Position>> anaphorResolutionPerspectivationListener,
			final BiConsumer<AnaphorResolutionProgressEvent, List<? extends AbstractAnaphoraPositionForRepresentation>> anaphorResolutionProgressListener,
			final BiConsumer<AnaphorResolutionEvent.EventType, AnaphorResolutionEvent> anaphorResolutionListener,
			final BiConsumer<ResolveAnaphorsJobEvent, Pair<Optional<ResolveAnaphorsJob>, Optional<Exception>>> resolveAnaphorsJobListener) {
		this(anaphorResolutionQueue, anaphorResolutionQueueHasJobs, editor, anaphoraReresolvingListener,
				anaphorResolutionPerspectivationListener, anaphorResolutionProgressListener, anaphorResolutionListener,
				resolveAnaphorsJobListener, null, null, null, null);
	}

	public AnaphoraResolvingElementListener(final Consumer<Job> anaphorResolutionQueue,
			final Supplier<Boolean> anaphorResolutionQueueHasJobs, final CognitiveEditor editor,
			final AnaphoraReresolvingDocumentListener anaphoraReresolvingListener,
			final BiConsumer<AnaphorResolutionPerspectivationEvent, List<? extends Position>> anaphorResolutionPerspectivationListener,
			final BiConsumer<AnaphorResolutionProgressEvent, List<? extends AbstractAnaphoraPositionForRepresentation>> anaphorResolutionProgressListener,
			final BiConsumer<AnaphorResolutionEvent.EventType, AnaphorResolutionEvent> anaphorResolutionListener,
			final BiConsumer<ResolveAnaphorsJobEvent, Pair<Optional<ResolveAnaphorsJob>, Optional<Exception>>> resolveAnaphorsJobListener,
			final Supplier<Boolean> editorHasInput, final Supplier<ICompilationUnit> editorCompilationUnit,
			final Supplier<IDocument> getOriginDocument,
			final BiConsumer<CompilationUnit, ICompilationUnit> createAndScheduleJob) {
		this.anaphorResolutionQueue = anaphorResolutionQueue;
		this.anaphorResolutionQueueHasJobs = anaphorResolutionQueueHasJobs;
		this.editor = editor;
		this.anaphoraReresolvingListener = anaphoraReresolvingListener;
		this.anaphorResolutionPerspectivationListener = anaphorResolutionPerspectivationListener;
		this.anaphorResolutionProgressListener = anaphorResolutionProgressListener;
		this.anaphorResolutionListener = anaphorResolutionListener;
		this.resolveAnaphorsJobListener = resolveAnaphorsJobListener;
		this.getOriginDocument = getOriginDocument == null ? this::getOriginDocument : getOriginDocument;
		this.editorHasInput = editorHasInput == null ? this::editorHasInput : editorHasInput;
		this.editorCompilationUnit = editorCompilationUnit == null ? this::getEditorCompilationUnit
				: editorCompilationUnit;
		this.createAndScheduleJob = createAndScheduleJob == null ? this::createAndScheduleJob : createAndScheduleJob;
		annotationModelSupplier = () -> editor.getDocumentProvider().getAnnotationModel(editor.getEditorInput());
		originSelectionSupplier = () -> supplySync(editor.getViewer().getTextWidget(),
				() -> editor.getSelectionProvider().getSelection());
		originSelectionSetter = newSelection -> runSync(editor.getViewer().getTextWidget(),
				() -> editor.getSelectionProvider().setSelection(newSelection));
	}

	public void markAnaphorsForReResolution(final Pair<Integer, Integer> offsetAndLength,
			final IDocument originDocument) {
		collectTrackingPositions(offsetAndLength, originDocument, memberTrackingPositionsToReResolve);
	}

	/**
	 * Performs the {@link ResolveAnaphorsRefactoring}, if a number of known
	 * triggering problems are found.
	 *
	 * TODO: Elaborate when and how the refactoring is applied
	 *
	 * @param event the last change of the element
	 * @see ElaborateAnaphorDescriptor
	 */
	@Override
	public void elementChanged(final ElementChangedEvent event) {
		if (!editorHasInput.get()) {
			return;
		}
		final IJavaElementDelta delta = event.getDelta();
		maybeUpdateTrackedMembers(delta);
		if (event.getType() == POST_RECONCILE) {
			maybeScheduleReResolution(delta);
		}
	}

	protected void maybeUpdateTrackedMembers(final IJavaElementDelta delta) {
		final ICompilationUnit compilationUnitInEditor = editorCompilationUnit.get();
		if (affectedByDeltaOrChildDelta(compilationUnitInEditor, delta)) {
			updateTrackedMembers(compilationUnitInEditor, delta);
		}
	}

	protected boolean affectedByDeltaOrChildDelta(final ICompilationUnit compilationUnitInEditor,
			final IJavaElementDelta delta) {
		if (compilationUnitInEditor.equals(delta.getElement())) {
			return true;
		}
		return affectedByDeltasOfChildDeltas(compilationUnitInEditor, delta.getAffectedChildren());
	}

	protected boolean affectedByDeltasOfChildDeltas(final ICompilationUnit compilationUnitInEditor,
			final IJavaElementDelta[] deltas) {
		for (final IJavaElementDelta delta : deltas) {
			if (affectedByDeltaOrChildDelta(compilationUnitInEditor, delta)) {
				return true;
			}
		}
		return false;
	}

	protected void updateTrackedMembers(final IJavaElement compilationUnitElement, final IJavaElementDelta delta) {
		final IDocument originDocument = getOriginDocument.get();
		if (originDocument == null) {
			// Cannot add positions to a non-existant document
			return;
		}
		final boolean membersWereChanged = trackChangedMembers(delta, originDocument);
		if (!membersWereChanged) {
			maybeInitializeMemberTracking(originDocument, (ICompilationUnit) compilationUnitElement);
		}
	}

	protected void maybeInitializeMemberTracking(final IDocument originDocument,
			final ICompilationUnit compilationUnitElement) {
		if (!compilationUnitElement.equals(currentlyTrackedCompilationUnit)) {
			removeAllMemberTrackingPositions(originDocument);
			createAndAddMemberTrackingPositions(originDocument, compilationUnitElement);
			currentlyTrackedCompilationUnit = compilationUnitElement;
		}
	}

	protected boolean trackChangedMembers(final IJavaElementDelta delta, final IDocument originDocument) {
		if (delta.getElement() instanceof IMember) {
			final boolean memberWasChanged = trackMemberChange(delta, (IMember) delta.getElement(), originDocument);
			if (memberWasChanged) {
				return true;
			}
		}
		return trackChangedMembers(delta.getAffectedChildren(), originDocument);
	}

	protected boolean trackChangedMembers(final IJavaElementDelta[] affectedChildren, final IDocument originDocument) {
		return stream(affectedChildren).map(child -> trackChangedMembers(child, originDocument))
				.reduce((changed0, changed1) -> changed0 | changed1).orElse(false);
	}

	protected boolean trackMemberChange(final IJavaElementDelta delta, final IMember member,
			final IDocument originDocument) {
		if (delta.getKind() == ADDED) {
			trackAddedMember(member, originDocument);
			return true;
		} else if (delta.getKind() == REMOVED) {
			trackRemovedMember(member, originDocument);
			return true;
		}
		return false;
	}

	protected void trackAddedMember(final IMember member, final IDocument originDocument) {
		createAndAddMemberTrackingPosition(member, originDocument, position -> {
			synchronized (memberTrackingPositionsToReResolve) {
				memberTrackingPositionsToReResolve.add(position);
			}
		});
	}

	protected void trackRemovedMember(final IMember member, final IDocument originDocument) {
		removeMemberTrackingPosition(member, originDocument);
		removeMemberFromQueue(member);
	}

	protected void removeMemberFromQueue(final IMember member) {
		synchronized (memberTrackingPositionsToReResolve) {
			final Optional<MemberTrackingPosition> positionToRemove = memberTrackingPositionsToReResolve.stream()
					.filter(position -> position.member.equals(member)).findFirst();
			positionToRemove.ifPresent(memberTrackingPositionsToReResolve::remove);
		}
	}

	protected boolean editorHasInput() {
		return editor.getEditorInput() != null;
	}

	protected ICompilationUnit getEditorCompilationUnit() {
		return (ICompilationUnit) JavaUI.getEditorInputTypeRoot(editor.getEditorInput());
	}

	protected IDocument getOriginDocument() {
		return editor.getDocumentProvider().getDocument(editor.getEditorInput());
	}

	protected void maybeScheduleReResolution(final IJavaElementDelta delta) {
		final CompilationUnit compilationUnitAST = delta.getCompilationUnitAST();
		if (compilationUnitAST == null) {
			return;
		}
		final IJavaElement compilationUnitElement = compilationUnitAST.getJavaElement();
		if (compilationUnitElement == null) {
			return;
		}
		final ICompilationUnit compilationUnitInEditor = editorCompilationUnit.get();
		if (compilationUnitElement.equals(compilationUnitInEditor)) {
			createAndScheduleJob.accept(compilationUnitAST, (ICompilationUnit) compilationUnitElement);
		}
	}

	protected void createAndScheduleJob(final CompilationUnit compilationUnitAST,
			final ICompilationUnit compilationUnitElement) {
		final IDocument originDocument = getOriginDocument.get();
		final IProblem[] problems = compilationUnitAST.getProblems();

		// Members are marked instead of the anaphor positions, because every previous
		// refactoring might have created new anaphor positions that would not have been
		// collected. Instead, anaphor positions are obtained from the member when a
		// refactoring is to be created.
		collectTrackingPositions(compilationUnitElement, originDocument, problems, memberTrackingPositionsToReResolve);
		final Map<MemberTrackingPosition, List<IProblem>> problemsByTrackingPosition = collectProblemMembersToReResolve(
				originDocument, problems, compilationUnitElement);

		if (anaphorResolutionQueueHasJobs.get()) {
			// Do not schedule another job while one is currently running
			resolveAnaphorsJobListener.accept(NotCreatedBecauseAnotherJobIsAlreadyRunning,
					new ImmutablePair<>(empty(), empty()));
			return;
		}

		resolveAnaphorsJobListener.accept(TriggeredByElementChange, new ImmutablePair<>(empty(), empty()));
		createAndScheduleJobIfThereAreMembersToBeReResolved(editor.getViewer().getTextWidget().getDisplay(),
				compilationUnitAST, originDocument, memberTrackingPositionsToReResolve, problemsByTrackingPosition,
				annotationModelSupplier, originSelectionSupplier, originSelectionSetter, anaphoraReresolvingListener,
				anaphorResolutionListener, anaphorResolutionPerspectivationListener, anaphorResolutionProgressListener,
				resolveAnaphorsJobListener, anaphorResolutionQueue, anaphorResolutionQueueHasJobs);
	}

}
