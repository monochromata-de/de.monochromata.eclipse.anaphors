package de.monochromata.eclipse.anaphors.position;

import org.eclipse.jdt.core.dom.ASTNode;

public class AnaphoraPositionBeforeStartOfASTNode extends AbstractAnaphoraPositionAroundASTNode {

	public AnaphoraPositionBeforeStartOfASTNode(final NodeType nodeType, final ASTNode astNode, final int offset,
			final int length) {
		super(nodeType, astNode, offset, length);
	}

	public AnaphoraPositionBeforeStartOfASTNode(final NodeType nodeType, final ASTNode astNode, final int offset) {
		super(nodeType, astNode, offset);
	}

	public AnaphoraPositionBeforeStartOfASTNode(final NodeType nodeType, final ASTNode astNode) {
		super(nodeType, astNode);
	}

	@Override
	public String toString() {
		return "AnaphoraPositionBeforeStartOfASTNode [offset=" + offset + ", length=" + length + ", isDeleted="
				+ isDeleted + ", astNode=" + astNode + ", nodeType=" + nodeType + "]";
	}

}
