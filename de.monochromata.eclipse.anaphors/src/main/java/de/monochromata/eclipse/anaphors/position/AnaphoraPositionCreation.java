package de.monochromata.eclipse.anaphors.position;

import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static de.monochromata.eclipse.position.DoublyLinkedPosition.link;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapPositionExceptions;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPositionCategoryException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.statushandlers.StatusManager;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.Activator;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionAroundASTNode.NodeType;
import de.monochromata.eclipse.persp.PerspectivationPosition;
import de.monochromata.eclipse.position.PositionExceptionWrapping.BadPositionCategoryThrowingConsumer;

public interface AnaphoraPositionCreation {

	// Creating AbstractAnaphoraPositionForRepresentation instances

	static PositionForRelatedExpression addPositionForRelatedExpression(final IDocument document,
			final PerspectivationPosition perspectivationPosition) {
		final PositionForRelatedExpression position = createPositionForRelatedExpression(perspectivationPosition);
		wrapPositionExceptions(() -> addPosition(document, position));
		return position;
	}

	static PositionForRelatedExpression createPositionForRelatedExpression(
			final PerspectivationPosition perspectivationPosition) {
		return new PositionForRelatedExpression(perspectivationPosition.offset, perspectivationPosition.length,
				perspectivationPosition);
	}

	static PositionForAnaphor createAndLinkAndAddPositionForAnaphor(final IDocument document,
			final PositionForRelatedExpression positionForRelatedExpression, final Anaphora anaphora,
			final PerspectivationPosition perspectivationPosition) {
		final PositionForAnaphor positionForAnaphor = createAndLinkPositionForAnaphor(positionForRelatedExpression,
				anaphora, perspectivationPosition);
		wrapPositionExceptions(() -> addPosition(document, positionForAnaphor));
		return positionForAnaphor;
	}

	static PositionForAnaphor createAndLinkPositionForAnaphor(
			final PositionForRelatedExpression positionForRelatedExpression, final Anaphora anaphora,
			final PerspectivationPosition perspectivationPosition) {
		final PositionForAnaphor positionForAnaphor = createPositionForAnaphor(anaphora, perspectivationPosition);
		link(positionForRelatedExpression, positionForAnaphor);
		return positionForAnaphor;
	}

	static PositionForAnaphor addPositionForAnaphor(final IDocument document, final Anaphora anaphora,
			final PerspectivationPosition perspectivationPosition) {
		final PositionForAnaphor position = createPositionForAnaphor(anaphora, perspectivationPosition);
		wrapPositionExceptions(() -> addPosition(document, position));
		return position;
	}

	static PositionForAnaphor createPositionForAnaphor(final Anaphora anaphora,
			final PerspectivationPosition perspectivationPosition) {
		return new PositionForAnaphor(perspectivationPosition.offset, perspectivationPosition.length,
				perspectivationPosition, anaphora);
	}

	// Creating AbstractAnaphoraPositionOnASTNode instances

	static <T extends AbstractAnaphoraPositionOnASTNode> List<T> addPositionsForAstNodes(final IDocument document,
			final PublicAnaphora anaphora) {
		return createAnaphoraPositions(positions -> addPositionsForAstNodes(document, anaphora, positions));
	}

	static <T extends AbstractAnaphoraPositionOnASTNode> void addPositionsForAstNodes(final IDocument document,
			final PublicAnaphora anaphora, final Builder<T> positions)
			throws BadLocationException, BadPositionCategoryException {
		final ASTNode relatedExpressionNode = anaphora.getRelatedExpression().getRelatedExpression();
		final Expression anaphorExpression = anaphora.getAnaphorExpression();
		final AnaphoraPositionOnRelatedExpression relatedExpressionPosition = new AnaphoraPositionOnRelatedExpression(
				relatedExpressionNode, relatedExpressionNode.getStartPosition(), relatedExpressionNode.getLength());
		addPosition(document, positions, (T) relatedExpressionPosition);
		final AnaphoraPositionOnAnaphor anaphorPosition = new AnaphoraPositionOnAnaphor(anaphorExpression,
				anaphorExpression.getStartPosition(), anaphorExpression.getLength(), relatedExpressionPosition);
		addPosition(document, positions, (T) anaphorPosition);
		link(relatedExpressionPosition, anaphorPosition);
	}

	// Creating AbstractAnaphoraPositionAroundASTNode instances

	@Deprecated
	static <T extends AbstractAnaphoraPositionAroundASTNode> List<T> addPositionsAroundAstNodes(
			final IDocument document, final PublicAnaphora anaphora) {
		return createAnaphoraPositions(positions -> addPositionsAroundAstNodes(document, anaphora, positions));
	}

	static <T extends AbstractAnaphoraPositionAroundASTNode> void addPositionsAroundAstNodes(final IDocument document,
			final PublicAnaphora anaphora, final Builder<T> positions)
			throws BadLocationException, BadPositionCategoryException {
		final AnaphoraPositionBeforeStartOfASTNode positionBeforeStartOfRelatedExpression = (AnaphoraPositionBeforeStartOfASTNode) addPositionBeforeStartOf(
				document, positions, NodeType.RelatedExpression,
				anaphora.getRelatedExpression().getRelatedExpression());
		addPositionAfterEndOf(document, positions, NodeType.RelatedExpression,
				anaphora.getRelatedExpression().getRelatedExpression(), positionBeforeStartOfRelatedExpression);
		final AnaphoraPositionBeforeStartOfASTNode positionBeforeStartOfAnaphor = (AnaphoraPositionBeforeStartOfASTNode) addPositionBeforeStartOf(
				document, positions, NodeType.Anaphor, anaphora.getAnaphorExpression());
		addPositionAfterEndOf(document, positions, NodeType.Anaphor, anaphora.getAnaphorExpression(),
				positionBeforeStartOfAnaphor);
	}

	static <T extends AbstractAnaphoraPositionAroundASTNode> T addPositionBeforeStartOf(final IDocument document,
			final Builder<T> positions, final NodeType nodeType, final ASTNode node)
			throws BadLocationException, BadPositionCategoryException {
		final AnaphoraPositionBeforeStartOfASTNode position = new AnaphoraPositionBeforeStartOfASTNode(nodeType, node,
				node.getStartPosition(), 0);
		return addPosition(document, positions, (T) position);
	}

	static <T extends AbstractAnaphoraPositionAroundASTNode> void addPositionAfterEndOf(final IDocument document,
			final Builder<T> positions, final NodeType nodeType, final ASTNode node,
			final AnaphoraPositionBeforeStartOfASTNode positionBeforeStart)
			throws BadLocationException, BadPositionCategoryException {
		final int positionAfterNode = node.getStartPosition() + node.getLength();
		final int startPosition = Math.min(document.getLength(), positionAfterNode);
		final AnaphoraPositionAfterEndOfASTNode position = new AnaphoraPositionAfterEndOfASTNode(nodeType, node,
				positionBeforeStart, startPosition, 0);
		addPosition(document, positions, (T) position);
	}

	static <T extends AbstractAnaphoraPosition> T addPosition(final IDocument document, final Builder<T> positions,
			final T position) throws BadLocationException, BadPositionCategoryException {
		addPosition(document, position);
		positions.add(position);
		return position;
	}

	static <T extends AbstractAnaphoraPosition> T addPosition(final IDocument document, final T position)
			throws BadLocationException, BadPositionCategoryException {
		document.addPosition(ANAPHORA_CATEGORY, position);
		return position;
	}

	static <T> List<T> createAnaphoraPositions(
			final BadPositionCategoryThrowingConsumer<Stream.Builder<T>, BadLocationException> factoryMethod) {
		final Stream.Builder<T> positions = Stream.builder();
		try {
			factoryMethod.accept(positions);
			return positions.build().collect(toList());
		} catch (final BadLocationException e) {
			// TODO: "Handle" the exception like that or propagate it?
			StatusManager.getManager()
					.handle(new Status(IStatus.ERROR, "Failed to create anaphora positions", Activator.PLUGIN_ID, e));
			return positions.build().collect(toList());
		} catch (final BadPositionCategoryException e) {
			throw new IllegalStateException(e);
		}
	}

}
