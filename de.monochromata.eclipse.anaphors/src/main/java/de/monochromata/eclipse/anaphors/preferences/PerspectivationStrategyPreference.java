package de.monochromata.eclipse.anaphors.preferences;

import static de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy.underspecifyEverything;
import static de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy.underspecifyNothing;
import static de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy.underspecifyRelatedExpressions;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.PERSPECTIVATION_STRATEGY_DEFAULT;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.PERSPECTIVATION_STRATEGY_EVERYTHING;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.PERSPECTIVATION_STRATEGY_NOTHING;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.PERSPECTIVATION_STRATEGY_RELATED_EXPRESSIONS;

import de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy;
import de.monochromata.eclipse.anaphors.Util;

public interface PerspectivationStrategyPreference {

    static PerspectivationStrategy getByName(final String strategyName) {
        switch (strategyName) {
        case PERSPECTIVATION_STRATEGY_NOTHING:
            return underspecifyNothing();
        case PERSPECTIVATION_STRATEGY_RELATED_EXPRESSIONS:
            return underspecifyRelatedExpressions();
        case PERSPECTIVATION_STRATEGY_EVERYTHING:
            return underspecifyEverything();
        default:
            Util.logError("Unknown perspectivation strategy " + strategyName + ". Using default strategy instead.");
            return underspecifyRelatedExpressions();
        }
    }

    static String getName(final PerspectivationStrategy perspectivationStrategy) {
        if (perspectivationStrategy == underspecifyNothing()) {
            return PERSPECTIVATION_STRATEGY_NOTHING;
        } else if (perspectivationStrategy == underspecifyRelatedExpressions()) {
            return PERSPECTIVATION_STRATEGY_RELATED_EXPRESSIONS;
        } else if (perspectivationStrategy == underspecifyEverything()) {
            return PERSPECTIVATION_STRATEGY_EVERYTHING;
        } else {
            Util.logError("Unknown perspectivation strategy " + perspectivationStrategy + ". Returning default name.");
            return PERSPECTIVATION_STRATEGY_DEFAULT;
        }
    }

}
