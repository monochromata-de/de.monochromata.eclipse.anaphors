package de.monochromata.eclipse.anaphors.position;

import org.eclipse.jdt.core.dom.ASTNode;

/**
 * Used temporarily to track the source location of a known {@link ASTNode}
 * while changes are applied before or after the {@link ASTNode}.
 * <p>
 * Because this position refers to an {@link ASTNode}, it should only be used up
 * until before unknown AST changes can happen and should never be persisted.
 * <p>
 * If the source code of the AST nodes is (partly) replaced, these positions
 * need to be removed and anaphora resolution needs to be performed again.
 */
public abstract class AbstractAnaphoraPositionOnASTNode extends AbstractAnaphoraPositionForTransformation {

	public AbstractAnaphoraPositionOnASTNode(final ASTNode astNode, final int offset, final int length) {
		super(astNode, offset, length);
	}

	public AbstractAnaphoraPositionOnASTNode(final ASTNode astNode, final int offset) {
		super(astNode, offset);
	}

	public AbstractAnaphoraPositionOnASTNode(final ASTNode astNode) {
		super(astNode);
	}

}
