package de.monochromata.eclipse.anaphors.editor.update;

import static de.monochromata.eclipse.anaphors.editor.update.MemberCollection.getAllMembers;
import static de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPosition.MEMBER_TRACKING_CATEGORY;
import static de.monochromata.eclipse.anaphors.editor.update.marking.WithSourceRange.withSourceRange;

import java.util.Optional;
import java.util.function.Consumer;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IMember;
import org.eclipse.jface.text.IDocument;

import de.monochromata.eclipse.anaphors.Util;

public interface MemberTrackingPositionCreation {

    static void createAndAddMemberTrackingPositions(final IDocument originDocument,
            final ICompilationUnit compilationUnit) {
        getAllMembers(compilationUnit).forEach(createAndAddMemberTrackingPosition(originDocument));
    }

    static Consumer<IMember> createAndAddMemberTrackingPosition(
            final IDocument originDocument) {
        return member -> createAndAddMemberTrackingPosition(member, originDocument, unused -> {
        });
    }

    static void createAndAddMemberTrackingPosition(final IMember member,
            final IDocument originDocument, final Consumer<MemberTrackingPosition> positionConsumer) {
        createMemberTrackingPosition(member).ifPresent(position -> {
            addMemberTrackingPosition(originDocument, position);
            positionConsumer.accept(position);
        });
    }

    static void addMemberTrackingPosition(final IDocument originDocument, final MemberTrackingPosition position) {
        try {
            originDocument.addPosition(MEMBER_TRACKING_CATEGORY, position);
        } catch (final Exception e) {
            Util.logException("Failed to add member tracking position", e);
        }
    }

    static Optional<MemberTrackingPosition> createMemberTrackingPosition(final IMember member) {
        return withSourceRange(member, sourceRange -> new MemberTrackingPosition(sourceRange.getOffset(),
                sourceRange.getLength(), member));
    }

}
