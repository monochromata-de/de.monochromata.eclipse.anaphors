package de.monochromata.eclipse.anaphors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.statushandlers.StatusManager;

public class Util {

    public static <T> List<T> getPluginObjects(List<T> list, final String extensionPointId) {
        if (list == null) {
            list = loadPluginObjects(extensionPointId);
        }
        return list;
    }

    private static <T> List<T> loadPluginObjects(final String extensionPointId) {
        final List<T> adapters = new ArrayList<>();
        final IExtensionRegistry registry = Platform.getExtensionRegistry();
        final IExtensionPoint extensionPoint = registry.getExtensionPoint(extensionPointId);
        final IExtension[] extensions = extensionPoint.getExtensions();
        for (final IExtension extension : extensions) {
            loadPluginObject(adapters, extension);
        }
        return adapters;
    }

    private static <T> void loadPluginObject(final List<T> adapters, final IExtension extension) {
        final IConfigurationElement[] elements = extension.getConfigurationElements();
        for (final IConfigurationElement element : elements) {
            try {
                // Provoke a ClassCastException because we cannot access the
                // type T at runtime
                // to ensure that the returned object is an instance of T
                @SuppressWarnings("unchecked")
                final T extInstance = (T) element.createExecutableExtension("class");
                adapters.add(extInstance);
            } catch (final Exception e) {
                logAndShowException("Error while loading plugin objects", e);
            }
        }
    }

    public static void logInfo(final String message) {
        StatusManager.getManager().handle(new Status(IStatus.INFO, Activator.PLUGIN_ID, message), StatusManager.LOG);
    }

    public static void logAndShowInfo(final String message) {
        StatusManager.getManager().handle(new Status(IStatus.INFO, Activator.PLUGIN_ID, message),
                StatusManager.LOG | StatusManager.SHOW);
    }

    public static void logAndShowBlockingInfo(final String message) {
        StatusManager.getManager().handle(new Status(IStatus.INFO, Activator.PLUGIN_ID, message),
                StatusManager.LOG | StatusManager.SHOW | StatusManager.BLOCK);
    }

    public static void logError(final String message) {
        StatusManager.getManager().handle(new Status(IStatus.ERROR, Activator.PLUGIN_ID, message), StatusManager.LOG);
    }

    public static void logAndShowError(final String message) {
        StatusManager.getManager().handle(new Status(IStatus.ERROR, Activator.PLUGIN_ID, message),
                StatusManager.LOG | StatusManager.SHOW);
    }

    public static void logException(final String message, final Exception e) {
        StatusManager.getManager().handle(new Status(IStatus.ERROR, Activator.PLUGIN_ID, message, e),
                StatusManager.LOG);
    }

    public static void logAndShowException(final String message, final Exception e) {
        StatusManager.getManager().handle(new Status(IStatus.ERROR, Activator.PLUGIN_ID, message, e),
                StatusManager.LOG | StatusManager.SHOW);
    }
}
