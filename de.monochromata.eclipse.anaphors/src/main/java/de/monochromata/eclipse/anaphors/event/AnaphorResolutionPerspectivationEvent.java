package de.monochromata.eclipse.anaphors.event;

import java.util.List;

import de.monochromata.eclipse.persp.PerspectivationPosition;
import de.monochromata.event.ConsumerNotifier;

/**
 * An {@link AnaphorResolutionPerspectivationEvent} is used with a
 * {@link ConsumerNotifier} and comes with a {@link List} of
 * {@link PerspectivationPosition}s.
 */
public enum AnaphorResolutionPerspectivationEvent {
    /**
     * Sent after a single referent of a definite expression has been selected and
     * when the position of the related expression and the anaphor are known and
     * before the source code is modified in order to resolve the anaphor.
     */
    AboutToStartDocumentRewritingForAnaphorResolution,

    /**
     * Sent when source code modification is complete and the anaphor is resolved.
     */
    CompletedDocumentRewritingForAnaphorResolution
}