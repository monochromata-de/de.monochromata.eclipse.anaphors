package de.monochromata.eclipse.anaphors.editor.update.job;

import static de.monochromata.eclipse.anaphors.editor.AmbiguityAnnotationRemoval.getAnnotationsToRemove;
import static de.monochromata.eclipse.anaphors.editor.update.AnaphorPositionCollection.getAnaphorPositions;
import static de.monochromata.eclipse.anaphors.editor.update.TriggeringPositionNormalization.ignoreProblemPositionsThatOverlapWithPositionsFromMember;
import static de.monochromata.eclipse.anaphors.editor.update.marking.ProblemMarking.createProblemPositions;
import static java.util.Collections.singletonList;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.compiler.IProblem;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.internal.corext.refactoring.code.ResolveAnaphorsRefactoring;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Display;

import de.monochromata.eclipse.anaphors.editor.update.AnaphoraReresolvingDocumentListener;
import de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPosition;
import de.monochromata.eclipse.anaphors.editor.update.marking.ProblemMarking;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionPerspectivationEvent;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionProgressEvent;
import de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.anaphors.position.KnownProblemPosition;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.function.TriConsumer;

public interface ResolveAnaphorsJobCreation {

	static void createAndScheduleJobIfThereAreMembersToBeReResolved(final Display display,
			final CompilationUnit compilationUnitAST, final IDocument originDocument,
			final List<MemberTrackingPosition> memberTrackingPositionsToReResolve,
			final Map<MemberTrackingPosition, List<IProblem>> problemsByTrackingPosition,
			final Supplier<IAnnotationModel> annotationModelSupplier,
			final Supplier<ISelection> originSelectionSupplier, final Consumer<ISelection> originSelectionSetter,
			final AnaphoraReresolvingDocumentListener anaphoraReresolvingListener,
			final BiConsumer<AnaphorResolutionEvent.EventType, AnaphorResolutionEvent> anaphorResolutionListener,
			final BiConsumer<AnaphorResolutionPerspectivationEvent, List<? extends Position>> anaphorResolutionPerspectivationListener,
			final BiConsumer<AnaphorResolutionProgressEvent, List<? extends AbstractAnaphoraPositionForRepresentation>> anaphorResolutionProgressListener,
			final BiConsumer<ResolveAnaphorsJobEvent, Pair<Optional<ResolveAnaphorsJob>, Optional<Exception>>> resolveAnaphorsJobListener,
			final Consumer<Job> anaphorResolutionQueue, final Supplier<Boolean> anaphorResolutionQueueHasJobs) {
		final Function<MemberTrackingPosition, List<KnownProblemPosition>> problemPositionFactory = memberToReResolve -> createProblemPositions(
				memberToReResolve, problemsByTrackingPosition);
		createAndScheduleJobIfThereAreMembersToBeReResolved(display, compilationUnitAST, originDocument,
				memberTrackingPositionsToReResolve, problemPositionFactory, annotationModelSupplier,
				originSelectionSupplier, originSelectionSetter, anaphoraReresolvingListener, anaphorResolutionListener,
				anaphorResolutionPerspectivationListener, anaphorResolutionProgressListener, resolveAnaphorsJobListener,
				anaphorResolutionQueue, anaphorResolutionQueueHasJobs);
	}

	static void createAndScheduleJobIfThereAreMembersToBeReResolved(final Display display,
			final CompilationUnit compilationUnitAST, final IDocument originDocument,
			final List<MemberTrackingPosition> memberTrackingPositionsToReResolve,
			final Function<MemberTrackingPosition, List<KnownProblemPosition>> problemPositionFactory,
			final Supplier<IAnnotationModel> annotationModelSupplier,
			final Supplier<ISelection> originSelectionSupplier, final Consumer<ISelection> originSelectionSetter,
			final AnaphoraReresolvingDocumentListener anaphoraReresolvingListener,
			final BiConsumer<AnaphorResolutionEvent.EventType, AnaphorResolutionEvent> anaphorResolutionListener,
			final BiConsumer<AnaphorResolutionPerspectivationEvent, List<? extends Position>> anaphorResolutionPerspectivationListener,
			final BiConsumer<AnaphorResolutionProgressEvent, List<? extends AbstractAnaphoraPositionForRepresentation>> anaphorResolutionProgressListener,
			final BiConsumer<ResolveAnaphorsJobEvent, Pair<Optional<ResolveAnaphorsJob>, Optional<Exception>>> resolveAnaphorsJobListener,
			final Consumer<Job> anaphorResolutionQueue, final Supplier<Boolean> anaphorResolutionQueueHasJobs) {
		createAndScheduleJobIfThereAreMembersToBeReResolved(memberTrackingPositionsToReResolve,
				trackingPosition -> getAnaphorPositions(originDocument, singletonList(trackingPosition)),
				problemPositionFactory,
				(trackingPositionToReResolve, anaphorPositions, newProblems) -> createAndScheduleJob(display,
						compilationUnitAST, originDocument, trackingPositionToReResolve,
						memberTrackingPositionsToReResolve, anaphorPositions, newProblems, annotationModelSupplier,
						originSelectionSupplier, originSelectionSetter, anaphoraReresolvingListener,
						anaphorResolutionListener, anaphorResolutionPerspectivationListener,
						anaphorResolutionProgressListener, resolveAnaphorsJobListener, anaphorResolutionQueue,
						anaphorResolutionQueueHasJobs),
				sourceReference -> removeAmbiguityAnnotations(annotationModelSupplier.get(), sourceReference));
	}

	static void createAndScheduleJobIfThereAreMembersToBeReResolved(
			final List<MemberTrackingPosition> memberTrackingPositionsToReResolve,
			final Function<MemberTrackingPosition, List<PositionForAnaphor>> anaphorPositionFactory,
			final Function<MemberTrackingPosition, List<KnownProblemPosition>> problemPositionFactory,
			final TriConsumer<MemberTrackingPosition, List<PositionForAnaphor>, List<KnownProblemPosition>> jobFactoryAndScheduler,
			final Consumer<MemberTrackingPosition> ambiguityAnnotationRemover) {
		synchronized (memberTrackingPositionsToReResolve) {
			while (!memberTrackingPositionsToReResolve.isEmpty()) {
				final MemberTrackingPosition trackingPositionToReResolve = memberTrackingPositionsToReResolve.remove(0);
				final List<PositionForAnaphor> anaphorPositions = anaphorPositionFactory
						.apply(trackingPositionToReResolve);
				final List<KnownProblemPosition> allProblemPositions = problemPositionFactory
						.apply(trackingPositionToReResolve);
				final List<KnownProblemPosition> uniqueProblemPositions = ignoreProblemPositionsThatOverlapWithPositionsFromMember(
						anaphorPositions, allProblemPositions);

				if (thereArePositions(anaphorPositions, uniqueProblemPositions)) {
					jobFactoryAndScheduler.accept(trackingPositionToReResolve, anaphorPositions,
							uniqueProblemPositions);
					break;
				} else {
					// Remove ambiguity annotations for members without positions because no job
					// will be scheduled that would remove the annotations.
					ambiguityAnnotationRemover.accept(trackingPositionToReResolve);
				}
			}
		}
	}

	static boolean thereArePositions(final List<PositionForAnaphor> anaphorPositions,
			final List<KnownProblemPosition> newProblems) {
		return !anaphorPositions.isEmpty() || !newProblems.isEmpty();
	}

	static void createAndScheduleJob(final Display display, final CompilationUnit compilationUnitAST,
			final IDocument originDocument, final MemberTrackingPosition trackingPositionToReResolve,
			final List<MemberTrackingPosition> memberTrackingPositionsToReResolve,
			final List<PositionForAnaphor> anaphorPositions, final List<KnownProblemPosition> newProblems,
			final Supplier<IAnnotationModel> annotationModelSupplier,
			final Supplier<ISelection> originSelectionSupplier, final Consumer<ISelection> originSelectionSetter,
			final AnaphoraReresolvingDocumentListener anaphoraReresolvingListener,
			final BiConsumer<AnaphorResolutionEvent.EventType, AnaphorResolutionEvent> anaphorResolutionListener,
			final BiConsumer<AnaphorResolutionPerspectivationEvent, List<? extends Position>> anaphorResolutionPerspectivationListener,
			final BiConsumer<AnaphorResolutionProgressEvent, List<? extends AbstractAnaphoraPositionForRepresentation>> anaphorResolutionProgressListener,
			final BiConsumer<ResolveAnaphorsJobEvent, Pair<Optional<ResolveAnaphorsJob>, Optional<Exception>>> resolveAnaphorsJobListener,
			final Consumer<Job> anaphorResolutionQueue, final Supplier<Boolean> anaphorResolutionQueueHasJobs) {
		ProblemMarking.addProblemPositionsToDocument(newProblems, originDocument);
		final ResolveAnaphorsRefactoring refactoring = createRefactoring(display, compilationUnitAST, originDocument,
				trackingPositionToReResolve, anaphorPositions, newProblems, annotationModelSupplier,
				originSelectionSupplier, originSelectionSetter, anaphoraReresolvingListener, anaphorResolutionListener,
				anaphorResolutionPerspectivationListener, anaphorResolutionProgressListener);
		final ResolveAnaphorsJob job = new ResolveAnaphorsJob(display, anaphorResolutionQueue,
				anaphorResolutionQueueHasJobs, originDocument, trackingPositionToReResolve,
				memberTrackingPositionsToReResolve, newProblems, refactoring, compilationUnitAST,
				annotationModelSupplier, originSelectionSupplier, originSelectionSetter, anaphoraReresolvingListener,
				anaphorResolutionListener, anaphorResolutionPerspectivationListener, anaphorResolutionProgressListener,
				resolveAnaphorsJobListener);
		anaphorResolutionQueue.accept(job);
	}

	static ResolveAnaphorsRefactoring createRefactoring(final Display display, final CompilationUnit compilationUnitAST,
			final IDocument originDocument, final MemberTrackingPosition trackingPositionToReResolve,
			final List<PositionForAnaphor> anaphorPositions, final List<KnownProblemPosition> newProblems,
			final Supplier<IAnnotationModel> annotationModelSupplier,
			final Supplier<ISelection> originSelectionSupplier, final Consumer<ISelection> originSelectionSetter,
			final AnaphoraReresolvingDocumentListener anaphoraReresolvingListener,
			final BiConsumer<AnaphorResolutionEvent.EventType, AnaphorResolutionEvent> anaphorResolutionListener,
			final BiConsumer<AnaphorResolutionPerspectivationEvent, List<? extends Position>> anaphorResolutionPerspectivationListener,
			final BiConsumer<AnaphorResolutionProgressEvent, List<? extends AbstractAnaphoraPositionForRepresentation>> anaphorResolutionProgressListener) {
		final IAnnotationModel annotationModel = annotationModelSupplier.get();
		return new ResolveAnaphorsRefactoring(display, originDocument, annotationModel, originSelectionSupplier,
				originSelectionSetter, compilationUnitAST, anaphoraReresolvingListener, anaphorResolutionListener,
				anaphorResolutionPerspectivationListener, anaphorResolutionProgressListener,
				trackingPositionToReResolve, anaphorPositions, newProblems);
	}

	static void removeAmbiguityAnnotations(final IAnnotationModel annotationModel,
			final MemberTrackingPosition trackingPosition) {
		getAnnotationsToRemove(annotationModel, trackingPosition)
				.forEach(annotationAndPosition -> annotationModel.removeAnnotation(annotationAndPosition.getLeft()));
	}

}
