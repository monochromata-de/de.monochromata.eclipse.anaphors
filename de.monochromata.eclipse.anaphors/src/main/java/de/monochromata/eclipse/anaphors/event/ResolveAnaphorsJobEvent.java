package de.monochromata.eclipse.anaphors.event;

import java.util.Optional;

import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.eclipse.anaphors.editor.update.job.ResolveAnaphorsJob;

/**
 * Informs about the status of {@link ResolveAnaphorsJob}s and comes with a
 * {@link Pair} of and {@link Optional} with a {@link ResolveAnaphorsJob} that
 * the event is about and an {@link Optional} with an {@link Exception} that is
 * present if the job failed.
 */
public enum ResolveAnaphorsJobEvent {

    /**
     * Has neither job, nor exception.
     */
    TriggeredByUpcomingDocumentChange,

    /**
     * Has neither job, nor exception.
     */
    TriggeredByElementChange,

    /**
     * Has neither job, nor exception.
     */
    TriggeredAsFollowUpAfterJobWithoutChanges,

    /**
     * Has the created job and no exception.
     */
    Created,

    /**
     * Has neither job, nor exception.
     */
    NotCreatedBecauseAnotherJobIsAlreadyRunning,

    /**
     * Has the started job and no exception.
     */
    Started,

    /**
     * Has the succeeded job and no exception.
     */
    Succeeded,

    /**
     * Has the failed job and the exception that caused the job to fail.
     */
    Failed,

    /**
     * Has the failed job and the exception that caused the job to fail.
     */
    FailedWithBadLocationExceptionWillRetry

}
