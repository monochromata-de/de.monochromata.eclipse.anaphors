package de.monochromata.eclipse.anaphors.editor;

import java.util.stream.Stream;

import org.eclipse.jface.text.Position;

import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public interface AnaphoraQuerying {

	static Stream<AbstractAnaphoraPositionForRepresentation> getAnaphoraAt(final int startPosition, final int length,
			final Stream<? extends Position> positions) {
		return positions.filter(position -> anaphoraSelected(startPosition, length, position))
				.flatMap(position -> getMatches((AbstractAnaphoraPositionForRepresentation) position));
	}

	static Stream<AbstractAnaphoraPositionForRepresentation> getMatches(
			final AbstractAnaphoraPositionForRepresentation position) {
		if (position instanceof PositionForRelatedExpression) {
			return getMatches((PositionForRelatedExpression) position);
		} else if (position instanceof PositionForAnaphor) {
			return getMatches(((PositionForAnaphor) position).getRelatedExpressionPosition());
		}
		throw new IllegalArgumentException("Unknown position type: " + position.getClass().getName());
	}

	static Stream<AbstractAnaphoraPositionForRepresentation> getMatches(final PositionForRelatedExpression position) {
		return Stream.concat(Stream.of(position), position.getNext().stream().map(pos -> pos));
	}

	static boolean anaphoraSelected(final int startPosition, final int length, final Position anaphorPosition) {
		return positionSelected(startPosition, length, anaphorPosition);
	}

	static boolean positionSelected(final int startPosition, final int length, final Position position) {
		return position.overlapsWith(startPosition, length)
				|| zeroLengthSelectionAtEndOfPosition(startPosition, length, position);
	}

	static boolean zeroLengthSelectionAtEndOfPosition(final int startPosition, final int length,
			final Position position) {
		return length == 0 && position.offset + position.length == startPosition;
	}

}
