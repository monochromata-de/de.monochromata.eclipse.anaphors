package de.monochromata.eclipse.anaphors;

import static de.monochromata.anaphors.ast.chain.ChainTraversal.getAnaphorElements;
import static de.monochromata.anaphors.ast.chain.ChainTraversal.getRelatedExpressionElementForAnaphorElement;
import static java.util.stream.Collectors.toMap;

import java.util.Map;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicChainElement;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.ast.AnaphorPart;

public interface AnaphoraCreation {

	static Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, Anaphora> createAnaphoraByAnaphorPart(
			final PublicChainElement chainRoot) {
		return getAnaphorElements(chainRoot).map(anaphorElement -> new ImmutablePair<>(anaphorElement.anaphor,
				new Anaphora(getRelatedExpressionElementForAnaphorElement(anaphorElement).relatedExpression,
						anaphorElement.anaphor)))
				.collect(toMap(Pair::getKey, Pair::getValue));
	}

}
