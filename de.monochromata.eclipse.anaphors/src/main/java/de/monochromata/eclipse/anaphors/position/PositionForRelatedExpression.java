package de.monochromata.eclipse.anaphors.position;

import de.monochromata.eclipse.persp.PerspectivationPosition;

public class PositionForRelatedExpression extends AbstractAnaphoraPositionForRepresentation {

    /**
     * For testing: creates a position with a perspectivation position at the same
     * position, without perspectivations.
     */
    public PositionForRelatedExpression(final int offset, final int length) {
        this(offset, length, new PerspectivationPosition(offset, length));
    }

    public PositionForRelatedExpression(final int offset, final int length,
            final PerspectivationPosition perspectivationPosition) {
        super(offset, length, perspectivationPosition);
    }

    public PositionForRelatedExpression(final int offset, final PerspectivationPosition perspectivationPosition) {
        super(offset, perspectivationPosition);
    }

    public PositionForRelatedExpression(final PerspectivationPosition perspectivationPosition) {
        super(perspectivationPosition);
    }

    @Override
    public boolean equals(final Object other) {
        return super.equals(other) && getClass() == other.getClass();
    }

    @Override
    public String toString() {
        return "PositionForRelatedExpression [offset=" + offset
                + ", length=" + length + ", isDeleted=" + isDeleted + "]";
    }

}
