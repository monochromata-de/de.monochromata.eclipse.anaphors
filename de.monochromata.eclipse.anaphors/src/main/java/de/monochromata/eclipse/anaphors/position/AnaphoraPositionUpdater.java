package de.monochromata.eclipse.anaphors.position;

import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.eclipse.jface.text.IDocument;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.position.DoublyLinkedPositionUpdater;
import de.monochromata.function.TriConsumer;

public class AnaphoraPositionUpdater<T extends AbstractAnaphoraPosition<T>>
		extends DoublyLinkedPositionUpdater<T, Void> {

	public AnaphoraPositionUpdater(final Consumer<List<Anaphora>> deletedAnaphorasListener) {
		super(ANAPHORA_CATEGORY, createPreDeleteListener(deletedAnaphorasListener), null);
	}

	protected static <T extends AbstractAnaphoraPosition<T>> TriConsumer<T, IDocument, Void> createPreDeleteListener(
			final Consumer<List<Anaphora>> deletedAnaphorasListener) {
		final BiConsumer<T, IDocument> maintainChain = maintainChainBeforeDeletion(ANAPHORA_CATEGORY);
		return (position, document, unusedDeletionStrategy) -> {
			notifyListener(position, deletedAnaphorasListener);
			maintainChain.accept(position, document);
		};
	}

	private static void notifyListener(final AbstractAnaphoraPosition position,
			final Consumer<List<Anaphora>> deletedAnaphorasListener) {
		if (position instanceof PositionForAnaphor) {
			notifyListener((PositionForAnaphor) position, deletedAnaphorasListener);
		} else if (position instanceof PositionForRelatedExpression) {
			notifyListener((PositionForRelatedExpression) position, deletedAnaphorasListener);
		}
	}

	protected static void notifyListener(final PositionForAnaphor position,
			final Consumer<List<Anaphora>> deletedAnaphorasListener) {
		deletedAnaphorasListener.accept(singletonList(position.anaphora));
	}

	protected static void notifyListener(final PositionForRelatedExpression position,
			final Consumer<List<Anaphora>> deletedAnaphorasListener) {
		final List<Anaphora> anaphoraOfNext = getAnaphoraOfNext(position);
		deletedAnaphorasListener.accept(anaphoraOfNext);
	}

	protected static List<Anaphora> getAnaphoraOfNext(final PositionForRelatedExpression position) {
		return position.getNext().stream().filter(next -> next instanceof PositionForAnaphor)
				.map(next -> ((PositionForAnaphor) next).anaphora).collect(toList());
	}

}
