package de.monochromata.eclipse.anaphors.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public interface PreferenceConstants {

    String USE_EYETRACKING = "useEyetracking";
    String ADD_FINAL_MODIFIER_TO_CREATED_TEMPORARY_VARIABLES = "addFinalModifierToCreatedTemporaryVariables";

    String PERSPECTIVATION_STRATEGY = "perspectivationStrategy";

    String PERSPECTIVATION_STRATEGY_NOTHING = "nothing";
    String PERSPECTIVATION_STRATEGY_RELATED_EXPRESSIONS = "relatedExpression";
    String PERSPECTIVATION_STRATEGY_EVERYTHING = "everything";

    String PERSPECTIVATION_STRATEGY_DEFAULT = PERSPECTIVATION_STRATEGY_RELATED_EXPRESSIONS;

    String MARK_RESOLUTION_OCCURRENCES = "markResolutionOccurrences";
    String USE_LOCAL_VARIABLE_TYPE_INFERENCE = "useLocalVariableTypeInference";

}
