package de.monochromata.eclipse.anaphors.editor;

import static de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPosition.MEMBER_TRACKING_CATEGORY;
import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static de.monochromata.eclipse.anaphors.position.KnownProblemPosition.KNOWN_PROBLEM_CATEGORY;

import java.util.List;
import java.util.function.Consumer;

import org.eclipse.jface.text.DefaultPositionUpdater;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.editors.text.ForwardingDocumentProvider;
import org.eclipse.ui.texteditor.IDocumentProvider;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.position.AnaphoraPositionUpdater;
import de.monochromata.eclipse.anaphors.position.KnownProblemPositionUpdater;
import de.monochromata.eclipse.persp.PerspectivationDocumentManager;

/**
 * The document provider is used to create new origin documents.
 * <p>
 * See {@link PerspectivationDocumentManager} for creation of image documents.
 */
public class AnaphoraDocumentProvider extends ForwardingDocumentProvider {

    public static final String PARTITIONING = AnaphoraDocumentProvider.class.getName() + ".partitioning";

    public AnaphoraDocumentProvider(final IDocumentProvider parentProvider,
            final Consumer<List<Anaphora>> anaphoraDeletionListener) {
        super(PARTITIONING, originDocument -> configure(originDocument, anaphoraDeletionListener), parentProvider);
    }

    public AnaphoraDocumentProvider(
            final Consumer<List<Anaphora>> anaphoraDeletionListener) {
        super(PARTITIONING, originDocument -> configure(originDocument, anaphoraDeletionListener));
    }

    public static void configure(final IDocument originDocument,
            final Consumer<List<Anaphora>> anaphoraDeletionListener) {
        if (!originDocument.containsPositionCategory(ANAPHORA_CATEGORY)) {
            originDocument.addPositionCategory(ANAPHORA_CATEGORY);
            originDocument.addPositionUpdater(new AnaphoraPositionUpdater(anaphoraDeletionListener));
        }
        if (!originDocument.containsPositionCategory(KNOWN_PROBLEM_CATEGORY)) {
            originDocument.addPositionCategory(KNOWN_PROBLEM_CATEGORY);
            originDocument.addPositionUpdater(new KnownProblemPositionUpdater());
        }
        if (!originDocument.containsPositionCategory(MEMBER_TRACKING_CATEGORY)) {
            originDocument.addPositionCategory(MEMBER_TRACKING_CATEGORY);
            originDocument.addPositionUpdater(new DefaultPositionUpdater(MEMBER_TRACKING_CATEGORY));
        }
    }

}
