package de.monochromata.eclipse.anaphors.editor.update.marking;

import static de.monochromata.eclipse.anaphors.editor.update.marking.ProblemConversion.toOffsetAndLength;
import static de.monochromata.eclipse.anaphors.position.KnownProblemPosition.KNOWN_PROBLEM_CATEGORY;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapPositionExceptions;
import static de.monochromata.eclipse.position.PositionQuerying.getTrackingPositions;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.compiler.IProblem;
import org.eclipse.jface.text.IDocument;

import de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPosition;
import de.monochromata.eclipse.anaphors.position.KnownProblemPosition;

public interface ProblemMarking {

    static Map<MemberTrackingPosition, List<IProblem>> collectProblemMembersToReResolve(final IDocument originDocument,
            final IProblem[] problems, final IJavaElement compilationUnitElement) {
        final Map<MemberTrackingPosition, List<IProblem>> problemsByMember = new HashMap<>();
        stream(problems)
                .filter(ProblemMarking::shouldHandle)
                .forEach(indexProblemsByMember(originDocument, problemsByMember));
        return problemsByMember;
    }

    static Consumer<IProblem> indexProblemsByMember(final IDocument originDocument,
            final Map<MemberTrackingPosition, List<IProblem>> problemsByMember) {
        return problem -> indexProblemByMember(problem, originDocument, problemsByMember);
    }

    static void indexProblemByMember(final IProblem problem, final IDocument originDocument,
            final Map<MemberTrackingPosition, List<IProblem>> problemsByMember) {
        final Pair<Integer, Integer> offsetAndLength = toOffsetAndLength(problem);
        getTrackingPositions(offsetAndLength, originDocument)
                .forEach(trackingPosition -> problemsByMember
                        .computeIfAbsent(trackingPosition, unused -> new ArrayList<>())
                        .add(problem));
    }

    static boolean shouldHandle(final IProblem problem) {
        return problem.getID() == IProblem.UnresolvedVariable || problem.getID() == IProblem.UndefinedName;
    }

    static List<KnownProblemPosition> createProblemPositions(final MemberTrackingPosition trackingPositionToReResolve,
            final Map<MemberTrackingPosition, List<IProblem>> problemsByTrackingPosition) {
        return createProblemPositions(problemsByTrackingPosition.get(trackingPositionToReResolve));
    }

    static List<KnownProblemPosition> createProblemPositions(final List<IProblem> problems) {
        if (problems == null) {
            return emptyList();
        }
        return problems
                .stream()
                .map(KnownProblemPosition::new)
                .collect(toList());
    }

    static void addProblemPositionsToDocument(final List<KnownProblemPosition> problemPositions,
            final IDocument originDocument) {
        problemPositions.forEach(newProblem -> wrapPositionExceptions(
                () -> originDocument.addPosition(KNOWN_PROBLEM_CATEGORY, newProblem)));
    }

}
