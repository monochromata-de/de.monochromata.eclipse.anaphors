package de.monochromata.eclipse.anaphors.editor.update.job;

import static de.monochromata.eclipse.anaphors.editor.update.job.ResolveAnaphorsJobCreation.createAndScheduleJobIfThereAreMembersToBeReResolved;
import static de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent.Failed;
import static de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent.FailedWithBadLocationExceptionWillRetry;
import static de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent.TriggeredAsFollowUpAfterJobWithoutChanges;
import static de.monochromata.eclipse.anaphors.position.KnownProblemPosition.KNOWN_PROBLEM_CATEGORY;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadPositionCategoryException;
import static java.lang.System.identityHashCode;
import static java.util.Collections.emptyList;
import static java.util.Optional.empty;
import static org.eclipse.core.runtime.Status.OK_STATUS;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.internal.corext.refactoring.code.ResolveAnaphorsRefactoring;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CheckConditionsOperation;
import org.eclipse.ltk.core.refactoring.CreateChangeOperation;
import org.eclipse.ltk.core.refactoring.IUndoManager;
import org.eclipse.ltk.core.refactoring.NullChange;
import org.eclipse.ltk.core.refactoring.PerformChangeOperation;
import org.eclipse.ltk.core.refactoring.RefactoringCore;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.swt.widgets.Display;

import de.monochromata.eclipse.anaphors.Activator;
import de.monochromata.eclipse.anaphors.editor.update.AnaphoraReresolvingDocumentListener;
import de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPosition;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionPerspectivationEvent;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionProgressEvent;
import de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.anaphors.position.KnownProblemPosition;
import de.monochromata.eclipse.position.UncheckedBadLocationException;

public class ResolveAnaphorsJob extends Job {

	private final Display display;
	private final Consumer<Job> anaphorResolutionQueue;
	private final Supplier<Boolean> anaphorResolutionQueueHasJobs;
	private final IDocument originDocument;
	private final MemberTrackingPosition trackingPositionToReResolve;
	private final List<MemberTrackingPosition> trackingPositionsToReResolve;
	private final List<KnownProblemPosition> newProblems;
	private final ResolveAnaphorsRefactoring refactoring;
	protected final CompilationUnit astRoot;
	private final Supplier<IAnnotationModel> annotationModelSupplier;
	private final Supplier<ISelection> originSelectionSupplier;
	private final Consumer<ISelection> originSelectionSetter;
	private final AnaphoraReresolvingDocumentListener anaphoraReresolvingListener;
	protected final BiConsumer<AnaphorResolutionEvent.EventType, AnaphorResolutionEvent> anaphorResolutionListener;
	private final BiConsumer<AnaphorResolutionPerspectivationEvent, List<? extends Position>> anaphorResolutionPerspectivationListener;
	private final BiConsumer<ResolveAnaphorsJobEvent, Pair<Optional<ResolveAnaphorsJob>, Optional<Exception>>> resolveAnaphorsJobListener;
	private final BiConsumer<AnaphorResolutionProgressEvent, List<? extends AbstractAnaphoraPositionForRepresentation>> anaphorResolutionProgressListener;

	public ResolveAnaphorsJob(final Display display, final Consumer<Job> anaphorResolutionQueue,
			final Supplier<Boolean> anaphorResolutionQueueHasJobs, final IDocument originDocument,
			final MemberTrackingPosition trackingPositionToReResolve,
			final List<MemberTrackingPosition> trackingPositionsToReResolve,
			final List<KnownProblemPosition> newProblems, final ResolveAnaphorsRefactoring refactoring,
			final CompilationUnit astRoot, final Supplier<IAnnotationModel> annotationModelSupplier,
			final Supplier<ISelection> originSelectionSupplier, final Consumer<ISelection> originSelectionSetter,
			final AnaphoraReresolvingDocumentListener anaphoraReresolvingListener,
			final BiConsumer<AnaphorResolutionEvent.EventType, AnaphorResolutionEvent> anaphorResolutionListener,
			final BiConsumer<AnaphorResolutionPerspectivationEvent, List<? extends Position>> anaphorResolutionPerspectivationListener,
			final BiConsumer<AnaphorResolutionProgressEvent, List<? extends AbstractAnaphoraPositionForRepresentation>> anaphorResolutionProgressListener,
			final BiConsumer<ResolveAnaphorsJobEvent, Pair<Optional<ResolveAnaphorsJob>, Optional<Exception>>> resolveAnaphorsJobListener) {
		super("Resolve anaphors from problems");
		setPriority(SHORT);
		this.display = display;
		this.anaphorResolutionQueue = anaphorResolutionQueue;
		this.anaphorResolutionQueueHasJobs = anaphorResolutionQueueHasJobs;
		this.trackingPositionToReResolve = trackingPositionToReResolve;
		this.trackingPositionsToReResolve = trackingPositionsToReResolve;
		this.originDocument = originDocument;
		this.newProblems = newProblems;
		this.refactoring = refactoring;
		this.astRoot = astRoot;
		this.annotationModelSupplier = annotationModelSupplier;
		this.originSelectionSupplier = originSelectionSupplier;
		this.originSelectionSetter = originSelectionSetter;
		this.anaphoraReresolvingListener = anaphoraReresolvingListener;
		this.anaphorResolutionListener = anaphorResolutionListener;
		this.anaphorResolutionPerspectivationListener = anaphorResolutionPerspectivationListener;
		this.anaphorResolutionProgressListener = anaphorResolutionProgressListener;
		this.resolveAnaphorsJobListener = resolveAnaphorsJobListener;
		resolveAnaphorsJobListener.accept(ResolveAnaphorsJobEvent.Created,
				new ImmutablePair<>(Optional.of(this), empty()));
	}

	/**
	 * Access is widened so testing is easy.
	 */
	@Override
	public IStatus run(final IProgressMonitor monitor) {
		// TODO Use the progress monitor
		resolveAnaphorsJobListener.accept(ResolveAnaphorsJobEvent.Started,
				new ImmutablePair<>(Optional.of(this), empty()));
		try {
			final Pair<Boolean, RefactoringStatus> anyChangePerformanedAndConditionCheckingStatus = performRefactoring();
			final boolean anyChangePerformed = anyChangePerformanedAndConditionCheckingStatus.getLeft();
			final RefactoringStatus conditionCheckingStatus = anyChangePerformanedAndConditionCheckingStatus.getRight();
			if (!conditionCheckingStatus.isOK()) {
				final int severity = conditionCheckingStatus.getSeverity();
				System.err.println("Condition checking status is not ok: "
						+ conditionCheckingStatus.getMessageMatchingSeverity(severity));
			}
			if (!anyChangePerformed && !anaphorResolutionQueueHasJobs.get()) {
				resolveAnaphorsJobListener.accept(TriggeredAsFollowUpAfterJobWithoutChanges,
						new ImmutablePair<>(empty(), empty()));
				// If no change has been performed, the existing problems remain and will remain
				// unresolved as long as the source is not modified by the user.
				createAndScheduleJobIfThereAreMembersToBeReResolved(display, astRoot, originDocument,
						trackingPositionsToReResolve, unused -> emptyList(), annotationModelSupplier,
						originSelectionSupplier, originSelectionSetter, anaphoraReresolvingListener,
						anaphorResolutionListener, anaphorResolutionPerspectivationListener,
						anaphorResolutionProgressListener, resolveAnaphorsJobListener, anaphorResolutionQueue,
						anaphorResolutionQueueHasJobs);
			}
			resolveAnaphorsJobListener.accept(ResolveAnaphorsJobEvent.Succeeded,
					new ImmutablePair<>(Optional.of(this), empty()));
			return OK_STATUS;
		} catch (final UncheckedBadLocationException e) {
			resolveAnaphorsJobListener.accept(FailedWithBadLocationExceptionWillRetry,
					new ImmutablePair<>(Optional.of(this), Optional.of(e)));
			trackingPositionsToReResolve.add(trackingPositionToReResolve);
			return OK_STATUS;
		} catch (final Exception e) {
			resolveAnaphorsJobListener.accept(Failed, new ImmutablePair<>(Optional.of(this), Optional.of(e)));
			return createErrorStatus(e);
		} finally {
			removeNewProblemPositionsFromDocument();
		}
	}

	protected void removeNewProblemPositionsFromDocument() {
		newProblems.forEach(position -> wrapBadPositionCategoryException(
				() -> originDocument.removePosition(KNOWN_PROBLEM_CATEGORY, position)));
	}

	protected IStatus createErrorStatus(final Exception e) {
		final String message = "Failed to update anaphora in job " + identityHashCode(this) + ": " + e.getMessage();
		return new Status(IStatus.ERROR, Activator.PLUGIN_ID, message, e);
	}

	/**
	 * Perform the refactoring
	 *
	 * @param refactoring     the refactoring instance
	 * @param source          the source code of the element or file to be
	 *                        refactored
	 * @param anaphorPosition the position of the anaphor to be refactored
	 * @return whether any changes were performed and the condition checking status.
	 */
	protected Pair<Boolean, RefactoringStatus> performRefactoring() throws CoreException {
		final IUndoManager undoManager = RefactoringCore.getUndoManager();
		undoManager.flush(); // TODO: Need to synchronize?

		final IWorkspace workspace = ResourcesPlugin.getWorkspace();
		final CreateChangeOperation create = new CreateChangeOperation(
				new CheckConditionsOperation(refactoring, CheckConditionsOperation.ALL_CONDITIONS),
				RefactoringStatus.FATAL);
		workspace.run(create, null);
		final Change change = create.getChange();

		if (change == null) {
			// TODO: Need to investigate
			System.err.println("Change is null!");
			return new ImmutablePair<>(false, RefactoringStatus.createErrorStatus("Change is null"));
		}

		try {

			performChange(change, undoManager, refactoring);

			// TODO: Maybe catch CoreException and perform undo change
			final RefactoringStatus status = create.getConditionCheckingStatus();

			/*
			 * if (status.isOK()) { return null; } else { Change undo=
			 * perform.getUndoChange(); // TODO: What would the undo change be useful for?
			 * return status; }
			 */
			return new ImmutablePair<>(change instanceof NullChange, status);
		} catch (final UncheckedBadLocationException e) {
			throw e;
		} catch (final Exception e) {
			final String message = "Failed to update anaphora";
			final String exceptionStackTrace = getExceptionStackTrace(e);
			return new ImmutablePair<>(false,
					RefactoringStatus.createErrorStatus(message + ": " + exceptionStackTrace));
		}
	}

	protected String getExceptionStackTrace(final Exception e) {
		final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		final PrintStream printStream = new PrintStream(outputStream);
		e.printStackTrace(printStream);
		final String exceptionStackTrace = outputStream.toString();
		return exceptionStackTrace;
	}

	protected void performChange(final Change change, final IUndoManager undoManager,
			final ResolveAnaphorsRefactoring refactoring) {
		try {
			final PerformChangeOperation perform = new PerformChangeOperation(change);
			perform.setUndoManager(undoManager, refactoring.getName());
			ResourcesPlugin.getWorkspace().run(perform, null);
		} catch (final CoreException e) {
			throw new IllegalStateException(e.getMessage(), e);
		}
	}

}
