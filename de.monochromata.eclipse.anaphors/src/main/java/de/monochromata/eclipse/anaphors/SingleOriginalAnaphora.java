package de.monochromata.eclipse.anaphors;

import java.util.Optional;

import org.apache.commons.lang3.tuple.Triple;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

/**
 * Contains a single original anaphora. This type is used if anaphor resolution
 * failed.
 */
public class SingleOriginalAnaphora implements OriginalAnaphora {

	public final Optional<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>> originalAnaphora;

	public SingleOriginalAnaphora(
			final Optional<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>> originalAnaphora) {
		this.originalAnaphora = originalAnaphora;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((originalAnaphora == null) ? 0 : originalAnaphora.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final SingleOriginalAnaphora other = (SingleOriginalAnaphora) obj;
		if (originalAnaphora == null) {
			if (other.originalAnaphora != null) {
				return false;
			}
		} else if (!originalAnaphora.equals(other.originalAnaphora)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "SingleOriginalAnaphora [originalAnaphora=" + originalAnaphora + "]";
	}

}
