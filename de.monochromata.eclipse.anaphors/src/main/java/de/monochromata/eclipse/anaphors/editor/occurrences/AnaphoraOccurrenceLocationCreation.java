package de.monochromata.eclipse.anaphors.editor.occurrences;

import static org.eclipse.jdt.internal.core.manipulation.search.IOccurrencesFinder.F_READ_OCCURRENCE;
import static org.eclipse.jdt.internal.core.manipulation.search.IOccurrencesFinder.F_WRITE_OCCURRENCE;

import org.eclipse.jdt.internal.core.manipulation.search.IOccurrencesFinder.OccurrenceLocation;
import org.eclipse.jface.text.Position;

import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

@SuppressWarnings("restriction")
public interface AnaphoraOccurrenceLocationCreation {

    static OccurrenceLocation createOccurrenceLocation(final AbstractAnaphoraPositionForRepresentation position) {
        if (position instanceof PositionForRelatedExpression) {
            return createOccurrenceLocation((PositionForRelatedExpression) position);
        } else if (position instanceof PositionForAnaphor) {
            return createOccurrenceLocation((PositionForAnaphor) position);
        }
        throw new IllegalArgumentException("Unknown type: " + position.getClass().getName());
    }

    static OccurrenceLocation createOccurrenceLocation(final PositionForRelatedExpression position) {
        return createOccurrenceLocation(position, F_WRITE_OCCURRENCE,
                relatedExpressionDescription());
    }

    static String relatedExpressionDescription() {
        return "Related expression";
    }

    static OccurrenceLocation createOccurrenceLocation(final PositionForAnaphor position) {
        return createOccurrenceLocation(position, F_READ_OCCURRENCE,
                anaphorDescription(position.anaphora.anaphor));
    }

    static String anaphorDescription(final String identifier) {
        return "Anaphor co-referent to " + identifier;
    }

    static OccurrenceLocation createOccurrenceLocation(final Position position, final int flags,
            final String description) {
        return new OccurrenceLocation(position.offset, position.length, flags, description);
    }

}
