package de.monochromata.eclipse.anaphors.position;

import org.eclipse.jdt.core.dom.ASTNode;

public class AnaphoraPositionOnRelatedExpression extends AbstractAnaphoraPositionOnASTNode {

	public AnaphoraPositionOnRelatedExpression(final ASTNode astNode, final int offset, final int length) {
		super(astNode, offset, length);
	}

	public AnaphoraPositionOnRelatedExpression(final ASTNode astNode, final int offset) {
		super(astNode, offset);
	}

	public AnaphoraPositionOnRelatedExpression(final ASTNode astNode) {
		super(astNode);
	}

	@Override
	public String toString() {
		return "AnaphoraPositionOnRelatedExpression [offset=" + offset + ", length=" + length + ", isDeleted="
				+ isDeleted + ", astNode=" + astNode + "]";
	}

}
