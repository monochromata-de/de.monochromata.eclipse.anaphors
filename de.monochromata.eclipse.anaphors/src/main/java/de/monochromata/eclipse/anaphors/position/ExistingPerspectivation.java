package de.monochromata.eclipse.anaphors.position;

import static de.monochromata.eclipse.anaphors.position.ExistingPosition.isExistingPositionToBeRetained;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicChainElement;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.eclipse.anaphors.OriginalAnaphora;
import de.monochromata.eclipse.persp.PerspectivationPosition;

public interface ExistingPerspectivation {

	static Function<PublicChainElement, PerspectivationPosition> getExistingRelatedExpressionPerspectivation(
			final List<PerspectivationPosition> positionsToRemove) {
		return relatedExpressionElement -> getExistingRelatedExpressionPerspectivation(relatedExpressionElement,
				positionsToRemove);
	}

	static PerspectivationPosition getExistingRelatedExpressionPerspectivation(
			final PublicChainElement relatedExpressionElement, final List<PerspectivationPosition> positionsToRemove) {
		return getExistingRelatedExpressionPerspectivation(relatedExpressionElement,
				isExistingPositionToBeRetained(positionsToRemove));
	}

	static PerspectivationPosition getExistingRelatedExpressionPerspectivation(
			final PublicChainElement relatedExpressionElement,
			final Predicate<PerspectivationPosition> isExistingPositionToBeRetained) {
		// It does not matter which of the next elements is chosen - all next elements
		// should be anaphors related to the related expression element. The first
		// anaphor element is chosen to ease debugging.
		final var anaphorChainElement = relatedExpressionElement.next.stream().findFirst().orElseThrow();
		return getExistingPerspectivation(anaphorChainElement.anaphor, anaphorChainElement.anaphorAttachment,
				Triple::getLeft, isExistingPositionToBeRetained);
	}

	static Function<PublicChainElement, PerspectivationPosition> getExistingAnaphorPerspectivation(
			final List<PerspectivationPosition> positionsToRemove) {
		return anaphorElement -> getExistingAnaphorPerspectivation(anaphorElement, positionsToRemove);
	}

	static PerspectivationPosition getExistingAnaphorPerspectivation(final PublicChainElement anaphorElement,
			final List<PerspectivationPosition> positionsToRemove) {
		return getExistingAnaphorPerspectivation(anaphorElement, isExistingPositionToBeRetained(positionsToRemove));
	}

	static PerspectivationPosition getExistingAnaphorPerspectivation(final PublicChainElement anaphorElement,
			final Predicate<PerspectivationPosition> isExistingPositionToBeRetained) {
		return getExistingPerspectivation(anaphorElement.anaphor, anaphorElement.anaphorAttachment, Triple::getRight,
				isExistingPositionToBeRetained);
	}

	private static PerspectivationPosition getExistingPerspectivation(
			final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart,
			final OriginalAnaphora originalAnaphora,
			final Function<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>, AbstractAnaphoraPositionForRepresentation> getAnaphoraPosition,
			final Predicate<PerspectivationPosition> isExistingPositionToBeRetained) {
		final Function<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>, PerspectivationPosition> getPerspectivationPosition = getAnaphoraPosition
				.andThen(anaphorPosition -> anaphorPosition.perspectivationPosition);
		return ExistingPosition.getExistingPosition(anaphorPart, originalAnaphora, getPerspectivationPosition,
				isExistingPositionToBeRetained);
	}

}
