package de.monochromata.eclipse.anaphors.editor;

import static java.lang.String.format;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.internal.core.manipulation.search.IOccurrencesFinder;
import org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitEditor;
import org.eclipse.jdt.internal.ui.javaeditor.JavaEditor;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.ISelection;

import de.monochromata.eclipse.anaphors.Util;
import de.monochromata.eclipse.persp.CompilationUnitPerspectivationEditor;

/**
 * A compatibility layer between {@link CompilationUnitEditor} and
 * {@link CognitiveEditor}.
 *
 * <p>
 * The layer permits the latter to access to members of the former and hides
 * implementation-differences of the former from the latter.
 * </p>
 *
 */
@SuppressWarnings("restriction")
public class CompilationUnitEditorCompatibility extends CompilationUnitPerspectivationEditor {

    // TODO: Split into a generic part/interface and one for the specific
    // versions and a super-set of the members they have in common

    protected static Constructor<?> JavaEditor_OccurrencesFinderJob;

    protected static Field CompilationUnitEditor_CLOSE_BRACKETS;
    protected static Field CompilationUnitEditor_CLOSE_STRINGS;
    protected static Field CompilationUnitEditor_CODE_FORMATTER_TAB_SIZE;
    protected static Field CompilationUnitEditor_SPACES_FOR_TABS;
    protected static Field CompilationUnitEditor_fBracketInserter;
    protected static Field JavaEditor_fOccurrencesFinderJob;
    protected static Field SourceViewer_fContentAssistant;

    protected static Method CompilationUnitEditor_handlePreferenceStoreChanged;
    protected static Method CompilationUnitEditor_BracketInserter_setCloseAngularBracketsEnabled;
    protected static Method CompilationUnitEditor_BracketInserter_setCloseBracketsEnabled;
    protected static Method CompilationUnitEditor_BracketInserter_setCloseStringsEnabled;
    protected static Method JavaEditor_handlePreferenceStoreChanged;
    protected static Method JavaEditor_OccurrencesFinderJob_doCancel;
    protected static Method JavaEditor_OccurrencesFinderJob_run;

    /*
     * Configures {@link AccessibleObject} instances that void access restrictions
     * on members of super-types.
     *
     * TODO: This code will be specific to certain plug-in versions of jdt.core etc.
     */
    static {
        try {
            // The first JavaEditor parameter holding the enclosing instance is
            // not found in the source
            // because it is the implicitly added by the compiler.
            JavaEditor_OccurrencesFinderJob = getConstructorInInaccessibleMemberClassWithoutAccessChecks(
                    JavaEditor.class, "org.eclipse.jdt.internal.ui.javaeditor.JavaEditor$OccurrencesFinderJob",
                    JavaEditor.class, IDocument.class, IOccurrencesFinder.OccurrenceLocation[].class, ISelection.class);

            CompilationUnitEditor_fBracketInserter = getFieldWithoutAccessChecks(CompilationUnitEditor.class,
                    "fBracketInserter");
            CompilationUnitEditor_CLOSE_BRACKETS = getFieldWithoutAccessChecks(CompilationUnitEditor.class,
                    "CLOSE_BRACKETS");
            CompilationUnitEditor_CLOSE_STRINGS = getFieldWithoutAccessChecks(CompilationUnitEditor.class,
                    "CLOSE_STRINGS");
            CompilationUnitEditor_CODE_FORMATTER_TAB_SIZE = getFieldWithoutAccessChecks(CompilationUnitEditor.class,
                    "CODE_FORMATTER_TAB_SIZE");
            CompilationUnitEditor_SPACES_FOR_TABS = getFieldWithoutAccessChecks(CompilationUnitEditor.class,
                    "SPACES_FOR_TABS");
            JavaEditor_fOccurrencesFinderJob = getFieldWithoutAccessChecks(JavaEditor.class, "fOccurrencesFinderJob");
            SourceViewer_fContentAssistant = getFieldWithoutAccessChecks(SourceViewer.class, "fContentAssistant");

            CompilationUnitEditor_handlePreferenceStoreChanged = getMethodWithoutAccessChecks(
                    CompilationUnitEditor.class, "handlePreferenceStoreChanged", PropertyChangeEvent.class);
            CompilationUnitEditor_BracketInserter_setCloseAngularBracketsEnabled = getMethodInInaccessibleMemberClassWithoutAccessChecks(
                    CompilationUnitEditor.class,
                    "org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitEditor$BracketInserter",
                    "setCloseAngularBracketsEnabled", boolean.class);
            CompilationUnitEditor_BracketInserter_setCloseBracketsEnabled = getMethodInInaccessibleMemberClassWithoutAccessChecks(
                    CompilationUnitEditor.class,
                    "org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitEditor$BracketInserter",
                    "setCloseBracketsEnabled", boolean.class);
            CompilationUnitEditor_BracketInserter_setCloseStringsEnabled = getMethodInInaccessibleMemberClassWithoutAccessChecks(
                    CompilationUnitEditor.class,
                    "org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitEditor$BracketInserter",
                    "setCloseStringsEnabled", boolean.class);
            JavaEditor_handlePreferenceStoreChanged = getMethodWithoutAccessChecks(JavaEditor.class,
                    "handlePreferenceStoreChanged", PropertyChangeEvent.class);
            JavaEditor_OccurrencesFinderJob_doCancel = getMethodInInaccessibleMemberClassWithoutAccessChecks(
                    JavaEditor.class, "org.eclipse.jdt.internal.ui.javaeditor.JavaEditor$OccurrencesFinderJob",
                    "doCancel");
            JavaEditor_OccurrencesFinderJob_run = getMethodInInaccessibleMemberClassWithoutAccessChecks(
                    JavaEditor.class, "org.eclipse.jdt.internal.ui.javaeditor.JavaEditor$OccurrencesFinderJob", "run",
                    IProgressMonitor.class);

        } catch (SecurityException | ClassNotFoundException | NoSuchFieldException | NoSuchMethodException e) {
            Util.logAndShowException("Failed to gain access to super type members", e);
        }
    }

    protected static Constructor<?> getConstructorInInaccessibleMemberClassWithoutAccessChecks(
            final Class<?> containingClass,
            final String memberClassName, final Class<?>... parameterTypes)
            throws ClassNotFoundException, NoSuchMethodException, SecurityException {
        final Class<?> clazz = getMemberClass(containingClass, memberClassName);
        return getConstructorWithoutAccessChecks(clazz, parameterTypes);
    }

    protected static Constructor<?> getConstructorWithoutAccessChecks(final Class<?> clazz,
            final Class<?>... parameterTypes)
            throws NoSuchMethodException, SecurityException {
        final Constructor<?> constructor = clazz.getDeclaredConstructor(parameterTypes);
        constructor.setAccessible(true);
        return constructor;
    }

    protected static Method getMethodInInaccessibleMemberClassWithoutAccessChecks(final Class<?> containingClass,
            final String memberClassName, final String methodName, final Class<?>... parameterTypes)
            throws ClassNotFoundException, NoSuchMethodException, SecurityException {
        final Class<?> clazz = getMemberClass(containingClass, memberClassName);
        return getMethodWithoutAccessChecks(clazz, methodName, parameterTypes);
    }

    protected static Class<?> getMemberClass(final Class<?> containingClass, final String memberClassName)
            throws ClassNotFoundException {
        final boolean initialize = false;
        final Class<?> clazz = Class.forName(memberClassName, initialize, containingClass.getClassLoader());
        return clazz;
    }

    protected static Method getMethodWithoutAccessChecks(final Class<?> clazz, final String methodName,
            final Class<?>... parameterTypes)
            throws NoSuchMethodException, SecurityException {
        final Method method = clazz.getDeclaredMethod(methodName, parameterTypes);
        method.setAccessible(true);
        return method;
    }

    /**
     * Obtains the field declared in the given class and makes it accessible.
     */
    protected static Field getFieldWithoutAccessChecks(final Class<?> clazz, final String fieldName)
            throws NoSuchFieldException, SecurityException {
        final Field field = clazz.getDeclaredField(fieldName);
        field.setAccessible(true);
        return field;
    }

    protected static Object getStatic(final Field field) {
        return get(field, null);
    }

    protected static Object get(final Field field, final Object instance) {
        try {
            return field.get(instance);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException(
                    format("Failed to get value of %1$s field %2$s.%3$s of object %4$s via reflection",
                            (Modifier.isStatic(field.getModifiers()) ? "static " : ""),
                            field.getDeclaringClass().getName(), field.getName(), instance),
                    e);
        }
    }

    protected static void setStatic(final Field field, final Object value) {
        set(field, null, value);
    }

    protected static void set(final Field field, final Object instance, final Object value) {
        try {
            field.set(instance, value);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException(
                    format("Failed to set value of %1$s field %2$s.%3$s of object %4$s to %5$s via reflection",
                            (Modifier.isStatic(field.getModifiers()) ? "static " : ""),
                            field.getDeclaringClass().getName(), field.getName(), instance, value),
                    e);
        }
    }

    protected static Object invokeStatic(final Method method, final Object... args) {
        return invoke(method, null, args);
    }

    protected static Object invoke(final Method method, final Object instance, final Object... args) {
        try {
            return method.invoke(instance, args);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new RuntimeException(format("Failed to invoke %1$s method %2$s.%3$s(%4$s) via reflection",
                    (Modifier.isStatic(method.getModifiers()) ? "static " : ""), method.getDeclaringClass().getName(),
                    method.getName(), getListAsString(args)), e);
        }
    }

    protected static Object invoke(final Constructor<?> constructor, final Object... args) {
        try {
            return constructor.newInstance(args);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException e) {
            throw new RuntimeException(format("Failed to invoke constructor %1$s(%2$s) via reflection",
                    constructor.getName(), getListAsString(args)), e);
        }
    }

    protected static String getListAsString(final Object... args) {
        return Arrays.asList(args).stream().map(o -> o.toString()).collect(Collectors.joining(", "));
    }
}
