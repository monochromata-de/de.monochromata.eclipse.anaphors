package de.monochromata.eclipse.anaphors.editor.update.marking;

import static java.util.Optional.empty;
import static org.eclipse.jdt.core.SourceRange.isAvailable;

import java.util.Optional;
import java.util.function.Function;

import org.eclipse.jdt.core.IMember;
import org.eclipse.jdt.core.ISourceRange;
import org.eclipse.jdt.core.JavaModelException;

import de.monochromata.eclipse.anaphors.Util;

public interface WithSourceRange {

    static <T> Optional<T> withSourceRange(final IMember member, final Function<ISourceRange, T> rangeTransformer) {
        try {
            final ISourceRange sourceRange = member.getSourceRange();
            if (isAvailable(sourceRange)) {
                return Optional.of(rangeTransformer.apply(sourceRange));
            } else {
                Util.logInfo("Member cannot be tracked because it has no source range: " + member);
            }
        } catch (final JavaModelException e) {
            Util.logException("Failed to get source range from member " + member, e);
        }
        return empty();
    }

}
