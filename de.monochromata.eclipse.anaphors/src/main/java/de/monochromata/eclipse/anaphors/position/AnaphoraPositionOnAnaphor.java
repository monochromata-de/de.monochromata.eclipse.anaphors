package de.monochromata.eclipse.anaphors.position;

import org.eclipse.jdt.core.dom.ASTNode;

public class AnaphoraPositionOnAnaphor extends AbstractAnaphoraPositionOnASTNode {

	public final AnaphoraPositionOnRelatedExpression relatedExpressionPosition;

	public AnaphoraPositionOnAnaphor(final ASTNode astNode, final int offset, final int length,
			final AnaphoraPositionOnRelatedExpression relatedExpressionPosition) {
		super(astNode, offset, length);
		this.relatedExpressionPosition = relatedExpressionPosition;
	}

	public AnaphoraPositionOnAnaphor(final ASTNode astNode, final int offset,
			final AnaphoraPositionOnRelatedExpression relatedExpressionPosition) {
		super(astNode, offset);
		this.relatedExpressionPosition = relatedExpressionPosition;
	}

	public AnaphoraPositionOnAnaphor(final ASTNode astNode,
			final AnaphoraPositionOnRelatedExpression relatedExpressionPosition) {
		super(astNode);
		this.relatedExpressionPosition = relatedExpressionPosition;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((relatedExpressionPosition == null) ? 0 : relatedExpressionPosition.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final AnaphoraPositionOnAnaphor other = (AnaphoraPositionOnAnaphor) obj;
		if (relatedExpressionPosition == null) {
			if (other.relatedExpressionPosition != null) {
				return false;
			}
		} else if (!relatedExpressionPosition.equals(other.relatedExpressionPosition)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "AnaphoraPositionOnAnaphor [offset=" + offset + ", length=" + length + ", isDeleted=" + isDeleted
				+ ", astNode=" + astNode + ", relatedExpressionPosition=" + relatedExpressionPosition + "]";
	}

}
