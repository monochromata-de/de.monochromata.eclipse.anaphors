package de.monochromata.eclipse.anaphors.preferences;

import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.ADD_FINAL_MODIFIER_TO_CREATED_TEMPORARY_VARIABLES;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.MARK_RESOLUTION_OCCURRENCES;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.PERSPECTIVATION_STRATEGY;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.PERSPECTIVATION_STRATEGY_DEFAULT;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.USE_EYETRACKING;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.USE_LOCAL_VARIABLE_TYPE_INFERENCE;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import de.monochromata.eclipse.anaphors.Activator;

public class PreferenceInitializer extends AbstractPreferenceInitializer {

    @Override
    public void initializeDefaultPreferences() {
        final IPreferenceStore store = Activator.getDefault().getPreferenceStore();
        store.setDefault(USE_EYETRACKING, false);
        store.setDefault(ADD_FINAL_MODIFIER_TO_CREATED_TEMPORARY_VARIABLES, true);
        store.setDefault(PERSPECTIVATION_STRATEGY, PERSPECTIVATION_STRATEGY_DEFAULT);
        store.setDefault(MARK_RESOLUTION_OCCURRENCES, true);
        store.setDefault(USE_LOCAL_VARIABLE_TYPE_INFERENCE, true);
    }

}
