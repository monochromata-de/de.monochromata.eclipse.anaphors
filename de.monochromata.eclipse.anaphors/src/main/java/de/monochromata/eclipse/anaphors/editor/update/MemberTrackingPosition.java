package de.monochromata.eclipse.anaphors.editor.update;

import org.eclipse.jdt.core.IMember;
import org.eclipse.jface.text.Position;

/**
 * Used to closely track the source range of certain members.
 * <p>
 * The following previously unmet requirements let to the introduction of this
 * class:
 * <ul>
 * <li>The source range of members is not updated while the user types but only
 * when she saves or when the AST is reconciled.</li>
 * <li>When a member is replaced by another one, e.g. when a method's signature
 * changes, it is also not possible to determine its source range anymore even
 * though the source is (mostly) retained.</li>
 * </ul>
 *
 * @see IMember#getSourceRange()
 */
public class MemberTrackingPosition extends Position {

    public static final String MEMBER_TRACKING_CATEGORY = MemberTrackingPosition.class.getName();

    public final IMember member;

    public MemberTrackingPosition(final int offset, final int length, final IMember member) {
        super(offset, length);
        this.member = member;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((member == null) ? 0 : member.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MemberTrackingPosition other = (MemberTrackingPosition) obj;
        if (member == null) {
            if (other.member != null) {
                return false;
            }
        } else if (!member.equals(other.member)) {
            return false;
        }
        return true;
    }

}
