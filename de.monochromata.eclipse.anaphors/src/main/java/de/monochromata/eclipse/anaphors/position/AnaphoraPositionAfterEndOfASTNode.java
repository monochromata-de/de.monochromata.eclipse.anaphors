package de.monochromata.eclipse.anaphors.position;

import org.eclipse.jdt.core.dom.ASTNode;

public class AnaphoraPositionAfterEndOfASTNode extends AbstractAnaphoraPositionAroundASTNode {

	public final AnaphoraPositionBeforeStartOfASTNode startPosition;

	public AnaphoraPositionAfterEndOfASTNode(final NodeType nodeType, final ASTNode astNode,
			final AnaphoraPositionBeforeStartOfASTNode startPosition, final int offset, final int length) {
		super(nodeType, astNode, offset, length);
		this.startPosition = startPosition;
	}

	public AnaphoraPositionAfterEndOfASTNode(final NodeType nodeType, final ASTNode astNode,
			final AnaphoraPositionBeforeStartOfASTNode startPosition, final int offset) {
		super(nodeType, astNode, offset);
		this.startPosition = startPosition;
	}

	public AnaphoraPositionAfterEndOfASTNode(final NodeType nodeType, final ASTNode astNode,
			final AnaphoraPositionBeforeStartOfASTNode startPosition) {
		super(nodeType, astNode);
		this.startPosition = startPosition;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((startPosition == null) ? 0 : startPosition.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final AnaphoraPositionAfterEndOfASTNode other = (AnaphoraPositionAfterEndOfASTNode) obj;
		if (startPosition == null) {
			if (other.startPosition != null) {
				return false;
			}
		} else if (!startPosition.equals(other.startPosition)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "AnaphoraPositionAfterEndOfASTNode [offset=" + offset + ", length=" + length + ", isDeleted=" + isDeleted
				+ ", astNode=" + astNode + ", nodeType=" + nodeType + ", startPosition=" + startPosition + "]";
	}

}
