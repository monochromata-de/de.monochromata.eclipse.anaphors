package de.monochromata.eclipse.anaphors.position;

import org.eclipse.jdt.core.dom.ASTNode;

/**
 * Used temporarily to track the source location of a known {@link ASTNode}
 * while changes are applied to that {@link ASTNode}.
 * <p>
 * Because this position refers to an {@link ASTNode}, it should only be used up
 * until before unknown AST changes can happen and should never be persisted.
 * <p>
 * Positions around nodes enclose the region of the node and assume that all
 * modifications shrink the \emph{area} the source code region (potentially:
 * previously) covered by the node.
 */
public abstract class AbstractAnaphoraPositionAroundASTNode extends AbstractAnaphoraPositionForTransformation {

	public enum NodeType {
		RelatedExpression, Anaphor
	}

	public final NodeType nodeType;

	public AbstractAnaphoraPositionAroundASTNode(final NodeType nodeType, final ASTNode astNode, final int offset,
			final int length) {
		super(astNode, offset, length);
		this.nodeType = nodeType;
	}

	public AbstractAnaphoraPositionAroundASTNode(final NodeType nodeType, final ASTNode astNode, final int offset) {
		super(astNode, offset);
		this.nodeType = nodeType;
	}

	public AbstractAnaphoraPositionAroundASTNode(final NodeType nodeType, final ASTNode astNode) {
		super(astNode);
		this.nodeType = nodeType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((nodeType == null) ? 0 : nodeType.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final AbstractAnaphoraPositionAroundASTNode other = (AbstractAnaphoraPositionAroundASTNode) obj;
		if (nodeType != other.nodeType) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "AbstractAnaphoraPositionAroundASTNode [offset=" + offset + ", length=" + length + ", isDeleted="
				+ isDeleted + ", astNode=" + astNode + ", nodeType=" + nodeType + "]";
	}

}
