package de.monochromata.eclipse.anaphors.editor.update;

import static de.monochromata.eclipse.anaphors.Util.logInfo;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.function.BiConsumer;

import org.eclipse.jface.text.Position;

import de.monochromata.eclipse.anaphors.position.KnownProblemPosition;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;

public interface TriggeringPositionNormalization {

    static List<KnownProblemPosition> ignoreProblemPositionsThatOverlapWithPositionsFromMember(
            final List<PositionForAnaphor> positionsFromMember,
            final List<KnownProblemPosition> problemPositions) {
        return ignoreProblemPositionsThatOverlapWithPositionsFromMember(positionsFromMember, problemPositions,
                (anaphorPosition, problemPosition) -> logInfo("Ignoring problem position " + problemPosition
                        + " overlapping with anaphor position " + anaphorPosition + " while re-resolving anaphors"));
    }

    /**
     * Discards problem positions that overlap with positions from members.
     * Positions from members whose {@link Position.isDeleted} is {@literal true},
     * are not considered.
     *
     * @param overlapWarner
     *            is invoked whenever a problem position is discarded to support
     *            e.g. logging
     */
    static List<KnownProblemPosition> ignoreProblemPositionsThatOverlapWithPositionsFromMember(
            final List<PositionForAnaphor> positionsFromMember,
            final List<KnownProblemPosition> problemPositions,
            final BiConsumer<PositionForAnaphor, KnownProblemPosition> overlapWarner) {
        return problemPositions
                .stream()
                .filter(problemPosition -> !overlaps(positionsFromMember, problemPosition, overlapWarner))
                .collect(toList());
    }

    static boolean overlaps(final List<PositionForAnaphor> positionsFromMember,
            final KnownProblemPosition problemPosition,
            final BiConsumer<PositionForAnaphor, KnownProblemPosition> overlapWarner) {
        return positionsFromMember
                .stream()
                .anyMatch(anaphorPosition -> overlaps(anaphorPosition, problemPosition, overlapWarner));
    }

    static boolean overlaps(final PositionForAnaphor anaphorPosition,
            final KnownProblemPosition problemPosition,
            final BiConsumer<PositionForAnaphor, KnownProblemPosition> overlapWarner) {
        if (!anaphorPosition.isDeleted
                && overlap(anaphorPosition, problemPosition)) {
            overlapWarner.accept(anaphorPosition, problemPosition);
            return true;
        }
        return false;
    }

    static boolean overlap(final PositionForAnaphor anaphorPosition, final KnownProblemPosition problemPosition) {
        return anaphorPosition.overlapsWith(problemPosition.offset, problemPosition.length);
    }

}
