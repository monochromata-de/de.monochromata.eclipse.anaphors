package de.monochromata.eclipse.anaphors.preferences;

import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.ADD_FINAL_MODIFIER_TO_CREATED_TEMPORARY_VARIABLES;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.MARK_RESOLUTION_OCCURRENCES;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.PERSPECTIVATION_STRATEGY;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.PERSPECTIVATION_STRATEGY_EVERYTHING;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.PERSPECTIVATION_STRATEGY_NOTHING;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.PERSPECTIVATION_STRATEGY_RELATED_EXPRESSIONS;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.USE_LOCAL_VARIABLE_TYPE_INFERENCE;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import de.monochromata.eclipse.anaphors.Activator;

public class PreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

    public PreferencePage() {
        super(GRID);
        setPreferenceStore(Activator.getDefault().getPreferenceStore());
        setDescription("Configuration of anaphors in Eclipse");
    }

    @Override
    public void createFieldEditors() {
        createEyeTrackingEditor();
        createFinalModifierEditor();
        createPerspectivationStrategyEditor();
        createResolutionOccurrencesEditor();
        createLocalVariableTypeInferenceEditor();
    }

    protected void createEyeTrackingEditor() {
        // TODO: See
        // https://gitlab.com/monochromata-de/de.monochromata.eclipse.anaphors/issues/214
        // addField(new BooleanFieldEditor(USE_EYETRACKING, "&Whether or not to track
        // eye movements", getFieldEditorParent()));
    }

    protected void createFinalModifierEditor() {
        addField(new BooleanFieldEditor(ADD_FINAL_MODIFIER_TO_CREATED_TEMPORARY_VARIABLES,
                "&Declare generated local variables as final",
                getFieldEditorParent()));
    }

    protected void createPerspectivationStrategyEditor() {
        addField(new ComboFieldEditor(PERSPECTIVATION_STRATEGY,
                "How to present code",
                new String[][] {
                        { "Show full code", PERSPECTIVATION_STRATEGY_NOTHING },
                        { "Shorten related expressions", PERSPECTIVATION_STRATEGY_RELATED_EXPRESSIONS },
                        { "Shorten related expressions and anaphors", PERSPECTIVATION_STRATEGY_EVERYTHING }
                },
                getFieldEditorParent()));
    }

    protected void createResolutionOccurrencesEditor() {
        addField(new BooleanFieldEditor(MARK_RESOLUTION_OCCURRENCES,
                "Mark related expressions and anaphors after resolution",
                getFieldEditorParent()));
    }

    protected void createLocalVariableTypeInferenceEditor() {
        addField(new BooleanFieldEditor(USE_LOCAL_VARIABLE_TYPE_INFERENCE,
                "Use local variable type inference for Java >= 10 projects",
                getFieldEditorParent()));
    }

    @Override
    public void init(final IWorkbench workbench) {
    }

}