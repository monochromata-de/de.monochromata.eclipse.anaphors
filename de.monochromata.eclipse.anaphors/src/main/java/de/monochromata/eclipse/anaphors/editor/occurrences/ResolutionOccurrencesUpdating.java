package de.monochromata.eclipse.anaphors.editor.occurrences;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.eclipse.jdt.internal.core.manipulation.search.IOccurrencesFinder.F_WRITE_OCCURRENCE;

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Stream;

import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.internal.core.manipulation.search.IOccurrencesFinder.OccurrenceLocation;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.text.source.IAnnotationModelExtension;

import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;

/**
 * Based on {@code JavaEditor.OccurrencesFinderJob}
 */
@SuppressWarnings("restriction")
public interface ResolutionOccurrencesUpdating {

    int SECONDS_TO_DISPLAY_RESOLUTION_OCCURRENCES = 3;

    /**
     * @return The annotations present after the update.
     */
    static Annotation[] updateResolutionOccurrences(final Object annotationModelLock,
            final IAnnotationModel annotationModel,
            final Annotation[] existingAnnotations,
            final List<? extends AbstractAnaphoraPositionForRepresentation> addedOrUpdatedAnaphoraPositions,
            final Runnable resetExistingAnnotations) {
        return updateResolutionOccurrences(annotationModelLock, annotationModel, existingAnnotations,
                addedOrUpdatedAnaphoraPositions, resetExistingAnnotations, Job::schedule);
    }

    /**
     * @param resetExistingAnnotations
     * @return The annotations present after the update.
     */
    static Annotation[] updateResolutionOccurrences(final Object annotationModelLock,
            final IAnnotationModel annotationModel,
            final Annotation[] existingAnnotations,
            final List<? extends AbstractAnaphoraPositionForRepresentation> addedOrUpdatedAnaphoraPositions,
            final Runnable resetExistingAnnotations,
            final BiConsumer<RemoveResolutionOccurrencesJob, Long> jobScheduler) {
        if (addedOrUpdatedAnaphoraPositions.isEmpty()) {
            return existingAnnotations;
        }
        final List<? extends AbstractAnaphoraPositionForRepresentation> distinctAnaphoraPositions = addedOrUpdatedAnaphoraPositions
                .stream()
                .distinct()
                .collect(toList());
        final Stream<Entry<Annotation, Position>> annotationsAndPositions = createResolutionOccurrences(
                distinctAnaphoraPositions);
        final Annotation[] addedAnnotations = updateResolutionOccurrences(existingAnnotations, annotationsAndPositions,
                annotationModelLock,
                annotationModel);
        scheduleFading(addedAnnotations, annotationModelLock, annotationModel, resetExistingAnnotations, jobScheduler);
        return addedAnnotations;
    }

    static void scheduleFading(final Annotation[] annotationsToFade,
            final Object annotationModelLock,
            final IAnnotationModel annotationModel,
            final Runnable resetExistingAnnotations,
            final BiConsumer<RemoveResolutionOccurrencesJob, Long> jobScheduler) {
        final RemoveResolutionOccurrencesJob job = new RemoveResolutionOccurrencesJob(annotationsToFade,
                annotationModelLock, annotationModel,
                resetExistingAnnotations);
        jobScheduler.accept(job, SECONDS_TO_DISPLAY_RESOLUTION_OCCURRENCES * 1000l);
    }

    static Annotation[] updateResolutionOccurrences(final Annotation[] annotationsToRemove,
            final Stream<Entry<Annotation, Position>> annotationsToAdd, final Object annotationModelLock,
            final IAnnotationModel annotationModel) {
        if (annotationModel instanceof IAnnotationModelExtension) {
            return updateResolutionOccurrences0(annotationsToAdd, annotationModelLock, annotationsToRemove,
                    (IAnnotationModelExtension) annotationModel);
        } else {
            return updateResolutionOccurrences0(annotationsToAdd, annotationModelLock, annotationModel,
                    annotationsToRemove);
        }
    }

    static Annotation[] updateResolutionOccurrences0(final Stream<Entry<Annotation, Position>> annotationsAndPositions,
            final Object annotationModelLock, final Annotation[] annotationsToRemove,
            final IAnnotationModelExtension annotationModel) {
        // Note that it is ok to synchronize on the annotation model only and not on the
        // array of existing resolution occurrences. With the current implementation,
        // the following inconsistencies may occur:
        // (1) The existing resolution occurrences to be removed might have been removed
        // already, which is fine and has no effect.
        // (2) Other resolution occurrences than the one held here might have been added
        // in the meantime, which is fine too and will leave those other occurrences
        // unaffected.
        synchronized (annotationModelLock) {
            final Map<Annotation, Position> annotationsToAdd = annotationsAndPositions
                    .collect(toMap(Entry::getKey, Entry::getValue));
            annotationModel.replaceAnnotations(annotationsToRemove, annotationsToAdd);
            return annotationsToAdd.keySet().stream().toArray(Annotation[]::new);
        }
    }

    static Annotation[] updateResolutionOccurrences0(final Stream<Entry<Annotation, Position>> annotationsAndPositions,
            final Object annotationModelLock, final IAnnotationModel annotationModel,
            final Annotation[] annotationsToRemove) {
        // Note that it is ok to synchronize on the annotation model only and not on the
        // array of existing resolution occurrences. With the current implementation,
        // the following inconsistencies may occur:
        // (1) The existing resolution occurrences to be removed might have been removed
        // already, which is fine and has no effect.
        // (2) Other resolution occurrences than the one held here might have been added
        // in the meantime, which is fine too and will leave those other occurrences
        // unaffected.
        synchronized (annotationModelLock) {
            stream(annotationsToRemove).forEach(annotationModel::removeAnnotation);
            return annotationsAndPositions
                    .peek(annotationAndPosition -> annotationModel.addAnnotation(annotationAndPosition.getKey(),
                            annotationAndPosition.getValue()))
                    .toArray(Annotation[]::new);
        }
    }

    static Stream<Entry<Annotation, Position>> createResolutionOccurrences(
            final List<? extends AbstractAnaphoraPositionForRepresentation> addedOrUpdatedAnaphoraPositions) {
        return addedOrUpdatedAnaphoraPositions.stream()
                .map(AnaphoraOccurrenceLocationCreation::createOccurrenceLocation)
                .map(toAnnotationAndPosition());
    }

    static Function<OccurrenceLocation, Entry<Annotation, Position>> toAnnotationAndPosition() {
        return location -> new SimpleImmutableEntry<>(
                new Annotation(getAnnotationType(location), false, location.getDescription()),
                new Position(location.getOffset(), location.getLength()));

    }

    static String getAnnotationType(final OccurrenceLocation location) {
        return (location.getFlags() == F_WRITE_OCCURRENCE)
                ? "org.eclipse.jdt.ui.occurrences.write" //$NON-NLS-1$
                : "org.eclipse.jdt.ui.occurrences"; //$NON-NLS-1$
    }

}
