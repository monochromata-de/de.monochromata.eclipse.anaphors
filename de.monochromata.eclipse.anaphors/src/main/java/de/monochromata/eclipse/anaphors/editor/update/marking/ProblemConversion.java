package de.monochromata.eclipse.anaphors.editor.update.marking;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.compiler.IProblem;

public interface ProblemConversion {

    static Pair<Integer, Integer> toOffsetAndLength(final IProblem problem) {
        final int offset = problem.getSourceStart();
        final int length = problem.getSourceEnd() - problem.getSourceStart() + 1;
        return new ImmutablePair<>(offset, length);
    }

}
