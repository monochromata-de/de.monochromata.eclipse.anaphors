package de.monochromata.eclipse.anaphors.position;

import static java.util.Objects.requireNonNull;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.persp.PerspectivationPosition;

public class PositionForAnaphor extends AbstractAnaphoraPositionForRepresentation {

	public final Anaphora anaphora;

	/**
	 * For testing: creates a position with a perspectivation position at the same
	 * position, without perspectivations.
	 */
	public PositionForAnaphor(final int offset, final int length, final Anaphora anaphora) {
		this(offset, length, new PerspectivationPosition(offset, length), anaphora);
	}

	public PositionForAnaphor(final int offset, final int length, final PerspectivationPosition perspectivationPosition,
			final Anaphora anaphora) {
		super(offset, length, perspectivationPosition);
		this.anaphora = requireNonNull(anaphora);
	}

	public PositionForAnaphor(final int offset, final PerspectivationPosition perspectivationPosition,
			final Anaphora anaphora) {
		super(offset, perspectivationPosition);
		this.anaphora = requireNonNull(anaphora);
	}

	public PositionForAnaphor(final PerspectivationPosition perspectivationPosition, final Anaphora anaphora) {
		super(perspectivationPosition);
		this.anaphora = requireNonNull(anaphora);
	}

	public PositionForRelatedExpression getRelatedExpressionPosition() {
		return (PositionForRelatedExpression) getPrevious();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((anaphora == null) ? 0 : anaphora.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final PositionForAnaphor other = (PositionForAnaphor) obj;
		if (anaphora == null) {
			if (other.anaphora != null) {
				return false;
			}
		} else if (!anaphora.equals(other.anaphora)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "PositionForAnaphor [anaphora=" + anaphora + ", offset=" + offset + ", length=" + length + ", isDeleted="
				+ isDeleted + "]";
	}

}
