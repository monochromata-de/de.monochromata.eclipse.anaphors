package de.monochromata.eclipse.anaphors.swt;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;

/**
 * TODO: Move to de.mochromata.eclipse.swt and remove duplicate in
 * de.monochromata.eclipse.eyetracking.swt
 */
public interface SWTAccess {

    static <T> T supplySync(final Control control, final Supplier<T> supplier) {
        return supplySync(control.getDisplay(), supplier);
    }

    static <T> T supplySync(final Display display, final Supplier<T> supplier) {
        final AtomicReference<T> result = new AtomicReference<>();
        display.syncExec(() -> result.set(supplier.get()));
        return result.get();
    }

    static <T> void consumerSync(final T value, final Control control, final Consumer<T> consumer) {
        consumeSync(value, control.getDisplay(), consumer);
    }

    static <T> void consumeSync(final T value, final Display display, final Consumer<T> consumer) {
        display.syncExec(() -> consumer.accept(value));
    }

    static void runSync(final Control control, final Runnable runnable) {
        runSync(control.getDisplay(), runnable);
    }

    static void runSync(final Display display, final Runnable runnable) {
        display.syncExec(() -> runnable.run());
    }

}
