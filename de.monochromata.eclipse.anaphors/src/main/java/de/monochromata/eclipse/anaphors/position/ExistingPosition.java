package de.monochromata.eclipse.anaphors.position;

import static java.util.Optional.empty;
import static java.util.function.Predicate.not;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.eclipse.anaphors.MultipleOriginalAnaphoras;
import de.monochromata.eclipse.anaphors.OriginalAnaphora;
import de.monochromata.eclipse.anaphors.SingleOriginalAnaphora;

public interface ExistingPosition {

	/**
	 * Note that the decision whether or not to use the existing position is not
	 * based on the document (that contains positions) but on the list of positions
	 * to be removed. This is relevant during updates that are implemented as
	 * removal + (re-)addition. Updates are computed without actually modifying the
	 * document, so the (re-)addition needs to check whether the previously computed
	 * but not yet performed removal removed the position.
	 */
	static <P> Predicate<P> isExistingPositionToBeRetained(final List<? super P> positionsToRemove) {
		return not(positionsToRemove::contains);
	}

	static <P> P getExistingPosition(
			final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart,
			final OriginalAnaphora originalAnaphora,
			final Function<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>, P> getPosition,
			final Predicate<P> isExistingPositionToBeRetained) {
		if (originalAnaphora instanceof MultipleOriginalAnaphoras) {
			return getExistingPosition(anaphorPart, (MultipleOriginalAnaphoras) originalAnaphora, getPosition,
					isExistingPositionToBeRetained);
		} else if (originalAnaphora instanceof SingleOriginalAnaphora) {
			return getExistingPosition((SingleOriginalAnaphora) originalAnaphora, getPosition,
					isExistingPositionToBeRetained);
		} else {
			throw new IllegalArgumentException("Unknown subtype of " + OriginalAnaphora.class.getSimpleName() + ": "
					+ originalAnaphora.getClass().getName());
		}
	}

	private static <P> P getExistingPosition(
			final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart,
			final MultipleOriginalAnaphoras originalAnaphoras,
			final Function<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>, P> getPosition,
			final Predicate<P> isExistingPositionToBeRetained) {
		final var optionalOriginalAnaphora = getOptionalOriginalAnaphora(anaphorPart, originalAnaphoras);
		return getExistingPosition(optionalOriginalAnaphora, getPosition, isExistingPositionToBeRetained);
	}

	private static Optional<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>> getOptionalOriginalAnaphora(
			final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart,
			final MultipleOriginalAnaphoras originalAnaphoras) {
		return originalAnaphoras.originalAnaphoraByAnaphorPart.getOrDefault(anaphorPart, empty());
	}

	private static <P> P getExistingPosition(final SingleOriginalAnaphora originalAnaphora,
			final Function<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>, P> getPosition,
			final Predicate<P> isExistingPositionToBeRetained) {
		return getExistingPosition(originalAnaphora.originalAnaphora, getPosition, isExistingPositionToBeRetained);
	}

	private static <P> P getExistingPosition(
			final Optional<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>> originalAnaphora,
			final Function<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>, P> getPosition,
			final Predicate<P> isExistingPositionToBeRetained) {
		return originalAnaphora.map(getPosition).filter(isExistingPositionToBeRetained::test).orElse(null);
	}

}
