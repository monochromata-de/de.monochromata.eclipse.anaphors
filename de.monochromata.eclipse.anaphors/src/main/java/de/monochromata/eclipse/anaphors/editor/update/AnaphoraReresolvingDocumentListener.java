package de.monochromata.eclipse.anaphors.editor.update;

import static de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent.TriggeredByUpcomingDocumentChange;
import static java.util.Optional.empty;

import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.ElementChangedEvent;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IElementChangedListener;
import org.eclipse.jdt.core.IMember;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentListener;

import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.editor.update.job.ResolveAnaphorsJob;
import de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent;

/**
 * This listener is used to collect {@link IMember}s that are affected by edits
 * about-to-be-performed and that in turn removes all anaphora inside these
 * members so they can be re-resolved later.
 * <p>
 * This class only collects top-level members. E.g. if a compilation unit
 * contains a type with a method and that method contains an anonymous inner
 * class with an instance initializer, only the method but not the contained
 * instance initializer will be reported by this class because it is assumed
 * that only the source ranges obtained from the members are used to find
 * anaphors. The source ranges can be updated, though, which is why members are
 * collected during the events and source ranges should be obtained from the
 * members only shortly before they are needed.
 * <p>
 * This class implements {@link IDocumentListener} instead of
 * {@link IElementChangedListener} for {@link ElementChangedEvent#POST_CHANGE}
 * because the latter does not provide information on all changes within a
 * member's body and the event is only sent on saving but while the user is
 * typing.
 */
public class AnaphoraReresolvingDocumentListener implements IDocumentListener {

    protected final Supplier<ICompilationUnit> compilationUnitSupplier;
    protected final BiConsumer<Pair<Integer, Integer>, IDocument> reResolutionTrigger;
    protected final BiConsumer<ResolveAnaphorsJobEvent, Pair<Optional<ResolveAnaphorsJob>, Optional<Exception>>> resolveAnaphorsJobListener;
    protected boolean enabled = true;

    public AnaphoraReresolvingDocumentListener(final CognitiveEditor editor,
            final BiConsumer<Pair<Integer, Integer>, IDocument> reResolutionTrigger,
            final BiConsumer<ResolveAnaphorsJobEvent, Pair<Optional<ResolveAnaphorsJob>, Optional<Exception>>> resolveAnaphorsJobListener) {
        this(() -> (ICompilationUnit) JavaUI.getEditorInputTypeRoot(editor.getEditorInput()),
                reResolutionTrigger, resolveAnaphorsJobListener);
    }

    public AnaphoraReresolvingDocumentListener(final Supplier<ICompilationUnit> compilationUnitSupplier,
            final BiConsumer<Pair<Integer, Integer>, IDocument> reResolutionTrigger,
            final BiConsumer<ResolveAnaphorsJobEvent, Pair<Optional<ResolveAnaphorsJob>, Optional<Exception>>> resolveAnaphorsJobListener) {
        this.compilationUnitSupplier = compilationUnitSupplier;
        this.reResolutionTrigger = reResolutionTrigger;
        this.resolveAnaphorsJobListener = resolveAnaphorsJobListener;
    }

    /**
     * @return {@literal true} unless {@link #setEnabled(false)} has been invoked.
     */
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public void documentAboutToBeChanged(final DocumentEvent event) {
        // Ignored
    }

    @Override
    public void documentChanged(final DocumentEvent event) {
        if (isEnabled()) {
            final ImmutablePair<Integer, Integer> offsetAndLength = new ImmutablePair<>(event.fOffset, event.fLength);
            resolveAnaphorsJobListener.accept(TriggeredByUpcomingDocumentChange,
                    new ImmutablePair<>(empty(), empty()));
            reResolutionTrigger.accept(offsetAndLength, event.getDocument());
        }
    }

}
