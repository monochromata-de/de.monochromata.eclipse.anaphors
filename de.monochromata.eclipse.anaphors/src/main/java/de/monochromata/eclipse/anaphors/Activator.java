package de.monochromata.eclipse.anaphors;

// TODO: See https://gitlab.com/monochromata-de/de.monochromata.eclipse.anaphors/issues/214
// import static de.monochromata.eclipse.eyetracking.swt.SWTAccess.supplySync;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.MARK_RESOLUTION_OCCURRENCES;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.PERSPECTIVATION_STRATEGY;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.USE_EYETRACKING;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.USE_LOCAL_VARIABLE_TYPE_INFERENCE;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.AstParsing;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphoraResolution;
import org.eclipse.jdt.core.dom.anaphors.PublicAstFunctions;
import org.eclipse.jdt.core.dom.anaphors.PublicDom;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpressions;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpressionsCollector;
import org.eclipse.jdt.core.dom.anaphors.PublicTransformations;
import org.eclipse.jdt.internal.ui.JavaPluginImages;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import de.monochromata.anaphors.ast.ASTBasedAnaphorResolution;
import de.monochromata.anaphors.ast.feature.FeatureContainer;
import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy;
import de.monochromata.anaphors.ast.reference.strategy.concept.FauxHyponymy;
import de.monochromata.anaphors.ast.reference.strategy.concept.Hyponymy;
import de.monochromata.anaphors.ast.reference.strategy.concept.NameRecurrence;
import de.monochromata.anaphors.ast.reference.strategy.concept.TypeRecurrence;
import de.monochromata.anaphors.ast.reference.strategy.feature.FeatureRecurrence;
import de.monochromata.anaphors.ast.relatedexp.strategy.ClassInstanceCreationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalVariableDeclarationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.MethodInvocationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.ParameterDeclarationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.anaphors.ast.spi.AnaphorsSpi;
import de.monochromata.anaphors.ast.spi.TransformationsSpi;
import de.monochromata.anaphors.ast.strategy.AnaphorResolutionStrategy;
import de.monochromata.anaphors.ast.strategy.DA1ReStrategy;
import de.monochromata.anaphors.ast.strategy.IA1MrStrategy;
import de.monochromata.anaphors.ast.strategy.IA2FStrategy;
import de.monochromata.anaphors.ast.strategy.IA2MgStrategy;
import de.monochromata.anaphors.cog.ActivationBasedAnaphorResolution;
import de.monochromata.anaphors.cog.spi.CogSpi;
import de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy;
// TODO: See https://gitlab.com/monochromata-de/de.monochromata.eclipse.anaphors/issues/214
// import de.monochromata.eclipse.anaphors.cog.EyeTrackingBasedMemoryAccess;
import de.monochromata.eclipse.anaphors.preferences.AnaphorsPreferences;
import de.monochromata.eclipse.anaphors.preferences.PerspectivationStrategyPreference;
import de.monochromata.eclipse.persp.PerspectivationPosition;
// TODO: See https://gitlab.com/monochromata-de/de.monochromata.eclipse.anaphors/issues/214
// import de.monochromata.eyetracking.EyeTracker;

public class Activator extends AbstractUIPlugin {

    public static final String PLUGIN_ID = "de.monochromata.eclipse.anaphors";
    public static final String INDEX_OF_ANAPHORS_IN_EDITOR_PREFERENCES_NODE_PREFIX = PLUGIN_ID
            + ".indexOfAnaphorsInEditor";
    public static final String ANAPHORA_DETAILS_PREFERENCE_NODE_PREFIX = PLUGIN_ID + ".anaphoraDetails";
    private static BundleContext context;
    private static Activator instance;

    private PublicDom anaphorsSpi;
    private PublicRelatedExpressions relatedExpressionsSpi;
    private PublicAnaphoraResolution anaphoraResolutionSpi;
    private AnaphorsPreferences preferencesSpi;
    private PublicTransformations transformationsSpi;
    private ASTBasedAnaphorResolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorResolution;
    private ActivationBasedAnaphorResolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> activationBasedResolution;
    private boolean activationBasedResolutionIsEnabled = false;
    private PerspectivationStrategy perspectivationStrategy;
    private boolean markResolutionOccurrences = true;
    private boolean useLocalVariableTypeInference = true;
    // TOODO: See
    // https://gitlab.com/monochromata-de/de.monochromata.eclipse.anaphors/issues/214
    // private EyeTrackingBasedMemoryAccess memoryAccess;

    private final IPropertyChangeListener propertyChangeListener = event -> {
        final String property = event.getProperty();
        if (property.equals(USE_EYETRACKING)) {
            enableActivationBasedAnaphorResolution((Boolean) event.getNewValue());
        } else if (property.equals(PERSPECTIVATION_STRATEGY)) {
            setPerspectivationStrategy((String) event.getNewValue());
        } else if (property.equals(MARK_RESOLUTION_OCCURRENCES)) {
            markResolutionOccurrences = ((Boolean) event.getNewValue());
        } else if (property.equals(USE_LOCAL_VARIABLE_TYPE_INFERENCE)) {
            useLocalVariableTypeInference = ((Boolean) event.getNewValue());
        }
    };
    private ParameterDeclarationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression> pdStrategy;
    private LocalVariableDeclarationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression> lvdStrategy;
    private ClassInstanceCreationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression, PublicAnaphora> cicStrategy;
    private MethodInvocationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression, PublicAnaphora> miStrategy;
    private List<RelatedExpressionStrategy<ASTNode, Type, IBinding, ITypeBinding, CompilationUnit, String, PublicRelatedExpression>> relatedExpressionStrategies;
    private List<AnaphorResolutionStrategy<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>> resolutionStrategies;
    private List<ReferentializationStrategy<Expression, ITypeBinding, CompilationUnit, String, String>> referentializationStrategies;

    static BundleContext getContext() {
        return context;
    }

    public static ImageDescriptor createImageDescriptor(final IPath path) {
        return JavaPluginImages.createImageDescriptor(context.getBundle(), path, true);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.
     * BundleContext)
     */
    @Override
    public void start(final BundleContext bundleContext) throws Exception {
        Activator.context = bundleContext;
        instance = this;
        anaphorsSpi = new PublicDom();
        relatedExpressionsSpi = new PublicRelatedExpressions();
        anaphoraResolutionSpi = new PublicAnaphoraResolution();
        preferencesSpi = new AnaphorsPreferences(getPreferenceStore());
        transformationsSpi = createTransformationsSpi();
        configureJdtCoreAdapter();
        configureActivationBasedAnaphorResolution();
        configureResolutionOccurrences();
        configureLocalVariableTypeInference();
    }

    protected void configureJdtCoreAdapter() {
        // TODO: Demo code that acts as a starting point for a version bridge to
        // react differently to different versions of jdt core.
        // TODO: Use PackageAdmin or a method that returns all bundles/for a
        // specific version
        // TODO: Disable the functionality of this plugin, if no known version
        // of the jdt.* bundles is available.
        final Bundle jdtCoreBundle = Platform.getBundle("org.eclipse.jdt.core");
        System.err.println("org.eclispe.jdt.core=" + jdtCoreBundle);
        System.err.println("org.eclipse.jdt.core.version=" + jdtCoreBundle.getVersion());
    }

    protected void configureActivationBasedAnaphorResolution() {
        getPreferenceStore().addPropertyChangeListener(propertyChangeListener);
        enableActivationBasedAnaphorResolution(getPreferenceStore().getBoolean(USE_EYETRACKING));
        setPerspectivationStrategy(getPreferenceStore().getString(PERSPECTIVATION_STRATEGY));
    }

    protected void configureResolutionOccurrences() {
        markResolutionOccurrences = getPreferenceStore().getBoolean(MARK_RESOLUTION_OCCURRENCES);
    }

    protected void configureLocalVariableTypeInference() {
        useLocalVariableTypeInference = getPreferenceStore().getBoolean(USE_LOCAL_VARIABLE_TYPE_INFERENCE);
    }

    protected void enableActivationBasedAnaphorResolution(final boolean enable) {
        if (!activationBasedResolutionIsEnabled && enable) {
            enableActivationBasedResolution();
        } else if (activationBasedResolutionIsEnabled && !enable) {
            disableActivationBasedResolution();
        }
    }

    protected synchronized void enableActivationBasedResolution() {
        // TODO: See
        // https://gitlab.com/monochromata-de/de.monochromata.eclipse.anaphors/issues/214
        // final EyeTracker eyeTracker = getEyeTracker();
        activationBasedResolutionIsEnabled = true;
        // TODO: See
        // https://gitlab.com/monochromata-de/de.monochromata.eclipse.anaphors/issues/214
        // memoryAccess = supplySync(PlatformUI.getWorkbench().getDisplay(),
        // () -> new EyeTrackingBasedMemoryAccess(eyeTracker, getShell(),
        // PlatformUI.getWorkbench()));
        // CogSpi.setMemory(memoryAccess.getMemory());
    }

    protected synchronized void disableActivationBasedResolution() {
        try {
            // TODO: See
            // https://gitlab.com/monochromata-de/de.monochromata.eclipse.anaphors/issues/214
            // final EyeTrackingBasedMemoryAccess accessToClose = memoryAccess;
            activationBasedResolution = null;
            activationBasedResolutionIsEnabled = false;
            CogSpi.setMemory(null);
            // TODO: See
            // https://gitlab.com/monochromata-de/de.monochromata.eclipse.anaphors/issues/214
            // accessToClose.close();
        } catch (final Exception e) {
            StatusManager.getManager()
                    .handle(new Status(Status.ERROR, PLUGIN_ID, "Failed to diable activation-based resolution", e));
        }
    }

    // TODO: See
    // https://gitlab.com/monochromata-de/de.monochromata.eclipse.anaphors/issues/214
    // private EyeTracker getEyeTracker() {
    // return
    // de.monochromata.eclipse.eyetracking.EyeTracking.getDefault().getEyeTracker();
    // }

    // TODO: See
    // https://gitlab.com/monochromata-de/de.monochromata.eclipse.anaphors/issues/214
    // private Shell getShell() {
    // return supplySync(PlatformUI.getWorkbench().getDisplay(), () -> {
    // final IWorkbenchWindow activeWindow =
    // PlatformUI.getWorkbench().getActiveWorkbenchWindow();
    // if (activeWindow == null) {
    // return null;
    // }
    // return activeWindow.getShell();
    // });
    // }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
     */
    @Override
    public void stop(final BundleContext bundleContext) throws Exception {
        Activator.context = null;
    }

    public static Activator getDefault() {
        return instance;
    }

    public PublicDom getAnaphorsSpi() {
        return anaphorsSpi;
    }

    public PublicRelatedExpressions getRelatedExpressionsSpi() {
        return relatedExpressionsSpi;
    }

    public PublicAnaphoraResolution getAnaphoraResolutionSpi() {
        return anaphoraResolutionSpi;
    }

    public AnaphorsPreferences getPreferencesSpi() {
        return preferencesSpi;
    }

    public TransformationsSpi<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> getTransformationsSpi() {
        return transformationsSpi;
    }

    public ASTBasedAnaphorResolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> getAnaphorResolution() {
        if (anaphorResolution == null) {
            anaphorResolution = createASTBasedAnaphorResolution();
        }
        return anaphorResolution;
    }

    protected ASTBasedAnaphorResolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> createASTBasedAnaphorResolution() {
        return new ASTBasedAnaphorResolution<>(getRelatedExpressionStrategies(), getAnaphorResolutionStrategies(),
                getReferentializationStrategies(), this::createRelatedExpressionsCollector);
    }

    public List<RelatedExpressionStrategy<ASTNode, Type, IBinding, ITypeBinding, CompilationUnit, String, PublicRelatedExpression>> getRelatedExpressionStrategies() {
        if (relatedExpressionStrategies == null) {
            // TODO: Add pdStrategy
            relatedExpressionStrategies = Arrays.asList(getLocalVariableDeclarationStrategy(), getClassInstanceCreationStrategy(), getMethodInvocationStrategy());
        }
        return relatedExpressionStrategies;
    }

    public ParameterDeclarationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression> getParameterDeclarationStrategy() {
        if (pdStrategy == null) {
            pdStrategy = new ParameterDeclarationStrategy<>(relatedExpressionsSpi, preferencesSpi);
        }
        return pdStrategy;
    }

    public LocalVariableDeclarationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression> getLocalVariableDeclarationStrategy() {
        if (lvdStrategy == null) {
            lvdStrategy = new LocalVariableDeclarationStrategy<>(relatedExpressionsSpi, preferencesSpi);
        }
        return lvdStrategy;
    }

    public ClassInstanceCreationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression, PublicAnaphora> getClassInstanceCreationStrategy() {
        if (cicStrategy == null) {
            cicStrategy = new ClassInstanceCreationStrategy<>(relatedExpressionsSpi, preferencesSpi);
        }
        return cicStrategy;
    }

    public MethodInvocationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression, PublicAnaphora> getMethodInvocationStrategy() {
        if (miStrategy == null) {
            miStrategy = new MethodInvocationStrategy<>(relatedExpressionsSpi, preferencesSpi);
        }
        return miStrategy;
    }

    private List<AnaphorResolutionStrategy<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>> getAnaphorResolutionStrategies() {
        if (resolutionStrategies == null) {
            resolutionStrategies = Arrays.asList(
                    new DA1ReStrategy<>(anaphorsSpi, relatedExpressionsSpi, anaphoraResolutionSpi),
                    new IA1MrStrategy<>(anaphorsSpi, relatedExpressionsSpi, anaphoraResolutionSpi),
                    new IA2FStrategy<>(anaphorsSpi, relatedExpressionsSpi, anaphoraResolutionSpi),
                    new IA2MgStrategy<>(anaphorsSpi, relatedExpressionsSpi, anaphoraResolutionSpi));
        }
        return resolutionStrategies;
    }

    private List<ReferentializationStrategy<Expression, ITypeBinding, CompilationUnit, String, String>> getReferentializationStrategies() {
        if (referentializationStrategies == null) {
            referentializationStrategies = createReferentializationStrategies();
        }
        return referentializationStrategies;
    }

    protected List<ReferentializationStrategy<Expression, ITypeBinding, CompilationUnit, String, String>> createReferentializationStrategies() {
        final var hyponymy = new HyponymyUnderDevelopment(anaphorsSpi, PublicAstFunctions::getSuperClass,
                PublicAstFunctions::getImplementedInterfaces);
        return Arrays.asList(new NameRecurrence<>(anaphorsSpi), new TypeRecurrence<>(anaphorsSpi), hyponymy,
                new FauxHyponymy<>(anaphorsSpi),
                new FeatureRecurrence<>(new NameRecurrence<>(anaphorsSpi), anaphorsSpi),
                new FeatureRecurrence<>(new TypeRecurrence<>(anaphorsSpi), anaphorsSpi),
                new FeatureRecurrence<>(hyponymy, anaphorsSpi)
        /*
         * TODO: new FeatureRecurrence<Object, E, Object, Object, B, VB, TB, S, I, QI>(
         * new FauxHyponymy<E, VB, TB, S, I, QI>()),
         */);
    }

    public static class HyponymyUnderDevelopment extends
            Hyponymy<ASTNode, Expression, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> {

        public HyponymyUnderDevelopment() {
        }

        public HyponymyUnderDevelopment(final PublicDom anaphorsSpi,
                final Function<ITypeBinding, Optional<ITypeBinding>> getSuperClass,
                final Function<ITypeBinding, List<ITypeBinding>> getImplementedInterfaces) {
            super((AnaphorsSpi) anaphorsSpi, getSuperClass, getImplementedInterfaces);
        }

        public HyponymyUnderDevelopment(final Function<ITypeBinding, Optional<ITypeBinding>> getSuperClass,
                final Function<ITypeBinding, List<ITypeBinding>> getImplementedInterfaces,
                final Function<String, Function<ITypeBinding, Boolean>> nameOfIdentifierEqualsSimpleNameOfTypeBinding,
                final Function<String, Function<ITypeBinding, Boolean>> conceptualTypeInIdentifierEqualsSimpleNameOfTypeBinding) {
            super(getSuperClass, getImplementedInterfaces, nameOfIdentifierEqualsSimpleNameOfTypeBinding,
                    conceptualTypeInIdentifierEqualsSimpleNameOfTypeBinding);
        }

        @Override
        public FeatureContainer<String> getFeaturesRemainingInIdentifierIfItCanReferUsingConceptualType(
                final String idFromDefiniteExpression,
                final Referent<ITypeBinding, CompilationUnit, String, String> potentialReferent,
                final CompilationUnit scope) {
            return null;
        }

    }

    public ActivationBasedAnaphorResolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> getActivationBasedResolution() {
        if (activationBasedResolution == null) {
            activationBasedResolution = createActivationBasedAnaphorResolution();
        }
        return activationBasedResolution;
    }

    protected ActivationBasedAnaphorResolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> createActivationBasedAnaphorResolution() {
        return new ActivationBasedAnaphorResolution<>(getAnaphorResolution(), transformationsSpi);
    }

    protected void setPerspectivationStrategy(final String preferenceValue) {
        setPerspectivationStrategyInternal(PerspectivationStrategyPreference.getByName(preferenceValue));
    }

    public void setPerspectivationStrategy(final PerspectivationStrategy perspectivationStrategy) {
        getPreferenceStore().setValue(PERSPECTIVATION_STRATEGY,
                PerspectivationStrategyPreference.getName(perspectivationStrategy));
        setPerspectivationStrategyInternal(perspectivationStrategy);
    }

    protected void setPerspectivationStrategyInternal(final PerspectivationStrategy perspectivationStrategy) {
        this.perspectivationStrategy = perspectivationStrategy;
    }

    public PerspectivationStrategy getPerspectivationStrategy() {
        return perspectivationStrategy;
    }

    public void enableMarkResolutionOccurrences(final boolean enabled) {
        getPreferenceStore().setValue(MARK_RESOLUTION_OCCURRENCES, enabled);
        markResolutionOccurrences = enabled;
    }

    public boolean markResolutionOccurrences() {
        return markResolutionOccurrences;
    }

    public void enableUseLocalVariableTypeInference(final boolean enabled) {
        getPreferenceStore().setValue(USE_LOCAL_VARIABLE_TYPE_INFERENCE, enabled);
        useLocalVariableTypeInference = enabled;
    }

    public boolean useLocalVariableTypeInference() {
        return useLocalVariableTypeInference;
    }

    protected PublicTransformations createTransformationsSpi() {
        // TODO: Document this limit and maybe test different values empirically
        final int maxDepthOfCallChainAnalysis = 10;
        return new PublicTransformations(maxDepthOfCallChainAnalysis, typeRoot -> AstParsing.parse(typeRoot, true));
    }

    public PublicRelatedExpressionsCollector createRelatedExpressionsCollector() {
        return new PublicRelatedExpressionsCollector(getParameterDeclarationStrategy(), getLocalVariableDeclarationStrategy(), getClassInstanceCreationStrategy(),
                getMethodInvocationStrategy(), getRelatedExpressionsSpi());
    }

}
