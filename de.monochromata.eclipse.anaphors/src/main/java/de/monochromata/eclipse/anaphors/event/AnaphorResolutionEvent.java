package de.monochromata.eclipse.anaphors.event;

import static java.util.Collections.emptyList;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jdt.internal.corext.refactoring.code.ResolveAnaphorsRefactoring;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.event.ConsumerNotifier;

/**
 * An AnaphoraResolutionEvent is used with a {@link ConsumerNotifier} and comes
 * with a {@link List} of {@link Pair}s of {@link Optional}s of
 * {@link Anaphora}s.
 */
public class AnaphorResolutionEvent {

	public enum EventType {
		ABOUT_TO_REALIZE_ANAPHORS, ABOUT_TO_UNDO_ANAPHOR_REALIZATION
	}

	public static final AnaphorResolutionEvent NO_CHANGES = new AnaphorResolutionEvent(emptyList(), emptyList(),
			emptyList(), emptyList());

	/**
	 * The resolved and realized representations of added anaphoras.
	 */
	public final List<Pair<Anaphora, Anaphora>> addedAnaphoras;

	/**
	 * The retained anaphoras (i.e. anaphoras that have been resolved to the same
	 * anaphora they used to have).
	 */
	public final List<Anaphora> retainedAnaphoras;

	/**
	 * The previous realized and the new resolved and realized representation of
	 * updated anaphoras.
	 */
	public final List<Triple<Anaphora, Anaphora, Anaphora>> updatedAnaphoras;

	/**
	 * The realized representations of removed anaphoras.
	 * <p>
	 * Note: when a document edit removes the
	 * {@link AbstractAnaphoraPositionForRepresentation} positions representing the
	 * anaphora, no {@link ResolveAnaphorsRefactoring} will be triggered and no
	 * {@link AnaphorResolutionEvent} will be sent.
	 */
	public final List<Anaphora> removedAnaphoras;

	public AnaphorResolutionEvent(final List<Pair<Anaphora, Anaphora>> addedAnaphoras,
			final List<Anaphora> retainedAnaphoras, final List<Triple<Anaphora, Anaphora, Anaphora>> updatedAnaphoras,
			final List<Anaphora> removedAnaphoras) {
		this.addedAnaphoras = addedAnaphoras;
		this.retainedAnaphoras = retainedAnaphoras;
		this.updatedAnaphoras = updatedAnaphoras;
		this.removedAnaphoras = removedAnaphoras;
	}

	@Override
	public String toString() {
		return "AnaphorResolutionEvent [addedAnaphoras=" + addedAnaphoras + ", retainedAnaphoras=" + retainedAnaphoras
				+ ", updatedAnaphoras=" + updatedAnaphoras + ", removedAnaphoras=" + removedAnaphoras + "]";
	}

}
