package de.monochromata.eclipse.anaphors.position;

import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadPositionCategoryException;

import java.util.List;
import java.util.stream.Stream;

import org.eclipse.jface.text.IDocument;

public interface AnaphoraPositionRemoval {

	static <T extends AbstractAnaphoraPosition> void removePositions(final IDocument document,
			final List<T> positions) {
		removePositions(document, positions.stream());
	}

	static <T extends AbstractAnaphoraPosition> void removePositions(final IDocument document, final Stream<T> stream) {
		stream.forEach(position -> removePosition(document, position));
	}

	static <T extends AbstractAnaphoraPosition> void removePosition(final IDocument document, final T position) {
		wrapBadPositionCategoryException(() -> document.removePosition(ANAPHORA_CATEGORY, position));
	}

}
