package de.monochromata.eclipse.anaphors.editor.update;

import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static de.monochromata.eclipse.position.PositionQuerying.getCoveredPositions;
import static java.util.stream.Collectors.toList;

import java.util.List;

import org.eclipse.jface.text.IDocument;

import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;

public interface AnaphorPositionCollection {

    static List<PositionForAnaphor> getAnaphorPositions(final IDocument originDocument,
            final List<MemberTrackingPosition> trackingPositions) {
        return getCoveredPositions(trackingPositions, ANAPHORA_CATEGORY, originDocument)
                .filter(position -> position instanceof PositionForAnaphor)
                .map(position -> (PositionForAnaphor) position)
                .collect(toList());
    }

}
