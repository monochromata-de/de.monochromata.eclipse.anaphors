package de.monochromata.eclipse.anaphors.editor.update;

import static de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPosition.MEMBER_TRACKING_CATEGORY;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadPositionCategoryException;
import static java.util.Arrays.stream;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.eclipse.jdt.core.IMember;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;

public interface MemberTrackingPositionRemoval {

    static void removeAllMemberTrackingPositions(final IDocument originDocument) {
        getAllMemberTrackingPositions(originDocument)
                .forEach(removeMemberTrackingPosition(originDocument));
    }

    static Optional<MemberTrackingPosition> removeMemberTrackingPosition(final IMember member,
            final IDocument originDocument) {
        return removeMemberTrackingPosition(member, originDocument, unused -> {
        });
    }

    static Optional<MemberTrackingPosition> removeMemberTrackingPosition(final IMember member,
            final IDocument originDocument, final Consumer<MemberTrackingPosition> positionConsumer) {
        final Optional<MemberTrackingPosition> optionalPosition = getMemberTrackingPosition(member, originDocument);
        optionalPosition.ifPresent(removeMemberTrackingPosition(originDocument));
        optionalPosition.ifPresent(positionConsumer::accept);
        return optionalPosition;
    }

    static Optional<MemberTrackingPosition> getMemberTrackingPosition(final IMember member,
            final IDocument originDocument) {
        return getAllMemberTrackingPositions(originDocument)
                .map(position -> (MemberTrackingPosition) position)
                .filter(position -> position.member.equals(member))
                .findFirst();
    }

    static Stream<Position> getAllMemberTrackingPositions(final IDocument originDocument) {
        return stream(wrapBadPositionCategoryException(() -> originDocument.getPositions(MEMBER_TRACKING_CATEGORY)));
    }

    static Consumer<? super Position> removeMemberTrackingPosition(final IDocument originDocument) {
        return position -> wrapBadPositionCategoryException(
                () -> originDocument.removePosition(MEMBER_TRACKING_CATEGORY, position));
    }

}
