/*******************************************************************************
 * Copyright (c) 2000, 2014 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.monochromata.eclipse.anaphors.editor;

import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionPerspectivationEvent.AboutToStartDocumentRewritingForAnaphorResolution;
import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionPerspectivationEvent.CompletedDocumentRewritingForAnaphorResolution;
import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionProgressEvent.AnaphoraAddedOrChanged;
import static de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent.Created;
import static de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent.Failed;
import static de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent.FailedWithBadLocationExceptionWillRetry;
import static de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent.NotCreatedBecauseAnotherJobIsAlreadyRunning;
import static de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent.Started;
import static de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent.Succeeded;
import static de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent.TriggeredAsFollowUpAfterJobWithoutChanges;
import static de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent.TriggeredByElementChange;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.MARK_RESOLUTION_OCCURRENCES;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.PERSPECTIVATION_STRATEGY;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadLocationException;
import static java.lang.System.identityHashCode;
import static java.util.Optional.ofNullable;

import java.lang.reflect.AccessibleObject;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.internal.core.manipulation.search.IOccurrencesFinder.OccurrenceLocation;
import org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitEditor;
import org.eclipse.jdt.internal.ui.javaeditor.JavaEditor;
import org.eclipse.jdt.internal.ui.javaeditor.JavaEditorErrorTickUpdater;
import org.eclipse.jdt.internal.ui.text.ContentAssistPreference;
import org.eclipse.jdt.internal.ui.text.JavaWordFinder;
import org.eclipse.jdt.internal.ui.viewsupport.JavaElementImageProvider;
import org.eclipse.jdt.internal.ui.viewsupport.JavaUILabelProvider;
import org.eclipse.jdt.ui.PreferenceConstants;
import org.eclipse.jdt.ui.ProblemsLabelDecorator;
import org.eclipse.jdt.ui.SharedASTProvider;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentExtension4;
import org.eclipse.jface.text.IDocumentListener;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ISynchronizable;
import org.eclipse.jface.text.ITextInputListener;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.contentassist.ContentAssistant;
import org.eclipse.jface.text.contentassist.IContentAssistant;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.text.source.IAnnotationModelExtension;
import org.eclipse.jface.text.source.IOverviewRuler;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.IVerticalRuler;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWindowListener;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.IDocumentProvider;

import de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy;
import de.monochromata.eclipse.anaphors.Activator;
import de.monochromata.eclipse.anaphors.editor.occurrences.AnaphoraOccurrencesFinder;
import de.monochromata.eclipse.anaphors.editor.occurrences.ResolutionOccurrencesUpdating;
import de.monochromata.eclipse.anaphors.editor.update.AnaphoraReresolvingDocumentListener;
import de.monochromata.eclipse.anaphors.editor.update.AnaphoraResolvingElementListener;
import de.monochromata.eclipse.anaphors.editor.update.job.AnaphorResolutionQueue;
import de.monochromata.eclipse.anaphors.editor.update.job.ResolveAnaphorsJob;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionPerspectivationEvent;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionProgressEvent;
import de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.anaphors.preferences.PerspectivationStrategyPreference;
import de.monochromata.event.ConsumerNotifier;
import de.monochromata.event.Notification;

/**
 * An editor for Java with anaphors.
 *
 * <p>
 * The editor makes internal methods and fields from third-parties accessible
 * via {@link AccessibleObject#setAccessible(boolean)}.
 * </p>
 *
 * <p>
 * Alternatively, Equinox Weaving could be used, which would require a javaagent
 * to be installed, though. Installation would require manipulation of the
 * startup arguments of the JVM hosting the Eclipse IDE or reliance on the Sun
 * Tools API which either does not work out of the box (javaagent) or might not
 * be available on all JVM's (Tools API).
 * <ul>
 * <li><a href="http://dhruba.name/2010/02/07/creation-dynamic-loading-and-
 * instrumentation-with-javaagents/>Creation, dynamic loading and
 * instrumentation with javaagents</a></li>
 * <li><a href="https://www.eclipse.org/equinox/weaving/">Equinox Weaving</a>
 * </li>
 * <li><a href="https://wiki.eclipse.org/Equinox_Weaving_QuickStart">Equinox
 * Weaving Quickstart</a></li>
 * <li><a
 * href="http://martinlippert.blogspot.de/2008/07/dependency-injection-for-
 * extensions.html>Dependency Injection for Extensions, Third Edition</a></li>
 * <li><a href=
 * "https://eclipse.org/aspectj/doc/next/devguide/ltw-configuration.html">
 * Chapter 5. Load-Time Weaving</a></li>
 * </ul>
 * </p>
 */
public class CognitiveEditor extends CompilationUnitEditorCompatibility {

	public static final String COGNITIVE_EDITOR_ID = "de.monochromata.coged4j.editor.CognitiveEditor";

	protected final MemberInvalidationRegistrar memberInvalidationRegistrar = new MemberInvalidationRegistrar();
	protected final AnaphorResolutionQueue anaphorResolutionQueue = new AnaphorResolutionQueue();
	protected AnaphoraReresolvingDocumentListener anaphoraReresolvingListener;
	protected AnaphoraResolvingElementListener anaphoraResolvingListener;
	protected final ConsumerNotifier<AnaphorResolutionPerspectivationEvent, List<? extends Position>> anaphorResolutionPerspectivationNotifier = new ConsumerNotifier<>();
	protected final ConsumerNotifier<AnaphorResolutionProgressEvent, List<? extends AbstractAnaphoraPositionForRepresentation>> anaphorResolutionProgressNotifier = new ConsumerNotifier<>();
	protected final ConsumerNotifier<AnaphorResolutionEvent.EventType, AnaphorResolutionEvent> anaphorResolutionNotifier = new ConsumerNotifier<>();
	protected final ConsumerNotifier<ResolveAnaphorsJobEvent, Pair<Optional<ResolveAnaphorsJob>, Optional<Exception>>> resolveAnaphorsJobNotifier = new ConsumerNotifier<>();
	protected final IPropertyChangeListener activatorPropertyChangeListener = this::handleActivatorPropertyChange;

	// Fields hidden from super-classes

	/**
	 * Listener to annotation model changes that updates the error tick in the tab
	 * image
	 */
	protected ErrorTickUpdater fErrorTickUpdater;

	/**
	 * Holds the current occurrence annotations.
	 *
	 * @since 3.0
	 */
	private Annotation[] fOccurrenceAnnotations = null;

	private boolean markResolutionOccurrences = true;
	private Annotation[] resolutionOccurrenceAnnotations = new Annotation[0];

	/**
	 * Tells whether all occurrences of the element at the current caret location
	 * are automatically marked in this editor.
	 *
	 * @since 3.0
	 */
	private boolean fMarkOccurrenceAnnotations;

	/**
	 * Tells whether the occurrence annotations are sticky i.e. whether they stay
	 * even if there's no valid Java element at the current caret position. Only
	 * valid if {@link #fMarkOccurrenceAnnotations} is <code>true</code>.
	 *
	 * @since 3.0
	 */
	private boolean fStickyOccurrenceAnnotations;
	// private OccurrencesFinderJob fOccurrencesFinderJob;

	/** The occurrences finder job canceler */
	private OccurrencesFinderJobCanceler fOccurrencesFinderJobCanceler;

	/**
	 * The selection used when forcing occurrence marking through code.
	 *
	 * @since 3.0
	 */
	private ISelection fForcedMarkOccurrencesSelection;

	/**
	 * The document modification stamp at the time when the last occurrence marking
	 * took place.
	 *
	 * @since 3.1
	 */
	private long fMarkOccurrenceModificationStamp = IDocumentExtension4.UNKNOWN_MODIFICATION_STAMP;

	/**
	 * The region of the word under the caret used to when computing the current
	 * occurrence markings.
	 *
	 * @since 3.1
	 */
	private IRegion fMarkOccurrenceTargetRegion;

	/**
	 * The internal shell activation listener for updating occurrences.
	 *
	 * @since 3.0
	 */
	private ActivationListener fActivationListener = new ActivationListener();

	public CognitiveEditor() {
		setDocumentProvider(new AnaphoraDocumentProvider(getDocumentProvider(), anaphorasToDelete -> {
		}));
		final BiConsumer<Pair<Integer, Integer>, IDocument> reResolutionTrigger = (offsetAndLength,
				originDocument) -> anaphoraResolvingListener.markAnaphorsForReResolution(offsetAndLength,
						originDocument);
		anaphoraReresolvingListener = new AnaphoraReresolvingDocumentListener(this, reResolutionTrigger,
				resolveAnaphorsJobNotifier::fire);
		anaphoraResolvingListener = new AnaphoraResolvingElementListener(anaphorResolutionQueue,
				anaphorResolutionQueue::hasJobs, this, anaphoraReresolvingListener,
				anaphorResolutionPerspectivationNotifier::fire, anaphorResolutionProgressNotifier::fire,
				anaphorResolutionNotifier::fire, resolveAnaphorsJobNotifier::fire);
		JavaCore.addElementChangedListener(anaphoraResolvingListener);
		fErrorTickUpdater = new ErrorTickUpdater(this);
		registerJobEventLoggingListener();
	}

	protected void registerJobEventLoggingListener() {
		/*
		 * resolveAnaphorsJobNotifier.addListener(TriggeredByUpcomingDocumentChange,
		 * unused ->
		 * System.err.println("Anaphor resolution triggered by document change"));
		 */
		resolveAnaphorsJobNotifier.addListener(TriggeredByElementChange,
				unused -> System.err.println("Anaphor resolution triggered by element change"));
		resolveAnaphorsJobNotifier.addListener(TriggeredAsFollowUpAfterJobWithoutChanges,
				unused -> System.err.println("Anaphor resolution triggered as follow-up after job without changes"));
		resolveAnaphorsJobNotifier.addListener(NotCreatedBecauseAnotherJobIsAlreadyRunning,
				jobAndException -> System.err.println("Another job is already running that needs to finish first."));
		resolveAnaphorsJobNotifier.addListener(Created, jobAndException -> System.err
				.println("Created new job " + identityHashCode(jobAndException.getLeft().get())));
		resolveAnaphorsJobNotifier.addListener(Started, jobAndException -> System.err
				.println("Started  job from problems " + identityHashCode(jobAndException.getLeft().get())));
		resolveAnaphorsJobNotifier.addListener(Succeeded, jobAndException -> System.err
				.println("Finished job from problems " + identityHashCode(jobAndException.getLeft().get())));
		resolveAnaphorsJobNotifier.addListener(FailedWithBadLocationExceptionWillRetry,
				jobAndException -> System.err.println("Failed with bad location exception (will re-try) "
						+ identityHashCode(jobAndException.getLeft())));
		resolveAnaphorsJobNotifier.addListener(Failed, jobAndException -> {
			System.err.println("Failed with exception " + identityHashCode(jobAndException.getLeft().get()));
			jobAndException.getRight().get().printStackTrace();
		});
	}

	public Notification<AnaphorResolutionPerspectivationEvent, Consumer<List<? extends Position>>> getAnaphorResolutionPerspectivationNotifier() {
		return anaphorResolutionPerspectivationNotifier;
	}

	public Notification<AnaphorResolutionProgressEvent, Consumer<List<? extends AbstractAnaphoraPositionForRepresentation>>> getAnaphorResolutionProgressNotifier() {
		return anaphorResolutionProgressNotifier;
	}

	public Notification<AnaphorResolutionEvent.EventType, Consumer<AnaphorResolutionEvent>> getAnaphorResolutionNotifier() {
		return anaphorResolutionNotifier;
	}

	public Notification<ResolveAnaphorsJobEvent, Consumer<Pair<Optional<ResolveAnaphorsJob>, Optional<Exception>>>> getResolveAnaphorsJobNotifier() {
		return resolveAnaphorsJobNotifier;
	}

	@Override
	@SuppressWarnings("restriction")
	public void dispose() {
		JavaCore.removeElementChangedListener(anaphoraResolvingListener);
		memberInvalidationRegistrar.uninstall();
		anaphoraReresolvingListener = null;
		anaphoraResolvingListener = null;
		fMarkOccurrenceAnnotations = false;
		if (fErrorTickUpdater != null) {
			fErrorTickUpdater.dispose();
			fErrorTickUpdater = null;
		}
		super.dispose();
		if (fActivationListener != null) {
			PlatformUI.getWorkbench().removeWindowListener(fActivationListener);
			fActivationListener = null;
		}
	}

	@Override
	protected AnaphorsSourceViewer createJavaSourceViewer(final Composite parent, final IVerticalRuler verticalRuler,
			final IOverviewRuler overviewRuler, final boolean isOverviewRulerVisible, final int styles,
			final IPreferenceStore store) {
		final AnaphorsSourceViewer viewer = new AnaphorsSourceViewer(this, this::getInputJavaElement, parent,
				verticalRuler, overviewRuler, isOverviewRulerVisible, styles, store);
		getAnaphorResolutionPerspectivationNotifier().addListener(AboutToStartDocumentRewritingForAnaphorResolution,
				viewer::initiateSelectionUpdate);
		getAnaphorResolutionPerspectivationNotifier().addListener(CompletedDocumentRewritingForAnaphorResolution,
				viewer::finishSelectionUpdate);
		getAnaphorResolutionProgressNotifier().addListener(AnaphoraAddedOrChanged, this::updateResolutionOccurrences);
		return viewer;
	}

	protected void handleActivatorPropertyChange(final PropertyChangeEvent event) {
		final String property = event.getProperty();
		final Object newValue = event.getNewValue();
		if (PERSPECTIVATION_STRATEGY.equals(property)) {
			updatePerspectivationPositions((String) newValue);
		} else if (MARK_RESOLUTION_OCCURRENCES.equals(property)) {
			markResolutionOccurrences = (Boolean) newValue;
		}
	}

	@SuppressWarnings("restriction")
	@Override
	protected void setPreferenceStore(final IPreferenceStore store) {
		super.setPreferenceStore(store);
		fMarkOccurrenceAnnotations = store.getBoolean(PreferenceConstants.EDITOR_MARK_OCCURRENCES);
		fStickyOccurrenceAnnotations = store.getBoolean(PreferenceConstants.EDITOR_STICKY_OCCURRENCES);
		// TODO: Add a preference key to choose whether or not to mark
		// anaphors etc.?
	}

	// TODO: Need to re-implements protected void
	// handlePreferenceStoreChanged(PropertyChangeEvent event) {
	// that casts to AdaptedSourceViewer
	@SuppressWarnings("restriction")
	@Override
	protected void handlePreferenceStoreChanged(final PropertyChangeEvent event) {
		try {
			final String property = event.getProperty();
			System.err.println("Updated property " + property);
			boolean newBooleanValue = false;
			final Object newValue = event.getNewValue();
			if (newValue != null) {
				newBooleanValue = Boolean.valueOf(newValue.toString()).booleanValue();
			}
			if (PreferenceConstants.EDITOR_MARK_OCCURRENCES.equals(property)) {
				if (newBooleanValue != fMarkOccurrenceAnnotations) {
					fMarkOccurrenceAnnotations = newBooleanValue;
				}
				// Also delegate to the upstream editor
			}
			if (PreferenceConstants.EDITOR_STICKY_OCCURRENCES.equals(property)) {
				fStickyOccurrenceAnnotations = newBooleanValue;
				return;
			}
		} finally {
			handlePreferenceStoreChanged_inlinedFromCompilationUnitEditor(event);
			// TODO: Solve this issue (raising a ClassCastException)
			System.err.println("Not forwarding handlePreferenceStoreChanged(...) to CompilationUnitEditor");
			// super.handlePreferenceStoreChanged(event);
		}
	}

	protected void updatePerspectivationPositions(final String newPerspectivationStrategyName) {
		final IEditorInput input = getEditorInput();
		ofNullable(getDocumentProvider()).map(provider -> provider.getDocument(input)).ifPresent(
				originDocument -> updatePerspectivationPositions(newPerspectivationStrategyName, originDocument));
	}

	protected void updatePerspectivationPositions(final String newPerspectivationStrategyName,
			final IDocument originDocument) {
		final PerspectivationStrategy newStrategy = PerspectivationStrategyPreference
				.getByName(newPerspectivationStrategyName);
		ofNullable(getSourceViewer()).filter(viewer -> viewer instanceof AnaphorsSourceViewer)
				.map(viewer -> (AnaphorsSourceViewer) viewer)
				.ifPresent(viewer -> viewer.updatePerspectivationPositions(originDocument, newStrategy));
	}

	/**
	 * This method works with an {@link AnaphorsSourceViewer}, as well as with a
	 * {@link CompilationUnitEditor.AdaptedSourceViewer}.
	 *
	 * @param event
	 */
	@SuppressWarnings("restriction")
	protected void handlePreferenceStoreChanged_inlinedFromCompilationUnitEditor(final PropertyChangeEvent event) {

		try {

			final SourceViewer sv = (SourceViewer) getSourceViewer();
			if (sv != null) {

				final String p = event.getProperty();
				final Object bracketInserter = get(CompilationUnitEditor_fBracketInserter, this);

				if (getStatic(CompilationUnitEditor_CLOSE_BRACKETS).equals(p)) {
					invoke(CompilationUnitEditor_BracketInserter_setCloseBracketsEnabled, bracketInserter,
							getPreferenceStore().getBoolean(p));
					return;
				}

				if (getStatic(CompilationUnitEditor_CLOSE_STRINGS).equals(p)) {
					invoke(CompilationUnitEditor_BracketInserter_setCloseStringsEnabled, bracketInserter,
							getPreferenceStore().getBoolean(p));
					return;
				}

				if (JavaCore.COMPILER_SOURCE.equals(p)) {
					final boolean closeAngularBrackets = JavaCore.VERSION_1_5
							.compareTo(getPreferenceStore().getString(p)) <= 0;
					invoke(CompilationUnitEditor_BracketInserter_setCloseAngularBracketsEnabled, bracketInserter,
							closeAngularBrackets);
				}

				if (getStatic(CompilationUnitEditor_SPACES_FOR_TABS).equals(p)) {
					if (isTabsToSpacesConversionEnabled()) {
						installTabsToSpacesConverter();
					} else {
						uninstallTabsToSpacesConverter();
					}
					return;
				}

				if (PreferenceConstants.EDITOR_SMART_TAB.equals(p)) {
					if (getPreferenceStore().getBoolean(PreferenceConstants.EDITOR_SMART_TAB)) {
						setActionActivationCode("IndentOnTab", '\t', -1, SWT.NONE); //$NON-NLS-1$
					} else {
						removeActionActivationCode("IndentOnTab"); //$NON-NLS-1$
					}
				}

				final IContentAssistant c = (IContentAssistant) get(SourceViewer_fContentAssistant, sv);
				if (c instanceof ContentAssistant) {
					ContentAssistPreference.changeConfiguration((ContentAssistant) c, getPreferenceStore(), event);
				}

				if (getStatic(CompilationUnitEditor_CODE_FORMATTER_TAB_SIZE).equals(p)
						&& isTabsToSpacesConversionEnabled()) {
					uninstallTabsToSpacesConverter();
					installTabsToSpacesConverter();
				}
			}

		} finally {
			handlePreferenceStoreChanged_inlinedFromJavaEditor(event);
		}
	}

	protected void handlePreferenceStoreChanged_inlinedFromJavaEditor(final PropertyChangeEvent event) {
		// TODO: Inline handlePreferenceStoreChanged(...) from JavaEditor and
		// its super-classes
		System.err.println("TODO: Inline handlePreferenceStoreChanged(...) from JavaEditor and its super-classes");
	}

	protected void updateResolutionOccurrences(
			final List<? extends AbstractAnaphoraPositionForRepresentation> addedOrUpdatedAnaphoraPositions) {
		if (!markResolutionOccurrences) {
			return;
		}

		final IAnnotationModel annotationModel = getDocumentProvider().getAnnotationModel(getEditorInput());
		if (annotationModel == null) {
			return;
		}

		final Object annotationModelLock = getLockObject(annotationModel);

		resolutionOccurrenceAnnotations = ResolutionOccurrencesUpdating.updateResolutionOccurrences(annotationModelLock,
				annotationModel, resolutionOccurrenceAnnotations, addedOrUpdatedAnaphoraPositions,
				() -> resolutionOccurrenceAnnotations = new Annotation[0]);
	}

	/**
	 * Updates the occurrences annotations based on the current selection.
	 *
	 * TODO: Refactor
	 * {@link JavaEditor#updateOccurrenceAnnotations(ITextSelection, org.eclipse.jdt.core.dom.CompilationUnit)}
	 * to avoid duplicating parts of its code in this implementation.
	 *
	 * @param selection the text selection
	 * @param astRoot   the compilation unit AST
	 * @since 3.0
	 */
	@Override
	@SuppressWarnings("restriction")
	protected void updateOccurrenceAnnotations(final ITextSelection selection,
			final org.eclipse.jdt.core.dom.CompilationUnit astRoot) {

		final Job job = (Job) get(JavaEditor_fOccurrencesFinderJob, this);
		if (job != null) {
			job.cancel();
		}

		if (!fMarkOccurrenceAnnotations) {
			return;
		}

		if (astRoot == null || selection == null) {
			return;
		}

		final IDocument document = getDocumentProvider().getDocument(getEditorInput());
		if (document == null) {
			return;
		}

		boolean enabled = true;
		boolean hasChanged = false;
		boolean forwardToSuperclass = false;
		if (document instanceof IDocumentExtension4) {
			final int offset = selection.getOffset();
			final long currentModificationStamp = ((IDocumentExtension4) document).getModificationStamp();
			final IRegion markOccurrenceTargetRegion = fMarkOccurrenceTargetRegion;
			hasChanged = currentModificationStamp != fMarkOccurrenceModificationStamp;
			if (markOccurrenceTargetRegion != null && !hasChanged) {
				if (markOccurrenceTargetRegion.getOffset() <= offset
						&& offset <= markOccurrenceTargetRegion.getOffset() + markOccurrenceTargetRegion.getLength()) {
					enabled = false;
				}
			}
			if (enabled) {
				fMarkOccurrenceTargetRegion = JavaWordFinder.findWord(document, offset);
				fMarkOccurrenceModificationStamp = currentModificationStamp;
			}
		}

		if (enabled) {
			OccurrenceLocation[] locations = null;

			final String word = fMarkOccurrenceTargetRegion == null ? "<anaphora>"
					: wrapBadLocationException(() -> document.get(fMarkOccurrenceTargetRegion.getOffset(),
							fMarkOccurrenceTargetRegion.getLength()));
			final AnaphoraOccurrencesFinder acFinder = new AnaphoraOccurrencesFinder(word, document);
			final String initializationResult = acFinder.initialize(astRoot, selection.getOffset(),
					selection.getLength());
			if (initializationResult == null) {
				locations = acFinder.getOccurrences();
			}

			if (locations == null || locations.length == 0) {
				if (!fStickyOccurrenceAnnotations) {
					removeOccurrenceAnnotations();
				} else {
					// of current annotations
					removeOccurrenceAnnotations();
				}
				forwardToSuperclass = true;
			} else {

				final Object newJob = invoke(JavaEditor_OccurrencesFinderJob, this, document, locations, selection);
				set(JavaEditor_fOccurrencesFinderJob, this, newJob);
				// fOccurrencesFinderJob.setPriority(Job.DECORATE);
				// fOccurrencesFinderJob.setSystem(true);
				// fOccurrencesFinderJob.schedule();
				invoke(JavaEditor_OccurrencesFinderJob_run, newJob, new NullProgressMonitor());
			}
		}
		if (forwardToSuperclass) {
			super.updateOccurrenceAnnotations(selection, astRoot);
		}
	}

	@SuppressWarnings("restriction")
	@Override
	protected void installOccurrencesFinder(final boolean forceUpdate) {
		fMarkOccurrenceAnnotations = true;
		if (forceUpdate && getSelectionProvider() != null) {
			fForcedMarkOccurrencesSelection = getSelectionProvider().getSelection();
		}
		if (fOccurrencesFinderJobCanceler == null) {
			fOccurrencesFinderJobCanceler = new OccurrencesFinderJobCanceler();
			fOccurrencesFinderJobCanceler.install();
		}
		super.installOccurrencesFinder(forceUpdate);
	}

	@SuppressWarnings("restriction")
	@Override
	protected void uninstallOccurrencesFinder() {
		fMarkOccurrenceAnnotations = false;

		if (get(JavaEditor_fOccurrencesFinderJob, this) != null) {
			((Job) get(JavaEditor_fOccurrencesFinderJob, this)).cancel();
			set(JavaEditor_fOccurrencesFinderJob, this, null);
		}

		if (fOccurrencesFinderJobCanceler != null) {
			fOccurrencesFinderJobCanceler.uninstall();
			fOccurrencesFinderJobCanceler = null;
		}

		removeOccurrenceAnnotations();
		super.uninstallOccurrencesFinder();
	}

	// TODO: Do not copy this method anymore, but use an
	// AccessibleObject.setAccessible(...) instead for fOccurrenceAnnotations
	void removeOccurrenceAnnotations() {
		fMarkOccurrenceModificationStamp = IDocumentExtension4.UNKNOWN_MODIFICATION_STAMP;
		fMarkOccurrenceTargetRegion = null;

		final IDocumentProvider documentProvider = getDocumentProvider();
		if (documentProvider == null) {
			return;
		}

		final IAnnotationModel annotationModel = documentProvider.getAnnotationModel(getEditorInput());
		if (annotationModel == null || fOccurrenceAnnotations == null) {
			return;
		}

		synchronized (getLockObject(annotationModel)) {
			if (annotationModel instanceof IAnnotationModelExtension) {
				((IAnnotationModelExtension) annotationModel).replaceAnnotations(fOccurrenceAnnotations, null);
			} else {
				for (final Annotation fOccurrenceAnnotation : fOccurrenceAnnotations) {
					annotationModel.removeAnnotation(fOccurrenceAnnotation);
				}
			}
			fOccurrenceAnnotations = null;
		}
	}

	@SuppressWarnings("restriction")
	@Override
	public void createPartControl(final Composite parent) {
		PlatformUI.getWorkbench().addWindowListener(fActivationListener);
		super.createPartControl(parent);
	}

	@SuppressWarnings("restriction")
	@Override
	protected void doSelectionChanged(final ISelection selection) {
		final ITypeRoot inputJavaElement = getInputJavaElement();
		if (inputJavaElement == null) {
			return;
		}

		final ISelectionProvider selectionProvider = getSelectionProvider();
		if (selectionProvider == null) {
			return;
		}

		final ISelection textSelection = selectionProvider.getSelection();
		if (!(textSelection instanceof ITextSelection)) {
			return;
		}

		final org.eclipse.jdt.core.dom.CompilationUnit ast = SharedASTProvider.getAST(inputJavaElement,
				SharedASTProvider.WAIT_NO /* DO NOT USE WAIT_ACTIVE_ONLY */ , getProgressMonitor());
		if (ast != null) {
			fForcedMarkOccurrencesSelection = textSelection;
		}
		super.doSelectionChanged(selection);
	}

	/**
	 * Internal activation listener.
	 *
	 * @since 3.0
	 */
	// TODO: Based on JavaEditor.ActivationListener
	private class ActivationListener implements IWindowListener {
		@Override
		@SuppressWarnings("restriction")
		public void windowActivated(final IWorkbenchWindow window) {
			if (window == getEditorSite().getWorkbenchWindow() && fMarkOccurrenceAnnotations && isActivePart()) {
				fForcedMarkOccurrencesSelection = getSelectionProvider().getSelection();
				System.err.println(
						"window activated: setting fForcedMarkOccurrencesSelection=" + fForcedMarkOccurrencesSelection);
			}
		}

		@Override
		public void windowDeactivated(final IWorkbenchWindow window) {
		}

		@Override
		public void windowClosed(final IWorkbenchWindow window) {
		}

		@Override
		public void windowOpened(final IWorkbenchWindow window) {
		}
	}

	// TODO: Based on JavaEditor.OccurrencesFinderJob
	/**
	 * Finds and marks occurrence annotations.
	 *
	 * @since 3.0
	 *
	 *        class OccurrencesFinderJob extends Job {
	 *
	 *        private final IDocument fDocument; private final ISelection
	 *        fSelection; private final ISelectionValidator fPostSelectionValidator;
	 *        private boolean fCanceled = false; private final OccurrenceLocation[]
	 *        fLocations;
	 *
	 *        public OccurrencesFinderJob(IDocument document, OccurrenceLocation[]
	 *        locations, ISelection selection) { super( "Anaphors occurrences
	 *        marker"); fDocument = document; fSelection = selection; fLocations =
	 *        locations;
	 *
	 *        if (getSelectionProvider() instanceof ISelectionValidator)
	 *        fPostSelectionValidator = (ISelectionValidator)
	 *        getSelectionProvider(); else fPostSelectionValidator = null; }
	 *
	 *        // cannot use cancel() because it is declared final void doCancel() {
	 *        fCanceled = true; cancel(); }
	 *
	 *        private boolean isCanceled(IProgressMonitor progressMonitor) { return
	 *        fCanceled || progressMonitor.isCanceled() || fPostSelectionValidator
	 *        != null && !(fPostSelectionValidator.isValid(fSelection) ||
	 *        fForcedMarkOccurrencesSelection == fSelection) ||
	 *        LinkedModeModel.hasInstalledModel(fDocument); }
	 *
	 *        /*
	 * @see Job#run(org.eclipse.core.runtime.IProgressMonitor)
	 *
	 * @Override public IStatus run(IProgressMonitor progressMonitor) { if
	 *           (isCanceled(progressMonitor)) return Status.CANCEL_STATUS;
	 *
	 *           ITextViewer textViewer = getViewer(); if (textViewer == null)
	 *           return Status.CANCEL_STATUS;
	 *
	 *           IDocument document = textViewer.getDocument(); if (document ==
	 *           null) return Status.CANCEL_STATUS;
	 *
	 *           IDocumentProvider documentProvider = getDocumentProvider(); if
	 *           (documentProvider == null) return Status.CANCEL_STATUS;
	 *
	 *           IAnnotationModel annotationModel =
	 *           documentProvider.getAnnotationModel(getEditorInput()); if
	 *           (annotationModel == null) return Status.CANCEL_STATUS;
	 *
	 *           // Add occurrence annotations int length = fLocations.length;
	 *           Map<Annotation, Position> annotationMap = new HashMap<Annotation,
	 *           Position>(length); for (int i = 0; i < length; i++) {
	 *
	 *           if (isCanceled(progressMonitor)) return Status.CANCEL_STATUS;
	 *
	 *           OccurrenceLocation location = fLocations[i]; Position position =
	 *           new Position(location.getOffset(), location.getLength());
	 *
	 *           String description = location.getDescription(); String
	 *           annotationType = (location.getFlags() ==
	 *           IOccurrencesFinder.F_WRITE_OCCURRENCE) ?
	 *           "org.eclipse.jdt.ui.occurrences.write" :
	 *           "org.eclipse.jdt.ui.occurrences"; //$NON-NLS-1$ //$NON-NLS-2$
	 *
	 *           annotationMap.put(new Annotation(annotationType, false,
	 *           description), position); }
	 *
	 *           if (isCanceled(progressMonitor)) return Status.CANCEL_STATUS;
	 *
	 *           synchronized (getLockObject(annotationModel)) { if (annotationModel
	 *           instanceof IAnnotationModelExtension) {
	 *           ((IAnnotationModelExtension)
	 *           annotationModel).replaceAnnotations(fOccurrenceAnnotations,
	 *           annotationMap); } else { removeOccurrenceAnnotations();
	 *           Iterator<Entry<Annotation, Position>> iter =
	 *           annotationMap.entrySet().iterator(); while (iter.hasNext()) {
	 *           Entry<Annotation, Position> mapEntry = iter.next();
	 *           annotationModel.addAnnotation(mapEntry.getKey(),
	 *           mapEntry.getValue()); } } fOccurrenceAnnotations =
	 *           annotationMap.keySet().toArray(new
	 *           Annotation[annotationMap.keySet().size()]); }
	 *
	 *           return Status.OK_STATUS; } }
	 */

	/**
	 * Cancels the occurrences finder job upon document changes.
	 *
	 * @since 3.0
	 */
	class OccurrencesFinderJobCanceler implements IDocumentListener, ITextInputListener {

		public void install() {
			final ISourceViewer sourceViewer = getSourceViewer();
			if (sourceViewer == null) {
				return;
			}

			final StyledText text = sourceViewer.getTextWidget();
			if (text == null || text.isDisposed()) {
				return;
			}

			sourceViewer.addTextInputListener(this);

			final IDocument document = getDocumentProvider().getDocument(getEditorInput());
			if (document != null) {
				document.addDocumentListener(this);
			}
		}

		public void uninstall() {
			final ISourceViewer sourceViewer = getSourceViewer();
			if (sourceViewer != null) {
				sourceViewer.removeTextInputListener(this);
			}

			final IDocumentProvider documentProvider = getDocumentProvider();
			if (documentProvider != null) {
				final IDocument document = documentProvider.getDocument(getEditorInput());
				if (document != null) {
					document.removeDocumentListener(this);
				}
			}
		}

		/*
		 * @see org.eclipse.jface.text.IDocumentListener#documentAboutToBeChanged(org
		 * .eclipse.jface.text.DocumentEvent)
		 */
		@Override
		public void documentAboutToBeChanged(final DocumentEvent event) {
			if (get(JavaEditor_fOccurrencesFinderJob, CognitiveEditor.this) != null) {
				final Job job = (Job) get(JavaEditor_fOccurrencesFinderJob, CognitiveEditor.this);
				invoke(JavaEditor_OccurrencesFinderJob_doCancel, job);
			}
		}

		/*
		 * @see org.eclipse.jface.text.IDocumentListener#documentChanged(org.eclipse.
		 * jface.text.DocumentEvent)
		 */
		@Override
		public void documentChanged(final DocumentEvent event) {
		}

		/*
		 * @see org.eclipse.jface.text.ITextInputListener#
		 * inputDocumentAboutToBeChanged(org.eclipse.jface.text.IDocument,
		 * org.eclipse.jface.text.IDocument)
		 */
		@Override
		public void inputDocumentAboutToBeChanged(final IDocument oldInput, final IDocument newInput) {
			if (oldInput == null) {
				return;
			}

			oldInput.removeDocumentListener(this);
		}

		/*
		 * @see org.eclipse.jface.text.ITextInputListener#inputDocumentChanged(org.
		 * eclipse.jface.text.IDocument, org.eclipse.jface.text.IDocument)
		 */
		@Override
		public void inputDocumentChanged(final IDocument oldInput, final IDocument newInput) {
			if (newInput == null) {
				return;
			}
			newInput.addDocumentListener(this);
		}
	}

	/**
	 * Returns the lock object for the given annotation model.
	 *
	 * @param annotationModel the annotation model
	 * @return the annotation model's lock object
	 * @since 3.0
	 */
	// TODO: Copy of JavaEditor.getLockObject(IAnnotationModel)
	private Object getLockObject(final IAnnotationModel annotationModel) {
		if (annotationModel instanceof ISynchronizable) {
			final Object lock = ((ISynchronizable) annotationModel).getLockObject();
			if (lock != null) {
				return lock;
			}
		}
		return annotationModel;
	}

	/*
	 * @see AbstractTextEditor#doSetInput(IEditorInput)
	 */
	@Override
	protected void doSetInput(final IEditorInput input) throws CoreException {
		manageActivatorPropertyChangeListener(input != null);
		memberInvalidationRegistrar.uninstall();
		super.doSetInput(input); // This also updates the title image with the
									// super-class base image
		if (fErrorTickUpdater != null) {
			fErrorTickUpdater.updateEditorImage(getInputJavaElement());
		}
		memberInvalidationRegistrar.install(input);
	}

	protected void manageActivatorPropertyChangeListener(final boolean install) {
		if (install && getEditorInput() == null) {
			Activator.getDefault().getPreferenceStore().addPropertyChangeListener(activatorPropertyChangeListener);
			readActivatorPreferences();
		} else if (!install && getEditorInput() != null) {
			Activator.getDefault().getPreferenceStore().removePropertyChangeListener(activatorPropertyChangeListener);
		}
	}

	protected void readActivatorPreferences() {
		markResolutionOccurrences = Activator.getDefault().getPreferenceStore().getBoolean(MARK_RESOLUTION_OCCURRENCES);
	}

	protected class MemberInvalidationRegistrar {

		private IDocument lastDocument;

		protected void uninstall() {
			if (lastDocument != null) {
				lastDocument.removeDocumentListener(anaphoraReresolvingListener);
			}
		}

		protected void install(final IEditorInput newInput) {
			if (newInput != null) {
				final IDocument document = getDocumentProvider().getDocument(getEditorInput());
				document.addDocumentListener(anaphoraReresolvingListener);
				lastDocument = document;
			}
		}

	}

	/**
	 * Configures the toggle comment action
	 *
	 * @since 3.0
	 *
	 *        private void configureToggleCommentAction() { IAction action=
	 *        getAction("ToggleComment"); //$NON-NLS-1$ if (action instanceof
	 *        ToggleCommentAction) { ISourceViewer sourceViewer= getSourceViewer();
	 *        SourceViewerConfiguration configuration=
	 *        getSourceViewerConfiguration();
	 *        ((ToggleCommentAction)action).configure(sourceViewer, configuration);
	 *        } }
	 */

	@SuppressWarnings("restriction")
	protected static class ErrorTickUpdater extends JavaEditorErrorTickUpdater {

		// Hides the identically-named private field of the super class
		protected JavaEditor fJavaEditor;
		// Hides the identically-named private field of the super class
		protected JavaUILabelProvider fLabelProvider;

		public ErrorTickUpdater(final JavaEditor editor) {
			super(editor);
			fJavaEditor = editor;
			fLabelProvider = new AnaphorLabelProvider(0, JavaElementImageProvider.SMALL_ICONS);
			fLabelProvider.addLabelDecorator(new ProblemsLabelDecorator(null));
		}

		// Copy from super-class
		@Override
		public void updateEditorImage(final IJavaElement jelement) {
			final Image titleImage = fJavaEditor.getTitleImage();
			if (titleImage == null) {
				return;
			}
			Image newImage;
			if (jelement instanceof ICompilationUnit && !jelement.getJavaProject().isOnClasspath(jelement)) {
				newImage = fLabelProvider.getImage(jelement.getResource());
			} else {
				newImage = fLabelProvider.getImage(jelement);
			}
			if (titleImage != newImage) {
				postImageChange(newImage);
			}
		}

		// Copy from super-class
		private void postImageChange(final Image newImage) {
			final Shell shell = fJavaEditor.getEditorSite().getShell();
			if (shell != null && !shell.isDisposed()) {
				shell.getDisplay().syncExec(() -> fJavaEditor.updatedTitleImage(newImage));
			}
		}

		@Override
		public void dispose() {
			super.dispose();
			fLabelProvider.dispose();
		}
	}

	@SuppressWarnings("restriction")
	protected static class AnaphorLabelProvider extends JavaUILabelProvider {
		/**
		 * @param textFlags  Flags defined in <code>JavaElementLabels</code>.
		 * @param imageFlags Flags defined in <code>JavaElementImageProvider</code>.
		 */
		public AnaphorLabelProvider(final long textFlags, final int imageFlags) {
			super(textFlags, imageFlags);
			// Remove image provider installed by super class
			if (fImageLabelProvider != null) {
				fImageLabelProvider.dispose();
			}

			// Install a custom imahe provider
			fImageLabelProvider = new AnaphorImageProvider();
		}
	}

	@SuppressWarnings("restriction")
	protected static class AnaphorImageProvider extends JavaElementImageProvider {

		public static final IPath ICONS_PATH = new Path("$nl$/icons/full");
		public static final ImageDescriptor DESC_OBJS_CUNIT_ANAPHORS = Activator
				.createImageDescriptor(ICONS_PATH.append("obj16").append("jcu_obj.gif"));

		@Override
		public ImageDescriptor getBaseImageDescriptor(final IJavaElement element, final int renderFlags) {
			switch (element.getElementType()) {
			case IJavaElement.COMPILATION_UNIT:
				return DESC_OBJS_CUNIT_ANAPHORS;
			default:
				return super.getBaseImageDescriptor(element, renderFlags);
			}

		}

	}
}
