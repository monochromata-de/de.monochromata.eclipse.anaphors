package de.monochromata.eclipse.anaphors;

import static java.util.Collections.singletonList;
import static org.eclipse.jdt.internal.corext.refactoring.code.context.IndexingFromAnaphorResolutionContext.index;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jdt.internal.corext.refactoring.code.context.AnaphorResolutionContext;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

/**
 * Contains duplicates of an optional original anaphora for all potential
 * anaphors that have been resolved. This type is used after successful anaphor
 * resolution.
 */
public class MultipleOriginalAnaphoras implements OriginalAnaphora {

	public final Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, Optional<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>>> originalAnaphoraByAnaphorPart;

	public MultipleOriginalAnaphoras(final AnaphorResolutionContext context) {
		this(singletonList(context));
	}

	public MultipleOriginalAnaphoras(final List<AnaphorResolutionContext> anaphorResolutionContexts) {
		this(createOriginalAnaphoraByAnaphorPart(anaphorResolutionContexts));
	}

	public MultipleOriginalAnaphoras(
			final Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, Optional<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>>> originalAnaphoraByAnaphorPart) {
		this.originalAnaphoraByAnaphorPart = originalAnaphoraByAnaphorPart;
	}

	protected static Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, Optional<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>>> createOriginalAnaphoraByAnaphorPart(
			final List<AnaphorResolutionContext> anaphorResolutionContexts) {
		return index(anaphorResolutionContexts, context -> context.originalAnaphora);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((originalAnaphoraByAnaphorPart == null) ? 0 : originalAnaphoraByAnaphorPart.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final MultipleOriginalAnaphoras other = (MultipleOriginalAnaphoras) obj;
		if (originalAnaphoraByAnaphorPart == null) {
			if (other.originalAnaphoraByAnaphorPart != null) {
				return false;
			}
		} else if (!originalAnaphoraByAnaphorPart.equals(other.originalAnaphoraByAnaphorPart)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "MultipleOriginalAnaphora [originalAnaphoraByAnaphorPart=" + originalAnaphoraByAnaphorPart + "]";
	}

}
