package de.monochromata.eclipse.anaphors.event;

import java.util.List;

import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.event.ConsumerNotifier;

/**
 * An {@link AnaphorResolutionProgressEvent} is used with a
 * {@link ConsumerNotifier} and comes with a {@link List} of
 * {@link AbstractAnaphoraPositionForRepresentation} instances.
 */
public enum AnaphorResolutionProgressEvent {
    /**
     * Sent when anaphora relations have been added or changed.
     */
    AnaphoraAddedOrChanged,

    /**
     * Sent when the addition/change of the given anaphora relations is about to be
     * un-done.
     */
    AboutToUndoAnaphoraAddedOrChanged,
}