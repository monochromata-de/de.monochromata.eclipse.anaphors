package de.monochromata.eclipse.anaphors.preferences;

import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.ADD_FINAL_MODIFIER_TO_CREATED_TEMPORARY_VARIABLES;
import static de.monochromata.eclipse.anaphors.preferences.PreferenceConstants.USE_LOCAL_VARIABLE_TYPE_INFERENCE;

import org.eclipse.jface.preference.IPreferenceStore;

import de.monochromata.anaphors.preferences.Preferences;

public class AnaphorsPreferences extends Preferences {

    private final IPreferenceStore preferenceStore;

    public AnaphorsPreferences(final IPreferenceStore preferenceStore) {
        // TODO: Make preferences immutable?
        super(false, false);
        this.preferenceStore = preferenceStore;
    }

    @Override
    public boolean getAddFinalModifierToCreatedTemporaryVariables() {
        return preferenceStore.getBoolean(ADD_FINAL_MODIFIER_TO_CREATED_TEMPORARY_VARIABLES);
    }

    @Override
    public boolean getUseLocalVariableTypeInference() {
        return preferenceStore.getBoolean(USE_LOCAL_VARIABLE_TYPE_INFERENCE);
    }

}
