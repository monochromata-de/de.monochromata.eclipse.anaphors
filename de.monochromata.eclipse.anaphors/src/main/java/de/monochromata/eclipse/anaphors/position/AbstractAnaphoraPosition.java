package de.monochromata.eclipse.anaphors.position;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.Position;

import de.monochromata.eclipse.position.DoublyLinkedPosition;

public abstract class AbstractAnaphoraPosition<T extends AbstractAnaphoraPosition<T>> extends Position
		implements DoublyLinkedPosition<T> {

	public static final String ANAPHORA_CATEGORY = AbstractAnaphoraPosition.class.getName();
	private T previous;
	private final List<T> next = new ArrayList<>();

	public AbstractAnaphoraPosition() {
	}

	public AbstractAnaphoraPosition(final int offset, final int length) {
		super(offset, length);
	}

	public AbstractAnaphoraPosition(final int offset) {
		super(offset);
	}

	@Override
	public T getPrevious() {
		return previous;
	}

	@Override
	public void setPrevious(final T previous) {
		this.previous = previous;
	}

	@Override
	public List<T> getNext() {
		return next;
	}

	@Override
	public void addNext(final T next) {
		this.next.add(next);
	}

	@Override
	public void addNext(final List<T> next) {
		this.next.addAll(next);
	}

	@Override
	public void removeNext(final T next) {
		this.next.remove(next);
	}

	@Override
	public void removeNext(final List<T> next) {
		this.next.removeAll(next);
	}

}
