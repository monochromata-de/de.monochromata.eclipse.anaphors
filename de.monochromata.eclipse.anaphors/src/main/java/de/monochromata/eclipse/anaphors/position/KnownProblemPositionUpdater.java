package de.monochromata.eclipse.anaphors.position;

import static de.monochromata.eclipse.anaphors.position.KnownProblemPosition.KNOWN_PROBLEM_CATEGORY;

import de.monochromata.eclipse.position.GrowingPositionUpdater;

public class KnownProblemPositionUpdater extends GrowingPositionUpdater {

    public KnownProblemPositionUpdater() {
        super(KNOWN_PROBLEM_CATEGORY, false);
    }

}
