package de.monochromata.eclipse.anaphors.position;

import org.eclipse.jdt.core.dom.ASTNode;

/**
 * Used temporarily to track the source location of a known {@link ASTNode}.
 * <p>
 * Because this position refers to an {@link ASTNode}, it should only be used up
 * until before unknown AST changes can happen and should never be persisted.
 */
public abstract class AbstractAnaphoraPositionForTransformation extends AbstractAnaphoraPosition {

	public final ASTNode astNode;

	public AbstractAnaphoraPositionForTransformation(final ASTNode astNode, final int offset, final int length) {
		super(offset, length);
		this.astNode = astNode;
	}

	public AbstractAnaphoraPositionForTransformation(final ASTNode astNode, final int offset) {
		super(offset);
		this.astNode = astNode;
	}

	public AbstractAnaphoraPositionForTransformation(final ASTNode astNode) {
		super();
		this.astNode = astNode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((astNode == null) ? 0 : astNode.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final AbstractAnaphoraPositionForTransformation other = (AbstractAnaphoraPositionForTransformation) obj;
		if (astNode == null) {
			if (other.astNode != null) {
				return false;
			}
		} else if (!astNode.equals(other.astNode)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "AbstractAnaphoraPositionForTransformation [offset=" + offset + ", length=" + length + ", isDeleted="
				+ isDeleted + ", astNode=" + astNode + "]";
	}

}
