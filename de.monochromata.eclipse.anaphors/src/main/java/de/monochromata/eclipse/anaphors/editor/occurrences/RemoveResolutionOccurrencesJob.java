package de.monochromata.eclipse.anaphors.editor.occurrences;

import static de.monochromata.eclipse.anaphors.editor.occurrences.ResolutionOccurrencesUpdating.updateResolutionOccurrences;
import static java.util.stream.Stream.empty;
import static org.eclipse.core.runtime.Status.OK_STATUS;

import java.util.function.BiConsumer;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;

public class RemoveResolutionOccurrencesJob extends Job {

    private final Annotation[] annotationsToRemove;
    private final Object annotationModelLock;
    private final IAnnotationModel annotationModel;
    private final Runnable resetExistingAnnotations;
    private final BiConsumer<RemoveResolutionOccurrencesJob, Long> scheduler;

    public RemoveResolutionOccurrencesJob(final Annotation[] annotationsToRemove,
            final Object annotationModelLock,
            final IAnnotationModel annotationModel,
            final Runnable resetExistingAnnotations) {
        this(annotationsToRemove, annotationModelLock, annotationModel, resetExistingAnnotations, Job::schedule,
                Job::setPriority);
    }

    public RemoveResolutionOccurrencesJob(final Annotation[] annotationsToRemove,
            final Object annotationModelLock,
            final IAnnotationModel annotationModel,
            final Runnable resetExistingAnnotations,
            final BiConsumer<RemoveResolutionOccurrencesJob, Long> scheduler,
            final BiConsumer<RemoveResolutionOccurrencesJob, Integer> prioritySetter) {
        super("Fade resolution occurrences");
        this.annotationsToRemove = annotationsToRemove;
        this.annotationModelLock = annotationModelLock;
        this.annotationModel = annotationModel;
        this.resetExistingAnnotations = resetExistingAnnotations;
        this.scheduler = scheduler;
        setSystem(true);
        prioritySetter.accept(this, SHORT);
    }

    public Annotation[] getAnnotationsToRemove() {
        return annotationsToRemove;
    }

    @Override
    public IStatus run(final IProgressMonitor monitor) {
        updateResolutionOccurrences(annotationsToRemove, empty(), annotationModelLock, annotationModel);
        resetExistingAnnotations.run();
        return OK_STATUS;
    }

}
