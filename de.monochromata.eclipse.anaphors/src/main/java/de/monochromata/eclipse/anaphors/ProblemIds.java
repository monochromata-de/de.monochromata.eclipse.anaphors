package de.monochromata.eclipse.anaphors;

public interface ProblemIds {

    /**
     * A problem category (a prefix for the problem IDs).
     */
    int AnaphorRelated = 0x90000000;

    /**
     * A problem ID for ambiguous references.
     */
    int AmbiguousReference = 1;

}
