package de.monochromata.eclipse.anaphors.editor.update;

import static java.util.Objects.requireNonNull;
import static org.eclipse.jdt.core.dom.anaphors.DomVisiting.requireNode;

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.core.filebuffers.FileBuffers;
import org.eclipse.core.filebuffers.ITextFileBuffer;
import org.eclipse.core.filebuffers.LocationKind;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jface.text.IDocument;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionAroundASTNode;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionAroundASTNode.NodeType;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForTransformation;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionOnASTNode;
import de.monochromata.eclipse.anaphors.position.AnaphoraPositionAfterEndOfASTNode;
import de.monochromata.eclipse.anaphors.position.AnaphoraPositionBeforeStartOfASTNode;
import de.monochromata.eclipse.anaphors.position.AnaphoraPositionCreation;
import de.monochromata.eclipse.anaphors.position.AnaphoraPositionOnAnaphor;
import de.monochromata.eclipse.anaphors.position.AnaphoraPositionOnRelatedExpression;
import de.monochromata.eclipse.anaphors.position.AnaphoraPositionRemoval;
import de.monochromata.function.ThrowingSupplier;

/**
 * {@link #updateAnaphoraWithPositionsForAstNodes(ASTBasedAnaphora, CompilationUnit, Supplier)}
 * and
 * {@link #updateAnaphoraWithPositionsAroundAstNodes(ASTBasedAnaphora, CompilationUnit, Supplier)}
 * are the main methods of this interface.
 */
public interface AstBasedAnaphoraUpdating {

    static <T extends Throwable> PublicAnaphora updateAnaphoraWithPositionsForAstNodes(
            final PublicAnaphora preliminaryAnaphora, final CompilationUnit existingScope,
            final ThrowingSupplier<CompilationUnit, T> changeOperation) throws T {
        return updateAnaphora(preliminaryAnaphora, existingScope, changeOperation,
                AnaphoraPositionCreation::addPositionsForAstNodes,
                AstBasedAnaphoraUpdating::updateAnaphoraWithEqualNodes);
    }

    @Deprecated
    static <T extends Throwable> PublicAnaphora updateAnaphoraWithPositionsAroundAstNodes(
            final PublicAnaphora preliminaryAnaphora, final CompilationUnit existingScope,
            final ThrowingSupplier<CompilationUnit, T> changeOperation) throws T {
        return updateAnaphora(preliminaryAnaphora, existingScope, changeOperation,
                AnaphoraPositionCreation::addPositionsAroundAstNodes,
                AstBasedAnaphoraUpdating::updateAnaphoraWithNodesInRange);
    }

    static <P extends AbstractAnaphoraPosition, T extends Throwable> PublicAnaphora updateAnaphora(
            final PublicAnaphora preliminaryAnaphora, final CompilationUnit existingScope,
            final ThrowingSupplier<CompilationUnit, T> changeOperation,
            final BiFunction<IDocument, PublicAnaphora, List<P>> positionCreator,
            final Function<Triple<List<P>, CompilationUnit, PublicAnaphora>, PublicAnaphora> anaphoraUpdater) throws T {
        final IDocument document = getDocumentForPositionsTracking(existingScope);
        final List<P> positions = positionCreator.apply(document, preliminaryAnaphora);
        try {
            final CompilationUnit newScope = changeOperation.get();
            return anaphoraUpdater.apply(new ImmutableTriple<>(positions, newScope, preliminaryAnaphora));
        } finally {
            AnaphoraPositionRemoval.removePositions(document, positions);
        }
    }

    static IDocument getDocumentForPositionsTracking(final CompilationUnit existingScope) {
        final ITextFileBuffer textFileBuffer = requireTextFileBuffer(existingScope);
        return textFileBuffer.getDocument();
    }

    static ITextFileBuffer requireTextFileBuffer(final CompilationUnit existingScope) {
        final IPath location = existingScope.getTypeRoot().getPath();
        // TODO: Which location kind to choose if not IFile? See the JavaDoc of
        // getTextFileBuffer(...) and LocationKind
        final LocationKind locationKind = location instanceof IFile ? LocationKind.IFILE : LocationKind.NORMALIZE;
        final ITextFileBuffer textFileBuffer = FileBuffers.getTextFileBufferManager().getTextFileBuffer(location,
                locationKind);
        if (textFileBuffer == null) {
            throw new IllegalStateException("No text file buffer found for " + location);
        }
        return textFileBuffer;
    }

    static <T extends AbstractAnaphoraPositionOnASTNode> PublicAnaphora updateAnaphoraWithEqualNodes(
            final Triple<List<T>, CompilationUnit, PublicAnaphora> input) {
        return updateAnaphoraWithEqualNodes(input.getLeft(), input.getMiddle(), input.getRight());
    }

    /**
     * Obtains a new related expression and anaphor from the new scope that are
     * equal to the ones from the given anaphora relation and creates a anaphora
     * relation using the two.
     *
     * @param positions Positions for the AST nodes of related expression and
     *                  anaphor created via
     *                  {@link AnaphoraPositionCreation#addPositionsForAstNodes(org.eclipse.jface.text.IDocument, ASTBasedAnaphora)}
     * @throws IllegalArgumentException If the given positions do not comprise
     *                                  positions for the related expression and
     *                                  anaphor of the preliminary anaphora
     *                                  relation.
     * @see #getEqualNodeFromNewScope(CompilationUnit, Map, ASTNode, Class)
     */
    static <T extends AbstractAnaphoraPositionOnASTNode> PublicAnaphora updateAnaphoraWithEqualNodes(
            final List<T> positions, final CompilationUnit newScope, final PublicAnaphora anaphora) {
        final Map<ASTNode, T> positionsByAstNode = mapOnAstNodeByAstNode(positions);

        final ASTNode newRelatedExpression = getEqualNodeFromNewScope(newScope, positionsByAstNode,
                anaphora.getRelatedExpression().getRelatedExpression(), AnaphoraPositionOnRelatedExpression.class);
        final Expression newAnaphor = (Expression) getEqualNodeFromNewScope(newScope, positionsByAstNode,
                anaphora.getAnaphorExpression(), AnaphoraPositionOnAnaphor.class);

        return recreateForNewAST(anaphora, newScope, newRelatedExpression, anaphora.getAnaphor(), newAnaphor);
    }

    static <T extends AbstractAnaphoraPositionOnASTNode> Map<ASTNode, T> mapOnAstNodeByAstNode(
            final List<T> positions) {
        return positions.stream().filter(position -> position instanceof AbstractAnaphoraPositionOnASTNode)
                .map(position -> new SimpleImmutableEntry<>(position.astNode, position))
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
    }

    static <T extends AbstractAnaphoraPositionForTransformation> ASTNode getEqualNodeFromNewScope(
            final CompilationUnit newScope, final Map<ASTNode, T> positionsByAstNode, final ASTNode oldNode,
            final Class<? extends AbstractAnaphoraPositionOnASTNode> positionType) {
        final T position = requirePosition(positionsByAstNode, oldNode, positionType);
        return requireNode(newScope, position);
    }

    static <T extends AbstractAnaphoraPosition> T requirePosition(final Map<ASTNode, T> positionsByAstNode,
            final ASTNode astNode, final Class<? extends AbstractAnaphoraPositionOnASTNode> positionType) {
        final T position = positionsByAstNode.get(astNode);
        if (!positionType.isInstance(position)) {
            throw new IllegalArgumentException(
                    "The given positions contain no position of type " + positionType.getName());
        }
        return position;
    }

    static <T extends AbstractAnaphoraPositionAroundASTNode> PublicAnaphora updateAnaphoraWithNodesInRange(
            final Triple<List<T>, CompilationUnit, PublicAnaphora> input) {
        return updateAnaphoraWithNodesInRange(input.getLeft(), input.getMiddle(), input.getRight());
    }

    /**
     * Obtains a new related expression and anaphor AST node from the new scope that
     * are between the positions before the start and after the end of respective
     * nodes from the given anaphora relation and creates a anaphora relation using
     * the new nodes.
     *
     * @param positions Positions before the start and after the end for the AST
     *                  nodes of related expression and anaphor created via
     *                  {@link AnaphoraPositionCreation#addPositionsAroundAstNodes(org.eclipse.jface.text.IDocument, ASTBasedAnaphora)}
     * @throws IllegalArgumentException If the given positions do not comprise
     *                                  positions for the related expression and
     *                                  anaphor of the preliminary anaphora
     *                                  relation.
     * @see #getEqualNodeFromNewScope(CompilationUnit, Map, ASTNode, Class)
     */
    static <T extends AbstractAnaphoraPositionAroundASTNode> PublicAnaphora updateAnaphoraWithNodesInRange(
            final List<T> positions, final CompilationUnit newScope, final PublicAnaphora anaphora) {
        final Map<ASTNode, Pair<T, T>> positionsByAstNode = mapAroundAstNodeByAstNode(positions);

        final ASTNode newRelatedExpression = getNodeBetween(newScope, positionsByAstNode,
                anaphora.getRelatedExpression().getRelatedExpression(), NodeType.RelatedExpression);
        final Expression newAnaphor = (Expression) getNodeBetween(newScope, positionsByAstNode,
                anaphora.getAnaphorExpression(), NodeType.Anaphor);

        return recreateForNewAST(anaphora, newScope, newRelatedExpression, anaphora.getAnaphor(), newAnaphor);
    }

    static <T extends AbstractAnaphoraPositionAroundASTNode> Map<ASTNode, Pair<T, T>> mapAroundAstNodeByAstNode(
            final List<T> positions) {
        final Map<ASTNode, Pair<T, T>> beforeAndAfterByAstNode = new HashMap<>();
        positions.stream().filter(position -> position instanceof AbstractAnaphoraPositionAroundASTNode)
                .forEach(position -> mapBeforeAndAfterByAstNode(beforeAndAfterByAstNode, position));
        return beforeAndAfterByAstNode;
    }

    static <T extends AbstractAnaphoraPositionAroundASTNode> void mapBeforeAndAfterByAstNode(
            final Map<ASTNode, Pair<T, T>> beforeAndAfterByAstNode, final T position) {
        if (position instanceof AnaphoraPositionBeforeStartOfASTNode) {
            mapBeforeByAstNode(beforeAndAfterByAstNode, position);
        } else if (position instanceof AnaphoraPositionAfterEndOfASTNode) {
            mapAfterByAstNode(beforeAndAfterByAstNode, position);
        } else {
            throw new IllegalArgumentException("Unknown subclass of "
                    + AbstractAnaphoraPositionAroundASTNode.class.getName() + " " + position.getClass().getName());
        }
    }

    static <T extends AbstractAnaphoraPositionAroundASTNode> void mapBeforeByAstNode(
            final Map<ASTNode, Pair<T, T>> beforeAndAfterByAstNode, final T positionBeforeNode) {
        mapBeforeOrAfterByAstNode(beforeAndAfterByAstNode, positionBeforeNode,
                () -> new ImmutablePair<>(positionBeforeNode, null),
                (existingPair, position) -> new ImmutablePair<>(position,
                        requireNonNull(existingPair.getRight(), "position after AST node required but missing")));
    }

    static <T extends AbstractAnaphoraPositionAroundASTNode> void mapAfterByAstNode(
            final Map<ASTNode, Pair<T, T>> beforeAndAfterByAstNode, final T positionAfterNode) {
        mapBeforeOrAfterByAstNode(beforeAndAfterByAstNode, positionAfterNode,
                () -> new ImmutablePair<>(null, positionAfterNode),
                (existingPair, position) -> new ImmutablePair<>(
                        requireNonNull(existingPair.getLeft(), "position before AST node required but missing"),
                        position));
    }

    static <T extends AbstractAnaphoraPositionAroundASTNode> void mapBeforeOrAfterByAstNode(
            final Map<ASTNode, Pair<T, T>> beforeAndAfterByAstNode, final T position,
            final Supplier<Pair<T, T>> initializer, final BiFunction<Pair<T, T>, T, Pair<T, T>> updater) {
        final ASTNode astNode = position.astNode;
        final Pair<T, T> pair = beforeAndAfterByAstNode.get(astNode);
        if (pair == null) {
            beforeAndAfterByAstNode.put(astNode, initializer.get());
        } else {
            beforeAndAfterByAstNode.put(astNode, updater.apply(pair, position));
        }
    }

    static <T extends AbstractAnaphoraPositionAroundASTNode> ASTNode getNodeBetween(final CompilationUnit newScope,
            final Map<ASTNode, Pair<T, T>> positionsByAstNode, final ASTNode oldNode, final NodeType nodeType) {
        final Pair<T, T> beforeAndAfterNode = positionsByAstNode.get(oldNode);
        final T beforeStart = requirePosition(beforeAndAfterNode, Pair::getLeft, nodeType);
        final T afterEnd = requirePosition(beforeAndAfterNode, Pair::getRight, nodeType);
        final int startPosition = beforeStart.offset;
        final int length = afterEnd.offset - startPosition;
        return requireNode(newScope, startPosition, length);
    }

    static <T extends AbstractAnaphoraPositionAroundASTNode> T requirePosition(final Pair<T, T> beforeAndAfterNode,
            final Function<Pair<T, T>, T> accessor, final NodeType nodeType) {
        return requireNonNull(beforeAndAfterNode == null ? null : accessor.apply(beforeAndAfterNode),
                "The given positions contain no position with node type " + nodeType.name());
    }

    private static PublicAnaphora recreateForNewAST(final PublicAnaphora existingAnaphora,
            final CompilationUnit newScope, final ASTNode newRelatedExpression, final String newAnaphor,
            final Expression newAnaphorExpression) {
        final var strategy = existingAnaphora.getAnaphorResolutionStrategy();
        // TODO: Move to a factory that is also used by
        // PublicRelatedExpressionsCollector
        final var newRelatedExp = PublicRelatedExpression.create(newRelatedExpression);
        final var newReferent = strategy.createReferent(newScope, newRelatedExp,
                existingAnaphora.getReferent().getMemento());
        return strategy.createAnaphora(newScope, newAnaphor, newAnaphorExpression, newRelatedExp, newReferent,
                existingAnaphora.getReferentializationStrategy());
    }

}
