package de.monochromata.eclipse.anaphors.editor.update;

import static java.util.Arrays.stream;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IInitializer;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMember;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IParent;
import org.eclipse.jdt.core.JavaModelException;

public interface MemberCollection {

    static void getModifiedMembers(final IJavaElement[] elements,
            final Builder<IMember> streamBuilder, final AtomicBoolean membersAdded,
            final Predicate<IMember> shouldMemberBeAdded) {
        stream(elements)
                .forEach(element -> getModifiedMembers(element, streamBuilder, membersAdded, shouldMemberBeAdded));
    }

    static Stream<IMember> getAllMembers(final IJavaElement element) {
        final Builder<IMember> streamBuilder = Stream.builder();
        final AtomicBoolean membersAdded = new AtomicBoolean(false);
        addAllMembers(element, streamBuilder, membersAdded);
        return streamBuilder.build();
    }

    static void addAllMembers(final IJavaElement element, final Builder<IMember> streamBuilder,
            final AtomicBoolean membersAdded) {
        getModifiedMembers(element, streamBuilder, membersAdded, member -> true);
    }

    static void getModifiedMembers(final IJavaElement element, final Builder<IMember> streamBuilder,
            final AtomicBoolean membersAdded, final Predicate<IMember> shouldMemberBeAdded) {
        final boolean added = getMemberIfPossible(element, streamBuilder, membersAdded, shouldMemberBeAdded);
        if (!added) {
            addChildMembers(element, streamBuilder, membersAdded, shouldMemberBeAdded);
        }
    }

    static boolean getMemberIfPossible(final IJavaElement element, final Builder<IMember> streamBuilder,
            final AtomicBoolean membersAdded, final Predicate<IMember> shouldMemberBeAdded) {
        if (element instanceof IField || element instanceof IInitializer || element instanceof IMethod) {
            return getMemberIfPossible((IMember) element, streamBuilder, membersAdded, shouldMemberBeAdded);
        }
        return false;
    }

    static boolean getMemberIfPossible(final IMember member,
            final Builder<IMember> streamBuilder, final AtomicBoolean membersAdded,
            final Predicate<IMember> shouldMemberBeAdded) {
        if (shouldMemberBeAdded.test(member)) {
            streamBuilder.add(member);
            membersAdded.set(true);
            return true;
        }
        return false;
    }

    static void addChildMembers(final IJavaElement element, final Builder<IMember> streamBuilder,
            final AtomicBoolean membersAdded,
            final Predicate<IMember> shouldMemberBeAdded) {
        if (element instanceof IParent) {
            try {
                getModifiedMembers(((IParent) element).getChildren(), streamBuilder, membersAdded, shouldMemberBeAdded);
            } catch (final JavaModelException e) {
                throw new IllegalStateException(e);
            }
        }
    }

}
