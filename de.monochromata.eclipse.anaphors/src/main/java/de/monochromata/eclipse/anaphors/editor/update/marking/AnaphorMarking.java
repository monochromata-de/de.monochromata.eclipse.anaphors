package de.monochromata.eclipse.anaphors.editor.update.marking;

import static de.monochromata.eclipse.position.PositionQuerying.getTrackingPositions;
import static java.util.Arrays.stream;

import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.compiler.IProblem;
import org.eclipse.jface.text.IDocument;

import de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPosition;

public interface AnaphorMarking {

    static void collectTrackingPositions(final IJavaElement compilationUnitElement,
            final IDocument originDocument, final IProblem[] problems,
            final List<MemberTrackingPosition> memberTrackingPositionsToReResolve) {
        stream(problems)
                .map(ProblemConversion::toOffsetAndLength)
                .forEach(collectTrackingPositions(originDocument,
                        memberTrackingPositionsToReResolve));
    }

    static Consumer<Pair<Integer, Integer>> collectTrackingPositions(final IDocument originDocument,
            final List<MemberTrackingPosition> memberTrackingPositionsToReResolve) {
        return (offsetAndLength) -> collectTrackingPositions(offsetAndLength, originDocument,
                memberTrackingPositionsToReResolve);
    }

    static void collectTrackingPositions(final Pair<Integer, Integer> offsetAndLength,
            final IDocument originDocument,
            final List<MemberTrackingPosition> memberTrackingPositionsToReResolve) {
        final int offset = offsetAndLength.getLeft();
        final int length = offsetAndLength.getRight();
        collectTrackingPositions(originDocument, memberTrackingPositionsToReResolve, offset, length);
    }

    static void collectTrackingPositions(final IDocument originDocument,
            final List<MemberTrackingPosition> memberTrackingPositionsToReResolve, final int offset, final int length) {
        synchronized (memberTrackingPositionsToReResolve) {
            getTrackingPositions(offset, length, originDocument)
                    .forEach(collectTrackingPosition(memberTrackingPositionsToReResolve));
        }
    }

    static Consumer<MemberTrackingPosition> collectTrackingPosition(
            final List<MemberTrackingPosition> memberTrackingPositionsToReResolve) {
        return position -> {
            synchronized (memberTrackingPositionsToReResolve) {
                if (!memberTrackingPositionsToReResolve.contains(position)) {
                    memberTrackingPositionsToReResolve.add(position);
                }
            }
        };
    }

}
