package de.monochromata.eclipse.anaphors.editor.update.job;

import java.util.function.Consumer;

import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobGroup;

public class AnaphorResolutionQueue implements Consumer<Job> {

    // Only resolve 1 anaphor at a time so resolution does not interfere
    protected final int maxThreads = 1;
    protected final JobGroup jobGroup = new JobGroup("Resolve anaphors", maxThreads, 0);

    @Override
    public void accept(final Job job) {
        job.setJobGroup(jobGroup);
        job.schedule();
    }

    public boolean hasJobs() {
        return !jobGroup.getActiveJobs().isEmpty();
    }
}
