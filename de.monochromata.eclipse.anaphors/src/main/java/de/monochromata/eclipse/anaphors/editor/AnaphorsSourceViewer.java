package de.monochromata.eclipse.anaphors.editor;

import static de.monochromata.eclipse.anaphors.swt.SWTAccess.runSync;
import static de.monochromata.eclipse.anaphors.swt.SWTAccess.supplySync;
import static de.monochromata.eclipse.persp.PerspectivationPositionEnabling.configurePerspectivation;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadLocationException;
import static java.util.Optional.ofNullable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.internal.ui.JavaPlugin;
import org.eclipse.jdt.internal.ui.text.java.JavaFormattingContext;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentInformationMappingExtension2;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ISlaveDocumentManager;
import org.eclipse.jface.text.IWidgetTokenKeeper;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.contentassist.IContentAssistant;
import org.eclipse.jface.text.formatter.FormattingContextProperties;
import org.eclipse.jface.text.formatter.IFormattingContext;
import org.eclipse.jface.text.source.IOverviewRuler;
import org.eclipse.jface.text.source.IVerticalRuler;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;

import de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy;
import de.monochromata.eclipse.persp.PerspectivationDocument;
import de.monochromata.eclipse.persp.PerspectivationDocumentManager;
import de.monochromata.eclipse.persp.PerspectivationViewer;

/**
 * Based on CompilationUnitEditor.AdaptedSourceViewer
 */
@SuppressWarnings("restriction")
public class AnaphorsSourceViewer extends PerspectivationViewer {

	private final CognitiveEditor editor;
	private final Supplier<IJavaElement> inputJavaElementSupplier;

	private AnaphoraPartSelectionUpdater selectionUpdater;

	public AnaphorsSourceViewer(final CognitiveEditor editor, final Supplier<IJavaElement> inputJavaElementSupplier,
			final Composite parent, final IVerticalRuler verticalRuler, final IOverviewRuler overviewRuler,
			final boolean showAnnotationsOverview, final int styles, final IPreferenceStore store) {
		super(parent, verticalRuler, overviewRuler, showAnnotationsOverview, styles, store);
		this.editor = editor;
		this.inputJavaElementSupplier = inputJavaElementSupplier;
	}

	public IContentAssistant getContentAssistant() {
		return fContentAssistant;
	}

	@Override
	protected ISlaveDocumentManager createSlaveDocumentManager() {
		return new PerspectivationDocumentManager();
	}

	/*
	 * @see ITextOperationTarget#doOperation(int)
	 */
	@Override
	public void doOperation(final int operation) {

		if (getTextWidget() == null) {
			return;
		}

		switch (operation) {
		case CONTENTASSIST_PROPOSALS:
			final long time = JavaPlugin.DEBUG_RESULT_COLLECTOR ? System.currentTimeMillis() : 0;
			String msg = fContentAssistant.showPossibleCompletions();
			if (JavaPlugin.DEBUG_RESULT_COLLECTOR) {
				final long delta = System.currentTimeMillis() - time;
				System.err.println("Code Assist (total): " + delta); //$NON-NLS-1$
			}
			editor.setStatusLineErrorMessage(msg);
			return;
		case QUICK_ASSIST:
			/*
			 * XXX: We can get rid of this once the SourceViewer has a way to update the
			 * status line https://bugs.eclipse.org/bugs/show_bug.cgi?id=133787
			 */
			msg = fQuickAssistAssistant.showPossibleQuickAssists();
			editor.setStatusLineErrorMessage(msg);
			return;
		}

		super.doOperation(operation);
	}

	/*
	 * @see IWidgetTokenOwner#requestWidgetToken(IWidgetTokenKeeper)
	 */
	@Override
	public boolean requestWidgetToken(final IWidgetTokenKeeper requester) {
		if (PlatformUI.getWorkbench().getHelpSystem().isContextHelpDisplayed()) {
			return false;
		}
		return super.requestWidgetToken(requester);
	}

	/*
	 * @see IWidgetTokenOwnerExtension#requestWidgetToken(IWidgetTokenKeeper, int)
	 *
	 * @since 3.0
	 */
	@Override
	public boolean requestWidgetToken(final IWidgetTokenKeeper requester, final int priority) {
		if (PlatformUI.getWorkbench().getHelpSystem().isContextHelpDisplayed()) {
			return false;
		}
		return super.requestWidgetToken(requester, priority);
	}

	/*
	 * @see org.eclipse.jface.text.source.SourceViewer#createFormattingContext()
	 *
	 * @since 3.0
	 */
	@Override
	public IFormattingContext createFormattingContext() {
		final IFormattingContext context = new JavaFormattingContext();

		Map<String, String> preferences;
		final IJavaElement inputJavaElement = inputJavaElementSupplier.get();
		final IJavaProject javaProject = inputJavaElement != null ? inputJavaElement.getJavaProject() : null;
		if (javaProject == null) {
			preferences = new HashMap<>(JavaCore.getOptions());
		} else {
			preferences = new HashMap<>(javaProject.getOptions(true));
		}

		context.setProperty(FormattingContextProperties.CONTEXT_PREFERENCES, preferences);

		return context;
	}

	@Override
	public void setSelectedRange(final int selectionOffset, final int selectionLength) {
		if (selectionUpdater != null && selectionLength < 0) {
			// This case happens when the rewrite session for anaphor resolution ends and
			// code has been inserted at the cursor position.

			// When text is inserted in the master document at the cursor position,
			// selection length is negative, i.e. needs to be substracted from offset.
			// Otherwise the cursor would turn into a selection.

			final IRegion correctedSelection = selectionUpdater.correctSelection(selectionOffset + selectionLength,
					-selectionLength);
			super.setSelectedRange(correctedSelection.getOffset(), correctedSelection.getLength());
		} else {
			super.setSelectedRange(selectionOffset, selectionLength);
		}
	}

	public void initiateSelectionUpdate(final List<? extends Position> positions) {
		final Point selectedRange = supplySync(getControl(), this::getSelectedRange);
		final Pair<Pair<? extends Position, SelectionRelativeToAnaphoraPart>, List<Pair<? extends Position, SelectionRelativeToAnaphoraPart>>> closestsAndAllPositionsAndRelativeSelections = SelectionRelativeToAnaphoraPart
				.createForPairWithClosestPosition(selectedRange.x, selectedRange.y, positions);
		selectionUpdater = new AnaphoraPartSelectionUpdater(closestsAndAllPositionsAndRelativeSelections.getLeft(),
				closestsAndAllPositionsAndRelativeSelections.getRight(),
				originOffset -> wrapBadLocationException(
						() -> ((IDocumentInformationMappingExtension2) fInformationMapping)
								.toClosestImageRegion(new Region(originOffset, 0))).getOffset(),
				fInformationMapping::toOriginOffset);
	}

	public void finishSelectionUpdate(final List<? extends Position> positions) {
		selectionUpdater = null;
	}

	public void updatePerspectivationPositions(final IDocument originDocument, final PerspectivationStrategy strategy) {
		runSync(getControl(),
				() -> ofNullable(getVisibleDocument()).map(imageDocument -> (PerspectivationDocument) imageDocument)
						.ifPresent(imageDocument -> configurePerspectivation(originDocument, imageDocument,
								strategy.getPerspectivationConfiguration())));
	}

}