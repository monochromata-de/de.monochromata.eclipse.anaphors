package de.monochromata.eclipse.anaphors.editor.occurrences;

import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadPositionCategoryException;
import static java.util.Arrays.stream;

import java.util.function.BiFunction;
import java.util.stream.Stream;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.internal.core.manipulation.search.IOccurrencesFinder;
import org.eclipse.jface.text.IDocument;

import de.monochromata.eclipse.anaphors.editor.AnaphoraQuerying;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;

/**
 * Marks the related expression of the selected anaphor and all other anaphors
 * co-referent to the selected anaphor.
 */
@SuppressWarnings("restriction")
public class AnaphoraOccurrencesFinder implements IOccurrencesFinder {

    public static final int SEARCH_KIND = 100;
    public static final String ID = AnaphoraOccurrencesFinder.class.getSimpleName();

    private final String word;
    private final BiFunction<Integer, Integer, Stream<AbstractAnaphoraPositionForRepresentation>> positionsProvider;
    private CompilationUnit fASTRoot;
    private int fSelectionOffset;
    private int fSelectionLength;
    private boolean fAnaphoraFound;
    private OccurrenceLocation[] fResult;

    public AnaphoraOccurrencesFinder(final String word, final IDocument document) {
        this(word, (startPosition, length) -> AnaphoraQuerying.getAnaphoraAt(startPosition, length,
                stream(wrapBadPositionCategoryException(
                        () -> document.getPositions(ANAPHORA_CATEGORY)))));
    }

    public AnaphoraOccurrencesFinder(final String word,
            final BiFunction<Integer, Integer, Stream<AbstractAnaphoraPositionForRepresentation>> positionsProvider) {
        this.word = word;
        this.positionsProvider = positionsProvider;
    }

    @Override
    public String initialize(final CompilationUnit root, final int selectionOffset, final int selectionLength) {
        fASTRoot = root;
        fSelectionOffset = selectionOffset;
        fSelectionLength = selectionLength;
        fAnaphoraFound = false;
        return null; // null means "no problem"
    }

    @Override
    public String initialize(final CompilationUnit root, final ASTNode node) {
        return initialize(root, node.getStartPosition(), node.getLength());
    }

    @Override
    public String getJobLabel() {
        return "Search for Related Expression and Co-referent Anaphors";
    }

    @Override
    public String getUnformattedPluralLabel() {
        return "''{0}(...)'' - {1} related expressions and co-referent anaphors in ''{2}''";
    }

    @Override
    public String getUnformattedSingularLabel() {
        return "''{0}(...)'' - 1 related expression in ''{1}''";
    }

    @Override
    public String getElementName() {
        return word;
    }

    @Override
    public CompilationUnit getASTRoot() {
        return fASTRoot;
    }

    @Override
    public OccurrenceLocation[] getOccurrences() {
        if (!fAnaphoraFound) {
            fResult = createResult();
        }
        return fResult;
    }

    protected OccurrenceLocation[] createResult() {
        final Stream<AbstractAnaphoraPositionForRepresentation> positions = positionsProvider
                .apply(fSelectionOffset, fSelectionLength);
        fAnaphoraFound = true;
        return positions
                .map(AnaphoraOccurrenceLocationCreation::createOccurrenceLocation)
                .toArray(OccurrenceLocation[]::new);
    }

    @Override
    public int getSearchKind() {
        return SEARCH_KIND;
    }

    @Override
    public String getID() {
        return ID;
    }
}
