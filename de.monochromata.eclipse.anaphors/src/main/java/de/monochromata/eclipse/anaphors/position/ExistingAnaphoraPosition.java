package de.monochromata.eclipse.anaphors.position;

import static de.monochromata.eclipse.anaphors.position.ExistingPosition.isExistingPositionToBeRetained;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicChainElement;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.eclipse.anaphors.OriginalAnaphora;

public interface ExistingAnaphoraPosition {

	static Function<PublicChainElement, PositionForRelatedExpression> getExistingPositionForRelatedExpression(
			final List<AbstractAnaphoraPositionForRepresentation> positionsToRemove) {
		return relatedExpressionElement -> getExistingPositionForRelatedExpression(relatedExpressionElement,
				positionsToRemove);
	}

	static PositionForRelatedExpression getExistingPositionForRelatedExpression(
			final PublicChainElement relatedExpressionElement,
			final List<AbstractAnaphoraPositionForRepresentation> positionsToRemove) {
		return getExistingPositionForRelatedExpression(relatedExpressionElement,
				isExistingPositionToBeRetained(positionsToRemove));
	}

	static PositionForRelatedExpression getExistingPositionForRelatedExpression(
			final PublicChainElement relatedExpressionElement,
			final Predicate<PositionForRelatedExpression> isExistingPositionToBeRetained) {
		// It does not matter which of the next elements is chosen - all next elements
		// should be anaphors related to the related expression element. The first
		// anaphor element is chosen to ease debugging.
		final var anaphorChainElement = relatedExpressionElement.next.stream().findFirst().orElseThrow();
		return getExistingAnaphoraPosition(anaphorChainElement.anaphor, anaphorChainElement.anaphorAttachment,
				Triple::getLeft, isExistingPositionToBeRetained);
	}

	static Function<PublicChainElement, PositionForAnaphor> getExistingPositionForAnaphor(
			final List<AbstractAnaphoraPositionForRepresentation> positionsToRemove) {
		return anaphorElement -> getExistingPositionForAnaphor(anaphorElement, positionsToRemove);
	}

	static PositionForAnaphor getExistingPositionForAnaphor(final PublicChainElement anaphorElement,
			final List<AbstractAnaphoraPositionForRepresentation> positionsToRemove) {
		return getExistingPositionForAnaphor(anaphorElement, isExistingPositionToBeRetained(positionsToRemove));
	}

	static PositionForAnaphor getExistingPositionForAnaphor(final PublicChainElement anaphorElement,
			final Predicate<PositionForAnaphor> isExistingPositionToBeRetained) {
		return getExistingAnaphoraPosition(anaphorElement.anaphor, anaphorElement.anaphorAttachment, Triple::getRight,
				isExistingPositionToBeRetained);
	}

	private static <P extends AbstractAnaphoraPositionForRepresentation> P getExistingAnaphoraPosition(
			final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart,
			final OriginalAnaphora originalAnaphora,
			final Function<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>, P> getAnaphoraPosition,
			final Predicate<P> isExistingPositionToBeRetained) {
		return ExistingPosition.getExistingPosition(anaphorPart, originalAnaphora, getAnaphoraPosition,
				isExistingPositionToBeRetained);
	}

}
