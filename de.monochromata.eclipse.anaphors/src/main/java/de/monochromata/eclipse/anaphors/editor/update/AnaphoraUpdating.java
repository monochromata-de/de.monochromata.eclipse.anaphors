package de.monochromata.eclipse.anaphors.editor.update;

import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jface.text.Position;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;

public interface AnaphoraUpdating {

	static <R extends Position, A extends Position> void update(final Optional<Triple<R, Anaphora, A>> existingAnaphora,
			final List<Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>> potentialNewAnaphoras,
			final Consumer<Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>> adder,
			final Consumer<Triple<R, Anaphora, A>> retainer,
			final BiConsumer<Triple<R, Anaphora, A>, Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>> updater,
			final Consumer<List<Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>>> ambiguityWarner,
			final BiConsumer<Anaphora, A> deleter) {
		// See #151: The current implementation is fail fast and removes
		// previously-working code as soon as it stops working
		if (!existingAnaphora.isPresent()) {
			updateMissingAnaphora(potentialNewAnaphoras, adder, ambiguityWarner);
		} else {
			updateExistingAnaphora(existingAnaphora.get(), potentialNewAnaphoras, adder, retainer, updater,
					ambiguityWarner, deleter);
		}
	}

	static void updateMissingAnaphora(
			final List<Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>> potentialNewAnaphoras,
			final Consumer<Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>> adder,
			final Consumer<List<Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>>> ambiguityWarner) {
		if (potentialNewAnaphoras.size() == 1) {
			adder.accept(potentialNewAnaphoras.get(0));
		} else if (potentialNewAnaphoras.size() > 1) {
			ambiguityWarner.accept(potentialNewAnaphoras);
		} else { // implies potentialNewAnaphoras.isEmpty()
			// do nothing
		}
	}

	static <R extends Position, A extends Position> void updateExistingAnaphora(
			final Triple<R, Anaphora, A> existingAnaphora,
			final List<Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>> potentialNewAnaphoras,
			final Consumer<Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>> adder,
			final Consumer<Triple<R, Anaphora, A>> retainer,
			final BiConsumer<Triple<R, Anaphora, A>, Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>> updater,
			final Consumer<List<Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>>> ambiguityWarner,
			final BiConsumer<Anaphora, A> deleter) {
		if (potentialNewAnaphoras.isEmpty()) {
			deleter.accept(existingAnaphora.getMiddle(), existingAnaphora.getRight());
			// A warning will be added by JDT implicitly because the definite expression
			// refers to an undefined variable.
		} else if (potentialNewAnaphoras.size() == 1) {
			updateExistingAnaphora(existingAnaphora, potentialNewAnaphoras.get(0), adder, retainer, updater, deleter);
		} else { // implies potentialNewAnaphoras.size() > 1
			deleter.accept(existingAnaphora.getMiddle(), existingAnaphora.getRight());
			ambiguityWarner.accept(potentialNewAnaphoras);
		}
	}

	static <R extends Position, A extends Position> void updateExistingAnaphora(
			final Triple<R, Anaphora, A> existingAnaphora,
			final Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>> newAnaphora,
			final Consumer<Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>> adder,
			final Consumer<Triple<R, Anaphora, A>> retainer,
			final BiConsumer<Triple<R, Anaphora, A>, Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>> updater,
			final BiConsumer<Anaphora, A> deleter) {
		if (AnaphoraComparing.isEqual(existingAnaphora, toTriple(newAnaphora))) {
			retainer.accept(existingAnaphora);
		} else {
			updater.accept(existingAnaphora, newAnaphora);
		}
	}

	static Triple<Position, Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>, Position> toTriple(
			final Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>> newAnaphora) {
		final ASTNode relatedExpressionNode = newAnaphora.getLeft().getRelatedExpression().getRelatedExpression();
		final Position relatedExpressionPosition = toPosition(relatedExpressionNode);
		final Position anaphorPosition = toPosition(newAnaphora.getRight().getAnaphorExpression());
		return new ImmutableTriple<>(relatedExpressionPosition, newAnaphora, anaphorPosition);
	}

	static Position toPosition(final ASTNode relatedExpressionNode) {
		return new Position(relatedExpressionNode.getStartPosition(), relatedExpressionNode.getLength());
	}

}
