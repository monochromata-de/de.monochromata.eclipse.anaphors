package de.monochromata.eclipse.anaphors.position;

import org.eclipse.jdt.core.dom.ASTNode;

import de.monochromata.eclipse.persp.PerspectivationPosition;

/**
 * Used to persistently represent related expressions or anaphors.
 * <p>
 * Because the position is potentially persistent, it does not refer to
 * {@link ASTNode}s.
 */
public abstract class AbstractAnaphoraPositionForRepresentation
		extends AbstractAnaphoraPosition<AbstractAnaphoraPositionForRepresentation> {

	/**
	 * The perspectivation position that belongs to this position. The
	 * perspectivation position is linked here so it does not beed to be searched
	 * from the positions.
	 */
	public final PerspectivationPosition perspectivationPosition;

	public AbstractAnaphoraPositionForRepresentation(final int offset, final int length,
			final PerspectivationPosition perspectivationPosition) {
		super(offset, length);
		this.perspectivationPosition = perspectivationPosition;
	}

	public AbstractAnaphoraPositionForRepresentation(final int offset,
			final PerspectivationPosition perspectivationPosition) {
		super(offset);
		this.perspectivationPosition = perspectivationPosition;
	}

	public AbstractAnaphoraPositionForRepresentation(final PerspectivationPosition perspectivationPosition) {
		this.perspectivationPosition = perspectivationPosition;
	}

}
