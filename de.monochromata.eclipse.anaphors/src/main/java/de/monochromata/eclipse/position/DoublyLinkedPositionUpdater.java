package de.monochromata.eclipse.position;

import java.util.function.BiConsumer;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;

import de.monochromata.function.TriConsumer;

/**
 * An updater for positions that implement {@link DoublyLinkedPosition}.
 * <p>
 * When a position in the tree is removed, the tree is retained as much as
 * possible.
 * <p>
 * Note that this updater assumes to operate on {@link MarkablePosition}s.
 *
 * @param <T> the type of position updated
 * @param <D> the type of the deletion strategy applied
 *
 */
public class DoublyLinkedPositionUpdater<T extends DoublyLinkedPosition<T>, D> extends GrowingPositionUpdater
		implements PositionDeleter<D> {

	private final TriConsumer<T, IDocument, D> preDeleteListener;
	private final D defaultDeletionStrategy;

	/**
	 * @param preDeleteListener is informed before a position is deleted.
	 * @see #maintainChain(Position, IDocument)
	 */
	public DoublyLinkedPositionUpdater(final String positionCategory,
			final TriConsumer<T, IDocument, D> preDeleteListener, final D defaultDeletionStrategy) {
		super(positionCategory);
		this.preDeleteListener = preDeleteListener;
		this.defaultDeletionStrategy = defaultDeletionStrategy;
	}

	/**
	 * @throws ClassCastException If the current position is not an instance of
	 *                            {@link DoublyLinkedPosition}
	 * @see GrowingPositionUpdater#position
	 */
	@Override
	public void deletePosition(final Position position, final IDocument document) {
		deletePosition(position, document, defaultDeletionStrategy);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void deletePosition(final Position position, final IDocument document, final D deletionStrategy) {
		preDeleteListener.accept((T) position, document, deletionStrategy);
		super.deletePosition(position, document);
	}

	/**
	 * Can be used to create a pre-delete listener to update the chain of positions
	 * when a position is deleted.
	 * <ul>
	 * <li>when the given position is first:
	 * <ul>
	 * <li>if there are only two elements in the chain: delete and unlink the next
	 * element (without invoking the pre-delete listener for the next element)</li>
	 * <li>else: unlink the position and the next position</li>
	 * </ul>
	 * </li>
	 * <li>when the given position is last:
	 * <ul>
	 * <li>and there are only two elements in the chain: delete and unlink the
	 * previous element (without invoking the pre-delete listener for the next
	 * element)</li>
	 * <li>else: unlink the position and the previous position</li>
	 * </ul>
	 * </li>
	 * <li>else (when the given position is neither first, nor last): link the
	 * previous to the next position (i.e. remove the current position from the
	 * chain)
	 * </ul>
	 *
	 * @see #DoublyLinkedPositionUpdater(String, BiConsumer)
	 */
	public static <T extends DoublyLinkedPosition<T>> BiConsumer<T, IDocument> maintainChainBeforeDeletion(
			final String category) {
		return (positionToDelete,
				document) -> new DoublyLinkedPositionDeleter<T>(
						position -> doDeletePosition(category, (Position) position, document))
								.maintainChainBeforeDeletion().accept(positionToDelete);

	}

}
