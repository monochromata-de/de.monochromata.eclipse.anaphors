package de.monochromata.eclipse.position;

import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.concat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

public class ChangeCollectingDoublyLinkedDeleter<T extends DoublyLinkedPosition<T>> implements DoublyLinkedDeletion<T> {

	private enum Which {
		Former, Latter
	}

	private final List<T> positionsToAdd;
	private final List<T> positionsToRemove;
	private final List<Pair<T, T>> positionsToLink;
	private final List<Pair<T, T>> positionsToUnlink;
	private final List<Consumer<T>> deletionListeners = new ArrayList<>();

	public ChangeCollectingDoublyLinkedDeleter(final List<T> positionsToAdd, final List<T> positionsToRemove,
			final List<Pair<T, T>> positionsToLink, final List<Pair<T, T>> positionsToUnlink) {
		this.positionsToAdd = positionsToAdd;
		this.positionsToRemove = positionsToRemove;
		this.positionsToLink = positionsToLink;
		this.positionsToUnlink = positionsToUnlink;
	}

	@Override
	public boolean isUnlinked(final T position) {
		return isFirst(position) && isLast(position);
	}

	@Override
	public boolean isFirst(final T position) {
		return getPrevious(position) == null;
	}

	@Override
	public boolean isLast(final T position) {
		final var next = getNext(position);
		return next == null || next.isEmpty();
	}

	@Override
	public boolean hasPositionBeforePrevious(final T position) {
		if (isFirst(position)) {
			return false;
		}
		final var previous = getPrevious(position);
		return null != getPrevious(previous);
	}

	@Override
	public boolean hasSiblings(final T position) {
		return getSiblings(position).findAny().isPresent();
	}

	private Stream<T> getSiblings(final T position) {
		return ofNullable(getPrevious(position)).map(this::getNext).map(List::stream).orElseGet(Stream::empty)
				.filter(sibling -> sibling != position);
	}

	@Override
	public boolean hasPositionsAfterNext(final T position) {
		return !getPositionsAfterNext(position).isEmpty();
	}

	private List<T> getPositionsAfterNext(final T position) {
		return isLast(position) ? emptyList()
				: getNext(position).stream().flatMap(next -> getNext(next).stream()).collect(toList());
	}

	@Override
	public T getPrevious(final T position) {
		final var previous = position.getPrevious();
		final var previousToBeLinked = getPreviousToBeLinked(position);
		if (previous != null && positionsToUnlink.contains(new ImmutablePair<>(previous, position))) {
			return null;
		}
		return previousToBeLinked.orElse(previous);
	}

	protected Optional<T> getPreviousToBeLinked(final T position) {
		return positionsToLink.stream().filter(previousAndNext -> previousAndNext.getRight().equals(position))
				.map(Pair::getLeft).findAny();
	}

	@Override
	public List<T> getNext(final T position) {
		return concat(getExistingNext(position), getFutureNext(position)).collect(toList());
	}

	private Stream<T> getExistingNext(final T position) {
		return Optional.ofNullable(position.getNext()).orElseGet(Collections::emptyList).stream()
				.filter(next -> !positionsToUnlink.contains(new ImmutablePair<>(position, next)));
	}

	private Stream<T> getFutureNext(final T position) {
		return positionsToLink.stream().filter(previousAndNext -> previousAndNext.getLeft().equals(position))
				.map(Pair::getRight);
	}

	public void addDeletionListener(final Consumer<T> deletionListener) {
		deletionListeners.add(deletionListener);
	}

	public void removeDeletionListener(final Consumer<T> deletionListener) {
		deletionListeners.remove(deletionListener);
	}

	@Override
	public void deletePosition(final T position) {
		rejectDuplicateRemoval(position);
		rejectRemoveAfterAdd(position);
		rejectRemoveAfterLink(position);
		positionsToRemove.add(position);
		deletionListeners.forEach(listener -> listener.accept(position));
	}

	private void rejectDuplicateRemoval(final T positionToRemove) {
		if (positionsToRemove.contains(positionToRemove)) {
			throw new IllegalStateException("Cannot delete position " + positionToRemove + " twice - "
					+ "the position is already to be deleted");
		}
	}

	private void rejectRemoveAfterAdd(final T positionToRemove) {
		if (positionsToAdd.contains(positionToRemove)) {
			throw new IllegalStateException("Position " + positionToRemove
					+ " is to be added already and shall now be removed. "
					+ "This is either a design flaw or a temporal order is required for positions to be added and removed.");
		}
	}

	private void rejectRemoveAfterLink(final T positionToRemove) {
		rejectRemoveAfterLink(positionToRemove, Pair::getLeft, Pair::getRight);
		rejectRemoveAfterLink(positionToRemove, Pair::getRight, Pair::getLeft);
	}

	private void rejectRemoveAfterLink(final T positionToRemove, final Function<Pair<T, T>, T> getFiltered,
			final Function<Pair<T, T>, T> getLinked) {
		final var toBeLinkedTo = positionsToLink.stream()
				.filter(link -> getFiltered.apply(link).equals(positionToRemove)).map(getLinked).findAny();
		if (toBeLinkedTo.isPresent()) {
			throw new IllegalStateException("Position " + positionToRemove + " is already to be linked to "
					+ toBeLinkedTo.get() + " and shall now be removed. "
					+ "This is either a design flaw or a temporal order is required for positions to be added and removed.");
		}
	}

	@Override
	public void unlink(final T previous, final T next) {
		final var linkToUnlink = new ImmutablePair<>(previous, next);
		requireLink(linkToUnlink);
		rejectDuplicateUnlinking(linkToUnlink);
		rejectUnlinkAfterLink(linkToUnlink);
		positionsToUnlink.add(linkToUnlink);
	}

	private void requireLink(final ImmutablePair<T, T> potentialLink) {
		if (!areLinked(potentialLink)) {
			throw new IllegalStateException("Cannot unlink " + potentialLink.getLeft() + " and "
					+ potentialLink.getRight() + ". " + "The positions are not yet linked.");
		}
	}

	private boolean areLinked(final Pair<T, T> potentialLink) {
		return potentialLink.getLeft().equals(potentialLink.getRight().getPrevious());
	}

	private void rejectDuplicateUnlinking(final ImmutablePair<T, T> potentialLink) {
		if (positionsToUnlink.contains(potentialLink)) {
			throw new IllegalStateException("Cannot unlink " + potentialLink.getLeft() + " and "
					+ potentialLink.getRight() + ". The positions are to be unlinked already.");
		}
	}

	private void rejectUnlinkAfterLink(final ImmutablePair<T, T> potentialLink) {
		if (positionsToLink.contains(potentialLink)) {
			throw new IllegalStateException("Positions " + potentialLink.getLeft() + " and " + potentialLink.getRight()
					+ " are to be linked and to be unlinked at the same time. "
					+ "This is either a design flaw or a temporal order is required for positions to be linked and unlinked.");
		}
	}

	@Override
	public void link(final T previous, final T next) {
		rejectDuplicateLinking(previous, next);
		rejectLinkAfterUnlink(previous, next);
		rejectLinkAfterDelete(previous, next);
		positionsToLink.add(new ImmutablePair<>(previous, next));
	}

	private void rejectDuplicateLinking(final T previous, final T next) {
		final var existingPrevious = getPreviousToBeLinkedTo(next);
		if (existingPrevious.isPresent()) {
			throw new IllegalStateException("You attempted to link previous position " + previous + " to position "
					+ next + " " + "but the latter is already to be linked to previous position "
					+ existingPrevious.get() + ". " + "There can only be one previous position. "
					+ "This is either a design flaw or a temporal order is required for positions to be linked.");
		}
	}

	private void rejectLinkAfterUnlink(final T previous, final T next) {
		final var previousToBeUnlinked = getPreviousToBeUnlinkedFrom(next);
		if (previousToBeUnlinked.isPresent()) {
			throw new IllegalStateException("You attempted to link position " + previous + " to position " + next + " "
					+ "but the latter is already to be unlinked from position " + previousToBeUnlinked.get()
					+ ". This is either a design flaw or a temporal order is required for positions to be linked and unlinked.");
		}
	}

	private void rejectLinkAfterDelete(final T previous, final T next) {
		rejectLinkAfterDelete(previous, next, Which.Former);
		rejectLinkAfterDelete(previous, next, Which.Latter);
	}

	private void rejectLinkAfterDelete(final T previous, final T next, final Which which) {
		final var toCheck = requireNonNull(
				which.equals(Which.Former) ? previous : which.equals(Which.Latter) ? next : null);
		if (positionsToRemove.contains(toCheck)) {
			throw new IllegalStateException("You attempted to link position " + previous + " to position " + next + " "
					+ "but the " + which.name().toLowerCase() + " is already to be deleted. "
					+ "This is either a design flaw or a temporal order is required for positions to be linked and deleted.");
		}
	}

	private Optional<T> getPreviousToBeLinkedTo(final T next) {
		return positionsToLink.stream().filter(link -> link.getRight().equals(next)).map(Pair::getLeft).findAny();
	}

	private Optional<T> getPreviousToBeUnlinkedFrom(final T next) {
		return positionsToUnlink.stream().filter(link -> link.getRight().equals(next)).map(Pair::getLeft).findAny();
	}

}
