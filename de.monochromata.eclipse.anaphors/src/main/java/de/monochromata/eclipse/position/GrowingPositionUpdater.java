package de.monochromata.eclipse.position;

import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadPositionCategoryException;
import static java.lang.Math.max;
import static java.lang.Math.min;

import java.util.function.BinaryOperator;

import org.eclipse.jface.text.DefaultPositionUpdater;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IPositionUpdater;
import org.eclipse.jface.text.Position;

/**
 * A position updater that always grows positions and never removes them. Its
 * structure is based on the one of {@link DefaultPositionUpdater}, but it does
 * not inherit from it because it would override all inherited methods.
 */
public class GrowingPositionUpdater implements IPositionUpdater {

    protected final String category;

    /**
     * Whether or not positions with length 0 are deleted (defaults to
     * {@literal true})
     */
    protected boolean deleteEmptyPositions;

    /** Caches the currently investigated position */
    protected Position position;

    /** Caches the offset of the replaced text */
    protected int offset;
    /** Caches the length of the replaced text */
    protected int length;
    /** Caches the length of the newly inserted text */
    protected int replaceLength;

    public GrowingPositionUpdater(final String positionCategory) {
        this(positionCategory, true);
    }

    public GrowingPositionUpdater(final String positionCategory, final boolean deleteEmptyPositions) {
        category = positionCategory;
        this.deleteEmptyPositions = deleteEmptyPositions;
    }

    public String getCategory() {
        return category;
    }

    public void setDeleteEmptyPositions(final boolean deleteEmptyPositions) {
        this.deleteEmptyPositions = deleteEmptyPositions;
    }

    public boolean isDeleteEmptyPositions() {
        return deleteEmptyPositions;
    }

    @Override
    public void update(final DocumentEvent event) {
        wrapBadPositionCategoryException(() -> {
            offset = event.getOffset();
            length = event.getLength();
            replaceLength = (event.getText() == null ? 0 : event.getText().length());

            final IDocument document = event.getDocument();
            final Position[] positions = document.getPositions(getCategory());
            for (final Position position : positions) {
                this.position = position;
                adaptToReplace(document);
            }
        });
    }

    protected void adaptToReplace(final IDocument document) {
        if (offset != position.offset && editEndsBeforeOrAtPosition(length)) {
            position.offset += replaceLength - length;
        } else if (editIsCompletelyInPosition(length)) {
            position.length += replaceLength - length;
        } else {
            if (length > 0) {
                adaptToRemove();
            }

            if (replaceLength > 0) {
                adaptToInsert();
            }

            if (position.length < 0) {
                position.length = 0;
            }
        }

        if (isDeleteEmptyPositions() && position.length <= 0) {
            deletePosition(document);
        }
    }

    protected void adaptToRemove() {
        // Adapt to removal of text
        if (position.length == 0) {
            // Retain length 0, do not make it negative.
        } else if (editIsInsidePosition(length)) {
            final int positionEnd = position.offset + position.length;
            final int removeEnd = offset + length;
            final int end = min(positionEnd, removeEnd);
            final int removedLength = end - offset;
            position.length = position.length - removedLength;
        } else {
            adaptToEdit(length, this::substract);
        }
    }

    protected void adaptToInsert() {
        // Adapt to insertion of replacement text
        if (editIsInsidePosition(replaceLength)) {
            position.length += replaceLength;
        } else {
            adaptToEdit(replaceLength, this::add);
        }
    }

    protected void adaptToEdit(final int length, final BinaryOperator<Integer> calculation) {
        if (editIsAtEndOfPosition(length)) {
            position.length = calculation.apply(position.length, length);
        } else if (editIsBeforePosition()) {
            final int originalPositionOffset = position.offset;
            final int deltaBeforePosition = min(position.offset - offset, length);
            position.offset = calculation.apply(position.offset, deltaBeforePosition);
            final int deltaInsidePosition = max(0, offset + this.length - originalPositionOffset);
            position.length = calculation.apply(position.length, deltaInsidePosition);
        }
    }

    protected boolean editIsCompletelyInPosition(final int length) {
        return length > 0 && position.offset <= offset && offset + this.length <= position.offset + position.length;
    }

    protected boolean editIsInsidePosition(final int length) {
        // TODO: Use a field instead of re-calculating
        final int positionEnd = position.offset + position.length;
        return length > 0 && position.offset <= offset && offset < positionEnd;
    }

    protected boolean editIsAtEndOfPosition(final int length) {
        // TODO: Use a field instead of re-calculating
        final int positionEnd = position.offset + position.length;
        return length > 0 && position.offset <= offset && offset == positionEnd;
    }

    private boolean editEndsBeforeOrAtPosition(final int length) {
        return offset + length <= position.offset;
    }

    private boolean editIsBeforePosition() {
        return offset < position.offset;
    }

    protected int add(final int a, final int b) {
        return a + b;
    }

    protected int substract(final int a, final int b) {
        return a - b;
    }

    protected void deletePosition(final IDocument document) {
        deletePosition(position, document);
    }

    protected void deletePosition(final Position position, final IDocument document) {
        doDeletePosition(category, position, document);
    }

    protected static void doDeletePosition(final String category, final Position position, final IDocument document) {
        position.delete();
        wrapBadPositionCategoryException(() -> document.removePosition(category, position));
    }

}
