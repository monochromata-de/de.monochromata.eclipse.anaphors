package de.monochromata.eclipse.position;

import static java.util.Arrays.stream;

import org.eclipse.jface.text.IDocument;

public interface PositionDeletion {

    static <D> PositionDeleter<D> getPositionDeleter(final IDocument originDocument,
            final Class<? extends PositionDeleter<D>> deleterType) {
        final PositionDeleter<D> positionDeleter = stream(originDocument.getPositionUpdaters())
                .filter(updater -> deleterType.isInstance(updater))
                .map(updater -> (PositionDeleter<D>) updater)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No " + deleterType.getName() + " found"));
        return positionDeleter;
    }

}
