package de.monochromata.eclipse.position;

import java.util.List;
import java.util.function.Consumer;

/**
 * DoublyLinkedDeletion based on {@link DoublyLinkedPosition}s i.e. all methods
 * delegate to respective methods in {@link DoublyLinkedPosition} and changes
 * are instantly effective.
 */
public class DoublyLinkedPositionDeleter<T extends DoublyLinkedPosition<T>> implements DoublyLinkedDeletion<T> {

	public final Consumer<T> delete;

	public DoublyLinkedPositionDeleter(final Consumer<T> delete) {
		this.delete = delete;
	}

	@Override
	public boolean isUnlinked(final T position) {
		return position.isUnlinked();
	}

	@Override
	public boolean isFirst(final T position) {
		return position.isFirst();
	}

	@Override
	public boolean isLast(final T position) {
		return position.isLast();
	}

	@Override
	public boolean hasPositionBeforePrevious(final T position) {
		return position.hasPositionBeforePrevious();
	}

	@Override
	public boolean hasSiblings(final T position) {
		return position.hasSiblings();
	}

	@Override
	public boolean hasPositionsAfterNext(final T position) {
		return position.hasPositionsAfterNext();
	}

	@Override
	public T getPrevious(final T position) {
		return position.getPrevious();
	}

	@Override
	public List<T> getNext(final T position) {
		return position.getNext();
	}

	@Override
	public void deletePosition(final T position) {
		delete.accept(position);
	}

	@Override
	public void unlink(final T previous, final T next) {
		DoublyLinkedPosition.unlink(previous, next);
	}

	@Override
	public void link(final T previous, final T next) {
		DoublyLinkedPosition.link(previous, next);
	}

}
