package de.monochromata.eclipse.position;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPositionCategoryException;

public interface PositionExceptionWrapping {

    static <O> O wrapPositionExceptions(
            final BadPositionCategoryThrowingSupplier<O, BadLocationException> runnable) {
        try {
            return wrapBadPositionCategoryException(runnable);
        } catch (final BadLocationException e) {
            throw new UncheckedBadLocationException(e);
        }
    }

    static void wrapPositionExceptions(final BadPositionCategoryThrowingRunnable<BadLocationException> runnable) {
        try {
            wrapBadPositionCategoryException(runnable);
        } catch (final BadLocationException e) {
            throw new UncheckedBadLocationException(e);
        }
    }

    static void wrapBadLocationException(final BadLocationThrowingRunnable runnable) {
        try {
            runnable.run();
        } catch (final BadLocationException e) {
            throw new UncheckedBadLocationException(e);
        }
    }

    static <I> void wrapBadLocationException(final I input, final BadLocationThrowingConsumer<I> consumer) {
        try {
            consumer.accept(input);
        } catch (final BadLocationException e) {
            throw new UncheckedBadLocationException(e);
        }
    }

    static <O> O wrapBadLocationException(final BadLocationThrowingSupplier<O> supplier) {
        try {
            return supplier.get();
        } catch (final BadLocationException e) {
            throw new UncheckedBadLocationException(e);
        }
    }

    static <I1, I2, I3> void wrapBadLocationException(final I1 input1, final I2 input2, final I3 input3,
            final BadLocationThrowingTriConsumer<I1, I2, I3> consumer) {
        try {
            consumer.accept(input1, input2, input3);
        } catch (final BadLocationException e) {
            throw new UncheckedBadLocationException(e);
        }
    }

    static <I, O> O wrapBadLocationExceptionFunction(final I input, final BadLocationThrowingFunction<I, O> function) {
        try {
            return function.apply(input);
        } catch (final BadLocationException e) {
            throw new UncheckedBadLocationException(e);
        }
    }

    static <I1, I2, O> O wrapBadLocationExceptionFunction(final I1 input1, final I2 input2,
            final BadLocationThrowingBiFunction<I1, I2, O> function) {
        try {
            return function.apply(input1, input2);
        } catch (final BadLocationException e) {
            throw new UncheckedBadLocationException(e);
        }
    }

    static <T extends Throwable> void wrapBadPositionCategoryException(
            final BadPositionCategoryThrowingRunnable<T> runnable) throws T {
        try {
            runnable.run();
        } catch (final BadPositionCategoryException e) {
            throw new IllegalStateException(e);
        }
    }

    static <O, T extends Throwable> O wrapBadPositionCategoryException(
            final BadPositionCategoryThrowingSupplier<O, T> supplier) throws T {
        try {
            return supplier.get();
        } catch (final BadPositionCategoryException e) {
            throw new IllegalStateException(e);
        }
    }

    static <I, T extends Throwable> void wrapBadPositionCategoryException(final I input,
            final BadPositionCategoryThrowingConsumer<I, T> consumer) throws T {
        try {
            consumer.accept(input);
        } catch (final BadPositionCategoryException e) {
            throw new IllegalStateException(e);
        }
    }

    static <I, O, T extends Throwable> O wrapBadPositionCategoryExceptionFunction(final I input,
            final BadPositionCategoryThrowingFunction<I, O, T> function) throws T {
        try {
            return function.apply(input);
        } catch (final BadPositionCategoryException e) {
            throw new IllegalStateException(e);
        }
    }

    static <I1, I2, O, T extends Throwable> O wrapBadPositionCategoryExceptionFunction(final I1 input1, final I2 input2,
            final BadPositionCategoryThrowingBiFunction<I1, I2, O, T> function) throws T {
        try {
            return function.apply(input1, input2);
        } catch (final BadPositionCategoryException e) {
            throw new IllegalStateException(e);
        }
    }

    @FunctionalInterface
    public interface BadLocationThrowingRunnable {

        public void run() throws BadLocationException;

    }

    @FunctionalInterface
    public interface BadLocationThrowingConsumer<I> {

        public void accept(final I input) throws BadLocationException;

    }

    @FunctionalInterface
    public interface BadLocationThrowingSupplier<O> {

        public O get() throws BadLocationException;

    }

    @FunctionalInterface
    public interface BadLocationThrowingTriConsumer<I1, I2, I3> {

        public void accept(final I1 input1, final I2 input2, final I3 input3) throws BadLocationException;

    }

    @FunctionalInterface
    public interface BadLocationThrowingFunction<I, O> {

        public O apply(final I input) throws BadLocationException;

    }

    @FunctionalInterface
    public interface BadLocationThrowingBiFunction<I1, I2, O> {

        public O apply(final I1 input1, final I2 input2) throws BadLocationException;

    }

    @FunctionalInterface
    public interface BadPositionCategoryThrowingRunnable<T extends Throwable> {

        public void run() throws T, BadPositionCategoryException;

    }

    @FunctionalInterface
    public interface BadPositionCategoryThrowingSupplier<O, T extends Throwable> {

        public O get() throws T, BadPositionCategoryException;

    }

    @FunctionalInterface
    public interface BadPositionCategoryThrowingConsumer<I, T extends Throwable> {

        public void accept(final I input) throws T, BadPositionCategoryException;

    }

    @FunctionalInterface
    public interface BadPositionCategoryThrowingBiConsumer<I1, I2, T extends Throwable> {

        public void accept(final I1 input1, final I2 input2) throws T, BadPositionCategoryException;

    }

    @FunctionalInterface
    public interface BadPositionCategoryThrowingFunction<I, O, T extends Throwable> {

        public O apply(final I input) throws T, BadPositionCategoryException;

    }

    @FunctionalInterface
    public interface BadPositionCategoryThrowingBiFunction<I1, I2, O, T extends Throwable> {

        public O apply(final I1 input1, final I2 input2) throws T, BadPositionCategoryException;

    }

}
