package de.monochromata.eclipse.position;

import static java.util.List.copyOf;

import java.util.List;
import java.util.function.Consumer;

public interface DoublyLinkedDeletion<T extends DoublyLinkedPosition<T>> {

	default Consumer<T> deletePositionAndMaintainChain(final Consumer<T> deletePosition) {
		return positionToDelete -> {
			maintainChainBeforeDeletion(positionToDelete);
			deletePosition.accept(positionToDelete);
		};
	}

	default Consumer<T> maintainChainBeforeDeletion() {
		return positionToDelete -> maintainChainBeforeDeletion(positionToDelete);
	}

	default void maintainChainBeforeDeletion(final T positionToDelete) {
		if (isUnlinked(positionToDelete)) {
			// Do nothing
		} else if (isLast(positionToDelete)) {
			if (!hasPositionBeforePrevious(positionToDelete) && !hasSiblings(positionToDelete)) {
				deletePrevious(positionToDelete);
			}
			unlinkPrevious(positionToDelete);
		} else if (isFirst(positionToDelete)) {
			if (!hasPositionsAfterNext(positionToDelete)) {
				deleteNext(positionToDelete);
			}
			unlinkNext(positionToDelete);
		} else {
			unlinkIntermediate(positionToDelete);
		}
	}

	default void deletePrevious(final T currentPosition) {
		final T previousPosition = getPrevious(currentPosition);
		deletePosition(previousPosition);
	}

	default void unlinkPrevious(final T currentPosition) {
		final T previousPosition = getPrevious(currentPosition);
		unlink(previousPosition, currentPosition);
	}

	default void deleteNext(final T currentPosition) {
		getNext(currentPosition).forEach(this::deletePosition);
	}

	default void unlinkNext(final T currentPosition) {
		final List<T> next = copyOf(getNext(currentPosition));
		next.forEach(unlinkFrom(currentPosition));
	}

	default void unlinkIntermediate(final T currentPosition) {
		final T previous = getPrevious(currentPosition);
		final List<T> next = copyOf(getNext(currentPosition));
		unlink(previous, currentPosition);
		next.forEach(unlinkFrom(currentPosition));
		next.forEach(linkTo(previous));
	}

	boolean isUnlinked(T position);

	boolean isFirst(T position);

	boolean isLast(T position);

	boolean hasPositionBeforePrevious(T position);

	boolean hasSiblings(T position);

	boolean hasPositionsAfterNext(T position);

	T getPrevious(T position);

	List<T> getNext(T position);

	void deletePosition(T position);

	default Consumer<T> unlinkFrom(final T previous) {
		return next -> unlink(previous, next);
	}

	void unlink(T previous, T next);

	default Consumer<T> linkTo(final T previous) {
		return next -> link(previous, next);
	}

	void link(T previous, T next);
}
