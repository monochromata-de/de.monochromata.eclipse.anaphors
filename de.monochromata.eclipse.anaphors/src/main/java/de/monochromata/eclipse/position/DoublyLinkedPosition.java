package de.monochromata.eclipse.position;

import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;

/**
 * Doubly linked positions form a tree.
 */
public interface DoublyLinkedPosition<T extends DoublyLinkedPosition<T>> {

	T getPrevious();

	void setPrevious(T previous);

	List<T> getNext();

	void addNext(T next);

	void addNext(List<T> next);

	void removeNext(T next);

	void removeNext(List<T> next);

	default boolean isUnlinked() {
		return isFirst() && isLast();
	}

	default boolean isFirst() {
		return getPrevious() == null;
	}

	default boolean isLast() {
		return getNext() == null || getNext().isEmpty();
	}

	default boolean hasPositionBeforePrevious() {
		return getPositionBeforePrevious() != null;
	}

	default T getPositionBeforePrevious() {
		return isFirst() ? null : getPrevious().getPrevious();
	}

	default boolean hasSiblings() {
		return getSiblings().findAny().isPresent();
	}

	default Stream<T> getSiblings() {
		return ofNullable(getPrevious()).map(previous -> previous.getNext().stream().filter(sibling -> sibling != this))
				.orElseGet(Stream::empty);
	}

	default boolean hasPositionsAfterNext() {
		return !getPositionsAfterNext().isEmpty();
	}

	default List<T> getPositionsAfterNext() {
		return isLast() ? emptyList() : getNext().stream().flatMap(next -> next.getNext().stream()).collect(toList());
	}

	static <T extends DoublyLinkedPosition<T>> void link(final Pair<T, T> positions) {
		link(positions.getLeft(), positions.getRight());
	}

	static <T extends DoublyLinkedPosition<T>> void link(final T previous, final T next) {
		if (!previous.getNext().contains(next)) {
			previous.addNext(next);
		}
		next.setPrevious(previous);
	}

	static <T extends DoublyLinkedPosition<T>> void unlink(final Pair<T, T> positions) {
		unlink(positions.getLeft(), positions.getRight());
	}

	static <P extends DoublyLinkedPosition<P>> void unlink(final P previous, final P next) {
		previous.removeNext(next);
		next.setPrevious(null);
	}

}
