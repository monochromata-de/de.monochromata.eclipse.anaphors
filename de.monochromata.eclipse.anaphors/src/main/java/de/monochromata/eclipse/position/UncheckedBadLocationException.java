package de.monochromata.eclipse.position;

import org.eclipse.jface.text.BadLocationException;

public class UncheckedBadLocationException extends RuntimeException {

    public UncheckedBadLocationException(final BadLocationException parent) {
        super(parent);
    }

}
