package de.monochromata.eclipse.position;

import static java.util.Arrays.stream;

import java.util.stream.Stream;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;

/**
 * Supports deletion of {@link MarkablePosition}s - they will only be deleted
 * when they have not yet been deleted and not been marked to be deleted.
 *
 * @param <D>
 *            a deletion strategy optionally provided by the position deleter.
 */
public interface PositionDeleter<D> {

    default void deletePositions(final Position[] positions, final IDocument document,
            final D deletionStrategy) {
        deletePositions(stream(positions), document, deletionStrategy);
    }

    default void deletePositions(final Stream<Position> stream, final IDocument document,
            final D deletionStrategy) {
        stream.forEach(position -> deletePositionIfNotYetDeleted(position, document, deletionStrategy));
    }

    default void deletePositionIfNotYetDeleted(final Position position, final IDocument document,
            final D deletionStrategy) {
        if (!position.isDeleted) {
            deletePosition(position, document, deletionStrategy);
        }
    }

    void deletePosition(Position position, final IDocument document, final D deletionStrategy);

}
