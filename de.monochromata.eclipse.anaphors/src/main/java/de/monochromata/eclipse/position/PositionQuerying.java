package de.monochromata.eclipse.position;

import static de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPosition.MEMBER_TRACKING_CATEGORY;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadPositionCategoryException;
import static java.util.Arrays.stream;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.IMember;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;

import de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPosition;

public interface PositionQuerying {

    static Stream<MemberTrackingPosition> getTrackingPositions(final Pair<Integer, Integer> offsetAndLength,
            final IDocument originDocument) {
        final Integer offset = offsetAndLength.getLeft();
        final Integer length = offsetAndLength.getRight();
        return getTrackingPositions(offset, length, originDocument);
    }

    static Stream<MemberTrackingPosition> getTrackingPositions(final int offset, final int length,
            final IDocument originDocument) {
        return stream(getPositions(MEMBER_TRACKING_CATEGORY, originDocument))
                .filter(position -> position.overlapsWith(offset, length))
                .map(position -> (MemberTrackingPosition) position);
    }

    static Optional<MemberTrackingPosition> getTrackingPosition(final IMember member, final IDocument originDocument) {
        return stream(getPositions(MEMBER_TRACKING_CATEGORY, originDocument))
                .map(position -> (MemberTrackingPosition) position)
                .filter(position -> position.member.equals(member))
                .findFirst();
    }

    static <P extends Position> Stream<P> getCoveredPositions(final List<MemberTrackingPosition> trackingPositions,
            final String positionCategory, final IDocument originDocument) {
        final Position[] positions = getPositions(positionCategory, originDocument);
        return getCoveredPositions(trackingPositions, positions);
    }

    static <P extends Position> Stream<P> getCoveredPositions(final List<MemberTrackingPosition> trackingPositions,
            final Position[] positions) {
        return trackingPositions
                .stream()
                .flatMap(trackingPosition -> PositionQuerying
                        .<P>getCoveredPositions(trackingPosition, positions));
    }

    static Position[] getPositions(final String positionCategory, final IDocument originDocument) {
        return wrapBadPositionCategoryException(
                () -> originDocument.getPositions(positionCategory));
    }

    @SuppressWarnings("unchecked")
    static <P extends Position> Stream<P> getCoveredPositions(final MemberTrackingPosition trackingPosition,
            final Position[] positions) {
        return stream(positions)
                .filter(position -> position.overlapsWith(trackingPosition.getOffset(), trackingPosition.getLength()))
                .map(position -> (P) position)
                .distinct();
    }

}
