package org.eclipse.jdt.internal.corext.refactoring.code;

import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.SourceRange;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.internal.corext.dom.fragments.ASTFragmentFactory;
import org.eclipse.jdt.internal.corext.dom.fragments.IASTFragment;
import org.eclipse.jdt.internal.corext.dom.fragments.IExpressionFragment;
import org.eclipse.jdt.internal.corext.refactoring.Checks;
import org.eclipse.jdt.internal.corext.refactoring.code.context.AnaphorResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.code.context.SharedAnaphorResolutionContext;

public interface ExpressionFragmentCreation {

    static IExpressionFragment getSelectedExpression(
            final SharedAnaphorResolutionContext sharedAnaphorResolution,
            final AnaphorResolutionContext anaphorResolution)
            throws JavaModelException {
        if (anaphorResolution.selectedExpression != null) {
            return anaphorResolution.selectedExpression;
        }
        final IASTFragment selectedFragment = ASTFragmentFactory.createFragmentForSourceRange(
                new SourceRange(anaphorResolution.definiteExpressionPosition.offset,
                        anaphorResolution.definiteExpressionPosition.length),
                sharedAnaphorResolution.compilationUnitNode, sharedAnaphorResolution.cu);

        if (selectedFragment instanceof IExpressionFragment
                && !Checks.isInsideJavadoc(selectedFragment.getAssociatedNode())) {
            anaphorResolution.selectedExpression = (IExpressionFragment) selectedFragment;
        } else if (selectedFragment != null) {
            if (selectedFragment.getAssociatedNode() instanceof ExpressionStatement) {
                final ExpressionStatement exprStatement = (ExpressionStatement) selectedFragment.getAssociatedNode();
                final Expression expression = exprStatement.getExpression();
                anaphorResolution.selectedExpression = (IExpressionFragment) ASTFragmentFactory
                        .createFragmentForFullSubtree(expression);
            } else if (selectedFragment.getAssociatedNode() instanceof Assignment) {
                final Assignment assignment = (Assignment) selectedFragment.getAssociatedNode();
                anaphorResolution.selectedExpression = (IExpressionFragment) ASTFragmentFactory
                        .createFragmentForFullSubtree(assignment);
            }
        }

        if (anaphorResolution.selectedExpression != null
                && Checks.isEnumCase(anaphorResolution.selectedExpression.getAssociatedExpression().getParent())) {
            anaphorResolution.selectedExpression = null;
        }

        return anaphorResolution.selectedExpression;
    }

}
