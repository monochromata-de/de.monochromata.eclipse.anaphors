package org.eclipse.jdt.internal.corext.refactoring.code.context;

import static java.util.stream.Collectors.toMap;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;

import de.monochromata.anaphors.ast.AnaphorPart;

public interface IndexingFromAnaphorResolutionContext {

	static <T> Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, T> index(
			final List<AnaphorResolutionContext> anaphorResolutionContexts,
			final Function<AnaphorResolutionContext, T> accessor) {
		return anaphorResolutionContexts.stream().flatMap(
				context -> context.potentialAnaphoras.stream().map(anaphora -> new ImmutablePair<>(context, anaphora)))
				.collect(toMap(contextAndAnaphora -> contextAndAnaphora.getRight().getAnaphorPart(),
						contextAndAnaphora -> accessor.apply(contextAndAnaphora.getLeft())));
	}

}
