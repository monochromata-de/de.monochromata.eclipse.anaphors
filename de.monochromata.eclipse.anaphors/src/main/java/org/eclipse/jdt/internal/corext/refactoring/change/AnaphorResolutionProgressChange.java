package org.eclipse.jdt.internal.corext.refactoring.change;

import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionProgressEvent.AboutToUndoAnaphoraAddedOrChanged;
import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionProgressEvent.AnaphoraAddedOrChanged;

import java.util.List;
import java.util.function.BiConsumer;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import de.monochromata.eclipse.anaphors.event.AnaphorResolutionProgressEvent;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;

public class AnaphorResolutionProgressChange extends Change {

	private final AnaphorResolutionProgressEvent eventToFire;
	private final BiConsumer<AnaphorResolutionProgressEvent, List<? extends AbstractAnaphoraPositionForRepresentation>> anaphorResolutionProgessNotifier;
	private final List<AbstractAnaphoraPositionForRepresentation> positions;

	public AnaphorResolutionProgressChange(final AnaphorResolutionProgressEvent eventToFire,
			final List<AbstractAnaphoraPositionForRepresentation> positions,
			final BiConsumer<AnaphorResolutionProgressEvent, List<? extends AbstractAnaphoraPositionForRepresentation>> anaphorResolutionProgessNotifier) {
		this.eventToFire = eventToFire;
		this.anaphorResolutionProgessNotifier = anaphorResolutionProgessNotifier;
		this.positions = positions;
	}

	@Override
	public String getName() {
		return "send anaphor resolution progress event " + eventToFire;
	}

	public AnaphorResolutionProgressEvent getEventToFire() {
		return eventToFire;
	}

	@Override
	public void initializeValidationData(final IProgressMonitor pm) {
	}

	@Override
	public RefactoringStatus isValid(final IProgressMonitor pm) throws CoreException, OperationCanceledException {
		return new RefactoringStatus(); // has severity OK
	}

	@Override
	public Change perform(final IProgressMonitor pm) throws CoreException {
		anaphorResolutionProgessNotifier.accept(eventToFire, positions);
		return createUndoChange();
	}

	private Change createUndoChange() {
		return new AnaphorResolutionProgressChange(getOppositeEvent(), positions, anaphorResolutionProgessNotifier);
	}

	private AnaphorResolutionProgressEvent getOppositeEvent() {
		switch (eventToFire) {
		case AnaphoraAddedOrChanged:
			return AboutToUndoAnaphoraAddedOrChanged;
		case AboutToUndoAnaphoraAddedOrChanged:
			return AnaphoraAddedOrChanged;
		default:
			throw new IllegalStateException("Unknown event: " + eventToFire);
		}
	}

	@Override
	public Object getModifiedElement() {
		// The change is not directly related to a single element
		return null;
	}

}
