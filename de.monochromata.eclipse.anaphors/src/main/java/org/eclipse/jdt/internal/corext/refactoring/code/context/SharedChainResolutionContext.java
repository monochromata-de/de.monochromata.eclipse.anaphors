package org.eclipse.jdt.internal.corext.refactoring.code.context;

import static org.eclipse.jdt.internal.corext.refactoring.code.context.IndexingFromAnaphorResolutionContext.index;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jdt.internal.corext.dom.fragments.IExpressionFragment;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;

import de.monochromata.anaphors.ast.AnaphorPart;

public class SharedChainResolutionContext {

	public final Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, IExpressionFragment> selectedExpressionByAnaphorPart;
	public final List<Pair<Annotation, Position>> problemAnnotationsToRemove;
	public final List<Pair<Annotation, Position>> problemAnnotationsToAdd;
	public boolean thereAreModifications;

	public SharedChainResolutionContext(final List<AnaphorResolutionContext> anaphorResolutionContexts,
			final List<Pair<Annotation, Position>> problemAnnotationsToRemove,
			final List<Pair<Annotation, Position>> problemAnnotationsToAdd) {
		this(createSelectedExpressionByAnaphorPart(anaphorResolutionContexts), problemAnnotationsToRemove,
				problemAnnotationsToAdd);
	}

	public SharedChainResolutionContext(
			final Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, IExpressionFragment> selectedExpressionByAnaphorPart,
			final List<Pair<Annotation, Position>> problemAnnotationsToRemove,
			final List<Pair<Annotation, Position>> problemAnnotationsToAdd) {
		this.selectedExpressionByAnaphorPart = selectedExpressionByAnaphorPart;
		this.problemAnnotationsToRemove = problemAnnotationsToRemove;
		this.problemAnnotationsToAdd = problemAnnotationsToAdd;
	}

	protected static Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, IExpressionFragment> createSelectedExpressionByAnaphorPart(
			final List<AnaphorResolutionContext> anaphorResolutionContexts) {
		return index(anaphorResolutionContexts, context -> context.selectedExpression);
	}

	public boolean problemAnnotationsNeedToBeUpdated() {
		return !problemAnnotationsToRemove.isEmpty() || !problemAnnotationsToAdd.isEmpty();
	}

}
