package org.eclipse.jdt.internal.corext.refactoring.code;

import static de.monochromata.eclipse.anaphors.editor.update.AnaphoraDeletion.delete;
import static de.monochromata.eclipse.anaphors.editor.update.AnaphoraDeletion.deletePositionsIn;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadLocationException;
import static java.lang.Character.toLowerCase;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jface.text.IDocument;
import org.eclipse.text.edits.DeleteEdit;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.ReplaceEdit;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.persp.HidePosition;
import de.monochromata.eclipse.persp.PerspectivationPosition;
import de.monochromata.eclipse.persp.ToLowerCasePosition;
import de.monochromata.eclipse.position.ChangeCollectingDoublyLinkedDeleter;

/**
 * De-realization reverts realization. De-realization is not based on the AST to
 * be able to perform it even if there are compiler errors.
 *
 * @see RealizeAnaphors
 */
public interface DeRealizeAnaphors {

	static void deRealize(final List<Triple<Anaphora, PositionForAnaphor, Optional<String>>> anaphorasToDelete,
			final IDocument originDocument, final Optional<MultiTextEdit> derealizationEdit,
			final ChangeCollectingDoublyLinkedDeleter<PerspectivationPosition> maintainPerspectivation,
			final List<PerspectivationPosition> perspectivationPositionsToDelete,
			final ChangeCollectingDoublyLinkedDeleter<AbstractAnaphoraPositionForRepresentation> maintainAnaphoras,
			final List<AbstractAnaphoraPositionForRepresentation> anaphoraPositionsToDelete) {
		deRealize(anaphorasToDelete, maintainPerspectivation, perspectivationPositionsToDelete, maintainAnaphoras,
				anaphoraPositionsToDelete,
				derealizationEdit.map(createTextEditsForDeletedPerspectivations(originDocument)).orElse(unused -> {
				}));
	}

	/**
	 * It makes sense to skip recording of text edits during de-realization if a new
	 * realization will follow immediately that will override the existing code
	 * anyways, e.g. during re-realization.
	 *
	 * @see ReRealizeAnaphors
	 */
	static void deRealizeWithoutGeneratingTextEdits(
			final List<Triple<Anaphora, PositionForAnaphor, Optional<String>>> anaphorasToDelete,
			final ChangeCollectingDoublyLinkedDeleter<PerspectivationPosition> maintainPerspectivation,
			final List<PerspectivationPosition> perspectivationPositionsToDelete,
			final ChangeCollectingDoublyLinkedDeleter<AbstractAnaphoraPositionForRepresentation> maintainAnaphoras,
			final List<AbstractAnaphoraPositionForRepresentation> anaphoraPositionsToDelete) {
		deRealize(anaphorasToDelete, maintainPerspectivation, perspectivationPositionsToDelete, maintainAnaphoras,
				anaphoraPositionsToDelete, unused -> {
				});
	}

	private static void deRealize(final List<Triple<Anaphora, PositionForAnaphor, Optional<String>>> anaphorasToDelete,
			final ChangeCollectingDoublyLinkedDeleter<PerspectivationPosition> maintainPerspectivation,
			final List<PerspectivationPosition> perspectivationPositionsToDelete,
			final ChangeCollectingDoublyLinkedDeleter<AbstractAnaphoraPositionForRepresentation> maintainAnaphoras,
			final List<AbstractAnaphoraPositionForRepresentation> anaphoraPositionsToDelete,
			final Consumer<PerspectivationPosition> createTextEdits) {
		try {
			maintainPerspectivation.addDeletionListener(createTextEdits);
			final var anaphorPositions = getAnaphorPositions(anaphorasToDelete);
			final var deletePerspectivationPosition = delete(maintainPerspectivation.maintainChainBeforeDeletion(),
					perspectivationPositionsToDelete, createTextEdits);
			final var deleteAnaphoraPosition = delete(maintainAnaphoras.maintainChainBeforeDeletion(),
					anaphoraPositionsToDelete);
			deletePositionsIn(anaphorPositions, deletePerspectivationPosition, deleteAnaphoraPosition);
		} finally {
			maintainPerspectivation.removeDeletionListener(createTextEdits);
		}
	}

	private static Function<MultiTextEdit, Consumer<PerspectivationPosition>> createTextEditsForDeletedPerspectivations(
			final IDocument originDocument) {
		return derealizationEdit -> positionToDelete -> {
			inlineToLowerCaseIntoOriginDocument(positionToDelete.getToLowerCasePositions(), originDocument,
					derealizationEdit);
			removeHiddenRegionsFromOriginDocument(positionToDelete.getHidePositions(), derealizationEdit);
		};
	}

	private static List<PositionForAnaphor> getAnaphorPositions(
			final List<Triple<Anaphora, PositionForAnaphor, Optional<String>>> anaphorasToDelete) {
		return anaphorasToDelete.stream().map(Triple::getMiddle).collect(toList());
	}

	private static void inlineToLowerCaseIntoOriginDocument(final List<ToLowerCasePosition> positionsToInline,
			final IDocument originDocument, final MultiTextEdit derealizationEdit) {
		positionsToInline.forEach(
				position -> wrapBadLocationException(() -> derealizationEdit.addChild(new ReplaceEdit(position.offset,
						position.length, Character.toString(toLowerCase(originDocument.getChar(position.offset)))))));
	}

	private static void removeHiddenRegionsFromOriginDocument(final List<HidePosition> positionsToRemove,
			final MultiTextEdit derealizationEdit) {
		positionsToRemove
				.forEach(position -> derealizationEdit.addChild(new DeleteEdit(position.offset, position.length)));
	}

}
