package org.eclipse.jdt.internal.corext.refactoring.change;

import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent.EventType.ABOUT_TO_REALIZE_ANAPHORS;
import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent.EventType.ABOUT_TO_UNDO_ANAPHOR_REALIZATION;

import java.util.function.BiConsumer;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent.EventType;

public class AnaphorResolutionEventChange extends Change {

	private final AnaphorResolutionEvent.EventType eventTypeToFire;
	private final AnaphorResolutionEvent eventToFire;
	private final BiConsumer<AnaphorResolutionEvent.EventType, AnaphorResolutionEvent> anaphorResolutionNotifier;

	public AnaphorResolutionEventChange(final AnaphorResolutionEvent.EventType eventTypeToFire,
			final AnaphorResolutionEvent eventToFire,
			final BiConsumer<AnaphorResolutionEvent.EventType, AnaphorResolutionEvent> anaphorResolutionNotifier) {
		this.eventTypeToFire = eventTypeToFire;
		this.eventToFire = eventToFire;
		this.anaphorResolutionNotifier = anaphorResolutionNotifier;
	}

	public AnaphorResolutionEvent.EventType getEventTypeToFire() {
		return eventTypeToFire;
	}

	public AnaphorResolutionEvent getEventToFire() {
		return eventToFire;
	}

	@Override
	public String getName() {
		return "send anaphor resolution event " + eventToFire;
	}

	@Override
	public void initializeValidationData(final IProgressMonitor pm) {
	}

	@Override
	public RefactoringStatus isValid(final IProgressMonitor pm) throws CoreException, OperationCanceledException {
		return new RefactoringStatus(); // has severity OK
	}

	@Override
	public Change perform(final IProgressMonitor pm) throws CoreException {
		anaphorResolutionNotifier.accept(eventTypeToFire, eventToFire);
		return createUndoChange();
	}

	protected Change createUndoChange() {
		return new AnaphorResolutionEventChange(getOppositeEventType(), eventToFire, anaphorResolutionNotifier);
	}

	protected EventType getOppositeEventType() {
		switch (eventTypeToFire) {
		case ABOUT_TO_REALIZE_ANAPHORS:
			return ABOUT_TO_UNDO_ANAPHOR_REALIZATION;
		case ABOUT_TO_UNDO_ANAPHOR_REALIZATION:
			return ABOUT_TO_REALIZE_ANAPHORS;
		default:
			throw new IllegalArgumentException("Unknown event type " + eventTypeToFire);
		}
	}

	@Override
	public Object getModifiedElement() {
		// The change is not directly related to a single element
		return null;
	}

}
