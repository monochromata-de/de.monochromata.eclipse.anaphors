package org.eclipse.jdt.internal.corext.refactoring.change;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

public class AnnotationUpdateChange extends Change {

    private final Consumer<Annotation> removeAnnotation;
    private final BiConsumer<Annotation, Position> addAnnotation;
    private final List<Pair<Annotation, Position>> annotationsToRemove;
    private final List<Pair<Annotation, Position>> annotationsToAdd;

    public AnnotationUpdateChange(final IAnnotationModel annotationModel,
            final List<Pair<Annotation, Position>> annotationsToRemove,
            final List<Pair<Annotation, Position>> annotationsToAdd) {
        this(annotationModel::removeAnnotation, annotationModel::addAnnotation, annotationsToRemove, annotationsToAdd);
    }

    public AnnotationUpdateChange(final Consumer<Annotation> removeAnnotation,
            final BiConsumer<Annotation, Position> addAnnotation,
            final List<Pair<Annotation, Position>> annotationsToRemove,
            final List<Pair<Annotation, Position>> annotationsToAdd) {
        this.removeAnnotation = removeAnnotation;
        this.addAnnotation = addAnnotation;
        this.annotationsToRemove = annotationsToRemove;
        this.annotationsToAdd = annotationsToAdd;
    }

    @Override
    public String getName() {
        // TODO i18n
        return "Update annotations";
    }

    @Override
    public void initializeValidationData(final IProgressMonitor pm) {
    }

    @Override
    public RefactoringStatus isValid(final IProgressMonitor pm) throws CoreException, OperationCanceledException {
        return new RefactoringStatus(); // has severity OK
    }

    @Override
    public Change perform(final IProgressMonitor pm) throws CoreException {
        removeAnnotationsToRemove();
        addAnnotationsToAdd();
        return createUndoChange();
    }

    protected void removeAnnotationsToRemove() {
        annotationsToRemove
                .forEach(annotationAndPosition -> removeAnnotation.accept(annotationAndPosition.getLeft()));
    }

    protected void addAnnotationsToAdd() {
        annotationsToAdd
                .forEach(annotationAndPosition -> addAnnotation.accept(annotationAndPosition.getLeft(),
                        annotationAndPosition.getRight()));
    }

    protected Change createUndoChange() {
        // Swap the lists of annotations to add the removed and remove the added ones.
        return new AnnotationUpdateChange(removeAnnotation, addAnnotation, annotationsToAdd, annotationsToRemove);
    }

    @Override
    public Object getModifiedElement() {
        // The change is not directly related to a single element
        return null;
    }

}
