package org.eclipse.jdt.internal.corext.refactoring.code;

import static java.util.Optional.empty;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.LambdaExpression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jdt.core.dom.rewrite.ImportRewrite;
import org.eclipse.jdt.core.dom.rewrite.ImportRewrite.ImportRewriteContext;
import org.eclipse.jdt.core.dom.rewrite.ImportRewrite.TypeLocation;
import org.eclipse.jdt.internal.corext.codemanipulation.ContextSensitiveImportRewriteContext;
import org.eclipse.jdt.internal.corext.dom.ASTNodes;
import org.eclipse.jdt.internal.corext.dom.Bindings;
import org.eclipse.jdt.internal.corext.refactoring.RefactoringCoreMessages;
import org.eclipse.jdt.internal.corext.refactoring.code.context.SharedAnaphorResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.code.context.SharedChainResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.code.context.SuccessfulChainResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.structure.CompilationUnitRewrite;
import org.eclipse.text.edits.TextEditGroup;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalTempVariableIntroducingStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.eclipse.anaphors.Activator;
import de.monochromata.eclipse.anaphors.Util;

/**
 * Realization integrates resolved anaphors into the AST.
 *
 * @see ResolveAnaphor
 * @see ReRealizeAnaphors
 * @see DeRealizeAnaphors
 */
public interface RealizeAnaphors {

    static void realizeAnaphors(final IProgressMonitor pm,
            final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
            final SharedChainResolutionContext sharedChainResolutionContext,
            final List<Triple<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>> anaphorasAndContextsToAdd,
            final CompilationUnitRewrite cuRewrite, final boolean declareFinal) throws CoreException {
        final Map<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, List<Pair<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>>> anaphorsAndContextsByRelatedExpression = getAnaphorsAndContextsByRelatedExpression(
                anaphorasAndContextsToAdd);
        realizeAnaphors(pm, sharedAnaphorResolutionContext, sharedChainResolutionContext, anaphorasAndContextsToAdd,
                anaphorsAndContextsByRelatedExpression, cuRewrite, declareFinal);
    }

    static Map<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, List<Pair<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>>> getAnaphorsAndContextsByRelatedExpression(
            final List<Triple<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>> anaphorasAndContextsToAdd) {
        final Map<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, List<Pair<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>>> anaphorsAndContextsByRelatedExpression = new LinkedHashMap<>();
        anaphorasAndContextsToAdd.forEach(partsAndContext -> anaphorsAndContextsByRelatedExpression
                .computeIfAbsent(partsAndContext.getLeft(), unused -> new ArrayList<>())
                .add(new ImmutablePair<>(partsAndContext.getMiddle(), partsAndContext.getRight())));
        return anaphorsAndContextsByRelatedExpression;
    }

    static void realizeAnaphors(final IProgressMonitor pm,
            final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
            final SharedChainResolutionContext sharedChainResolutionContext,
            final List<Triple<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>> anaphorasAndContextsToAdd,
            final Map<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, List<Pair<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>>> anaphorsAndAContextByRelatedExpression,
            final CompilationUnitRewrite cuRewrite, final boolean declareFinal) throws CoreException {
        try {
            pm.beginTask(RefactoringCoreMessages.ExtractTempRefactoring_checking_preconditions, 1);
            try {
                realizeAnaphoras(sharedAnaphorResolutionContext, sharedChainResolutionContext,
                        anaphorasAndContextsToAdd, anaphorsAndAContextByRelatedExpression, cuRewrite, declareFinal);
            } catch (final CoreException e) {
                // TODO: Should not continue, actually ...
                Util.logException("Failed to resolve anaphor", e);
            }
        } finally {
            pm.done();
        }
    }

    static void realizeAnaphoras(final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
            final SharedChainResolutionContext sharedChainResolutionContext,
            final List<Triple<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>> anaphorasAndContextsToAdd,
            final Map<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, List<Pair<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>>> anaphorsAndContextByRelatedExpression,
            final CompilationUnitRewrite cuRewrite, final boolean declareFinal) throws CoreException {
        realizeRelatedExpressions(sharedAnaphorResolutionContext, anaphorsAndContextByRelatedExpression, cuRewrite,
                declareFinal);
        realizeAnaphors(cuRewrite, sharedAnaphorResolutionContext, sharedChainResolutionContext,
                anaphorasAndContextsToAdd);
    }

    static void realizeRelatedExpressions(final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
            final Map<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, List<Pair<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>>> anaphorsAndContextsByRelatedExpression,
            final CompilationUnitRewrite cuRewrite, final boolean declareFinal) throws CoreException {
        for (final Entry<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, List<Pair<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>>> relatedExpressionAndAnaphorsAndContexts : anaphorsAndContextsByRelatedExpression
                .entrySet()) {
            realizeRelatedExpression(sharedAnaphorResolutionContext, relatedExpressionAndAnaphorsAndContexts.getKey(),
                    relatedExpressionAndAnaphorsAndContexts.getValue(), cuRewrite, declareFinal);
        }
    }

    static void realizeRelatedExpression(final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
            final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
            final List<Pair<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>> anaphorPartsAndContexts,
            final CompilationUnitRewrite cuRewrite, final boolean declareFinal) throws CoreException {
        final RelatedExpressionStrategy<ASTNode, Type, IBinding, ITypeBinding, CompilationUnit, String, PublicRelatedExpression> relatedExpressionStrategy = relatedExpressionPart
                .getRelatedExpressionStrategy();
        if (relatedExpressionStrategy instanceof LocalTempVariableIntroducingStrategy) {
            @SuppressWarnings("rawtypes")
            final LocalTempVariableIntroducingStrategy<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> localTempVariableIntroducingStrategy = (LocalTempVariableIntroducingStrategy<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>) relatedExpressionStrategy;
            final String guessedTempName = replaceRelatedExpressionWithTempDeclaration(
                    localTempVariableIntroducingStrategy, cuRewrite, sharedAnaphorResolutionContext,
                    relatedExpressionPart, anaphorPartsAndContexts, declareFinal);
            saveGuessedTempName(anaphorPartsAndContexts, Optional.of(guessedTempName));
        } else {
            createAndSaveRealizedAnaphoras(sharedAnaphorResolutionContext, relatedExpressionPart,
                    anaphorPartsAndContexts);
            saveGuessedTempName(anaphorPartsAndContexts, empty());
        }
    }

    static void saveGuessedTempName(
            final List<Pair<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>> anaphorPartsAndContexts,
            final Optional<String> guessedTempName) {
        anaphorPartsAndContexts.stream().map(Pair::getRight)
                .forEach(context -> context.guessedTempName = guessedTempName);
    }

    static void createAndSaveRealizedAnaphoras(final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
            final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
            final List<Pair<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>> anaphorPartsAndContexts) {
        anaphorPartsAndContexts.stream()
                .forEach(anaphorPartAndContext -> createAndSaveRealizedAnaphoras(sharedAnaphorResolutionContext,
                        relatedExpressionPart, anaphorPartsAndContexts, anaphorPartAndContext.getLeft(),
                        anaphorPartAndContext.getRight()));
    }

    static void createAndSaveRealizedAnaphoras(final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
            final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
            final List<Pair<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>> anaphorPartsAndContexts,
            final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart,
            final SuccessfulChainResolutionContext context) {
        final Anaphora resolvedAnaphora = createAnaphoraToBeRealized(relatedExpressionPart,
                getAnaphorParts(anaphorPartsAndContexts), anaphorPart,
                sharedAnaphorResolutionContext.compilationUnitNode);
        saveAnaphoraToBeRealized(anaphorPart, context, resolvedAnaphora);
    }

    @SuppressWarnings("restriction")
    static String replaceRelatedExpressionWithTempDeclaration(
            final LocalTempVariableIntroducingStrategy<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> localTempVariableIntroducingStrategy,
            final CompilationUnitRewrite compilationUnitRewrite,
            final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
            final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
            final List<Pair<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>> anaphorPartsAndContexts,
            final boolean declareFinal) throws CoreException {
        final ASTRewrite rewrite = compilationUnitRewrite.getASTRewrite();
        final ASTNode relatedExpressionNode = relatedExpressionPart.getRelatedExpression().getRelatedExpression();
        if (!(relatedExpressionNode instanceof Expression)) {
            // TODO: Test this case
            // TODO: This means that this method is specific to the related
            // expression i.e. must be handled differently for combination
            // anaphors, complex anaphors, plural anaphors etc.
            throw new RuntimeException("Related expression is not an expression");
        }
        final Expression relatedExpression = (Expression) relatedExpressionNode;

        final Expression initializer = createInitializerForReplacingRelatedExpression(compilationUnitRewrite,
                relatedExpressionPart);
        final Pair<String, VariableDeclarationStatement> tempNameAndDeclaration = createTempDeclaration(
                relatedExpression, initializer, localTempVariableIntroducingStrategy, compilationUnitRewrite,
                sharedAnaphorResolutionContext, relatedExpressionPart, anaphorPartsAndContexts, declareFinal);
        final VariableDeclarationStatement tempDeclaration = tempNameAndDeclaration.getRight();
        ASTNode replacement;

        final ASTNode parent = relatedExpression.getParent();
        final boolean isParentLambda = parent instanceof LambdaExpression;
        final AST ast = rewrite.getAST();
        if (isParentLambda) {
            final Block blockBody = ast.newBlock();
            blockBody.statements().add(tempDeclaration);
            if (!Bindings.isVoidType(((LambdaExpression) parent).resolveMethodBinding().getReturnType())) {
                final List<VariableDeclarationFragment> fragments = tempDeclaration.fragments();
                final SimpleName varName = fragments.get(0).getName();
                final ReturnStatement returnStatement = ast.newReturnStatement();
                returnStatement.setExpression(ast.newSimpleName(varName.getIdentifier()));
                blockBody.statements().add(returnStatement);
            }
            replacement = blockBody;
        } else if (ASTNodes.isControlStatementBody(parent.getLocationInParent())) {
            final Block block = ast.newBlock();
            block.statements().add(tempDeclaration);
            replacement = block;
        } else {
            replacement = tempDeclaration;
        }
        final ASTNode replacee = isParentLambda
                || !ASTNodes.hasSemicolon((ExpressionStatement) parent, sharedAnaphorResolutionContext.cu)
                        ? relatedExpression
                        : parent;
        rewrite.replace(replacee, replacement, compilationUnitRewrite
                .createGroupDescription(RefactoringCoreMessages.ExtractTempRefactoring_declare_local_variable));
        return tempNameAndDeclaration.getLeft();
    }

    static Expression createInitializerForReplacingRelatedExpression(final CompilationUnitRewrite rewrite,
            final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart)
            throws JavaModelException {
        final ASTRewrite astRewrite = rewrite.getASTRewrite();
        final ASTNode replacee = relatedExpressionPart.getRelatedExpression().getRelatedExpression();
        if (!(replacee instanceof Expression)) {
            throw new RuntimeException("Only expressions can be used as initializers, not " + replacee);
        }
        // Name qualifier= null;
        // Expression replacement= ast.newQualifiedName(qualifier,
        // (SimpleName)initializer);
        return (Expression) astRewrite.createMoveTarget(replacee);
    }

    static Pair<String, VariableDeclarationStatement> createTempDeclaration(final Expression relatedExpression,
            final Expression initializer,
            final LocalTempVariableIntroducingStrategy<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> localTempVariableIntroducingStrategy,
            final CompilationUnitRewrite compilationUnitRewrite,
            final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
            final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
            final List<Pair<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>> anaphorPartsAndContexts,
            final boolean declareFinal) throws CoreException {
        final AST ast = compilationUnitRewrite.getAST();
        final ImportRewrite importRewrite = compilationUnitRewrite.getImportRewrite();

        final VariableDeclarationFragment vdf = ast.newVariableDeclarationFragment();
        final List<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>> anaphorParts = getAnaphorParts(
                anaphorPartsAndContexts);
        final String tempName = localTempVariableIntroducingStrategy.getIdentifierForTempVariable(relatedExpressionPart,
                anaphorParts, sharedAnaphorResolutionContext.compilationUnitNode);
        createAndSaveAnaphoraToBeRealized(relatedExpressionPart, anaphorPartsAndContexts, anaphorParts,
                sharedAnaphorResolutionContext.compilationUnitNode);
        vdf.setName(ast.newSimpleName(tempName));
        vdf.setInitializer(initializer);

        final VariableDeclarationStatement vds = ast.newVariableDeclarationStatement(vdf);
        if (declareFinal) {
            vds.modifiers().add(ast.newModifier(ModifierKeyword.FINAL_KEYWORD));
        }
        final Type type = getTypeForTempVariable(relatedExpressionPart,
                sharedAnaphorResolutionContext.compilationUnitNode,
                typeBinding -> normalizeType(relatedExpression, ast, importRewrite, typeBinding));
        vds.setType(type);
        return new ImmutablePair<>(tempName, vds);
    }

    private static Type getTypeForTempVariable(
            final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
            final CompilationUnit scope, final Function<ITypeBinding, Type> importRewrite) {
        final PublicRelatedExpression relatedExpression = relatedExpressionPart.getRelatedExpression();
        return getTypeForTempVariable(relatedExpression, scope, importRewrite);
    }

    private static Type getTypeForTempVariable(final PublicRelatedExpression relatedExpression,
            final CompilationUnit scope, final Function<ITypeBinding, Type> importRewrite) {
        if (useLocalVariableTypeInference(scope)) {
            return Activator.getDefault().getRelatedExpressionsSpi().getReservedTypeVar(scope);
        }

        final var node = relatedExpression.getRelatedExpression();
        if (node instanceof ClassInstanceCreation) {
            return Activator.getDefault().getRelatedExpressionsSpi().getClassInstanceCreationTypeForTempVar(node, scope,
                    importRewrite);
        }
        if (node instanceof MethodInvocation) {
            return Activator.getDefault().getRelatedExpressionsSpi().getMethodInvocationReturnTypeForTempVar(node,
                    scope, importRewrite);
        }
        return Activator.getDefault().getRelatedExpressionsSpi().getLocalDeclarationTypeForTempVar(node, scope,
                importRewrite);
    }

    private static boolean useLocalVariableTypeInference(final CompilationUnit scope) {
        return Activator.getDefault().getPreferencesSpi().getUseLocalVariableTypeInference()
                && Activator.getDefault().getRelatedExpressionsSpi().supportsLocalVariableTypeInference(scope);
    }

    static Type normalizeType(final Expression relatedExpression, final AST ast, final ImportRewrite importRewrite,
            final ITypeBinding resolvedBinding) {
        final ITypeBinding normalizedTypeBinding = Bindings.normalizeForDeclarationUse(resolvedBinding, ast);
        final ImportRewriteContext context = new ContextSensitiveImportRewriteContext(relatedExpression, importRewrite);
        return importRewrite.addImport(normalizedTypeBinding, ast, context, TypeLocation.LOCAL_VARIABLE);
    }

    static List<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>> getAnaphorParts(
            final List<Pair<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>> anaphorPartsAndContexts) {
        return anaphorPartsAndContexts.stream().map(anaphorPartAndContext -> anaphorPartAndContext.getLeft())
                .collect(toList());
    }

    static void createAndSaveAnaphoraToBeRealized(
            final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
            final List<Pair<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>> anaphorPartsAndContexts,
            final List<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>> anaphorParts,
            final CompilationUnit scope) {
        anaphorPartsAndContexts.forEach(anaphorPartAndContext -> createAnaphoraToBeRealized(relatedExpressionPart,
                anaphorParts, anaphorPartAndContext.getLeft(), scope, anaphorPartAndContext.getRight()));
    }

    static void createAnaphoraToBeRealized(
            final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
            final List<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>> anaphorParts,
            final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart,
            final CompilationUnit scope, final SuccessfulChainResolutionContext context) {
        final Anaphora anaphoraToBeRealized = createAnaphoraToBeRealized(relatedExpressionPart, anaphorParts,
                anaphorPart, scope);
        saveAnaphoraToBeRealized(anaphorPart, context, anaphoraToBeRealized);
    }

    static Anaphora createAnaphoraToBeRealized(
            final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
            final List<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>> anaphorParts,
            final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart,
            final CompilationUnit scope) {
        final String anaphorToBeRealized = anaphorPart.getAnaphorResolutionStrategy()
                .getAnaphorToBeRealized(relatedExpressionPart, anaphorParts, anaphorPart, scope);
        return createAnaphoraToBeRealized(relatedExpressionPart, anaphorPart, anaphorToBeRealized);
    }

    static Anaphora createAnaphoraToBeRealized(
            final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
            final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart,
            final String anaphorToBeRealized) {
        return new Anaphora(
                relatedExpressionPart.getRelatedExpressionStrategy().getKindOfRelatedExpressionToBeRealized(),
                anaphorPart.getAnaphorResolutionStrategy().getKindOfAnaphorResolutionStrategyToBeRealized(anaphorPart),
                anaphorPart.getAnaphorResolutionStrategy().getKindOfReferentializationStrategyToBeRealized(anaphorPart),
                anaphorToBeRealized);
    }

    static void saveAnaphoraToBeRealized(
            final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart,
            final SuccessfulChainResolutionContext context, final Anaphora anaphoraToBeRealized) {
        context.anaphoraToBeRealizedByAnaphorPart.put(anaphorPart, anaphoraToBeRealized);
    }

    @SuppressWarnings("restriction")
    static void realizeAnaphors(final CompilationUnitRewrite compilationUnitRewrite,
            final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
            final SharedChainResolutionContext sharedChainResolutionContext,
            final List<Triple<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>> anaphorasAndContextsToAdd)
            throws JavaModelException {
        for (final Triple<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext> anaphoraAndContext : anaphorasAndContextsToAdd) {
            realizeAnaphor(compilationUnitRewrite, sharedAnaphorResolutionContext, sharedChainResolutionContext,
                    anaphoraAndContext.getLeft(), anaphoraAndContext.getMiddle(), anaphoraAndContext.getRight());
        }
    }

    @SuppressWarnings("restriction")
    static void realizeAnaphor(final CompilationUnitRewrite compilationUnitRewrite,
            final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
            final SharedChainResolutionContext sharedChainResolutionContext,
            final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
            final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart,
            final SuccessfulChainResolutionContext chainResolutionContext) throws JavaModelException {
        final TextEditGroup editGroup = compilationUnitRewrite
                .createGroupDescription("Qualifying anaphor with related expression");
        realizeAnaphor(compilationUnitRewrite, editGroup, sharedAnaphorResolutionContext, sharedChainResolutionContext,
                relatedExpressionPart, anaphorPart, chainResolutionContext);
    }

    static Expression realizeAnaphor(final CompilationUnitRewrite rewrite, final TextEditGroup editGroup,
            final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
            final SharedChainResolutionContext sharedChainResolutionContext,
            final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
            final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart,
            final SuccessfulChainResolutionContext chainResolutionContext) throws JavaModelException {
        final ASTRewrite astRewrite = rewrite.getASTRewrite();
        final AST ast = astRewrite.getAST();
        final Expression replacee = sharedChainResolutionContext.selectedExpressionByAnaphorPart.get(anaphorPart)
                .getAssociatedExpression();
        final Expression replacement = anaphorPart.getAnaphorResolutionStrategy().realize(relatedExpressionPart,
                anaphorPart, replacee, chainResolutionContext.guessedTempName, ast, astRewrite);
        astRewrite.replace(replacee, replacement, editGroup);
        return replacement;
    }

}
