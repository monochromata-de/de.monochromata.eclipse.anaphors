package org.eclipse.jdt.internal.corext.refactoring.change;

import static de.monochromata.eclipse.anaphors.swt.SWTAccess.runSync;
import static de.monochromata.eclipse.persp.PerspectivationPosition.PERSPECTIVATION_CATEGORY;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapPositionExceptions;

import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.swt.widgets.Display;

import de.monochromata.eclipse.persp.PerspectivationPosition;

public class UpdatePerspectivationPositionsChange extends UpdateDoublyLinkedPositionsChange<PerspectivationPosition> {

	private final Consumer<Runnable> runInSwtThread;

	public UpdatePerspectivationPositionsChange(final Display display, final IDocument document,
			final List<PerspectivationPosition> positionsToAdd,
			final List<Pair<PerspectivationPosition, PerspectivationPosition>> positionsToLink,
			final List<PerspectivationPosition> positionsToRemove,
			final List<Pair<PerspectivationPosition, PerspectivationPosition>> positionsToUnlink) {
		this(runnable -> runSync(display, runnable), document, positionsToAdd, positionsToLink, positionsToRemove,
				positionsToUnlink);
	}

	public UpdatePerspectivationPositionsChange(final Consumer<Runnable> runInSwtThread, final IDocument document,
			final List<PerspectivationPosition> positionsToAdd,
			final List<Pair<PerspectivationPosition, PerspectivationPosition>> positionsToLink,
			final List<PerspectivationPosition> positionsToRemove,
			final List<Pair<PerspectivationPosition, PerspectivationPosition>> positionsToUnlink) {
		super(document, "perspectivation", PERSPECTIVATION_CATEGORY, positionsToAdd, positionsToLink, positionsToRemove,
				positionsToUnlink);
		this.runInSwtThread = runInSwtThread;
	}

	@Override
	protected void removePosition(final PerspectivationPosition position) {
		runInSwtThread.accept(() -> wrapPositionExceptions(() -> position.undoPerspectivation()));
		super.removePosition(position);
	}

	@Override
	protected Change createUndo() {
		return new UpdatePerspectivationPositionsChange(runInSwtThread, document, positionsToRemove, positionsToUnlink,
				positionsToAdd, positionsToLink);
	}

}
