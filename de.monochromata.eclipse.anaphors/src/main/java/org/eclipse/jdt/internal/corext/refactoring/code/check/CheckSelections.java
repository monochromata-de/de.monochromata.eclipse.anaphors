package org.eclipse.jdt.internal.corext.refactoring.code.check;

import static org.eclipse.jdt.internal.corext.refactoring.code.ExpressionFragmentCreation.getSelectedExpression;
import static org.eclipse.jdt.internal.corext.refactoring.code.ResolveAnaphor.resolveAnaphor;
import static org.eclipse.jdt.internal.corext.refactoring.code.check.ExtractTempChecking.isReferringToLocalVariableFromFor;
import static org.eclipse.jdt.internal.corext.refactoring.code.check.ExtractTempChecking.isUsedInForInitializerOrUpdater;

import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Annotation;
import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Initializer;
import org.eclipse.jdt.core.dom.LambdaExpression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.StructuralPropertyDescriptor;
import org.eclipse.jdt.core.dom.SuperConstructorInvocation;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jdt.internal.corext.dom.ASTNodes;
import org.eclipse.jdt.internal.corext.dom.fragments.IExpressionFragment;
import org.eclipse.jdt.internal.corext.refactoring.RefactoringCoreMessages;
import org.eclipse.jdt.internal.corext.refactoring.code.CodeRefactoringUtil;
import org.eclipse.jdt.internal.corext.refactoring.code.context.AnaphorResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.code.context.SharedAnaphorResolutionContext;
import org.eclipse.jface.text.Position;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import de.monochromata.anaphors.ast.ASTBasedAnaphorResolution;
import de.monochromata.anaphors.cog.ActivationBasedAnaphorResolution;

public interface CheckSelections {

	static RefactoringStatus checkSelections(final IProgressMonitor pm,
			final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
			final List<AnaphorResolutionContext> anaphorResolutionContexts,
			final ActivationBasedAnaphorResolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> activationBasedAnaphorResolution,
			final ASTBasedAnaphorResolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> astBasedAnaphorResolution,
			final List<Pair<org.eclipse.jface.text.source.Annotation, Position>> problemAnnotationsToAdd)
			throws JavaModelException {
		for (final AnaphorResolutionContext anaphorResolutionContext : anaphorResolutionContexts) {
			anaphorResolutionContext.refactoringStatus = checkSelection(pm, sharedAnaphorResolutionContext,
					anaphorResolutionContext, activationBasedAnaphorResolution, astBasedAnaphorResolution,
					problemAnnotationsToAdd);
		}
		// Always proceed, failed contexts will be ignored later
		return new RefactoringStatus(); // implies OK
	}

	static Stream<RefactoringStatus> getRefactoringStatus(
			final List<AnaphorResolutionContext> anaphorResolutionContexts) {
		return anaphorResolutionContexts.stream().map(context -> context.refactoringStatus);
	}

	static RefactoringStatus checkSelection(final IProgressMonitor pm,
			final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
			final AnaphorResolutionContext anaphorResolutionContext,
			final ActivationBasedAnaphorResolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> activationBasedAnaphorResolution,
			final ASTBasedAnaphorResolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> astBasedAnaphorResolution,
			final List<Pair<org.eclipse.jface.text.source.Annotation, Position>> problemAnnotationsToAdd)
			throws JavaModelException {
		try {
			pm.beginTask("", 7); //$NON-NLS-1$

			final IExpressionFragment selectedExpression = getSelectedExpression(sharedAnaphorResolutionContext,
					anaphorResolutionContext);

			if (selectedExpression == null) {
				final String message = RefactoringCoreMessages.ExtractTempRefactoring_select_expression;
				return CodeRefactoringUtil.checkMethodSyntaxErrors(
						anaphorResolutionContext.definiteExpressionPosition.offset,
						anaphorResolutionContext.definiteExpressionPosition.length,
						sharedAnaphorResolutionContext.compilationUnitNode, message);
			}
			pm.worked(1);

			if (isUsedInExplicitConstructorCall(sharedAnaphorResolutionContext, anaphorResolutionContext)) {
				return RefactoringStatus
						.createFatalErrorStatus(RefactoringCoreMessages.ExtractTempRefactoring_explicit_constructor);
			}
			pm.worked(1);

			final ASTNode associatedNode = selectedExpression.getAssociatedNode();
			if (getEnclosingBodyNode(sharedAnaphorResolutionContext, anaphorResolutionContext) == null
					|| ASTNodes.getParent(associatedNode, Annotation.class) != null) {
				return RefactoringStatus.createFatalErrorStatus(
						RefactoringCoreMessages.ExtractTempRefactoring_expr_in_method_or_initializer);
			}
			pm.worked(1);

			if (associatedNode instanceof Name && associatedNode.getParent() instanceof ClassInstanceCreation
					&& associatedNode.getLocationInParent() == ClassInstanceCreation.TYPE_PROPERTY) {
				return RefactoringStatus
						.createFatalErrorStatus(RefactoringCoreMessages.ExtractTempRefactoring_name_in_new);
			}
			pm.worked(1);

			final RefactoringStatus result = new RefactoringStatus();
			result.merge(checkExpression(sharedAnaphorResolutionContext, anaphorResolutionContext,
					activationBasedAnaphorResolution, astBasedAnaphorResolution, problemAnnotationsToAdd));
			if (result.hasFatalError()) {
				return result;
			}
			pm.worked(1);

			if (isUsedInForInitializerOrUpdater(
					getSelectedExpression(sharedAnaphorResolutionContext, anaphorResolutionContext)
							.getAssociatedExpression())) {
				return RefactoringStatus
						.createFatalErrorStatus(RefactoringCoreMessages.ExtractTempRefactoring_for_initializer_updater);
			}
			pm.worked(1);

			if (isReferringToLocalVariableFromFor(
					getSelectedExpression(sharedAnaphorResolutionContext, anaphorResolutionContext)
							.getAssociatedExpression())) {
				return RefactoringStatus
						.createFatalErrorStatus(RefactoringCoreMessages.ExtractTempRefactoring_refers_to_for_variable);
			}
			pm.worked(1);

			return result;
		} finally {
			pm.done();
		}
	}

	static boolean isUsedInExplicitConstructorCall(final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
			final AnaphorResolutionContext anaphorResolutionContext) throws JavaModelException {
		final Expression selectedExpression = anaphorResolutionContext.selectedExpression.getAssociatedExpression();
		if (ASTNodes.getParent(selectedExpression, ConstructorInvocation.class) != null) {
			return true;
		}
		if (ASTNodes.getParent(selectedExpression, SuperConstructorInvocation.class) != null) {
			return true;
		}
		return false;
	}

	static ASTNode getEnclosingBodyNode(final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
			final AnaphorResolutionContext anaphorResolutionContext) throws JavaModelException {
		ASTNode node = anaphorResolutionContext.selectedExpression.getAssociatedNode();

		// expression must be in a method, lambda or initializer body
		// make sure it is not in method or parameter annotation
		StructuralPropertyDescriptor location = null;
		while (node != null && !(node instanceof BodyDeclaration)) {
			location = node.getLocationInParent();
			node = node.getParent();
			if (node instanceof LambdaExpression) {
				break;
			}
		}
		if (location == MethodDeclaration.BODY_PROPERTY || location == Initializer.BODY_PROPERTY
				|| (location == LambdaExpression.BODY_PROPERTY
						&& ((LambdaExpression) node).resolveMethodBinding() != null)) {
			return (ASTNode) node.getStructuralProperty(location);
		}
		return null;
	}

	static RefactoringStatus checkExpression(final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
			final AnaphorResolutionContext anaphorResolutionContext,
			final ActivationBasedAnaphorResolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> activationBasedAnaphorResolution,
			final ASTBasedAnaphorResolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> astBasedAnaphorResolution,
			final List<Pair<org.eclipse.jface.text.source.Annotation, Position>> problemAnnotationsToAdd)
			throws JavaModelException {
		final Expression selectedExpression = anaphorResolutionContext.selectedExpression.getAssociatedExpression();
		if (selectedExpression == null) {
			// TODO: Use a property
			return RefactoringStatus.createFatalErrorStatus("Could not get selected expression");
		}
		return resolveAnaphor(selectedExpression, sharedAnaphorResolutionContext, anaphorResolutionContext,
				(CompilationUnit) selectedExpression.getRoot(), activationBasedAnaphorResolution,
				astBasedAnaphorResolution, problemAnnotationsToAdd);
	}

}
