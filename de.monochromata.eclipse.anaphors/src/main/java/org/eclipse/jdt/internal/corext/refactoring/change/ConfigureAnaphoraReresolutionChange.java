package org.eclipse.jdt.internal.corext.refactoring.change;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import de.monochromata.eclipse.anaphors.editor.update.AnaphoraReresolvingDocumentListener;

/**
 * Configures, whether {@link AnaphoraReresolvingDocumentListener} invalidates changed
 * members, or not.
 *
 * @see AnaphoraReresolvingDocumentListener#setEnabled(boolean)
 */
public class ConfigureAnaphoraReresolutionChange extends Change {

    private final AnaphoraReresolvingDocumentListener listener;
    private final boolean enable;

    public ConfigureAnaphoraReresolutionChange(final AnaphoraReresolvingDocumentListener listener, final boolean enable) {
        this.listener = listener;
        this.enable = enable;
    }

    @Override
    public String getName() {
        return "Configure member invalidation";
    }

    @Override
    public void initializeValidationData(final IProgressMonitor pm) {
    }

    @Override
    public RefactoringStatus isValid(final IProgressMonitor pm) throws CoreException, OperationCanceledException {
        return new RefactoringStatus(); // has severity OK
    }

    @Override
    public Change perform(final IProgressMonitor pm) throws CoreException {
        listener.setEnabled(enable);
        return new ConfigureAnaphoraReresolutionChange(listener, !enable);
    }

    @Override
    public Object getModifiedElement() {
        // This change is not related to a specific element.
        return null;
    }

}
