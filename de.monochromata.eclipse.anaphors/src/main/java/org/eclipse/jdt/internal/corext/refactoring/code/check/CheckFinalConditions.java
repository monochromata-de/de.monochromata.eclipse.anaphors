package org.eclipse.jdt.internal.corext.refactoring.code.check;

import static org.eclipse.jdt.internal.corext.refactoring.code.check.ExtractTempChecking.isLeftValue;
import static org.eclipse.jdt.internal.corext.refactoring.code.check.ExtractTempChecking.isReferringToLocalVariableFromFor;

import java.util.Collection;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.internal.corext.dom.fragments.IASTFragment;
import org.eclipse.jdt.internal.corext.refactoring.RefactoringCoreMessages;
import org.eclipse.jdt.internal.corext.refactoring.code.context.SharedAnaphorResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.code.context.SharedChainResolutionContext;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

public interface CheckFinalConditions {

    static RefactoringStatus checkRefactoringResult(final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
            final SharedChainResolutionContext sharedChainResolutionContext)
            throws JavaModelException {
        return checkMatchingFragments(sharedAnaphorResolutionContext.cu,
                sharedChainResolutionContext);
    }

    static RefactoringStatus checkMatchingFragments(final ICompilationUnit cu,
            final SharedChainResolutionContext sharedChainResolutionContext) throws JavaModelException {
        final RefactoringStatus result = new RefactoringStatus();
        final Collection<? extends IASTFragment> matchingFragments = getMatchingFragments(
                sharedChainResolutionContext);
        for (final IASTFragment matchingFragment : matchingFragments) {
            final ASTNode node = matchingFragment.getAssociatedNode();
            if (isLeftValue(node) && !isReferringToLocalVariableFromFor((Expression) node)) {
                final String msg = RefactoringCoreMessages.ExtractTempRefactoring_assigned_to;
                result.addWarning(msg);
            }
        }
        return result;
    }

    static Collection<? extends IASTFragment> getMatchingFragments(
            final SharedChainResolutionContext sharedChainResolutionContext)
            throws JavaModelException {
        return sharedChainResolutionContext.selectedExpressionByAnaphorPart.values();
    }

}
