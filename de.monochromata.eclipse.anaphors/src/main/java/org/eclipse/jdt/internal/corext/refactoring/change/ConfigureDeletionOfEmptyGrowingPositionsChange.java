package org.eclipse.jdt.internal.corext.refactoring.change;

import static java.util.Arrays.stream;
import static org.eclipse.ltk.core.refactoring.RefactoringStatus.createErrorStatus;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import de.monochromata.eclipse.position.GrowingPositionUpdater;

/**
 * Configures whether or not a {@link GrowingPositionUpdater} deletes positions
 * whose length is set to 0.
 *
 * @see GrowingPositionUpdater#setDeleteEmptyPositions(boolean)
 */
public class ConfigureDeletionOfEmptyGrowingPositionsChange extends Change {

    private final IDocument document;
    private final String positionCategory;
    private final boolean enableDeletion;

    private GrowingPositionUpdater positionUpdater;

    /**
     * @param document
     *            The document that contains a {@link GrowingPositionUpdater}
     * @param positionCategory
     *            The position category that uses the {@link GrowingPositionUpdater}
     * @param enableDeletion
     *            Whether to enable deletion of positions {@literal true}, or not
     *            {@literal false}.
     * @see GrowingPositionUpdater#setDeleteEmptyPositions(boolean)
     */
    public ConfigureDeletionOfEmptyGrowingPositionsChange(final IDocument document, final String positionCategory,
            final boolean enableDeletion) {
        this.document = document;
        this.positionCategory = positionCategory;
        this.enableDeletion = enableDeletion;
    }

    public boolean isEnableDeletion() {
        return enableDeletion;
    }

    @Override
    public String getName() {
        return "Configure deletion of empty growing positions";
    }

    @Override
    public void initializeValidationData(final IProgressMonitor pm) {
    }

    @Override
    public RefactoringStatus isValid(final IProgressMonitor pm) throws CoreException, OperationCanceledException {
        if (!document.containsPositionCategory(positionCategory)) {
            return createErrorStatus("The document does not contain the position category " + positionCategory);
        }
        positionUpdater = findPositionUpdater();
        if (positionUpdater == null) {
            return createErrorStatus("The document contains no " + GrowingPositionUpdater.class.getName()
                    + " for position category " + positionCategory);
        }
        return new RefactoringStatus(); // has severity OK
    }

    protected GrowingPositionUpdater findPositionUpdater() {
        return stream(document.getPositionUpdaters())
                .filter(GrowingPositionUpdater.class::isInstance)
                .map(updater -> (GrowingPositionUpdater) updater)
                .filter(updater -> updater.getCategory().equals(positionCategory))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Change perform(final IProgressMonitor pm) throws CoreException {
        positionUpdater.setDeleteEmptyPositions(enableDeletion);
        return new ConfigureDeletionOfEmptyGrowingPositionsChange(document, positionCategory, !enableDeletion);
    }

    @Override
    public Object getModifiedElement() {
        // This change is not related to a specific element.
        return null;
    }

}
