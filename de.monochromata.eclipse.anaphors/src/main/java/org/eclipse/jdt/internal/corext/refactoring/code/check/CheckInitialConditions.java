package org.eclipse.jdt.internal.corext.refactoring.code.check;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.internal.corext.refactoring.Checks;
import org.eclipse.jdt.internal.corext.refactoring.code.context.SharedAnaphorResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.util.RefactoringASTParser;
import org.eclipse.jdt.internal.corext.refactoring.util.ResourceUtil;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

public interface CheckInitialConditions {

    static RefactoringStatus checkInitialConditions(final IProgressMonitor pm,
            final SharedAnaphorResolutionContext sharedAnaphorResolutionContext, final Object validationContext)
            throws CoreException {
        try {
            pm.beginTask("", 6); //$NON-NLS-1$

            final RefactoringStatus result = Checks.validateModifiesFiles(
                    ResourceUtil.getFiles(new ICompilationUnit[] { sharedAnaphorResolutionContext.cu }),
                    validationContext, new NullProgressMonitor());
            if (result.hasFatalError()) {
                return result;
            }

            if (sharedAnaphorResolutionContext.compilationUnitNode == null) {
                sharedAnaphorResolutionContext.compilationUnitNode = RefactoringASTParser
                        .parseWithASTProvider(sharedAnaphorResolutionContext.cu, true, new SubProgressMonitor(pm, 3));
            } else {
                pm.worked(3);
            }

            return result;

        } finally {
            pm.done();
        }
    }

}
