package org.eclipse.jdt.internal.corext.refactoring.code;

import static de.monochromata.anaphors.ast.chain.ChainTraversal.getRelatedExpressionElementForAnaphorElement;
import static de.monochromata.eclipse.anaphors.AnaphoraCreation.createAnaphoraByAnaphorPart;
import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent.NO_CHANGES;
import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent.EventType.ABOUT_TO_REALIZE_ANAPHORS;
import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionPerspectivationEvent.AboutToStartDocumentRewritingForAnaphorResolution;
import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionPerspectivationEvent.CompletedDocumentRewritingForAnaphorResolution;
import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionProgressEvent.AnaphoraAddedOrChanged;
import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static de.monochromata.eclipse.persp.PerspectivationPosition.PERSPECTIVATION_CATEGORY;
import static de.monochromata.eclipse.persp.PerspectivationPositionCreation.createPerspectivationPositions;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadPositionCategoryException;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.Optional.empty;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.eclipse.jdt.internal.corext.refactoring.code.AnaphoraPositionCreationForRefactoring.createPositionsToAdd;
import static org.eclipse.jdt.internal.corext.refactoring.code.check.CheckFinalConditions.checkRefactoringResult;
import static org.eclipse.jdt.internal.corext.refactoring.code.check.CheckSelections.checkSelections;
import static org.eclipse.jdt.internal.corext.refactoring.code.context.AnaphorResolutionContextCreation.createAnaphorResolutionContexts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.compiler.IProblem;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicChainElement;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jdt.core.refactoring.CompilationUnitChange;
import org.eclipse.jdt.internal.corext.SourceRangeFactory;
import org.eclipse.jdt.internal.corext.refactoring.RefactoringCoreMessages;
import org.eclipse.jdt.internal.corext.refactoring.base.JavaStringStatusContext;
import org.eclipse.jdt.internal.corext.refactoring.change.AnaphorResolutionEventChange;
import org.eclipse.jdt.internal.corext.refactoring.change.AnaphorResolutionPerspectivationChange;
import org.eclipse.jdt.internal.corext.refactoring.change.AnaphorResolutionProgressChange;
import org.eclipse.jdt.internal.corext.refactoring.change.AnnotationUpdateChange;
import org.eclipse.jdt.internal.corext.refactoring.change.ConfigureAnaphoraReresolutionChange;
import org.eclipse.jdt.internal.corext.refactoring.change.ConfigureDeletionOfEmptyGrowingPositionsChange;
import org.eclipse.jdt.internal.corext.refactoring.change.UpdateAnaphoraPositionsChange;
import org.eclipse.jdt.internal.corext.refactoring.change.UpdatePerspectivationPositionsChange;
import org.eclipse.jdt.internal.corext.refactoring.code.check.CheckInitialConditions;
import org.eclipse.jdt.internal.corext.refactoring.code.context.AmbiguousChainResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.code.context.AnaphorResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.code.context.ChainResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.code.context.FailedChainResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.code.context.NothingFoundChainResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.code.context.SharedAnaphorResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.code.context.SharedChainResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.code.context.SuccessfulChainResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.rename.RefactoringAnalyzeUtil;
import org.eclipse.jdt.internal.corext.refactoring.structure.CompilationUnitRewrite;
import org.eclipse.jdt.internal.corext.refactoring.util.NoCommentSourceRangeComputer;
import org.eclipse.jdt.internal.corext.refactoring.util.RefactoringASTParser;
import org.eclipse.jdt.internal.ui.javaeditor.ASTProvider;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.NullChange;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.RefactoringStatusEntry;
import org.eclipse.swt.widgets.Display;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.TextEdit;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.ast.ASTBasedAnaphorResolution;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.anaphors.ast.chain.ChainTraversal;
import de.monochromata.anaphors.ast.chain.Chaining;
import de.monochromata.anaphors.cog.ActivationBasedAnaphorResolution;
import de.monochromata.eclipse.anaphors.Activator;
import de.monochromata.eclipse.anaphors.MultipleOriginalAnaphoras;
import de.monochromata.eclipse.anaphors.OriginalAnaphora;
import de.monochromata.eclipse.anaphors.SingleOriginalAnaphora;
import de.monochromata.eclipse.anaphors.editor.AmbiguityAnnotationRemoval;
import de.monochromata.eclipse.anaphors.editor.update.AnaphoraReresolvingDocumentListener;
import de.monochromata.eclipse.anaphors.editor.update.AnaphoraUpdating;
import de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPosition;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionPerspectivationEvent;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionProgressEvent;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;
import de.monochromata.eclipse.persp.PerspectivationPosition;
import de.monochromata.eclipse.persp.PerspectivationPositionCreation;
import de.monochromata.eclipse.position.ChangeCollectingDoublyLinkedDeleter;

/**
 * Maybe delegate to ExtractTempRefactoring instead?
 * <p>
 * This refactoring creates problem annotations if it finds an ambiguous
 * anaphor. TODO: Actually, that analysis should not be part of the refactoring
 * but should happen before the refactoring is attempted.
 */
public class ResolveAnaphorsRefactoring extends Refactoring {

	// TODO: Decrease / make configurable via preferences
	public static final int NUMBER_OF_CHUNKS_TO_CONSIDER = 20;
	protected final Display display;
	protected final IDocument originDocument;
	protected final AnaphoraReresolvingDocumentListener anaphoraReresolvingListener;
	protected final BiConsumer<AnaphorResolutionEvent.EventType, AnaphorResolutionEvent> anaphorResolutionListener;
	protected final BiConsumer<AnaphorResolutionPerspectivationEvent, List<? extends Position>> anaphorResolutionPerspectivationListener;
	protected final BiConsumer<AnaphorResolutionProgressEvent, List<? extends AbstractAnaphoraPositionForRepresentation>> anaphorResolutionProgressListener;
	protected final Supplier<ISelection> originSelectionSupplier;
	protected final Consumer<ISelection> originSelectionSetter;

	protected State state;
	protected boolean fDeclareFinal;
	protected final SharedAnaphorResolutionContext sharedAnaphorResolutionContext;
	protected boolean fCheckResultForCompileProblems;

	private final List<PerspectivationPosition> perspectivationPositionsToAdd = new ArrayList<>();
	private final List<PerspectivationPosition> perspectivationPositionsToRemove = new ArrayList<>();
	private final List<Pair<PerspectivationPosition, PerspectivationPosition>> perspectivationPositionsToLink = new ArrayList<>();
	private final List<Pair<PerspectivationPosition, PerspectivationPosition>> perspectivationPositionsToUnlink = new ArrayList<>();
	private final ChangeCollectingDoublyLinkedDeleter<PerspectivationPosition> maintainPerspectivation = new ChangeCollectingDoublyLinkedDeleter<>(
			perspectivationPositionsToAdd, perspectivationPositionsToRemove, perspectivationPositionsToLink,
			perspectivationPositionsToUnlink);

	private final List<AbstractAnaphoraPositionForRepresentation> anaphoraPositionsToAdd = new ArrayList<>();
	private final List<AbstractAnaphoraPositionForRepresentation> anaphoraPositionsToRemove = new ArrayList<>();
	private final List<Pair<AbstractAnaphoraPositionForRepresentation, AbstractAnaphoraPositionForRepresentation>> anaphoraPositionsToLink = new ArrayList<>();
	private final List<Pair<AbstractAnaphoraPositionForRepresentation, AbstractAnaphoraPositionForRepresentation>> anaphoraPositionsToUnlink = new ArrayList<>();
	private final ChangeCollectingDoublyLinkedDeleter<AbstractAnaphoraPositionForRepresentation> maintainAnaphoras = new ChangeCollectingDoublyLinkedDeleter<>(
			anaphoraPositionsToAdd, anaphoraPositionsToRemove, anaphoraPositionsToLink, anaphoraPositionsToUnlink);

	protected CompilationUnitChange fChange;
	protected ActivationBasedAnaphorResolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> fActivationBasedResolution;
	protected ASTBasedAnaphorResolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> fAnaphorResolution;

	public ResolveAnaphorsRefactoring(final Display display, final IDocument originDocument,
			final IAnnotationModel annotationModel, final Supplier<ISelection> originSelectionSupplier,
			final Consumer<ISelection> originSelectionSetter, final CompilationUnit astRoot,
			final AnaphoraReresolvingDocumentListener anaphoraReresolvingListener,
			final BiConsumer<AnaphorResolutionEvent.EventType, AnaphorResolutionEvent> anaphorResolutionListener,
			final BiConsumer<AnaphorResolutionPerspectivationEvent, List<? extends Position>> anaphorResolutionPerspectivationListener,
			final BiConsumer<AnaphorResolutionProgressEvent, List<? extends AbstractAnaphoraPositionForRepresentation>> anaphorResolutionProgressListener,
			final MemberTrackingPosition trackingPositionToReResolve, final List<PositionForAnaphor> anaphorPositions,
			final List<? extends Position> problemPositions) {
		Assert.isTrue(!anaphorPositions.isEmpty() || !problemPositions.isEmpty(), "no definite expressions selected");
		Assert.isTrue(astRoot.getTypeRoot() instanceof ICompilationUnit);

		sharedAnaphorResolutionContext = new SharedAnaphorResolutionContext((ICompilationUnit) astRoot.getTypeRoot(),
				astRoot, annotationModel, trackingPositionToReResolve);
		state = new AnaphorContextsCreated(
				createAnaphorResolutionContexts(anaphorPositions, problemPositions, originDocument));
		fDeclareFinal = false;
		fCheckResultForCompileProblems = true;
		this.display = display;
		this.originDocument = originDocument;
		this.originSelectionSupplier = originSelectionSupplier;
		this.originSelectionSetter = originSelectionSetter;
		this.anaphoraReresolvingListener = anaphoraReresolvingListener;
		this.anaphorResolutionListener = anaphorResolutionListener;
		this.anaphorResolutionPerspectivationListener = anaphorResolutionPerspectivationListener;
		this.anaphorResolutionProgressListener = anaphorResolutionProgressListener;
		initialize();
	}

	protected void initialize() {
		setDeclareFinal(Activator.getDefault().getPreferencesSpi().getAddFinalModifierToCreatedTemporaryVariables());
		setCheckResultForCompileProblems(false);
		initAnaphorResolution();
	}

	public boolean declareFinal() {
		return fDeclareFinal;
	}

	@Override
	public String getName() {
		return "Resolve anaphors";
	}

	public void setDeclareFinal(final boolean declareFinal) {
		fDeclareFinal = declareFinal;
	}

	public void setCheckResultForCompileProblems(final boolean checkResultForCompileProblems) {
		fCheckResultForCompileProblems = checkResultForCompileProblems;
	}

	protected void initAnaphorResolution() {
		fAnaphorResolution = Activator.getDefault().getAnaphorResolution();
		fActivationBasedResolution = Activator.getDefault().getActivationBasedResolution();
	}

	@Override
	public RefactoringStatus checkInitialConditions(final IProgressMonitor pm) throws CoreException {
		final var result = CheckInitialConditions.checkInitialConditions(pm, sharedAnaphorResolutionContext,
				getValidationContext());

		if (result.hasFatalError()) {
			anaphorResolutionListener.accept(ABOUT_TO_REALIZE_ANAPHORS, NO_CHANGES);
		}

		return result;
	}

	@Override
	public RefactoringStatus checkFinalConditions(final IProgressMonitor pm) throws CoreException {
		try {
			pm.beginTask(RefactoringCoreMessages.ExtractTempRefactoring_checking_preconditions, 5);

			final SubProgressMonitor selectionCheckPm = new SubProgressMonitor(pm, 3);
			final List<Pair<Annotation, Position>> problemAnnotationsToRemove = AmbiguityAnnotationRemoval
					.getAnnotationsToRemove(sharedAnaphorResolutionContext.annotationModel,
							sharedAnaphorResolutionContext.trackingPositionToReResolve);
			final List<Pair<Annotation, Position>> problemAnnotationsToAdd = new ArrayList<>();
			final RefactoringStatus result = checkSelections(selectionCheckPm, sharedAnaphorResolutionContext,
					getState(AnaphorContextsCreated.class).anaphorResolutionContexts, fActivationBasedResolution,
					fAnaphorResolution, problemAnnotationsToAdd);
			pm.worked(1);

			state = createChainContextsCreated(problemAnnotationsToRemove, problemAnnotationsToAdd);

			// TODO: localize
			fChange = new CompilationUnitChange("(De-)Realize anaphors", sharedAnaphorResolutionContext.cu);
			final MultiTextEdit rootEdit = new MultiTextEdit();
			fChange.setEdit(rootEdit);

			final List<Triple<Anaphora, PositionForAnaphor, Optional<String>>> anaphorasToDelete = new ArrayList<>();
			final List<Anaphora> retainedAnaphoras = new ArrayList<>();
			final List<Triple<Anaphora, Anaphora, Supplier<Anaphora>>> updatedResolvedAnaphoras = new ArrayList<>();
			final List<Pair<Triple<Anaphora, PositionForAnaphor, Optional<String>>, Triple<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>>> anaphorasAndContextsToUpdate = new ArrayList<>();
			final List<Pair<Anaphora, Supplier<Anaphora>>> addedResolvedAnaphoras = new ArrayList<>();
			final List<Triple<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>> anaphorasAndContextsToAdd = new ArrayList<>();
			updateAnaphoras(getState(ChainContextsCreated.class).sharedChainResolutionContext,
					getState(ChainContextsCreated.class).chainResolutionContexts, anaphorasToDelete, retainedAnaphoras,
					updatedResolvedAnaphoras, anaphorasAndContextsToUpdate, addedResolvedAnaphoras,
					anaphorasAndContextsToAdd);
			getState(ChainContextsCreated.class).sharedChainResolutionContext.thereAreModifications = !anaphorasToDelete
					.isEmpty() || !anaphorasAndContextsToUpdate.isEmpty() || !anaphorasAndContextsToAdd.isEmpty();
			System.err.println("anaphorasToDelete=" + anaphorasToDelete);
			System.err.println("anaphorasAndContextsToUpdate=" + anaphorasAndContextsToUpdate);
			System.err.println("anaphorasAndContextsToAdd=" + anaphorasAndContextsToAdd);

			final CompilationUnitRewrite cuRewrite = new CompilationUnitRewrite(sharedAnaphorResolutionContext.cu,
					sharedAnaphorResolutionContext.compilationUnitNode);
			cuRewrite.getASTRewrite().setTargetSourceRangeComputer(new NoCommentSourceRangeComputer());

			derealizeAnaphors(anaphorasToDelete, rootEdit, maintainPerspectivation, perspectivationPositionsToRemove,
					maintainAnaphoras, anaphoraPositionsToRemove);
			reRealizeAnaphors(pm, anaphorasAndContextsToUpdate, cuRewrite, maintainPerspectivation,
					perspectivationPositionsToRemove, maintainAnaphoras, anaphoraPositionsToRemove);
			realizeAnaphors(pm, anaphorasAndContextsToAdd, cuRewrite);

			final List<Pair<Anaphora, Anaphora>> addedAnaphoras = addRealizedAnaphoras(addedResolvedAnaphoras);
			final List<Triple<Anaphora, Anaphora, Anaphora>> updatedAnaphoras = getDeletedAndResolvedAndRealizedAnaphoras(
					updatedResolvedAnaphoras);
			final List<Anaphora> removedAnaphoras = anaphorasToDelete.stream().map(Triple::getLeft).collect(toList());
			getState(ChainContextsCreated.class).anaphorResolutionEvent = new AnaphorResolutionEvent(addedAnaphoras,
					retainedAnaphoras, updatedAnaphoras, removedAnaphoras);

			final CompilationUnitChange compilationUnitChange = fChange;
			final Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, Anaphora> anaphorasToBeRealized = getAnaphoraToBeRealizedByAnaphorPart();
			final TextEdit edit = compilationUnitChange.getEdit();
			final List<Pair<PublicChainElement, PerspectivationPosition>> chainElementsAndPerspectivationPositions = createPerspectivationPositions(
					edit, sharedAnaphorResolutionContext, getState(ChainContextsCreated.class).chainResolutionContexts,
					Activator.getDefault().getPerspectivationStrategy().getPerspectivationConfiguration(),
					perspectivationPositionsToRemove, perspectivationPositionsToLink,
					Activator.getDefault().getRelatedExpressionsSpi(), Activator.getDefault().getAnaphorsSpi());

			perspectivationPositionsToAdd
					.addAll(PerspectivationPositionCreation.getPositions(chainElementsAndPerspectivationPositions));
			anaphoraPositionsToAdd
					.addAll(createPositionsToAdd(getState(ChainContextsCreated.class).chainResolutionContexts,
							anaphorasToBeRealized, indexByElement(chainElementsAndPerspectivationPositions),
							anaphoraPositionsToRemove, anaphoraPositionsToLink));

			result.merge(checkRefactoringResult(sharedAnaphorResolutionContext,
					getState(ChainContextsCreated.class).sharedChainResolutionContext));

			fChange.setKeepPreviewEdits(true);
			if (fCheckResultForCompileProblems) {
				checkNewSource(new SubProgressMonitor(pm, 1), result, sharedAnaphorResolutionContext.cu,
						sharedAnaphorResolutionContext.compilationUnitNode);
			}

			if (result.hasFatalError()) {
				anaphorResolutionListener.accept(ABOUT_TO_REALIZE_ANAPHORS, NO_CHANGES);
			}

			return result;
		} finally {
			pm.done();
		}
	}

	protected List<Pair<Anaphora, Anaphora>> addRealizedAnaphoras(
			final List<Pair<Anaphora, Supplier<Anaphora>>> addedResolvedAnaphoras) {
		return addedResolvedAnaphoras.stream().map(this::addRealizedAnaphora).collect(toList());
	}

	protected Pair<Anaphora, Anaphora> addRealizedAnaphora(
			final Pair<Anaphora, Supplier<Anaphora>> addedResolvedAnaphora) {
		return new ImmutablePair<>(addedResolvedAnaphora.getLeft(), addedResolvedAnaphora.getRight().get());
	}

	protected List<Triple<Anaphora, Anaphora, Anaphora>> getDeletedAndResolvedAndRealizedAnaphoras(
			final List<Triple<Anaphora, Anaphora, Supplier<Anaphora>>> updatedResolvedAnaphoras) {
		return updatedResolvedAnaphoras.stream().map(this::addRealizedAnaphora).collect(toList());
	}

	protected Triple<Anaphora, Anaphora, Anaphora> addRealizedAnaphora(
			final Triple<Anaphora, Anaphora, Supplier<Anaphora>> addedResolvedAnaphora) {
		return new ImmutableTriple<>(addedResolvedAnaphora.getLeft(), addedResolvedAnaphora.getMiddle(),
				addedResolvedAnaphora.getRight().get());
	}

	protected ChainContextsCreated createChainContextsCreated(
			final List<Pair<Annotation, Position>> problemAnnotationsToRemove,
			final List<Pair<Annotation, Position>> problemAnnotationsToAdd) {
		final List<AnaphorResolutionContext> anaphorResolutionContexts = getState(
				AnaphorContextsCreated.class).anaphorResolutionContexts;
		final SharedChainResolutionContext sharedChainResolutionContext = new SharedChainResolutionContext(
				anaphorResolutionContexts, problemAnnotationsToRemove, problemAnnotationsToAdd);
		final List<ChainResolutionContext> chainResolutionContexts = createChainResolutionContexts(
				anaphorResolutionContexts);
		return new ChainContextsCreated(chainResolutionContexts, sharedChainResolutionContext);
	}

	protected List<ChainResolutionContext> createChainResolutionContexts(
			final List<AnaphorResolutionContext> anaphorResolutionContexts) {
		final Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, Position> positionsByAnaphorPart = getPositionsByAnaphorPart(
				anaphorResolutionContexts);
		return anaphorResolutionContexts.stream().map(this::toStatusAndOriginalAnaphoraAndChains)
				.map(Collections::singletonList).reduce(ChainMerging::mergeStatusAndPotentialChains)
				.orElseGet(() -> emptyList()).stream()
				.flatMap(refactoringStatusAndChains -> createChainResolutionContexts(refactoringStatusAndChains,
						positionsByAnaphorPart))
				.collect(toList());
	}

	protected Stream<ChainResolutionContext> createChainResolutionContexts(
			final Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>> statusAndAndOriginalAnaphoraAndChains,
			final Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, Position> positionsByAnaphorPart) {
		final RefactoringStatus refactoringStatus = statusAndAndOriginalAnaphoraAndChains.getLeft();
		if (refactoringStatus.isOK()) {
			return createSuccessfulChainResolutionContext(statusAndAndOriginalAnaphoraAndChains.getRight(),
					positionsByAnaphorPart);
		}
		return createFailedChainResolutionContext(statusAndAndOriginalAnaphoraAndChains);
	}

	protected Stream<ChainResolutionContext> createSuccessfulChainResolutionContext(
			final List<PublicChainElement> chainRoots,
			final Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, Position> positionsByAnaphorPart) {
		return chainRoots.stream()
				.map(chainRoot -> new SuccessfulChainResolutionContext(
						selectPositions(chainRoot, positionsByAnaphorPart), chainRoot,
						createAnaphoraByAnaphorPart(chainRoot)));
	}

	protected Stream<ChainResolutionContext> createFailedChainResolutionContext(
			final Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>> statusAndAndOriginalAnaphoraAndChains) {
		if (statusAndAndOriginalAnaphoraAndChains.getMiddle().isPresent()
				&& statusAndAndOriginalAnaphoraAndChains.getRight().isEmpty()) {
			return Stream.of(new NothingFoundChainResolutionContext(statusAndAndOriginalAnaphoraAndChains.getLeft(),
					statusAndAndOriginalAnaphoraAndChains.getMiddle()
							.orElseThrow(IllegalArgumentException::new).originalAnaphora));
		} else if (!statusAndAndOriginalAnaphoraAndChains.getRight().isEmpty()) {
			return Stream.of(new AmbiguousChainResolutionContext(statusAndAndOriginalAnaphoraAndChains.getLeft(),
					statusAndAndOriginalAnaphoraAndChains.getRight()));
		}
		throw new IllegalStateException("Could not create failed chain resolution context");
	}

	protected Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>> toStatusAndOriginalAnaphoraAndChains(
			final AnaphorResolutionContext context) {
		final Pair<Optional<SingleOriginalAnaphora>, List<PublicChainElement>> originalAnaphoraAndPotentialChains = toOriginalAnaphoraAndChains(
				context);
		// The original anaphora from the middle will be used if the list on the right
		// is empty.
		// The original anaphora from the middle will be ignored if the list on the
		// right is not empty - in that case the original anaphoras from the chain
		// elements on the right shall be used.
		return new ImmutableTriple<>(context.refactoringStatus, originalAnaphoraAndPotentialChains.getLeft(),
				originalAnaphoraAndPotentialChains.getRight());
	}

	private OriginalAnaphora getOriginalAnaphora(final AnaphorResolutionContext context) {
		if (context.refactoringStatus.isOK()) {
			return new MultipleOriginalAnaphoras(context);
		}
		return new SingleOriginalAnaphora(context.originalAnaphora);
	}

	protected Pair<Optional<SingleOriginalAnaphora>, List<PublicChainElement>> toOriginalAnaphoraAndChains(
			final AnaphorResolutionContext context) {
		final OriginalAnaphora originalAnaphora = getOriginalAnaphora(context);
		final List<PublicChainElement> potentialChains = context.potentialAnaphoras.stream().<PublicChainElement>map(
				potentialAnaphora -> Chaining.toChain(potentialAnaphora, originalAnaphora, PublicChainElement::new))
				.collect(toList());
		// The original anaphora from the left will be used if the list on the right
		// is empty.
		// The original anaphora from the left will be empty if the list on the
		// right is not empty - in that case the original anaphoras from the chain
		// elements on the right shall be used.
		final Optional<SingleOriginalAnaphora> singleOriginalAnaphora = originalAnaphora instanceof SingleOriginalAnaphora
				? Optional.of((SingleOriginalAnaphora) originalAnaphora)
				: empty();
		if (!singleOriginalAnaphora.isPresent() && potentialChains.isEmpty()) {
			throw new IllegalStateException("There should either be a single original anaphora or potential chains");
		}
		return new ImmutablePair<>(singleOriginalAnaphora, potentialChains);
	}

	private List<Position> selectPositions(final PublicChainElement chain,
			final Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, Position> positionsByAnaphorPart) {
		return ChainTraversal.getAnaphorElements(chain).map(anaphorElement -> anaphorElement.anaphor)
				.map(positionsByAnaphorPart::get).collect(toList());
	}

	private Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, Position> getPositionsByAnaphorPart(
			final List<AnaphorResolutionContext> anaphorResolutionContexts) {
		return anaphorResolutionContexts.stream()
				.flatMap(context -> context.potentialAnaphoras.stream()
						.map(anaphora -> new ImmutablePair<>(anaphora.getAnaphorPart(), context)))
				.collect(toMap(Pair::getLeft,
						anaphorPartAndContext -> anaphorPartAndContext.getRight().definiteExpressionPosition));
	}

	protected void derealizeAnaphors(
			final List<Triple<Anaphora, PositionForAnaphor, Optional<String>>> anaphorasToDelete,
			final MultiTextEdit rootEdit,
			final ChangeCollectingDoublyLinkedDeleter<PerspectivationPosition> maintainPerspectivation,
			final List<PerspectivationPosition> perspectivationPositionsToDelete,
			final ChangeCollectingDoublyLinkedDeleter<AbstractAnaphoraPositionForRepresentation> maintainAnaphoras,
			final List<AbstractAnaphoraPositionForRepresentation> anaphoraPositionsToDelete) {
		final MultiTextEdit derealizationEdit = new MultiTextEdit();
		DeRealizeAnaphors.deRealize(anaphorasToDelete, originDocument, Optional.of(derealizationEdit),
				maintainPerspectivation, perspectivationPositionsToDelete, maintainAnaphoras,
				anaphoraPositionsToDelete);
		// The derealizationEdit is added to the root added after its children haven
		// been added because at that point in time then multi edit calculates its range
		// from the ranges of its children.
		rootEdit.addChild(derealizationEdit);
	}

	protected void reRealizeAnaphors(final IProgressMonitor pm,
			final List<Pair<Triple<Anaphora, PositionForAnaphor, Optional<String>>, Triple<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>>> oldAndNewAnaphorasAndContextsToUpdate,
			final CompilationUnitRewrite cuRewrite,
			final ChangeCollectingDoublyLinkedDeleter<PerspectivationPosition> maintainPerspectivation,
			final List<PerspectivationPosition> perspectivationPositionsToDelete,
			final ChangeCollectingDoublyLinkedDeleter<AbstractAnaphoraPositionForRepresentation> maintainAnaphoras,
			final List<AbstractAnaphoraPositionForRepresentation> anaphoraPositionsToDelete) throws CoreException {
		ReRealizeAnaphors.reRealizeAnaphors(pm, oldAndNewAnaphorasAndContextsToUpdate, cuRewrite,
				sharedAnaphorResolutionContext, getState(ChainContextsCreated.class).sharedChainResolutionContext,
				fDeclareFinal, maintainPerspectivation, perspectivationPositionsToDelete, maintainAnaphoras,
				anaphoraPositionsToDelete);
	}

	private void deletePositions(final String positionCategory, final List<? extends Position> positionsToDelete) {
		positionsToDelete.forEach(position -> deletePosition(positionCategory, position));
	}

	private void deletePosition(final String positionCategory, final Position positionToDelete) {
		wrapBadPositionCategoryException(() -> originDocument.removePosition(positionCategory, positionToDelete));
	}

	protected void realizeAnaphors(final IProgressMonitor pm,
			final List<Triple<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>> anaphorasAndContextsToAdd,
			final CompilationUnitRewrite cuRewrite) throws CoreException {
		RealizeAnaphors.realizeAnaphors(new SubProgressMonitor(pm, 2), sharedAnaphorResolutionContext,
				getState(ChainContextsCreated.class).sharedChainResolutionContext, anaphorasAndContextsToAdd, cuRewrite,
				fDeclareFinal);

		cuRewrite.attachChange(fChange, true, new SubProgressMonitor(pm, 1));
	}

	protected void updateAnaphoras(final SharedChainResolutionContext sharedChainResolutionContext,
			final List<ChainResolutionContext> chainResolutionContexts,
			final List<Triple<Anaphora, PositionForAnaphor, Optional<String>>> anaphorasToDelete,
			final List<Anaphora> retainedAnaphoras,
			final List<Triple<Anaphora, Anaphora, Supplier<Anaphora>>> updatedResolvedAnaphoras,
			final List<Pair<Triple<Anaphora, PositionForAnaphor, Optional<String>>, Triple<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>>> anaphorasAndContextsToUpdate,
			final List<Pair<Anaphora, Supplier<Anaphora>>> addedResolvedAnaphoras,
			final List<Triple<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>> anaphorasAndContextsToAdd) {
		for (final ChainResolutionContext chainResolutionContext : chainResolutionContexts) {
			if (chainResolutionContext instanceof SuccessfulChainResolutionContext) {
				final SuccessfulChainResolutionContext successfulContext = (SuccessfulChainResolutionContext) chainResolutionContext;
				getAnaphorElements(successfulContext)
						.forEach(anaphorElement -> updateAnaphoras0(sharedChainResolutionContext,
								successfulContext.originalAnaphoraByAnaphorPart.get(anaphorElement.anaphor),
								singletonList(anaphorElement), anaphorasToDelete, anaphoraParts -> {
									addedResolvedAnaphoras.add(new ImmutablePair<>(
											successfulContext.resolvedAnaphoraByAnaphorPart
													.get(anaphoraParts.getRight()),
											() -> successfulContext.anaphoraToBeRealizedByAnaphorPart
													.get(anaphoraParts.getRight())));
									anaphorasAndContextsToAdd.add(new ImmutableTriple<>(anaphoraParts.getLeft(),
											anaphoraParts.getRight(), successfulContext));
								}, retainedAnaphora -> retainedAnaphoras.add(retainedAnaphora.getMiddle()),
								(oldAnaphor, newAnaphor) -> {
									updatedResolvedAnaphoras.add(new ImmutableTriple<>(oldAnaphor.getMiddle(),
											successfulContext.resolvedAnaphoraByAnaphorPart.get(newAnaphor.getRight()),
											() -> successfulContext.anaphoraToBeRealizedByAnaphorPart
													.get(newAnaphor.getRight())));
									anaphorasAndContextsToUpdate.add(new ImmutablePair<>(
											new ImmutableTriple<>(oldAnaphor.getMiddle(), oldAnaphor.getRight(),
													empty()),
											new ImmutableTriple<>(newAnaphor.getLeft(), newAnaphor.getRight(),
													successfulContext)));
								}, empty()));
			} else if (chainResolutionContext instanceof FailedChainResolutionContext) {
				final FailedChainResolutionContext failedContext = (FailedChainResolutionContext) chainResolutionContext;
				final String errorMessage = failedContext.refactoringStatus
						.getMessageMatchingSeverity(failedContext.refactoringStatus.getSeverity());
				updateAnaphoras0(sharedChainResolutionContext, failedContext.originalAnaphora,
						getAnaphorElements(failedContext), anaphorasToDelete, unused -> {
							throw new UnsupportedOperationException("Cannot realize anaphora from "
									+ FailedChainResolutionContext.class.getSimpleName());
						}, unused -> {
							throw new UnsupportedOperationException("Cannot retain anaphora from "
									+ FailedChainResolutionContext.class.getSimpleName());
						}, (unused0, unused1) -> {
							throw new UnsupportedOperationException("Cannot update anaphora from "
									+ FailedChainResolutionContext.class.getSimpleName());
						}, Optional.of(errorMessage));
			} else {
				throw new IllegalArgumentException(
						"Unknown sub-type of chain resolution context: " + chainResolutionContext.getClass().getName());
			}
		}
	}

	protected void updateAnaphoras0(final SharedChainResolutionContext sharedChainResolutionContext,
			final Optional<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>> originalAnaphora,
			final List<PublicChainElement> anaphorElements,
			final List<Triple<Anaphora, PositionForAnaphor, Optional<String>>> anaphorasToDelete,
			final Consumer<Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>> adder,
			final Consumer<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>> retainer,
			final BiConsumer<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>, Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>> updater,
			final Optional<String> errorMessage) {
		AnaphoraUpdating.update(originalAnaphora, toPartsPairs(anaphorElements), adder, retainer, updater,
				potentialAnaphoras -> warnOnAmbiguity(potentialAnaphoras, errorMessage),
				(anaphora, anaphorPosition) -> delete(anaphora, anaphorPosition, errorMessage, anaphorasToDelete));
	}

	protected void warnOnAmbiguity(
			final List<Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>> potentialAnaphoras,
			final Optional<String> errorMessage) {
		System.err.println("TODO: Ambiguity warning, error message: " + errorMessage.orElse("TODO"));
	}

	protected void delete(final Anaphora anaphora, final PositionForAnaphor anaphorPosition,
			final Optional<String> errorMessage,
			final List<Triple<Anaphora, PositionForAnaphor, Optional<String>>> anaphorasToDelete) {
		anaphorasToDelete.add(new ImmutableTriple<>(anaphora, anaphorPosition, errorMessage));
	}

	protected List<PublicChainElement> getAnaphorElements(final SuccessfulChainResolutionContext context) {
		return ChainTraversal.getAnaphorElements(context.anaphorChainRoot).collect(toList());
	}

	protected List<PublicChainElement> getAnaphorElements(final FailedChainResolutionContext context) {
		if (context instanceof NothingFoundChainResolutionContext) {
			return emptyList();
		} else if (context instanceof AmbiguousChainResolutionContext) {
			return getAnaphorElements((AmbiguousChainResolutionContext) context);
		} else {
			throw new IllegalStateException("Unknown type: " + context.getClass().getName());
		}
	}

	protected List<PublicChainElement> getAnaphorElements(final AmbiguousChainResolutionContext context) {
		final List<PublicChainElement> potentialAnaphorElements = context.potentialAnaphorChainRoots.stream()
				.flatMap(potentialChainRoot -> ChainTraversal.getAnaphorElements(potentialChainRoot)).collect(toList());

		if (!(potentialAnaphorElements.size() == context.potentialAnaphorChainRoots.size())) {
			throw new IllegalStateException(
					"Expecting every potential anaphor chain root to contain exactly 1 anaphor");
		}

		return potentialAnaphorElements;
	}

	protected List<Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>>> toPartsPairs(
			final List<PublicChainElement> anaphorElements) {
		return anaphorElements.stream().map(this::toPartsPair).collect(toList());
	}

	protected Pair<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>> toPartsPair(
			final PublicChainElement anaphorElement) {
		return new ImmutablePair<>(getRelatedExpressionElementForAnaphorElement(anaphorElement).relatedExpression,
				anaphorElement.anaphor);
	}

	private void checkNewSource(final SubProgressMonitor monitor, final RefactoringStatus result,
			final ICompilationUnit cu, final CompilationUnit compilationUnitNode) throws CoreException {
		final String newCuSource = fChange.getPreviewContent(new NullProgressMonitor());
		final CompilationUnit newCUNode = new RefactoringASTParser(ASTProvider.SHARED_AST_LEVEL).parse(newCuSource, cu,
				true, true, monitor);
		final IProblem[] newProblems = RefactoringAnalyzeUtil.getIntroducedCompileProblems(newCUNode,
				compilationUnitNode);
		for (final IProblem problem : newProblems) {
			if (problem.isError()) {
				result.addEntry(new RefactoringStatusEntry(
						(problem.isError() ? RefactoringStatus.ERROR : RefactoringStatus.WARNING), problem.getMessage(),
						new JavaStringStatusContext(newCuSource, SourceRangeFactory.create(problem))));
			}
		}
	}

	public Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, Anaphora> getAnaphoraToBeRealizedByAnaphorPart() {
		return getState(ChainContextsCreated.class).chainResolutionContexts.stream()
				.filter(context -> context instanceof SuccessfulChainResolutionContext)
				.map(context -> (SuccessfulChainResolutionContext) context)
				.flatMap(context -> context.anaphoraToBeRealizedByAnaphorPart.entrySet().stream())
				.collect(toMap(Entry::getKey, Entry::getValue));
	}

	@Override
	public Change createChange(final IProgressMonitor pm) throws CoreException {
		try {
			pm.beginTask(RefactoringCoreMessages.ExtractTempRefactoring_checking_preconditions, 1);
			final SharedChainResolutionContext sharedChainResolutionContext = getState(
					ChainContextsCreated.class).sharedChainResolutionContext;
			if (!sharedChainResolutionContext.thereAreModifications) {
				anaphorResolutionListener.accept(ABOUT_TO_REALIZE_ANAPHORS,
						/* there might be retained anaphors in the event */
						getState(ChainContextsCreated.class).anaphorResolutionEvent);
				if (sharedChainResolutionContext.problemAnnotationsNeedToBeUpdated()) {
					return createAnnotationUpdateChange(sharedChainResolutionContext);
				}
				return new NullChange();
			}
			// TODO: i18n
			final CompositeChange compositeChange = new CompositeChange("Resolve anaphor");
			compositeChange.markAsSynthetic();
			compositeChange.add(new AnaphorResolutionEventChange(ABOUT_TO_REALIZE_ANAPHORS,
					getState(ChainContextsCreated.class).anaphorResolutionEvent, anaphorResolutionListener));
			if (!perspectivationPositionsToAdd.isEmpty()) {
				compositeChange.add(
						new AnaphorResolutionPerspectivationChange(AboutToStartDocumentRewritingForAnaphorResolution,
								perspectivationPositionsToAdd, anaphorResolutionPerspectivationListener));
			}
			compositeChange.add(new UpdatePerspectivationPositionsChange(display, originDocument,
					perspectivationPositionsToAdd, perspectivationPositionsToLink, perspectivationPositionsToRemove,
					perspectivationPositionsToUnlink));
			compositeChange.add(new UpdateAnaphoraPositionsChange(originDocument, anaphoraPositionsToAdd,
					anaphoraPositionsToLink, anaphoraPositionsToRemove, anaphoraPositionsToUnlink));
			compositeChange.add(createAnnotationUpdateChange(sharedChainResolutionContext));
			compositeChange.add(new ConfigureAnaphoraReresolutionChange(anaphoraReresolvingListener, false));
			compositeChange.add(new ConfigureDeletionOfEmptyGrowingPositionsChange(originDocument,
					PERSPECTIVATION_CATEGORY, false));
			compositeChange
					.add(new ConfigureDeletionOfEmptyGrowingPositionsChange(originDocument, ANAPHORA_CATEGORY, false));
			compositeChange.add(fChange);
			compositeChange.add(new ConfigureAnaphoraReresolutionChange(anaphoraReresolvingListener, true));
			compositeChange.add(
					new ConfigureDeletionOfEmptyGrowingPositionsChange(originDocument, PERSPECTIVATION_CATEGORY, true));
			compositeChange
					.add(new ConfigureDeletionOfEmptyGrowingPositionsChange(originDocument, ANAPHORA_CATEGORY, true));
			if (!perspectivationPositionsToAdd.isEmpty()) {
				compositeChange.add(new AnaphorResolutionProgressChange(AnaphoraAddedOrChanged, anaphoraPositionsToAdd,
						anaphorResolutionProgressListener));
				compositeChange
						.add(new AnaphorResolutionPerspectivationChange(CompletedDocumentRewritingForAnaphorResolution,
								perspectivationPositionsToAdd, anaphorResolutionPerspectivationListener));
			}
			return compositeChange;
		} finally {
			pm.done();
		}
	}

	protected AnnotationUpdateChange createAnnotationUpdateChange(
			final SharedChainResolutionContext sharedChainResolutionContext) {
		return new AnnotationUpdateChange(sharedAnaphorResolutionContext.annotationModel,
				sharedChainResolutionContext.problemAnnotationsToRemove,
				sharedChainResolutionContext.problemAnnotationsToAdd);
	}

	protected Map<PublicChainElement, PerspectivationPosition> indexByElement(
			final List<Pair<PublicChainElement, PerspectivationPosition>> perspectivationPositions) {
		return perspectivationPositions.stream().collect(toMap(Pair::getKey, Pair::getValue));
	}

	protected List<PublicChainElement> getAnaphorChainRoots() {
		return getState(ChainContextsCreated.class).chainResolutionContexts.stream()
				.filter(context -> context instanceof SuccessfulChainResolutionContext)
				.map(context -> ((SuccessfulChainResolutionContext) context).anaphorChainRoot).collect(toList());
	}

	protected <S extends State> S getState(final Class<S> stateType) {
		if (!stateType.isInstance(state)) {
			throw new IllegalStateException(
					"Expected state " + stateType.getName() + " but found " + state.getClass().getName());
		}
		return (S) state;
	}

	protected static class State {

	}

	protected static class AnaphorContextsCreated extends State {

		protected final List<AnaphorResolutionContext> anaphorResolutionContexts;

		protected AnaphorContextsCreated(final List<AnaphorResolutionContext> anaphorResolutionContexts) {
			this.anaphorResolutionContexts = anaphorResolutionContexts;
		}

	}

	protected static class ChainContextsCreated extends State {

		protected final List<ChainResolutionContext> chainResolutionContexts;
		protected final SharedChainResolutionContext sharedChainResolutionContext;
		protected AnaphorResolutionEvent anaphorResolutionEvent;

		protected ChainContextsCreated(final List<ChainResolutionContext> chainResolutionContexts,
				final SharedChainResolutionContext sharedChainResolutionContext) {
			this.chainResolutionContexts = chainResolutionContexts;
			this.sharedChainResolutionContext = sharedChainResolutionContext;
		}

	}

}
