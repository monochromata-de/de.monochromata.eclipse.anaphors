package org.eclipse.jdt.internal.corext.refactoring.code.context;

import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadLocationException;
import static java.util.Optional.empty;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public interface AnaphorResolutionContextCreation {

	static List<AnaphorResolutionContext> createAnaphorResolutionContexts(
			final List<PositionForAnaphor> anaphorPositions, final List<? extends Position> problemPositions,
			final IDocument originDocument) {

		return Stream.concat(createContextsForAnaphorPositions(anaphorPositions),
				createContextsForProblemPositions(problemPositions, originDocument)).collect(toList());
	}

	static Stream<AnaphorResolutionContext> createContextsForAnaphorPositions(
			final List<PositionForAnaphor> anaphorPositions) {
		return anaphorPositions.stream().map(position -> createContextForAnaphorPosition(position));
	}

	static AnaphorResolutionContext createContextForAnaphorPosition(final PositionForAnaphor position) {
		final Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor> existingAnaphora = new ImmutableTriple<>(
				position.getRelatedExpressionPosition(), position.anaphora, position);
		return new AnaphorResolutionContext(position, position.anaphora.anaphor, Optional.of(existingAnaphora));
	}

	static Stream<AnaphorResolutionContext> createContextsForProblemPositions(
			final List<? extends Position> problemPositions, final IDocument originDocument) {
		return problemPositions.stream().map(position -> createContextForProblemPosition(position, originDocument));
	}

	static AnaphorResolutionContext createContextForProblemPosition(final Position position,
			final IDocument originDocument) {
		return new AnaphorResolutionContext(position, getAnaphor(position, originDocument), empty());
	}

	static String getAnaphor(final Position position, final IDocument originDocument) {
		return wrapBadLocationException(() -> originDocument.get(position.offset, position.length));
	}

}
