package org.eclipse.jdt.internal.corext.refactoring.code.check;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.CatchClause;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.LambdaExpression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.SwitchCase;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.internal.corext.dom.fragments.IASTFragment;

public interface ExtractTempChecking {

    class ForStatementChecker extends ASTVisitor {

        final Collection<IVariableBinding> fForInitializerVariables;

        boolean fReferringToForVariable = false;

        public ForStatementChecker(final Collection<IVariableBinding> forInitializerVariables) {
            Assert.isNotNull(forInitializerVariables);
            fForInitializerVariables = forInitializerVariables;
        }

        public boolean isReferringToForVariable() {
            return fReferringToForVariable;
        }

        @Override
        public boolean visit(final SimpleName node) {
            final IBinding binding = node.resolveBinding();
            if (binding != null && fForInitializerVariables.contains(binding)) {
                fReferringToForVariable = true;
            }
            return false;
        }
    }

    static boolean allArraysEqual(final Object[][] arrays, final int position) {
        final Object element = arrays[0][position];
        for (final Object[] array2 : arrays) {
            final Object[] array = array2;
            if (!element.equals(array[position])) {
                return false;
            }
        }
        return true;
    }

    static boolean canReplace(final IASTFragment fragment) {
        final ASTNode node = fragment.getAssociatedNode();
        final ASTNode parent = node.getParent();
        if (parent instanceof VariableDeclarationFragment) {
            final VariableDeclarationFragment vdf = (VariableDeclarationFragment) parent;
            if (node.equals(vdf.getName())) {
                return false;
            }
        }
        if (isMethodParameter(node)) {
            return false;
        }
        if (isThrowableInCatchBlock(node)) {
            return false;
        }
        if (parent instanceof ExpressionStatement) {
            return false;
        }
        if (parent instanceof LambdaExpression) {
            return false;
        }
        if (isLeftValue(node)) {
            return false;
        }
        if (isReferringToLocalVariableFromFor((Expression) node)) {
            return false;
        }
        if (isUsedInForInitializerOrUpdater((Expression) node)) {
            return false;
        }
        if (parent instanceof SwitchCase) {
            return false;
        }
        return true;
    }

    static Object[] getArrayPrefix(final Object[] array, final int prefixLength) {
        Assert.isTrue(prefixLength <= array.length);
        Assert.isTrue(prefixLength >= 0);
        final Object[] prefix = new Object[prefixLength];
        for (int i = 0; i < prefix.length; i++) {
            prefix[i] = array[i];
        }
        return prefix;
    }

    // return List<IVariableBinding>
    static List<IVariableBinding> getForInitializedVariables(
            final VariableDeclarationExpression variableDeclarations) {
        final List<IVariableBinding> forInitializerVariables = new ArrayList<>(1);
        for (final Iterator<VariableDeclarationFragment> iter = variableDeclarations.fragments().iterator(); iter
                .hasNext();) {
            final VariableDeclarationFragment fragment = iter.next();
            final IVariableBinding binding = fragment.resolveBinding();
            if (binding != null) {
                forInitializerVariables.add(binding);
            }
        }
        return forInitializerVariables;
    }

    static Object[] getLongestArrayPrefix(final Object[][] arrays) {
        int length = -1;
        if (arrays.length == 0) {
            return new Object[0];
        }
        int minArrayLength = arrays[0].length;
        for (int i = 1; i < arrays.length; i++) {
            minArrayLength = Math.min(minArrayLength, arrays[i].length);
        }

        for (int i = 0; i < minArrayLength; i++) {
            if (!allArraysEqual(arrays, i)) {
                break;
            }
            length++;
        }
        if (length == -1) {
            return new Object[0];
        }
        return getArrayPrefix(arrays[0], length + 1);
    }

    static ASTNode[] getParents(final ASTNode node) {
        ASTNode current = node;
        final List<ASTNode> parents = new ArrayList<>();
        do {
            parents.add(current.getParent());
            current = current.getParent();
        } while (current.getParent() != null);
        Collections.reverse(parents);
        return parents.toArray(new ASTNode[parents.size()]);
    }

    static boolean isLeftValue(final ASTNode node) {
        final ASTNode parent = node.getParent();
        if (parent instanceof Assignment) {
            final Assignment assignment = (Assignment) parent;
            if (assignment.getLeftHandSide() == node) {
                return true;
            }
        }
        if (parent instanceof PostfixExpression) {
            return true;
        }
        if (parent instanceof PrefixExpression) {
            final PrefixExpression.Operator op = ((PrefixExpression) parent).getOperator();
            if (op.equals(PrefixExpression.Operator.DECREMENT)) {
                return true;
            }
            if (op.equals(PrefixExpression.Operator.INCREMENT)) {
                return true;
            }
            return false;
        }
        return false;
    }

    static boolean isMethodParameter(final ASTNode node) {
        return (node instanceof SimpleName) && (node.getParent() instanceof SingleVariableDeclaration)
                && (node.getParent().getParent() instanceof MethodDeclaration);
    }

    static boolean isReferringToLocalVariableFromFor(final Expression expression) {
        ASTNode current = expression;
        ASTNode parent = current.getParent();
        while (parent != null && !(parent instanceof BodyDeclaration)) {
            if (parent instanceof ForStatement) {
                final ForStatement forStmt = (ForStatement) parent;
                if (forStmt.initializers().contains(current) || forStmt.updaters().contains(current)
                        || forStmt.getExpression() == current) {
                    final List<Expression> initializers = forStmt.initializers();
                    if (initializers.size() == 1 && initializers.get(0) instanceof VariableDeclarationExpression) {
                        final List<IVariableBinding> forInitializerVariables = getForInitializedVariables(
                                (VariableDeclarationExpression) initializers.get(0));
                        final ForStatementChecker checker = new ForStatementChecker(forInitializerVariables);
                        expression.accept(checker);
                        if (checker.isReferringToForVariable()) {
                            return true;
                        }
                    }
                }
            }
            current = parent;
            parent = current.getParent();
        }
        return false;
    }

    static boolean isThrowableInCatchBlock(final ASTNode node) {
        return (node instanceof SimpleName) && (node.getParent() instanceof SingleVariableDeclaration)
                && (node.getParent().getParent() instanceof CatchClause);
    }

    static boolean isUsedInForInitializerOrUpdater(final Expression expression) {
        final ASTNode parent = expression.getParent();
        if (parent instanceof ForStatement) {
            final ForStatement forStmt = (ForStatement) parent;
            return forStmt.initializers().contains(expression) || forStmt.updaters().contains(expression);
        }
        return false;
    }

    static IASTFragment[] retainOnlyReplacableMatches(final IASTFragment[] allMatches) {
        final List<IASTFragment> result = new ArrayList<>(allMatches.length);
        for (final IASTFragment allMatche : allMatches) {
            if (canReplace(allMatche)) {
                result.add(allMatche);
            }
        }
        return result.toArray(new IASTFragment[result.size()]);
    }

}
