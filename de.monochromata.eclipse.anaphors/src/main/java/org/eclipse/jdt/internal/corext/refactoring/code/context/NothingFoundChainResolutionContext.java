package org.eclipse.jdt.internal.corext.refactoring.code.context;

import java.util.Optional;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class NothingFoundChainResolutionContext extends FailedChainResolutionContext {

    public NothingFoundChainResolutionContext(final RefactoringStatus refactoringStatus,
            final Optional<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>> originalAnaphora) {
        super(refactoringStatus, originalAnaphora);
    }

}
