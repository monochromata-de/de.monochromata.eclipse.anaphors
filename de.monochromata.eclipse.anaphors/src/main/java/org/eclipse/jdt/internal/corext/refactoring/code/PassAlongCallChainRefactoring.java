package org.eclipse.jdt.internal.corext.refactoring.code;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.ParenthesizedExpression;
import org.eclipse.jdt.core.dom.VariableDeclaration;
import org.eclipse.jdt.core.dom.rewrite.ImportRewrite;
import org.eclipse.jdt.core.dom.rewrite.ImportRewrite.ImportRewriteContext;
import org.eclipse.jdt.core.refactoring.IJavaRefactorings;
import org.eclipse.jdt.core.refactoring.descriptors.ChangeMethodSignatureDescriptor;
import org.eclipse.jdt.core.refactoring.descriptors.IntroduceParameterDescriptor;
import org.eclipse.jdt.internal.core.refactoring.descriptors.RefactoringSignatureDescriptorFactory;
import org.eclipse.jdt.internal.corext.codemanipulation.ContextSensitiveImportRewriteContext;
import org.eclipse.jdt.internal.corext.dom.Bindings;
import org.eclipse.jdt.internal.corext.refactoring.ParameterInfo;
import org.eclipse.jdt.internal.corext.refactoring.RefactoringAvailabilityTester;
import org.eclipse.jdt.internal.corext.refactoring.RefactoringCoreMessages;
import org.eclipse.jdt.internal.corext.refactoring.base.RefactoringStatusCodes;
import org.eclipse.jdt.internal.corext.refactoring.changes.DynamicValidationRefactoringChange;
import org.eclipse.jdt.internal.corext.refactoring.structure.ChangeSignatureProcessor;
import org.eclipse.jdt.internal.corext.refactoring.structure.CompilationUnitRewrite;
import org.eclipse.jdt.internal.corext.refactoring.tagging.IDelegateUpdating;
import org.eclipse.jdt.internal.corext.util.Messages;
import org.eclipse.jdt.internal.ui.JavaPlugin;
import org.eclipse.jdt.internal.ui.viewsupport.BasicElementLabels;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.core.refactoring.RefactoringContribution;
import org.eclipse.ltk.core.refactoring.RefactoringCore;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.RefactoringStatusEntry;
import org.eclipse.ltk.core.refactoring.participants.ProcessorBasedRefactoring;

/**
 * A modified copy of {@link IntroduceParameterRefactoring} that introduces a
 * parameter to be able to pass the upstream expression towards the
 * <p>
 * TODO: Add a descriptor and a contribution?
 * <p>
 * TODO: The copied code might require the EPL license.
 */
public class PassAlongCallChainRefactoring extends Refactoring implements IDelegateUpdating {

	private static final String ATTRIBUTE_ARGUMENT = "argument"; //$NON-NLS-1$

	private static final String[] KNOWN_METHOD_NAME_PREFIXES = { "get", "is" }; //$NON-NLS-2$ //$NON-NLS-1$

	private final ICompilationUnit fSourceCU;
	private final ASTNode fUpstreamNode;

	private final IMethod fMethod;
	private Refactoring fChangeSignatureRefactoring;
	private ChangeSignatureProcessor fChangeSignatureProcessor;
	private ParameterInfo fParameter;
	private String fParameterName;

	/**
	 * Creates a new introduce parameter refactoring.
	 *
	 * @param unit
	 *            the compilation unit, or <code>null</code> if invoked by
	 *            scripting
	 * @param selectionStart
	 *            start
	 * @param selectionLength
	 *            length
	 */
	public PassAlongCallChainRefactoring(final CompilationUnit scope, final ASTNode upstreamNode,
			final MethodInvocation methodInvocation) {
		fSourceCU = (ICompilationUnit) scope.getJavaElement();
		fUpstreamNode = upstreamNode;
		fMethod = (IMethod) methodInvocation.resolveMethodBinding().getJavaElement();
	}

	// ------------------- IDelegateUpdating ----------------------

	@Override
	public boolean canEnableDelegateUpdating() {
		return true;
	}

	@Override
	public boolean getDelegateUpdating() {
		return (fChangeSignatureProcessor != null) ? fChangeSignatureProcessor.getDelegateUpdating() : false;
	}

	@Override
	public void setDelegateUpdating(final boolean updating) {
		if (fChangeSignatureProcessor != null) {
			fChangeSignatureProcessor.setDelegateUpdating(updating);
		}
	}

	@Override
	public void setDeprecateDelegates(final boolean deprecate) {
		if (fChangeSignatureProcessor != null) {
			fChangeSignatureProcessor.setDeprecateDelegates(deprecate);
		}
	}

	@Override
	public boolean getDeprecateDelegates() {
		return (fChangeSignatureProcessor != null) ? fChangeSignatureProcessor.getDeprecateDelegates() : false;
	}

	// ------------------- /IDelegateUpdating ---------------------

	@Override
	public String getName() {
		// TODO: Replace
		return RefactoringCoreMessages.IntroduceParameterRefactoring_name;
	}

	// --- checkActivation

	@Override
	public RefactoringStatus checkInitialConditions(final IProgressMonitor pm) throws CoreException {
		try {
			pm.beginTask("", 7); //$NON-NLS-1$

			if (!fSourceCU.isStructureKnown()) {
				return RefactoringStatus
						.createFatalErrorStatus(RefactoringCoreMessages.IntroduceParameterRefactoring_syntax_error);
			}

			pm.worked(3);

			RefactoringStatus result = new RefactoringStatus();
			// first try:
			fChangeSignatureProcessor = RefactoringAvailabilityTester.isChangeSignatureAvailable(fMethod)
					? new ChangeSignatureProcessor(fMethod) : null;
			if (fChangeSignatureProcessor == null) {
				return RefactoringStatus.createFatalErrorStatus(
						RefactoringCoreMessages.IntroduceParameterRefactoring_expression_in_method);
			}
			fChangeSignatureRefactoring = new ProcessorBasedRefactoring(fChangeSignatureProcessor);
			fChangeSignatureRefactoring.setValidationContext(getValidationContext());
			result.merge(fChangeSignatureProcessor.checkInitialConditions(new SubProgressMonitor(pm, 1)));
			if (result.hasFatalError()) {
				final RefactoringStatusEntry entry = result.getEntryMatchingSeverity(RefactoringStatus.FATAL);
				if (entry.getCode() == RefactoringStatusCodes.OVERRIDES_ANOTHER_METHOD
						|| entry.getCode() == RefactoringStatusCodes.METHOD_DECLARED_IN_INTERFACE) {
					// second try:
					final IMethod method = (IMethod) entry.getData();
					fChangeSignatureProcessor = RefactoringAvailabilityTester.isChangeSignatureAvailable(method)
							? new ChangeSignatureProcessor(method) : null;
					if (fChangeSignatureProcessor == null) {
						final String msg = Messages.format(
								RefactoringCoreMessages.IntroduceParameterRefactoring_cannot_introduce,
								entry.getMessage());
						return RefactoringStatus.createFatalErrorStatus(msg);
					}
					fChangeSignatureRefactoring = new ProcessorBasedRefactoring(fChangeSignatureProcessor);
					fChangeSignatureRefactoring.setValidationContext(getValidationContext());
					result = fChangeSignatureProcessor.checkInitialConditions(new SubProgressMonitor(pm, 1));
					if (result.hasFatalError()) {
						return result;
					}
				} else {
					return result;
				}
			} else {
				pm.worked(1);
			}

			CompilationUnitRewrite cuRewrite = fChangeSignatureProcessor.getBaseCuRewrite();
			if (!cuRewrite.getCu().equals(fSourceCU)) {
				cuRewrite = new CompilationUnitRewrite(fSourceCU); // TODO:
																	// should
																	// try to
																	// avoid
																	// throwing
																	// away this
																	// AST
			}

			pm.worked(1);

			if (result.hasFatalError()) {
				return result;
			}

			result.merge(addParameterInfo(cuRewrite));

			return result;
		} finally {
			pm.done();
			if (fChangeSignatureRefactoring != null) {
				fChangeSignatureRefactoring.setValidationContext(null);
			}
		}
	}

	private RefactoringStatus addParameterInfo(final CompilationUnitRewrite cuRewrite) throws JavaModelException {
		final ASTNode node = fUpstreamNode instanceof ParenthesizedExpression
				? ((ParenthesizedExpression) fUpstreamNode).getExpression() : fUpstreamNode;

		final ImportRewrite importRewrite = cuRewrite.getImportRewrite();
		final ImportRewriteContext importRewriteContext = new ContextSensitiveImportRewriteContext(fUpstreamNode,
				importRewrite);

		final Pair<ITypeBinding, RefactoringStatus> typeBindingAndStatus = getTypeBinding();
		final RefactoringStatus typeStatus = typeBindingAndStatus.getRight();
		if (typeStatus != null && typeStatus.hasFatalError()) {
			return typeStatus;
		}
		final ITypeBinding typeBinding = typeBindingAndStatus.getLeft();

		final String typeName = importRewrite.addImport(typeBinding, importRewriteContext);
		final String name = fParameterName != null ? fParameterName : guessedParameterName();
		final Pair<String, RefactoringStatus> defaultValueAndStatus = getDefaultValue(cuRewrite, typeBinding, node,
				importRewrite, importRewriteContext);
		final RefactoringStatus defaultStatus = defaultValueAndStatus.getRight();
		if (defaultStatus != null && defaultStatus.hasFatalError()) {
			return defaultStatus;
		}

		fParameter = ParameterInfo.createInfoForAddedParameter(typeBinding, typeName, name,
				defaultValueAndStatus.getLeft());
		final List<ParameterInfo> parameterInfos = fChangeSignatureProcessor.getParameterInfos();
		final int parametersCount = parameterInfos.size();
		if (parametersCount > 0 && parameterInfos.get(parametersCount - 1).isOldVarargs()) {
			parameterInfos.add(parametersCount - 1, fParameter);
		} else {
			parameterInfos.add(fParameter);
		}
		return null;
	}

	protected Pair<ITypeBinding, RefactoringStatus> getTypeBinding() throws JavaModelException {
		if (fUpstreamNode instanceof Expression) {
			return getTypeBinding((Expression) fUpstreamNode);
		} else if (fUpstreamNode instanceof VariableDeclaration) {
			return getTypeBinding((VariableDeclaration) fUpstreamNode);
		} else {
			// TODO: Introduce a local variable/underspecified direct anaphor
			// instead
			final RefactoringStatus fatalError = RefactoringStatus.createFatalErrorStatus(
					"Unknown node type for fUpstreamNode: " + fUpstreamNode.getClass().getName());
			return new ImmutablePair<>(null, fatalError);
		}
	}

	protected Pair<ITypeBinding, RefactoringStatus> getTypeBinding(final Expression expression) {
		final ITypeBinding binding = Bindings.normalizeForDeclarationUse(expression.resolveTypeBinding(),
				expression.getAST());
		return new ImmutablePair<>(binding, null);
	}

	protected Pair<ITypeBinding, RefactoringStatus> getTypeBinding(final VariableDeclaration declaration)
			throws JavaModelException {
		final IVariableBinding binding = declaration.resolveBinding();
		if (binding == null) {
			final String declarationSource = fSourceCU.getBuffer().getText(declaration.getStartPosition(),
					declaration.getLength());
			// TODO: Add file name, line and column
			final RefactoringStatus fatalError = RefactoringStatus
					.createFatalErrorStatus("Failed to resolve variable: " + declarationSource);
			return new ImmutablePair<>(null, fatalError);
		}
		return new ImmutablePair<>(binding.getType(), null);
	}

	protected Pair<String, RefactoringStatus> getDefaultValue(final CompilationUnitRewrite cuRewrite,
			final ITypeBinding typeBinding, final ASTNode node, final ImportRewrite importRewrite,
			final ImportRewriteContext importRewriteContext) throws JavaModelException {
		String defaultValue = null;
		if (node.getParent() instanceof VariableDeclaration) {
			defaultValue = ((VariableDeclaration) node.getParent()).getName().getIdentifier();
		} else {
			// TODO: Introduce a local variable/underspecified direct anaphor
			// instead
			final RefactoringStatus fatalError = RefactoringStatus.createFatalErrorStatus(
					"Unknown node type for fUpstreamNode: " + fUpstreamNode.getClass().getName());
			return new ImmutablePair<>(null, fatalError);
		}

		if (defaultValue == null) {
			defaultValue = fSourceCU.getBuffer().getText(node.getStartPosition(), node.getLength());
		}
		return new ImmutablePair<>(defaultValue, null);
	}

	// TODO: Check upstream expression (again)

	public List<ParameterInfo> getParameterInfos() {
		return fChangeSignatureProcessor.getParameterInfos();
	}

	public ParameterInfo getAddedParameterInfo() {
		return fParameter;
	}

	public String getMethodSignaturePreview() throws JavaModelException {
		return fChangeSignatureProcessor.getNewMethodSignature();
	}

	// --- Input setting/validation

	public void setParameterName(final String name) {
		Assert.isNotNull(name);
		fParameter.setNewName(name);
	}

	/**
	 * must only be called <i>after</i> checkActivation()
	 *
	 * @return guessed parameter name
	 */
	public String guessedParameterName() {
		final String[] proposals = guessParameterNames();
		if (proposals.length == 0) {
			return ""; //$NON-NLS-1$
		} else {
			return proposals[0];
		}
	}

	// --- TODO: copied from ExtractTempRefactoring - should extract
	// ------------------------------

	/**
	 * Must only be called <i>after</i> checkActivation(). The first proposal
	 * should be used as "best guess" (if it exists).
	 *
	 * @return proposed variable names (may be empty, but not null).
	 */
	public String[] guessParameterNames() {
		final LinkedHashSet<String> proposals = new LinkedHashSet<String>(); // retain
																				// ordering,
																				// but
																				// prevent
																				// duplicates
		if (fUpstreamNode.getParent() instanceof VariableDeclaration) {
			proposals.add(((VariableDeclaration) fUpstreamNode.getParent()).getName().getIdentifier());
		}
		// TODO: Add further guesses
		return proposals.toArray(new String[proposals.size()]);
	}

	// ----------------------------------------------------------------------

	public RefactoringStatus validateInput() {
		return fChangeSignatureProcessor.checkSignature();
	}

	// --- checkInput

	@Override
	public RefactoringStatus checkFinalConditions(final IProgressMonitor pm) throws CoreException {
		fChangeSignatureRefactoring.setValidationContext(getValidationContext());
		try {
			return fChangeSignatureRefactoring.checkFinalConditions(pm);
		} finally {
			fChangeSignatureRefactoring.setValidationContext(null);
		}
	}

	@Override
	public Change createChange(final IProgressMonitor pm) throws CoreException {
		fChangeSignatureRefactoring.setValidationContext(getValidationContext());
		try {
			final Change[] changes = fChangeSignatureProcessor.getAllChanges();
			return new DynamicValidationRefactoringChange(getRefactoringDescriptor(),
					RefactoringCoreMessages.IntroduceParameterRefactoring_name, changes);
		} finally {
			fChangeSignatureRefactoring.setValidationContext(null);
			pm.done();
		}
	}

	private IntroduceParameterDescriptor getRefactoringDescriptor() {
		final ChangeMethodSignatureDescriptor extended = (ChangeMethodSignatureDescriptor) fChangeSignatureProcessor
				.createDescriptor();
		final RefactoringContribution contribution = RefactoringCore
				.getRefactoringContribution(IJavaRefactorings.CHANGE_METHOD_SIGNATURE);

		final Map<String, String> argumentsMap = contribution.retrieveArgumentMap(extended);

		final Map<String, String> arguments = new HashMap<String, String>();
		arguments.put(ATTRIBUTE_ARGUMENT, fParameter.getNewName());
		arguments.putAll(argumentsMap);
		String signature = fChangeSignatureProcessor.getMethodName();
		try {
			signature = fChangeSignatureProcessor.getOldMethodSignature();
		} catch (final JavaModelException exception) {
			JavaPlugin.log(exception);
		}
		final String description = Messages.format(
				RefactoringCoreMessages.IntroduceParameterRefactoring_descriptor_description_short,
				BasicElementLabels.getJavaElementName(fChangeSignatureProcessor.getMethod().getElementName()));
		// TODO: Create a correct descriptor - and understand what it is used
		// for
		return RefactoringSignatureDescriptorFactory.createIntroduceParameterDescriptor(extended.getProject(),
				description, "", arguments, extended.getFlags());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getDelegateUpdatingTitle(final boolean plural) {
		if (plural) {
			return RefactoringCoreMessages.DelegateCreator_keep_original_changed_plural;
		} else {
			return RefactoringCoreMessages.DelegateCreator_keep_original_changed_singular;
		}
	}

}
