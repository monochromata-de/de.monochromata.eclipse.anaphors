package org.eclipse.jdt.internal.corext.refactoring.code.context;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jdt.core.dom.anaphors.PublicChainElement;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.SingleOriginalAnaphora;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class AmbiguousChainResolutionContext extends FailedChainResolutionContext {

	public final List<PublicChainElement> potentialAnaphorChainRoots;

	public AmbiguousChainResolutionContext(final RefactoringStatus refactoringStatus,
			final List<PublicChainElement> potentialAnaphorChainRoots) {
		super(refactoringStatus, getOriginalAnaphora(potentialAnaphorChainRoots));
		this.potentialAnaphorChainRoots = potentialAnaphorChainRoots;
	}

	protected static Optional<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>> getOriginalAnaphora(
			final List<PublicChainElement> potentialAnaphorChainRoots) {
		return potentialAnaphorChainRoots.stream().flatMap(chainRoot -> chainRoot.next.stream())
				.map(AmbiguousChainResolutionContext::getOriginalAnaphora)
				.reduce((originalAnaphora1, originalAnaphora2) -> {
					requireEqualAnaphoras(originalAnaphora1, originalAnaphora2);
					return originalAnaphora1;
				}).map(originalAnaphora -> originalAnaphora.originalAnaphora).orElseGet(Optional::empty);
	}

	protected static void requireEqualAnaphoras(final SingleOriginalAnaphora originalAnaphora1,
			final SingleOriginalAnaphora originalAnaphora2) {
		if (!originalAnaphora1.equals(originalAnaphora2)) {
			throw new IllegalArgumentException("original anaphoras are not equal");
		}
	}

	protected static SingleOriginalAnaphora getOriginalAnaphora(final PublicChainElement anaphorElement) {
		requireTerminalAnaphorElementWithSingleOriginalAnaphora(anaphorElement);
		return (SingleOriginalAnaphora) anaphorElement.anaphorAttachment;
	}

	protected static void requireTerminalAnaphorElementWithSingleOriginalAnaphora(
			final PublicChainElement anaphorElement) {
		if (!anaphorElement.next.isEmpty() || anaphorElement.anaphorAttachment == null
				|| !(anaphorElement.anaphorAttachment instanceof SingleOriginalAnaphora)) {
			throw new IllegalArgumentException("Invalid anaphor element");
		}
	}

}
