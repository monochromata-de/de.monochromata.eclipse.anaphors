package org.eclipse.jdt.internal.corext.refactoring.code;

import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jdt.core.dom.anaphors.PublicChainElement;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import de.monochromata.anaphors.ast.chain.Chaining;
import de.monochromata.eclipse.anaphors.SingleOriginalAnaphora;

public interface ChainMerging {

	/**
	 * @return pairs with a non-OK refactoring status will be at the end of the
	 *         returned list
	 */
	static List<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> mergeStatusAndPotentialChains(
			final List<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> statusAndPotentialChains1,
			final List<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> statusAndPotentialChains2) {
		return mergeStatusAndPotentialChains(statusAndPotentialChains1, statusAndPotentialChains2,
				(chains1, chains2) -> Chaining.merge(chains1, chains2, PublicChainElement::new));
	}

	/**
	 * @return pairs with a non-OK refactoring status will be at the end of the
	 *         returned list
	 */
	static List<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> mergeStatusAndPotentialChains(
			final List<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> statusAndPotentialChains1,
			final List<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> statusAndPotentialChains2,
			final BinaryOperator<List<PublicChainElement>> mergeOperator) {
		final Stream<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> mergedPairs = merge(
				getOkPairs(statusAndPotentialChains1), getOkPairs(statusAndPotentialChains2), mergeOperator).stream();
		final Stream<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> erroneousPairs = Stream
				.concat(getErroneousPairs(statusAndPotentialChains1), getErroneousPairs(statusAndPotentialChains2));
		return Stream.concat(mergedPairs, erroneousPairs).collect(toList());
	}

	static List<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> getOkPairs(
			final List<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> statusAndPotentialChains) {
		return statusAndPotentialChains.stream().filter(pair -> pair.getLeft().isOK()).collect(toList());
	}

	static Stream<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> getErroneousPairs(
			final List<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> statusAndPotentialChains) {
		return statusAndPotentialChains.stream().filter(pair -> !pair.getLeft().isOK());
	}

	static List<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> merge(
			final List<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> statusAndPotentialChains1,
			final List<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> statusAndPotentialChains2,
			final BinaryOperator<List<PublicChainElement>> mergeOperator) {
		return mergeOperator
				.apply(getPotentialChains(statusAndPotentialChains1), getPotentialChains(statusAndPotentialChains2))
				.stream().map(chainElement -> new ImmutableTriple<>(new RefactoringStatus(),
						Optional.<SingleOriginalAnaphora>empty(), singletonList(chainElement)))
				.collect(toList());
	}

	static List<PublicChainElement> getPotentialChains(
			final List<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> statusAndOriginalAnaphoraAndPotentialChains) {
		return statusAndOriginalAnaphoraAndPotentialChains.stream()
				.flatMap(statusAndOriginalAnaphoraAndChains -> statusAndOriginalAnaphoraAndChains.getRight().stream())
				.collect(toList());
	}

}
