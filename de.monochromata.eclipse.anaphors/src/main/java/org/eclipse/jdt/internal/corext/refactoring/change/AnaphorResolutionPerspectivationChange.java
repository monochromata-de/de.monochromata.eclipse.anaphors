package org.eclipse.jdt.internal.corext.refactoring.change;

import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionPerspectivationEvent.AboutToStartDocumentRewritingForAnaphorResolution;
import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionPerspectivationEvent.CompletedDocumentRewritingForAnaphorResolution;

import java.util.List;
import java.util.function.BiConsumer;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.jface.text.Position;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import de.monochromata.eclipse.anaphors.event.AnaphorResolutionPerspectivationEvent;

public class AnaphorResolutionPerspectivationChange extends Change {

    private final AnaphorResolutionPerspectivationEvent eventToFire;
    private final List<? extends Position> positions;
    private final BiConsumer<AnaphorResolutionPerspectivationEvent, List<? extends Position>> listener;

    public AnaphorResolutionPerspectivationChange(final AnaphorResolutionPerspectivationEvent eventToFire,
            final List<? extends Position> positions,
            final BiConsumer<AnaphorResolutionPerspectivationEvent, List<? extends Position>> listener) {
        this.eventToFire = eventToFire;
        this.positions = positions;
        this.listener = listener;
    }

    @Override
    public String getName() {
        return "send anaphor resolution perspectivation event " + eventToFire;
    }

    public AnaphorResolutionPerspectivationEvent getEventToFire() {
        return eventToFire;
    }

    @Override
    public void initializeValidationData(final IProgressMonitor pm) {
    }

    @Override
    public RefactoringStatus isValid(final IProgressMonitor pm) throws CoreException, OperationCanceledException {
        return new RefactoringStatus(); // has severity OK
    }

    @Override
    public Change perform(final IProgressMonitor pm) throws CoreException {
        listener.accept(eventToFire, positions);
        return createUndoChange();
    }

    protected AnaphorResolutionPerspectivationChange createUndoChange() {
        return new AnaphorResolutionPerspectivationChange(getOppositeEvent(), positions, listener);
    }

    private AnaphorResolutionPerspectivationEvent getOppositeEvent() {
        switch (eventToFire) {
        case AboutToStartDocumentRewritingForAnaphorResolution:
            return CompletedDocumentRewritingForAnaphorResolution;
        case CompletedDocumentRewritingForAnaphorResolution:
            return AboutToStartDocumentRewritingForAnaphorResolution;
        default:
            throw new IllegalStateException("Unknown event: " + eventToFire);
        }
    }

    @Override
    public Object getModifiedElement() {
        // The change is not directly related to a single element
        return null;
    }

}
