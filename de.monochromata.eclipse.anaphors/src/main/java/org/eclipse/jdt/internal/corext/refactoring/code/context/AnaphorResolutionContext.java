package org.eclipse.jdt.internal.corext.refactoring.code.context;

import static java.util.Collections.emptyList;
import static java.util.Optional.empty;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jdt.internal.corext.dom.fragments.IExpressionFragment;
import org.eclipse.jface.text.Position;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class AnaphorResolutionContext {

	public final Position definiteExpressionPosition;
	public final String originalAnaphor;
	public final Optional<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>> originalAnaphora;
	public IExpressionFragment selectedExpression;

	public RefactoringStatus refactoringStatus;
	public List<PublicAnaphora> potentialAnaphoras = emptyList();
	public Optional<Anaphora> anaphora = empty();

	public AnaphorResolutionContext(final Position definiteExpressionPosition, final String originalAnaphor,
			final Optional<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>> originalAnaphora) {
		this.definiteExpressionPosition = definiteExpressionPosition;
		this.originalAnaphor = originalAnaphor;
		this.originalAnaphora = originalAnaphora;
	}
}
