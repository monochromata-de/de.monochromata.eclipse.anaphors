package org.eclipse.jdt.internal.corext.refactoring.code;

import static de.monochromata.anaphors.ast.chain.ChainTraversal.getAnaphorElementsForRelatedExpressionElement;
import static de.monochromata.anaphors.ast.chain.ChainTraversal.getRelatedExpressionElements;
import static de.monochromata.eclipse.anaphors.position.ExistingAnaphoraPosition.getExistingPositionForAnaphor;
import static de.monochromata.eclipse.anaphors.position.ExistingAnaphoraPosition.getExistingPositionForRelatedExpression;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicChainElement;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jdt.internal.corext.refactoring.code.context.ChainResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.code.context.SuccessfulChainResolutionContext;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;
import de.monochromata.eclipse.persp.PerspectivationPosition;

public interface AnaphoraPositionCreationForRefactoring {

	/**
	 * A type used to mark returned anaphora positions.
	 */
	enum ReturnType {
		Existing, New
	}

	Function<Map<PublicChainElement, PerspectivationPosition>, Function<PublicChainElement, PositionForRelatedExpression>> CREATE_POSITION_FOR_RELATED_EXPRESSION = perspectivationPositionsByElement -> relatedExpressionElement -> {
		final var perspectivationPosition = perspectivationPositionsByElement.get(relatedExpressionElement);
		return createPositionForRelatedExpression(perspectivationPosition);
	};

	Function<Map<PublicChainElement, PerspectivationPosition>, Function<Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, Anaphora>, Function<PublicChainElement, PositionForAnaphor>>> CREATE_POSITION_FOR_ANAPHOR = perspectivationPositionsByElement -> anaphoraToBeRealizedByAnaphorPart -> anaphorElement -> {
		final var anaphora = anaphoraToBeRealizedByAnaphorPart.get(anaphorElement.anaphor);
		final var perspectivationPosition = perspectivationPositionsByElement.get(anaphorElement);
		return createPositionForAnaphor(anaphora, perspectivationPosition);
	};

	static List<AbstractAnaphoraPositionForRepresentation> createPositionsToAdd(
			final List<ChainResolutionContext> chainResolutionContexts,
			final Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, Anaphora> anaphoraToBeRealizedByAnaphorPart,
			final Map<PublicChainElement, PerspectivationPosition> perspectivationPositionsByElement,
			final List<AbstractAnaphoraPositionForRepresentation> positionsToRemove,
			final List<Pair<AbstractAnaphoraPositionForRepresentation, AbstractAnaphoraPositionForRepresentation>> positionsToLink) {
		final List<SuccessfulChainResolutionContext> successfulContexts = chainResolutionContexts.stream()
				.filter(context -> context instanceof SuccessfulChainResolutionContext)
				.map(context -> (SuccessfulChainResolutionContext) context).collect(toList());
		if (successfulContexts.isEmpty()) {
			return emptyList();
		}

		final var createPositionForRelatedExpression = CREATE_POSITION_FOR_RELATED_EXPRESSION
				.apply(perspectivationPositionsByElement);
		final var createPositionForAnaphor = CREATE_POSITION_FOR_ANAPHOR.apply(perspectivationPositionsByElement)
				.apply(anaphoraToBeRealizedByAnaphorPart);
		final Stream.Builder<AbstractAnaphoraPositionForRepresentation> positionsToAdd = Stream.builder();
		successfulContexts.stream()
				.forEach(context -> loadOrCreatePositions(context, positionsToAdd, positionsToLink,
						getExistingPositionForRelatedExpression(positionsToRemove), createPositionForRelatedExpression,
						getExistingPositionForAnaphor(positionsToRemove), createPositionForAnaphor));
		return positionsToAdd.build().collect(toList());
	}

	static void loadOrCreatePositions(final SuccessfulChainResolutionContext chainResolutionContext,
			final Stream.Builder<AbstractAnaphoraPositionForRepresentation> positionsToAdd,
			final List<Pair<AbstractAnaphoraPositionForRepresentation, AbstractAnaphoraPositionForRepresentation>> positionsToLink,
			final Function<PublicChainElement, PositionForRelatedExpression> getExistingRelatedExpressionPosition,
			final Function<PublicChainElement, PositionForRelatedExpression> createRelatedExpressionPosition,
			final Function<PublicChainElement, PositionForAnaphor> getExistingAnaphorPosition,
			final Function<PublicChainElement, PositionForAnaphor> createAnaphorPosition) {
		getRelatedExpressionElements(chainResolutionContext.anaphorChainRoot)
				.forEach(relatedExpressionElement -> loadOrCreatePositions(relatedExpressionElement, positionsToAdd,
						positionsToLink, getExistingRelatedExpressionPosition, createRelatedExpressionPosition,
						getExistingAnaphorPosition, createAnaphorPosition));
	}

	static void loadOrCreatePositions(final PublicChainElement relatedExpressionElement,
			final Stream.Builder<AbstractAnaphoraPositionForRepresentation> positionsToAdd,
			final List<Pair<AbstractAnaphoraPositionForRepresentation, AbstractAnaphoraPositionForRepresentation>> positionsToLink,
			final Function<PublicChainElement, PositionForRelatedExpression> getExistingRelatedExpressionPosition,
			final Function<PublicChainElement, PositionForRelatedExpression> createRelatedExpressionPosition,
			final Function<PublicChainElement, PositionForAnaphor> getExistingAnaphorPosition,
			final Function<PublicChainElement, PositionForAnaphor> createAnaphorPosition) {
		final var relatedExpressionPosition = loadOrCreatePosition(relatedExpressionElement,
				getExistingRelatedExpressionPosition, createRelatedExpressionPosition, positionsToAdd);
		loadOrCreateAnaphorPositions(relatedExpressionElement, getExistingAnaphorPosition, createAnaphorPosition,
				positionsToAdd).forEach(collectPositionsToLink(positionsToLink, relatedExpressionPosition));
	}

	static Stream<Pair<ReturnType, PositionForAnaphor>> loadOrCreateAnaphorPositions(
			final PublicChainElement relatedExpressionElement,
			final Function<PublicChainElement, PositionForAnaphor> getExistingPosition,
			final Function<PublicChainElement, PositionForAnaphor> createPosition,
			final Stream.Builder<AbstractAnaphoraPositionForRepresentation> positionsToAdd) {
		return getAnaphorElementsForRelatedExpressionElement(relatedExpressionElement).stream()
				.map(anaphorElement -> loadOrCreatePosition(anaphorElement, getExistingPosition, createPosition,
						positionsToAdd));
	}

	static <P extends AbstractAnaphoraPositionForRepresentation> Pair<ReturnType, P> loadOrCreatePosition(
			final PublicChainElement relatedExpressionElement,
			final Function<PublicChainElement, P> getExistingPosition,
			final Function<PublicChainElement, P> createPosition,
			final Stream.Builder<AbstractAnaphoraPositionForRepresentation> positionsToAdd) {
		final var existingPosition = getExistingPosition.apply(relatedExpressionElement);
		if (existingPosition != null) {
			return new ImmutablePair<>(ReturnType.Existing, existingPosition);
		}
		final var newPosition = createPosition.apply(relatedExpressionElement);
		positionsToAdd.add(newPosition);
		return new ImmutablePair<>(ReturnType.New, newPosition);
	}

	static PositionForRelatedExpression createPositionForRelatedExpression(
			final PerspectivationPosition perspectivationPosition) {
		return new PositionForRelatedExpression(perspectivationPosition.offset, perspectivationPosition.length,
				perspectivationPosition);
	}

	static PositionForAnaphor createPositionForAnaphor(final Anaphora anaphora,
			final PerspectivationPosition perspectivationPosition) {
		return new PositionForAnaphor(perspectivationPosition.offset, perspectivationPosition.length,
				perspectivationPosition, anaphora);
	}

	static Consumer<Pair<ReturnType, PositionForAnaphor>> collectPositionsToLink(
			final List<Pair<AbstractAnaphoraPositionForRepresentation, AbstractAnaphoraPositionForRepresentation>> positionsToLink,
			final Pair<ReturnType, PositionForRelatedExpression> relatedExpressionPosition) {
		return anaphorPosition -> {
			if (relatedExpressionPosition.getLeft() != ReturnType.Existing
					|| anaphorPosition.getLeft() != ReturnType.Existing) {
				positionsToLink
						.add(new ImmutablePair<>(relatedExpressionPosition.getRight(), anaphorPosition.getRight()));
			}
		};
	}

}
