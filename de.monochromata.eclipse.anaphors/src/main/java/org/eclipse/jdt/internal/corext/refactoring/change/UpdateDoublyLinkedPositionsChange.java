package org.eclipse.jdt.internal.corext.refactoring.change;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.ltk.core.refactoring.Change;

import de.monochromata.eclipse.position.DoublyLinkedPosition;

public class UpdateDoublyLinkedPositionsChange<P extends Position & DoublyLinkedPosition<P>>
		extends UpdatePositionsChange<P> {
	public final List<Pair<P, P>> positionsToLink;
	public final List<Pair<P, P>> positionsToUnlink;

	public UpdateDoublyLinkedPositionsChange(final IDocument document, final String positionDescription,
			final String positionCategory, final List<P> positionsToAdd, final List<Pair<P, P>> positionsToLink,
			final List<P> positionsToRemove, final List<Pair<P, P>> positionsToUnlink) {
		super(document, positionDescription, positionCategory, positionsToAdd, positionsToRemove);
		this.positionsToLink = positionsToLink;
		this.positionsToUnlink = positionsToUnlink;
	}

	@Override
	protected void performRemoval() {
		super.performRemoval();
		unlinkPositions();
	}

	protected void unlinkPositions() {
		positionsToUnlink.forEach(DoublyLinkedPosition::unlink);
	}

	@Override
	protected void performAddition() {
		linkPositions();
		super.performAddition();
	}

	protected void linkPositions() {
		positionsToLink.forEach(DoublyLinkedPosition::link);
	}

	@Override
	protected Change createUndo() {
		return new UpdateDoublyLinkedPositionsChange<>(document, positionDescription, positionCategory,
				positionsToRemove, positionsToUnlink, positionsToAdd, positionsToLink);
	}
}
