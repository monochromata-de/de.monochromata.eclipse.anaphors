package org.eclipse.jdt.internal.corext.refactoring.code;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jdt.internal.corext.refactoring.code.context.SharedAnaphorResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.code.context.SharedChainResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.code.context.SuccessfulChainResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.structure.CompilationUnitRewrite;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.persp.PerspectivationPosition;
import de.monochromata.eclipse.position.ChangeCollectingDoublyLinkedDeleter;

public interface ReRealizeAnaphors {

	static void reRealizeAnaphors(final IProgressMonitor pm,
			final List<Pair<Triple<Anaphora, PositionForAnaphor, Optional<String>>, Triple<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>>> oldAndNewAnaphorasAndContextsToUpdate,
			final CompilationUnitRewrite cuRewrite, final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
			final SharedChainResolutionContext sharedChainResolutionContext, final boolean declareFinal,
			final ChangeCollectingDoublyLinkedDeleter<PerspectivationPosition> maintainPerspectivation,
			final List<PerspectivationPosition> perspectivationPositionsToDelete,
			final ChangeCollectingDoublyLinkedDeleter<AbstractAnaphoraPositionForRepresentation> maintainAnaphoras,
			final List<AbstractAnaphoraPositionForRepresentation> anaphoraPositionsToDelete) throws CoreException {
		final List<Triple<Anaphora, PositionForAnaphor, Optional<String>>> anaphorasToUpdate = oldAndNewAnaphorasAndContextsToUpdate
				.stream().map(Pair::getLeft).collect(toList());
		// No text edits are recorded during de-realization, because re-realization will
		// override the existing code anyways.
		DeRealizeAnaphors.deRealizeWithoutGeneratingTextEdits(anaphorasToUpdate, maintainPerspectivation,
				perspectivationPositionsToDelete, maintainAnaphoras, anaphoraPositionsToDelete);

		final List<Triple<RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, SuccessfulChainResolutionContext>> anaphorasAndContextsToUpdate = oldAndNewAnaphorasAndContextsToUpdate
				.stream().map(Pair::getRight).collect(toList());
		RealizeAnaphors.realizeAnaphors(new SubProgressMonitor(pm, 2), sharedAnaphorResolutionContext,
				sharedChainResolutionContext, anaphorasAndContextsToUpdate, cuRewrite, declareFinal);
	}

}
