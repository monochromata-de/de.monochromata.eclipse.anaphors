package org.eclipse.jdt.internal.corext.refactoring.change;

import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ltk.core.refactoring.Change;

import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;

public class UpdateAnaphoraPositionsChange extends UpdateDoublyLinkedPositionsChange<AbstractAnaphoraPositionForRepresentation> {

	public UpdateAnaphoraPositionsChange(final IDocument document,
			final List<AbstractAnaphoraPositionForRepresentation> positionsToAdd,
			final List<Pair<AbstractAnaphoraPositionForRepresentation, AbstractAnaphoraPositionForRepresentation>> positionsToLink,
			final List<AbstractAnaphoraPositionForRepresentation> positionsToRemove,
			final List<Pair<AbstractAnaphoraPositionForRepresentation, AbstractAnaphoraPositionForRepresentation>> positionsToUnlink) {
		super(document, "anaphora", ANAPHORA_CATEGORY, positionsToAdd, positionsToLink, positionsToRemove,
				positionsToUnlink);
	}

	@Override
	protected Change createUndo() {
		return new UpdateAnaphoraPositionsChange(document, positionsToRemove, positionsToUnlink, positionsToAdd,
				positionsToLink);
	}

}
