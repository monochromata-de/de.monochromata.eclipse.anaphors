package org.eclipse.jdt.internal.corext.refactoring.code.context;

import static de.monochromata.anaphors.ast.chain.ChainTraversal.getAnaphorElements;
import static java.util.stream.Collectors.toMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicChainElement;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jface.text.Position;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.eclipse.anaphors.MultipleOriginalAnaphoras;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class SuccessfulChainResolutionContext implements ChainResolutionContext {

	public final List<Position> definiteExpressionPositions;
	public final Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, Optional<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>>> originalAnaphoraByAnaphorPart;
	public final PublicChainElement anaphorChainRoot;
	public final Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, Anaphora> resolvedAnaphoraByAnaphorPart;
	public final Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, Anaphora> anaphoraToBeRealizedByAnaphorPart = new HashMap<>();
	public Optional<String> guessedTempName;

	public SuccessfulChainResolutionContext(final List<Position> definiteExpressionPositions,
			final PublicChainElement anaphorChainRoot,
			final Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, Anaphora> resolvedAnaphoraByAnaphorPart) {
		this.definiteExpressionPositions = definiteExpressionPositions;
		originalAnaphoraByAnaphorPart = collectOriginalAnaphoraByAnaphorPart(anaphorChainRoot);
		this.anaphorChainRoot = anaphorChainRoot;
		this.resolvedAnaphoraByAnaphorPart = resolvedAnaphoraByAnaphorPart;
	}

	protected static Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, Optional<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>>> collectOriginalAnaphoraByAnaphorPart(
			final PublicChainElement anaphorChainRoot) {
		return getAnaphorElements(anaphorChainRoot).map(anaphorElement -> anaphorElement.anaphorAttachment)
				.map(attachment -> (MultipleOriginalAnaphoras) attachment)
				.flatMap(originalAnaphoras -> originalAnaphoras.originalAnaphoraByAnaphorPart.entrySet().stream())
				.collect(toMap(Entry::getKey, Entry::getValue));

	}

}
