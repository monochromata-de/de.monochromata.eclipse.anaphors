package org.eclipse.jdt.internal.corext.refactoring.code.context;

import static java.util.Objects.requireNonNull;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jface.text.source.IAnnotationModel;

import de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPosition;

public class SharedAnaphorResolutionContext {

    public final ICompilationUnit cu;
    public final IAnnotationModel annotationModel;
    public final MemberTrackingPosition trackingPositionToReResolve;
    public CompilationUnit compilationUnitNode;

    public SharedAnaphorResolutionContext(final ICompilationUnit cu, final CompilationUnit compilationUnitNode,
            final IAnnotationModel annotationModel, final MemberTrackingPosition trackingPositionToReResolve) {
        this(cu, annotationModel, trackingPositionToReResolve);
        this.compilationUnitNode = requireNonNull(compilationUnitNode);
    }

    public SharedAnaphorResolutionContext(final ICompilationUnit cu, final IAnnotationModel annotationModel,
            final MemberTrackingPosition trackingPositionToReResolve) {
        this.cu = cu;
        this.annotationModel = annotationModel;
        this.trackingPositionToReResolve = trackingPositionToReResolve;
    }

}
