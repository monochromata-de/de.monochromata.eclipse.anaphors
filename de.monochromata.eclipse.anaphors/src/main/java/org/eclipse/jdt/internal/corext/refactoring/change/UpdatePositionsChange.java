package org.eclipse.jdt.internal.corext.refactoring.change;

import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapPositionExceptions;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

public class UpdatePositionsChange<P extends Position> extends Change {

	protected final IDocument document;
	protected final String positionDescription;
	protected final String positionCategory;
	public final List<P> positionsToAdd;
	public final List<P> positionsToRemove;

	public UpdatePositionsChange(final IDocument document, final String positionDescription,
			final String positionCategory, final List<P> positionsToAdd, final List<P> positionsToRemove) {
		this.document = document;
		this.positionDescription = positionDescription;
		this.positionCategory = positionCategory;
		this.positionsToAdd = positionsToAdd;
		this.positionsToRemove = positionsToRemove;
	}

	@Override
	public String getName() {
		return "Update " + positionDescription + " positions";
	}

	@Override
	public void initializeValidationData(final IProgressMonitor pm) {
	}

	@Override
	public RefactoringStatus isValid(final IProgressMonitor pm) throws CoreException, OperationCanceledException {
		return new RefactoringStatus(); // has severity OK
	}

	@Override
	public Change perform(final IProgressMonitor pm) throws CoreException {
		performRemoval();
		performAddition();
		return createUndo();
	}

	protected void performRemoval() {
		removePositions();
	}

	protected void removePositions() {
		positionsToRemove.forEach(this::removePosition);
	}

	protected void removePosition(final P position) {
		wrapPositionExceptions(() -> document.removePosition(positionCategory, position));
	}

	protected void performAddition() {
		addPositions();
	}

	protected void addPositions() {
		positionsToAdd.forEach(this::addPosition);
	}

	protected void addPosition(final P position) {
		wrapPositionExceptions(() -> document.addPosition(positionCategory, position));
	}

	protected Change createUndo() {
		return new UpdatePositionsChange<>(document, positionDescription, positionCategory, positionsToRemove,
				positionsToAdd);
	}

	@Override
	public Object getModifiedElement() {
		// The change is not directly related to a single element
		return null;
	}

}
