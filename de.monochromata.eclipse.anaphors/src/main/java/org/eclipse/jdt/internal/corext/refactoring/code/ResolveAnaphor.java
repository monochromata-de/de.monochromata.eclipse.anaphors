package org.eclipse.jdt.internal.corext.refactoring.code;

import static de.monochromata.eclipse.anaphors.editor.AmbiguityAnnotationCreation.createProblemAnnotationForAnaphoraRelations;
import static de.monochromata.eclipse.anaphors.editor.AmbiguityAnnotationCreation.createProblemAnnotationForResolutions;
import static java.util.Collections.singletonList;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jdt.internal.corext.refactoring.code.context.AnaphorResolutionContext;
import org.eclipse.jdt.internal.corext.refactoring.code.context.SharedAnaphorResolutionContext;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import de.monochromata.anaphors.ast.ASTBasedAnaphorResolution;
import de.monochromata.anaphors.cog.ActivationBasedAnaphorResolution;
import de.monochromata.anaphors.cog.Resolution;

public interface ResolveAnaphor {

	static RefactoringStatus resolveAnaphor(final Expression expr,
			final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
			final AnaphorResolutionContext anaphorResolutionContext, final CompilationUnit cu,
			final ActivationBasedAnaphorResolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> activationBasedAnaphorResolution,
			final ASTBasedAnaphorResolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> astBasedAnaphorResolution,
			final List<Pair<Annotation, Position>> problemAnnotationsToAdd) {
		final Pair<RefactoringStatus, PublicAnaphora> activationBasedResult = resolveActivationBasedAnaphor(expr,
				anaphorResolutionContext, cu, sharedAnaphorResolutionContext.annotationModel,
				activationBasedAnaphorResolution, problemAnnotationsToAdd);
		if (activationBasedResult.getRight() != null) {
			anaphorResolutionContext.potentialAnaphoras = singletonList(activationBasedResult.getRight());
			return null;
		} else if (activationBasedResult.getLeft() != null) {
			return activationBasedResult.getLeft();
		}
		return resolveAstBasedAnaphor(expr, cu, sharedAnaphorResolutionContext, anaphorResolutionContext,
				astBasedAnaphorResolution, problemAnnotationsToAdd);
	}

	/**
	 * @return a pair containing either a successfully prepared anaphora relation, a
	 *         {@link RefactoringStatus} to signal an error, or none of both if no
	 *         potential anaphora relations was found via activation.
	 */
	static Pair<RefactoringStatus, PublicAnaphora> resolveActivationBasedAnaphor(final Expression expr,
			final AnaphorResolutionContext anaphorResolutionContext, final CompilationUnit cu,
			final IAnnotationModel annotationModel,
			final ActivationBasedAnaphorResolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> activationBasedAnaphorResolution,
			final List<Pair<Annotation, Position>> problemAnnotationsToAdd) {
		final List<Resolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>> resolutions = activationBasedAnaphorResolution
				.resolveAnaphor(anaphorResolutionContext.originalAnaphor, expr, cu, System.currentTimeMillis(),
						ResolveAnaphorsRefactoring.NUMBER_OF_CHUNKS_TO_CONSIDER);
		if (resolutions.isEmpty()) {
			return new ImmutablePair<>(null, null);
		} else if (resolutions.size() > 1) {
			problemAnnotationsToAdd.add(createProblemAnnotationForResolutions(cu,
					anaphorResolutionContext.definiteExpressionPosition, resolutions));
			final String msg = "More than one activation-based resolutions for anaphor "
					+ anaphorResolutionContext.originalAnaphor + ": " + resolutions;
			final RefactoringStatus status = RefactoringStatus.createFatalErrorStatus(msg);
			return new ImmutablePair<>(status, null);
		} else {
			try {
				// TODO: Must not be performed here, but in the change in
				// ResolveAnaphorRefactoring
				final Resolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> resolution = resolutions
						.get(0);
				final PublicAnaphora anaphora = resolution.getPreparatoryTransformation()
						.perform(resolution.getCheckResult(), resolution.getPreliminaryAnaphora());
				return new ImmutablePair<>(null, anaphora);
			} catch (final Exception e) {
				// TODO: Avoid this exception earlier
				e.printStackTrace();
				return new ImmutablePair<>(null, null);
			}
		}
	}

	static RefactoringStatus resolveAstBasedAnaphor(final Expression expr, final CompilationUnit cu,
			final SharedAnaphorResolutionContext sharedAnaphorResolutionContext,
			final AnaphorResolutionContext anaphorResolutionContext,
			final ASTBasedAnaphorResolution<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> astBasedAnaphorResolution,
			final List<Pair<Annotation, Position>> problemAnnotationsToAdd) {
		anaphorResolutionContext.potentialAnaphoras = astBasedAnaphorResolution
				.resolveAnaphor(anaphorResolutionContext.originalAnaphor, expr, cu);
		if (anaphorResolutionContext.potentialAnaphoras.isEmpty()) {
			final String msg = "Anaphor " + anaphorResolutionContext.originalAnaphor
					+ " could not be resolved: no potential anaphora relations found";
			return RefactoringStatus.createFatalErrorStatus(msg);
		} else if (anaphorResolutionContext.potentialAnaphoras.size() > 1) {
			problemAnnotationsToAdd.add(createProblemAnnotationForAnaphoraRelations(cu,
					anaphorResolutionContext.definiteExpressionPosition, anaphorResolutionContext.potentialAnaphoras));
			final String msg = "More than one AST-based resolutions for anaphor "
					+ anaphorResolutionContext.originalAnaphor + ": " + anaphorResolutionContext.potentialAnaphoras;
			return RefactoringStatus.createFatalErrorStatus(msg);
		} else { // implies anaphoraRelations.size() == 1
			return null;
		}
	}

}
