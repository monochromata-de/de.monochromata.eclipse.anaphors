package org.eclipse.jdt.core.dom.anaphors.check;

import static java.lang.Integer.compare;
import static org.eclipse.jdt.core.dom.anaphors.DomTraversal.getStatement;
import static org.eclipse.jdt.core.dom.anaphors.DomTraversal.isAfterOrEqual;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.anaphors.DomTraversal;

/**
 * @see #selectCallChain(List, Expression, BodyDeclaration)
 */
public interface CallChainSelection {

	/**
	 * Select a call chain from the given candidate call chains that
	 * <ol>
	 * <li>contains an invocation of the invocable containing the downstream
	 * expression
	 * <li>starts after the upstream expression or includes the upstream
	 * expression
	 * <li>is the first of the shortest ones of the remaining call chains
	 * <li>or is empty if no such call chain has been found.
	 * </ol>
	 */
	static List<Invocation> selectCallChain(final List<List<Invocation>> candidateCallChains,
			final ASTNode upstreamNode, final BodyDeclaration downstreamBodyDeclaration, final CompilationUnit scope) {
		final Map<Invocation, BodyDeclaration> bodyDeclarationsCache = new WeakHashMap<>();
		return candidateCallChains.stream()
				.filter(callChain -> containsInvocationOf(callChain, downstreamBodyDeclaration, scope,
						bodyDeclarationsCache))
				.filter(callChain -> startsAfterOrIncludesUpstreamNode(callChain, upstreamNode))
				.min((chain1, chain2) -> compare(chain1.size(), chain2.size())).orElseGet(Collections::emptyList);
	}

	static boolean containsInvocationOf(final List<Invocation> callChain,
			final BodyDeclaration downstreamBodyDeclaration, final CompilationUnit scope,
			final Map<Invocation, BodyDeclaration> bodyDeclarationsCache) {
		return callChain.stream().filter(
				invocation -> isInvocationOf(invocation, downstreamBodyDeclaration, scope, bodyDeclarationsCache))
				.findFirst().isPresent();
	}

	static boolean isInvocationOf(final Invocation invocation, final BodyDeclaration downstreamBodyDeclaration,
			final CompilationUnit scope, final Map<Invocation, BodyDeclaration> bodyDeclarationsCache) {
		final BodyDeclaration invocationDeclaration = getBodyDeclaration(invocation, scope, bodyDeclarationsCache);
		if (invocationDeclaration == null) {
			// TODO: body declarations from external CompilationUnits/ClassFiles
			// cannot currently be obtained and hence cannot be analyzed yet
			return false;
		}
		return invocationDeclaration.equals(downstreamBodyDeclaration);
	}

	static BodyDeclaration getBodyDeclaration(final Invocation invocation, final CompilationUnit scope,
			final Map<Invocation, BodyDeclaration> bodyDeclarationsCache) {
		final BodyDeclaration cachedDeclaration = bodyDeclarationsCache.get(invocation);
		if (cachedDeclaration != null) {
			return cachedDeclaration;
		}
		final BodyDeclaration declaration = DomTraversal.getBodyDeclaration(invocation, scope);
		bodyDeclarationsCache.put(invocation, declaration);
		return declaration;
	}

	static boolean startsAfterOrIncludesUpstreamNode(final List<Invocation> callChain, final ASTNode upstreamNode) {
		return startsAfterOrIncludesUpstreamExpression(callChain.get(0), upstreamNode);
	}

	static boolean startsAfterOrIncludesUpstreamExpression(final Invocation invocation, final ASTNode upstreamNode) {
		return startsAfter(invocation.getASTNode(), upstreamNode);
	}

	static boolean startsAfter(final ASTNode astNode, final ASTNode upstreamNode) {
		// TODO: Add tests for upstream nodes like variable declarations for
		// parameter declarations
		if (astNode instanceof Statement) {
			final Statement upstreamStatement = getStatement(upstreamNode);
			// TODO: Needs null-check if upstreamNode is a parameter declaration
			// (VariableDeclaration)
			return isAfterOrEqual(upstreamStatement, (Statement) astNode);
		} else if (astNode instanceof Expression) {
			final Statement upstreamStatement = getStatement(upstreamNode);
			// TODO: Needs null-check if upstreamNode is a parameter declaration
			// (VariableDeclaration)
			return isAfterOrEqual(upstreamStatement, getStatement(astNode));
		} else {
			throw new IllegalArgumentException(
					"astNode " + astNode.getClass().getName() + " is neither a Statement, nor an Expression");
		}
	}

}
