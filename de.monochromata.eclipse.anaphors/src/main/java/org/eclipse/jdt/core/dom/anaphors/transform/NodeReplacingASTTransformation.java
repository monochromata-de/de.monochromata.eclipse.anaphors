package org.eclipse.jdt.core.dom.anaphors.transform;

import static de.monochromata.eclipse.anaphors.editor.update.AstBasedAnaphoraUpdating.updateAnaphoraWithPositionsForAstNodes;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;

import de.monochromata.anaphors.ast.transform.ASTTransformation;

/**
 * An {@link ASTTransformation} that obtains fresh AST nodes after applying a
 * delegate transformation.
 */
public class NodeReplacingASTTransformation implements
		ASTTransformation<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> {

	private final CompilationUnit existingScope;
	private final Transformation delegate;

	public NodeReplacingASTTransformation(final CompilationUnit existingScope, final Transformation delegate) {
		this.existingScope = existingScope;
		this.delegate = delegate;
	}

	@Override
	public PublicAnaphora perform(final PublicAnaphora preliminaryAnaphora) {
		return updateAnaphoraWithPositionsForAstNodes(preliminaryAnaphora, existingScope, delegate::perform);
	}

}
