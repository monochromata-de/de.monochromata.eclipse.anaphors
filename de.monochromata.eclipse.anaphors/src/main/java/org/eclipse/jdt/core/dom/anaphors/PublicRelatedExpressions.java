package org.eclipse.jdt.core.dom.anaphors;

import static java.lang.Double.parseDouble;
import static org.eclipse.jdt.core.dom.AST.JLS10;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.BooleanLiteral;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.LambdaExpression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.NullLiteral;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SimpleType;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.VariableDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.internal.corext.dom.Bindings;
import org.eclipse.jface.text.DocumentEvent;

import de.monochromata.anaphors.ast.feature.Feature;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalTempVariableContents;
import de.monochromata.anaphors.ast.spi.RelatedExpressionsSpi;
import de.monochromata.anaphors.perspectivation.Perspectivation;
import de.monochromata.eclipse.persp.PerspectivationPosition;

/**
 * @since 3.11
 */
public class PublicRelatedExpressions implements
        RelatedExpressionsSpi<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression> {

    @Override
    public Type getReservedTypeVar(final CompilationUnit scope) {
        final SimpleName varName = scope.getAST().newSimpleName("var");
        return scope.getAST().newSimpleType(varName);
    }

    public boolean isParameterDeclaration(final ASTNode node) {
        return node instanceof SingleVariableDeclaration;
    }

    public boolean isLocalVariableDeclaration(final ASTNode node) {
        return (node instanceof VariableDeclaration && !isParameterDeclaration(node))
                || isVariableDeclarationStatementWithSingleFragment(node);
    }

    @Override
    public boolean isOnlyFragmentOfMultiVariable(final ASTNode node1, final ASTNode node2) {
        return isOnlyFragmentOfVariableDeclarationExpression(node1, node2)
                || isOnlyFragmentOfVariableDeclarationStatement(node1, node2);
    }

    protected boolean isOnlyFragmentOfVariableDeclarationExpression(final ASTNode node1, final ASTNode node2) {
        return node1 instanceof VariableDeclarationExpression && node2 instanceof VariableDeclarationFragment
                && ((VariableDeclarationExpression) node1).fragments().size() == 1
                && ((VariableDeclarationExpression) node1).fragments().get(0) == node2;
    }

    protected boolean isOnlyFragmentOfVariableDeclarationStatement(final ASTNode node1, final ASTNode node2) {
        return node1 instanceof VariableDeclarationStatement && node2 instanceof VariableDeclarationFragment
                && ((VariableDeclarationStatement) node1).fragments().size() == 1
                && ((VariableDeclarationStatement) node1).fragments().get(0) == node2;
    }

    /**
     * This condition is introduced so the local temp variables
     * (LocalVariableStatement) introduced by CIC are re-resolved with the same
     * related expression position as LVD (i.e. from a LocalVariableStatement, not
     * from a VariableDeclarationFragment which is a VariableDeclaration).
     */
    protected boolean isVariableDeclarationStatementWithSingleFragment(final ASTNode node) {
        return node instanceof VariableDeclarationStatement
                && ((VariableDeclarationStatement) node).fragments().size() == 1;
    }

    protected VariableDeclarationFragment getOnlyFragmentInVariableDeclarationStatement(final ASTNode node) {
        return ((VariableDeclarationFragment) ((VariableDeclarationStatement) node).fragments().get(0));
    }

    public String getLocalDeclarationIdentifier(final ASTNode node) {
        return applyToLocalDeclaration(node, "variable identifier", decl -> decl.getName().getIdentifier());
    }

    public IVariableBinding resolveLocalDeclarationBinding(final ASTNode node, final CompilationUnit scope) {
        return applyToLocalDeclaration(node, "variable binding", VariableDeclaration::resolveBinding);
    }

    public int getLengthOfLocalDeclarationTypeForTempVar(final ASTNode relatedExpression, final CompilationUnit scope) {
        final ITypeBinding typeBinding = resolveLocalDeclarationType(relatedExpression, scope);
        return getLengthOfNormalizedTypeBinding(typeBinding, scope);
    }

    public Type getLocalDeclarationTypeForTempVar(final ASTNode node, final CompilationUnit scope,
            final Function<ITypeBinding, Type> importRewrite) {
        return importRewrite.apply(resolveLocalDeclarationType(node, scope));
    }

    public ITypeBinding resolveLocalDeclarationType(final ASTNode node, final CompilationUnit scope) {
        return applyToLocalDeclaration(node, "variable type binding", decl -> decl.resolveBinding().getType());
    }

    protected <T> T applyToLocalDeclaration(final ASTNode node, final String targetDescription,
            final Function<VariableDeclaration, T> transformer) {
        if (node instanceof VariableDeclaration) {
            return transformer.apply((VariableDeclaration) node);
        } else if (node instanceof VariableDeclarationStatement) {
            if (isVariableDeclarationStatementWithSingleFragment(node)) {
                return transformer.apply(getOnlyFragmentInVariableDeclarationStatement(node));
            }
            throw new IllegalArgumentException("Cannot obtain " + targetDescription + " from "
                    + node.getClass().getName() + " with multiple variable declaration fragments");
        } else {
            throw new IllegalArgumentException("Unexpected node type: " + node.getClass().getName());
        }
    }

    @Override
    public boolean hasInitializer(final ASTNode node, final ASTNode potentialInitializer) {
        if (!(potentialInitializer instanceof Expression)) {
            return false;
        } else if (node instanceof VariableDeclaration) {
            return ((VariableDeclaration) node).getInitializer() == potentialInitializer;
        } else if (node instanceof LambdaExpression) {
            return false;
        } else if (node instanceof MethodDeclaration) {
            return false;
        } else if (node instanceof VariableDeclarationStatement) {
            return ((VariableDeclarationStatement) node).fragments().size() == 1
                    && ((VariableDeclarationFragment) ((VariableDeclarationStatement) node).fragments().get(0))
                            .getInitializer() == potentialInitializer;
        } else {
            throw new IllegalArgumentException("Unexpected node type: " + node.getClass().getName());
        }
    }

    public boolean isClassInstanceCreationExpression(final ASTNode node) {
        return node instanceof ClassInstanceCreation;
    }

    public int getLengthOfClassInstanceCreationTypeForTempVar(final ASTNode relatedExpression,
            final CompilationUnit scope) {
        final ITypeBinding typeBinding = resolveClassInstanceCreationType(relatedExpression, scope);
        return getLengthOfNormalizedTypeBinding(typeBinding, scope);
    }

    public Type getClassInstanceCreationTypeForTempVar(final ASTNode expression, final CompilationUnit scope,
            final Function<ITypeBinding, Type> importRewrite) {
        return importRewrite.apply(resolveClassInstanceCreationType(expression, scope));
    }

    public ITypeBinding resolveClassInstanceCreationType(final ASTNode expression, final CompilationUnit scope) {
        return ((ClassInstanceCreation) expression).resolveTypeBinding();
    }

    public boolean isMethodInvocationExpression(final ASTNode node) {
        return node instanceof MethodInvocation && !isQualifierOfOtherMethodInvocation((MethodInvocation) node);
    }

    protected boolean isQualifierOfOtherMethodInvocation(final MethodInvocation node) {
        return node.getParent() instanceof MethodInvocation
                && ((MethodInvocation) node.getParent()).getExpression() == node;
    }

    public String getMethodInvocationIdentifier(final ASTNode relatedExpression) {
        return ((MethodInvocation) relatedExpression).getName().getIdentifier();
    }

    @Override
    public IMethodBinding resolveMethodInvocationBinding(final ASTNode relatedExpression, final CompilationUnit scope) {
        return ((MethodInvocation) relatedExpression).resolveMethodBinding();
    }

    public int getLengthOfMethodInvocationReturnTypeForTempVar(final ASTNode relatedExpression,
            final CompilationUnit scope) {
        final ITypeBinding typeBinding = resolveMethodInvocationReturnType(relatedExpression, scope);
        return getLengthOfNormalizedTypeBinding(typeBinding, scope);
    }

    protected int getLengthOfNormalizedTypeBinding(final ITypeBinding typeBinding, final CompilationUnit scope) {
        final ITypeBinding normalizedTypeBinding = Bindings.normalizeForDeclarationUse(typeBinding, scope.getAST());
        return normalizedTypeBinding.getName().length();
    }

    public Type getMethodInvocationReturnTypeForTempVar(final ASTNode relatedExpression, final CompilationUnit scope,
            final Function<ITypeBinding, Type> importRewrite) {
        final ITypeBinding typeBinding = resolveMethodInvocationReturnType(relatedExpression, scope);
        return importRewrite.apply(typeBinding);
    }

    public ITypeBinding resolveMethodInvocationReturnType(final ASTNode relatedExpression,
            final CompilationUnit scope) {
        return ((MethodInvocation) relatedExpression).resolveTypeBinding();
    }

    @Override
    public Set<Feature<String>> getFeatures(final ASTNode relatedExpression) {
        final Set<Feature<String>> features = new HashSet<>();
        getFeatures(relatedExpression, features);
        return features;
    }

    public void getFeatures(final ASTNode node, final Set<Feature<String>> features) {
        if (node instanceof ClassInstanceCreation) {
            getFeatures((ClassInstanceCreation) node, features);
        } else if (isLocalVariableDeclaration(node)) {
            getFeaturesForLVD(node, features);
        } else if (isParameterDeclaration(node)) {
            // Currently, a parameter declaration contains only type and name that are not
            // considered features.
            // TODO: Might later have features from annotations
        } else if (node instanceof MethodInvocation) {
            getFeatures((MethodInvocation) node, features);
        } else if (node instanceof SimpleName) {
            DomFeatureContainer.parseIdentifier(((SimpleName) node).getIdentifier(), features);
        } else if (node instanceof NumberLiteral) {
            features.add(DomFeatureContainer.createNormalizedFeature(((NumberLiteral) node).getToken()));
        } else if (node instanceof BooleanLiteral) {
            DomFeatureContainer.parseStringLiteral(Boolean.toString(((BooleanLiteral) node).booleanValue()), features);
        } else if (node instanceof CharacterLiteral) {
            features.add(DomFeatureContainer.createNormalizedFeature(((CharacterLiteral) node).charValue() + ""));
        } else if (node instanceof StringLiteral) {
            getFeatures((StringLiteral) node, features);
        } else if (node instanceof NullLiteral) {
            DomFeatureContainer.parseStringLiteral("null", features);
        } // ignore other nodes
    }

    /**
     * Parses features of class-instance creation expressions created from the
     * arguments to the class-instance creation expression.
     *
     * <p>
     * TODO: also parse features from type parameters
     * </p>
     *
     * @param cic the class-instance creation expression whose features are to be
     *            obtained.
     * @return the features of the class-instance creation expression.
     */
    public void getFeatures(final ClassInstanceCreation cic, final Set<Feature<String>> features) {
        // TODO: Test with features created for arguments of CIC passed as
        // argument of another CIC. Maybe need to create meaningfully structured
        // features.
        for (final Object argument : cic.arguments()) {
            final Expression expression = (Expression) argument;
            DomVisiting.collectSimpleNamesAndLiterals(expression).forEach(astNode -> getFeatures(astNode, features));
        }
        Optional.ofNullable(cic.getAnonymousClassDeclaration()).ifPresent(declaration -> DomVisiting
                .collectSimpleNamesAndLiterals(declaration).forEach(astNode -> getFeatures(astNode, features)));
    }

    public void getFeatures(final MethodInvocation mi, final Set<Feature<String>> features) {
        // TODO: Test with features created for arguments of MI passed as
        // argument of another MI. Maybe need to create meaningfully structured
        // features.
        for (final Object argument : mi.arguments()) {
            final var expression = (Expression) argument;
            DomVisiting.collectSimpleNamesAndLiterals(expression).forEach(astNode -> getFeatures(astNode, features));
        }
    }

    public void getFeaturesForLVD(final ASTNode node, final Set<Feature<String>> features) {
        if (node instanceof VariableDeclaration && !isParameterDeclaration(node)) {
            getFeatures(((VariableDeclaration) node).getInitializer(), features);
        } else if (isVariableDeclarationStatementWithSingleFragment(node)) {
            final var lvd = (VariableDeclarationStatement) node;
            final var fragment = (VariableDeclarationFragment) lvd.fragments().get(0);
            getFeatures(fragment.getInitializer(), features);
        }
    }

    public void getFeatures(final StringLiteral literal, final Set<Feature<String>> features) {
        // TODO: Test with complex features? I.e. new Instance("Hello World!")
        // and an anaphor helloInstance ... i.e.
        // TODO: Remove/replace non-identifier characters?
        DomFeatureContainer.parseStringLiteral(literal.getLiteralValue(), features);
    }

    @Override
    public boolean compare(final ASTNode node1, final ASTNode node2) {
        return DomComparison.compare(node1, node2);
    }

    @Override
    public String getDescription(final ASTNode node) {
        try {
            final int start = node.getStartPosition();
            final int end = start + node.getLength();
            return getModelCompilationUnit(node).getSource().substring(start, end);
        } catch (final JavaModelException e) {
            return "<no source available>";
        }
    }

    @Override
    public int getLine(final ASTNode node) {
        return getCompilationUnit(node).getLineNumber(node.getStartPosition());
    }

    @Override
    public int getColumn(final ASTNode node) {
        return getCompilationUnit(node).getColumnNumber(node.getStartPosition());
    }

    protected ICompilationUnit getModelCompilationUnit(final ASTNode node) {
        return (ICompilationUnit) getCompilationUnit(node).getJavaElement();
    }

    protected CompilationUnit getCompilationUnit(final ASTNode node) {
        return (CompilationUnit) node.getRoot();
    }

    @Override
    public int getLengthOfSimpleNameOfType(final Type type) {
        if (isReservedTypeVar(type)) {
            return "var".length();
        }
        return getLengthOfSimpleName(type.resolveBinding());
    }

    private int getLengthOfSimpleName(final ITypeBinding typeBinding) {
        return typeBinding.getName().length();
    }

    protected boolean isReservedTypeVar(final Type type) {
        if (!(type instanceof SimpleType)) {
            return false;
        }
        final SimpleType simpleType = (SimpleType) type;
        if (!(simpleType.getName() instanceof SimpleName)) {
            return false;
        }
        final SimpleName simpleName = (SimpleName) simpleType.getName();
        return "var".equals(simpleName.getIdentifier());
    }

    @Override
    public int getLength(final String identifier) {
        return identifier.length();
    }

    @Override
    public String guessTempName(final PublicRelatedExpression relatedExpression,
            final List<Pair<LocalTempVariableContents, String>> variableContentsAndAnaphors,
            final CompilationUnit scope) {
        return variableContentsAndAnaphors.stream()
                .map(indirectionAndAnaphor -> guessTempNameInternal(relatedExpression, indirectionAndAnaphor.getLeft(),
                        indirectionAndAnaphor.getRight(), scope))
                .reduce((name1, name2) -> name1.length() < name2.length() ? name1 : name2)
                .orElseThrow(() -> new IllegalArgumentException("No name found from " + variableContentsAndAnaphors));
    }

    public String guessTempNameInternal(final PublicRelatedExpression relatedExpression,
            final LocalTempVariableContents variableContents, final String anaphor, final CompilationUnit scope) {
        switch (variableContents) {
        case ANCHOR:
            final ITypeBinding resolvedType = relatedExpression.resolveType(scope);
            return guessTempNameInternal(resolvedType);
        case REFERENT:
            return anaphor;
        default:
            throw new IllegalArgumentException("Unknown variable contents: " + variableContents);
        }
    }

    protected String guessTempNameInternal(final ITypeBinding resolvedType) {
        return lowerCaseFirst(resolvedType.getErasure().getName());
    }

    protected static String lowerCaseFirst(final String name) {
        if (name.length() < 2) {
            return name.toLowerCase();
        }
        return Character.toLowerCase(name.charAt(0)) + name.substring(1);
    }

    @Override
    public String toQualifiedIdentifier(final String identifier) {
        // This method is required, because qualified identifiers (QI) and identifiers
        // (I) have different type parameters, even though this implementation has the
        // same type for both.
        return identifier;
    }

    @Override
    public String identifierToString(final String identifier) {
        return identifier;
    }

    @Override
    public String qualifiedIdentifierToString(final String qualifiedIdentifier) {
        return qualifiedIdentifier;
    }

    @Override
    public String getIdentifier(final ITypeBinding typeBinding) {
        return typeBinding.getErasure().getName();
    }

    @Override
    public boolean supportsLocalVariableTypeInference(final CompilationUnit scope) {
        final String sourceVersion = scope.getJavaElement().getJavaProject().getOption(JavaCore.COMPILER_SOURCE, true);
        final boolean supports = parseDouble(sourceVersion) >= JLS10;
        return supports;
    }

    @Override
    public PerspectivationPosition createPositionForNode(final ASTNode node, final Predicate<DocumentEvent> condition,
            final List<Perspectivation> perspectivations) {
        return new PerspectivationPosition(node.getStartPosition(), node.getLength(), condition, perspectivations);
    }
}
