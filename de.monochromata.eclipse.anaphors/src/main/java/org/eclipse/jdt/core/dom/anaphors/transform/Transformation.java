package org.eclipse.jdt.core.dom.anaphors.transform;

import org.eclipse.jdt.core.dom.CompilationUnit;

@FunctionalInterface
public interface Transformation {
	/**
	 * @return The new scope after the change, or {@literal null}, if the
	 *         transformation failed.
	 */
	CompilationUnit perform();
}
