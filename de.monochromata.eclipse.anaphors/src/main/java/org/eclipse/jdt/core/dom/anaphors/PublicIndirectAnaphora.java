package org.eclipse.jdt.core.dom.anaphors;

import java.util.function.Supplier;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;

import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.DefaultIndirectAnaphora;
import de.monochromata.anaphors.ast.RelatedExpressionPart;

public class PublicIndirectAnaphora extends
		DefaultIndirectAnaphora<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>
		implements PublicAnaphora {

	public PublicIndirectAnaphora(
			final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
			final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart,
			final String underspecifiedRelation, final boolean isUnderspecified) {
		super(relatedExpressionPart, anaphorPart, underspecifiedRelation, isUnderspecified);
	}

	public PublicIndirectAnaphora(
			final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
			final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart,
			final String underspecifiedRelation, final Supplier<String> customReferenceDescriptionSupplier,
			final boolean isUnderspecified) {
		super(relatedExpressionPart, anaphorPart, underspecifiedRelation, customReferenceDescriptionSupplier,
				isUnderspecified);
	}

}
