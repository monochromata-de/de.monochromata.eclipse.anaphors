package org.eclipse.jdt.core.dom.anaphors.check;

import static org.eclipse.jdt.core.dom.anaphors.DomTraversal.getBodyDeclaration;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.Expression;

import de.monochromata.anaphors.ast.spi.TransformationsSpi;

public interface SameInvocableCheck {

	/**
	 * @see TransformationsSpi#partOfSameInvocable(Object, Object)
	 */
	public static boolean partOfSameInvocable(final ASTNode node, final Expression expression) {
		// TODO: This is not strictly correct, because body declarations also
		// include type declarations, and enum declarations, enum constant
		// declarations, field declarations
		final BodyDeclaration bodyDeclaration1 = getBodyDeclaration(node);
		final BodyDeclaration bodyDeclaration2 = getBodyDeclaration(expression);
		return bodyDeclaration1 == bodyDeclaration2;
	}

}
