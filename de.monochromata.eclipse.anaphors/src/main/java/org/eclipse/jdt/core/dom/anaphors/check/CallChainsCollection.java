package org.eclipse.jdt.core.dom.anaphors.check;

import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;
import static org.eclipse.jdt.core.dom.anaphors.DomTraversal.getBodyDeclaration;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.stream.Stream;

import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.CompilationUnit;

/**
 * @see #collectCallChains(BodyDeclaration, int)
 */
public interface CallChainsCollection {

	/**
	 * Collects all call chains in the given body declaration recursively until
	 * the maximum call chain length is reached.
	 */
	static List<List<Invocation>> collectCallChains(final BodyDeclaration bodyDeclaration, final CompilationUnit scope,
			final int maximumCallChainLength) {
		return collectCallChains(bodyDeclaration, new WeakHashMap<>(), scope, 0, maximumCallChainLength)
				.collect(toList());
	}

	static Stream<LinkedList<Invocation>> collectCallChains(final BodyDeclaration bodyDeclaration,
			final Map<BodyDeclaration, List<LinkedList<Invocation>>> cacheOfSubChains, final CompilationUnit scope,
			final int currentCallChainLength, final int maxCallChainLength) {
		if (bodyDeclaration == null) {
			// TODO: This if-statements handles cases when an invocation refers
			// to a method/... that is not declared in the current compilation
			// unit. E.g. java.lang.Long.<init>(long) is defined in another
			// CompilationUnit / in another ClassFile. The JavaModel needs to be
			// searched for such CompilationUnits / ClassFiles.
			return Stream.empty();
		}
		final List<LinkedList<Invocation>> cachedSubChain = cacheOfSubChains.get(bodyDeclaration);
		if (cachedSubChain != null) {
			return cachedSubChain.stream();
		} else if (currentCallChainLength > maxCallChainLength) {
			return Stream.empty();
		}
		final List<LinkedList<Invocation>> subChain = createSubChain(bodyDeclaration, cacheOfSubChains, scope,
				currentCallChainLength, maxCallChainLength);
		cacheOfSubChains.put(bodyDeclaration, subChain);
		return subChain.stream();
	}

	static List<LinkedList<Invocation>> createSubChain(final BodyDeclaration bodyDeclaration,
			final Map<BodyDeclaration, List<LinkedList<Invocation>>> cacheOfSubChains, final CompilationUnit scope,
			final int currentCallChainLength, final int maxCallChainLength) {
		return InvocationsCollection.collectionInvocations(bodyDeclaration).stream()
				.flatMap(invocation -> collectCallChains(invocation, cacheOfSubChains, scope, currentCallChainLength,
						maxCallChainLength))
				.collect(toList());
	}

	static Stream<LinkedList<Invocation>> collectCallChains(final Invocation invocation,
			final Map<BodyDeclaration, List<LinkedList<Invocation>>> cacheOfSubChains, final CompilationUnit scope,
			final int currentCallChainLength, final int maxCallChainLength) {
		if ((currentCallChainLength + 1) >= maxCallChainLength) {
			final LinkedList<Invocation> chain = new LinkedList<>();
			chain.addFirst(invocation);
			return Stream.of(chain);
		}
		final LinkedList<LinkedList<Invocation>> subChain = collectCallChains(getBodyDeclaration(invocation, scope),
				cacheOfSubChains, scope, currentCallChainLength + 1, maxCallChainLength)
						.collect(toCollection(LinkedList::new));
		return prependToLists(subChain, invocation).stream();
	}

	static LinkedList<LinkedList<Invocation>> prependToLists(final LinkedList<LinkedList<Invocation>> callChains,
			final Invocation invocation) {
		if (callChains.isEmpty()) {
			final LinkedList<Invocation> initialChain = new LinkedList<>();
			initialChain.addFirst(invocation);
			callChains.addFirst(initialChain);
		} else {
			callChains.forEach(callChain -> prependToList(callChain, invocation));
		}
		return callChains;
	}

	static LinkedList<Invocation> prependToList(final LinkedList<Invocation> callChain, final Invocation invocation) {
		callChain.addFirst(invocation);
		return callChain;
	}

}
