package org.eclipse.jdt.core.dom.anaphors.check;

import java.util.function.Function;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.MethodInvocation;

public class DefaultInvocation implements Invocation {

	private final ConstructorInvocation constructorInvocation;
	private final MethodInvocation methodInvocation;
	private final ClassInstanceCreation classInstanceCreation;
	private final Function<DefaultInvocation, ASTNode> astNodeSupplier;

	public DefaultInvocation(final ConstructorInvocation constructorInvocation) {
		this(constructorInvocation, null, null, DefaultInvocation::getConstructorInvocation);
	}

	public DefaultInvocation(final MethodInvocation methodInvocation) {
		this(null, methodInvocation, null, DefaultInvocation::getMethodInvocation);
	}

	public DefaultInvocation(final ClassInstanceCreation classInstanceCreation) {
		this(null, null, classInstanceCreation, DefaultInvocation::getClassInstanceCreation);
	}

	private DefaultInvocation(final ConstructorInvocation constructorInvocation,
			final MethodInvocation methodInvocation, final ClassInstanceCreation classInstanceCreation,
			final Function<DefaultInvocation, ASTNode> astNodeSupplier) {
		this.constructorInvocation = constructorInvocation;
		this.methodInvocation = methodInvocation;
		this.classInstanceCreation = classInstanceCreation;
		this.astNodeSupplier = astNodeSupplier;
	}

	@Override
	public ASTNode getASTNode() {
		return astNodeSupplier.apply(this);
	}

	@Override
	public ConstructorInvocation getConstructorInvocation() {
		return constructorInvocation;
	}

	@Override
	public MethodInvocation getMethodInvocation() {
		return methodInvocation;
	}

	@Override
	public ClassInstanceCreation getClassInstanceCreation() {
		return classInstanceCreation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classInstanceCreation == null) ? 0 : classInstanceCreation.hashCode());
		result = prime * result + ((constructorInvocation == null) ? 0 : constructorInvocation.hashCode());
		result = prime * result + ((methodInvocation == null) ? 0 : methodInvocation.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final DefaultInvocation other = (DefaultInvocation) obj;
		if (classInstanceCreation == null) {
			if (other.classInstanceCreation != null) {
				return false;
			}
		} else if (!classInstanceCreation.equals(other.classInstanceCreation)) {
			return false;
		}
		if (constructorInvocation == null) {
			if (other.constructorInvocation != null) {
				return false;
			}
		} else if (!constructorInvocation.equals(other.constructorInvocation)) {
			return false;
		}
		if (methodInvocation == null) {
			if (other.methodInvocation != null) {
				return false;
			}
		} else if (!methodInvocation.equals(other.methodInvocation)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		if (isConstructorInvocation()) {
			return "DefaultInvocation [constructorInvocation=" + constructorInvocation + ", astNodeSupplier="
					+ astNodeSupplier + "]";
		} else if (isMethodInvocation()) {
			return "DefaultInvocation [methodInvocation=" + methodInvocation + ", astNodeSupplier=" + astNodeSupplier
					+ "]";
		} else if (isClassInstanceCreation()) {
			return "DefaultInvocation [classInstanceCreation=" + classInstanceCreation + ", astNodeSupplier="
					+ astNodeSupplier + "]";
		} else {
			return "DefaultInvocation [constructorInvocation=" + constructorInvocation + ", methodInvocation="
					+ methodInvocation + ", classInstanceCreation=" + classInstanceCreation + ", astNodeSupplier="
					+ astNodeSupplier + "]";
		}
	}

}
