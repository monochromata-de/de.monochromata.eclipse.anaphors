package org.eclipse.jdt.core.dom.anaphors;

import static org.eclipse.jdt.core.dom.anaphors.DomTraversal.getBodyDeclaration;
import static org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression.createForClassInstanceCreation;
import static org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression.createForLocalVariableDeclaration;
import static org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression.createForMethodInvocation;
import static org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression.createForParameterDeclaration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.LambdaExpression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.TypeDeclarationStatement;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;
import org.eclipse.jface.text.DocumentEvent;

import de.monochromata.anaphors.ast.relatedexp.RelatedExpressionsCollector;
import de.monochromata.anaphors.ast.relatedexp.strategy.ClassInstanceCreationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalVariableDeclarationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.MethodInvocationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.ParameterDeclarationStrategy;
import de.monochromata.eclipse.persp.PerspectivationPosition;

/**
 * @since 3.11
 */
public class PublicRelatedExpressionsCollector implements
        RelatedExpressionsCollector<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, PublicRelatedExpression> {

    private final ParameterDeclarationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression> parameterDeclarationStrategy;
    private final LocalVariableDeclarationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression> localVariableDeclarationStrategy;
    private final ClassInstanceCreationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression, PublicAnaphora> classInstanceCreationStrategy;
    private final MethodInvocationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression, PublicAnaphora> methodInvocationStrategy;
    private final PublicRelatedExpressions relatedExpressionsSpi;

    public PublicRelatedExpressionsCollector(
            final ParameterDeclarationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression> parameterDeclarationStrategy,
            final LocalVariableDeclarationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression> localVariableDeclarationStrategy,
            final ClassInstanceCreationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression, PublicAnaphora> classInstanceCreationStrategy,
            final MethodInvocationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression, PublicAnaphora> methodInvocationStrategy,
            final PublicRelatedExpressions relatedExpressionsSpi) {
        this.parameterDeclarationStrategy = parameterDeclarationStrategy;
        this.localVariableDeclarationStrategy = localVariableDeclarationStrategy;
        this.classInstanceCreationStrategy = classInstanceCreationStrategy;
        this.methodInvocationStrategy = methodInvocationStrategy;
        this.relatedExpressionsSpi = relatedExpressionsSpi;
    }

    @Override
    public List<PublicRelatedExpression> traverse(final Expression definiteExpression, final CompilationUnit scope) {
        // TODO: Use something like
        // ASTNodes.getVisibleLocalVariablesInLocalScope instead
        final BodyDeclaration bodyDeclaration = getBodyDeclaration(definiteExpression);
        final Set<ASTNode> scopesToBody = getScopesToBody(definiteExpression, bodyDeclaration);
        return traverse(definiteExpression, bodyDeclaration, scopesToBody);
    }

    protected Set<ASTNode> getScopesToBody(final Expression definiteExpression, final BodyDeclaration bodyDeclaration) {
        ASTNode currentParent = definiteExpression;
        final Set<ASTNode> scopes = new HashSet<>();
        while (currentParent != null && currentParent != bodyDeclaration) {
            scopes.add(currentParent);
            currentParent = currentParent.getParent();
        }
        return scopes;
    }

    protected List<PublicRelatedExpression> traverse(final Expression definiteExpression,
            final BodyDeclaration bodyDeclaration, final Set<ASTNode> scopesToBody) {
        final List<PublicRelatedExpression> potentialRelatedExpressions = new ArrayList<>();
        bodyDeclaration
                .accept(new Visitor(definiteExpression.getStartPosition(), potentialRelatedExpressions, scopesToBody));
        return potentialRelatedExpressions;
    }

    private class Visitor extends ASTVisitor {

        // TODO: Is the check of this limit sufficient for complex expressions
        // like e.g. "a.getB().foo(<definiteExpression>, session, message)"?
        private final int startPositionOfDefiniteExpression;
        private final List<PublicRelatedExpression> potentialRelatedExpressions;
        private final Set<ASTNode> scopesToBody;

        public Visitor(final int startPositionOfDefiniteExpression,
                final List<PublicRelatedExpression> potentialRelatedExpressions, final Set<ASTNode> scopesToBody) {
            this.startPositionOfDefiniteExpression = startPositionOfDefiniteExpression;
            this.potentialRelatedExpressions = potentialRelatedExpressions;
            this.scopesToBody = scopesToBody;
        }

        @Override
        public boolean visit(final MethodDeclaration node) {
            return node.getStartPosition() < startPositionOfDefiniteExpression;
        }

        @Override
        public boolean visit(final Block node) {
            return visitScope(node);
        }

        @Override
        public boolean visit(final DoStatement node) {
            return visitScope(node);
        }

        @Override
        public boolean visit(final EnhancedForStatement node) {
            return visitScope(node);
        }

        @Override
        public boolean visit(final ForStatement node) {
            return visitScope(node);
        }

        @Override
        public boolean visit(final IfStatement node) {
            return visitScope(node);
        }

        @Override
        public boolean visit(final SwitchStatement node) {
            return visitScope(node);
        }

        @Override
        public boolean visit(final TryStatement node) {
            return visitScope(node);
        }

        @Override
        public boolean visit(final TypeDeclarationStatement node) {
            return visitScope(node);
        }

        @Override
        public boolean visit(final WhileStatement node) {
            return visitScope(node);
        }

        protected boolean visitScope(final ASTNode node) {
            return scopesToBody.contains(node);
        }

        @Override
        public boolean visit(final SingleVariableDeclaration node) {
            if (node.getStartPosition() < startPositionOfDefiniteExpression) {
                if (parameterDeclarationStrategy != null && relatedExpressionsSpi.isParameterDeclaration(node)) {
                    collectForPD(node);
                    return true;
                } else if (localVariableDeclarationStrategy != null
                        && relatedExpressionsSpi.isLocalVariableDeclaration(node)) {
                    // This condition is not tested because the Java language does not use
                    // SingleVariableDeclaration with an initializer so far.
                    if (!initializerContainsAnaphor(node.getInitializer())) {
                        collectForLVD(node);
                    }
                    return true;
                }
            }
            return false;
        }

        @Override
        public boolean visit(final VariableDeclarationFragment node) {
            if (node.getStartPosition() < startPositionOfDefiniteExpression) {
                // TODO: Why is there no check for
                // relatedExpressionsSpi.isLocalVariableDeclaration(node) here?
                if (!initializerContainsAnaphor(node.getInitializer())) {
                    collectForLVD(node);
                }
                return true;
            }
            return false;
        }

        @Override
        public boolean visit(final LambdaExpression node) {
            if (node.getStartPosition() < startPositionOfDefiniteExpression && parameterDeclarationStrategy != null
                    && relatedExpressionsSpi.isParameterDeclaration(node)) {
                collectForPD(node);
                return true;
            } else if (visitScope(node)) {
                return true;
            }
            return false;
        }

        @Override
        public boolean visit(final VariableDeclarationExpression node) {
            if (node.getStartPosition() < startPositionOfDefiniteExpression && localVariableDeclarationStrategy != null
                    && relatedExpressionsSpi.isLocalVariableDeclaration(node)) {
                if (!hasFragmentWhoseInitializerContainsAnaphor(node.fragments())) {
                    collectForLVD(node);
                }
                return true;
            }
            return false;
        }

        @Override
        public boolean visit(final VariableDeclarationStatement node) {
            if (node.getStartPosition() < startPositionOfDefiniteExpression && localVariableDeclarationStrategy != null
                    && relatedExpressionsSpi.isLocalVariableDeclaration(node)) {
                if (!hasFragmentWhoseInitializerContainsAnaphor(node.fragments())) {
                    collectForLVD(node);
                }
                return true;
            }
            return false;
        }

        protected boolean hasFragmentWhoseInitializerContainsAnaphor(
                final List<VariableDeclarationFragment> fragments) {
            return fragments.stream().anyMatch(fragment -> initializerContainsAnaphor(fragment.getInitializer()));
        }

        protected boolean initializerContainsAnaphor(final Expression initializer) {
            return initializer != null && scopesToBody.contains(initializer);
        }

        protected boolean argumentsContainAnaphor(final List<Expression> arguments) {
            return arguments.stream().anyMatch(argument -> scopesToBody.contains(argument));
        }

        protected void collectForPD(final ASTNode node) {
            if (parameterDeclarationStrategy != null) {
                final var relatedExpression = createForParameterDeclaration(node);
                potentialRelatedExpressions.add(relatedExpression);
            }
        }

        protected void collectForLVD(final ASTNode node) {
            if (localVariableDeclarationStrategy != null) {
                // TODO: Move isEffectivelyFinal and canBeUsedInsteadOfBiPredicate to the
                // related expression strategy?!
                final var relatedExpression = createForLocalVariableDeclaration(node);
                potentialRelatedExpressions.add(relatedExpression);
            }
        }

        @Override
        public boolean visit(final ClassInstanceCreation expr) {
            if (expr.getStartPosition() < startPositionOfDefiniteExpression && classInstanceCreationStrategy != null
                    && relatedExpressionsSpi.isClassInstanceCreationExpression(expr)) {
                if (!argumentsContainAnaphor(expr.arguments())) {
                    collectForCIC(expr);
                }
                return true;
            }
            return false;
        }

        protected void collectForCIC(final ClassInstanceCreation expr) {
            if (classInstanceCreationStrategy != null) {
                // TODO: Move isEffectivelyFinal and canBeUsedInsteadOfBiPredicate to the
                // related expression strategy?!
                // TODO: Note that callbacks are fine as long as they do not cross project
                // boundaries
                final var relatedExpression = createForClassInstanceCreation(expr);
                potentialRelatedExpressions.add(relatedExpression);
            }
        }

        // TODO: Also visit ArrayCreation

        @Override
        public boolean visit(final MethodInvocation expr) {
            if (expr.getStartPosition() < startPositionOfDefiniteExpression && methodInvocationStrategy != null
                    && relatedExpressionsSpi.isMethodInvocationExpression(expr)) {
                if (!argumentsContainAnaphor(expr.arguments())) {
                    collectForMI(expr);
                }
                return true;

            }
            return false;
        }

        protected void collectForMI(final MethodInvocation expr) {
            if (methodInvocationStrategy != null) {
                final PublicRelatedExpression relatedExpression = createForMethodInvocation(expr);
                potentialRelatedExpressions.add(relatedExpression);
            }
        }

        // TODO: Also visit SuperMethodInvocation

    }
}
