package org.eclipse.jdt.core.dom.anaphors;

import static java.util.Arrays.asList;
import static java.util.Optional.ofNullable;

import java.util.List;
import java.util.Optional;

import org.eclipse.jdt.core.dom.ITypeBinding;

public interface PublicAstFunctions {

	static Optional<ITypeBinding> getSuperClass(final ITypeBinding typeBinding) {
		return ofNullable(typeBinding.getSuperclass());
	}

	static List<ITypeBinding> getImplementedInterfaces(final ITypeBinding typeBinding) {
		return asList(typeBinding.getInterfaces());
	}

}
