package org.eclipse.jdt.core.dom.anaphors;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import de.monochromata.anaphors.ast.feature.Feature;
import de.monochromata.anaphors.ast.feature.FeatureContainer;

public class DomFeatureContainer implements FeatureContainer<String> {

	private final Set<Feature<String>> features;

	public DomFeatureContainer() {
		this(new HashSet<>());
	}

	public DomFeatureContainer(final Set<Feature<String>> features) {
		this.features = features;
	}

	@Override
	public Set<Feature<String>> getFeatures() {
		return Collections.unmodifiableSet(features);
	}

	@Override
	public boolean isEmpty() {
		return features.isEmpty();
	}

	@Override
	public boolean containsFeaturesOf(final FeatureContainer<String> other) {
		return features.containsAll(other.getFeatures());
	}

	public static void parseStringLiteral(final String literalValue, final Set<Feature<String>> features) {
		for (final String featureString : literalValue
				.split("[^\\p{javaJavaIdentifierStart}\\p{javaJavaIdentifierPart}]")) {
			if (!featureString.isEmpty()) {
				features.add(createNormalizedFeature(featureString));
			}
		}
	}

	public static DomFeatureContainer parseIdentifier(final String identifier) {
		// Based on
		// http://stackoverflow.com/questions/7593969/regex-to-split-camelcase-or-titlecase-advanced
		// TODO: Maybe use
		// http://commons.apache.org/proper/commons-lang/javadocs/api-3.1/org/apache/commons/lang3/StringUtils.html#splitByCharacterTypeCamelCase%28java.lang.String%29
		// instead
		final Set<Feature<String>> features = new HashSet<>();
		parseIdentifier(identifier, features);
		return new DomFeatureContainer(features);
	}

	public static void parseIdentifier(final String identifier, final Set<Feature<String>> features) {
		for (final String featureString : identifier.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])")) {
			if (!featureString.isEmpty()) {
				features.add(createNormalizedFeature(featureString));
			}
		}
	}

	/**
	 * Creates a new feature from the given feature name, with the first character
	 * turned to lower-case.
	 *
	 * @param featureName the name of the feature
	 * @return a new normalized feature
	 * @throws IllegalArgumentException If the feature name is {@code null} or
	 *                                  empty.
	 */
	public static DomFeature createNormalizedFeature(final String featureName) {
		if (featureName == null) {
			throw new IllegalArgumentException("Null feature name");
		}
		if (featureName.isEmpty()) {
			throw new IllegalArgumentException("Empty feature name");
		}

		String normalizedName = "" + Character.toLowerCase(featureName.charAt(0));
		if (featureName.length() > 1) {
			normalizedName += featureName.substring(1);
		}
		return new DomFeature(normalizedName);
	}
}
