package org.eclipse.jdt.core.dom.anaphors.check;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Statement;

public interface Invocation {

	/**
	 * @return Returns the result of {@link #getConstructorInvocation()},
	 *         {@link #getMethodInvocation()}, or
	 *         {@link #getClassInstanceCreation()}; exactly one of these returns
	 *         a non-null value. Note that a {@link ConstructorInvocation} is a
	 *         {@link Statement} while {@link MethodInvocation} and
	 *         {@link ClassInstanceCreation} are {@link Expression}s.
	 */
	ASTNode getASTNode();

	ConstructorInvocation getConstructorInvocation();

	default boolean isConstructorInvocation() {
		return getConstructorInvocation() != null;
	}

	MethodInvocation getMethodInvocation();

	default boolean isMethodInvocation() {
		return getMethodInvocation() != null;
	}

	ClassInstanceCreation getClassInstanceCreation();

	default boolean isClassInstanceCreation() {
		return getClassInstanceCreation() != null;
	}

}
