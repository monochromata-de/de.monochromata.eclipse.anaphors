package org.eclipse.jdt.core.dom.anaphors.check;

import static org.eclipse.jdt.core.dom.anaphors.check.CallChainSelection.selectCallChain;
import static org.eclipse.jdt.core.dom.anaphors.check.CallChainsCollection.collectCallChains;

import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.anaphors.DomTraversal;

import de.monochromata.anaphors.ast.spi.TransformationsSpi;

public interface CallChainCheck {

	/**
	 * Used to implement
	 * {@link TransformationsSpi#passAlongCallChain(Object, Object)}.
	 *
	 * @return either an empty list if there is no call chain between the
	 *         invocables containing the given expression or a list representing
	 *         a call chain from the invocable containing upstreamExpression to
	 *         the invocable containing downstreamExpression.
	 */
	static List<Invocation> check(final ASTNode upstreamNode, final Expression downstreamExpression,
			final CompilationUnit scope, final int maxDepthOfCallChainAnalysis) {
		final BodyDeclaration upstreamBodyDeclaration = DomTraversal.getBodyDeclaration(upstreamNode);
		final BodyDeclaration downstreamBodyDeclaration = DomTraversal.getBodyDeclaration(downstreamExpression);
		final List<List<Invocation>> candidateCallChains = collectCallChains(upstreamBodyDeclaration, scope,
				maxDepthOfCallChainAnalysis);
		// TODO: Add test scenarios for downstream expressions that are not
		// invocable (i.e. field declarations).
		return selectCallChain(candidateCallChains, upstreamNode, downstreamBodyDeclaration, scope);
	}

}
