package org.eclipse.jdt.core.dom.anaphors;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;

import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.DefaultDirectAnaphora;
import de.monochromata.anaphors.ast.RelatedExpressionPart;

public class PublicDirectAnaphora extends
		DefaultDirectAnaphora<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>
		implements PublicAnaphora {

	public PublicDirectAnaphora(
			final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
			final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart,
			final boolean isUnderspecified) {
		super(relatedExpressionPart, anaphorPart, isUnderspecified);
	}

}
