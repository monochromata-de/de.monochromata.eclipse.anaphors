package org.eclipse.jdt.core.dom.anaphors.transform;

import static java.util.Objects.requireNonNull;

import java.util.List;
import java.util.function.Function;

import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.anaphors.check.Invocation;
import org.eclipse.jdt.internal.corext.refactoring.code.PassAlongCallChainRefactoring;
import org.eclipse.ltk.core.refactoring.Change;

/**
 * Potentially introduces a local variable for an upstream expression and then
 * introduce parameters to make the value of the upstream expression available
 * to the body declaration containing the downstream expression.
 */
public class PassAlongCallChain implements Transformation {

	private final List<Invocation> callChain;
	private final ASTNode upstreamNode;
	private final Expression downstreamExpression;
	private final CompilationUnit scope;
	private final Function<ITypeRoot, CompilationUnit> astParser;

	public PassAlongCallChain(final List<Invocation> callChain, final ASTNode upstreamNode,
			final Expression downstreamExpression, final CompilationUnit scope,
			final Function<ITypeRoot, CompilationUnit> astParser) {
		// Require non-null type root that's later used in perform()
		requireConstructedFromTypeRoot(scope);
		this.callChain = callChain;
		this.upstreamNode = upstreamNode;
		this.downstreamExpression = downstreamExpression;
		this.scope = scope;
		this.astParser = astParser;
	}

	private static void requireConstructedFromTypeRoot(final CompilationUnit scope) {
		requireNonNull(scope.getJavaElement(), "a JavaElement is required as the source of the CompilationUnit");
	}

	@Override
	public CompilationUnit perform() {
		// TODO: Does not work for the current test, because there is a second
		// method invocation that cannot be manipulated ...
		// callChain.stream().forEach(this::perform);
		final Change change = perform(callChain.get(0));
		if (change == null) {
			return null;
		}
		// Obtain a new AST to get the new elements and to re-resolve bindings
		// (happens only AST construction).
		return astParser.apply((ITypeRoot) scope.getJavaElement());
	}

	protected Change perform(final Invocation invocation) {
		// TODO: Add support for the remaining kinds of invocations
		if (invocation.isMethodInvocation()) {
			return perform(invocation.getMethodInvocation());
		} else {
			throw new IllegalArgumentException("Unsupported kind of invocation: " + invocation);
		}
	}

	protected Change perform(final MethodInvocation methodInvocation) {
		final PassAlongCallChainRefactoring refactoring = createRefactoring(methodInvocation);
		return RefactoringBasedTransforming.perform(refactoring);
	}

	protected PassAlongCallChainRefactoring createRefactoring(final MethodInvocation methodInvocation) {
		final PassAlongCallChainRefactoring refactoring = new PassAlongCallChainRefactoring(scope, upstreamNode,
				methodInvocation);
		refactoring.setDelegateUpdating(false);
		refactoring.setDeprecateDelegates(false);
		return refactoring;
	}

}
