package org.eclipse.jdt.core.dom.anaphors;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.eclipse.jdt.core.dom.ASTNode.FIELD_ACCESS;
import static org.eclipse.jdt.core.dom.ASTNode.RETURN_STATEMENT;
import static org.eclipse.jdt.core.dom.ASTNode.SIMPLE_NAME;
import static org.eclipse.jdt.core.dom.Modifier.isPublic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.LambdaExpression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.VariableDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;

import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalTempVariableIntroducingStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalVariableDeclarationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.ParameterDeclarationStrategy;
import de.monochromata.anaphors.ast.spi.AnaphoraResolutionSpi;

/**
 * @since 3.11
 */
public class PublicAnaphoraResolution implements
		AnaphoraResolutionSpi<ASTNode, Expression, Type, IBinding, IVariableBinding, IVariableBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> {

	@Override
	public <RP extends RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AP extends AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>> PublicAnaphora createDirectAnaphora(
			final RP relatedExpressionPart, final AP anaphorPart, final boolean isUnderspecified) {
		return new PublicDirectAnaphora(relatedExpressionPart, anaphorPart, isUnderspecified);
	}

	@Override
	public <RP extends RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AP extends AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>> PublicAnaphora createIndirectAnaphora(
			final RP relatedExpressionPart, final AP anaphorPart, final String underspecifiedRelation,
			final boolean isUnderspecified) {
		return new PublicIndirectAnaphora(relatedExpressionPart, anaphorPart, underspecifiedRelation, isUnderspecified);
	}

	@Override
	public <RP extends RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression>, AP extends AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>> PublicAnaphora createIndirectAnaphora(
			final RP relatedExpressionPart, final AP anaphorPart, final String underspecifiedRelation,
			final Supplier<String> customReferenceDescriptionSupplier, final boolean isUnderspecified) {
		return new PublicIndirectAnaphora(relatedExpressionPart, anaphorPart, underspecifiedRelation,
				customReferenceDescriptionSupplier, isUnderspecified);
	}

	@Override
	public List<IVariableBinding> getAccessibleFields(final ITypeBinding typeBinding, final CompilationUnit scope) {
		return stream(typeBinding.getDeclaredFields())
				.filter(variableBinding -> isPublic(variableBinding.getModifiers())).collect(toList());
	}

	@Override
	public String getFieldName(final IVariableBinding fieldBinding) {
		if (!fieldBinding.isField()) {
			throw new IllegalArgumentException("Not a field binding: " + fieldBinding);
		}
		return fieldBinding.getName();
	}

	@Override
	public boolean identifierStartsUpperCase(final String identifier) {
		return identifier.length() > 0 && Character.isUpperCase(identifier.charAt(0));
	}

	@Override
	public String getFieldDescription(final IVariableBinding fieldBinding) {
		return getFieldName(fieldBinding);
	}

	@Override
	public ITypeBinding resolveType(final IVariableBinding fieldBinding, final CompilationUnit scope) {
		return fieldBinding.getType();
	}

	@Override
	public List<IMethodBinding> getAccessibleGetterMethods(final ITypeBinding typeBinding,
			final CompilationUnit scope) {
		final List<IMethodBinding> getters = new ArrayList<>();
		for (final IMethodBinding mb : typeBinding.getDeclaredMethods()) {
			final String name = mb.getName();
			if (isPublic(mb.getModifiers()) && name.startsWith("get") && name.length() > 3
					&& Character.isUpperCase(name.charAt(3)) && !mb.getReturnType().getQualifiedName().equals("void")
					&& mb.getParameterTypes().length == 0) {
				getters.add(mb);
			}
		}

		// Recurse to super class
		final ITypeBinding superClass = typeBinding.getSuperclass();
		if (superClass != null) {
			getters.addAll(getAccessibleGetterMethods(superClass, scope));
		}
		return getters;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean doesGetterMethodOnlyReturnValueOf(final IMethodBinding methodBinding,
			final IVariableBinding fieldBinding, final CompilationUnit scope) {
		final MethodDeclaration mDecl = (MethodDeclaration) scope.findDeclaringNode(methodBinding);
		if (mDecl == null) {
			System.err.println("Found no declaration for " + methodBinding);
			return false;
		}
		final Block body = mDecl.getBody();
		if (body != null) {
			final List<Statement> statements = body.statements();
			final int numberOfStatements = statements.size();
			if (numberOfStatements == 1) {
				final Statement statement = statements.get(0);
				if (statement.getNodeType() == RETURN_STATEMENT) {
					final ReturnStatement rStmt = (ReturnStatement) statement;
					final Expression rExpr = rStmt.getExpression();
					if (rExpr.getNodeType() == FIELD_ACCESS) {
						final FieldAccess fieldAccess = (FieldAccess) rExpr;
						final IVariableBinding returnedFieldBinding = fieldAccess.resolveFieldBinding();
						return fieldBinding.isEqualTo(returnedFieldBinding);
					} else if (rExpr.getNodeType() == SIMPLE_NAME) {
						final SimpleName simpleName = (SimpleName) rExpr;
						final IBinding binding = simpleName.resolveBinding();
						return binding instanceof IVariableBinding && ((IVariableBinding) binding).isField()
								&& fieldBinding.isEqualTo(binding);
					} // TODO: else { perform flow analysis using
						// org.eclipse.jdt.internal.corext.refactoring.code.flow
				}
			}
		}
		return false;
	}

	@Override
	public String getReferentName(final IMethodBinding methodBinding) {
		String methodName = methodBinding.getName();
		methodName = removeAccessorPrefixes(methodName);
		return methodName;
	}

	// TODO: Move to a more suitable place
	public static String removeAccessorPrefixes(String methodName) {
		// TODO: Handle setters
		// TODO: Handle "to" prefix
		if (methodName.startsWith("get") && methodName.length() > 3) {
			methodName = Character.toString(Character.toLowerCase(methodName.charAt(3))) + methodName.substring(4);
		}
		return methodName;
	}

	@Override
	public String getMethodName(final IMethodBinding methodBinding) {
		return methodBinding.getName();
	}

	@Override
	public String getMethodDescription(final IMethodBinding methodBinding) {
		return methodBinding.getName() + getParametersDescription(methodBinding) + ":"
				+ methodBinding.getReturnType().getName();
	}

	protected String getParametersDescription(final IMethodBinding methodBinding) {
		final String types = Arrays.stream(methodBinding.getParameterTypes()).map(ITypeBinding::getName)
				.collect(Collectors.joining(", "));
		return "(" + types + ")";
	}

	@Override
	public ITypeBinding resolveReturnType(final IMethodBinding methodBinding, final CompilationUnit scope) {
		return methodBinding.getReturnType();
	}

	@Override
	public Expression realizeDA1Re(
			final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
			final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart,
			final Expression replacee, final Optional<String> guessedTempName, final Object... support) {
		final AST ast = (AST) support[0];
		final ASTRewrite astRewrite = (ASTRewrite) support[1];
		return realizeVariableAccess(relatedExpressionPart, guessedTempName, astRewrite);
	}

	@Override
	public Expression realizeIA1Mr(
			final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
			final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart,
			final Expression replacee, final Optional<String> guessedTempName, final Object[] support) {
		// TODO: Remove unused anaphorPart parameter
		final AST ast = (AST) support[0];
		final ASTRewrite astRewrite = (ASTRewrite) support[1];
		return realizeVariableAccess(relatedExpressionPart, guessedTempName, astRewrite);
	}

	protected Expression realizeVariableAccess(
			final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
			final Optional<String> guessedTempName, final ASTRewrite astRewrite) {
		if (relatedExpressionPart.getRelatedExpressionStrategy() instanceof LocalTempVariableIntroducingStrategy) {
			return createExpressionForCIC(relatedExpressionPart, guessedTempName, astRewrite);
		} else if (relatedExpressionPart.getRelatedExpressionStrategy() instanceof ParameterDeclarationStrategy) {
			return createExpressionForPD(relatedExpressionPart, astRewrite);
		} else if (relatedExpressionPart.getRelatedExpressionStrategy() instanceof LocalVariableDeclarationStrategy) {
			return createExpressionForLVD(relatedExpressionPart, astRewrite);
		} else {
			throw new IllegalArgumentException("Unknown kind of related expression strategy: "
					+ relatedExpressionPart.getRelatedExpressionStrategy().getKind());
		}
	}

	@Override
	public boolean couldBeAPreviousRealization(final Expression definiteExpression) {
		return definiteExpression instanceof SimpleName || definiteExpression instanceof FieldAccess
				|| definiteExpression instanceof QualifiedName || definiteExpression instanceof MethodInvocation;
	}

	@Override
	public String getIdFromPreviousRealization(final Expression definiteExpression) {
		if (definiteExpression instanceof SimpleName) {
			return getIdFromPreviousRealization((SimpleName) definiteExpression);
		} else if (definiteExpression instanceof FieldAccess) {
			return getIdFromPreviousRealization((FieldAccess) definiteExpression);
		} else if (definiteExpression instanceof QualifiedName) {
			return getIdFromPreviousRealization((QualifiedName) definiteExpression);
		} else if (definiteExpression instanceof MethodInvocation) {
			return getIdFromPreviousRealization((MethodInvocation) definiteExpression);
		} else {
			throw new IllegalArgumentException("Unknown type of AST node: " + definiteExpression.getClass().getName());
		}
	}

	public String getIdFromPreviousRealization(final SimpleName simpleName) {
		return simpleName.getIdentifier();
	}

	public String getIdFromPreviousRealization(final FieldAccess fieldAccess) {
		return fieldAccess.getName().getIdentifier();
	}

	public String getIdFromPreviousRealization(final QualifiedName qualifiedName) {
		return qualifiedName.getName().getIdentifier();
	}

	public String getIdFromPreviousRealization(final MethodInvocation methodInvocation) {
		return removeAccessorPrefixes(methodInvocation.getName().getIdentifier());
	}

	@Override
	public Expression realizeIA2F(
			final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
			final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart,
			final Expression replacee, final Optional<String> guessedTempName, final Object... support) {
		final AST ast = (AST) support[0];
		final ASTRewrite astRewrite = (ASTRewrite) support[1];

		final Expression qualifier = createExpressionForRelatedExpression(relatedExpressionPart, guessedTempName,
				astRewrite);
		final FieldAccess replacement = ast.newFieldAccess();
		replacement.setExpression(qualifier);
		if (!anaphorPart.getReferent().hasName()) {
			throw new IllegalStateException("Field has no name: " + anaphorPart.getReferent());
		}
		final SimpleName anaphor = ast.newSimpleName(anaphorPart.getReferent().getName());
		replacement.setName(anaphor);
		return replacement;
	}

	@Override
	public Expression realizeIA2Mg(
			final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
			final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart,
			final Expression replacee, final Optional<String> guessedTempName, final Object... support) {
		final AST ast = (AST) support[0];
		final ASTRewrite astRewrite = (ASTRewrite) support[1];

		final Expression qualifier = createExpressionForRelatedExpression(relatedExpressionPart, guessedTempName,
				astRewrite);
		final MethodInvocation replacement = ast.newMethodInvocation();
		replacement.setExpression(qualifier);
		if (!anaphorPart.getReferent().hasMethodName()) {
			throw new IllegalStateException("Method has no name: " + anaphorPart.getReferent());
		}
		final SimpleName methodName = ast.newSimpleName(anaphorPart.getReferent().getMethodName());
		replacement.setName(methodName);
		return replacement;
	}

	@Override
	public String getIA1MrUnderspecifiedRelation(final PublicRelatedExpression potentialRelatedExpression,
			final IMethodBinding methodBinding, final CompilationUnit scope) {
		return methodBinding.getDeclaringClass().getQualifiedName() + "." + getMethodDescription(methodBinding);
	}

	@Override
	public String getIA2FUnderspecifiedRelation(final PublicRelatedExpression relatedExpression,
			final IVariableBinding fieldBinding, final CompilationUnit scope) {
		return relatedExpression.resolveType(scope).getQualifiedName() + "." + fieldBinding.getName();
	}

	@Override
	public String getIA2MgUnderspecifiedRelation(final PublicRelatedExpression relatedExpression,
			final IMethodBinding methodBinding, final CompilationUnit scope) {
		return relatedExpression.resolveType(scope).getQualifiedName() + "." + methodBinding.getName() + "(" + Arrays
				.stream(methodBinding.getParameterTypes()).map(ITypeBinding::getName).collect(Collectors.joining(","))
				+ ")";
	}

	protected Expression createExpressionForRelatedExpression(
			final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
			final Optional<String> guessedTempName, final ASTRewrite astRewrite) {
		return realizeVariableAccess(relatedExpressionPart, guessedTempName, astRewrite);
	}

	protected Expression createExpressionForCIC(
			final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
			final Optional<String> guessedTempName, final ASTRewrite astRewrite) {
		return guessedTempName.map(identifier -> astRewrite.getAST().newSimpleName(identifier)).orElseThrow(
				() -> new IllegalStateException("Required guessed temp name is missing for related expression strategy "
						+ relatedExpressionPart.getRelatedExpressionStrategy().getKind()));
	}

	protected Expression createExpressionForPD(
			final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
			final ASTRewrite astRewrite) {
		return createExpressionForPDorLVD(relatedExpressionPart, astRewrite);
	}

	protected Expression createExpressionForLVD(
			final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
			final ASTRewrite astRewrite) {
		return createExpressionForPDorLVD(relatedExpressionPart, astRewrite);
	}

	private Expression createExpressionForPDorLVD(
			final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart,
			final ASTRewrite astRewrite) {
		final ASTNode copyTarget = relatedExpressionPart.getRelatedExpression().getRelatedExpression();
		return (Expression) astRewrite.createCopyTarget(getNameFromCopyTarget(copyTarget));
	}

	protected Name getNameFromCopyTarget(final ASTNode copyTarget) {
		if (copyTarget instanceof VariableDeclaration) {
			return ((VariableDeclaration) copyTarget).getName();
		} else if (copyTarget instanceof LambdaExpression) {
			return ((VariableDeclaration) ((LambdaExpression) copyTarget).parameters().get(0)).getName();
		} else if (copyTarget instanceof VariableDeclarationExpression) {
			return ((VariableDeclaration) ((VariableDeclarationExpression) copyTarget).fragments().get(0)).getName();
		} else if (copyTarget instanceof VariableDeclarationStatement) {
			return ((VariableDeclaration) ((VariableDeclarationStatement) copyTarget).fragments().get(0)).getName();
		} else {
			throw new RuntimeException(
					"Unsupported related expression: " + copyTarget + " (" + copyTarget.getClass().getName() + ")");
		}
	}
}
