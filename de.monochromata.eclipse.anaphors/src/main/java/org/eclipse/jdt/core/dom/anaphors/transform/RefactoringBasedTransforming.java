package org.eclipse.jdt.core.dom.anaphors.transform;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CheckConditionsOperation;
import org.eclipse.ltk.core.refactoring.CreateChangeOperation;
import org.eclipse.ltk.core.refactoring.IUndoManager;
import org.eclipse.ltk.core.refactoring.PerformChangeOperation;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.core.refactoring.RefactoringCore;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ui.statushandlers.StatusManager;

import de.monochromata.eclipse.anaphors.Activator;

public interface RefactoringBasedTransforming {

	/**
	 * @return the performed change if the refactoring succeeded
	 */
	static Change perform(final Refactoring refactoring) {
		try {
			final Pair<RefactoringStatus, Change> statusAndChange = performRefactoring(refactoring);
			if (statusAndChange.getLeft().isOK()) {
				return statusAndChange.getRight();
			} else {
				// TODO: Should fail the method
				System.err.println("Condition checking status is not ok");
				return null;
			}
		} catch (final CoreException ce) {
			StatusManager.getManager().handle(ce, Activator.PLUGIN_ID);
			return null;
		}
	}

	static Pair<RefactoringStatus, Change> performRefactoring(final Refactoring refactoring) throws CoreException {
		final IUndoManager undoManager = RefactoringCore.getUndoManager();
		undoManager.flush(); // TODO: Need to synchronize?

		final IWorkspace workspace = ResourcesPlugin.getWorkspace();
		final CreateChangeOperation create = new CreateChangeOperation(
				new CheckConditionsOperation(refactoring, CheckConditionsOperation.ALL_CONDITIONS),
				RefactoringStatus.FATAL);
		workspace.run(create, null);
		final Change change = create.getChange();

		if (change == null) {
			// TODO: Should fail the method
			// TODO: Need to investigate
			System.err.println("Change is null!");
			return new ImmutablePair<>(RefactoringStatus.createErrorStatus("Change is null"), change);
		}

		final PerformChangeOperation perform = new PerformChangeOperation(change);
		perform.setUndoManager(undoManager, refactoring.getName());
		workspace.run(perform, null);
		// TODO: Maybe catch CoreException and perform undo change
		final RefactoringStatus status = create.getConditionCheckingStatus();

		/*
		 * if (status.isOK()) { return null; } else { Change undo=
		 * perform.getUndoChange(); // TODO: What would the undo change be
		 * useful for? return status; }
		 */
		return new ImmutablePair<>(status, change);
	}

}
