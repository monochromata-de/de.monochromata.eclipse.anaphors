/**
 * The implementation of the <code>de.monochromata.anaphors.spi</code> service
 * provider interface for the public AST of the eclipse compiler.
 */
package org.eclipse.jdt.core.dom.anaphors;