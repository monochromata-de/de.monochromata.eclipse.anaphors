package org.eclipse.jdt.core.dom.anaphors;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.VariableDeclaration;
import org.eclipse.jdt.core.dom.anaphors.check.Invocation;

/**
 * Search the DOM bottom-up or along node-specific relations.
 *
 * @see DomVisiting
 */
public interface DomTraversal {

	static CompilationUnit getCompilationUnit(final ASTNode node) {
		return getParentOfType(node, CompilationUnit.class);
	}

	static BodyDeclaration getBodyDeclaration(final Invocation invocation, final CompilationUnit scope) {
		if (invocation.isMethodInvocation()) {
			return getBodyDeclaration(invocation.getMethodInvocation(), scope);
		} else if (invocation.isConstructorInvocation()) {
			return getBodyDeclaration(invocation.getConstructorInvocation(), scope);
		} else if (invocation.isClassInstanceCreation()) {
			return getBodyDeclaration(invocation.getClassInstanceCreation(), scope);
		} else {
			throw new IllegalArgumentException("Unknown kind of invocation: " + invocation);
		}
	}

	static BodyDeclaration getBodyDeclaration(final MethodInvocation methodInvocation, final CompilationUnit scope) {
		final IMethodBinding potentiallyGenericDeclaration = methodInvocation.resolveMethodBinding()
				.getMethodDeclaration();
		return getBodyDeclaration(potentiallyGenericDeclaration, scope);
	}

	static BodyDeclaration getBodyDeclaration(final ConstructorInvocation constructorInvocation,
			final CompilationUnit scope) {
		final IMethodBinding potentiallyGenericDeclaration = constructorInvocation.resolveConstructorBinding()
				.getMethodDeclaration();
		return getBodyDeclaration(potentiallyGenericDeclaration, scope);
	}

	static BodyDeclaration getBodyDeclaration(final ClassInstanceCreation classInstanceCreation,
			final CompilationUnit scope) {
		final IMethodBinding potentiallyGenericDeclaration = classInstanceCreation.resolveConstructorBinding();
		return getBodyDeclaration(potentiallyGenericDeclaration, scope);
	}

	static BodyDeclaration getBodyDeclaration(final IMethodBinding methodBinding, final CompilationUnit scope) {
		// scope.findDeclaringNode(methodBinding); cannot be used, because the
		// method binding might originate from a different AST
		// TODO: Add tests for all related methods that cover multiple ASTs
		final ASTNode declaringNode = scope.findDeclaringNode(methodBinding.getKey());
		if (declaringNode == null) {
			// TODO: The bound method is not declared in the current compilation
			// unit
			return null;
		}
		return getBodyDeclaration(declaringNode);
	}

	static BodyDeclaration getBodyDeclaration(final ASTNode node) {
		return getParentOfType(node, BodyDeclaration.class);
	}

	static Block getBlock(final ASTNode node) {
		return getParentOfType(node, Block.class);
	}

	static Statement getStatement(final ASTNode node) {
		return getParentOfType(node, Statement.class);
	}

	static VariableDeclaration getVariableDeclaration(final ASTNode node) {
		// TODO: Do not walk past the statement level
		return getParentOfType(node, VariableDeclaration.class);
	}

	@SuppressWarnings("unchecked")
	static <T> T getParentOfType(final ASTNode node, final Class<T> parentType) {
		final ASTNode parent = node.getParent();
		if (parentType.isInstance(node)) {
			return (T) node;
		} else if (parent == null) {
			return null; // root reached
		} else {
			return getParentOfType(parent, parentType);
		}
	}

	static boolean isAfterOrEqual(final Statement firstStatement, final Statement secondStatement) {
		// TODO: Use firstStatement.getLocationInParent() instead
		final Block firstBlock = getBlock(firstStatement);
		final Block secondBlock = getBlock(secondStatement);
		// TODO: There might be intermediate blocks
		if (firstBlock != secondBlock) {
			throw new RuntimeException("There might be intermediate blocks ...");
		}
		return firstBlock.statements().indexOf(firstStatement) <= firstBlock.statements().indexOf(secondStatement);
	}

}
