package org.eclipse.jdt.core.dom.anaphors.check;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.AnnotationTypeMemberDeclaration;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.EnumConstantDeclaration;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.Initializer;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

/**
 * @see #collectionInvocations(BodyDeclaration)
 */
public interface InvocationsCollection {

	/**
	 * Collects all invocations in the given body declaration.
	 */
	static List<Invocation> collectionInvocations(final BodyDeclaration bodyDeclaration) {
		if (bodyDeclaration instanceof AnnotationTypeMemberDeclaration) {
			// TODO: Add a test and implement
			throw new RuntimeException("Not yet implemented");
		} else if (bodyDeclaration instanceof EnumConstantDeclaration) {
			// TODO: Add a test and implement
			throw new RuntimeException("Not yet implemented");
		} else if (bodyDeclaration instanceof FieldDeclaration) {
			return collectInvocations((FieldDeclaration) bodyDeclaration);
		} else if (bodyDeclaration instanceof Initializer) {
			return collectInvocations((Initializer) bodyDeclaration);
		} else if (bodyDeclaration instanceof MethodDeclaration) {
			return collectInvocations((MethodDeclaration) bodyDeclaration);
		} else {
			throw new IllegalArgumentException(
					"Unknown subtype of BodyDeclaration: " + bodyDeclaration.getClass().getName());
		}
	}

	@SuppressWarnings("unchecked")
	static List<Invocation> collectInvocations(final FieldDeclaration fieldDeclaration) {
		return collectInvocations(fieldDeclaration.fragments());
	}

	static List<Invocation> collectInvocations(final List<VariableDeclarationFragment> fragments) {
		// Note that only one of the fragments will contain the upstream
		// expression
		return fragments.stream().flatMap(fragment -> collectInvocations(fragment)).collect(Collectors.toList());

	}

	static Stream<Invocation> collectInvocations(final VariableDeclarationFragment fragment) {
		// The given fragment may be one out of multiple fragments of a field
		// declaration. Only one of these fragments can contain the given
		// upstream expression.
		// TODO: There might be complex expressions i.e. chains of method
		// invocations / argument construction, lambdas being passed ...
		// TODO: Add tests for these conditions
		// TODO: final Expression expression = fragment.getInitializer()
		throw new RuntimeException("Not yet implemented");
	}

	static List<Invocation> collectInvocations(final Initializer initializer) {
		return collectInvocations(initializer.getBody());
	}

	static List<Invocation> collectInvocations(final MethodDeclaration methodDeclaration) {
		return collectInvocations(methodDeclaration.getBody());
	}

	static List<Invocation> collectInvocations(final Block body) {
		final List<Invocation> invocations = new ArrayList<>();
		// TODO: During traversal there might already be a call chain: if a
		// method invocation is used to create an argument for another method
		// invocation? No! Make clear that that's not a call chain.
		body.accept(new ASTVisitor() {

			@Override
			public boolean visit(final ClassInstanceCreation node) {
				invocations.add(new DefaultInvocation(node));
				return true;
			}

			@Override
			public boolean visit(final ConstructorInvocation node) {
				invocations.add(new DefaultInvocation(node));
				return true;
			}

			@Override
			public boolean visit(final MethodInvocation node) {
				invocations.add(new DefaultInvocation(node));
				return true;
			}

		});
		return invocations;
	}

}
