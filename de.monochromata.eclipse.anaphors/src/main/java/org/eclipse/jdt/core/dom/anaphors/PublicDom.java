package org.eclipse.jdt.core.dom.anaphors;

import java.util.List;
import java.util.Locale;
import java.util.function.Predicate;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jface.text.DocumentEvent;

import de.monochromata.anaphors.ast.feature.FeatureContainer;
import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.anaphors.ast.spi.AnaphorsSpi;
import de.monochromata.anaphors.perspectivation.Perspectivation;
import de.monochromata.eclipse.persp.PerspectivationPosition;

/**
 * @since 3.11
 */
public class PublicDom implements
        AnaphorsSpi<ASTNode, Expression, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition> {

    @Override
    public boolean isSimpleName(final Expression definiteExpression) {
        return definiteExpression instanceof SimpleName;
    }

    @Override
    public String getIdentifierOfSimpleName(final Expression simpleName) {
        return ((SimpleName) simpleName).getIdentifier();
    }

    protected boolean nameEqualsIdentifier(final String name, final String id, final boolean caseSensitive) {
        if (caseSensitive) {
            return name.equals(id);
        } else {
            return name.equalsIgnoreCase(id);
        }
    }

    @Override
    public boolean nameOfReferentEqualsIdentifier(
            final Referent<ITypeBinding, CompilationUnit, String, String> referent, final String id,
            final boolean caseSensitive) {
        if (!referent.hasName()) {
            return false;
        }
        return nameEqualsIdentifier(referent.getName(), id, caseSensitive);
    }

    protected boolean nameMatchesConceptualTypeOfIdentifier(final String name, final String id,
            final boolean caseSensitive) {
        if (caseSensitive) {
            if (name.length() == id.length()) {
                return name.equals(id);
            } else if (name.length() < id.length()) {
                // Adjust to camelCase in id
                return id.endsWith(Character.toUpperCase(name.charAt(0)) + name.substring(1));
            } // else: the referent name cannot match the conceptual type of
              // the identifier
            return false;
        } else {
            return id.toLowerCase().endsWith(name.toLowerCase());
        }
    }

    @Override
    public boolean nameOfReferentMatchesConceptualTypeOfIdentifier(
            final Referent<ITypeBinding, CompilationUnit, String, String> referent, final String id,
            final boolean caseSensitive) {
        if (!referent.hasName()) {
            return false;
        }
        return nameMatchesConceptualTypeOfIdentifier(referent.getName(), id, caseSensitive);
    }

    protected FeatureContainer<String> getFeaturesRemainingInIdentifierBesidesConceptualTypeOfName(String name,
            final String id, final boolean caseSensitive) {
        String idString = id;
        String prefix = null;
        if (caseSensitive) {
            if (name.length() < id.length()) {
                // Adjust to camelCase in the id, if the id contains a feature
                // prefix
                name = Character.toUpperCase(name.charAt(0)) + name.substring(1);
            }
        } else {
            // Ignore case
            idString = id.toLowerCase();
            name = name.toLowerCase();
        }

        final int lastIndex = idString.lastIndexOf(name);
        if (lastIndex == -1) {
            return null;
        }
        if (lastIndex + name.length() != id.length()) {
            return null;
        }
        prefix = id.substring(0, lastIndex);

        return DomFeatureContainer.parseIdentifier(prefix);
    }

    @Override
    public FeatureContainer<String> getFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentName(final String id,
            final Referent<ITypeBinding, CompilationUnit, String, String> referent, final boolean caseSensitive) {
        if (!referent.hasName()) {
            return null;
        }

        return getFeaturesRemainingInIdentifierBesidesConceptualTypeOfName(referent.getName(), id, caseSensitive);
    }

    @Override
    public boolean nameOfIdentifierEqualsSimpleNameOfTypeBinding(final String id, final ITypeBinding type,
            final boolean caseSensitive) {
        return nameEqualsIdentifier(type.getErasure().getName(), id, caseSensitive);
    }

    @Override
    public boolean conceptualTypeInIdentifierEqualsSimpleNameOfType(final String id, final ITypeBinding type,
            final boolean caseSensitive) {
        return nameMatchesConceptualTypeOfIdentifier(type.getErasure().getName(), id, caseSensitive);
    }

    @Override
    public FeatureContainer<String> getFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentType(final String id,
            final ITypeBinding referentType, final boolean caseSensitive) {
        return getFeaturesRemainingInIdentifierBesidesConceptualTypeOfName(referentType.getErasure().getName(), id,
                caseSensitive);
    }

    @Override
    public boolean nameOfIdentifierEqualsFauxHyponymOfSimpleNameOfTypeBinding(String id, final ITypeBinding type,
            final boolean caseSensitive) {
        String name = type.getErasure().getName();
        if (!caseSensitive) {
            // TODO: There might be a more efficient implementation
            // Locale.ROOT is used to perform a Locale-insensitive conversion.
            // See String.toLowerCase().
            name = name.toLowerCase(Locale.ROOT);
            id = id.toLowerCase(Locale.ROOT);
        }
        return name.indexOf(id) >= 1;
    }

    @Override
    public boolean conceptualTypeInIdentifierEqualsFauxHyponymyOfSimpleNameOfType(final String id,
            final ITypeBinding type, final boolean caseSensitive) {
        // TODO: In need of test cases
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public FeatureContainer<String> getFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentTypeWithFauxHyponymy(
            final String id, final ITypeBinding type, final boolean caseSensitive) {
        // TODO: In need of test cases
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public PerspectivationPosition createPositionForExpression(final Expression expression,
            final Predicate<DocumentEvent> condition, final List<Perspectivation> perspectivations) {
        return new PerspectivationPosition(expression.getStartPosition(), expression.getLength(), condition,
                perspectivations);
    }

    @Override
    public int getLength(final String qualifiedIdentifier) {
        return qualifiedIdentifier.length();
    }

}
