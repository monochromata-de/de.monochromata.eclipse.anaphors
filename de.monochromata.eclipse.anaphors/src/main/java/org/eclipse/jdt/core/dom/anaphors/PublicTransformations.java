package org.eclipse.jdt.core.dom.anaphors;

import java.util.List;
import java.util.function.Function;

import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.check.CallChainCheck;
import org.eclipse.jdt.core.dom.anaphors.check.Invocation;
import org.eclipse.jdt.core.dom.anaphors.check.SameInvocableCheck;
import org.eclipse.jdt.core.dom.anaphors.transform.NodeReplacingASTTransformation;
import org.eclipse.jdt.core.dom.anaphors.transform.PassAlongCallChain;
import org.eclipse.jface.text.DocumentEvent;

import de.monochromata.anaphors.ast.spi.TransformationsSpi;
import de.monochromata.anaphors.ast.transform.ASTTransformation;
import de.monochromata.eclipse.persp.PerspectivationPosition;

public class PublicTransformations implements
		TransformationsSpi<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> {

	private final int maxDepthOfCallChainAnalysis;
	private final Function<ITypeRoot, CompilationUnit> astParser;

	/**
	 * @param maxDepthOfCallChainAnalysis the maximum number of method invocations
	 *                                    to analyze with each call chain
	 */
	public PublicTransformations(final int maxDepthOfCallChainAnalysis,
			final Function<ITypeRoot, CompilationUnit> astParser) {
		this.maxDepthOfCallChainAnalysis = maxDepthOfCallChainAnalysis;
		this.astParser = astParser;
	}

	@Override
	public boolean partOfSameInvocable(final ASTNode node, final Expression expression) {
		return SameInvocableCheck.partOfSameInvocable(node, expression);
	}

	@Override
	public ASTTransformation<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> passAlongCallChain(
			final ASTNode upstreamNode, final Expression downstreamExpression, final CompilationUnit scope) {
		final List<Invocation> callChain = CallChainCheck.check(upstreamNode, downstreamExpression, scope,
				maxDepthOfCallChainAnalysis);
		if (callChain.isEmpty()) {
			return null;
		}
		final PassAlongCallChain basicTransformation = new PassAlongCallChain(callChain, upstreamNode,
				downstreamExpression, scope, astParser);
		return new NodeReplacingASTTransformation(scope, basicTransformation);
	}

}
