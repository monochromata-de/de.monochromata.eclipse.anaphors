package org.eclipse.jdt.core.dom.anaphors;

import java.util.Map;

import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;

public interface AstParsing {

	static CompilationUnit parse(final ITypeRoot typeRoot, final boolean resolveBindings) {
		final ASTParser parser = createJava8Parser(typeRoot, resolveBindings);
		final CompilationUnit result = (CompilationUnit) parser.createAST(null);
		return result;
	}

	static ASTParser createJava8Parser(final ITypeRoot typeRoot, final boolean resolveBindings) {
		final ASTParser parser = ASTParser.newParser(AST.JLS8);
		parser.setResolveBindings(resolveBindings);
		parser.setSource(typeRoot);
		setJavaVersion8(parser);
		return parser;
	}

	@SuppressWarnings("rawtypes")
	static void setJavaVersion8(final ASTParser parser) {
		final Map options = JavaCore.getOptions();
		JavaCore.setComplianceOptions(JavaCore.VERSION_1_8, options);
		parser.setCompilerOptions(options);
	}

}
