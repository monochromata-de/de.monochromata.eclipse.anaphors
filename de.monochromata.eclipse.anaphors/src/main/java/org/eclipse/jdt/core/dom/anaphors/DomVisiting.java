package org.eclipse.jdt.core.dom.anaphors;

import static java.lang.Integer.MAX_VALUE;
import static java.util.Objects.requireNonNull;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.BooleanLiteral;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.NullLiteral;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.StringLiteral;

import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForTransformation;

/**
 * Search the DOM top-down via generic parent-child relations.
 *
 * @see DomTraversal
 */
public interface DomVisiting {

	static Stream<ASTNode> collectSimpleNamesAndLiterals(final ASTNode scope) {
		return collectNodes(scope, SimpleName.class, StringLiteral.class, NumberLiteral.class, BooleanLiteral.class,
				CharacterLiteral.class, NullLiteral.class);
	}

	/**
	 * @return a {@link SimpleName} with the given identifier or {@literal null} if
	 *         no such {@link SimpleName} was found in the given
	 *         {@link CompilationUnit}.
	 * @see SimpleName#getIdentifier()
	 */
	static SimpleName findSimpleName(final CompilationUnit scope, final String identifier) {
		return (SimpleName) findNode(scope,
				node -> node instanceof SimpleName && ((SimpleName) node).getIdentifier().equals(identifier));
	}

	/**
	 * @return a {@link StringLiteral} with the given literal value or
	 *         {@literal null} if no such {@link StringLiteral} was found in the
	 *         given {@link CompilationUnit}.
	 * @see StringLiteral#getLiteralValue()
	 */
	static StringLiteral findStringLiteral(final CompilationUnit scope, final String stringLiteralValue) {
		return (StringLiteral) findNode(scope, node -> node instanceof StringLiteral
				&& ((StringLiteral) node).getLiteralValue().equals(stringLiteralValue));
	}

	static ASTNode requireNode(final CompilationUnit scope, final AbstractAnaphoraPositionForTransformation position) {
		return requireNode(scope, position.astNode.getNodeType(), position.getOffset(), position.getLength());
	}

	/**
	 * Returns the first ASTNode in the given scope, whose source is at the given
	 * start position with the given length.
	 * <p>
	 * TODO: Might require some flexibility if there is whitespace after the start
	 * position and before startPosition+length
	 *
	 * @throws NullPointerException if there is no such node.
	 */
	static ASTNode requireNode(final CompilationUnit scope, final int startPosition, final int length) {
		return requireNode(scope, node -> node.getStartPosition() == startPosition && node.getLength() == length,
				() -> "Could not find a node with startPosition=" + startPosition + ", length=" + length
						+ " in the given scope");
	}

	/**
	 * Returns the first ASTNode in the given scope that has the given type and
	 * whose source is at the given start position with the given length.
	 *
	 * @throws NullPointerException if there is no such node.
	 */
	static ASTNode requireNode(final CompilationUnit scope, final int nodeType, final int startPosition,
			final int length) {
		return requireNode(scope,
				node -> node.getNodeType() == nodeType && node.getStartPosition() == startPosition
						&& node.getLength() == length,
				() -> "Could not find a node with nodeType=" + nodeType + ", startPosition=" + startPosition
						+ ", length=" + length + " in the given scope");
	}

	static ASTNode requireNode(final CompilationUnit scope, final Predicate<ASTNode> predicate,
			final Supplier<String> errorMessageSupplier) {
		final ASTNode node = findNode(scope, predicate);
		return requireNonNull(node, errorMessageSupplier.get());
	}

	static ASTNode findNodeAt(final CompilationUnit scope, final int offsetInDocument) {
		// TODO: Can it happen that child nodes have the same source range but
		// should be returned instead of the current node?
		// TODO: Well, it can happen that the child nodes have smaller source
		// ranges ...
		final AtomicInteger smallestMatchingLength = new AtomicInteger(MAX_VALUE);
		final AtomicReference<ASTNode> bestNode = new AtomicReference<>();
		findNode(scope, node -> {
			final int start = node.getStartPosition();
			final int length = node.getLength();
			final int end = start + length;
			final boolean matchesSourceRange = offsetInDocument >= start && offsetInDocument <= end;
			if (matchesSourceRange && length < smallestMatchingLength.get()) {
				// "length < ..." instead of "length <= ..." means that child
				// nodes with the same source range will not supersede their
				// parent node.
				smallestMatchingLength.set(length);
				bestNode.set(node);
			}
			// Try to find closer matching children if the current node matches
			final boolean continueToChildren = !matchesSourceRange;
			return continueToChildren;
		});
		return bestNode.get();
	}

	static Stream<ASTNode> collectNodes(final ASTNode scope, final Class<? extends ASTNode>... nodeTypes) {
		final Stream.Builder<ASTNode> builder = Stream.builder();
		scope.accept(new ASTVisitor() {
			@Override
			@SuppressWarnings("unchecked")
			public boolean preVisit2(final ASTNode node) {
				for (final Class<? extends ASTNode> nodeType : nodeTypes) {
					if (nodeType.isInstance(node)) {
						builder.add(node);
						return true; // continue to children
					}
				}
				return true; // continue to children
			}
		});
		return builder.build();
	}

	static ASTNode findNode(final CompilationUnit scope, final Predicate<ASTNode> predicate) {
		final AtomicReference<ASTNode> matchingNode = new AtomicReference<>();
		scope.accept(new ASTVisitor() {
			@Override
			public boolean preVisit2(final ASTNode node) {
				final boolean matches = predicate.test(node);
				if (matches) {
					matchingNode.set(node);
				}
				final boolean continueToChildren = !matches;
				return continueToChildren;
			}

		});
		return matchingNode.get();
	}

}
