/**
 * Checks to evaluate whether a transformation can be applied
 */
package org.eclipse.jdt.core.dom.anaphors.check;