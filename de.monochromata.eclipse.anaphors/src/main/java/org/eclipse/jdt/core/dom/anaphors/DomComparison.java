package org.eclipse.jdt.core.dom.anaphors;

import java.util.List;
import java.util.Objects;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ChildListPropertyDescriptor;
import org.eclipse.jdt.core.dom.ChildPropertyDescriptor;
import org.eclipse.jdt.core.dom.SimplePropertyDescriptor;
import org.eclipse.jdt.core.dom.StructuralPropertyDescriptor;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;

public interface DomComparison {

    static boolean compare(final ASTNode node1, final ASTNode node2) {
        if (node1 == null && node2 == null) {
            return true;
        } else if ((node1 == null && node2 != null) || (node1 != null && node2 == null)) {
            return false;
        } else if (node1.getClass() != node2.getClass()) {
            return false;
        } else if (node1 instanceof VariableDeclarationFragment && node2 instanceof VariableDeclarationFragment) {
            return compare((VariableDeclarationFragment) node1, (VariableDeclarationFragment) node2);
        } else {
            final List<StructuralPropertyDescriptor> structuralProperties = node1.structuralPropertiesForType();
            return compare(structuralProperties, node1, node2);
        }
    }

    static boolean compare(final VariableDeclarationFragment expression1,
            final VariableDeclarationFragment expression2) {
        // TODO: extend comparison by dimensions, initializer, annotations, ...
        return compare(expression1.getName(), expression2.getName())
                && compare(getTypeFromParent(expression1), getTypeFromParent(expression2));
    }

    static Type getTypeFromParent(final VariableDeclarationFragment fragment) {
        final ASTNode parent = fragment.getParent();
        if (parent instanceof VariableDeclarationExpression) {
            return ((VariableDeclarationExpression) parent).getType();
        } else if (parent instanceof VariableDeclarationStatement) {
            return ((VariableDeclarationStatement) parent).getType();
        } else {
            throw new IllegalArgumentException("Unknown type of parent: " + parent.getClass().getName());
        }
    }

    static boolean compare(final List<StructuralPropertyDescriptor> structuralProperties, final ASTNode node1,
            final ASTNode node2) {
        for (final StructuralPropertyDescriptor descriptor : structuralProperties) {
            if (!compare(descriptor, node1, node2)) {
                return false;
            }
        }
        return true;
    }

    static boolean compare(final StructuralPropertyDescriptor descriptor, final ASTNode node1, final ASTNode node2) {
        if (descriptor instanceof SimplePropertyDescriptor) {
            return compare((SimplePropertyDescriptor) descriptor, node1, node2);
        } else if (descriptor instanceof ChildPropertyDescriptor) {
            return compare((ChildPropertyDescriptor) descriptor, node1, node2);
        } else if (descriptor instanceof ChildListPropertyDescriptor) {
            return compare((ChildListPropertyDescriptor) descriptor, node1, node2);
        }
        throw new IllegalArgumentException("Unknown descriptor type: " + descriptor.getClass().getName());
    }

    static boolean compare(final SimplePropertyDescriptor descriptor, final ASTNode node1, final ASTNode node2) {
        final Object value1 = node1.getStructuralProperty(descriptor);
        final Object value2 = node2.getStructuralProperty(descriptor);
        return Objects.equals(value1, value2);
    }

    static boolean compare(final ChildPropertyDescriptor descriptor, final ASTNode node1, final ASTNode node2) {
        final ASTNode child1 = (ASTNode) node1.getStructuralProperty(descriptor);
        final ASTNode child2 = (ASTNode) node2.getStructuralProperty(descriptor);
        return compare(child1, child2);
    }

    static boolean compare(final ChildListPropertyDescriptor descriptor, final ASTNode node1, final ASTNode node2) {
        final List<ASTNode> children1 = (List<ASTNode>) node1.getStructuralProperty(descriptor);
        final List<ASTNode> children2 = (List<ASTNode>) node2.getStructuralProperty(descriptor);
        return compare(children1, children2);
    }

    static boolean compare(final List<ASTNode> nodes1, final List<ASTNode> nodes2) {
        if (nodes1.size() != nodes2.size()) {
            return false;
        }
        for (int i = 0; i < nodes1.size(); i++) {
            if (!compare(nodes1.get(i), nodes2.get(i))) {
                return false;
            }
        }
        return true;
    }

}
