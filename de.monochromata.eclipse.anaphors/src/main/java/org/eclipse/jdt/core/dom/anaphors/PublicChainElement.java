package org.eclipse.jdt.core.dom.anaphors;

import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;

import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.anaphors.ast.chain.ChainElement;
import de.monochromata.eclipse.anaphors.OriginalAnaphora;

public class PublicChainElement extends
		ChainElement<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, OriginalAnaphora, PublicRelatedExpression, PublicAnaphora, PublicChainElement> {

	public PublicChainElement(final PublicChainElement next,
			final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpression) {
		super(next, relatedExpression);
	}

	public PublicChainElement(final List<PublicChainElement> next,
			final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpression) {
		super(next, relatedExpression);
	}

	public PublicChainElement(final PublicChainElement previous,
			final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphor,
			final OriginalAnaphora anaphorAttachment) {
		super(previous, anaphor, anaphorAttachment);
	}

	public PublicChainElement(final PublicChainElement previous, final List<PublicChainElement> next,
			final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpression,
			final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphor,
			final OriginalAnaphora anaphorAttachment) {
		super(previous, next, relatedExpression, anaphor, anaphorAttachment);
	}

}
