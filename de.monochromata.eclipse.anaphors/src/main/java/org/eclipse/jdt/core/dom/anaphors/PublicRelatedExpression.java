package org.eclipse.jdt.core.dom.anaphors;

import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Supplier;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jface.text.DocumentEvent;

import de.monochromata.anaphors.ast.relatedexp.DefaultRelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.ClassInstanceCreationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalVariableDeclarationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.MethodInvocationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.ParameterDeclarationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.anaphors.ast.spi.RelatedExpressionsSpi;
import de.monochromata.eclipse.anaphors.Activator;
import de.monochromata.eclipse.persp.PerspectivationPosition;

public class PublicRelatedExpression extends
        DefaultRelatedExpression<ASTNode, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression> {

    public PublicRelatedExpression(final boolean isEffectivelyFinal, final Supplier<String> nameSupplier,
            final ASTNode relatedExpression,
            final RelatedExpressionStrategy<ASTNode, Type, IBinding, ITypeBinding, CompilationUnit, String, PublicRelatedExpression> strategy,
            final RelatedExpressionsSpi<ASTNode, ?, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, ?, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression> relatedExpressionsSpi,
            final Function<CompilationUnit, IBinding> nameBindingResolver,
            final Function<CompilationUnit, Integer> lengthOfTypeForTempVarFunction,
            final BiFunction<CompilationUnit, Function<ITypeBinding, Type>, Type> typeForTypeVarFunction,
            final Function<CompilationUnit, ITypeBinding> typeResolver,
            final BiPredicate<? super DefaultRelatedExpression<ASTNode, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression>, PublicRelatedExpression> canBeUsedInsteadOfBiPredicate) {
        super(isEffectivelyFinal, nameSupplier, relatedExpression, strategy, relatedExpressionsSpi, nameBindingResolver,
                lengthOfTypeForTempVarFunction, typeForTypeVarFunction, typeResolver, canBeUsedInsteadOfBiPredicate);
    }

    public static PublicRelatedExpression create(final ASTNode node) {
        final var relatedExpressionsSpi = Activator.getDefault().getRelatedExpressionsSpi();
        if (relatedExpressionsSpi.isParameterDeclaration(node)) {
            return createForParameterDeclaration(node);
        } else if (relatedExpressionsSpi.isLocalVariableDeclaration(node)) {
            return createForLocalVariableDeclaration(node);
        } else if (relatedExpressionsSpi.isClassInstanceCreationExpression(node)) {
            return createForClassInstanceCreation((ClassInstanceCreation) node);
        } else if (relatedExpressionsSpi.isMethodInvocationExpression(node)) {
            return createForMethodInvocation((MethodInvocation) node);
        }
        throw new IllegalArgumentException(node.toString());
    }

    public static PublicRelatedExpression createForParameterDeclaration(final ASTNode node) {
        final var parameterDeclarationStrategy = Activator.getDefault().getParameterDeclarationStrategy();
        final var relatedExpressionsSpi = Activator.getDefault().getRelatedExpressionsSpi();
        return createForParameterDeclaration(node, parameterDeclarationStrategy, relatedExpressionsSpi);
    }

    public static PublicRelatedExpression createForParameterDeclaration(final ASTNode node,
            final ParameterDeclarationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression> parameterDeclarationStrategy,
            final PublicRelatedExpressions relatedExpressionsSpi) {
        return new PublicRelatedExpression(true, () -> relatedExpressionsSpi.getLocalDeclarationIdentifier(node), node,
                parameterDeclarationStrategy, relatedExpressionsSpi,
                scope -> relatedExpressionsSpi.resolveLocalDeclarationBinding(node, scope),
                scope -> relatedExpressionsSpi.getLengthOfLocalDeclarationTypeForTempVar(node, scope),
                (scope, importRewrite) -> relatedExpressionsSpi.getLocalDeclarationTypeForTempVar(node, scope,
                        importRewrite),
                scope -> relatedExpressionsSpi.resolveLocalDeclarationType(node, scope),
                (thisInstance, otherInstance) -> refersToSameAstNode(thisInstance, otherInstance));
    }

    public static PublicRelatedExpression createForLocalVariableDeclaration(final ASTNode node) {
        final var localVariableDeclarationStrategy = Activator.getDefault().getLocalVariableDeclarationStrategy();
        final var relatedExpressionsSpi = Activator.getDefault().getRelatedExpressionsSpi();
        return createForLocalVariableDeclaration(node, localVariableDeclarationStrategy, relatedExpressionsSpi);
    }

    public static PublicRelatedExpression createForLocalVariableDeclaration(final ASTNode node,
            final LocalVariableDeclarationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression> localVariableDeclarationStrategy,
            final PublicRelatedExpressions relatedExpressionsSpi) {
        return new PublicRelatedExpression(true, () -> relatedExpressionsSpi.getLocalDeclarationIdentifier(node), node,
                localVariableDeclarationStrategy, relatedExpressionsSpi,
                newScope -> relatedExpressionsSpi.resolveLocalDeclarationBinding(node, newScope),
                newScope -> relatedExpressionsSpi.getLengthOfLocalDeclarationTypeForTempVar(node, newScope),
                (unused0, unused1) -> {
                    throw new UnsupportedOperationException();
                }, newScope -> relatedExpressionsSpi.resolveLocalDeclarationType(node, newScope),
                (thisInstance, otherInstance) -> refersToSameAstNode(thisInstance, otherInstance)
                        || isOnlyFragment(thisInstance, otherInstance, relatedExpressionsSpi)
                        || isInitializer(thisInstance, otherInstance, relatedExpressionsSpi));
    }

    public static PublicRelatedExpression createForClassInstanceCreation(final ClassInstanceCreation expr) {
        final var classInstanceCreationStrategy = Activator.getDefault().getClassInstanceCreationStrategy();
        final var relatedExpressionsSpi = Activator.getDefault().getRelatedExpressionsSpi();
        return createForClassInstanceCreation(expr, classInstanceCreationStrategy, relatedExpressionsSpi);
    }

    public static PublicRelatedExpression createForClassInstanceCreation(final ClassInstanceCreation expr,
            final ClassInstanceCreationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression, PublicAnaphora> classInstanceCreationStrategy,
            final PublicRelatedExpressions relatedExpressionsSpi) {
        return new PublicRelatedExpression(false, () -> null, expr, classInstanceCreationStrategy,
                relatedExpressionsSpi, scope -> null,
                scope -> relatedExpressionsSpi.getLengthOfClassInstanceCreationTypeForTempVar(expr, scope),
                (unused0, unused1) -> {
                    throw new UnsupportedOperationException();
                }, scope -> relatedExpressionsSpi.resolveClassInstanceCreationType(expr, scope),
                (thisInstance, otherInstance) -> thisInstance.equals(otherInstance)
                        && thisInstance.getRelatedExpression() == otherInstance.getRelatedExpression());
    }

    public static PublicRelatedExpression createForMethodInvocation(final MethodInvocation expr) {
        final var methodInvocationStrategy = Activator.getDefault().getMethodInvocationStrategy();
        final var relatedExpressionsSpi = Activator.getDefault().getRelatedExpressionsSpi();
        return createForMethodInvocation(expr, methodInvocationStrategy, relatedExpressionsSpi);
    }

    public static PublicRelatedExpression createForMethodInvocation(final MethodInvocation expr,
            final MethodInvocationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression, PublicAnaphora> methodInvocationStrategy,
            final PublicRelatedExpressions relatedExpressionsSpi) {
        return new PublicRelatedExpression(false, () -> relatedExpressionsSpi.getMethodInvocationIdentifier(expr), expr,
                methodInvocationStrategy, relatedExpressionsSpi,
                scope -> relatedExpressionsSpi.resolveMethodInvocationBinding(expr, scope),
                scope -> relatedExpressionsSpi.getLengthOfMethodInvocationReturnTypeForTempVar(expr, scope),
                (scope, importRewrite) -> relatedExpressionsSpi.getMethodInvocationReturnTypeForTempVar(expr, scope,
                        importRewrite),
                scope -> relatedExpressionsSpi.resolveMethodInvocationReturnType(expr, scope),
                (thisInstance, otherInstance) -> thisInstance.equals(otherInstance)
                        && thisInstance.getRelatedExpression() == otherInstance.getRelatedExpression());
    }

    private static boolean refersToSameAstNode(final DefaultRelatedExpression thisInstance,
            final PublicRelatedExpression otherInstance) {
        return thisInstance.equals(otherInstance)
                && thisInstance.getRelatedExpression() == otherInstance.getRelatedExpression();
    }

    private static boolean isOnlyFragment(final DefaultRelatedExpression thisInstance,
            final PublicRelatedExpression otherInstance,
            final RelatedExpressionsSpi<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression> relatedExpressionsSpi) {
        return relatedExpressionsSpi.isOnlyFragmentOfMultiVariable((ASTNode) thisInstance.getRelatedExpression(),
                otherInstance.getRelatedExpression());
    }

    private static boolean isInitializer(final DefaultRelatedExpression thisInstance,
            final PublicRelatedExpression otherInstance,
            final RelatedExpressionsSpi<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression> relatedExpressionsSpi) {
        return otherInstance.getStrategy() instanceof ClassInstanceCreationStrategy && relatedExpressionsSpi
                .hasInitializer((ASTNode) thisInstance.getRelatedExpression(), otherInstance.getRelatedExpression());
    }

}
