package de.monochromata;

import java.io.BufferedReader;
import java.io.FileReader;

public class FileReaderTest {

	public static void main(String[] args) throws Exception {
		FileReader fileReader = new FileReader("info.txt");
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		System.err.println(bufferedReader.readLine());
	}
	
}
