package de.monochromata.eclipse.anaphors.cog.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import de.monochromata.eclipse.anaphors.cog.EyeTrackingBasedMemoryAccessTest;

/**
 * Needs to be run as a JUnit Plug-in Test, but not in the UI thread.
 */
@RunWith(Suite.class)
@SuiteClasses({ EyeTrackingBasedMemoryAccessTest.class })
public class AllTests {

}
