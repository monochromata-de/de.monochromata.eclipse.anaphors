package de.monochromata.eclipse.anaphors.cog.swt;

import java.util.function.Consumer;

import org.eclipse.jdt.internal.ui.javaeditor.JavaEditor;
import org.eclipse.ui.IPageListener;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.IWindowListener;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.IWorkbenchWindow;

public class JavaEditorTracker implements IWindowListener, IPageListener, IPartListener2 {

	private final IWorkbench workbench;
	private final Consumer<JavaEditor> editorConsumer;
	private IWorkbenchWindow activeWindow;
	private IWorkbenchPage activePage;

	public JavaEditorTracker(final IWorkbench workbench, final Consumer<JavaEditor> editorConsumer) {
		this.workbench = workbench;
		this.editorConsumer = editorConsumer;
		initialize();
	}

	protected void initialize() {
		workbench.addWindowListener(this);
		final IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
		if (window != null) {
			windowActivated(window);
		}
	}

	// IWindowListener

	@Override
	public void windowActivated(final IWorkbenchWindow window) {
		System.err.println("workbench window activated: " + window);
		window.addPageListener(this);
		activeWindow = window;
		final IWorkbenchPage page = window.getActivePage();
		if (page != null) {
			pageActivated(page);
		}
	}

	@Override
	public void windowDeactivated(final IWorkbenchWindow window) {
		System.err.println("workbench window deactivated: " + window);
		window.removePageListener(this);
		activeWindow = null;
	}

	@Override
	public void windowClosed(final IWorkbenchWindow window) {
	}

	@Override
	public void windowOpened(final IWorkbenchWindow window) {
	}

	// IPageListener

	@Override
	public void pageActivated(final IWorkbenchPage page) {
		System.err.println("workbench page activated: " + page);
		if (activePage != null) {
			activePage.removePartListener(this);
		}
		page.addPartListener(this);
		activePage = page;
		// TODO: Maybe obtain a list of visible parts / editors instead
		final IWorkbenchPartReference part = page.getActivePartReference();
		if (part != null) {
			partActivated(part);
		}
	}

	@Override
	public void pageClosed(final IWorkbenchPage page) {
	}

	@Override
	public void pageOpened(final IWorkbenchPage page) {
	}

	// IPartListener2

	@Override
	public void partActivated(final IWorkbenchPartReference partRef) {
		// TODO: Maybe implement partVisible(...) instead? Then multiple parts
		// might be visible and tracked at a time, e.g. when editors are split
		// vertically / horizontally.
		final IWorkbenchPart part = partRef.getPart(false);
		if (part instanceof JavaEditor) {
			editorConsumer.accept((JavaEditor) part);
		}
	}

	@Override
	public void partBroughtToTop(final IWorkbenchPartReference partRef) {
	}

	@Override
	public void partClosed(final IWorkbenchPartReference partRef) {
	}

	@Override
	public void partDeactivated(final IWorkbenchPartReference partRef) {
		// TODO: Maybe implement partHidde(...) instead? Then multiple parts
		// might be visible and tracked at a time, e.g. when editors are split
		// vertically / horizontally.
		editorConsumer.accept(null);
	}

	@Override
	public void partOpened(final IWorkbenchPartReference partRef) {
	}

	@Override
	public void partHidden(final IWorkbenchPartReference partRef) {
	}

	@Override
	public void partVisible(final IWorkbenchPartReference partRef) {
	}

	@Override
	public void partInputChanged(final IWorkbenchPartReference partRef) {
	}

}
