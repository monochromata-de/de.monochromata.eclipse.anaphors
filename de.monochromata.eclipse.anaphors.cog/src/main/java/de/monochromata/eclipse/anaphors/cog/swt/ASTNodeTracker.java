package de.monochromata.eclipse.anaphors.cog.swt;

import java.util.function.BiConsumer;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.internal.ui.javaeditor.JavaEditor;
import org.eclipse.swt.custom.StyledText;

import de.monochromata.eclipse.eyetracking.control.ControlTracker;
import de.monochromata.event.Tracker;
import de.monochromata.eyetracking.Point;

/**
 * Combines gaze and a control with the {@link ASTNode} at that gaze position,
 * if
 * <ul>
 * <li>the control is a {@link StyledText} that belongs to a {@link JavaEditor}
 * <li>and there is an {@link ASTNode} at that location.
 * </ul>
 * <p>
 * An ASTNodeTracker typically requires a {@link ControlTracker} to function. It
 * will then forward all control events for which an ASTNode could be assigned
 * to the registered listeners.
 * <p>
 * An ASTNodeTracker shall be closed after use to remove its event listener from
 * the underlying {@link ControlTracker}.
 */
public interface ASTNodeTracker extends Tracker<BiConsumer<Point, ASTNode>>, AutoCloseable {

	/**
	 * Removes this {@link ASTNodeTracker} from the underlying
	 * {@link ControlTracker}. Note that the underlying {@link ControlTracker}
	 * will not be stopped by this method.
	 */
	@Override
	void close() throws Exception;

}
