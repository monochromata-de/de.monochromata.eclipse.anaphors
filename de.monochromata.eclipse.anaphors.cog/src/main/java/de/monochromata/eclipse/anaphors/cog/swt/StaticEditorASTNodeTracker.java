package de.monochromata.eclipse.anaphors.cog.swt;

import static de.monochromata.eclipse.anaphors.cog.swt.DocumentOffsetDetermination.getDocumentOffset;
import static java.util.Objects.requireNonNull;
import static org.eclipse.jdt.core.dom.anaphors.DomVisiting.findNodeAt;

import java.lang.reflect.Method;
import java.util.function.BiConsumer;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.internal.ui.javaeditor.JavaEditor;
import org.eclipse.jdt.ui.SharedASTProvider;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.texteditor.AbstractTextEditor;

import de.monochromata.eclipse.eyetracking.control.ControlTracker;
import de.monochromata.event.AbstractTracker;
import de.monochromata.eyetracking.Point;

/**
 * TODO: Create a sub-class that reacts to activation/de-activation of editors
 */
public class StaticEditorASTNodeTracker extends AbstractTracker<BiConsumer<Point, ASTNode>> implements ASTNodeTracker {

	private final BiConsumer<Point, Control> controlListener = this::assignASTNode;
	private final ControlTracker controlTracker;
	private JavaEditor editor;
	private CompilationUnit scope;

	public StaticEditorASTNodeTracker(final ControlTracker controlTracker, final JavaEditor editor) {
		this.controlTracker = requireNonNull(controlTracker);
		initializeControlTracker();
		setEditor(editor);
	}

	protected StaticEditorASTNodeTracker(final ControlTracker controlTracker) {
		this.controlTracker = requireNonNull(controlTracker);
		initializeControlTracker();
	}

	protected void initializeControlTracker() {
		controlTracker.addListener(controlListener);
	}

	protected void setEditor(final JavaEditor editor) {
		if (editor == null) {
			this.editor = null;
		} else if (this.editor == null || this.editor != editor) {
			this.editor = requireNonNull(editor);
			initializeEditor();
		}
	}

	protected void initializeEditor() {
		scope = getCompilationUnit(editor, false);
	}

	protected void assignASTNode(final Point point, final Control control) {
		if (editor == null) {
			return;
		}

		// TODO: Try to use StyledText.class instead of Control.class
		final StyledText editorWidget = (StyledText) editor.getAdapter(Control.class);
		if (control != editorWidget) {
			return;
		}

		final ISourceViewer viewer = getSourceViewer();
		if (viewer == null) {
			return;
		}

		final int documentOffset = getDocumentOffset(editorWidget, viewer, point);
		if (documentOffset == -1) {
			return;
		}

		final ASTNode astNode = findNodeAt(scope, documentOffset);
		notifyListeners(point, astNode);
	}

	private void notifyListeners(final Point point, final ASTNode astNode) {
		notifyListeners(listener -> listener.accept(point, astNode));
	}

	// TODO: Find a better way to obtain the source viewer - probably via
	// getAdapter()?
	protected ISourceViewer getSourceViewer() {
		try {
			final Method method = AbstractTextEditor.class.getDeclaredMethod("getSourceViewer");
			method.setAccessible(true);
			return (ISourceViewer) method.invoke(editor);
		} catch (final Exception e) {
			// TODO: Handle this properly
			e.printStackTrace();
			return null;
		}
	}

	// TODO: Move somewhere else
	// TODO: When to re-parse / how to cache but invalidate the cache on change?
	static CompilationUnit getCompilationUnit(final IEditorPart editor, final boolean resolveBindings) {
		final IEditorInput eip = editor.getEditorInput();
		if (eip instanceof IFileEditorInput) {
			final IFileEditorInput feip = (IFileEditorInput) eip;
			final IJavaElement ije = feip.getAdapter(IJavaElement.class);
			if (ije != null && ije instanceof ICompilationUnit) {
				// TODO: Maybe use a progress monitor?
				return SharedASTProvider.getAST((ITypeRoot) ije, SharedASTProvider.WAIT_ACTIVE_ONLY, null);
			}
		}
		return null;
	}

	@Override
	public void close() throws Exception {
		controlTracker.removeListener(controlListener);
	}

}
