/**
 * Cognitive parts of anphor resolution in Eclipse.
 */
package de.monochromata.eclipse.anaphors.cog;