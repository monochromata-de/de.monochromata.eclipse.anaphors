package de.monochromata.eclipse.anaphors.cog.swt;

import static de.monochromata.eclipse.eyetracking.swt.SWTAccess.supplySync;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewerExtension5;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.swt.custom.StyledText;

import de.monochromata.eyetracking.Point;

public interface DocumentOffsetDetermination {

	/**
	 * @return the offset in the {@link IDocument}, or {@literal -1} for invalid
	 *         locations.
	 */
	static int getDocumentOffset(final StyledText styledText, final ISourceViewer viewer, final Point point) {
		return getTextOffset(styledText, viewer, point.getX(), point.getY());
	}

	/**
	 * @return the offset in the {@link IDocument}, or {@literal -1} for invalid
	 *         locations.
	 */
	static int getTextOffset(final StyledText styledText, final ISourceViewer viewer, final int x, final int y) {
		if (styledText.isDisposed()) {
			return -1;
		}
		return supplySync(styledText, () -> {
			final org.eclipse.swt.graphics.Point controlRelativeLocation = styledText.toControl(x, y);
			// TODO: Add further tests to determine the semantics of
			// getOffsetAtLocation(...)
			final org.eclipse.swt.graphics.Point originOfCharacterBoundingBox = controlRelativeLocation;
			// TODO: What happens for locations that are on whitespace? To what
			// offset are they mapped?

			try {
				final int widgetOffset = styledText.getOffsetAtLocation(originOfCharacterBoundingBox);
				if (viewer instanceof ITextViewerExtension5) {
					final ITextViewerExtension5 extension = (ITextViewerExtension5) viewer;
					return extension.widgetOffset2ModelOffset(widgetOffset);
				} else {
					return widgetOffset + viewer.getVisibleRegion().getOffset();
				}
			} catch (final IllegalArgumentException e) {
				// Thrown by styledText.getOffsetAtLocation() if there is no
				// character at the specified location
				return -1;
			}
		});
	}

}
