package de.monochromata.eclipse.anaphors.cog.swt;

import org.eclipse.ui.IWorkbench;

import de.monochromata.eclipse.eyetracking.control.ControlTracker;

public class DynamicEditorASTNodeTracker extends StaticEditorASTNodeTracker {

	private final JavaEditorTracker editorTracker;

	public DynamicEditorASTNodeTracker(final ControlTracker controlTracker, final IWorkbench workbench) {
		super(controlTracker);
		editorTracker = new JavaEditorTracker(workbench, this::setEditor);
	}

}
