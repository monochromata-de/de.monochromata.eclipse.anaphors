package de.monochromata.eclipse.anaphors.cog;

import static java.lang.System.currentTimeMillis;

import java.util.function.BiConsumer;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;

import de.monochromata.anaphors.cog.activation.LIFOFormula;
import de.monochromata.anaphors.cog.memory.DefaultChunk;
import de.monochromata.anaphors.cog.memory.Memory;
import de.monochromata.anaphors.cog.memory.ModifiableMemory;
import de.monochromata.anaphors.cog.memory.SortedListMemory;
import de.monochromata.eclipse.anaphors.cog.swt.ASTNodeTracker;
import de.monochromata.eclipse.anaphors.cog.swt.DynamicEditorASTNodeTracker;
import de.monochromata.eclipse.eyetracking.control.ControlTracker;
import de.monochromata.eclipse.eyetracking.control.StaticShellControlTracker;
import de.monochromata.eyetracking.EyeTracker;
import de.monochromata.eyetracking.Point;

public class EyeTrackingBasedMemoryAccess implements AutoCloseable {

	private final BiConsumer<Point, ASTNode> astNodeListener = this::addASTNode;
	private final LIFOFormula activationFormula = new LIFOFormula();
	private final ControlTracker controlTracker;
	private final ASTNodeTracker astNodeTracker;
	private final ModifiableMemory<ASTNode> memory;

	public EyeTrackingBasedMemoryAccess(final EyeTracker eyeTracker, final Shell shell, final IWorkbench workbench) {
		// TODO: Replace by a dynamic shell control tracker that discovers new
		// shells
		controlTracker = new StaticShellControlTracker(eyeTracker, shell);
		astNodeTracker = new DynamicEditorASTNodeTracker(controlTracker, workbench);
		memory = new SortedListMemory<>(activationFormula);
		initializeAstNodeTracker();
	}

	private void initializeAstNodeTracker() {
		astNodeTracker.addListener(astNodeListener);
	}

	protected void addASTNode(final Point point, final ASTNode node) {
		System.err.println("Adding to memory: " + node);
		final DefaultChunk<ASTNode> chunk = new DefaultChunk<>(activationFormula, node);
		memory.add(currentTimeMillis(), chunk);
	}

	public Memory<ASTNode> getMemory() {
		return memory;
	}

	@Override
	public void close() throws Exception {
		astNodeTracker.close();
		controlTracker.close();
	}

}
