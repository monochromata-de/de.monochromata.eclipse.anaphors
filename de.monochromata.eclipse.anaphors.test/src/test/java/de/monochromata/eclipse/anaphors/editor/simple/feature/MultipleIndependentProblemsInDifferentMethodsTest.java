package de.monochromata.eclipse.anaphors.editor.simple.feature;

import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent.EventType.ABOUT_TO_REALIZE_ANAPHORS;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class MultipleIndependentProblemsInDifferentMethodsTest extends AbstractSimpleFeatureEditorTest {

	@Override
	protected String getExpectedKindOfResolvedAnaphora() {
		return "CIC-DA1Re-RfRt";
	}

	@Override
	protected void manipulateEditorAndCheckAssertions(final ProblemCollectingChangeListener problemCollector,
			final CognitiveEditor editor) {
		final List<List<Pair<Anaphora, Anaphora>>> resolvedAnaphoras = new ArrayList<>();
		editor.getAnaphorResolutionNotifier().addListener(ABOUT_TO_REALIZE_ANAPHORS,
				event -> resolvedAnaphoras.add(event.addedAnaphoras));
		super.manipulateEditorAndCheckAssertions(problemCollector, editor);
		waitForExpectedPositions(editor, getExpectedPositions());
		assertGivenPositions(editor, getExpectedPositions());
		assertThat(resolvedAnaphoras).hasSize(2);
	}

	@Override
	public String getExpectedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple.feature;");
		builder.addLine("");
		builder.addLine("import javax.swing.JTextArea;");
		builder.addLine("");
		builder.addLine("public class MultipleIndependentProblemsInDifferentMethods {");
		builder.addLine("");
		builder.addLine("    public static void foo() {");
		builder.addLine("        final JTextArea jTextArea = new JTextArea();");
		builder.addLine("        jTextArea.setText(\"line count=\" + jTextArea.getLineCount());");
		builder.addLine("    }");
		builder.addLine("");
		builder.addLine("    public static void bar() {");
		builder.addLine("        final String helloString = new String(\"Hello\");");
		builder.addLine("        new String(\"Bello\");");
		builder.addLine("        System.err.println(helloString);");
		builder.addLine("    }");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	public String getExpectedUnderspecifiedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple.feature;");
		builder.addLine("");
		builder.addLine("import javax.swing.JTextArea;");
		builder.addLine("");
		builder.addLine("public class MultipleIndependentProblemsInDifferentMethods {");
		builder.addLine("");
		builder.addLine("    public static void foo() {");
		builder.addLine("        final JTextArea jTextArea = new JTextArea();");
		builder.addLine("        jTextArea.setText(\"line count=\" + lineCount);");
		builder.addLine("    }");
		builder.addLine("");
		builder.addLine("    public static void bar() {");
		builder.addLine("        new String(\"Hello\");");
		builder.addLine("        new String(\"Bello\");");
		builder.addLine("        System.err.println(helloString);");
		builder.addLine("    }");
		builder.addLine("}");
		return builder.toString();
	}

	protected List<? extends Position> getExpectedPositions() {
		final Anaphora lineCountAnaphora = new Anaphora("LVD", "IA2Mg", "Rn", "lineCount");
		final PositionForRelatedExpression lineCountRelatedExpression = new PositionForRelatedExpression(197, 44);
		final PositionForAnaphor lineCountAnaphor = new PositionForAnaphor(284, 24, lineCountAnaphora);

		final Anaphora helloStringAnaphora = new Anaphora("LVD", "DA1Re", "Rn", "helloString");
		final PositionForRelatedExpression helloStringRelatedExpression = new PositionForRelatedExpression(357, 47);
		final PositionForAnaphor helloStringAnaphor = new PositionForAnaphor(461, 11, helloStringAnaphora);

		return asList(lineCountAnaphor, lineCountRelatedExpression, helloStringRelatedExpression, helloStringAnaphor);
	}

}
