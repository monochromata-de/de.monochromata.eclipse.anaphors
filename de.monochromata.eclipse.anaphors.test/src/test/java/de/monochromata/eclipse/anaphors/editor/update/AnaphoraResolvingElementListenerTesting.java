package de.monochromata.eclipse.anaphors.editor.update;

import static de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPosition.MEMBER_TRACKING_CATEGORY;
import static de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPositionCreation.createMemberTrackingPosition;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapPositionExceptions;
import static java.util.Arrays.asList;
import static org.eclipse.jdt.core.ElementChangedEvent.POST_CHANGE;
import static org.eclipse.jdt.core.ElementChangedEvent.POST_RECONCILE;

import java.lang.reflect.Field;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.ElementChangedEvent;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElementDelta;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.anaphors.JavaModelTesting;
import org.eclipse.jdt.internal.core.JavaElementDelta;
import org.eclipse.jdt.internal.corext.refactoring.change.DocumentTesting;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;

import de.monochromata.eclipse.anaphors.NoopTesting;
import de.monochromata.eclipse.anaphors.editor.AnaphoraDocumentProvider;

public interface AnaphoraResolvingElementListenerTesting extends DocumentTesting, JavaModelTesting, NoopTesting {

    default void addFieldFooToMembersToReResolve(final AnaphoraResolvingElementListener testee,
            final CompilationUnit compilationUnit) {
        final IField foo = getFieldFoo(compilationUnit);
        final MemberTrackingPosition trackingPosition = createMemberTrackingPosition(foo)
                .orElseThrow(NullPointerException::new);
        getTrackingPositionsToReResolve(testee).add(trackingPosition);
    }

    default List<MemberTrackingPosition> getTrackingPositionsToReResolve(
            final AnaphoraResolvingElementListener testee) {
        try {
            final Field memberTrackingPositionsToReResolveField = testee.getClass()
                    .getDeclaredField("memberTrackingPositionsToReResolve");
            memberTrackingPositionsToReResolveField.setAccessible(true);
            return (List<MemberTrackingPosition>) memberTrackingPositionsToReResolveField
                    .get(testee);
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    default List<Position> getTrackingPositions(final IDocument originDocument) {
        try {
            final List<Position> trackingPositions = asList(
                    originDocument.getPositions(MEMBER_TRACKING_CATEGORY));
            return trackingPositions;
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    default void assertAddChildMember(final int eventType,
            final BiConsumer<IDocument, CompilationUnit> documentAsserter,
            final BiConsumer<AnaphoraResolvingElementListener, CompilationUnit> testeeAsserter) throws CoreException {
        assertDelta(eventType, this::noop, this::noop, this::addFieldFooDelta, documentAsserter,
                testeeAsserter);
    }

    default void assertRemoveChildMember(
            final int eventType,
            final BiConsumer<IDocument, CompilationUnit> documentSetup,
            final BiConsumer<AnaphoraResolvingElementListener, CompilationUnit> testeeSetup,
            final BiConsumer<IDocument, CompilationUnit> documentAsserter,
            final BiConsumer<AnaphoraResolvingElementListener, CompilationUnit> testeeAsserter) throws CoreException {
        assertDelta(eventType, documentSetup, testeeSetup, this::removeFieldFooDelta, documentAsserter, testeeAsserter);
    }

    default void addMemberPositionForFooField(final IDocument originDocument, final CompilationUnit compilationUnit) {
        wrapPositionExceptions(() -> {
            final IField foo = getFieldFoo(compilationUnit);
            final MemberTrackingPosition positionToBeRemoved = new MemberTrackingPosition(0, 1, foo);
            originDocument.addPosition(MEMBER_TRACKING_CATEGORY, positionToBeRemoved);
        });
    }

    default Consumer<JavaElementDelta> addFieldFooDelta(final CompilationUnit compilationUnit) {
        return delta -> {
            final IType type = getType(compilationUnit);
            final IField foo = getFieldFoo(compilationUnit);
            final JavaElementDelta typeDelta = delta.changed(type,
                    IJavaElementDelta.F_CHILDREN);
            typeDelta.added(foo);
        };
    }

    default Consumer<JavaElementDelta> removeFieldFooDelta(final CompilationUnit compilationUnit) {
        return delta -> {
            final IType type = getType(compilationUnit);
            final IField foo = getFieldFoo(compilationUnit);
            final JavaElementDelta typeDelta = delta.changed(type,
                    IJavaElementDelta.F_CHILDREN);
            typeDelta.removed(foo);
        };
    }

    default void assertDelta(final int eventType, final BiConsumer<IDocument, CompilationUnit> documentSetup,
            final BiConsumer<AnaphoraResolvingElementListener, CompilationUnit> testeeSetup,
            final Function<CompilationUnit, Consumer<JavaElementDelta>> deltaCustomizer,
            final BiConsumer<IDocument, CompilationUnit> documentAsserter,
            final BiConsumer<AnaphoraResolvingElementListener, CompilationUnit> testeeAsserter) throws CoreException {
        final BiConsumer<AnaphoraResolvingElementListener, CompilationUnit> eventSender = (testee,
                compilationUnit) -> sendEvent(eventType, testee, compilationUnit,
                        deltaCustomizer.apply(compilationUnit));
        assertDelta(documentSetup, testeeSetup, eventSender, documentAsserter, testeeAsserter);
    }

    default void assertDelta(final BiConsumer<IDocument, CompilationUnit> documentSetup,
            final BiConsumer<AnaphoraResolvingElementListener, CompilationUnit> testeeSetup,
            final BiConsumer<AnaphoraResolvingElementListener, CompilationUnit> eventSender,
            final BiConsumer<IDocument, CompilationUnit> documentAsserter,
            final BiConsumer<AnaphoraResolvingElementListener, CompilationUnit> testeeAsserter) throws CoreException {
        assertDocument("class Foo { String foo = \"baroeo\"; int bar() { return -1; } }",
                (originDocument, compilationUnit) -> {
                    final AnaphoraResolvingElementListener testee = createTesteeThatDoesNotCreateJobs(
                            originDocument, (ICompilationUnit) compilationUnit.getJavaElement());

                    documentSetup.accept(originDocument, compilationUnit);
                    testeeSetup.accept(testee, compilationUnit);

                    eventSender.accept(testee, compilationUnit);

                    documentAsserter.accept(originDocument, compilationUnit);
                    testeeAsserter.accept(testee, compilationUnit);
                });
    }

    default IField getFieldFoo(final CompilationUnit compilationUnit) {
        try {
            final IType type = getType(compilationUnit);
            return (IField) type.getChildren()[0];
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    default IType getType(final CompilationUnit compilationUnit) {
        try {
            return (IType) ((ICompilationUnit) compilationUnit.getJavaElement())
                    .getChildren()[0];
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    default AnaphoraResolvingElementListener createTesteeThatDoesNotCreateJobs(
            final IDocument originDocument, final ICompilationUnit editorCompilationUnit) {
        AnaphoraDocumentProvider.configure(originDocument, unused -> {
        });
        return createTesteeThatDoesNotCreateJobs(() -> originDocument, () -> editorCompilationUnit);
    }

    default AnaphoraResolvingElementListener createTesteeThatDoesNotCreateJobs(
            final Supplier<IDocument> originDocumentSupplier,
            final Supplier<ICompilationUnit> editorCompilationUnitSupplier) {
        return new AnaphoraResolvingElementListener(null, null, null, null, null, null, null, null,
                () -> true,
                editorCompilationUnitSupplier,
                originDocumentSupplier,
                this::noop);
    }

    default void sendPostReconcile(final AnaphoraResolvingElementListener listener,
            final CompilationUnit compilationUnit) {
        sendPostReconcile(listener, compilationUnit, delta -> {
        });
    }

    default void sendPostChange(final AnaphoraResolvingElementListener listener,
            final CompilationUnit compilationUnit,
            final Consumer<JavaElementDelta> deltaCustomizer) {
        sendEvent(POST_CHANGE, listener, compilationUnit, deltaCustomizer);
    }

    default void sendPostReconcile(final AnaphoraResolvingElementListener listener,
            final CompilationUnit compilationUnit,
            final Consumer<JavaElementDelta> deltaCustomizer) {
        sendEvent(POST_RECONCILE, listener, compilationUnit, deltaCustomizer);
    }

    default void sendEvent(final int eventType, final AnaphoraResolvingElementListener listener,
            final CompilationUnit compilationUnit, final Consumer<JavaElementDelta> deltaCustomizer) {
        final JavaElementDelta delta = new JavaElementDelta(compilationUnit.getJavaElement());
        delta.changedAST(compilationUnit);
        deltaCustomizer.accept(delta);
        final ElementChangedEvent event = new ElementChangedEvent(delta, eventType);
        listener.elementChanged(event);
    }

}
