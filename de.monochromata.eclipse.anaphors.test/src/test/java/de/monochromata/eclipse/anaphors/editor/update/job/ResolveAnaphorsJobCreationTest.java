package de.monochromata.eclipse.anaphors.editor.update.job;

import static de.monochromata.eclipse.anaphors.editor.update.job.ResolveAnaphorsJobCreation.createAndScheduleJobIfThereAreMembersToBeReResolved;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.update.InvocationRejection;
import de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPosition;
import de.monochromata.eclipse.anaphors.position.KnownProblemPosition;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;

public class ResolveAnaphorsJobCreationTest implements InvocationRejection {

	@Test
	public void doNothingIfThereAreNoMembers() {
		createAndScheduleJobIfThereAreMembersToBeReResolved(emptyList(), this::ensureNotInvoked, this::ensureNotInvoked,
				this::ensureNotInvoked, this::ensureNotInvoked);
	}

	@Test
	public void removeAmbiguityAnnotationsIfThereAreNoPositionsInAMember() {
		final MemberTrackingPosition trackingPosition = new MemberTrackingPosition(0, 10, null);
		final List<MemberTrackingPosition> positionsWhoseAmbiguityAnnotationsAreRemoved = new ArrayList<>();

		createAndScheduleJobIfThereAreMembersToBeReResolved(new ArrayList<>(asList(trackingPosition)),
				unused -> emptyList(), unused -> emptyList(), this::ensureNotInvoked,
				positionsWhoseAmbiguityAnnotationsAreRemoved::add);

		assertThat(positionsWhoseAmbiguityAnnotationsAreRemoved).containsExactly(trackingPosition);
	}

	@Test
	public void aJobIsScheduledIfThereAreAnaphorPositions() {
		final MemberTrackingPosition trackingPosition = new MemberTrackingPosition(0, 10, null);
		final List<MemberTrackingPosition> scheduledPositions = new ArrayList<>();

		final Anaphora anaphora = new Anaphora("a", "b", "c", "d");
		createAndScheduleJobIfThereAreMembersToBeReResolved(new ArrayList<>(asList(trackingPosition)),
				member -> singletonList(new PositionForAnaphor(0, 0, anaphora)), member -> emptyList(),
				(position, anaphorPositions, newProblems) -> scheduledPositions.add(position), this::ensureNotInvoked);

		assertThat(scheduledPositions).containsExactly(trackingPosition);
	}

	@Test
	public void aJobIsScheduledIfThereAreProblems() {
		final MemberTrackingPosition trackingPosition = new MemberTrackingPosition(0, 10, null);
		final List<MemberTrackingPosition> scheduledPositions = new ArrayList<>();

		createAndScheduleJobIfThereAreMembersToBeReResolved(new ArrayList<>(asList(trackingPosition)),
				unused -> emptyList(), unused -> singletonList(new KnownProblemPosition(0, 0, 0, "problemMessage")),
				(position, anaphorPositions, newProblems) -> scheduledPositions.add(position), this::ensureNotInvoked);

		assertThat(scheduledPositions).containsExactly(trackingPosition);
	}

	@Test
	public void aJobIsScheduledIfTheSecondMemberHasAnaphorPositions() {
		final MemberTrackingPosition trackingPositionWithoutPositions = new MemberTrackingPosition(0, 10, null);
		final MemberTrackingPosition trackingPositionWithPositions = new MemberTrackingPosition(10, 10, null);
		final List<MemberTrackingPosition> positionsWhoseAmbiguityAnnotationsAreRemoved = new ArrayList<>();
		final List<MemberTrackingPosition> scheduledPositions = new ArrayList<>();

		final Anaphora anaphora = new Anaphora("a", "b", "c", "d");
		createAndScheduleJobIfThereAreMembersToBeReResolved(
				new ArrayList<>(asList(trackingPositionWithoutPositions, trackingPositionWithPositions)),
				position -> position == trackingPositionWithPositions
						? singletonList(new PositionForAnaphor(0, 0, anaphora))
						: emptyList(),
				unused -> emptyList(), (position, anaphorPositions, newProblems) -> scheduledPositions.add(position),
				positionsWhoseAmbiguityAnnotationsAreRemoved::add);

		assertThat(positionsWhoseAmbiguityAnnotationsAreRemoved).containsExactly(trackingPositionWithoutPositions);
		assertThat(scheduledPositions).containsExactly(trackingPositionWithPositions);
	}

	@Test
	public void aJobIsScheduledIfTheSecondMemberHasAProblem() {
		final MemberTrackingPosition trackingPositionWithoutPositions = new MemberTrackingPosition(0, 10, null);
		final MemberTrackingPosition trackingPositionWithPositions = new MemberTrackingPosition(10, 10, null);
		final List<MemberTrackingPosition> positionsWhoseAmbiguityAnnotationsAreRemoved = new ArrayList<>();
		final List<MemberTrackingPosition> scheduledPositions = new ArrayList<>();

		createAndScheduleJobIfThereAreMembersToBeReResolved(
				new ArrayList<>(asList(trackingPositionWithoutPositions, trackingPositionWithPositions)),
				unused -> emptyList(),
				position -> position == trackingPositionWithPositions
						? singletonList(new KnownProblemPosition(0, 0, 0, "problemMessage"))
						: emptyList(),
				(position, anaphorPositions, newProblems) -> scheduledPositions.add(position),
				positionsWhoseAmbiguityAnnotationsAreRemoved::add);

		assertThat(positionsWhoseAmbiguityAnnotationsAreRemoved).containsExactly(trackingPositionWithoutPositions);
		assertThat(scheduledPositions).containsExactly(trackingPositionWithPositions);
	}

	@Test
	public void onlyScheduleAJobForTheFirstMember() {
		final MemberTrackingPosition trackingPosition0 = new MemberTrackingPosition(0, 10, null);
		final MemberTrackingPosition trackingPosition1 = new MemberTrackingPosition(10, 10, null);
		final List<MemberTrackingPosition> scheduledPositions = new ArrayList<>();

		createAndScheduleJobIfThereAreMembersToBeReResolved(
				new ArrayList<>(asList(trackingPosition0, trackingPosition1)), unused -> emptyList(),
				unused -> singletonList(new KnownProblemPosition(0, 0, 0, "problemMessage")),
				(position, anaphorPositions, newProblems) -> scheduledPositions.add(position), this::ensureNotInvoked);

		assertThat(scheduledPositions).containsExactly(trackingPosition0);
	}
}
