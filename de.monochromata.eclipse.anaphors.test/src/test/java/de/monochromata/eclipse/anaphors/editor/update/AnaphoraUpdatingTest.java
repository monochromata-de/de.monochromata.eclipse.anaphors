package de.monochromata.eclipse.anaphors.editor.update;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.Optional.empty;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jface.text.Position;
import org.junit.Test;

import de.monochromata.anaphors.Anaphora;

public class AnaphoraUpdatingTest implements AnaphoraUpdatingTesting {

	// No anaphora exists

	@Test
	public void doNothingIfNoAnaphoraExistsAndNoneIsFound() {
		AnaphoraUpdating.update(empty(), emptyList(), this::ensureNotInvoked, this::ensureNotInvoked,
				this::ensureNotInvoked, this::ensureNotInvoked, this::ensureNotInvoked);
	}

	@Test
	public void realizeAnaphorIfNoAnaphoraExistsAndOneIsFound() {
		final Pair foundAnaphora = new ImmutablePair(null, null);
		final List realizedAnaphora = new ArrayList<>();

		AnaphoraUpdating.update(empty(), singletonList(foundAnaphora), realizedAnaphora::add, this::ensureNotInvoked,
				this::ensureNotInvoked, this::ensureNotInvoked, this::ensureNotInvoked);

		assertThat(realizedAnaphora).containsExactly(foundAnaphora);
	}

	@Test
	public void addWarningIfNoAnaphoraExistsAndMoreThanOneIsFound() {
		final List foundAnaphoras = asList(null, null);
		final List warnedAnaphoras = new ArrayList<>();

		AnaphoraUpdating.update(empty(), foundAnaphoras, this::ensureNotInvoked, this::ensureNotInvoked,
				this::ensureNotInvoked, warnedAnaphoras::add, this::ensureNotInvoked);

		assertThat(warnedAnaphoras).containsExactly(foundAnaphoras);
	}

	// Anaphora exists

	@Test
	public void undoRealizationIfAnAnaphoraExistsButNoNewOneIsFound() {
		final Anaphora existingAnaphora = new Anaphora("reKind", "arKind", "refKind", "anaphor");
		final List<Pair<Anaphora, Position>> deletedAnaphoras = new ArrayList<>();

		AnaphoraUpdating.update(Optional.of(new ImmutableTriple<>(null, existingAnaphora, null)), emptyList(),
				this::ensureNotInvoked, this::ensureNotInvoked, this::ensureNotInvoked, this::ensureNotInvoked,
				(anaphora, anaphorPosition) -> deletedAnaphoras.add(new ImmutablePair<>(anaphora, anaphorPosition)));

		// A warning will be added by JDT implicitly because the definite expression
		// refers to an undefined variable.
		assertThat(deletedAnaphoras).containsExactly(new ImmutablePair<>(existingAnaphora, null));
	}

	@Test
	public void doNothingAnaphorIfAnAnaphoraExistsAndAnIdenticalOneIsFound() {
		final Triple<Position, Anaphora, Position> existingAnaphora = new ImmutableTriple<>(new Position(0, 10),
				new Anaphora("reKind", "arKind", "refKind", "anaphor"), new Position(10, 10));
		final Pair foundAnaphora = mockAnaphora("anaphor");
		final List retainedAnaphoras = new ArrayList<>();

		AnaphoraUpdating.update(Optional.of(existingAnaphora), singletonList(foundAnaphora), this::ensureNotInvoked,
				retainedAnaphoras::add, this::ensureNotInvoked, this::ensureNotInvoked, this::ensureNotInvoked);

		assertThat(retainedAnaphoras).hasSize(1);
	}

	@Test
	public void replaceTheRealizationIfAnAnaphoraExistsAndADifferingOneIsFound() {
		final Triple<Position, Anaphora, Position> existingAnaphora = new ImmutableTriple<>(new Position(0, 10),
				new Anaphora("reKind", "arKind", "refKind", "anaphor"), new Position(10, 10));
		final Pair foundAnaphora = mockAnaphora("otherAnaphor");
		final List updatedAnaphoras = new ArrayList<>();

		AnaphoraUpdating.update(Optional.of(existingAnaphora), singletonList(foundAnaphora), this::ensureNotInvoked,
				this::ensureNotInvoked,
				(oldAnaphora, newAnaphora) -> updatedAnaphoras.add(new ImmutablePair<>(oldAnaphora, newAnaphora)),
				this::ensureNotInvoked, this::ensureNotInvoked);

		assertThat(updatedAnaphoras).containsExactly(new ImmutablePair<>(existingAnaphora, foundAnaphora));
	}

	@Test
	public void undoRealizationAndAddAWarningIfAnAnaphoraExistsAndMoreThanOneNewOnesAreFound() {
		final Triple<Position, Anaphora, Position> existingAnaphora = new ImmutableTriple<>(new Position(0, 10),
				new Anaphora("reKind", "arKind", "refKind", "anaphor"), new Position(10, 10));
		final List foundAnaphoras = asList(null, null);
		final List deletedAnaphoras = new ArrayList<>();
		final List warnedAnaphoras = new ArrayList<>();

		AnaphoraUpdating.update(Optional.of(existingAnaphora), foundAnaphoras, this::ensureNotInvoked,
				this::ensureNotInvoked, this::ensureNotInvoked, warnedAnaphoras::add,
				(anaphora, anaphorPosition) -> deletedAnaphoras.add(new ImmutablePair<>(anaphora, anaphorPosition)));

		assertThat(deletedAnaphoras)
				.containsExactly(new ImmutablePair<>(existingAnaphora.getMiddle(), new Position(10, 10)));
		assertThat(warnedAnaphoras).containsExactly(foundAnaphoras);
	}

}
