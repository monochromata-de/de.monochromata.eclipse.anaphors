package de.monochromata.eclipse.anaphors.editor;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class CognitiveEditorTest implements CognitiveEditorTesting {

    private final CognitiveEditor editor = new CognitiveEditor();

    @Test
    public void hasAnAnaphoraDocumentProvider() {
        assertThat(editor.getDocumentProvider()).isInstanceOf(AnaphoraDocumentProvider.class);
    }

    @Test
    public void disposeAlsoDisposesErrorTickUpdater() throws Exception {
        assertThat(getErrorTickUpdater(editor)).isNotNull();

        try {
            editor.dispose();
        } catch (final NullPointerException e) {
            // AbstractTextEditor.dispose() might yield a NullPointerException (in Eclipse
            // Oxygen) because the editor has not been initialized.
        }

        assertThat(getErrorTickUpdater(editor)).isNull();
    }

}
