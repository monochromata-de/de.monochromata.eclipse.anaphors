package de.monochromata.eclipse.anaphors.editor.interactive.feature.perspectivation;

import static de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy.underspecifyEverything;
import static de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy.underspecifyNothing;
import static java.util.Arrays.asList;

import java.util.List;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class SwitchUnderspecifyEverythingToUnderspecifyNothingTest
        extends AbstractPerspectivationStrategyChangeTest {

    @Override
    protected PerspectivationStrategy getInitialPerspectivationStrategy() {
        return underspecifyEverything();
    }

    @Override
    protected String getInitialRelatedExpressionImage() {
        return "new A();";
    }

    @Override
    protected String getInitialAnaphorImage() {
        return "value";
    }

    @Override
    protected List<AbstractAnaphoraPositionForRepresentation> getExpectedPositions(final Anaphora anaphora) {
        return asList(new PositionForRelatedExpression(196, 20),
                new PositionForAnaphor(238, 12, anaphora));
    }

    @Override
    protected PerspectivationStrategy getUpdatedPerspectivationStrategy() {
        return underspecifyNothing();
    }

    @Override
    protected String getRelatedExpressionImageAfterChange() {
        return "final A a = new A();";
    }

    @Override
    protected String getAnaphorImageAfterChange() {
        return "a.getValue()";
    }

}
