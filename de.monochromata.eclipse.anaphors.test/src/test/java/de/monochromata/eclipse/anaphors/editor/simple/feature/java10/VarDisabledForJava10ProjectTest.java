package de.monochromata.eclipse.anaphors.editor.simple.feature.java10;

import static de.monochromata.eclipse.anaphors.editor.config.LanguageLevel.JLS_10;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.Activator;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.editor.config.TestConfig;
import de.monochromata.eclipse.anaphors.editor.simple.AbstractSimpleEditorTest;

public class VarDisabledForJava10ProjectTest extends AbstractSimpleEditorTest {

    public VarDisabledForJava10ProjectTest() {
        super(new TestConfig(JLS_10));
    }

    @Override
    protected String getExpectedKindOfResolvedAnaphora() {
        return "CIC-DA1Re-Rt";
    }

    @Override
    protected void manipulateEditorAndCheckAssertions(final ProblemCollectingChangeListener problemCollector,
            final CognitiveEditor editor) {
        final boolean oldUseLocalVariableTypeInference = Activator.getDefault().useLocalVariableTypeInference();
        try {
            Activator.getDefault().enableUseLocalVariableTypeInference(false);
            super.manipulateEditorAndCheckAssertions(problemCollector, editor);
        } finally {
            Activator.getDefault().enableUseLocalVariableTypeInference(oldUseLocalVariableTypeInference);
        }
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple.feature.java10;");
        builder.addLine("");
        builder.addLine("public class VarDisabledForJava10Project {");
        builder.addLine("");
        builder.addLine("   public static void main(String[] args) {");
        builder.addLine("       final A a = new A();");
        builder.addLine("       System.err.println(a);");
        builder.addLine("   }");
        builder.addLine("");
        builder.addLine("   private static class A {");
        builder.addLine("");
        builder.addLine("   }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple.feature.java10;");
        builder.addLine("");
        builder.addLine("public class VarDisabledForJava10Project {");
        builder.addLine("");
        builder.addLine("   public static void main(String[] args) {");
        builder.addLine("       new A();");
        builder.addLine("       System.err.println(a);");
        builder.addLine("   }");
        builder.addLine("");
        builder.addLine("   private static class A {");
        builder.addLine("");
        builder.addLine("   }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected Pair<Anaphora, Anaphora> getExpectedResolvedAndRealizedAnaphora() {
        return new ImmutablePair<>(new Anaphora("CIC", "DA1Re", "Rt", "a"),
                new Anaphora("LVD", "DA1Re", "Rn", "a"));
    }
}
