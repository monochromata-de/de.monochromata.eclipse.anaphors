package de.monochromata.eclipse.anaphors.editor.interactive.feature;

import static java.util.Arrays.asList;

import java.util.List;

import org.eclipse.jface.text.Position;
import org.eclipse.ui.IEditorPart;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class MemberUpdateWithoutSaveDoesNotYieldExceptionTest
        extends MemberUpdateDoesNotYieldExceptionTest {

    @Override
    protected List<? extends Position> getExpectedIntermediatePositions() {
        final Anaphora anaphora = new Anaphora("LVD", "DA1Re", "Rn", "button");
        final PositionForRelatedExpression positionForRelatedExpression = new PositionForRelatedExpression(247, 44);
        final PositionForAnaphor positionForAnaphor = new PositionForAnaphor(313, 6, anaphora);
        return asList(positionForRelatedExpression, positionForAnaphor);
    }

    @Override
    protected void editFile(final IEditorPart editor) {
        deleteTextOnLine(6);
        getBot().activeEditor().toTextEditor().insertText(6, 0, "public static void main() {\n");
        // Do not save so deltas are sent with POST_RECONCILE instead of POST_CHANGE
    }

    @Override
    protected List<? extends Position> getExpectedPositions() {
        final Anaphora anaphora = new Anaphora("LVD", "DA1Re", "Rn", "button");
        final PositionForRelatedExpression positionForRelatedExpression = new PositionForRelatedExpression(234, 44);
        final PositionForAnaphor positionForAnaphor = new PositionForAnaphor(300, 6, anaphora);
        return asList(positionForRelatedExpression, positionForAnaphor);
    }

}
