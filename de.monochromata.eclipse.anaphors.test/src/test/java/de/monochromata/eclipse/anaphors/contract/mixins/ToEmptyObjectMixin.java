package de.monochromata.eclipse.anaphors.contract.mixins;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = ToEmptyObjectSerializer.class)
public class ToEmptyObjectMixin {

}
