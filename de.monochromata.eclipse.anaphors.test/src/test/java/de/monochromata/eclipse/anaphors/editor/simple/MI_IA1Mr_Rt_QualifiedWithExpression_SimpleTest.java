package de.monochromata.eclipse.anaphors.editor.simple;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;

public class MI_IA1Mr_Rt_QualifiedWithExpression_SimpleTest extends AbstractSimpleEditorTest {

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
        builder.addLine("");
        builder.addLine("public class MI_IA1Mr_Rt_QualifiedWithExpression_Simple {");
        builder.addLine("");
        builder.addLine("    public static void main() {");
        builder.addLine("        final Foo foo = create()");
        builder.addLine("            .createFoo();");
        builder.addLine("        System.err.println(foo);");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static ObjectSession create() {");
        builder.addLine("        return new ObjectSession();");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static class ObjectSession {");
        builder.addLine("        public Foo createFoo() {");
        builder.addLine("            return new Foo();");
        builder.addLine("        }");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static class Foo {");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
        builder.addLine("");
        builder.addLine("public class MI_IA1Mr_Rt_QualifiedWithExpression_Simple {");
        builder.addLine("");
        builder.addLine("    public static void main() {");
        builder.addLine("        create()");
        builder.addLine("            .createFoo();");
        builder.addLine("        System.err.println(foo);");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static ObjectSession create() {");
        builder.addLine("        return new ObjectSession();");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static class ObjectSession {");
        builder.addLine("        public Foo createFoo() {");
        builder.addLine("            return new Foo();");
        builder.addLine("        }");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static class Foo {");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected Pair<Anaphora, Anaphora> getExpectedResolvedAndRealizedAnaphora() {
        return new ImmutablePair<>(new Anaphora("MI", "IA1Mr", "Rt", "foo"),
                new Anaphora("LVD", "DA1Re", "Rn", "foo"));
    }

}
