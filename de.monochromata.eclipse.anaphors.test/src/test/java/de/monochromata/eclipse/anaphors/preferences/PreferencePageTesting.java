package de.monochromata.eclipse.anaphors.preferences;

import static org.assertj.core.api.Assertions.assertThat;

import org.eclipse.swtbot.swt.finder.SWTBot;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotCheckBox;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotCombo;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTreeItem;

import de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy;
import de.monochromata.eclipse.anaphors.Activator;

public interface PreferencePageTesting {

    SWTBot swtBot();

    default void openAnaphorsPreferences() {
        final SWTBotTreeItem anaphorsNode = getAnaphorsNode();
        anaphorsNode.click();
    }

    default SWTBotTreeItem getAnaphorsNode() {
        try {
            final SWTBotTreeItem parentNode = swtBot().tree().getTreeItem("Cognitive Editor");
            parentNode.expand();
            return parentNode.getNode("Anaphors");
        } catch (final WidgetNotFoundException e) {
            // The "Cognitive Editor" node is only available if the coged4j plug-in is
            // installed.
            assertThat(e).hasMessage("Timed out waiting for tree item Cognitive Editor");
            // If "Cognitive Editor" is not available, "Anaphors" is a top-level item.
            return swtBot().tree().getTreeItem("Anaphors");
        }
    }

    default void assertDeclareFinalEditor() {
        assertCheckBox("Declare generated local variables as final");
    }

    default void assertPerspectivationStrategy() {
        assertThat(swtBot().label("How to present code")).isNotNull();
        final SWTBotCombo perspectivationComboBox = swtBot().comboBox();
        assertThat(perspectivationComboBox).isNotNull();
        assertThat(perspectivationComboBox.items())
                .containsExactly("Show full code",
                        "Shorten related expressions",
                        "Shorten related expressions and anaphors");
        assertThat(perspectivationComboBox.getText()).isEqualTo("Shorten related expressions");
    }

    default void assertResolutionOccurrences() {
        assertCheckBox("Mark related expressions and anaphors after resolution");
    }

    default void assertLocalVariableTypeInference() {
        assertCheckBox("Use local variable type inference for Java >= 10 projects");
    }

    default void assertCurrentPerspectivationStrategy(final PerspectivationStrategy expectedPerspectivatioStrategy) {
        assertThat(Activator.getDefault().getPerspectivationStrategy().getPerspectivationConfiguration())
                .isEqualTo(expectedPerspectivatioStrategy.getPerspectivationConfiguration());
    }

    default void applyChanges() {
        swtBot().button("Apply").click();
    }

    default void assertCheckBox(final String mnemonicText) {
        final SWTBotCheckBox checkBox = swtBot().checkBox(mnemonicText);
        assertThat(checkBox).isNotNull();
        assertThat(checkBox.isChecked()).isTrue();
    }

}
