package de.monochromata.eclipse.anaphors.position;

import static org.assertj.core.api.Assertions.fail;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class AnaphoraPositionCreationTest implements AnaphoraPositionCreationTesting {

	// Related to AbstractAnaphoraPositionForRepresentation

	@Test
	public void correctPositionsAreAddedForRepresentation() throws Exception {
		assertCorrectPositionsForRepresentation("\"relatedExpression\" \"anaphor\"", 1, "relatedExpression", 21,
				"anaphor", unused -> {
				}, 1, 17, 21, 7);
	}

	@Test
	public void positionsForRepresentationAreCorrectlyUpdatedForChangesBeforeTheNodes() throws Exception {
		assertCorrectPositionsForRepresentation("String foo = [ \"relatedExpression\", \"anaphor\" ];", 16,
				"relatedExpression", 37, "anaphor", document -> document.replace(7, 3, "fooAndBar"), 22, 17, 43, 7);
	}

	@Test
	public void positionsForRepresentationAreCorrectlyUpdatedForChangesOnTheNodes() throws Exception {
		assertCorrectPositionsForRepresentation("String foo = [ \"relatedExpression\", \"anaphor\" ];", 16,
				"relatedExpression", 37, "anaphor", document -> document.replace(23, 0, "Party"), 16, 22, 42, 7);
	}

	@Test
	public void positionsForRepresentationAreNotUpdatedForChangesAfterTheNodes() throws Exception {
		assertCorrectPositionsForRepresentation("String foo = [ \"relatedExpression\", \"anaphor\" ];", 16,
				"relatedExpression", 37, "anaphor", document -> document.replace(48, 0, "The end"), 16, 17, 37, 7);
	}

	// Related to AbstractAnaphoraPositionOnASTNode

	@Test
	public void correctPositionsAreAddedForAstNodes() throws Exception {
		assertCorrectPositionsForAstNodes("\"relatedExpression\" \"anaphor\"", 0, "relatedExpression", 20, "anaphor",
				unused -> {
				}, 0, 19, 20, 9);
	}

	@Test
	public void positionsForAstNodesAreCorrectlyUpdatedForChangesBeforeTheNodes() throws Exception {
		assertCorrectPositionsForAstNodes("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
				"relatedExpression", 36, "anaphor", document -> document.replace(7, 3, "fooAndBar"), 21, 19, 42, 9);
	}

	@Test
	public void positionsForAstNodesAreCorrectlyUpdatedForChangesOnTheNodes() throws Exception {
		assertCorrectPositionsForAstNodes("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
				"relatedExpression", 36, "anaphor", document -> document.replace(23, 0, "Party"), 15, 24, 41, 9);
	}

	@Test
	public void positionsForAstNodesAreCorrectlyLinked() throws Exception {
		assertCorrectPositionsForAstNodes("\"relatedExpression\" \"anaphor\"", 0, "relatedExpression", 20, "anaphor",
				unused -> {
				}, (unused, positions) -> assertPositionsAreLinked((List) positions));
	}

	// Related to AbstractAnaphoraPositionAroundASTNode

	@Ignore
	@Test
	public void correctPositionsAreAddedAroundAstNodes() throws Exception {
		assertCorrectPositionsAroundAstNodes("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
				"relatedExpression", 36, "anaphor", unusued -> {
				}, 15, 34, 36, 45);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyUpdatedForChangesOnTheNodes() throws Exception {
		assertCorrectPositionsAroundAstNodes("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
				"relatedExpression", 36, "anaphor", document -> {
					document.replace(23, 0, "Foo");
					document.replace(39, 0, "Bar");
				}, 15, 37, 39, 51);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyUpdatedForChangesBeforeTheStartOfTheNodes() throws Exception {
		assertCorrectPositionsAroundAstNodes("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
				"relatedExpression", 36, "anaphor", document -> document.replace(7, 3, "fooAndBar"), 21, 40, 42, 51);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyUpdatedForRemovalAtTheStartOfTheNodes() throws Exception {
		assertCorrectPositionsAroundAstNodes("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
				"relatedExpression", 36, "anaphor", document -> document.replace(15, 1, ""), 15, 33, 35, 44);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyUpdatedForCrossingRemovalAtTheStartOfTheNodes() throws Exception {
		assertCorrectPositionsAroundAstNodes("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
				"relatedExpression", 36, "anaphor", document -> document.replace(14, 3, ""), 14, 31, 33, 42);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyUpdatedForInsertionAtTheStartOfTheNodes() throws Exception {
		assertCorrectPositionsAroundAstNodes("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
				"relatedExpression", 36, "anaphor", document -> document.replace(15, 1, "\"a\", \""), 15, 39, 41, 50);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyUpdatedForCrossingInsertionAtTheStartOfTheNodes() throws Exception {
		assertCorrectPositionsAroundAstNodes("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
				"relatedExpression", 36, "anaphor", document -> document.replace(14, 6, "\"a\", \""), 14, 34, 36, 45);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyUpdatedForRemovalAtTheEndOfTheNodes() throws Exception {
		assertCorrectPositionsAroundAstNodes("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
				"relatedExpression", 36, "anaphor", document -> document.replace(34, 1, ""), 15, 34, 35, 44);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyUpdatedForCrossingRemovalAtTheEndOfTheNodes() throws Exception {
		assertCorrectPositionsAroundAstNodes("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
				"relatedExpression", 36, "anaphor", document -> document.replace(33, 3, ""), 15, 33, 33, 42);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyUpdatedForInsertionAtTheEndOfTheNodes() throws Exception {
		assertCorrectPositionsAroundAstNodes("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
				"relatedExpression", 36, "anaphor", document -> document.replace(34, 1, " ,"), 15, 34, 37, 46);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyUpdatedForCrossingInsertionAtTheEndOfTheNodes() throws Exception {
		assertCorrectPositionsAroundAstNodes("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
				"relatedExpression", 36, "anaphor", document -> document.replace(33, 3, "foo"), 15, 36, 36, 45);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreNotUpdatedForChangesAfterTheEndOfTheNodes() throws Exception {
		assertCorrectPositionsAroundAstNodes("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
				"relatedExpression", 36, "anaphor", document -> document.replace(47, 1, "something"), 15, 34, 36, 45);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyUpdatedForRemovalAndNodeStartAt0() throws Exception {
		assertCorrectPositionsAroundAstNodes("\"relatedExpression\", \"anaphor\" ];", 0, "relatedExpression", 21,
				"anaphor", document -> document.replace(0, 1, ""), 0, 18, 20, 29);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyUpdatedForCrossingRemovalAndNodeStartAt0() throws Exception {
		assertCorrectPositionsAroundAstNodes("\"relatedExpression\", \"anaphor\" ];", 0, "relatedExpression", 21,
				"anaphor", document -> document.replace(0, 2, ""), 0, 17, 19, 28);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyUpdatedForInsertionAndNodeStartAt0() throws Exception {
		assertCorrectPositionsAroundAstNodes("\"relatedExpression\", \"anaphor\" ];", 0, "relatedExpression", 21,
				"anaphor", document -> document.replace(0, 0, "Add"), 0, 22, 24, 33);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyUpdatedForCrossingInsertionAndNodeStartAt0() throws Exception {
		assertCorrectPositionsAroundAstNodes("\"relatedExpression\", \"anaphor\" ];", 0, "relatedExpression", 21,
				"anaphor", document -> document.replace(0, 3, "Add"), 0, 19, 21, 30);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyUpdatedForRemovalAndNodeEndAtEndOfDocument() throws Exception {
		assertCorrectPositionsAroundAstNodes("String foo = [ \"relatedExpression\", \"anaphor\"", 15,
				"relatedExpression", 36, "anaphor", document -> document.replace(44, 1, ""), 15, 34, 36, 44);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyUpdatedForCrossingRemovalAndNodeEndAtEndOfDocument()
			throws Exception {
		assertCorrectPositionsAroundAstNodes("String foo = [ \"relatedExpression\", \"anaphor\"", 15,
				"relatedExpression", 36, "anaphor", document -> document.replace(43, 2, ""), 15, 34, 36, 43);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyUpdatedForInsertionAndNodeEndAtEndOfDocument() throws Exception {
		assertCorrectPositionsAroundAstNodes("String foo = [ \"relatedExpression\", \"anaphor\"", 15,
				"relatedExpression", 36, "anaphor", document -> document.replace(45, 0, "Foo"), 15, 34, 36, 45);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyUpdatedForCrossingInsertionAndNodeEndAtEndOfDocument()
			throws Exception {
		assertCorrectPositionsAroundAstNodes("String foo = [ \"relatedExpression\", \"anaphor\"", 15,
				"relatedExpression", 36, "anaphor", document -> document.replace(43, 0, "Foo"), 15, 34, 36, 48);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyUpdatedForTwoStepReplacement() throws Exception {
		// Refactorings may introduce multiple modifications: a removal and then
		// a separate insertion
		assertCorrectPositionsAroundAstNodes("String foo = [ \"relatedExpression\", \"anaphor\"", 15,
				"relatedExpression", 36, "anaphor", document -> {
					document.replace(15, 19, "");
					document.replace(15, 0, "\"RELATEDEXPRESSIONSFOOBAR\"");
				}, 15, 41, 43, 52);
	}

	@Ignore
	@Test
	public void positionsAroundAstNodesAreCorrectlyLinked() {
		fail("Not yet implemented");
	}
}
