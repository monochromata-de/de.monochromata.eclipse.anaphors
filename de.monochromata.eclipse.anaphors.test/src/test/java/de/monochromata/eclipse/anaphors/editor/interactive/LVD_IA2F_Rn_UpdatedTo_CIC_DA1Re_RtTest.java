package de.monochromata.eclipse.anaphors.editor.interactive;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class LVD_IA2F_Rn_UpdatedTo_CIC_DA1Re_RtTest extends AbstractInteractiveDoubleAssertionEditorTest {

	@Override
	protected String getExpectedKindOfResolvedAnaphora() {
		return "LVD-IA2F-Rn";
	}

	@Override
	protected String getExpectedIntermediateRefactoredSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("public class LVD_IA2F_Rn_UpdatedTo_CIC_DA1Re_Rt {");
		builder.addLine("");
		builder.addLine("   public static void foo() {");
		builder.addLine("       A foo = new A();");
		builder.addLine("       System.err.println(foo.integer);");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("   private static class A {");
		builder.addLine("       public Integer integer;");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected String getExpectedIntermediateUnderspecifiedRefactoredSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("public class LVD_IA2F_Rn_UpdatedTo_CIC_DA1Re_Rt {");
		builder.addLine("");
		builder.addLine("   public static void foo() {");
		builder.addLine("       A foo = new A();");
		builder.addLine("       System.err.println(integer);");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("   private static class A {");
		builder.addLine("       public Integer integer;");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected List<Pair<? extends Annotation, ? extends Position>> getExpectedIntermediateAnnotations() {
		return emptyList();
	}

	@Override
	protected List<? extends Position> getExpectedIntermediatePositions() {
		final Anaphora anaphora = new Anaphora("LVD", "IA2F", "Rn", "integer");
		return asList(new PositionForAnaphor(193, 11, anaphora), new PositionForRelatedExpression(149, 16));
	}

	@Override
	protected void editFile(final IEditorPart editor) {
		getBot().activeEditor().toTextEditor().insertText(6, 2, "new Integer(0);\n");
		getBot().activeEditor().save();
	}

	@Override
	public String getExpectedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("public class LVD_IA2F_Rn_UpdatedTo_CIC_DA1Re_Rt {");
		builder.addLine("");
		builder.addLine("	public static void foo() {");
		builder.addLine("       A foo = new A();");
		builder.addLine("		final Integer integer = new Integer(0);");
		builder.addLine("		System.err.println(integer);");
		builder.addLine("	}");
		builder.addLine("");
		builder.addLine("   private static class A {");
		builder.addLine("       public Integer integer;");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	public String getExpectedUnderspecifiedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("public class LVD_IA2F_Rn_UpdatedTo_CIC_DA1Re_Rt {");
		builder.addLine("");
		builder.addLine("	public static void foo() {");
		builder.addLine("       A foo = new A();");
		builder.addLine("		new Integer(0);");
		builder.addLine("		System.err.println(integer);");
		builder.addLine("	}");
		builder.addLine("");
		builder.addLine("   private static class A {");
		builder.addLine("       public Integer integer;");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected int getExpectedNumberOfAnaphoraRelationsAfterEdit() {
		return 1;
	}

	@Override
	protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
		return emptyList();
	}

	@Override
	protected List<? extends Position> getExpectedPositions() {
		final Anaphora anaphora = new Anaphora("LVD", "DA1Re", "Rn", "integer");
		return asList(new PositionForAnaphor(236, 7, anaphora), new PositionForRelatedExpression(171, 39));
	}

}
