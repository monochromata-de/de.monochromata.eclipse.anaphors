package de.monochromata.eclipse.anaphors.preferences;

import static de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy.underspecifyEverything;
import static de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy.underspecifyRelatedExpressions;
import static org.assertj.core.api.Assertions.assertThat;

import org.eclipse.swtbot.swt.finder.SWTBot;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotCombo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PreferencePageTest implements PreferencePageTesting {

    private final SWTBot swtBot = new SWTBot();

    @Override
    public SWTBot swtBot() {
        return swtBot;
    }

    @Before
    public void openPreferences() {
        swtBot.activeShell().menu().menu("Window", "Preferences").click();
        openAnaphorsPreferences();
    }

    @After
    public void closePreferences() {
        swtBot.button("Cancel").click();
    }

    @Test
    public void preferencePageIsShown() {
        assertThat(swtBot.label("Configuration of anaphors in Eclipse")).isNotNull();
        assertDeclareFinalEditor();
        assertPerspectivationStrategy();
        assertResolutionOccurrences();
        assertLocalVariableTypeInference();
    }

    @Test
    public void changeOfPerspectivationStrategyAffectsActivator() {
        final String defaultSelection = "Shorten related expressions";
        final SWTBotCombo perspectivationComboBox = swtBot().comboBox();
        try {
            assertCurrentPerspectivationStrategy(underspecifyRelatedExpressions());
            assertThat(perspectivationComboBox.getText()).isEqualTo(defaultSelection);

            perspectivationComboBox.setSelection("Shorten related expressions and anaphors");
            applyChanges();

            assertCurrentPerspectivationStrategy(underspecifyEverything());
        } finally {
            perspectivationComboBox.setSelection(defaultSelection);
            applyChanges();
        }
    }

}
