package de.monochromata.eclipse.anaphors.editor.simple;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;

/**
 * Tests an anaphor of kind DA1Re that is related to a local variable
 * declaration and refers to its referent based on the (case-insensitive)
 * recurrence of the variable name.
 */
public class LVD_DA1Re_Rn_SimpleTest extends AbstractSimpleEditorTest {

	@Override
	public String getExpectedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
		builder.addLine("");
		builder.addLine("public class LVD_DA1Re_Rn_Simple {");
		builder.addLine("");
		builder.addLine("	public static void main() {");
		builder.addLine("		final String[] args = new String[0];");
		builder.addLine("		System.err.println(args);");
		builder.addLine("	}");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	public String getExpectedUnderspecifiedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
		builder.addLine("");
		builder.addLine("public class LVD_DA1Re_Rn_Simple {");
		builder.addLine("");
		builder.addLine("	public static void main() {");
		builder.addLine("		final String[] args = new String[0];");
		builder.addLine("		System.err.println(args);");
		builder.addLine("	}");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected Pair<Anaphora, Anaphora> getExpectedResolvedAndRealizedAnaphora() {
		return new ImmutablePair<>(new Anaphora("LVD", "DA1Re", "Rn", "Args"),
				new Anaphora("LVD", "DA1Re", "Rn", "args"));
	}

}
