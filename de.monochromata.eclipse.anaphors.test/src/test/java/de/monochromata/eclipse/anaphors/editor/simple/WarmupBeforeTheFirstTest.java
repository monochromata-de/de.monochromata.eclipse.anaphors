package de.monochromata.eclipse.anaphors.editor.simple;

import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;

public class WarmupBeforeTheFirstTest extends AbstractSimpleEditorTest {

    @Override
    protected String getExpectedKindOfResolvedAnaphora() {
        return "CIC-DA1Re-RfRt";
    }

    @Override
    public void assertAnaphoraRelations() {
        // Do not assert anaphora relations. The resolution does not work at first
        // inside Maven.
    }

    @Override
    protected int getNumberOfAnaphorasAfterReResolution() {
        // Assuming that the first resolution was not counted correctly
        return 1;
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
        builder.addLine("");
        builder.addLine("public class WarmupBeforeTheFirst {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		final A fooA = new A(\"foo\");");
        builder.addLine("		new A(\"bar\");");
        builder.addLine("		System.err.println(fooA);");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("	private static class A {");
        builder.addLine("		public A(String feature) {");
        builder.addLine("		}");
        builder.addLine("	}");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
        builder.addLine("");
        builder.addLine("public class WarmupBeforeTheFirst {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		new A(\"foo\");");
        builder.addLine("		new A(\"bar\");");
        builder.addLine("		System.err.println(fooA);");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("	private static class A {");
        builder.addLine("		public A(String feature) {");
        builder.addLine("		}");
        builder.addLine("	}");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected Pair<Anaphora, Anaphora> getExpectedResolvedAndRealizedAnaphora() {
        throw new UnsupportedOperationException("This test shall not assert the anaphora");
    }

}
