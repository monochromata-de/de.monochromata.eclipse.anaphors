package de.monochromata.eclipse.anaphors.editor.interactive;

import org.eclipse.jdt.internal.ui.JavaPlugin;
import org.eclipse.jdt.ui.PreferenceConstants;

public interface MarkOccurrencesTesting {

	default void enableMarkOccurrences() {
		JavaPlugin.getDefault().getPreferenceStore().setValue(PreferenceConstants.EDITOR_MARK_OCCURRENCES, true);
	}

}
