package de.monochromata.eclipse.anaphors.annotation;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;

public interface AnnotationsSerialization {

    static List<String> toStrings(final List<Pair<? extends Annotation, ? extends Position>> annotations) {
        return annotations.stream().map(AnnotationsSerialization::toString).collect(toList());
    }

    static String toString(final Pair<? extends Annotation, ? extends Position> annotation) {
        return toString(annotation.getLeft()) + "," + annotation.getRight();
    }

    static String toString(final Annotation annotation) {
        return "persistent=" + annotation.isPersistent() + " type=" + annotation.getType() + " text="
                + annotation.getText();
    }

}
