package de.monochromata.eclipse.anaphors.editor.interactive;

import static java.util.Collections.emptyList;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;

import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.persp.HidePosition;
import de.monochromata.eclipse.persp.PerspectivationPosition;
import de.monochromata.eclipse.persp.ToLowerCasePosition;

public abstract class AbstractAnaphoraRemovingTest extends AbstractInteractiveDoubleAssertionEditorTest {

    @Override
    protected void waitForAnaphoraRelationsAfterEdit(final CognitiveEditor editor) {
        // Do not wait for anaphora relations, because the relation will not be removed
        // by the refactoring but by the edit - no AnaphorResolutionEvent will be
        // created.
        // waitForAnaphoraRelations(editor, 0);
    }

    @Override
    protected List<Pair<? extends Annotation, ? extends Position>> getExpectedIntermediateAnnotations() {
        return emptyList();
    }

    @Override
    protected int getExpectedNumberOfAnaphoraRelationsAfterEdit() {
        return 0;
    }

    @Override
    protected List<? extends Position> getExpectedPositions() {
        throw new UnsupportedOperationException("assertPositionsDoNotHaveType is used instead of assertGivenPositions");
    }

    @Override
    protected void waitForPositionsAfterEdit(final CognitiveEditor editor) {
        waitForRemovalOfPositionsOfBacklistedTypes(editor, PerspectivationPosition.class,
                AbstractAnaphoraPositionForRepresentation.class, ToLowerCasePosition.class, HidePosition.class);
    }

    @Override
    protected void assertPositionsAfterEdit(final CognitiveEditor editor) {
        assertPositionsDoNotHaveType(editor, PerspectivationPosition.class,
                AbstractAnaphoraPositionForRepresentation.class, ToLowerCasePosition.class, HidePosition.class);
    }

}
