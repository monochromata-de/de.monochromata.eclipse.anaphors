package de.monochromata.eclipse.anaphors.editor.interactive;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class LVD_IA2F_Rn_UpdatedTo_MI_IA1Mr_RtTest extends AbstractInteractiveDoubleAssertionEditorTest {

	@Override
	protected String getExpectedKindOfResolvedAnaphora() {
		return "LVD-IA2F-Rn";
	}

	@Override
	protected String getExpectedIntermediateRefactoredSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("public class LVD_IA2F_Rn_UpdatedTo_MI_IA1Mr_Rt {");
		builder.addLine("");
		builder.addLine("   public static void foo() {");
		builder.addLine("       A foo = new A();");
		builder.addLine("       System.err.println(foo.integer);");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("   private static class A {");
		builder.addLine("       public Integer integer;");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected String getExpectedIntermediateUnderspecifiedRefactoredSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("public class LVD_IA2F_Rn_UpdatedTo_MI_IA1Mr_Rt {");
		builder.addLine("");
		builder.addLine("   public static void foo() {");
		builder.addLine("       A foo = new A();");
		builder.addLine("       System.err.println(integer);");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("   private static class A {");
		builder.addLine("       public Integer integer;");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected List<Pair<? extends Annotation, ? extends Position>> getExpectedIntermediateAnnotations() {
		return emptyList();
	}

	@Override
	protected List<? extends Position> getExpectedIntermediatePositions() {
		final Anaphora anaphora = new Anaphora("LVD", "IA2F", "Rn", "integer");
		return asList(new PositionForAnaphor(192, 11, anaphora), new PositionForRelatedExpression(148, 16));
	}

	@Override
	protected void editFile(final IEditorPart editor) {
		deleteTextOnLine(10); // Removes the field declaration
		getBot().activeEditor().toTextEditor().insertText(6, 2, "bar();\n");
		getBot().activeEditor().toTextEditor().insertText(12, 0, "\n");
		getBot().activeEditor().toTextEditor().insertText(13, 0, "   public static Integer bar() {\n");
		getBot().activeEditor().toTextEditor().insertText(14, 0, "      return Integer.valueOf(0);\n");
		getBot().activeEditor().toTextEditor().insertText(15, 0, "   }\n");
		getBot().activeEditor().save();
	}

	@Override
	public String getExpectedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("public class LVD_IA2F_Rn_UpdatedTo_MI_IA1Mr_Rt {");
		builder.addLine("");
		builder.addLine("	public static void foo() {");
		builder.addLine("       A foo = new A();");
		builder.addLine("		final Integer integer = bar();");
		builder.addLine("		System.err.println(integer);");
		builder.addLine("	}");
		builder.addLine("");
		builder.addLine("   private static class A {");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("   public static Integer bar() {");
		builder.addLine("      return Integer.valueOf(0);");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	public String getExpectedUnderspecifiedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("public class LVD_IA2F_Rn_UpdatedTo_MI_IA1Mr_Rt {");
		builder.addLine("");
		builder.addLine("	public static void foo() {");
		builder.addLine("       A foo = new A();");
		builder.addLine("		bar();");
		builder.addLine("		System.err.println(integer);");
		builder.addLine("	}");
		builder.addLine("");
		builder.addLine("   private static class A {");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("   public static Integer bar() {");
		builder.addLine("      return Integer.valueOf(0);");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected int getExpectedNumberOfAnaphoraRelationsAfterEdit() {
		return 1;
	}

	@Override
	protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
		return asList(new ImmutablePair<>(
				new Annotation("org.eclipse.ui.workbench.texteditor.quickdiffUnchanged", false, "18 changed lines "),
				new Position(0, 356)));
	}

	@Override
	protected List<? extends Position> getExpectedPositions() {
		final Anaphora anaphora = new Anaphora("LVD", "DA1Re", "Rn", "integer");
		return asList(new PositionForAnaphor(226, 7, anaphora), new PositionForRelatedExpression(170, 30));
	}

}
