package de.monochromata.eclipse.anaphors.editor.interactive.feature;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.interactive.AbstractInteractiveDoubleAssertionEditorTest;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class MemberUpdateDoesNotYieldExceptionTest
        extends AbstractInteractiveDoubleAssertionEditorTest {

    @Override
    protected String getExpectedKindOfResolvedAnaphora() {
        return "CIC-DA1Re-HyFx";
    }

    @Override
    protected String getExpectedIntermediateRefactoredSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature;");
        builder.addLine("");
        builder.addLine("import javax.swing.JButton;");
        builder.addLine("import javax.swing.JPanel;");
        builder.addLine("");
        builder.addLine("public class " + getSimpleNameOfTestedType() + " {");
        builder.addLine("");
        builder.addLine("   public static void main(String[] args) {");
        builder.addLine("       new JPanel();");
        builder.addLine("       final JButton button = new JButton(\"start\");");
        builder.addLine("   System.err.println(button);");
        builder.addLine("   }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected String getExpectedIntermediateUnderspecifiedRefactoredSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature;");
        builder.addLine("");
        builder.addLine("import javax.swing.JButton;");
        builder.addLine("");
        builder.addLine("public class " + getSimpleNameOfTestedType() + " {");
        builder.addLine("");
        builder.addLine("   public static void main(String[] args) {");
        builder.addLine("       new JPanel();");
        builder.addLine("       new JButton(\"start\");");
        builder.addLine("   System.err.println(button);");
        builder.addLine("   }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected List<Pair<? extends Annotation, ? extends Position>> getExpectedIntermediateAnnotations() {
        return emptyList();
    }

    @Override
    protected List<? extends Position> getExpectedIntermediatePositions() {
        final Anaphora anaphora = new Anaphora("LVD", "DA1Re", "Rn", "button");
        final PositionForRelatedExpression positionForRelatedExpression = new PositionForRelatedExpression(236, 44);
        final PositionForAnaphor positionForAnaphor = new PositionForAnaphor(302, 6, anaphora);
        return asList(positionForRelatedExpression, positionForAnaphor);
    }

    @Override
    protected void editFile(final IEditorPart editor) {
        deleteTextOnLine(6);
        getBot().activeEditor().toTextEditor().insertText(6, 0, "public static void main() {\n");
        getBot().activeEditor().save();
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature;");
        builder.addLine("");
        builder.addLine("import javax.swing.JButton;");
        builder.addLine("import javax.swing.JPanel;");
        builder.addLine("");
        builder.addLine("public class " + getSimpleNameOfTestedType() + " {");
        builder.addLine("");
        builder.addLine("	public static void main() {");
        builder.addLine("		new JPanel();");
        builder.addLine("		final JButton button = new JButton(\"start\");");
        builder.addLine("	System.err.println(button);");
        builder.addLine("	}");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature;");
        builder.addLine("");
        builder.addLine("import javax.swing.JButton;");
        builder.addLine("");
        builder.addLine("public class " + getSimpleNameOfTestedType() + " {");
        builder.addLine("");
        builder.addLine("	public static void main() {");
        builder.addLine("		new JPanel();");
        builder.addLine("		new JButton(\"start\");");
        builder.addLine("	System.err.println(button);");
        builder.addLine("	}");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected int getExpectedNumberOfAnaphoraRelationsAfterEdit() {
        return 1;
    }

    @Override
    protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
        return emptyList();
    }

    @Override
    protected List<? extends Position> getExpectedPositions() {
        final Anaphora anaphora = new Anaphora("LVD", "DA1Re", "Rn", "button");
        final PositionForRelatedExpression positionForRelatedExpression = new PositionForRelatedExpression(223, 44);
        final PositionForAnaphor positionForAnaphor = new PositionForAnaphor(289, 6, anaphora);
        return asList(positionForRelatedExpression, positionForAnaphor);
    }

    protected String getSimpleNameOfTestedType() {
        final String testedType = getTestedType();
        return testedType.substring(testedType.lastIndexOf('/') + 1);
    }

}
