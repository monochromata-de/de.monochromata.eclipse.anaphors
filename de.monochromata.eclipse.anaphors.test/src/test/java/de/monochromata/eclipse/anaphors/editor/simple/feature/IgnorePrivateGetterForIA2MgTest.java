package de.monochromata.eclipse.anaphors.editor.simple.feature;

import de.monochromata.eclipse.anaphors.editor.CodeBuilder;

public class IgnorePrivateGetterForIA2MgTest extends AbstractSimpleFeatureEditorTest {

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple.feature;");
        builder.addLine("");
        builder.addLine("public class IgnorePrivateGetterForIA2Mg {");
        builder.addLine("");
        builder.addLine("    public static void main(String[] args) {");
        builder.addLine("        foo(new B());");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    public static void foo(B foo) {");
        builder.addLine("        System.err.println(foo.getValue());");
        builder.addLine("    }");
        builder.addLine("}");
        builder.addLine("");
        builder.addLine("class B {");
        builder.addLine("    public Integer getValue() {");
        builder.addLine("        return 10;");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    /**");
        builder.addLine("     * Does not cause ambiguity because the getter is not visible");
        builder.addLine("     */");
        builder.addLine("    @SuppressWarnings(\"unused\")");
        builder.addLine("    private Integer getOtherValue() {");
        builder.addLine("        return 11;");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple.feature;");
        builder.addLine("");
        builder.addLine("public class IgnorePrivateGetterForIA2Mg {");
        builder.addLine("");
        builder.addLine("    public static void main(String[] args) {");
        builder.addLine("        foo(new B());");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    public static void foo(B foo) {");
        builder.addLine("        System.err.println(value);");
        builder.addLine("    }");
        builder.addLine("}");
        builder.addLine("");
        builder.addLine("class B {");
        builder.addLine("    public Integer getValue() {");
        builder.addLine("        return 10;");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    /**");
        builder.addLine("     * Does not cause ambiguity because the getter is not visible");
        builder.addLine("     */");
        builder.addLine("    @SuppressWarnings(\"unused\")");
        builder.addLine("    private Integer getOtherValue() {");
        builder.addLine("        return 11;");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

}
