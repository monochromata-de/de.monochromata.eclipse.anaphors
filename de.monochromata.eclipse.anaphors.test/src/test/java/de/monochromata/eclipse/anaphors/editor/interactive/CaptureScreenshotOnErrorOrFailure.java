package de.monochromata.eclipse.anaphors.editor.interactive;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swtbot.swt.finder.junit.SWTBotJunit4ClassRunner;
import org.eclipse.swtbot.swt.finder.junit.ScreenshotCaptureListener;
import org.junit.rules.MethodRule;
import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

/**
 * A rule that is able to capture screenshots explicitly and implicitly.
 * 
 * <p>
 * Screenshots need to be captured explicitly if e.g. a method annotated with
 * {@code @After} closes an editor used during a test. If an error occurred
 * during the test, the contents of the window hint at the source of the error
 * only until the window is closed. Because {@code @After} methods are executed
 * before the rule or the {@link SWTBotJunit4ClassRunner} are notified of the
 * failure, a screenshot taken implicitly by the two cannot hint at the source
 * of the error. The methods
 * {@link #captureScreenshot(Object, String, Throwable)}
 * {@link #captureScreenshot(Object, Method, Throwable)} are used to capture
 * screenshots explicitly, as in the following example. (Note that an exception
 * for which an explicit screenshot has been created will not trigger a
 * subsequent implicit screenshot, i.e. the old screenshot will not be
 * overwritten.)
 * 
 * <pre>
 * &#64;Test
 * public void testEditor() throws Exception {
 * 	try {
 * 		manipulateEditorAndCheckAssertions();
 * 	} catch (Throwable throwable) {
 * 		screenshot.captureScreenshot(this, "testEditor", throwable);
 * 		throw throwable;
 * 	}
 * }
 * 
 * &#64;After
 * public void tearDown() {
 * 	closeEditor(editor);
 * 	editor = null;
 * }
 * </pre>
 * </p>
 * 
 * <p>
 * A screenshot is captured implicitly if an exception is thrown, i.e. if an
 * assertion fails or if an exception is thrown that leads the test to end in an
 * error.
 * </p>
 * 
 * <p>
 * This rule might be used instead of {@link SWTBotJunit4ClassRunner}, e.g.
 * 
 * <pre>
 * public class FooTest {
 * 
 * 	&#064;Rule
 * 	public final CaptureScreenshotOnErrorOrFailure screenshot = new CaptureScreenshotOnErrorOrFailure();
 * 
 * 	&#064;Test
 * 	public void canSendEmail() {
 * 	}
 * }
 * </pre>
 * </p>
 * 
 * @see #captureScreenshot(Object, String, Throwable)
 * @see #captureScreenshot(Object, Method, Throwable)
 *
 */
public class CaptureScreenshotOnErrorOrFailure implements MethodRule {

	protected final ScreenshotCaptureListener screenshotCapturer = new ScreenshotCaptureListener();
	protected List<ScreenshotTrigger> triggers;

	/**
	 * Implicitly captures a screenshot if an exception is thrown.
	 * 
	 * <p>
	 * Note that for methods and exceptions for which a screenshot has been
	 * taken explicitly via {@link #captureScreenshot(Method, Throwable)}, no
	 * additional screenshot will be taken.
	 * </p>
	 * 
	 * @param base
	 *            The statement to be wrapped by this rule.
	 * @param method
	 *            The test method to which the rule is applied.
	 * @param target
	 *            The test instance on which the test method is invoked.
	 * @return The statement representing this rule.
	 * @see CaptureScreenshotOnErrorOrFailure#captureScreenshot(Method,
	 *      Throwable)
	 */
	@Override
	public Statement apply(Statement base, FrameworkMethod method, Object target) {
		return new Statement() {

			@Override
			public void evaluate() throws Throwable {
				try {
					clearTriggers();
					base.evaluate();
				} catch (Throwable t) {
					ScreenshotTrigger trigger = new ScreenshotTrigger(method.getMethod(), t);
					if (!hasPreviouslyTriggeredAScreenshot(trigger)) {
						captureScreenshotInternal(target, trigger);
					}
					throw t;
				}
			}
		};
	}

	/**
	 * Explicitly capture a screenshot after a throwable occurred in a test
	 * method.
	 * 
	 * @param test
	 *            The test instance on which the test method is invoked.
	 * @param methodName
	 *            The name of the test method declared or inherited by the class
	 *            of the test instance in which the throwable occurred.
	 * @param throwable
	 *            The throwable that was raised.
	 * @throws IllegalStateException
	 *             If a screenshot has been captured for this method and
	 *             throwable before during the current application of this rule,
	 *             i.e. during execution of the statement returned by
	 *             {@link #apply(Statement, FrameworkMethod, Object)}.
	 * @throws Exception
	 *             If an exception was raised while capturing the screenshot.
	 * @see #captureScreenshot(Object, Method, Throwable)
	 */
	public void captureScreenshot(Object test, String methodName, Throwable throwable) throws Exception {
		Method method = test.getClass().getMethod(methodName);
		captureScreenshot(test, method, throwable);
	}

	/**
	 * Explicitly capture a screenshot after a throwable occurred in a test
	 * method.
	 * 
	 * @param test
	 *            The test instance on which the test method is invoked.
	 * @param method
	 *            The test method declared or inherited by the class of the test
	 *            instance in which the throwable occurred.
	 * @param throwable
	 *            The throwable that was raised.
	 * @throws IllegalStateException
	 *             If a screenshot has been captured for this method and
	 *             throwable before during the current application of this rule,
	 *             i.e. during execution of the statement returned by
	 *             {@link #apply(Statement, FrameworkMethod, Object)}.
	 * @throws Exception
	 *             If an exception was raised while capturing the screenshot.
	 * @see #captureScreenshot(Object, String, Throwable)
	 */
	public void captureScreenshot(Object test, Method method, Throwable throwable) throws Exception {
		ScreenshotTrigger trigger = new ScreenshotTrigger(method, throwable);
		if (hasPreviouslyTriggeredAScreenshot(trigger))
			throw new IllegalStateException(
					"Cannot overwrite existing screenshot for method=" + method + " and throwable=" + throwable);
		captureScreenshotInternal(test, trigger);
	}

	protected void clearTriggers() {
		if (triggers != null)
			triggers.clear();
		triggers = null;
	}

	protected boolean hasPreviouslyTriggeredAScreenshot(ScreenshotTrigger trigger) {
		if (triggers == null)
			return false;
		return triggers.contains(trigger);
	}

	protected void captureScreenshotInternal(Object test, ScreenshotTrigger trigger) throws Exception {
		Description description = Description.createTestDescription(test.getClass(), trigger.getMethod().getName());
		screenshotCapturer.testFailure(new Failure(description, trigger.getThrowable()));
		recordTrigger(trigger);
	}

	protected void recordTrigger(ScreenshotTrigger trigger) {
		if (triggers == null)
			triggers = new ArrayList<ScreenshotTrigger>(1);
		triggers.add(trigger);
	}
}
