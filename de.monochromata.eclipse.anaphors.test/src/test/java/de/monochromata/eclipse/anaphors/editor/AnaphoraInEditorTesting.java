package de.monochromata.eclipse.anaphors.editor;

import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent.EventType.ABOUT_TO_REALIZE_ANAPHORS;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.fail;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent;
import de.monochromata.event.Notification;

public interface AnaphoraInEditorTesting extends WaitingDuringTesting {

	default void notifyWaitee(final Object waitee) {
		synchronized (waitee) {
			waitee.notifyAll();
		}
	}

	void assertAnaphoraRelations();

	List<List<Pair<Anaphora, Anaphora>>> getAddedAnaphoras();

	List<List<Anaphora>> getRetainedAnaphoras();

	List<List<Triple<Anaphora, Anaphora, Anaphora>>> getUpdatedAnaphoras();

	List<List<Anaphora>> getRemovedAnaphoras();

	default void waitForAnaphoraRelations(final CognitiveEditor editor) {
		waitForAnaphoraRelations(editor, 1);
	}

	default void waitForAnaphoraRelations(final CognitiveEditor editor, final int expectedNumberOfAnaphoraRelations) {
		waitForAnaphoraRelations(editor, () -> getNumberOfAnaphoraRelations() == expectedNumberOfAnaphoraRelations);
	}

	default void waitForMaintainedAnaphoraRelations(final CognitiveEditor editor,
			final int expectedNumberOfAnaphoraRelations) {
		waitForAnaphoraRelations(editor,
				() -> getNumberOfMaintainedAnaphoraRelations() == expectedNumberOfAnaphoraRelations);
	}

	default void waitForAnaphoraRelations(final CognitiveEditor editor, final Supplier<Boolean> condition) {
		if (!condition.get()) {
			final Object waitee = new Object();
			final Consumer<AnaphorResolutionEvent> listener = anaphorResolutionEvent -> {
				if (condition.get()) {
					notifyWaitee(waitee);
				}
			};
			final Notification<AnaphorResolutionEvent.EventType, Consumer<AnaphorResolutionEvent>> notifier = editor
					.getAnaphorResolutionNotifier();
			notifier.addListener(ABOUT_TO_REALIZE_ANAPHORS, listener);
			waitSecondsIfNotNotifiedBefore("anaphora relations", WAIT_FOR_CHANGE_TIMEOUT, condition, waitee);
			notifier.removeListener(ABOUT_TO_REALIZE_ANAPHORS, listener);
		}
	}

	default Pair<Anaphora, Anaphora> getLatestAnaphoraRelation() {
		final List<Pair<Anaphora, Anaphora>> addedAnaphoraRelations = getAddedAnaphoraRelations().collect(toList());
		if (addedAnaphoraRelations.isEmpty()) {
			fail("Expected >= 1 added anaphora relation but was " + addedAnaphoraRelations);
		}
		return addedAnaphoraRelations.get(addedAnaphoraRelations.size() - 1);
	}

	default Pair<Anaphora, Anaphora> getOnlyAddedAnaphoraRelation() {
		final List<Pair<Anaphora, Anaphora>> addedAnaphoraRelations = getAddedAnaphoraRelations().collect(toList());
		if (addedAnaphoraRelations.size() != 1) {
			fail("Expected 1 added anaphora relation but was " + addedAnaphoraRelations);
		}
		return addedAnaphoraRelations.get(0);
	}

	default long getNumberOfAnaphoraRelations() {
		final var added = getAddedAnaphoraRelations().count();
		final var removed = getRemovedAnaphoraRelations().count();
		return added - removed;
	}

	default long getNumberOfMaintainedAnaphoraRelations() {
		final long addedRelations = getAddedAnaphoraRelations().count();
		final long retainedRelations = getRetainedAnaphoraRelations().count();
		final long updatedRelations = getUpdatedAnaphoraRelations().count();
		return addedRelations + retainedRelations + updatedRelations;
	}

	default Stream<Pair<Anaphora, Anaphora>> getAddedAnaphoraRelations() {
		return getAddedAnaphoras().stream().flatMap(List::stream);
	}

	default Stream<Anaphora> getRetainedAnaphoraRelations() {
		return getRetainedAnaphoras().stream().flatMap(List::stream);
	}

	default Stream<Triple<Anaphora, Anaphora, Anaphora>> getUpdatedAnaphoraRelations() {
		return getUpdatedAnaphoras().stream().flatMap(List::stream);
	}

	default Stream<Anaphora> getRemovedAnaphoraRelations() {
		return getRemovedAnaphoras().stream().flatMap(List::stream);
	}

}
