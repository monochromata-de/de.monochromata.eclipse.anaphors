/**
 * Tests for
 * {@link de.monochromata.eclipse.anaphors.editor.CognitiveEditor}
 * .
 * 
 * The following kinds of tests are available:
 * <ul>
 * <li>Simple tests: Load a single file with a single anaphor (see package
 * {@link de.monochromata.eclipse.anaphors.editor.simple})</li>
 * <li>Interactive tests: Test the interactive features of the editor (see
 * package {@link de.monochromata.eclipse.anaphors.editor.interactive})</li>
 * </ul>
 * 
 * @author monochromata
 */
package de.monochromata.eclipse.anaphors.editor;