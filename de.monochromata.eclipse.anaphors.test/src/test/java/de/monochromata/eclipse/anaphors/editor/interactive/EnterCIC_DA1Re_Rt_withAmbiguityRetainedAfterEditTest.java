package de.monochromata.eclipse.anaphors.editor.interactive;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;

import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;

public class EnterCIC_DA1Re_Rt_withAmbiguityRetainedAfterEditTest
        extends AbstractInteractiveEditorTest {

    @Override
    protected void manipulateEditorAndCheckAssertions(final ProblemCollectingChangeListener problemCollector,
            final CognitiveEditor editor) {
        enableMarkOccurrences();
        editFile(editor);
        // don't wait for problems to be solved
        // dont't assert refactoring results, because no refactoring was
        // performed
        waitForSharedASTToBeReconciled(editor);
        waitForExpectedAnnotations(editor, getExpectedAnnotations());
        assertAnnotations(editor, getExpectedAnnotations());
    }

    @Override
    protected void editFile(final IEditorPart editor) {
        getBot().activeEditor().toTextEditor().insertText(10, 21, "j");
        getBot().activeEditor().save();
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
        builder.addLine("");
        builder.addLine("import javax.swing.JButton;");
        builder.addLine("import javax.swing.JPanel;");
        builder.addLine("");
        builder.addLine("public class EnterCIC_DA1Re_Rt_withAmbiguityRetainedAfterEdit {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		new JPanel();");
        builder.addLine("		new JButton(\"start\");");
        builder.addLine("		new JButton(\"stop\");");
        builder.addLine("		System.err.println(jbutton);");
        builder.addLine("	}");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
        builder.addLine("");
        builder.addLine("import javax.swing.JButton;");
        builder.addLine("import javax.swing.JPanel;");
        builder.addLine("");
        builder.addLine("public class EnterCIC_DA1Re_Rt_withAmbiguityRetainedAfterEdit {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		new JPanel();");
        builder.addLine("		new JButton(\"start\");");
        builder.addLine("		new JButton(\"stop\");");
        builder.addLine("		System.err.println(jbutton);");
        builder.addLine("	}");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
        return asList(
                new ImmutablePair<>(new Annotation("org.eclipse.jdt.ui.error", false,
                        "Ambiguous reference (more than 1 potential referents found):\n"
                                + "new JButton(\"start\") at 10:2 (CIC-DA1Re-Rt)\n"
                                + "new JButton(\"stop\") at 11:2 (CIC-DA1Re-Rt)"),
                        new Position(309, 7)),
                new ImmutablePair<>(
                        new Annotation("org.eclipse.jdt.ui.error", false, "jbutton cannot be resolved to a variable"),
                        new Position(309, 7)));
    }

    @Override
    protected List<? extends Position> getExpectedPositions() {
        return emptyList();
    }

}
