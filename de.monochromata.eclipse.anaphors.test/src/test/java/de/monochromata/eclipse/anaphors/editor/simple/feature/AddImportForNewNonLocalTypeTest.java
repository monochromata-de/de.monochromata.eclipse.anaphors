package de.monochromata.eclipse.anaphors.editor.simple.feature;

import de.monochromata.eclipse.anaphors.editor.CodeBuilder;

public class AddImportForNewNonLocalTypeTest extends AbstractSimpleFeatureEditorTest {

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple.feature;");
        builder.addLine("");
        builder.addLine("import java.util.concurrent.ExecutorService;");
        builder.addLine("import java.util.concurrent.Future;");
        builder.addLine("");
        builder.addLine("public class AddImportForNewNonLocalType {");
        builder.addLine("");
        builder.addLine("    public static void run(final ExecutorService executorService) {");
        builder.addLine("        final Future<Integer> future = executorService.submit(null, Integer.valueOf(0));");
        builder.addLine("        System.err.println(future);");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple.feature;");
        builder.addLine("");
        builder.addLine("import java.util.concurrent.ExecutorService;");
        builder.addLine("import java.util.concurrent.Future;");
        builder.addLine("");
        builder.addLine("public class AddImportForNewNonLocalType {");
        builder.addLine("");
        builder.addLine("    public static void run(final ExecutorService executorService) {");
        builder.addLine("        executorService.submit(null, Integer.valueOf(0));");
        builder.addLine("        System.err.println(future);");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

}
