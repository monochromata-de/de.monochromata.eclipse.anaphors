package de.monochromata.eclipse.anaphors.editor;

import static de.monochromata.eclipse.anaphors.editor.AmbiguityAnnotationCreation.createProblem;
import static org.assertj.core.api.Assertions.assertThat;

import org.eclipse.jdt.core.compiler.IProblem;
import org.eclipse.jdt.internal.compiler.problem.DefaultProblem;
import org.eclipse.jdt.internal.compiler.problem.ProblemSeverities;
import org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitDocumentProvider.ProblemAnnotation;
import org.junit.Test;

public class AmbiguityAnnotationHandlingTest {

    @Test
    @SuppressWarnings("restriction")
    public void newProblemAnnotationIsRecognizedAsAnaphorRelated() {
        final IProblem problem = createProblem("details", 0, 10, 1, 1, "Foo.java".toCharArray());
        final ProblemAnnotation problemAnnotation = new ProblemAnnotation(problem, null);
        assertThat(AmbiguityAnnotationRemoval.isAnaphorRelated(problemAnnotation)).isTrue();
    }

    @Test
    @SuppressWarnings("restriction")
    public void otherProblemAnnotationIsNotRecognizedAsAnaphorRelated() {
        final IProblem problem = new DefaultProblem("Foo.java".toCharArray(), "details", 0, new String[0],
                ProblemSeverities.Error, 0, 10, 1, 1);
        final ProblemAnnotation problemAnnotation = new ProblemAnnotation(problem, null);
        assertThat(AmbiguityAnnotationRemoval.isAnaphorRelated(problemAnnotation)).isFalse();
    }

}
