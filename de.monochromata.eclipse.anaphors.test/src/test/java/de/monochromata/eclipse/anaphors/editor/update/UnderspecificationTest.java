package de.monochromata.eclipse.anaphors.editor.update;

import static de.monochromata.anaphors.ast.relatedexp.strategy.LocalTempVariableContents.ANCHOR;
import static de.monochromata.anaphors.ast.relatedexp.strategy.LocalTempVariableContents.REFERENT;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadLocationException;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.eclipse.jdt.core.dom.AST.JLS8;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jface.text.DocumentEvent;
import org.junit.Ignore;
import org.junit.Test;

import de.monochromata.anaphors.ast.relatedexp.strategy.ClassInstanceCreationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalVariableDeclarationStrategy;
import de.monochromata.anaphors.ast.strategy.IA2FStrategy;
import de.monochromata.anaphors.ast.strategy.IA2MgStrategy;
import de.monochromata.anaphors.perspectivation.Perspectivation;
import de.monochromata.eclipse.persp.PerspectivationDocument;
import de.monochromata.eclipse.persp.PerspectivationPosition;

// See https://gitlab.com/de.monochromata/de.monochromata.anaphors/issues/25
@Ignore
public class UnderspecificationTest implements UnderspecificationTesting {

    // Test underspecifyRelatedExpression

    @Test
    public void underspecifyRelatedExpression_forCICRelatedExpressionStrategy() throws Exception {
        runWithSpis(spis -> {
            final AST ast = AST.newAST(JLS8);
            final ClassInstanceCreation cic = createClassInstanceCreation(ast, "Foo", 4);
            ast.newExpressionStatement(cic);
            final ClassInstanceCreationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression, PublicAnaphora> cicStrategy = new ClassInstanceCreationStrategy<>(
                    null, null);
            final var relatedExpression = PublicRelatedExpression.createForClassInstanceCreation(cic, cicStrategy,
                    null);

            final String relatedExpressionPrefix = "final Foo foo = ";
            final Consumer<PerspectivationDocument> editingConsumer = perspectivationDocument -> wrapBadLocationException(
                    () -> perspectivationDocument.replace(4, 0, relatedExpressionPrefix));
            final Predicate<DocumentEvent> condition = event -> event.fOffset == 4 && event.fLength == 0
                    && event.fText.equals(relatedExpressionPrefix);
            assertPerspectivationPositionForRelatedExpression("... new Foo(); field ...", relatedExpression,
                    new ClassInstanceCreationStrategy<>(null, null), "field", ANCHOR, editingConsumer, condition, 4, 25,
                    (perspectivationDocument, masterDocument) -> {
                        assertThat(masterDocument.get()).isEqualTo("... final Foo foo = new Foo(); field ...");
                        assertThat(perspectivationDocument.get()).isEqualTo("... new Foo(); field ...");
                    }, spis.relatedExpressionsSpi);
        });
    }

    @Test
    public void underspecifyRelatedExpression_forPDRelatedExpressionStrategy() {
        fail("TODO: implement");
    }

    @Test
    public void underspecifyRelatedExpression_forLVDRelatedExpressionStrategy() throws Exception {
        runWithSpis(spis -> {
            final AST ast = AST.newAST(JLS8);
            final SingleVariableDeclaration svd = createSingleVariableDeclaration(ast, "Foo", "foo", 0);
            final LocalVariableDeclarationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression> lvdStrategy = new LocalVariableDeclarationStrategy<>(
                    spis.relatedExpressionsSpi, spis.preferencesSpi);
            final var relatedExpression = PublicRelatedExpression.createForLocalVariableDeclaration(svd, lvdStrategy,
                    null);

            // elaborated source == underspecified source
            final List<Perspectivation> perspectivations = lvdStrategy.underspecifyRelatedExpression(relatedExpression,
                    singletonList(new ImmutablePair<>(REFERENT, "foo")), null);
            final PerspectivationPosition position = (PerspectivationPosition) spis.relatedExpressionsSpi
                    .createPositionForNode(relatedExpression.getRelatedExpression(), unused -> true, perspectivations);

            assertThat(position.offset).isEqualTo(0);
            assertThat(position.length).isEqualTo("Foo foo".length());
            assertThat(position.perspectivations).isEmpty();
        });
    }

    @Test
    public void underspecifyRelatedExpression_forMIRelatedExpressionStrategy() {
        fail("TODO: implement");
    }

    // Test underspecifyAnaphor

    @Test
    public void underspecifyAnaphor_forDA1ReAnaphorResolutionStrategy() throws Exception {
        runWithSpis(spis -> {
            final AST ast = AST.newAST(JLS8);
            final ClassInstanceCreation cic = createClassInstanceCreation(ast, "Foo", 4);
            ast.newExpressionStatement(cic);
            final ClassInstanceCreationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression, PublicAnaphora> cicStrategy = new ClassInstanceCreationStrategy<>(
                    spis.relatedExpressionsSpi, spis.preferencesSpi);
            final var relatedExpression = PublicRelatedExpression.createForClassInstanceCreation(cic, cicStrategy,
                    null);
            final SimpleName anaphorExpression = ast.newSimpleName("anaphor");
            anaphorExpression.setSourceRange(4, 7);

            // elaborated source == underspecified source
            final List<Perspectivation> perspectivations = cicStrategy.underspecifyRelatedExpression(relatedExpression,
                    singletonList(new ImmutablePair<>(REFERENT, "anaphor")), null);
            final PerspectivationPosition position = (PerspectivationPosition) spis.anaphorsSpi
                    .createPositionForExpression(anaphorExpression, unused -> true, perspectivations);

            assertThat(position.offset).isEqualTo(4);
            assertThat(position.length).isEqualTo(7);
            assertThat(position.perspectivations).isEmpty();
        });
    }

    @Test
    public void underspecifyAnaphor_forCIC_IA2F() throws Exception {
        runWithSpis(spis -> {
            final AST ast = AST.newAST(JLS8);
            final ClassInstanceCreation cic = createClassInstanceCreation(ast, "Foo", 4);
            ast.newExpressionStatement(cic);
            final ClassInstanceCreationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression, PublicAnaphora> cicStrategy = new ClassInstanceCreationStrategy<>(
                    null, null);
            final var relatedExpression = PublicRelatedExpression.createForClassInstanceCreation(cic, cicStrategy,
                    null);
            final SimpleName anaphorExpression = createSimpleName(ast, "field", 15);

            final String relatedExpressionPrefix = "final Foo foo = ";
            final String anaphorQualifier = "foo.";
            final Consumer<PerspectivationDocument> editingConsumer = perspectivationDocument -> {
                wrapBadLocationException(() -> perspectivationDocument.replace(4, 0, relatedExpressionPrefix));
                wrapBadLocationException(() -> perspectivationDocument.replace(15 + relatedExpressionPrefix.length(), 0,
                        anaphorQualifier));
            };
            final Predicate<DocumentEvent> condition = event -> event.fOffset == 31 && event.fLength == 0
                    && event.fText.equals(anaphorQualifier);
            assertPerspectivationPositionForAnaphor("... new Foo(); field ...", relatedExpression, "field",
                    anaphorExpression, new IA2FStrategy<>(null, null, null), editingConsumer, condition, 31, 9,
                    (perspectivationDocument, masterDocument) -> {
                        assertThat(masterDocument.get()).isEqualTo("... final Foo foo = new Foo(); foo.field ...");
                        assertThat(perspectivationDocument.get()).isEqualTo("... final Foo foo = new Foo(); field ...");
                    }, spis.anaphorsSpi);
        });
    }

    @Test
    public void underspecifyAnaphor_forPD_IA2F() {
        fail("TODO: implement");
    }

    @Test
    public void underspecifyAnaphor_forLVD_IA2F() throws Exception {
        runWithSpis(spis -> {
            final AST ast = AST.newAST(JLS8);
            final SingleVariableDeclaration svd = createSingleVariableDeclaration(ast, "Foo", "foo", 0);
            final LocalVariableDeclarationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression> lvdStrategy = new LocalVariableDeclarationStrategy<>(
                    null, null);
            final var relatedExpression = PublicRelatedExpression.createForLocalVariableDeclaration(svd, lvdStrategy,
                    null);
            final SimpleName anaphorExpression = createSimpleName(ast, "field", 12);

            final String anaphorQualifier = "foo.";
            final Consumer<PerspectivationDocument> editingConsumer = perspectivationDocument -> {
                wrapBadLocationException(() -> perspectivationDocument.replace(12, 0, anaphorQualifier));
            };
            final Predicate<DocumentEvent> condition = event -> event.fOffset == 12 && event.fLength == 0
                    && event.fText.equals(anaphorQualifier);
            assertPerspectivationPositionForAnaphor("Foo foo ... field ...", relatedExpression, "field",
                    anaphorExpression, new IA2FStrategy<>(null, null, null), editingConsumer, condition, 12, 9,
                    (perspectivationDocument, masterDocument) -> {
                        assertThat(masterDocument.get()).isEqualTo("Foo foo ... foo.field ...");
                        assertThat(perspectivationDocument.get()).isEqualTo("Foo foo ... field ...");
                    }, spis.anaphorsSpi);
        });
    }

    @Test
    public void underspecifyAnaphor_forCIC_IA2Mg() throws Exception {
        runWithSpis(spis -> {
            final AST ast = AST.newAST(JLS8);
            final ClassInstanceCreation cic = createClassInstanceCreation(ast, "Foo", 4);
            ast.newExpressionStatement(cic);
            final ClassInstanceCreationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression, PublicAnaphora> cicStrategy = new ClassInstanceCreationStrategy<>(
                    null, null);
            final var relatedExpression = PublicRelatedExpression.createForClassInstanceCreation(cic, cicStrategy,
                    null);
            final SimpleName anaphorExpression = createSimpleName(ast, "bar", 15);

            final String relatedExpressionPrefix = "final Foo foo = ";
            final String anaphorQualifier = "foo.";
            final Consumer<PerspectivationDocument> editingConsumer = perspectivationDocument -> {
                wrapBadLocationException(() -> perspectivationDocument.replace(4, 0, relatedExpressionPrefix));
                wrapBadLocationException(() -> perspectivationDocument.replace(31, 0, anaphorQualifier));
                wrapBadLocationException(() -> perspectivationDocument.replace(35, 3, "getBar()"));
            };
            final Predicate<DocumentEvent> condition = event -> event.fOffset == 35 && event.fLength == 3
                    && event.fText.equals("getBar()");
            assertPerspectivationPositionForAnaphor("... new Foo(); bar ...", relatedExpression, "bar",
                    anaphorExpression, new IA2MgStrategy<>(null, null, null), editingConsumer, condition, 31, 12,
                    (perspectivationDocument, masterDocument) -> {
                        assertThat(masterDocument.get()).isEqualTo("... final Foo foo = new Foo(); foo.getBar() ...");
                        assertThat(perspectivationDocument.get()).isEqualTo("... final Foo foo = new Foo(); bar ...");
                    }, spis.anaphorsSpi);
        });
    }

    @Test
    public void underspecifyAnaphor_forLPD_IA2Mg() {
        fail("TODO: implement");
    }

    @Test
    public void underspecifyAnaphor_forLVD_IA2Mg() throws Exception {
        runWithSpis(spis -> {
            final AST ast = AST.newAST(JLS8);
            final SingleVariableDeclaration svd = createSingleVariableDeclaration(ast, "Foo", "foo", 4);
            final LocalVariableDeclarationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression> lvdStrategy = new LocalVariableDeclarationStrategy<>(
                    null, null);
            final var relatedExpression = PublicRelatedExpression.createForLocalVariableDeclaration(svd, lvdStrategy,
                    null);
            final SimpleName anaphorExpression = createSimpleName(ast, "bar", 12);

            final Consumer<PerspectivationDocument> editingConsumer = perspectivationDocument -> {
                wrapBadLocationException(() -> perspectivationDocument.replace(12, 3, ""));
                wrapBadLocationException(() -> perspectivationDocument.replace(12, 0, ".getBar()"));
                wrapBadLocationException(() -> perspectivationDocument.replace(12, 0, "foo"));
            };
            final Predicate<DocumentEvent> condition = event -> event.fOffset == 12 && event.fLength == 0
                    && event.fText.equals("foo");
            assertPerspectivationPositionForAnaphor("Foo foo ... bar ...", relatedExpression, "bar", anaphorExpression,
                    new IA2MgStrategy<>(null, null, null), editingConsumer, condition, 12, 12,
                    (perspectivationDocument, masterDocument) -> {
                        assertThat(masterDocument.get()).isEqualTo("Foo foo ... foo.getBar() ...");
                        assertThat(perspectivationDocument.get()).isEqualTo("Foo foo ... bar ...");
                    }, spis.anaphorsSpi);
        });
    }

    @Test
    public void underspecifyAnaphor_forMI_IA1Mr() {
        fail("TODO: implement");
    }

}
