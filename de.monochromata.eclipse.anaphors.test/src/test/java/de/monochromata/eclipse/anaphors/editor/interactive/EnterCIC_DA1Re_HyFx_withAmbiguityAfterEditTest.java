package de.monochromata.eclipse.anaphors.editor.interactive;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;

import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;

public class EnterCIC_DA1Re_HyFx_withAmbiguityAfterEditTest extends AbstractInteractiveDoubleAssertionEditorTest {

	@Override
	public void waitForAnaphoraRelations(final CognitiveEditor editor) {
		waitForAnaphoraRelations(editor, 0);
	}

	@Override
	public void assertAnaphoraRelations() {
		// There should be no anaphora relations eventually. Their absence is asserted
		// indirectly via the absence of anaphora positions.
	}

	@Override
	protected List<Pair<? extends Annotation, ? extends Position>> getExpectedIntermediateAnnotations() {
		return emptyList();
	}

	@Override
	protected void waitForAnaphoraRelationsAfterEdit(final CognitiveEditor editor) {
		// Do not wait for anaphora relations, because the relation will not be removed
		// by the refactoring but by the edit - no AnaphorResolutionEvent will be
		// created.
		// waitForAnaphoraRelations(editor, 0);
	}

	@Override
	protected int getExpectedNumberOfAnaphoraRelationsAfterEdit() {
		return 0;
	}

	@Override
	protected String getExpectedIntermediateRefactoredSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("import javax.swing.JButton;");
		builder.addLine("import javax.swing.JPanel;");
		builder.addLine("");
		builder.addLine("public class EnterCIC_DA1Re_HyFx_withAmbiguityAfterEdit {");
		builder.addLine("");
		builder.addLine("   public static void main(String[] args) {");
		builder.addLine("       new JPanel();");
		builder.addLine("       new JButton(\"start\");");
		builder.addLine("       new JButton(\"stop\");");
		builder.addLine("   }");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected String getExpectedIntermediateUnderspecifiedRefactoredSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("import javax.swing.JButton;");
		builder.addLine("");
		builder.addLine("public class EnterCIC_DA1Re_HyFx_withAmbiguityAfterEdit {");
		builder.addLine("");
		builder.addLine("   public static void main(String[] args) {");
		builder.addLine("       new JPanel();");
		builder.addLine("       new JButton(\"start\");");
		builder.addLine("       new JButton(\"stop\");");
		builder.addLine("   }");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected List<? extends Position> getExpectedIntermediatePositions() {
		return emptyList();
	}

	@Override
	protected void editFile(final IEditorPart editor) {
		getBot().activeEditor().toTextEditor().insertText(10, 0, "System.err.println(j);\n");
		getBot().activeEditor().save();
	}

	@Override
	public String getExpectedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("import javax.swing.JButton;");
		builder.addLine("import javax.swing.JPanel;");
		builder.addLine("");
		builder.addLine("public class EnterCIC_DA1Re_HyFx_withAmbiguityAfterEdit {");
		builder.addLine("");
		builder.addLine("	public static void main(String[] args) {");
		builder.addLine("		new JPanel();");
		builder.addLine("		new JButton(\"start\");");
		builder.addLine("		new JButton(\"stop\");");
		builder.addLine("	System.err.println(j);");
		builder.addLine("	}");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	public String getExpectedUnderspecifiedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("import javax.swing.JButton;");
		builder.addLine("");
		builder.addLine("public class EnterCIC_DA1Re_HyFx_withAmbiguityAfterEdit {");
		builder.addLine("");
		builder.addLine("	public static void main(String[] args) {");
		builder.addLine("		new JPanel();");
		builder.addLine("		new JButton(\"start\");");
		builder.addLine("		new JButton(\"stop\");");
		builder.addLine("	System.err.println(j);");
		builder.addLine("	}");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
		return asList(
				new ImmutablePair<>(new Annotation("org.eclipse.jdt.ui.error", false,
						"Ambiguous reference (more than 1 potential referents found):\n"
								+ "getTreeLock():Object of new JPanel() at 9:2 (CIC-IA2Mg-HyFx)\n"
								+ "getSelectedObjects():Object[] of new JButton(\"start\") at 10:2 (CIC-IA2Mg-HyFx)\n"
								+ "getTreeLock():Object of new JButton(\"start\") at 10:2 (CIC-IA2Mg-HyFx)\n"
								+ "getSelectedObjects():Object[] of new JButton(\"stop\") at 11:8 (CIC-IA2Mg-HyFx)\n"
								+ "getTreeLock():Object of new JButton(\"stop\") at 11:8 (CIC-IA2Mg-HyFx)"),
						new Position(315, 1)),
				new ImmutablePair<>(
						new Annotation("org.eclipse.jdt.ui.error", false, "j cannot be resolved to a variable"),
						new Position(315, 1)));
	}

	@Override
	protected List<? extends Position> getExpectedPositions() {
		return emptyList();
	}

}
