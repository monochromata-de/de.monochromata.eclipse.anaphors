package de.monochromata.eclipse.anaphors.editor.simple.feature;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.eclipse.jdt.core.compiler.IProblem;
import org.eclipse.jface.text.Position;
import org.junit.Ignore;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.position.KnownProblemPosition;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

@Ignore
public class AmbiguousAnaphorShouldNotBlockResolutionOfOtherAnaphorsTest extends AbstractSimpleFeatureEditorTest {

    @Override
    protected String getExpectedKindOfResolvedAnaphora() {
        return "CIC-DA1Re-RfRt";
    }

    @Override
    protected void manipulateEditorAndCheckAssertions(final ProblemCollectingChangeListener problemCollector,
            final CognitiveEditor editor) {
        waitForAnaphoraRelations(editor, () -> addedAnaphoras.size() == 2 && addedAnaphoras.get(0).size() == 1
                && addedAnaphoras.get(1).size() == 0);
        waitForProblems(editor, 1);
        assertRefactoringResults(editor, problemCollector);
        waitForExpectedPositions(editor, getExpectedPositions());
        assertGivenPositions(editor, getExpectedPositions());
        // While one anaphor could not be resolved, its associated problem should not be
        // memorized. Instead, resolution should be re-attempted after every
        // reconciliation.
        assertPositionsDoNotHaveType(editor, KnownProblemPosition.class);
        assertAnaphoraRelation();
    }

    @Override
    protected void assertRefactoringResults(final CognitiveEditor editor, final List<IProblem> problems) {
        assertEventualSourceCode(editor);
        assertUnderspecifiedEventualSourceCode(editor);
        assertProblemMarkers(editor, problems);
    }

    @Override
    protected void assertProblemMarkers(final CognitiveEditor editor, final List<IProblem> problems) {
        assertThat(problems).hasSize(1);
        final IProblem problem = problems.get(0);
        assertThat(problem.getMessage()).isEqualTo("belloString cannot be resolved to a variable");
        assertThat(problem.getSourceLineNumber()).isEqualTo(8);
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple.feature;");
        builder.addLine("");
        builder.addLine("public class AmbiguousAnaphorShouldNotBlockResolutionOfOtherAnaphors {");
        builder.addLine("");
        builder.addLine("    public static void foo() {");
        builder.addLine("        final String helloString = new String(\"Hello\");");
        builder.addLine("        System.err.println(helloString);");
        builder.addLine("        System.err.println(belloString);");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple.feature;");
        builder.addLine("");
        builder.addLine("public class AmbiguousAnaphorShouldNotBlockResolutionOfOtherAnaphors {");
        builder.addLine("");
        builder.addLine("    public static void foo() {");
        builder.addLine("        new String(\"Hello\");");
        builder.addLine("        System.err.println(helloString);");
        builder.addLine("        System.err.println(belloString);");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    protected List<? extends Position> getExpectedPositions() {
        final Anaphora anaphora = new Anaphora("LVD", "DA1Re", "Rn", "helloString");
        final PositionForRelatedExpression relatedExpression = new PositionForRelatedExpression(176, 47);
        final PositionForAnaphor anaphor = new PositionForAnaphor(251, 11, anaphora);

        return asList(relatedExpression, anaphor);
    }

}
