package de.monochromata.eclipse.anaphors.editor.interactive;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class PD_IA2Mg_Rn_UpdatedTo_PD_IA2F_RnTest extends AbstractInteractiveDoubleAssertionEditorTest {

	@Override
	protected String getExpectedKindOfResolvedAnaphora() {
		return "PD-IA2Mg-Rn";
	}

	@Override
	protected String getExpectedIntermediateRefactoredSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("public class PD_IA2Mg_Rn_UpdatedTo_PD_IA2F_Rn {");
		builder.addLine("");
		builder.addLine("   public static void foo(A foo) {");
		builder.addLine("       System.err.println(foo.getValue());");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("   private static class A {");
		builder.addLine("       public Integer getValue() {");
		builder.addLine("          return Integer.valueOf(0);");
		builder.addLine("       }");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected String getExpectedIntermediateUnderspecifiedRefactoredSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("public class PD_IA2Mg_Rn_UpdatedTo_PD_IA2F_Rn {");
		builder.addLine("");
		builder.addLine("   public static void foo(A foo) {");
		builder.addLine("       System.err.println(value);");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("   private static class A {");
		builder.addLine("       public Integer getValue() {");
		builder.addLine("          return Integer.valueOf(0);");
		builder.addLine("       }");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected List<Pair<? extends Annotation, ? extends Position>> getExpectedIntermediateAnnotations() {
		return emptyList();
	}

	@Override
	protected List<? extends Position> getExpectedIntermediatePositions() {
		final Anaphora anaphora = new Anaphora("PD", "IA2Mg", "Rn", "value");
		return asList(new PositionForAnaphor(174, 14, anaphora), new PositionForRelatedExpression(138, 5));
	}

	@Override
	protected void editFile(final IEditorPart editor) {
		deleteTextOnLine(11); // Delete old method declaration
		deleteTextOnLine(10); // Delete old method declaration
		deleteTextOnLine(9); // Delete old method declaration
		getBot().activeEditor().toTextEditor().insertText(9, 0, "   public Integer value;\n");
		getBot().activeEditor().save();
	}

	@Override
	public String getExpectedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("public class PD_IA2Mg_Rn_UpdatedTo_PD_IA2F_Rn {");
		builder.addLine("");
		builder.addLine("	public static void foo(A foo) {");
		builder.addLine("		System.err.println(foo.value);");
		builder.addLine("	}");
		builder.addLine("");
		builder.addLine("   private static class A {");
		builder.addLine("      public Integer value;");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	public String getExpectedUnderspecifiedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("public class PD_IA2Mg_Rn_UpdatedTo_PD_IA2F_Rn {");
		builder.addLine("");
		builder.addLine("	public static void foo(A foo) {");
		builder.addLine("		System.err.println(value);");
		builder.addLine("	}");
		builder.addLine("");
		builder.addLine("   private static class A {");
		builder.addLine("      public Integer value;");
		builder.addLine("   }");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected int getExpectedNumberOfAnaphoraRelationsAfterEdit() {
		return 1;
	}

	@Override
	protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
		return asList(new ImmutablePair<>(
				new Annotation("org.eclipse.ui.workbench.texteditor.quickdiffUnchanged", false, "13 changed lines "),
				new Position(0, 260)));
	}

	@Override
	protected List<? extends Position> getExpectedPositions() {
		final Anaphora anaphora = new Anaphora("PD", "IA2F", "Rn", "value");
		return asList(new PositionForAnaphor(174, 9, anaphora), new PositionForRelatedExpression(138, 5));
	}

}
