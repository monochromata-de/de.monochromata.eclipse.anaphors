package de.monochromata.eclipse.anaphors.position;

import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadPositionCategoryException;
import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.BadPositionCategoryException;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentListener;
import org.eclipse.jface.text.IPositionUpdater;
import org.eclipse.jface.text.Position;

import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.editor.WaitingDuringTesting;
import de.monochromata.eclipse.position.DoublyLinkedPosition;

public interface PositionsTesting extends WaitingDuringTesting {

	default void assertPositionsAreLinked(final List<DoublyLinkedPosition> positions) {
		assertThat(positions).hasSize(2);
		assertPositionsAreLinked(positions.get(0), positions.get(1));
	}

	default void assertPositionsAreLinked(
			final Pair<? extends DoublyLinkedPosition, ? extends DoublyLinkedPosition> positions) {
		assertPositionsAreLinked(positions.getLeft(), positions.getRight());
	}

	default void assertPositionsAreLinked(final DoublyLinkedPosition leftPosition,
			final DoublyLinkedPosition rightPosition) {
		assertThat(leftPosition.getPrevious()).isNull();
		assertThat(leftPosition.getNext()).containsExactly(rightPosition);
		isRightTo(leftPosition, rightPosition);
	}

	default void assertPositionsAreLinked(final DoublyLinkedPosition leftPosition,
			final DoublyLinkedPosition... rightPositions) {
		assertThat(leftPosition.getPrevious()).isNull();
		assertThat(leftPosition.getNext()).containsExactlyElementsOf(asList(rightPositions));
		asList(rightPositions).forEach(isRightTo(leftPosition));
	}

	default Consumer<? super DoublyLinkedPosition> isRightTo(final DoublyLinkedPosition leftPosition) {
		return rightPosition -> isRightTo(leftPosition, rightPosition);
	}

	default void isRightTo(final DoublyLinkedPosition leftPosition, final DoublyLinkedPosition rightPosition) {
		assertThat(rightPosition.getPrevious()).isSameAs(leftPosition);
		assertThat(rightPosition.getNext()).isEmpty();
	}

	default void waitForExpectedPositions(final CognitiveEditor editor,
			final List<? extends Position> expectedPositions) {
		final IDocument document = getDocument(editor);
		waitForPositions(editor, () -> getPositions(document).containsAll(expectedPositions));
	}

	default void waitForRemovalOfPositionsOfBacklistedTypes(final CognitiveEditor editor,
			final Class<?>... blacklistedTypes) {
		final IDocument document = getDocument(editor);
		waitForPositions(editor, () -> doNotHaveBlacklistedTypes(getPositions(document), asList(blacklistedTypes)));
	}

	default void waitForPositions(final CognitiveEditor editor, final Supplier<Boolean> predicate) {
		final Object waitee = new Object();
		final ListenerNotifyingOnAwaitedPositions listener = new ListenerNotifyingOnAwaitedPositions(waitee, editor,
				predicate);
		try {
			getDocument(editor).addDocumentListener(listener);
			if (!predicate.get()) {
				waitSecondsIfNotNotifiedBefore("positions", WAIT_FOR_CHANGE_TIMEOUT, predicate, waitee);
			}
		} finally {
			getDocument(editor).removeDocumentListener(listener);
		}
	}

	default long getNumberOfAnaphoraPositions(final CognitiveEditor editor) {
		final IDocument document = getDocument(editor);
		return getAnaphoraPositions(document).count();
	}

	default Stream<Position> getAnaphoraPositions(final IDocument document) {
		return getPositionsStream(document)
				.filter(position -> position instanceof AbstractAnaphoraPositionForRepresentation);
	}

	static List<? extends Position> getPositions(final IDocument document) {
		final List<Position> positions = getPositionsStream(document).collect(toList());
		return positions;
	}

	static Stream<Position> getPositionsStream(final IDocument document) {
		final Stream<Position> result = stream(document.getPositionCategories())
				// Filter out the _textviewer_selection_category\\d+ because it is added /
				// removed dynamically and thus yields a BadPositionCategoryException every once
				// in a while. See the TextViewer class.
				.filter(category -> !category.startsWith("_textviewer_selection_category"))
				.flatMap(category -> getPositions(document, category));
		return result;
	}

	static Stream<Position> getPositions(final IDocument document, final String category) {
		return stream(wrapBadPositionCategoryException(() -> document.getPositions(category)));
	}

	static IDocument getDocument(final CognitiveEditor editor) {
		return editor.getDocumentProvider().getDocument(editor.getEditorInput());
	}

	default <T extends AbstractAnaphoraPosition> void assertExpectedPositions(final IDocument document,
			final List<T> positions, final T... expectedPositions) throws Exception {
		assertExpectedPositions(document, positions, asList(expectedPositions));
	}

	default <T extends AbstractAnaphoraPosition> void assertGivenExpectedPositions(final IDocument document,
			final List<T> positions, final T... expectedPositions) throws Exception {
		assertGivenExpectedPositions(document, positions, asList(expectedPositions));
	}

	default <T extends AbstractAnaphoraPosition> void assertExpectedPositions(final IDocument document,
			final List<T> positions, final List<T> expectedPositions) throws Exception {
		assertThat(positions).containsExactlyElementsOf(expectedPositions);
		assertAnaphoraPositions(document, expectedPositions);
	}

	default <T extends AbstractAnaphoraPosition> void assertGivenExpectedPositions(final IDocument document,
			final List<T> positions, final List<T> expectedPositions) throws Exception {
		assertThat(positions).containsExactlyElementsOf(expectedPositions);
		assertGivenPositions(document, expectedPositions);
	}

	default void assertGivenPositions(final CognitiveEditor editor, final List<? extends Position> expectedPositions) {
		final IDocument document = getDocument(editor);
		assertGivenPositions((List<Position>) getPositions(document), expectedPositions);
	}

	default void assertPositionsDoNotHaveType(final CognitiveEditor editor, final Class<?>... blacklistedTypes) {
		final IDocument document = getDocument(editor);
		assertGivenPositions((List<Position>) getPositions(document), blacklistedTypes);
	}

	default <T extends AbstractAnaphoraPosition> void assertPositions(final IDocument document,
			final T... expectedPositions) throws Exception {
		assertPositions(document, asList(expectedPositions));
	}

	default <T extends AbstractAnaphoraPosition> void assertPositions(final IDocument document,
			final List<T> expectedPositions) throws Exception {
		assertPositions(document, ANAPHORA_CATEGORY, expectedPositions);
	}

	default <T extends Position> void assertPositions(final IDocument document, final String positionsCategory,
			final T... expectedPositions) throws Exception {
		assertPositions(document, positionsCategory, asList(expectedPositions));
	}

	default <T extends Position> void assertAnaphoraPositions(final IDocument document, final List<T> expectedPositions)
			throws BadPositionCategoryException {
		assertPositions(document, ANAPHORA_CATEGORY, expectedPositions);
	}

	default <T extends Position> void assertPositions(final IDocument document, final String positionsCategory,
			final List<T> expectedPositions) throws BadPositionCategoryException {
		final List<Position> positions = asList(document.getPositions(positionsCategory));
		assertPositions(positions, expectedPositions);
	}

	default <T extends AbstractAnaphoraPosition> void assertGivenPositions(final IDocument document,
			final List<T> expectedPositions) throws BadPositionCategoryException {
		final List<Position> positions = asList(document.getPositions(ANAPHORA_CATEGORY));
		assertGivenPositions(positions, expectedPositions);
	}

	default <T extends Position> void assertPositions(final List<Position> positions, final List<T> expectedPositions) {
		assertThat(positions).containsExactlyElementsOf(expectedPositions);
	}

	default <T extends Position> void assertGivenPositions(final List<Position> positions,
			final List<T> expectedPositions) {
		assertThat(positions).containsAll(expectedPositions);
		// The following lines are used because above assertion does not capture all
		// mismatches that positions.containsAll(expectedPositions) captures.
		if (!positions.containsAll(expectedPositions)) {
			final String positionsString = getPositionsString(positions);
			final String expectedPositionsString = getPositionsString(expectedPositions);
			assertThat(positions.containsAll(expectedPositions)).describedAs(
					"positions=" + positionsString + "\ncontains all expectedPositions=" + expectedPositionsString)
					.isTrue();
		}
	}

	default String getPositionsString(final List<? extends Position> positions) {
		return positions.stream().map(Object::toString).collect(joining("\n"));
	}

	default <T extends Position> void assertGivenPositions(final List<Position> positions,
			final Class<?>... blacklistedTypes) {
		stream(blacklistedTypes).forEach(blacklistedType -> assertThat(positions)
				.describedAs("position is not of blacklisted type %s", blacklistedType)
				.allMatch(position -> doesNotHaveBlacklistedTypes(position, asList(blacklistedType))));
	}

	default boolean doNotHaveBlacklistedTypes(final List<? extends Position> positions,
			final List<Class<?>> blacklistedTypes) {
		for (final Position position : positions) {
			if (!doesNotHaveBlacklistedTypes(position, blacklistedTypes)) {
				return false;
			}
		}
		return true;
	}

	default boolean doesNotHaveBlacklistedTypes(final Position position, final List<Class<?>> blacklistedTypes) {
		for (final Class<?> type : blacklistedTypes) {
			if (type.isInstance(position)) {
				return false;
			}
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	default <T extends IPositionUpdater> T getPositionUpdater(final Class<T> type, final IDocument document) {
		return stream(document.getPositionUpdaters()).filter(type::isInstance).map(updater -> (T) updater).findFirst()
				.orElseThrow(() -> new IllegalArgumentException("Found no " + type));
	}

	class ListenerNotifyingOnAwaitedPositions implements IDocumentListener {

		private final Object waitee;
		private final CognitiveEditor editor;
		private final Supplier<Boolean> predicate;

		public ListenerNotifyingOnAwaitedPositions(final Object waitee, final CognitiveEditor editor,
				final Supplier<Boolean> predicate) {
			this.waitee = waitee;
			this.editor = editor;
			this.predicate = predicate;
		}

		@Override
		public void documentAboutToBeChanged(final DocumentEvent event) {
			// Ignore this event
		}

		@Override
		public void documentChanged(final DocumentEvent event) {
			maybeNotifyListener();
		}

		private void maybeNotifyListener() {
			if (predicate.get()) {
				synchronized (waitee) {
					waitee.notifyAll();
				}
			}
		}

	}

}
