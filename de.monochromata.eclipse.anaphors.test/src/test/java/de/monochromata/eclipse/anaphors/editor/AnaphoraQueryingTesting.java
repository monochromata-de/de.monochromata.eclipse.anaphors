package de.monochromata.eclipse.anaphors.editor;

import static java.util.Arrays.asList;

import java.util.List;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;
import de.monochromata.eclipse.persp.PerspectivationPosition;

public interface AnaphoraQueryingTesting {

	default List<AbstractAnaphoraPositionForRepresentation> oneAnaphora(final int relatedExpressionStart,
			final int relatedExpressionLength, final int anaphorStart, final int anaphorLength) {
		return oneAnaphora(relatedExpressionStart, relatedExpressionLength, anaphorStart, anaphorLength, "anaphor");
	}

	default List<AbstractAnaphoraPositionForRepresentation> oneAnaphora(final int relatedExpressionStart,
			final int relatedExpressionLength, final int anaphorStart, final int anaphorLength, final String anaphor) {
		final PositionForRelatedExpression relatedExpressionPosition = relatedExpressionPosition(relatedExpressionStart,
				relatedExpressionLength);
		final PositionForAnaphor anaphorPosition = anaphorPosition(anaphorStart, anaphorLength, anaphor,
				relatedExpressionPosition);
		return asList(relatedExpressionPosition, anaphorPosition);
	}

	default PositionForRelatedExpression relatedExpressionPosition(final int offset, final int length) {
		return new PositionForRelatedExpression(offset, length, new PerspectivationPosition(offset, length));
	}

	default PositionForAnaphor anaphorPosition(final int offset, final int length, final String anaphor,
			final PositionForRelatedExpression relatedExpressionPosition) {
		final Anaphora anaphora = anaphora(anaphor);
		final PositionForAnaphor anaphorPosition = new PositionForAnaphor(offset, length,
				new PerspectivationPosition(offset, length), anaphora);
		link(relatedExpressionPosition, anaphorPosition);
		return anaphorPosition;
	}

	default void link(final PositionForRelatedExpression relatedExpressionPosition,
			final PositionForAnaphor anaphorPosition) {
		relatedExpressionPosition.addNext(anaphorPosition);
		anaphorPosition.setPrevious(relatedExpressionPosition);
	}

	default Anaphora anaphora(final String anaphor) {
		return new Anaphora("kindOfRelatedExpression-" + anaphor, "kindOfAnaphorResolution-" + anaphor,
				"kindOfReferentialization-" + anaphor, anaphor);
	}

}
