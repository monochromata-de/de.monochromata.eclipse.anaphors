package de.monochromata.eclipse.anaphors.editor.update;

public interface InvocationRejection {

    default <T, R> R ensureNotInvoked(
            final T unused) {
        throw new RuntimeException("Should not be invoked");
    }

    default <T0, T1, R> R ensureNotInvoked(final T0 unused0, final T1 unused1) {
        throw new RuntimeException("Should not be invoked");
    }

    default <T0, T1, T2, R> R ensureNotInvoked(final T0 unused0, final T1 unused1, final T2 unused2) {
        throw new RuntimeException("Should not be invoked");
    }

}
