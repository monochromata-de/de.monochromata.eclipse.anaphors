package de.monochromata.eclipse.anaphors.editor;

import static de.monochromata.eclipse.anaphors.editor.SelectionRelativeToAnaphoraPart.create;
import static org.assertj.core.api.Assertions.assertThat;

import org.eclipse.jdt.internal.corext.refactoring.change.ChangeTesting;
import org.eclipse.jface.text.Position;

public interface SelectionRelativeToAnaphoraPartsTesting extends ChangeTesting {

    default void assertSelectionRelativeToDefiniteExpression(final int selectionOffset, final int selectionLength,
            final int anaphoraPartOffset, final int anaphoraPartLength,
            final SelectionRelativeToAnaphoraPart.Relation expectedRelation,
            final int expectedDistanceBetweenSelectionAndAnaphoraPartOffset) {
        final Position position = new Position(anaphoraPartOffset, anaphoraPartLength);
        final SelectionRelativeToAnaphoraPart selection = create(selectionOffset, selectionLength,
                position);

        assertThat(selection.relationBetweenSelectionAndAnaphoraPart).isEqualTo(expectedRelation);
        assertThat(selection.distanceBetweenSelectionAndAnaphoraPart)
                .isEqualTo(expectedDistanceBetweenSelectionAndAnaphoraPartOffset);
    }

}
