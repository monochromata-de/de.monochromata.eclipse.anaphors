package de.monochromata.eclipse.anaphors.editor.interactive;

import static java.util.Arrays.asList;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class EnterPD_DA1Re_RnTest extends AbstractInteractiveEditorTest {

	@Override
	protected void editFile(final IEditorPart editor) {
		getBot().activeEditor().toTextEditor().insertText(5, 21, "Args");
		getBot().activeEditor().save();
	}

	@Override
	public String getExpectedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("public class EnterPD_DA1Re_Rn {");
		builder.addLine("");
		builder.addLine("	public static void main(String[] args) {");
		builder.addLine("		System.err.println(args);");
		builder.addLine("	}");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	public String getExpectedUnderspecifiedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("public class EnterPD_DA1Re_Rn {");
		builder.addLine("");
		builder.addLine("	public static void main(String[] args) {");
		builder.addLine("		System.err.println(args);");
		builder.addLine("	}");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
		return asList(
				new ImmutablePair<>(
						new Annotation("org.eclipse.jdt.ui.occurrences", false, "Anaphor co-referent to args"),
						new Position(158, 4)),
				new ImmutablePair<>(new Annotation("org.eclipse.jdt.ui.occurrences.write", false, "Related expression"),
						new Position(120, 13)));
	}

	@Override
	protected List<? extends Position> getExpectedPositions() {
		final Anaphora anaphora = new Anaphora("PD", "DA1Re", "Rn", "args");
		return asList(new PositionForAnaphor(158, 4, anaphora), new PositionForRelatedExpression(120, 13));
	}

}
