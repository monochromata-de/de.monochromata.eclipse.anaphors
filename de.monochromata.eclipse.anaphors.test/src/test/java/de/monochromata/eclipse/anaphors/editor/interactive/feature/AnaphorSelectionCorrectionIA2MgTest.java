package de.monochromata.eclipse.anaphors.editor.interactive.feature;

import static java.util.Arrays.asList;

import java.util.List;

import org.eclipse.jface.text.Position;
import org.junit.Ignore;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

@Ignore
public class AnaphorSelectionCorrectionIA2MgTest extends AbstractAnaphorSelectionCorrectionTest
        implements SelectionUpdateTesting {

    @Override
    protected String getExpectedKindOfResolvedAnaphora() {
        return "LVD-IA2Mg-Rn";
    }

    @Override
    protected void editFile(final ProblemCollectingChangeListener problemCollector, final CognitiveEditor editor) {
        assertCorrectAnaphorResolution(problemCollector, editor, 1);
        assertReplacementsAndSelectionMovements(" jTextArea = new JTextArea();", 218, 2, problemCollector, editor);
        assertReplacementsAndSelectionMovements(" lineCount);", 31, 289, problemCollector, editor);
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature;");
        builder.addLine("");
        builder.addLine("import javax.swing.JTextArea;");
        builder.addLine("");
        builder.addLine("public class AnaphorSelectionCorrectionIA2Mg {");
        builder.addLine("");
        builder.addLine("    public static void main(String[] args) {");
        builder.addLine("        final JTextArea jTextArea = new JTextArea();");
        builder.addLine("        jTextArea.setText(\"line count=\" + jTextArea.getLineCount());");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature;");
        builder.addLine("");
        builder.addLine("import javax.swing.JTextArea;");
        builder.addLine("");
        builder.addLine("public class AnaphorSelectionCorrectionIA2Mg {");
        builder.addLine("");
        builder.addLine("    public static void main(String[] args) {");
        builder.addLine("        final JTextArea jTextArea = new JTextArea();");
        builder.addLine("        jTextArea.setText(\"line count=\" + lineCount);");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected List<? extends Position> getExpectedPositions() {
        final Anaphora anaphora = new Anaphora("LVD", "IA2Mg", "Rn", "lineCount");
        final PositionForRelatedExpression relatedExpression = new PositionForRelatedExpression(202, 44);
        final PositionForAnaphor anaphor = new PositionForAnaphor(289, 24, anaphora);
        return asList(anaphor, relatedExpression);
    }

}
