package de.monochromata.eclipse.anaphors.editor;

import static de.monochromata.eclipse.anaphors.editor.AnaphoraQuerying.getAnaphoraAt;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.stream.Stream;

import org.junit.Test;

import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class AnaphoraQueryingTest implements AnaphoraQueryingTesting {

    @Test
    public void anaphoraAfterRangeIsNotFound() {
        assertThat(getAnaphoraAt(0, 0, oneAnaphora(10, 5, 20, 3).stream())).isEmpty();
    }

    @Test
    public void anaphoraBeforeRangeIsNotFound() {
        assertThat(getAnaphoraAt(30, 0, oneAnaphora(10, 5, 20, 3).stream())).isEmpty();
    }

    @Test
    public void anaphorStartingAtSelectionIsFound() {
        final List<AbstractAnaphoraPositionForRepresentation> positions = oneAnaphora(10, 5, 20, 3);
        assertThat(getAnaphoraAt(20, 0, positions.stream())).containsExactlyElementsOf(positions);
    }

    @Test
    public void anaphorCoveringSelectionIsFound() {
        final List<AbstractAnaphoraPositionForRepresentation> positions = oneAnaphora(10, 5, 20, 3);
        assertThat(getAnaphoraAt(19, 3, positions.stream())).containsExactlyElementsOf(positions);
    }

    @Test
    public void anaphorEndingAtZeroLengthSelectionIsFound() {
        final List<AbstractAnaphoraPositionForRepresentation> positions = oneAnaphora(10, 5, 20, 3);
        assertThat(getAnaphoraAt(23, 0, positions.stream())).containsExactlyElementsOf(positions);
    }

    @Test
    public void relatedExpressionStartingAtSelectionIsFound() {
        final List<AbstractAnaphoraPositionForRepresentation> positions = oneAnaphora(10, 5, 20, 3);
        assertThat(getAnaphoraAt(10, 0, positions.stream())).containsExactlyElementsOf(positions);
    }

    @Test
    public void relatedExpressionCoveringSelectionIsFound() {
        final List<AbstractAnaphoraPositionForRepresentation> positions = oneAnaphora(10, 5, 20, 3);
        assertThat(getAnaphoraAt(12, 2, positions.stream())).containsExactlyElementsOf(positions);
    }

    @Test
    public void relatedExpressionEndingAtZeroLengthSelectionIsFound() {
        final List<AbstractAnaphoraPositionForRepresentation> positions = oneAnaphora(10, 5, 20, 3);
        assertThat(getAnaphoraAt(15, 0, positions.stream())).containsExactlyElementsOf(positions);
    }

    @Test
    public void cursorOnRelatedExpressionWith2AnaphorsYieldsAllThree() {
        final PositionForRelatedExpression relatedExpression = relatedExpressionPosition(0, 2);
        final PositionForAnaphor anaphor1 = anaphorPosition(10, 2, "anaphora1", relatedExpression);
        final PositionForAnaphor anaphor2 = anaphorPosition(20, 2, "anaphora2", relatedExpression);

        assertThat(getAnaphoraAt(1, 0, Stream.of(relatedExpression, anaphor1, anaphor2)))
                .containsExactly(relatedExpression, anaphor1, anaphor2);
    }

    @Test
    public void cursorOnAnaphorOfRelatedExpressionWith2AnaphorsYieldsAllThree() {
        final PositionForRelatedExpression relatedExpression = relatedExpressionPosition(0, 2);
        final PositionForAnaphor anaphor1 = anaphorPosition(10, 2, "anaphora1", relatedExpression);
        final PositionForAnaphor anaphor2 = anaphorPosition(20, 2, "anaphora2", relatedExpression);

        assertThat(getAnaphoraAt(10, 0, Stream.of(relatedExpression, anaphor1, anaphor2)))
                .containsExactly(relatedExpression, anaphor1, anaphor2);
    }

    @Test
    public void cursorOnRelatedExpressionOfFirstOfTwoChainsYieldsFirstChain() {
        final PositionForRelatedExpression relatedExpression1 = relatedExpressionPosition(0, 2);
        final PositionForAnaphor anaphor11 = anaphorPosition(10, 2, "anaphora1.1", relatedExpression1);
        final PositionForAnaphor anaphor12 = anaphorPosition(20, 2, "anaphora1.2", relatedExpression1);

        final PositionForRelatedExpression relatedExpression2 = relatedExpressionPosition(20, 2);
        final PositionForAnaphor anaphor21 = anaphorPosition(30, 2, "anaphora2.1", relatedExpression2);
        final PositionForAnaphor anaphor22 = anaphorPosition(40, 2, "anaphora2.2", relatedExpression2);

        assertThat(getAnaphoraAt(0, 0, Stream.of(relatedExpression1, anaphor11, anaphor12,
                relatedExpression2, anaphor21, anaphor22)))
                        .containsExactly(relatedExpression1, anaphor11, anaphor12);
    }

    @Test
    public void cursorOnAnaphorOfFirstOfTwoChainsYieldsFirstChain() {
        final PositionForRelatedExpression relatedExpression1 = relatedExpressionPosition(0, 2);
        final PositionForAnaphor anaphor11 = anaphorPosition(10, 2, "anaphora1.1", relatedExpression1);
        final PositionForAnaphor anaphor12 = anaphorPosition(20, 2, "anaphora1.2", relatedExpression1);

        final PositionForRelatedExpression relatedExpression2 = relatedExpressionPosition(20, 2);
        final PositionForAnaphor anaphor21 = anaphorPosition(30, 2, "anaphora2.1", relatedExpression2);
        final PositionForAnaphor anaphor22 = anaphorPosition(40, 2, "anaphora2.2", relatedExpression2);

        assertThat(getAnaphoraAt(10, 0, Stream.of(relatedExpression1, anaphor11, anaphor12,
                relatedExpression2, anaphor21, anaphor22)))
                        .containsExactly(relatedExpression1, anaphor11, anaphor12);
    }

    @Test
    public void cursorOnElementConnectingTwoChainsYieldsBothChains() {
        final PositionForRelatedExpression relatedExpression1 = relatedExpressionPosition(0, 2);
        final PositionForAnaphor anaphor11 = anaphorPosition(10, 2, "anaphora1.1", relatedExpression1);
        final PositionForAnaphor anaphor12 = anaphorPosition(20, 2, "anaphora1.2", relatedExpression1);

        final PositionForRelatedExpression relatedExpression2 = relatedExpressionPosition(20, 2);
        final PositionForAnaphor anaphor21 = anaphorPosition(30, 2, "anaphora2.1", relatedExpression2);
        final PositionForAnaphor anaphor22 = anaphorPosition(40, 2, "anaphora2.2", relatedExpression2);

        assertThat(getAnaphoraAt(20, 0, Stream.of(relatedExpression1, anaphor11, anaphor12,
                relatedExpression2, anaphor21, anaphor22)).collect(toList())).containsExactlyInAnyOrder(
                        relatedExpression1, anaphor11, anaphor12, relatedExpression2,
                        anaphor21, anaphor22);
    }

    @Test
    public void cursorOnAnaphorInSecondOfTwoChainsYieldsSecondChainOnly() {
        final PositionForRelatedExpression relatedExpression1 = relatedExpressionPosition(0, 2);
        final PositionForAnaphor anaphor11 = anaphorPosition(10, 2, "anaphora1.1", relatedExpression1);
        final PositionForAnaphor anaphor12 = anaphorPosition(20, 2, "anaphora1.2", relatedExpression1);

        final PositionForRelatedExpression relatedExpression2 = relatedExpressionPosition(20, 2);
        final PositionForAnaphor anaphor21 = anaphorPosition(30, 2, "anaphora2.1", relatedExpression2);
        final PositionForAnaphor anaphor22 = anaphorPosition(40, 2, "anaphora2.2", relatedExpression2);

        assertThat(getAnaphoraAt(30, 0, Stream.of(relatedExpression1, anaphor11, anaphor12,
                relatedExpression2, anaphor21, anaphor22)))
                        .containsExactly(relatedExpression2, anaphor21, anaphor22);
    }

}
