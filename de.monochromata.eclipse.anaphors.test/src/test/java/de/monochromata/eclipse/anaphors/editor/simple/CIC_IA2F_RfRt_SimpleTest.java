package de.monochromata.eclipse.anaphors.editor.simple;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;

public class CIC_IA2F_RfRt_SimpleTest extends AbstractSimpleEditorTest {

	@Override
	public String getExpectedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
		builder.addLine("");
		builder.addLine("public class CIC_IA2F_RfRt_Simple {");
		builder.addLine("");
		builder.addLine("	public static void main(String[] args) {");
		builder.addLine("		final A a = new A(\"foo\");");
		builder.addLine("		System.err.println(a.int0);");
		builder.addLine("	}");
		builder.addLine("");
		builder.addLine("	private static class A {");
		builder.addLine("		public Integer int0;");
		builder.addLine("		");
		builder.addLine("		public A(final String input) {}");
		builder.addLine("	}");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	public String getExpectedUnderspecifiedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
		builder.addLine("");
		builder.addLine("public class CIC_IA2F_RfRt_Simple {");
		builder.addLine("");
		builder.addLine("	public static void main(String[] args) {");
		builder.addLine("		new A(\"foo\");");
		builder.addLine("		System.err.println(int0);");
		builder.addLine("	}");
		builder.addLine("");
		builder.addLine("	private static class A {");
		builder.addLine("		public Integer int0;");
		builder.addLine("		");
		builder.addLine("		public A(final String input) {}");
		builder.addLine("	}");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected Pair<Anaphora, Anaphora> getExpectedResolvedAndRealizedAnaphora() {
		return new ImmutablePair<>(new Anaphora("CIC", "IA2F", "RfRt", "fooInteger"),
				new Anaphora("LVD", "IA2F", "Rn", "int0"));
	}

}
