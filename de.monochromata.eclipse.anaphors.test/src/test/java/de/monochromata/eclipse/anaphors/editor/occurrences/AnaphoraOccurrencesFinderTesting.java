package de.monochromata.eclipse.anaphors.editor.occurrences;

import static org.assertj.core.api.Assertions.assertThat;

import org.eclipse.jdt.internal.core.manipulation.search.IOccurrencesFinder.OccurrenceLocation;

import de.monochromata.eclipse.anaphors.editor.AnaphoraQueryingTesting;

public interface AnaphoraOccurrencesFinderTesting extends AnaphoraQueryingTesting {

    default void assertOccurrence(final OccurrenceLocation occurrenceLocation, final int offset,
            final int length, final int flags, final String description) {
        assertThat(occurrenceLocation.getOffset()).isEqualTo(offset);
        assertThat(occurrenceLocation.getLength()).isEqualTo(length);
        assertThat(occurrenceLocation.getFlags()).isEqualTo(flags);
        assertThat(occurrenceLocation.getDescription()).isEqualTo(description);
    }

}
