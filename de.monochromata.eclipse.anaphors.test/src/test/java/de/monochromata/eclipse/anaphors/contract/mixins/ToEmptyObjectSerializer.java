package de.monochromata.eclipse.anaphors.contract.mixins;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;

public class ToEmptyObjectSerializer extends JsonSerializer<Object> {

    @Override
    public void serializeWithType(final Object value, final JsonGenerator generator,
            final SerializerProvider serializers, final TypeSerializer typeSerializer) throws IOException {
        serialize(value, generator, serializers);
    }

    @Override
    public void serialize(final Object value, final JsonGenerator generator, final SerializerProvider serializers)
            throws IOException {
        generator.writeStartObject(value, 0);
        generator.writeEndObject();
    }
}