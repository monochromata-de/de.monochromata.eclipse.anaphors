package de.monochromata.eclipse.anaphors.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import de.monochromata.eclipse.anaphors.ast.reference.strategy.concept.HyponymyIntegrationTest;
import de.monochromata.eclipse.anaphors.editor.simple.CIC_DA1Re_Hy_SimpleTest;

/**
 * Needs to be run as a JUnit Plug-in Test, but not in the UI thread.
 */
@RunWith(Suite.class)
@SuiteClasses({ HyponymyIntegrationTest.class, CIC_DA1Re_Hy_SimpleTest.class })

public class ContractTests {

}
