package de.monochromata.eclipse.anaphors.annotation;

import static de.monochromata.eclipse.anaphors.annotation.AnnotationQuerying.getAnnotationModel;
import static de.monochromata.eclipse.anaphors.annotation.AnnotationQuerying.getAnnotationsFromEntireDocument;
import static de.monochromata.eclipse.anaphors.annotation.AnnotationsSerialization.toStrings;
import static java.util.Arrays.asList;
import static java.util.Collections.disjoint;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.jdt.ui.SharedASTProvider.WAIT_ACTIVE_ONLY;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jdt.ui.SharedASTProvider;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.text.source.IAnnotationModelListener;

import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.editor.WaitingDuringTesting;

public interface AnnotationsTesting extends WaitingDuringTesting {

    default void waitForSharedASTToBeReconciled(final CognitiveEditor editor) {
        final ITypeRoot typeRoot = JavaUI.getEditorInputTypeRoot(editor.getEditorInput());
        SharedASTProvider.getAST(typeRoot, WAIT_ACTIVE_ONLY, null);
    }

    default void waitForAllAnnotations(final CognitiveEditor editor,
            final List<Pair<? extends Annotation, ? extends Position>> expectedAnnotations) {
        waitForAnnotations(editor, containsExactlyExpectedAnnotations(expectedAnnotations));
    }

    default void waitForExpectedAnnotations(final CognitiveEditor editor,
            final List<Pair<? extends Annotation, ? extends Position>> expectedAnnotations) {
        waitForAnnotations(editor, expectedAnnotationsAvailable(expectedAnnotations));
    }

    default void waitForRemovalOfAnnotationsOfBacklistedTypes(final CognitiveEditor editor,
            final String... blacklistedTypes) {
        waitForAnnotations(editor, doNotHaveBlacklistedTypes(asList(blacklistedTypes)));
    }

    default void waitForAnnotations(final CognitiveEditor editor,
            final Predicate<IAnnotationModel> predicate) {
        final Supplier<Boolean> condition = () -> predicate.test(getAnnotationModel(editor));
        final Object waitee = new Object();
        final AnnotationListenerThatAbortsWaitingWhenExpectedAnnotationsArePresent listener = new AnnotationListenerThatAbortsWaitingWhenExpectedAnnotationsArePresent(
                predicate, waitee);
        getAnnotationModel(editor).addAnnotationModelListener(listener);
        if (!condition.get()) {
            waitSecondsIfNotNotifiedBefore("expected annotations", WAIT_FOR_CHANGE_TIMEOUT, condition, waitee);
        }
        getAnnotationModel(editor).removeAnnotationModelListener(listener);
    }

    default void assertAnnotations(final CognitiveEditor editor,
            final List<Pair<? extends Annotation, ? extends Position>> expectedAnnotations) {
        final List<Pair<? extends Annotation, ? extends Position>> actualAnnotations = getAnnotationsFromEntireDocument(
                editor).collect(toList());
        assertAnnotations(actualAnnotations, expectedAnnotations);
    }

    default void assertAnnotations(final IAnnotationModel annotationModel,
            final List<Pair<? extends Annotation, ? extends Position>> expectedAnnotations) {
        final List<Pair<? extends Annotation, ? extends Position>> actualAnnotations = getAnnotationsFromEntireDocument(
                annotationModel).collect(toList());
        assertAnnotations(actualAnnotations, expectedAnnotations);
    }

    default void assertAnnotations(final List<Pair<? extends Annotation, ? extends Position>> actualAnnotations,
            final List<Pair<? extends Annotation, ? extends Position>> expectedAnnotations) {
        assertThat(toStrings(actualAnnotations)).containsAll(toStrings(expectedAnnotations));
    }

    default void assertAllAnnotations(final CognitiveEditor editor,
            final List<Pair<? extends Annotation, ? extends Position>> expectedAnnotations) {
        final List<Pair<? extends Annotation, ? extends Position>> actualAnnotations = getAnnotationsFromEntireDocument(
                editor).collect(toList());
        assertAllAnnotations(actualAnnotations, expectedAnnotations);
    }

    default void assertAllAnnotations(final IAnnotationModel annotationModel,
            final List<Pair<? extends Annotation, ? extends Position>> expectedAnnotations) {
        final List<Pair<? extends Annotation, ? extends Position>> actualAnnotations = getAnnotationsFromEntireDocument(
                annotationModel).collect(toList());
        assertAllAnnotations(actualAnnotations, expectedAnnotations);
    }

    default void assertAllAnnotations(final List<Pair<? extends Annotation, ? extends Position>> actualAnnotations,
            final List<Pair<? extends Annotation, ? extends Position>> expectedAnnotations) {
        assertThat(toStrings(actualAnnotations)).containsExactlyElementsOf(toStrings(expectedAnnotations));
    }

    default void assertAnnotationsDoNotHaveTypes(final CognitiveEditor editor,
            final String... blacklistedTypes) {
        final Stream<String> actualTypes = getAnnotationsFromEntireDocument(editor)
                .map(Pair::getLeft)
                .map(Annotation::getType)
                .distinct();
        assertThat(actualTypes).doesNotContain(blacklistedTypes);
    }

    static Annotation markDeleted(final Annotation annotation) {
        annotation.markDeleted(true);
        return annotation;
    }

    static Predicate<IAnnotationModel> expectedAnnotationsAvailable(
            final List<Pair<? extends Annotation, ? extends Position>> expectedAnnotations) {
        return model -> {
            final List<String> actualAnnotations = getActualAnnotations(model);
            return actualAnnotations.containsAll(toStrings(expectedAnnotations));
        };
    }

    static Predicate<IAnnotationModel> containsExactlyExpectedAnnotations(
            final List<Pair<? extends Annotation, ? extends Position>> expectedAnnotations) {
        return model -> {
            final List<String> actualAnnotations = getActualAnnotations(model);
            return actualAnnotations.equals(toStrings(expectedAnnotations));
        };
    }

    static Predicate<IAnnotationModel> doNotHaveBlacklistedTypes(final List<String> blacklistedTypes) {
        return model -> {
            final Iterator<Annotation> iterator = model.getAnnotationIterator();
            final Set<String> actualTypes = new HashSet<>();
            while (iterator.hasNext()) {
                actualTypes.add(iterator.next().getType());
            }
            return disjoint(actualTypes, blacklistedTypes);
        };
    }

    static List<String> getActualAnnotations(final IAnnotationModel model) {
        final List<Pair<? extends Annotation, ? extends Position>> annotations = getAnnotationsFromEntireDocument(
                model).collect(toList());
        final List<String> actualAnnotations = toStrings(annotations);
        return actualAnnotations;
    }

    class AnnotationListenerThatAbortsWaitingWhenExpectedAnnotationsArePresent implements IAnnotationModelListener {

        private final Predicate<IAnnotationModel> condition;
        private final Object waitee;

        public AnnotationListenerThatAbortsWaitingWhenExpectedAnnotationsArePresent(
                final Predicate<IAnnotationModel> condition,
                final Object waitee) {
            this.condition = condition;
            this.waitee = waitee;
        }

        @Override
        public void modelChanged(final IAnnotationModel model) {
            if (condition.test(model)) {
                notifyWaitee();
            }
        }

        protected void notifyWaitee() {
            synchronized (waitee) {
                waitee.notifyAll();
            }
        }

    }

}
