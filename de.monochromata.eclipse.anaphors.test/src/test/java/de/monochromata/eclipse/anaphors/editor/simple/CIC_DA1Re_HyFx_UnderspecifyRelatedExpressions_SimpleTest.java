package de.monochromata.eclipse.anaphors.editor.simple;

import static de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy.underspecifyRelatedExpressions;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;

public class CIC_DA1Re_HyFx_UnderspecifyRelatedExpressions_SimpleTest extends AbstractSimpleEditorTest {

    @Override
    protected PerspectivationStrategy getPerspectivationStrategy() {
        return underspecifyRelatedExpressions();
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
        builder.addLine("");
        builder.addLine("public class CIC_DA1Re_HyFx_UnderspecifyRelatedExpressions_Simple {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		final MetallSchwert schwert = new MetallSchwert();");
        builder.addLine("		System.err.println(schwert);");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("	private static class MetallSchwert {");
        builder.addLine("");
        builder.addLine("	}");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
        builder.addLine("");
        builder.addLine("public class CIC_DA1Re_HyFx_UnderspecifyRelatedExpressions_Simple {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		new MetallSchwert();");
        builder.addLine("		System.err.println(schwert);");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("	private static class MetallSchwert {");
        builder.addLine("");
        builder.addLine("	}");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected Pair<Anaphora, Anaphora> getExpectedResolvedAndRealizedAnaphora() {
        return new ImmutablePair<>(new Anaphora("CIC", "DA1Re", "HyFx", "schwert"),
                new Anaphora("LVD", "DA1Re", "Rn", "schwert"));
    }

}
