package de.monochromata.eclipse.anaphors.editor.update;

import static de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPosition.MEMBER_TRACKING_CATEGORY;
import static de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPositionCreation.createMemberTrackingPosition;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadPositionCategoryException;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapPositionExceptions;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.jdt.core.ElementChangedEvent.POST_CHANGE;
import static org.eclipse.jdt.core.ElementChangedEvent.POST_RECONCILE;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.junit.Test;

import de.monochromata.eclipse.anaphors.editor.AnaphoraDocumentProvider;

public class AnaphoraResolvingElementListenerTest
        implements AnaphoraResolvingElementListenerTesting {

    // Initialization

    @Test
    public void memberPositionsAreCreatedAndAddedForAllMembersOnFirstPostReconcile() throws Exception {
        assertDocument("class Foo { String foo = \"baroeo\"; int bar() { return -1; } }",
                (originDocument, compilationUnit) -> wrapBadPositionCategoryException(() -> {
                    final IType type = (IType) ((ICompilationUnit) compilationUnit.getJavaElement())
                            .getChildren()[0];
                    final IField foo = (IField) type.getChildren()[0];
                    final IMethod bar = (IMethod) type.getChildren()[1];

                    final AnaphoraResolvingElementListener testee = createTesteeThatDoesNotCreateJobs(originDocument,
                            (ICompilationUnit) compilationUnit.getJavaElement());

                    sendPostReconcile(testee, compilationUnit);

                    final List<Position> trackingPositions = asList(
                            originDocument.getPositions(MEMBER_TRACKING_CATEGORY));
                    assertThat(trackingPositions).containsExactly(
                            new MemberTrackingPosition(12, 22, foo),
                            new MemberTrackingPosition(35, 24, bar));
                }));
    }

    @Test
    public void noNewMemberPositionsAreCreatedOrAddedOnSubsequentPostReconcile() throws Exception {
        assertDocument("class Foo { String foo = \"baroeo\"; int bar() { return -1; } }",
                (originDocument, compilationUnit) -> wrapBadPositionCategoryException(() -> {
                    final AnaphoraResolvingElementListener testee = createTesteeThatDoesNotCreateJobs(originDocument,
                            (ICompilationUnit) compilationUnit.getJavaElement());
                    sendPostReconcile(testee, compilationUnit);
                    final List<Position> initialTrackingPositions = asList(
                            originDocument.getPositions(MEMBER_TRACKING_CATEGORY));

                    sendPostReconcile(testee, compilationUnit);

                    final List<Position> subsequentTrackingPositions = asList(
                            originDocument.getPositions(MEMBER_TRACKING_CATEGORY));
                    assertThat(subsequentTrackingPositions)
                            .containsExactlyElementsOf(initialTrackingPositions);
                }));
    }

    @Test
    public void createAndAddOtherMemberPositionsOnFirstPostReconcileForOtherCompilationUnit() throws Exception {
        final AtomicReference<IDocument> originDocumentReference = new AtomicReference<>();
        final AtomicReference<ICompilationUnit> compilationUnitReference = new AtomicReference<>();
        final AnaphoraResolvingElementListener testee = createTesteeThatDoesNotCreateJobs(originDocumentReference::get,
                compilationUnitReference::get);

        assertDocument("Foo", "class Foo { String foo = \"baroeo\"; }",
                (originDocument, compilationUnit) -> wrapBadPositionCategoryException(() -> {
                    AnaphoraDocumentProvider.configure(originDocument, unused -> {
                    });
                    originDocumentReference.set(originDocument);
                    compilationUnitReference.set((ICompilationUnit) compilationUnit.getJavaElement());

                    final IType type = (IType) ((ICompilationUnit) compilationUnit.getJavaElement())
                            .getChildren()[0];
                    final IField foo = (IField) type.getChildren()[0];

                    sendPostReconcile(testee, compilationUnit);

                    final List<Position> trackingPositions = asList(
                            originDocument.getPositions(MEMBER_TRACKING_CATEGORY));
                    assertThat(trackingPositions).containsExactly(
                            new MemberTrackingPosition(12, 22, foo));
                }));

        assertDocument("Bar", "class Bar { int bar() { return -1; } }",
                (originDocument, compilationUnit) -> wrapBadPositionCategoryException(() -> {
                    AnaphoraDocumentProvider.configure(originDocument, unused -> {
                    });
                    originDocumentReference.set(originDocument);
                    compilationUnitReference.set((ICompilationUnit) compilationUnit.getJavaElement());

                    final IType type = (IType) ((ICompilationUnit) compilationUnit.getJavaElement())
                            .getChildren()[0];
                    final IMethod bar = (IMethod) type.getChildren()[0];

                    sendPostReconcile(testee, compilationUnit);

                    final List<Position> trackingPositions = asList(
                            originDocument.getPositions(MEMBER_TRACKING_CATEGORY));
                    assertThat(trackingPositions).containsExactly(
                            new MemberTrackingPosition(12, 24, bar));
                }));
    }

    /**
     * This is to make sure that old, outdated {@link MemberPosition}s do not
     * interfere with the new ones.
     */
    @Test
    public void removeExistingMemberPositionsOnFirstPostReconcileForACompilationUnit() throws Exception {
        assertDocument("class Foo { String foo = \"baroeo\"; int bar() { return -1; } }",
                (originDocument, compilationUnit) -> wrapPositionExceptions(() -> {
                    try {
                        final IType type = (IType) ((ICompilationUnit) compilationUnit.getJavaElement())
                                .getChildren()[0];
                        final IField foo = (IField) type.getChildren()[0];
                        final IMethod bar = (IMethod) type.getChildren()[1];

                        final AnaphoraResolvingElementListener testee = createTesteeThatDoesNotCreateJobs(
                                originDocument,
                                (ICompilationUnit) compilationUnit.getJavaElement());

                        final MemberTrackingPosition positionToBeRemoved = new MemberTrackingPosition(0, 1, null);
                        originDocument.addPosition(MEMBER_TRACKING_CATEGORY, positionToBeRemoved);

                        sendPostReconcile(testee, compilationUnit);

                        final List<Position> trackingPositions = asList(
                                originDocument.getPositions(MEMBER_TRACKING_CATEGORY));
                        assertThat(trackingPositions).containsExactly(
                                new MemberTrackingPosition(12, 22, foo),
                                new MemberTrackingPosition(35, 24, bar));
                    } catch (final JavaModelException e) {
                        throw new RuntimeException(e);
                    }
                }));
    }

    // Additions

    @Test
    public void anAddedChildMemberHasAPositionAddedToTheDocumentPostChange() throws Exception {
        anAddedChildMemberHasAPositionAddedToTheDocument(POST_CHANGE);
    }

    @Test
    public void anAddedChildMemberHasAPositionAddedToTheDocumentPostReconcile() throws Exception {
        anAddedChildMemberHasAPositionAddedToTheDocument(POST_RECONCILE);
    }

    protected void anAddedChildMemberHasAPositionAddedToTheDocument(final int eventType) throws Exception {
        assertAddChildMember(eventType, (originDocument, compilationUnit) -> {
            final IField foo = getFieldFoo(compilationUnit);
            final List<Position> trackingPositions = getTrackingPositions(originDocument);
            assertThat(trackingPositions).containsExactly(
                    new MemberTrackingPosition(12, 22, foo));
        }, this::noop);
    }

    @Test
    public void anAddedChildMemberHasItsPositionAddedToTheQueuePostChange() throws Exception {
        anAddedChildMemberHasItsPositionAddedToTheQueue(POST_CHANGE);
    }

    @Test
    public void anAddedChildMemberHasItsPositionAddedToTheQueuePostReconcile() throws Exception {
        anAddedChildMemberHasItsPositionAddedToTheQueue(POST_RECONCILE);
    }

    protected void anAddedChildMemberHasItsPositionAddedToTheQueue(final int eventType) throws Exception {
        assertAddChildMember(eventType, this::noop, (testee, compilationUnit) -> {
            final IField foo = getFieldFoo(compilationUnit);
            final MemberTrackingPosition trackingPosition = createMemberTrackingPosition(foo)
                    .orElseThrow(NullPointerException::new);
            final List<MemberTrackingPosition> membersToReResolve = getTrackingPositionsToReResolve(testee);
            assertThat(membersToReResolve).containsExactly(trackingPosition);
        });
    }

    // Removals

    @Test
    public void aRemovedChildMemberHasItsPositionRemovedFromTheDocumentPostChange() throws Exception {
        aRemovedChildMemberHasItsPositionRemovedFromTheDocument(POST_CHANGE);
    }

    @Test
    public void aRemovedChildMemberHasItsPositionRemovedFromTheDocumentPostReconcile() throws Exception {
        aRemovedChildMemberHasItsPositionRemovedFromTheDocument(POST_RECONCILE);
    }

    protected void aRemovedChildMemberHasItsPositionRemovedFromTheDocument(final int eventType) throws Exception {
        assertRemoveChildMember(eventType, this::addMemberPositionForFooField, this::noop,
                (originDocument, compilationUnit) -> assertThat(getTrackingPositions(originDocument)).isEmpty(),
                this::noop);
    }

    @Test
    public void nothingHappensIfARemovedChildMembersPositionIsNotContainedInTheDocumentAnymorePostChange()
            throws Exception {
        nothingHappensIfARemovedChildMembersPositionIsNotContainedInTheDocumentAnymore(POST_CHANGE);
    }

    @Test
    public void nothingHappensIfARemovedChildMembersPositionIsNotContainedInTheDocumentAnymorePostReconcile()
            throws Exception {
        nothingHappensIfARemovedChildMembersPositionIsNotContainedInTheDocumentAnymore(POST_RECONCILE);
    }

    protected void nothingHappensIfARemovedChildMembersPositionIsNotContainedInTheDocumentAnymore(final int eventType)
            throws Exception {
        assertRemoveChildMember(eventType, this::noop, this::noop,
                (originDocument, compilationUnit) -> assertThat(getTrackingPositions(originDocument)).isEmpty(),
                this::noop);
    }

    @Test
    public void aRemovedChildMemberHasItsPositionRemovedFromTheQueuePostChange() throws Exception {
        aRemovedChildMemberHasItsPositionRemovedFromTheQueue(POST_CHANGE);
    }

    @Test
    public void aRemovedChildMemberHasItsPositionRemovedFromTheQueuePostReconcile() throws Exception {
        aRemovedChildMemberHasItsPositionRemovedFromTheQueue(POST_RECONCILE);
    }

    protected void aRemovedChildMemberHasItsPositionRemovedFromTheQueue(final int eventType) throws Exception {
        assertRemoveChildMember(eventType, this::noop, this::addFieldFooToMembersToReResolve, this::noop,
                (testee, compilationUnit) -> assertThat(getTrackingPositionsToReResolve(testee)).isEmpty());
    }

    @Test
    public void nothingHappensIfARemovedChildMembersPositionIsNotEnqueuedPostChange() throws Exception {
        nothingHappensIfARemovedChildMembersPositionIsNotEnqueued(POST_CHANGE);
    }

    @Test
    public void nothingHappensIfARemovedChildMembersPositionIsNotEnqueuedPostReconcile() throws Exception {
        nothingHappensIfARemovedChildMembersPositionIsNotEnqueued(POST_RECONCILE);
    }

    protected void nothingHappensIfARemovedChildMembersPositionIsNotEnqueued(final int eventType) throws Exception {
        assertRemoveChildMember(eventType, this::noop, this::noop, this::noop,
                (testee, compilationUnit) -> assertThat(getTrackingPositionsToReResolve(testee)).isEmpty());
    }

    // Replacements are combinations of additions and removals and hence not tested
    // individually.

}
