package de.monochromata.eclipse.anaphors.position;

import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static de.monochromata.eclipse.anaphors.position.AnaphoraPositionCreation.addPosition;
import static de.monochromata.eclipse.position.DoublyLinkedPosition.link;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapPositionExceptions;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.eclipse.jdt.internal.corext.refactoring.code.AnaphoraPositionCreationForRefactoring.createPositionForRelatedExpression;

import java.util.List;
import java.util.function.BiConsumer;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.internal.corext.refactoring.code.AnaphoraPositionCreationForRefactoring;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IPositionUpdater;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.AnaphoraTesting;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionAroundASTNode.NodeType;
import de.monochromata.eclipse.persp.PerspectivationPosition;
import de.monochromata.eclipse.position.PositionExceptionWrapping.BadLocationThrowingConsumer;

public interface AnaphoraPositionCreationTesting extends AnaphoraTesting, PositionsTesting {

	IPositionUpdater POSITION_UPDATER = new AnaphoraPositionUpdater(anaphoras -> {
	});

	static PositionForAnaphor createAndLinkAndAddPositionForAnaphor(final IDocument document,
			final PositionForRelatedExpression positionForRelatedExpression, final Anaphora anaphora,
			final PerspectivationPosition perspectivationPosition) {
		final PositionForAnaphor positionForAnaphor = createAndLinkPositionForAnaphor(positionForRelatedExpression,
				anaphora, perspectivationPosition);
		wrapPositionExceptions(() -> addPosition(document, positionForAnaphor));
		return positionForAnaphor;
	}

	static PositionForAnaphor createAndLinkPositionForAnaphor(
			final PositionForRelatedExpression positionForRelatedExpression, final Anaphora anaphora,
			final PerspectivationPosition perspectivationPosition) {
		final PositionForAnaphor positionForAnaphor = AnaphoraPositionCreationForRefactoring
				.createPositionForAnaphor(anaphora, perspectivationPosition);
		link(positionForRelatedExpression, positionForAnaphor);
		return positionForAnaphor;
	}

	static PositionForRelatedExpression addPositionForRelatedExpression(final IDocument document,
			final PerspectivationPosition perspectivationPosition) {
		final PositionForRelatedExpression position = createPositionForRelatedExpression(perspectivationPosition);
		wrapPositionExceptions(() -> addPosition(document, position));
		return position;
	}

	default void assertCorrectPositionsForRepresentation(final String documentText, final int relatedExpressionStart,
			final String relatedExpressionCode, final int anaphorStart, final String anaphorCode,
			final BadLocationThrowingConsumer<IDocument> modification, final int expectedStartOfRelatedExpression,
			final int expectedLengthOfRelatedExpression, final int expectedStartOfAnaphor,
			final int expectedLengthOfAnaphor) throws Exception {
		assertCorrectPositionsForRepresentation(documentText, relatedExpressionStart, relatedExpressionCode,
				anaphorStart, anaphorCode, modification, (documentAndAnaphora, anaphoraPositions) -> {
					try {
						assertCorrectPositionsForRepresentation(documentAndAnaphora.getLeft(),
								asList(anaphoraPositions.getLeft(), anaphoraPositions.getRight()),
								documentAndAnaphora.getRight(), expectedStartOfRelatedExpression,
								expectedLengthOfRelatedExpression, expectedStartOfAnaphor, expectedLengthOfAnaphor);
					} catch (final Exception e) {
						throw new RuntimeException(e);
					}
				});
	}

	default void assertCorrectPositionsForRepresentation(final String documentText, final int relatedExpressionStart,
			final String relatedExpressionCode, final int anaphorStart, final String anaphorCode,
			final BadLocationThrowingConsumer<IDocument> modification,
			final BiConsumer<Pair<Document, Anaphora>, Pair<PositionForRelatedExpression, PositionForAnaphor>> positionsAsserter)
			throws Exception {
		final Pair<Document, PublicAnaphora> documentAndAnaphora = createConfiguredDocumentAndAnaphora(documentText,
				relatedExpressionStart, relatedExpressionCode, anaphorStart, anaphorCode);
		final Document document = documentAndAnaphora.getLeft();
		final Anaphora anaphora = new Anaphora(documentAndAnaphora.getRight(), documentAndAnaphora.getRight());

		final PositionForRelatedExpression relatedExpressionPosition = addPositionForRelatedExpression(document,
				new PerspectivationPosition(relatedExpressionStart, relatedExpressionCode.length(), null, emptyList()));
		final PositionForAnaphor anaphorPosition = createAndLinkAndAddPositionForAnaphor(document,
				relatedExpressionPosition, anaphora,
				new PerspectivationPosition(anaphorStart, anaphorCode.length(), null, emptyList()));
		final Pair anaphoraPositions = new ImmutablePair<>(relatedExpressionPosition, anaphorPosition);

		modification.accept(document);

		positionsAsserter.accept(new ImmutablePair<>(document, anaphora), anaphoraPositions);
	}

	default Pair<Document, PublicAnaphora> createConfiguredDocumentAndAnaphora(final String documentText,
			final int relatedExpressionStart, final String relatedExpressionCode, final int anaphorStart,
			final String anaphorCode) {
		final Pair<Document, PublicAnaphora> documentAndAnaphora = createDocumentAndAnaphora(documentText,
				relatedExpressionStart, relatedExpressionCode, anaphorStart, anaphorCode);
		final Document document = documentAndAnaphora.getLeft();
		document.addPositionCategory(ANAPHORA_CATEGORY);
		document.addPositionUpdater(POSITION_UPDATER);
		return documentAndAnaphora;
	}

	default void assertCorrectPositionsForRepresentation(final Document document,
			final List<AbstractAnaphoraPosition> positions, final Anaphora expectedAnaphora,
			final int expectedStartOfRelatedExpression, final int expectedLengthOfRelatedExpression,
			final int expectedStartOfAnaphor, final int expectedLengthOfAnaphor) throws Exception {

		final PositionForRelatedExpression relatedExpressionPosition = new PositionForRelatedExpression(
				expectedStartOfRelatedExpression, expectedLengthOfRelatedExpression,
				new PerspectivationPosition(expectedStartOfRelatedExpression, expectedLengthOfRelatedExpression));
		final PositionForAnaphor anaphorPosition = new PositionForAnaphor(expectedStartOfAnaphor,
				expectedLengthOfAnaphor, new PerspectivationPosition(expectedStartOfAnaphor, expectedLengthOfAnaphor),
				expectedAnaphora);

		assertGivenExpectedPositions(document, positions, relatedExpressionPosition, anaphorPosition);
	}

	default void assertCorrectPositionsForAstNodes(final String documentText, final int relatedExpressionStart,
			final String relatedExpressionCode, final int anaphorStart, final String anaphorCode,
			final BadLocationThrowingConsumer<IDocument> modification, final int expectedStartOfRelatedExpression,
			final int expectedLengthOfRelatedExpression, final int expectedStartOfAnaphor,
			final int expectedLengthOfAnaphor) throws Exception {
		assertCorrectPositionsForAstNodes(documentText, relatedExpressionStart, relatedExpressionCode, anaphorStart,
				anaphorCode, modification, (documentAndAnaphora, positions) -> {
					try {
						assertCorrectPositionsForAstNodes(documentAndAnaphora.getLeft(), documentAndAnaphora.getRight(),
								positions, expectedStartOfRelatedExpression, expectedLengthOfRelatedExpression,
								expectedStartOfAnaphor, expectedLengthOfAnaphor);
					} catch (final Exception e) {
						throw new RuntimeException(e);
					}
				});
	}

	default void assertCorrectPositionsForAstNodes(final String documentText, final int relatedExpressionStart,
			final String relatedExpressionCode, final int anaphorStart, final String anaphorCode,
			final BadLocationThrowingConsumer<IDocument> modification,
			final BiConsumer<Pair<Document, PublicAnaphora>, List<AbstractAnaphoraPosition>> positionsAsserter)
			throws Exception {
		final Pair<Document, PublicAnaphora> documentAndAnaphora = createConfiguredDocumentAndAnaphora(documentText,
				relatedExpressionStart, relatedExpressionCode, anaphorStart, anaphorCode);
		final Document document = documentAndAnaphora.getLeft();
		final PublicAnaphora anaphora = documentAndAnaphora.getRight();

		final List positions = AnaphoraPositionCreation.addPositionsForAstNodes(document, anaphora);

		modification.accept(document);

		positionsAsserter.accept(documentAndAnaphora, positions);
	}

	default void assertCorrectPositionsForAstNodes(final Document document, final PublicAnaphora anaphora,
			final List<AbstractAnaphoraPosition> positions, final int expectedStartOfRelatedExpression,
			final int expectedLengthOfRelatedExpression, final int expectedStartOfAnaphor,
			final int expectedLengthOfAnaphor) throws Exception {
		final AnaphoraPositionOnRelatedExpression relatedExpressionPosition = new AnaphoraPositionOnRelatedExpression(
				anaphora.getRelatedExpression().getRelatedExpression(), expectedStartOfRelatedExpression,
				expectedLengthOfRelatedExpression);
		final AnaphoraPositionOnAnaphor anaphorPosition = new AnaphoraPositionOnAnaphor(anaphora.getAnaphorExpression(),
				expectedStartOfAnaphor, expectedLengthOfAnaphor, relatedExpressionPosition);

		assertExpectedPositions(document, positions, relatedExpressionPosition, anaphorPosition);
	}

	default void assertCorrectPositionsAroundAstNodes(final String documentText, final int relatedExpressionStart,
			final String relatedExpressionCode, final int anaphorStart, final String anaphorCode,
			final BadLocationThrowingConsumer<IDocument> modification,
			final int expectedOffsetBeforeStartOfRelatedExpression, final int expectedOffsetAfterEndOfRelatedExpression,
			final int expectedOffsetBeforeStartOfAnaphor, final int expectedOffsetAfterEndOfAnaphor) throws Exception {
		final Pair<Document, PublicAnaphora> documentAndAnaphora = createConfiguredDocumentAndAnaphora(documentText,
				relatedExpressionStart, relatedExpressionCode, anaphorStart, anaphorCode);
		final Document document = documentAndAnaphora.getLeft();
		final PublicAnaphora anaphora = documentAndAnaphora.getRight();

		final List<AbstractAnaphoraPositionAroundASTNode> positions = AnaphoraPositionCreation
				.addPositionsAroundAstNodes(document, anaphora);

		modification.accept(document);

		assertCorrectPositionsAroundAstNodes(document, anaphora, positions,
				expectedOffsetBeforeStartOfRelatedExpression, expectedOffsetAfterEndOfRelatedExpression,
				expectedOffsetBeforeStartOfAnaphor, expectedOffsetAfterEndOfAnaphor);
	}

	default void assertCorrectPositionsAroundAstNodes(final Document document, final PublicAnaphora anaphora,
			final List<AbstractAnaphoraPositionAroundASTNode> positions,
			final int expectedOffsetBeforeStartOfRelatedExpression, final int expectedOffsetAfterEndOfRelatedExpression,
			final int expectedOffsetBeforeStartOfAnaphor, final int expectedOffsetAfterEndOfAnaphor) throws Exception {
		final AnaphoraPositionBeforeStartOfASTNode positionBeforeStartOfRelatedExpression = new AnaphoraPositionBeforeStartOfASTNode(
				NodeType.RelatedExpression, anaphora.getRelatedExpression().getRelatedExpression(),
				expectedOffsetBeforeStartOfRelatedExpression, 0);
		final AnaphoraPositionBeforeStartOfASTNode positionBeforeStartOfAnaphor = new AnaphoraPositionBeforeStartOfASTNode(
				NodeType.Anaphor, anaphora.getAnaphorExpression(), expectedOffsetBeforeStartOfAnaphor, 0);
		assertExpectedPositions(document, positions, positionBeforeStartOfRelatedExpression,
				new AnaphoraPositionAfterEndOfASTNode(NodeType.RelatedExpression,
						anaphora.getRelatedExpression().getRelatedExpression(), positionBeforeStartOfRelatedExpression,
						expectedOffsetAfterEndOfRelatedExpression, 0),
				positionBeforeStartOfAnaphor,
				new AnaphoraPositionAfterEndOfASTNode(NodeType.Anaphor, anaphora.getAnaphorExpression(),
						positionBeforeStartOfAnaphor, expectedOffsetAfterEndOfAnaphor, 0));
	}

}
