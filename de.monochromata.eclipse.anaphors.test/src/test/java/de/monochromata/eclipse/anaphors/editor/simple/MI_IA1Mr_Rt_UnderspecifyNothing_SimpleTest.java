package de.monochromata.eclipse.anaphors.editor.simple;

import static de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy.underspecifyNothing;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;

public class MI_IA1Mr_Rt_UnderspecifyNothing_SimpleTest extends AbstractSimpleEditorTest {

    @Override
    protected PerspectivationStrategy getPerspectivationStrategy() {
        return underspecifyNothing();
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
        builder.addLine("");
        builder.addLine("public class MI_IA1Mr_Rt_UnderspecifyNothing_Simple {");
        builder.addLine("");
        builder.addLine("    public static void main() {");
        builder.addLine("        final ObjectSession objectSession = create();");
        builder.addLine("        System.err.println(objectSession);");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static ObjectSession create() {");
        builder.addLine("        return null;");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static class ObjectSession {");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
        builder.addLine("");
        builder.addLine("public class MI_IA1Mr_Rt_UnderspecifyNothing_Simple {");
        builder.addLine("");
        builder.addLine("    public static void main() {");
        builder.addLine("        final ObjectSession objectSession = create();");
        builder.addLine("        System.err.println(objectSession);");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static ObjectSession create() {");
        builder.addLine("        return null;");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static class ObjectSession {");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected Pair<Anaphora, Anaphora> getExpectedResolvedAndRealizedAnaphora() {
        return new ImmutablePair<>(new Anaphora("MI", "IA1Mr", "Rt", "objectSession"),
                new Anaphora("LVD", "DA1Re", "Rn", "objectSession"));
    }

}
