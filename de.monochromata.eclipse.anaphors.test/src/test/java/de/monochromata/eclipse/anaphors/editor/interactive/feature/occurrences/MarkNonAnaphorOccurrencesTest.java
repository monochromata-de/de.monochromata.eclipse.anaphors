package de.monochromata.eclipse.anaphors.editor.interactive.feature.occurrences;

import static java.util.Arrays.asList;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;

import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.editor.interactive.AbstractInteractiveEditorTest;

public class MarkNonAnaphorOccurrencesTest extends AbstractInteractiveEditorTest {

    @Override
    protected void manipulateEditorAndCheckAssertions(final ProblemCollectingChangeListener problemCollector,
            final CognitiveEditor editor) {
        enableMarkOccurrences();
        editFile(editor);
        waitForExpectedAnnotations(editor, getExpectedAnnotations());
        assertAnnotations(editor, getExpectedAnnotations());
    }

    @Override
    protected void editFile(final IEditorPart editor) {
        getBot().activeEditor().toTextEditor().selectRange(5, 2, 0);
    }

    @Override
    protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
        return asList(
                new ImmutablePair<>(new Annotation("org.eclipse.jdt.ui.occurrences", false,
                        "Occurrence of 'foo'"), new Position(168, 3)),
                new ImmutablePair<>(
                        new Annotation("org.eclipse.jdt.ui.occurrences", false, "Occurrence of 'foo'"),
                        new Position(200, 3)));
    }

    @Override
    public String getExpectedEventualSourceCode() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        throw new UnsupportedOperationException();
    }

    @Override
    protected List<? extends Position> getExpectedPositions() {
        throw new UnsupportedOperationException();
    }

}
