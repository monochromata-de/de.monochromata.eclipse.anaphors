package de.monochromata.eclipse.anaphors.editor.update;

import static de.monochromata.eclipse.anaphors.editor.update.MemberCollection.getAllMembers;
import static org.assertj.core.api.Assertions.assertThat;

import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IInitializer;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.dom.anaphors.ASTTesting;
import org.eclipse.jdt.core.dom.anaphors.JavaModelTesting;
import org.junit.Test;

public class MemberCollectionTest implements ASTTesting, JavaModelTesting, MemberCollectionTesting {

    // Match the different types of members

    @Test
    public void findsInstanceMethod() throws Exception {
        assertProjectAndCompilationUnit("Foo", "class Foo { void getFoo() { return \"foo\"; } }",
                (compilationUnit, project) -> {
                    final IMethod method = findMethod("getFoo", compilationUnit);
                    assertThat(getAllMembers(compilationUnit.getJavaElement())).containsExactly(method);
                });
    }

    @Test
    public void findsStaticMethodInStaticNestedType() throws Exception {
        assertProjectAndCompilationUnit("Foo",
                "class Foo { static class B { static void getBar() { return \"bar\"; } } }",
                (compilationUnit, project) -> {
                    final IMethod staticMethodFromNestedType = findMethod("getBar", compilationUnit);
                    assertThat(getAllMembers(compilationUnit.getJavaElement()))
                            .containsExactly(staticMethodFromNestedType);
                });
    }

    @Test
    public void findsConstructor() throws Exception {
        assertProjectAndCompilationUnit("Foo", "class Foo { Foo() { System.out.println(\"foo\"); } }",
                (compilationUnit, project) -> {
                    final IMethod method = findMethod("Foo", compilationUnit);
                    assertThat(getAllMembers(compilationUnit.getJavaElement())).containsExactly(method);
                });
    }

    @Test
    public void findsInstanceInitializerOfAnonymousInnerType() throws Exception {
        assertProjectAndCompilationUnit("Foo",
                "class Foo { { System.out.println(\"foo\"); } }",
                (compilationUnit, project) -> {
                    final IInitializer initializer = findInitializer(compilationUnit);
                    assertThat(getAllMembers(compilationUnit.getJavaElement())).containsExactly(initializer);
                });
    }

    @Test
    public void findsStaticInitializer() throws Exception {
        assertProjectAndCompilationUnit("Foo", "class Foo { static { System.out.println(\"foo\"); } }",
                (compilationUnit, project) -> {
                    final IInitializer initializer = findInitializer(compilationUnit);
                    assertThat(getAllMembers(compilationUnit.getJavaElement())).containsExactly(initializer);
                });
    }

    @Test
    public void findsFieldIncludingItsInitializer() throws Exception {
        assertProjectAndCompilationUnit("Foo", "class Foo { String bar = \"baroeo\"; }",
                (compilationUnit, project) -> {
                    final IField field = findField("bar", compilationUnit);
                    assertThat(getAllMembers(compilationUnit.getJavaElement())).containsExactly(field);
                });
    }

    @Test
    public void matchMultipleMembers() throws Exception {
        assertProjectAndCompilationUnit("Foo", "class Foo { String foo; String bar; }",
                (compilationUnit, project) -> {
                    final IField foo = findField("foo", compilationUnit);
                    final IField bar = findField("bar", compilationUnit);
                    assertThat(getAllMembers(compilationUnit.getJavaElement())).containsExactly(foo, bar);
                });
    }
}
