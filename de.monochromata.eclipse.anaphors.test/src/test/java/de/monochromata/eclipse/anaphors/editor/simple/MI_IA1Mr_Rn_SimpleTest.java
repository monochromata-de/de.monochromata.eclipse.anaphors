package de.monochromata.eclipse.anaphors.editor.simple;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;

public class MI_IA1Mr_Rn_SimpleTest extends AbstractSimpleEditorTest {

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
        builder.addLine("");
        builder.addLine("public class MI_IA1Mr_Rn_Simple {");
        builder.addLine("");
        builder.addLine("    public static void main() {");
        builder.addLine("        final ObjectSession login = login();");
        builder.addLine("        System.err.println(login);");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static ObjectSession login() {");
        builder.addLine("        return null;");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static class ObjectSession {");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
        builder.addLine("");
        builder.addLine("public class MI_IA1Mr_Rn_Simple {");
        builder.addLine("");
        builder.addLine("    public static void main() {");
        builder.addLine("        login();");
        builder.addLine("        System.err.println(login);");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static ObjectSession login() {");
        builder.addLine("        return null;");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static class ObjectSession {");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected Pair<Anaphora, Anaphora> getExpectedResolvedAndRealizedAnaphora() {
        return new ImmutablePair<>(new Anaphora("MI", "IA1Mr", "Rn", "login"),
                new Anaphora("LVD", "DA1Re", "Rn", "login"));
    }

}
