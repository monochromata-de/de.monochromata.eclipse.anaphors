package de.monochromata.eclipse.anaphors.editor.interactive;

import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable.syncExec;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;

import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;

public abstract class AbstractInteractiveDoubleAssertionEditorTest extends AbstractInteractiveEditorTest {

	@Override
	protected void manipulateEditorAndCheckAssertions(final ProblemCollectingChangeListener problemCollector,
			final CognitiveEditor editor) {
		enableMarkOccurrences();
		waitForAnaphoraRelations(editor);
		assertAnaphoraRelations();
		waitForProblemsToBeSolved(editor);
		assertNoProblemMarkersPresent(editor, problemCollector.getProblems());
		assertEventualSourceCode(editor, getExpectedIntermediateRefactoredSourceCode());
		assertUnderspecifiedEventualSourceCode(editor, getExpectedIntermediateUnderspecifiedRefactoredSourceCode());
		waitForExpectedAnnotations(editor, getExpectedIntermediateAnnotations());
		assertAnnotations(editor, getExpectedIntermediateAnnotations());
		waitForExpectedPositions(editor, getExpectedIntermediatePositions());
		assertGivenPositions(editor, getExpectedIntermediatePositions());

		editFile(editor);
		syncExec(() -> editor.doSave(null));
		waitForAnaphoraRelationsAfterEdit(editor);
		waitForPositionsAfterEdit(editor);
		assertThat(getNumberOfAnaphoraPositions(editor)).isEqualTo(2 * getExpectedNumberOfAnaphoraRelationsAfterEdit());
		assertEventualSourceCode(editor);
		assertUnderspecifiedEventualSourceCode(editor);
		assertAnnotationsAfterChange(editor);
		assertPositionsAfterEdit(editor);
	}

	protected void assertAnnotationsAfterChange(final CognitiveEditor editor) {
		waitForExpectedAnnotations(editor, getExpectedAnnotations());
		assertAnnotations(editor, getExpectedAnnotations());
	}

	protected abstract String getExpectedIntermediateRefactoredSourceCode();

	protected abstract String getExpectedIntermediateUnderspecifiedRefactoredSourceCode();

	protected abstract List<Pair<? extends Annotation, ? extends Position>> getExpectedIntermediateAnnotations();

	protected abstract List<? extends Position> getExpectedIntermediatePositions();

	protected void waitForAnaphoraRelationsAfterEdit(final CognitiveEditor editor) {
		waitForAnaphoraRelations(editor, getExpectedNumberOfAnaphoraRelationsAfterEdit());
	}

	protected abstract int getExpectedNumberOfAnaphoraRelationsAfterEdit();

	protected void waitForPositionsAfterEdit(final CognitiveEditor editor) {
		waitForExpectedPositions(editor, getExpectedPositions());
	}

	protected void assertPositionsAfterEdit(final CognitiveEditor editor) {
		assertGivenPositions(editor, getExpectedPositions());
	}

}
