package de.monochromata.eclipse.anaphors.editor.interactive;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

import java.io.File;
import java.lang.reflect.Method;

import org.eclipse.swtbot.swt.finder.utils.SWTBotPreferences;
import org.junit.Test;
import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

/**
 * Tests {@link CaptureScreenshotOnErrorOrFailure}.
 */
public class CaptureScreenshotOnErrorOrFailureTest {

	private File screenshotFile;
	private long lastModified;

	@Test
	public void dontCaptureScreenshotOnSuccess() throws Throwable {
		testImplicitScreenshot("dontCaptureScreenshotOnSuccess", () -> {
			// No error or failure, it just works.
		}, false);
	}

	@Test
	public void captureScreenshotOnAssertionFailure() throws Throwable {
		testImplicitScreenshot("captureScreenshotOnAssertionFailure", () -> {
			fail("Failing on purpose");
		}, true);
	}

	@Test
	public void captureScreenshotOnException() throws Throwable {
		testImplicitScreenshot("captureScreenshotOnException", () -> {
			throw new RuntimeException("This is only a test.");
		}, true);
	}

	@Test
	public void captureScreenshotExplicitly() throws Throwable {
		final CaptureScreenshotOnErrorOrFailure screenshot = new CaptureScreenshotOnErrorOrFailure();
		final String methodName = "captureScreenshotExplicitly";
		final Method method = getClass().getMethod(methodName);
		screenshot.captureScreenshot(this, methodName, new RuntimeException("An error!"));
		assertScreenshotExistsAndDeleteIt(new FrameworkMethod(method), true);
	}

	@Test
	public void implicitlyCapturedScreenshotMustNotOverwriteExplicitlyCapturedOne() throws Throwable {
		try {
			final CaptureScreenshotOnErrorOrFailure screenshot = new CaptureScreenshotOnErrorOrFailure();
			final RuntimeException throwable = new RuntimeException("An error");

			final String methodName = "implicitlyCapturedScreenshotMustNotOverwriteExplicitlyCapturedOne";

			testImplicitScreenshot(methodName, () -> {
				// Trigger an explicit screenshot.
				try {
					final Method method = getClass().getMethod(methodName);
					screenshot.captureScreenshot(CaptureScreenshotOnErrorOrFailureTest.this, method, throwable);

					screenshotFile = getScreenshotFile(method);
					assertThatScreenshotFileExists(screenshotFile);
					lastModified = screenshotFile.lastModified();
					assertThat(lastModified).isNotZero();

					waitForASecond();
				} catch (final Exception ex) {
					throw new RuntimeException("Unexpected exception", ex);
				}

				// Trigger an implicit screenshot.
				throw throwable;
			}, true, screenshot, false);

			assertThat(screenshotFile.lastModified()).isEqualTo(lastModified);
		} finally {
			if (screenshotFile != null) {
				screenshotFile.delete();
			}
		}
	}

	protected void waitForASecond() throws InterruptedException {
		synchronized (this) {
			wait(1000L);
		}
	}

	@Test(expected = IllegalStateException.class)
	public void explicitlyCapturedScreenshotMustNotOverwriteImplicitlyCapturedOne() throws Throwable {

		final CaptureScreenshotOnErrorOrFailure screenshot = new CaptureScreenshotOnErrorOrFailure();
		final RuntimeException throwable = new RuntimeException("An error");

		final String methodName = "explicitlyCapturedScreenshotMustNotOverwriteImplicitlyCapturedOne";
		testImplicitScreenshot(methodName, () -> {
			// Trigger an implicit screenshot.
			throw throwable;
		}, true, screenshot);

		// Raise an IllegalStateException when attempting to overwrite the
		// implicit screenshot with an explicit one.
		screenshot.captureScreenshot(this, methodName, throwable);
	}

	protected void testImplicitScreenshot(final String methodName, final Runnable runnable,
			final boolean expectScreenshotFile) throws Throwable {
		testImplicitScreenshot(methodName, runnable, expectScreenshotFile, new CaptureScreenshotOnErrorOrFailure());
	}

	protected void testImplicitScreenshot(final String methodName, final Runnable runnable,
			final boolean expectScreenshotFile, final CaptureScreenshotOnErrorOrFailure screenshot) throws Throwable {
		testImplicitScreenshot(methodName, runnable, expectScreenshotFile, screenshot, true);
	}

	protected void testImplicitScreenshot(final String methodName, final Runnable runnable,
			final boolean expectScreenshotFile, final CaptureScreenshotOnErrorOrFailure screenshot,
			final boolean deleteFile) throws Throwable {

		final FrameworkMethod method = new FrameworkMethod(
				CaptureScreenshotOnErrorOrFailureTest.class.getMethod(methodName));

		final Statement statement = createScreenshotStatement(runnable, method, screenshot);
		evaluateStatementButIgnoreExceptions(statement);

		if (deleteFile) {
			assertScreenshotExistsAndDeleteIt(method, expectScreenshotFile);
		} else {
			assertThatScreenshotFileExists(method, expectScreenshotFile);
		}
	}

	protected Statement createScreenshotStatement(final Runnable runnable, final FrameworkMethod method,
			final CaptureScreenshotOnErrorOrFailure screenshot) {
		return screenshot.apply(new Statement() {
			@Override
			public void evaluate() throws Throwable {
				runnable.run();
			}
		}, method, this);
	}

	protected void evaluateStatementButIgnoreExceptions(final Statement statement) {
		try {
			statement.evaluate();
		} catch (final Throwable t) {
			// Ignore purposely thrown exceptions
		}
	}

	protected void assertScreenshotExistsAndDeleteIt(final FrameworkMethod method, final boolean screenshotExists) {
		final File file = getScreenshotFile(method);
		try {
			assertThatScreenshotFileExists(file, screenshotExists);
		} finally {
			file.delete();
		}
	}

	protected void assertThatScreenshotFileExists(final File file) {
		assertThatScreenshotFileExists(file, true);
	}

	protected void assertThatScreenshotFileExists(final FrameworkMethod method, final boolean screenshotExists) {
		final File file = getScreenshotFile(method);
		assertThatScreenshotFileExists(file, screenshotExists);
	}

	protected void assertThatScreenshotFileExists(final File file, final boolean screenshotExists) {
		assertThat(file.exists()).isEqualTo(screenshotExists);
	}

	protected File getScreenshotFile(final Method method) {
		return getScreenshotFile(new FrameworkMethod(method));
	}

	protected File getScreenshotFile(final FrameworkMethod method) {
		final Description description = Description.createTestDescription(method.getDeclaringClass(), method.getName());
		final RuntimeException dummyException = new RuntimeException();
		final Failure failure = new Failure(description, dummyException);
		final String fileName = SWTBotPreferences.SCREENSHOTS_DIR + "/" + failure.getTestHeader() + "." //$NON-NLS-1$
				+ SWTBotPreferences.SCREENSHOT_FORMAT.toLowerCase();
		final File file = new File(fileName);
		return file;
	}
}
