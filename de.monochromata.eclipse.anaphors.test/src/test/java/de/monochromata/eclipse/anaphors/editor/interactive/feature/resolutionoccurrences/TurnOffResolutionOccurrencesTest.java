package de.monochromata.eclipse.anaphors.editor.interactive.feature.resolutionoccurrences;

import static java.util.Arrays.asList;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;
import org.junit.After;
import org.junit.Ignore;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.Activator;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.editor.interactive.AbstractInteractiveDoubleAssertionEditorTest;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

@Ignore
public class TurnOffResolutionOccurrencesTest extends AbstractInteractiveDoubleAssertionEditorTest {

    @Override
    protected String getExpectedKindOfResolvedAnaphora() {
        return "CIC-IA2F-Rt";
    }

    @Override
    protected String getExpectedIntermediateRefactoredSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature.resolutionoccurrences;");
        builder.addLine("");
        builder.addLine("public class TurnOffResolutionOccurrences {");
        builder.addLine("");
        builder.addLine("   public static void main(String[] args) {");
        builder.addLine("       final A a = new A();");
        builder.addLine("       System.err.println(a.int0);");
        builder.addLine("   }");
        builder.addLine("");
        builder.addLine("   private static class A {");
        builder.addLine("       public Integer int0;");
        builder.addLine("   }");
        builder.addLine("");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected String getExpectedIntermediateUnderspecifiedRefactoredSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature.resolutionoccurrences;");
        builder.addLine("");
        builder.addLine("public class TurnOffResolutionOccurrences {");
        builder.addLine("");
        builder.addLine("   public static void main(String[] args) {");
        builder.addLine("       new A();");
        builder.addLine("       System.err.println(int0);");
        builder.addLine("   }");
        builder.addLine("");
        builder.addLine("   private static class A {");
        builder.addLine("       public Integer int0;");
        builder.addLine("   }");
        builder.addLine("");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected List<Pair<? extends Annotation, ? extends Position>> getExpectedIntermediateAnnotations() {
        return asList(
                new ImmutablePair<>(new Annotation("org.eclipse.jdt.ui.occurrences.write", false, "Related expression"),
                        new Position(181, 20)),
                new ImmutablePair<>(
                        new Annotation("org.eclipse.jdt.ui.occurrences", false, "Anaphor co-referent to int0"),
                        new Position(223, 6)));
    }

    @Override
    protected List<? extends Position> getExpectedIntermediatePositions() {
        final Anaphora anaphora = new Anaphora("LVD", "IA2F", "Rn", "int0");
        return asList(new PositionForAnaphor(223, 6, anaphora), new PositionForRelatedExpression(181, 20));
    }

    @Override
    protected void editFile(final IEditorPart editor) {
        Activator.getDefault().enableMarkResolutionOccurrences(false);
        deleteTextOnLine(4);
        getBot().activeEditor().toTextEditor().insertText(4, 0, "public static void main(final int int0) {\n");
    }

    @After
    public void reenableResolutionOccurrencesAfterTest() {
        Activator.getDefault().enableMarkResolutionOccurrences(true);
    }

    @Override
    protected int getExpectedNumberOfAnaphoraRelationsAfterEdit() {
        return 1;
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature.resolutionoccurrences;");
        builder.addLine("");
        builder.addLine("public class TurnOffResolutionOccurrences {");
        builder.addLine("");
        builder.addLine("   public static void main(final int int0) {");
        // TODO: The LVD should actually be removed
        builder.addLine("       final A a = new A();");
        builder.addLine("       System.err.println(int0);");
        builder.addLine("   }");
        builder.addLine("");
        builder.addLine("   private static class A {");
        builder.addLine("       public Integer int0;");
        builder.addLine("   }");
        builder.addLine("");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature.resolutionoccurrences;");
        builder.addLine("");
        builder.addLine("public class TurnOffResolutionOccurrences {");
        builder.addLine("");
        builder.addLine("   public static void main(final int int0) {");
        builder.addLine("       new A();");
        builder.addLine("       System.err.println(int0);");
        builder.addLine("   }");
        builder.addLine("");
        builder.addLine("   private static class A {");
        builder.addLine("       public Integer int0;");
        builder.addLine("   }");
        builder.addLine("");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected void assertAnnotationsAfterChange(final CognitiveEditor editor) {
        waitForRemovalOfAnnotationsOfBacklistedTypes(editor, "org.eclipse.jdt.ui.occurrences.write",
                "org.eclipse.jdt.ui.occurrences");
        assertAnnotationsDoNotHaveTypes(editor, "org.eclipse.jdt.ui.occurrences.write",
                "org.eclipse.jdt.ui.occurrences");
    }

    @Override
    protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
        throw new UnsupportedOperationException("A blacklist of types is used instead");
    }

    @Override
    protected List<? extends Position> getExpectedPositions() {
        final Anaphora anaphora = new Anaphora("LVD", "DA1Re", "Rn", "int0");
        return asList(new PositionForAnaphor(224, 4, anaphora), new PositionForRelatedExpression(162, 14));
    }
}
