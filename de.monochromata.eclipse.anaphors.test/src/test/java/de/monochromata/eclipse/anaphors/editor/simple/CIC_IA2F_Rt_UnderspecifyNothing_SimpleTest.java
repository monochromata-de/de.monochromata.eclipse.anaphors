package de.monochromata.eclipse.anaphors.editor.simple;

import static de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy.underspecifyNothing;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;

public class CIC_IA2F_Rt_UnderspecifyNothing_SimpleTest extends AbstractSimpleEditorTest {

    @Override
    protected PerspectivationStrategy getPerspectivationStrategy() {
        return underspecifyNothing();
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
        builder.addLine("");
        builder.addLine("public class CIC_IA2F_Rt_UnderspecifyNothing_Simple {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		final A a = new A();");
        builder.addLine("		System.err.println(a.int0);");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("	private static class A {");
        builder.addLine("		public Integer int0;");
        builder.addLine("	}");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
        builder.addLine("");
        builder.addLine("public class CIC_IA2F_Rt_UnderspecifyNothing_Simple {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		final A a = new A();");
        builder.addLine("		System.err.println(a.int0);");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("	private static class A {");
        builder.addLine("		public Integer int0;");
        builder.addLine("	}");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected Pair<Anaphora, Anaphora> getExpectedResolvedAndRealizedAnaphora() {
        return new ImmutablePair<>(new Anaphora("CIC", "IA2F", "Rt", "integer"),
                new Anaphora("LVD", "IA2F", "Rn", "int0"));
    }

}
