package de.monochromata.eclipse.anaphors.editor.interactive.feature.perspectivation;

import static de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy.underspecifyNothing;
import static de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy.underspecifyRelatedExpressions;
import static java.util.Arrays.asList;

import java.util.List;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class SwitchUnderspecifyNothingToUnderspecifyRelatedExpressionsTest
        extends AbstractPerspectivationStrategyChangeTest {

    @Override
    protected PerspectivationStrategy getInitialPerspectivationStrategy() {
        return underspecifyNothing();
    }

    @Override
    protected String getInitialRelatedExpressionImage() {
        return "final A a = new A();";
    }

    @Override
    protected String getInitialAnaphorImage() {
        return "a.getValue()";
    }

    @Override
    protected List<AbstractAnaphoraPositionForRepresentation> getExpectedPositions(final Anaphora anaphora) {
        return asList(new PositionForRelatedExpression(204, 20),
                new PositionForAnaphor(246, 12, anaphora));
    }

    @Override
    protected PerspectivationStrategy getUpdatedPerspectivationStrategy() {
        return underspecifyRelatedExpressions();
    }

    @Override
    protected String getRelatedExpressionImageAfterChange() {
        return "new A();";
    }

    @Override
    protected String getAnaphorImageAfterChange() {
        return "a.getValue()";
    }

}
