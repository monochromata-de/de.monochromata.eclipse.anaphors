package de.monochromata.eclipse.anaphors.editor;

/**
 * Used to construct source code with operating-system specific line
 * terminators.
 * 
 * @author monochromata
 */
public class CodeBuilder {

	/**
	 * The operating-system specific line terminator, determined from the system
	 * property {@code line.terminator}.
	 */
	public static final String OS_SPECIFIC_LINE_TERMINATOR = System.getProperty("line.separator");

	private final StringBuilder stringBuilder = new StringBuilder();

	/**
	 * Adds a line of code that will be terminated with
	 * {@link #OS_SPECIFIC_LINE_TERMINATOR}.
	 * 
	 * @param line
	 *            The line to add.
	 * @return This code builder to permit method chaining.
	 */
	public CodeBuilder addLine(String line) {
		stringBuilder.append(line + OS_SPECIFIC_LINE_TERMINATOR);
		return this;
	}

	/**
	 * @return The lines added so far, including line terminators.
	 */
	public String toString() {
		return stringBuilder.toString();
	}
}
