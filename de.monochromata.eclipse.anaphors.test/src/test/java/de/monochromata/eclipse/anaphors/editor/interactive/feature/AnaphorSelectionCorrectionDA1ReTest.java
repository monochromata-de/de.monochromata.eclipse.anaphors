package de.monochromata.eclipse.anaphors.editor.interactive.feature;

import static java.util.Arrays.asList;

import java.util.List;

import org.eclipse.jface.text.Position;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class AnaphorSelectionCorrectionDA1ReTest extends AbstractAnaphorSelectionCorrectionTest
        implements SelectionUpdateTesting {

    @Override
    protected String getExpectedKindOfResolvedAnaphora() {
        return "CIC-DA1Re-RfRt";
    }

    @Override
    protected void editFile(final ProblemCollectingChangeListener problemCollector,
            final CognitiveEditor editor) {
        assertCorrectAnaphorResolution(problemCollector, editor, 1);
        assertReplacementsAndSelectionMovements(" new String(\"Hello\");", 157,
                2, problemCollector, editor);
        assertReplacementsAndSelectionMovements("(helloString);", 234, 23, problemCollector, editor);
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature;");
        builder.addLine("");
        builder.addLine("public class AnaphorSelectionCorrectionDA1Re {");
        builder.addLine("");
        builder.addLine("    public static void foo() {");
        builder.addLine("        final String helloString = new String(\"Hello\");");
        builder.addLine("        new String(\"Bello\");");
        builder.addLine("        System.err.println(helloString);");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature;");
        builder.addLine("");
        builder.addLine("public class AnaphorSelectionCorrectionDA1Re {");
        builder.addLine("");
        builder.addLine("    public static void foo() {");
        builder.addLine("        new String(\"Hello\");");
        builder.addLine("        new String(\"Bello\");");
        builder.addLine("        System.err.println(helloString);");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected List<? extends Position> getExpectedPositions() {
        final Anaphora anaphora = new Anaphora("LVD", "DA1Re", "Rn", "helloString");
        final PositionForRelatedExpression relatedExpression = new PositionForRelatedExpression(157, 47);
        final PositionForAnaphor anaphor = new PositionForAnaphor(261, 11, anaphora);
        return asList(relatedExpression, anaphor);
    }

}
