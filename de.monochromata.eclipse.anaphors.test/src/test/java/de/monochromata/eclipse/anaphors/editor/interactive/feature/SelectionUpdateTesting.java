package de.monochromata.eclipse.anaphors.editor.interactive.feature;

import static org.eclipse.jface.text.TextSelection.emptySelection;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;

import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.editor.WaitingDuringTesting;

public interface SelectionUpdateTesting extends WaitingDuringTesting {

    default void waitForSelectionUpdate(final CognitiveEditor editor) {
        final Object waitee = new Object();
        final AtomicReference<ISelection> lastSelection = new AtomicReference<>(emptySelection());
        // Check whether the selection has changed
        final Supplier<Boolean> condition = () -> editor.getSelectionProvider().getSelection() != lastSelection.get();
        final ISelectionChangedListener listener = event -> {
            synchronized (waitee) {
                lastSelection.set(event.getSelection());
                waitee.notifyAll();
            }
        };
        editor.getSelectionProvider().addSelectionChangedListener(listener);
        if (!condition.get()) {
            waitSecondsIfNotNotifiedBefore("selection update", WAIT_FOR_CHANGE_TIMEOUT, condition, waitee);
        }
        editor.getSelectionProvider().removeSelectionChangedListener(listener);
    }

}
