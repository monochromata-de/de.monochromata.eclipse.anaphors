package de.monochromata.eclipse.anaphors.editor;

import static de.monochromata.eclipse.anaphors.editor.SelectionRelativeToAnaphoraPart.Relation.AtBeginningOfAnaphoraPart;
import static de.monochromata.eclipse.anaphors.editor.SelectionRelativeToAnaphoraPart.Relation.AtEndOfAnaphoraPart;
import static de.monochromata.eclipse.anaphors.editor.SelectionRelativeToAnaphoraPart.Relation.InsideAnaphoraPart;
import static de.monochromata.eclipse.anaphors.editor.SelectionRelativeToAnaphoraPart.Relation.OutsideAnaphoraPart;
import static org.assertj.core.api.Assertions.assertThat;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.Region;
import org.junit.Test;

public class AnaphoraPartSelectionUpdaterTest implements AnaphoraPartSelectionUpdaterTesting {

    // Before: selection not covering anaphora part.
    // Now: selection not covering anaphora part

    @Test
    public void newSelectionIsNotCorrectedIfSelectionWasAndIsOutsideAnaphoraParts() throws Throwable {
        final IRegion newSelection = new Region(10, 0);
        assertSelectionUpdate(OutsideAnaphoraPart, 2, newSelection.getOffset(), newSelection.getLength(),
                new Position(0, 0),
                correctedSelection -> assertThat(correctedSelection).isEqualTo(newSelection));
    }

    // Before: selection not covering anaphora part.
    // Now: selection covering anaphora part
    //
    // (It is assumed that the cursor was moved to the position of the anaphor being
    // resolved while resolution was in progress.) This case is not covered because
    // it cannot be told whether the new selection on the anaphor had length 0, or
    // not because it was not present when the existing selection was captured.

    // Before: selection covering anaphora part.
    // Now: selection not covering anaphora part

    @Test
    public void newSelectionIsNotCorrectedIfSelectionWasOnAnaphoraPartBeforeAndIsNowOutsideOfAnaphoraPart()
            throws Throwable {
        assertSelectionUpdate(AtBeginningOfAnaphoraPart, 0, 16, 5, new Position(0, 5),
                correctedSelection -> assertThat(correctedSelection).isEqualTo(new Region(16, 5)));
    }

    // Before: selection covering related expression.
    // Now: selection covering related expression

    @Test
    public void newSelectionIsCorrectedIfSelectionWasOnAnaphoraPartAndNowCoversEntireAnaphoraPart()
            throws Throwable {
        assertSelectionUpdate(AtBeginningOfAnaphoraPart, 0, 0, 5, new Position(0, 5),
                correctedSelection -> assertThat(correctedSelection).isEqualTo(new Region(0, 0)));
    }

    @Test
    public void newSelectionCorrectedToBeginningOfAnaphoraPartIfItWasThereBeforeAndNowTheEntireAnaphoraPartIsSelected()
            throws Throwable {
        assertSelectionUpdate(AtBeginningOfAnaphoraPart, 0, 0, 3, new Position(0, 3),
                correctedSelection -> assertThat(correctedSelection).isEqualTo(new Region(0, 0)));
    }

    @Test
    public void newSelectionIsNotCorrectedIfSelectionWasInsideAnaphoraPartButIsAtEndNow() throws Throwable {
        final IRegion newSelection = new Region(4, 0);
        assertSelectionUpdate(InsideAnaphoraPart, 2, newSelection.getOffset(), newSelection.getLength(),
                new Position(0, 4),
                correctedSelection -> assertThat(correctedSelection).isEqualTo(newSelection));
    }

    @Test
    public void newSelectionIsNotCorrectedIfSelectionWasAtBeginningBeforeButIsNowBeforeAnaphoraPart()
            throws Throwable {
        final IRegion newSelection = new Region(0, 0);
        assertSelectionUpdate(AtBeginningOfAnaphoraPart, 0, newSelection.getOffset(), newSelection.getLength(),
                new Position(1, 4),
                correctedSelection -> assertThat(correctedSelection).isEqualTo(newSelection));
    }

    @Test
    public void newSelectionIsCorrectedToBeginningOfAnaphoraPartIfWasBeforeAndNowEntireVisiblePartOfAnaphoraPartIsSelected()
            throws Throwable {
        assertSelectionUpdate(AtBeginningOfAnaphoraPart, 0,
                0, 7, new Position(0, 7),
                // The anaphora part is partly underspecified (0-1 are now hidden)
                originOffset -> toImageOffset(originOffset, 0, 1),
                imageOffset -> toOriginOffset(imageOffset, 0, 1),
                correctedSelection -> assertThat(correctedSelection).isEqualTo(new Region(0, 0)));
    }

    @Test
    public void newSelectionIsCorrectedToInsideAnaphoraPartIfWasBeforeAndNowEntireAnaphoraPartIsSelected()
            throws Throwable {
        assertSelectionUpdate(InsideAnaphoraPart, 2,
                0, 5, new Position(0, 5),
                correctedSelection -> assertThat(correctedSelection).isEqualTo(new Region(2, 0)));
    }

    @Test
    public void newSelectionIsCorrectedToInsideAnaphoraPartIfWasBeforeAndNowEntireVisiblePartOfAnaphoraPartIsSelected()
            throws Throwable {
        // The anaphora part is partly underspecified (0-1 are now hidden)
        assertSelectionUpdate(InsideAnaphoraPart, 2, 0, 7, new Position(0, 7),
                originOffset -> toImageOffset(originOffset, 0, 1),
                imageOffset -> toOriginOffset(imageOffset, 0, 1),
                correctedSelection -> assertThat(correctedSelection).isEqualTo(new Region(4, 0)));
    }

    @Test
    public void newSelectionIsCorrectedToEndOfAnaphoraPartIfWasBeforeAndNowEntireAnaphoraPartIsSelected()
            throws Throwable {
        assertSelectionUpdate(AtEndOfAnaphoraPart, 5, 0, 5, new Position(0, 5),
                correctedSelection -> assertThat(correctedSelection).isEqualTo(new Region(5, 0)));
    }

    @Test
    public void newSelectionIsCorrectedToEndOfAnaphoraPartIfWasBeforeAndNowEntireVisiblePartOfAnaphoraPartIsSelected()
            throws Throwable {
        assertSelectionUpdate(AtEndOfAnaphoraPart, 7,
                0, 9, new Position(0, 9),
                // The anaphora part is partly underspecified (0-1 are now hidden)
                originOffset -> toImageOffset(originOffset, 0, 1),
                imageOffset -> toOriginOffset(imageOffset, 0, 1),
                correctedSelection -> assertThat(correctedSelection).isEqualTo(new Region(9, 0)));
    }

    @Test
    public void newSelectionIsNotCorrectedIfWasOutsideBeforeAndRemainsOutsideEvenIfAnaphoraPartGrew()
            throws Throwable {
        // Old anaphora part had length 8, new has length 10 (2 of which are
        // underspecified)
        assertSelectionUpdate(OutsideAnaphoraPart, 9,
                0, 10, new Position(0, 10),
                // The related expression is partly underspecified (0-1 are now hidden)
                originOffset -> toImageOffset(originOffset, 0, 1),
                imageOffset -> toOriginOffset(imageOffset, 0, 1),
                correctedSelection -> assertThat(correctedSelection).isEqualTo(new Region(0, 10)));
    }

    @Test
    public void newSelectionIsCorrectedToEndOfAnaphoraPartIfWasOutsideBeforeButAnaphoraPartGrewAndSelectionIsAtEndOfAnaphoraPartNowWithoutUnderspecification()
            throws Throwable {
        assertSelectionUpdate(OutsideAnaphoraPart, 8,
                0, 8, new Position(0, 10),
                correctedSelection -> assertThat(correctedSelection).isEqualTo(new Region(8, 0)));
    }

    @Test
    public void newSelectionIsCorrectedToEndOfAnaphoraPartIfWasOutsideBeforeButAnaphoraPartGrewAndSelectionIsAtEndOfAnaphoraPartNow()
            throws Throwable {
        assertSelectionUpdate(OutsideAnaphoraPart, 8,
                0, 10, new Position(0, 10),
                // The related expression is partly underspecified (0-1 are now hidden)
                originOffset -> toImageOffset(originOffset, 0, 1),
                imageOffset -> toOriginOffset(imageOffset, 0, 1),
                correctedSelection -> assertThat(correctedSelection).isEqualTo(new Region(10, 0)));
    }

    // Before: selection on/covering first anaphora part.
    // Now: selection on/covering second anaphora part

    @Test
    public void correctSelectionWhenMovingSelectionFromFirstAnaphoraPartToSelectionInSecondAnaphoraPart()
            throws Throwable {
        assertSelectionUpdate(7, 0,
                new ImmutablePair<>(new Position(0, 5), new SelectionRelativeToAnaphoraPart(InsideAnaphoraPart, 1)),
                new ImmutablePair<>(new Position(5, 5), new SelectionRelativeToAnaphoraPart(OutsideAnaphoraPart, -4)),
                correctedSelection -> assertThat(correctedSelection).isEqualTo(new Region(7, 0)));
    }

    @Test
    public void doNotCorrectSelectionWhenMovingSelectionFromFirstAnaphoraPartToSecondAnaphoraPartCoveredBySelection()
            throws Throwable {
        // (It is assumed that the cursor was moved to the position of the second
        // anaphora part being resolved while resolution was in progress.) This
        // case is not corrected because it cannot be told whether the new
        // selection on the second anaphora part had length 0, or not because it
        // was not present when the existing selection was captured.
        assertSelectionUpdate(5, 5,
                new ImmutablePair<>(new Position(0, 5), new SelectionRelativeToAnaphoraPart(InsideAnaphoraPart, 1)),
                new ImmutablePair<>(new Position(5, 5), new SelectionRelativeToAnaphoraPart(OutsideAnaphoraPart, -4)),
                correctedSelection -> assertThat(correctedSelection).isEqualTo(new Region(5, 5)));
    }

    // Before: selection on/covering second anaphora part.
    // Now: selection on/covering first anaphora part

    @Test
    public void correctSelectionWhenMovingSelectionFromSecondAnaphoraPartToSelectionInFirstAnaphoraPart()
            throws Throwable {
        assertSelectionUpdate(3, 0,
                new ImmutablePair<>(new Position(0, 5), new SelectionRelativeToAnaphoraPart(OutsideAnaphoraPart, 7)),
                new ImmutablePair<>(new Position(5, 5), new SelectionRelativeToAnaphoraPart(InsideAnaphoraPart, 2)),
                correctedSelection -> assertThat(correctedSelection).isEqualTo(new Region(3, 0)));
    }

    @Test
    public void doNotCorrectSelectionWhenMovingSelectionFromSecondAnaphoraPartToFirstAnaphoraPartCoveredBySelection()
            throws Throwable {
        // (It is assumed that the cursor was moved to the position of the first
        // anaphora part being resolved while resolution was in progress.) This
        // case is not corrected because it cannot be told whether the new
        // selection on the first anaphora part had length 0, or not because it
        // was not present when the existing selection was captured.
        assertSelectionUpdate(0, 5,
                new ImmutablePair<>(new Position(0, 5), new SelectionRelativeToAnaphoraPart(OutsideAnaphoraPart, 7)),
                new ImmutablePair<>(new Position(5, 5), new SelectionRelativeToAnaphoraPart(InsideAnaphoraPart, 2)),
                correctedSelection -> assertThat(correctedSelection).isEqualTo(new Region(0, 5)));
    }

}
