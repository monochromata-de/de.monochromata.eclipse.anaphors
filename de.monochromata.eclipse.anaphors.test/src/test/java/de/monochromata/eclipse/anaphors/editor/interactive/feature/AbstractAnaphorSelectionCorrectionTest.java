package de.monochromata.eclipse.anaphors.editor.interactive.feature;

import static de.monochromata.eclipse.anaphors.swt.SWTAccess.supplySync;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.stream.IntStream;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IEditorPart;

import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.editor.interactive.AbstractInteractiveEditorTest;
import de.monochromata.eclipse.anaphors.swt.SWTAccess;

public abstract class AbstractAnaphorSelectionCorrectionTest
        extends AbstractInteractiveEditorTest {

    @Override
    protected void manipulateEditorAndCheckAssertions(final ProblemCollectingChangeListener problemCollector,
            final CognitiveEditor editor) {
        enableMarkOccurrences();
        editFile(problemCollector, editor);
    }

    protected abstract void editFile(final ProblemCollectingChangeListener problemCollector,
            final CognitiveEditor editor);

    @Override
    protected void editFile(final IEditorPart editorPart) {
        throw new UnsupportedOperationException(
                "Use editFile(" + ProblemCollectingChangeListener.class.getName()
                        + "," + CognitiveEditor.class.getName() + ") instead.");
    }

    protected void assertReplacementsAndSelectionMovements(final String replacementString,
            final int initialExpectedSelectionOffset, final int initialNumberOfAddedOrUpdatedAnaphoraRelations,
            final ProblemCollectingChangeListener problemCollector, final CognitiveEditor editor) {
        final char[] replacements = replacementString.toCharArray();
        IntStream
                .range(0, replacements.length)
                .forEach(i -> assertReplaceAndSelection(replacements[i], initialExpectedSelectionOffset + i, 0,
                        initialNumberOfAddedOrUpdatedAnaphoraRelations + i, problemCollector, editor));
    }

    protected void assertReplaceAndSelection(final char replacement, final int expectedSelectionOffset,
            final int expectedSelectionLength, final int numberOfAddedOrUpdatedAnaphoraRelations,
            final ProblemCollectingChangeListener problemCollector,
            final CognitiveEditor editor) {
        replaceText(expectedSelectionOffset, replacement, editor);
        assertCorrectAnaphorResolution(problemCollector, editor, numberOfAddedOrUpdatedAnaphoraRelations);
        assertSelection(expectedSelectionOffset, expectedSelectionLength, editor);
    }

    protected void replaceText(final int offset, final char replacement,
            final CognitiveEditor editor) {
        SWTAccess.runSync(editor.getAdapter(Control.class),
                () -> replaceTestInSwtThread(offset, replacement, editor));
    }

    protected void replaceTestInSwtThread(final int endOfSelectionOffset, final char replacement,
            final CognitiveEditor editor) {
        editor.getViewer().getTextWidget().setSelectionRange(endOfSelectionOffset - 1, 1);
        editor.getViewer().getTextWidget().replaceTextRange(endOfSelectionOffset - 1, 1, "" + replacement);
    }

    protected void assertSelection(final int expectedSelectionOffset, final int expectedSelectionLength,
            final CognitiveEditor editor) {
        final Point selectedRange = supplySync(editor.getViewer().getTextWidget().getDisplay(),
                () -> editor.getViewer().getTextWidget().getSelectionRange());
        assertThat(selectedRange.x).isEqualTo(expectedSelectionOffset);
        assertThat(selectedRange.y).isEqualTo(expectedSelectionLength);
    }

    protected void assertCorrectAnaphorResolution(final ProblemCollectingChangeListener problemCollector,
            final CognitiveEditor editor, final int numberOfAddedOrUpdatedAnaphoraRelations) {
        waitForMaintainedAnaphoraRelations(editor, numberOfAddedOrUpdatedAnaphoraRelations);
        waitForProblemsToBeSolved(editor);
        assertRefactoringResults(editor, problemCollector);
        waitForExpectedPositions(editor, getExpectedPositions());
        assertGivenPositions(editor, getExpectedPositions());
    }

    @Override
    protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
        throw new UnsupportedOperationException("Not used in this test");
    }
}
