package de.monochromata.eclipse.anaphors.editor.update;

import static de.monochromata.eclipse.anaphors.editor.update.TriggeringPositionNormalization.ignoreProblemPositionsThatOverlapWithPositionsFromMember;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.eclipse.anaphors.position.KnownProblemPosition;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;

public interface TriggeringPositionNormalizationTesting {

    default List<KnownProblemPosition> invokeTestee(
            final List<PositionForAnaphor> anaphorPositions,
            final List<KnownProblemPosition> problemPositions,
            final List<Pair<PositionForAnaphor, KnownProblemPosition>> overlaps) {
        return ignoreProblemPositionsThatOverlapWithPositionsFromMember(anaphorPositions,
                problemPositions, (anaphorPosition, removedPosition) -> overlaps
                        .add(new ImmutablePair<>(anaphorPosition, removedPosition)));
    }
}
