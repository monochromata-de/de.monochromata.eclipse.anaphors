package de.monochromata.eclipse.anaphors.editor.update;

import static de.monochromata.eclipse.persp.PerspectivationPosition.PERSPECTIVATION_CATEGORY;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapPositionExceptions;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.ASTTesting;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;

import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalTempVariableContents;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.anaphors.ast.spi.AnaphoraResolutionSpi;
import de.monochromata.anaphors.ast.spi.AnaphorsSpi;
import de.monochromata.anaphors.ast.spi.RelatedExpressionsSpi;
import de.monochromata.anaphors.ast.strategy.AnaphorResolutionStrategy;
import de.monochromata.anaphors.preferences.Preferences;
import de.monochromata.eclipse.anaphors.ASTSpis;
import de.monochromata.eclipse.persp.PerspectivationDocument;
import de.monochromata.eclipse.persp.PerspectivationDocumentManager;
import de.monochromata.eclipse.persp.PerspectivationPosition;
import de.monochromata.function.ThrowingConsumer;

public interface UnderspecificationTesting extends ASTTesting {

    default void assertPerspectivationPositionForRelatedExpression(final String source,
            final PublicRelatedExpression relatedExpression,
            final RelatedExpressionStrategy<ASTNode, Type, IBinding, ITypeBinding, CompilationUnit, String, PublicRelatedExpression> strategy,
            final String anaphor, final LocalTempVariableContents variableContents,
            final Consumer<PerspectivationDocument> editingConsumer, final Predicate<DocumentEvent> condition,
            final int expectedOffset, final int expectedLength,
            final BiConsumer<PerspectivationDocument, IDocument> resultAsserter,
            final RelatedExpressionsSpi<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression> relatedExpressionsSpi)
            throws Exception {
        assertPerspectivationPosition(source, editingConsumer, condition,
                () -> relatedExpressionsSpi.createPositionForNode(relatedExpression.getRelatedExpression(), condition,
                        strategy.underspecifyRelatedExpression(relatedExpression,
                                singletonList(new ImmutablePair<>(variableContents, anaphor)), null)),
                expectedOffset, expectedLength, resultAsserter);
    }

    default void assertPerspectivationPositionForAnaphor(final String source,
            final PublicRelatedExpression relatedExpression, final String anaphor, final Expression anaphorExpression,
            final AnaphorResolutionStrategy<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> strategy,
            final Consumer<PerspectivationDocument> editingConsumer, final Predicate<DocumentEvent> condition,
            final int expectedOffset, final int expectedLength,
            final BiConsumer<PerspectivationDocument, IDocument> resultAsserter,
            final AnaphorsSpi<ASTNode, Expression, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition> anaphorsSpi)
            throws Exception {
        assertPerspectivationPosition(source, editingConsumer, condition,
                () -> anaphorsSpi.createPositionForExpression(anaphorExpression, condition,
                        strategy.underspecifyAnaphor(relatedExpression, anaphor, anaphorExpression, null, null)),
                expectedOffset, expectedLength, resultAsserter);
    }

    default void assertPerspectivationPosition(final String source,
            final Consumer<PerspectivationDocument> editingConsumer, final Predicate<DocumentEvent> condition,
            final Supplier<PerspectivationPosition> perspectivationFactory, final int expectedOffset,
            final int expectedLength, final BiConsumer<PerspectivationDocument, IDocument> resultAsserter)
            throws Exception {
        runWithSpis(spis -> {
            final PerspectivationPosition position = perspectivationFactory.get();

            final PerspectivationDocumentManager manager = new PerspectivationDocumentManager();
            final IDocument masterDocument = new Document();
            final PerspectivationDocument perspectivationDocument = manager.createSlaveDocument(masterDocument);
            perspectivationDocument.set(source);

            wrapPositionExceptions(
                    () -> perspectivationDocument.getMasterDocument().addPosition(PERSPECTIVATION_CATEGORY, position));

            editingConsumer.accept(perspectivationDocument);

            assertThat("position.offset=" + position.offset + " position.length=" + position.length)
                    .isEqualTo("position.offset=" + expectedOffset + " position.length=" + expectedLength);

            resultAsserter.accept(perspectivationDocument, masterDocument);
        });
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    default void runWithSpis(final ThrowingConsumer<ASTSpis, Exception> consumer) throws Exception {
        final var anaphorsSpi = mockAnaphorsSpi();
        final var relatedExpressionsSpi = mockRelatedExpressionsSpi();
        final var anaphoraResolutionSpi = mock(AnaphoraResolutionSpi.class);
        final var preferencesSpi = mock(Preferences.class);

        final ASTSpis spis = new ASTSpis(anaphorsSpi, relatedExpressionsSpi, anaphoraResolutionSpi, preferencesSpi);
        consumer.accept(spis);
    }

    default AnaphorsSpi mockAnaphorsSpi() {
        final AnaphorsSpi anaphorsSpi = mock(AnaphorsSpi.class);
        when(anaphorsSpi.getLength("foo")).thenReturn(3);
        return anaphorsSpi;
    }

    default RelatedExpressionsSpi mockRelatedExpressionsSpi() {
        final ITypeBinding typeBinding = mock(ITypeBinding.class);
        final RelatedExpressionsSpi relatedExpressionsSpi = mock(RelatedExpressionsSpi.class);
        when(relatedExpressionsSpi.guessTempName(any(RelatedExpression.class), any(String.class),
                any(LocalTempVariableContents.class), isNull())).thenReturn("foo");
        when(relatedExpressionsSpi.getLocalDeclarationIdentifier(any(Object.class))).thenReturn("foo");
        when(relatedExpressionsSpi.toQualifiedIdentifier("foo")).thenReturn("foo");
        when(relatedExpressionsSpi.resolveClassInstanceCreationType(any(Object.class), isNull()))
                .thenReturn(typeBinding);
        when(typeBinding.getName()).thenReturn("Foo");
        when(relatedExpressionsSpi.getLengthOfSimpleName(typeBinding)).thenReturn(3);
        when(relatedExpressionsSpi.getLength("foo")).thenReturn(3);
        return relatedExpressionsSpi;
    }

}
