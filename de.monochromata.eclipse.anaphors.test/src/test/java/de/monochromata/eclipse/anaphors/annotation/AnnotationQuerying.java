package de.monochromata.eclipse.anaphors.annotation;

import static java.util.Objects.requireNonNull;

import java.util.Iterator;
import java.util.function.Function;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;

import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;

public interface AnnotationQuerying {

    static <A extends Annotation, P extends Position> Stream<Pair<A, P>> getAnnotationsFromEntireDocument(
            final CognitiveEditor editor, final Class<A> annotationType, final Class<P> positionType) {
        final Stream<Pair<Annotation, Position>> annotations = getAnnotationsFromEntireDocument(editor);
        return filterAnnotations(annotationType, annotations);
    }

    @SuppressWarnings("unchecked")
    static <A extends Annotation, P extends Position> Stream<Pair<A, P>> filterAnnotations(
            final Class<A> annotationType, final Stream<Pair<Annotation, Position>> annotations) {
        return annotations.filter(pair -> annotationType.isInstance(pair.getLeft())).map(pair -> (Pair<A, P>) pair);
    }

    static Stream<Pair<Annotation, Position>> getAnnotationsFromEntireDocument(
            final CognitiveEditor editor) {
        return getAnnotations(editor, IAnnotationModel::getAnnotationIterator);
    }

    static Stream<Pair<Annotation, Position>> getAnnotationsFromEntireDocument(final IAnnotationModel model) {
        return getAnnotations(model, IAnnotationModel::getAnnotationIterator);
    }

    static Stream<Pair<Annotation, Position>> getAnnotations(final CognitiveEditor editor,
            final Function<IAnnotationModel, Iterator<Annotation>> annotationIteratorSupplier) {
        final IAnnotationModel annotationModel = getAnnotationModel(editor);
        return getAnnotations(annotationModel, annotationIteratorSupplier);
    }

    static IAnnotationModel getAnnotationModel(final CognitiveEditor editor) {
        return requireNonNull(editor.getDocumentProvider().getAnnotationModel(editor.getEditorInput()));
    }

    static Stream<Pair<Annotation, Position>> getAnnotations(final IAnnotationModel annotationModel,
            final Function<IAnnotationModel, Iterator<Annotation>> annotationIteratorSupplier) {
        final Iterator<Annotation> iter = annotationIteratorSupplier.apply(annotationModel);
        final Stream.Builder<Pair<Annotation, Position>> annotations = Stream.builder();
        while (iter.hasNext()) {
            final Annotation annotation = iter.next();
            if (!annotation.isMarkedDeleted()) {
                final Position position = annotationModel.getPosition(annotation);
                annotations.add(new ImmutablePair<>(annotation, position));
            }
        }
        return annotations.build();
    }

}
