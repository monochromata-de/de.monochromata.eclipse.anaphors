package de.monochromata.eclipse.anaphors.editor;

import org.eclipse.ui.IEditorPart;

import de.monochromata.eclipse.anaphors.swt.SWTAccess;

public interface SynchronousTesting {

    default void runSync(final Runnable runnable, final IEditorPart editor) {
        runSync(runnable, (CognitiveEditor) editor);
    }

    default void runSync(final Runnable runnable, final CognitiveEditor editor) {
        SWTAccess.runSync(editor.getViewer().getTextWidget(), runnable);
    }
}
