package de.monochromata.eclipse.anaphors.editor.simple;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;

public class LVD_IA2F_RfRt_SimpleTest extends AbstractSimpleEditorTest {

	@Override
	public String getExpectedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
		builder.addLine("");
		builder.addLine("public class LVD_IA2F_RfRt_Simple {");
		builder.addLine("");
		builder.addLine("	public static void foo() {");
		builder.addLine("		A foo = new A(\"bar\");");
		builder.addLine("		System.err.println(foo.Field);");
		builder.addLine("	}");
		builder.addLine("");
		builder.addLine("	private static class A {");
		builder.addLine("		public Integer Field;");
		builder.addLine("		");
		builder.addLine("		public A(final String input) {}");
		builder.addLine("	}");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	public String getExpectedUnderspecifiedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
		builder.addLine("");
		builder.addLine("public class LVD_IA2F_RfRt_Simple {");
		builder.addLine("");
		builder.addLine("	public static void foo() {");
		builder.addLine("		A foo = new A(\"bar\");");
		builder.addLine("		System.err.println(field);");
		builder.addLine("	}");
		builder.addLine("");
		builder.addLine("	private static class A {");
		builder.addLine("		public Integer Field;");
		builder.addLine("		");
		builder.addLine("		public A(final String input) {}");
		builder.addLine("	}");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected Pair<Anaphora, Anaphora> getExpectedResolvedAndRealizedAnaphora() {
		return new ImmutablePair<>(new Anaphora("LVD", "IA2F", "RfRt", "barInteger"),
				new Anaphora("LVD", "IA2F", "Rn", "field"));
	}

}
