package de.monochromata.eclipse.anaphors.editor.occurrences;

import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.jdt.internal.core.manipulation.search.IOccurrencesFinder.F_READ_OCCURRENCE;
import static org.eclipse.jdt.internal.core.manipulation.search.IOccurrencesFinder.F_WRITE_OCCURRENCE;

import java.util.stream.Stream;

import org.eclipse.jdt.internal.core.manipulation.search.IOccurrencesFinder.OccurrenceLocation;
import org.junit.Test;

import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class AnaphoraOccurrencesFinderTest implements AnaphoraOccurrencesFinderTesting {

    @Test
    public void returnNoOccurrencesWhenThereAreNoPositions() {
        final AnaphoraOccurrencesFinder finder = new AnaphoraOccurrencesFinder("word",
                (unused0, unused1) -> Stream.empty());
        assertThat(finder.getOccurrences()).isEmpty();
    }

    @Test
    public void returnOccurrencesForAllPositions() {
        final PositionForRelatedExpression relatedExpression = relatedExpressionPosition(0, 2);
        final PositionForAnaphor anaphor1 = anaphorPosition(10, 3, "anaphor1", relatedExpression);
        final PositionForAnaphor anaphor2 = anaphorPosition(20, 4, "anaphor2", relatedExpression);

        final AnaphoraOccurrencesFinder finder = new AnaphoraOccurrencesFinder("word",
                (unused0, unused1) -> Stream.of(relatedExpression, anaphor1, anaphor2));
        final OccurrenceLocation[] occurrences = finder.getOccurrences();
        assertThat(occurrences).hasSize(3);
        assertOccurrence(occurrences[0], 0, 2, F_WRITE_OCCURRENCE, "Related expression");
        assertOccurrence(occurrences[1], 10, 3, F_READ_OCCURRENCE, "Anaphor co-referent to anaphor1");
        assertOccurrence(occurrences[2], 20, 4, F_READ_OCCURRENCE, "Anaphor co-referent to anaphor2");
    }

}
