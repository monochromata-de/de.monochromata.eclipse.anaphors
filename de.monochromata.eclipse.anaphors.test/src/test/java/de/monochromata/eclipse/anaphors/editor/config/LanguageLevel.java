package de.monochromata.eclipse.anaphors.editor.config;

public enum LanguageLevel {

    JLS_8(1.8, "de.monochromata.eclipse.anaphors.test.resources"),
    JLS_10(10, "de.monochromata.eclipse.anaphors.test.resources10");

    public final double javaSpecVersion;
    public final String testProjectName;

    private LanguageLevel(final double javaSpecVersion, final String testProjectName) {
        this.javaSpecVersion = javaSpecVersion;
        this.testProjectName = testProjectName;
    }

}
