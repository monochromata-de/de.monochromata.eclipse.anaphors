package de.monochromata.eclipse.anaphors.editor;

import static java.util.Arrays.stream;
import static org.assertj.core.api.Assertions.assertThat;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IPositionUpdater;

public interface AnaphoraDocumentProviderTesting {

    default void positionCategoryIsAdded(final String category, final IDocument document) {
        assertThat(document.getPositionCategories()).contains(category);
    }

    default void assertPositionUpdaterType(final Class<? extends IPositionUpdater> positionUpdaterType,
            final IDocument document) {
        assertThat(stream(document.getPositionUpdaters()).map(Object::getClass).map(Class::getName))
                .contains(positionUpdaterType.getName());
    }

}
