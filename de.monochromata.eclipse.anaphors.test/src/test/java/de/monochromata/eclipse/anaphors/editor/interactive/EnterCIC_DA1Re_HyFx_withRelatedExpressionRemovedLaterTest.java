package de.monochromata.eclipse.anaphors.editor.interactive;

import static java.util.Arrays.asList;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;
import de.monochromata.eclipse.persp.PerspectivationPosition;

public class EnterCIC_DA1Re_HyFx_withRelatedExpressionRemovedLaterTest
        extends AbstractAnaphoraRemovingTest {

    @Override
    protected String getExpectedKindOfResolvedAnaphora() {
        return "CIC-DA1Re-HyFx";
    }

    @Override
    protected void editFile(final IEditorPart editor) {
        deleteTextOnLine(8);
        getBot().activeEditor().save();
    }

    @Override
    protected String getExpectedIntermediateRefactoredSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
        builder.addLine("");
        builder.addLine("import javax.swing.JButton;");
        builder.addLine("import javax.swing.JPanel;");
        builder.addLine("");
        builder.addLine("public class EnterCIC_DA1Re_HyFx_withRelatedExpressionRemovedLater {");
        builder.addLine("");
        builder.addLine("    public static void main(String[] args) {");
        builder.addLine("        new JPanel();");
        builder.addLine("        final JButton button = new JButton(\"start\");");
        builder.addLine("        System.err.println(button);");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected String getExpectedIntermediateUnderspecifiedRefactoredSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
        builder.addLine("");
        builder.addLine("import javax.swing.JButton;");
        builder.addLine("");
        builder.addLine("public class EnterCIC_DA1Re_HyFx_withRelatedExpressionRemovedLater {");
        builder.addLine("");
        builder.addLine("    public static void main(String[] args) {");
        builder.addLine("        new JPanel();");
        builder.addLine("        new JButton(\"start\");");
        builder.addLine("        System.err.println(button);");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected List<? extends Position> getExpectedIntermediatePositions() {
        final Anaphora anaphora = new Anaphora("LVD", "DA1Re", "Rn", "button");
        return asList(new PerspectivationPosition(263, 44, null, null),
                new PerspectivationPosition(335, 6, null, null),
                new PositionForAnaphor(335, 6, anaphora),
                new PositionForRelatedExpression(263, 44));
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
        builder.addLine("");
        builder.addLine("import javax.swing.JButton;");
        builder.addLine("import javax.swing.JPanel;");
        builder.addLine("");
        builder.addLine("public class EnterCIC_DA1Re_HyFx_withRelatedExpressionRemovedLater {");
        builder.addLine("");
        builder.addLine("    public static void main(String[] args) {");
        builder.addLine("        new JPanel();");
        builder.addLine("        System.err.println(button);");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
        builder.addLine("");
        builder.addLine("import javax.swing.JButton;");
        builder.addLine("");
        builder.addLine("public class EnterCIC_DA1Re_HyFx_withRelatedExpressionRemovedLater {");
        builder.addLine("");
        builder.addLine("    public static void main(String[] args) {");
        builder.addLine("        new JPanel();");
        builder.addLine("        System.err.println(button);");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
        return asList(
                new ImmutablePair<>(new Annotation("org.eclipse.jdt.ui.error", false,
                        "button cannot be resolved to a variable"),
                        new Position(282, 6)));
    }
}
