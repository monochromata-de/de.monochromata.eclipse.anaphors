package de.monochromata.eclipse.anaphors.ast.reference.strategy.concept;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.anaphors.PublicAstFunctions;
import org.eclipse.jdt.core.dom.anaphors.PublicDom;
import org.junit.AfterClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.anaphors.ast.reference.strategy.concept.Hyponymy;
import de.monochromata.contract.environment.junit.FutureJunitContractExtension;
import de.monochromata.eclipse.anaphors.contract.ContractTesting;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HyponymyIntegrationTest implements ContractTesting {

    private static final FutureJunitContractExtension futureContractExtension = new FutureJunitContractExtension(
            HyponymyIntegrationTest.class);
    private static final CompilationUnit scope = mock(CompilationUnit.class);

    // TODO: implement consumers for remaining methods in this test class
    // TODO: Looking at call graphs both bottom-up and top-down, with type
    // information, will allow untested variants to be detected (e.g. when there is
    // an argument subtype that is not yet covered by a test)

    // TODO: Move to java-contract-junit
    @AfterClass
    public static void savePacts() {
        futureContractExtension.save();
    }

    @Test
    public void kind() throws Exception {
        testContract(hyponymy -> {
            assertThat(hyponymy.getKind()).isEqualTo("Hy");
        });
    }

    @Test
    public void canReferTo_false() throws Exception {
        // TODO: Use other means to get to the consumer Id here - when integrated into
        // JUnit 5
        testContract(hyponymy -> {
            final String idFromDefiniteExpression = "super";

            final var potentialReferent = mock(Referent.class);
            final var referentType = mock(ITypeBinding.class);

            when(potentialReferent.resolveType(any())).thenReturn(referentType);
            when(referentType.getSuperclass()).thenReturn(null);
            when(referentType.getInterfaces()).thenReturn(new ITypeBinding[0]);

            final var scope = scope();

            final var result = hyponymy.canReferTo(idFromDefiniteExpression, potentialReferent, scope);
            assertThat(result).isFalse();
        });
    }

    // TODO: Needs more cases like transitive superclass
    @Test
    public void canReferTo_superclass() throws Exception {
        testContract(hyponymy -> {
            final String idFromDefiniteExpression = "super";
            final var superType = type("Super");
            final var referentType = type("Sub", superType);
            final var potentialReferent = potentialReferent(referentType);
            final var scope = scope();
            final var result = hyponymy.canReferTo(idFromDefiniteExpression, potentialReferent, scope);
            assertThat(result).isTrue();
        });
    }

    // TODO: Needs more cases like interfaces implemented by super classes or
    // interface of interface
    @Test
    public void canReferTo_implementedInterface() throws Exception {
        // TODO: Use other means to get to the consumer Id here - when integrated into
        // JUnit 5
        testContract(hyponymy -> {
            final String idFromDefiniteExpression = "interface";
            final var potentialReferent = mock(Referent.class);
            final var referentType = mock(ITypeBinding.class);
            final var interfaceType = mock(ITypeBinding.class);

            when(potentialReferent.resolveType(any())).thenReturn(referentType);
            when(referentType.getName()).thenReturn("Sub");
            when(referentType.getSuperclass()).thenReturn(null);
            when(referentType.getInterfaces()).thenReturn(new ITypeBinding[] { interfaceType });

            when(interfaceType.getName()).thenReturn("Interface");
            when(interfaceType.getErasure()).thenReturn(interfaceType);
            when(interfaceType.getInterfaces()).thenReturn(new ITypeBinding[0]);

            final var scope = scope();

            final var result = hyponymy.canReferTo(idFromDefiniteExpression, potentialReferent, scope);
            assertThat(result).isTrue();
        });
    }

    @Test
    public void canReferToUsingConceptualType_doesNotReferToConceptualTypeOfSuperType() throws Exception {
        testContract(hyponymy -> {
            final var superType = type("Super");
            final var referentType = type("Sub", superType);
            final var potentialReferent = potentialReferent(referentType);

            assertCannotReferToUsingConceptualType(hyponymy, "doesNotMatch", potentialReferent);
        });
    }

    @Test
    public void canReferToUsingConceptualType_idRefersToConceptualTypeOfSuperType() throws Exception {
        testContract(hyponymy -> {
            final var superType = type("Super");
            final var referentType = type("Sub", superType);
            final var potentialReferent = potentialReferent(referentType);

            assertCanReferToUsingConceptualType(hyponymy, "super", potentialReferent);
        });
    }

    @Test
    public void canReferToUsingConceptualType_suffixRefersToConceptualTypeOfSuperType() throws Exception {
        testContract(hyponymy -> {
            final var superType = type("Super");
            final var referentType = type("Sub", superType);
            final var potentialReferent = potentialReferent(referentType);

            assertCanReferToUsingConceptualType(hyponymy, "mySuper", potentialReferent);
        });
    }

    @Test
    public void canReferToUsingConceptualType_idRefersToConceptualTypeOfSuperSuperType() throws Exception {
        testContract(hyponymy -> {
            final var superSuperType = type("Root");
            final var superType = type("Super", superSuperType);
            final var referentType = type("Sub", superType);
            final var potentialReferent = potentialReferent(referentType);

            assertCanReferToUsingConceptualType(hyponymy, "root", potentialReferent);
        });
    }

    @Test
    public void canReferToUsingConceptualType_suffixRefersToConceptualTypeOfSuperSuperType() throws Exception {
        testContract(hyponymy -> {
            final var superSuperType = type("Root");
            final var superType = type("Super", superSuperType);
            final var referentType = type("Sub", superType);
            final var potentialReferent = potentialReferent(referentType);

            assertCanReferToUsingConceptualType(hyponymy, "myRoot", potentialReferent);
        });
    }

    // TODO: Collect upstream providers on the fly while seeing arguments
    // - then upstreamProviders can be serialized before interactions again
    // TODO: Create upstream providers on the fly while seeing arguments
    // TODO: Add a real integration test, where the contract is injected deep in the
    // code to get the real invocations produced by a integration- or e2e test
    // TODO: Start embedding upstream pacts in the pact in question
    // TODO: Draft integration into cucumber-jvm to avoid implementing connection
    // to Pact Broker, JUnit 5 extension etc.

    @Test
    public void canReferToUsingConceptualType_doesNotReferToConceptualTypeOfInterface() throws Exception {
        testContract(hyponymy -> {
            final var interfaces = new ITypeBinding[] { type("Inter") };
            final var referentType = type("Sub", interfaces);
            final var potentialReferent = potentialReferent(referentType);

            assertCannotReferToUsingConceptualType(hyponymy, "doesNotMatch", potentialReferent);
        });
    }

    @Test
    public void canReferToUsingConceptualType_idRefersToConceptualTypeOfInterface() throws Exception {
        testContract(hyponymy -> {
            final var interfaces = new ITypeBinding[] { type("Inter") };
            final var referentType = type("Sub", interfaces);
            final var potentialReferent = potentialReferent(referentType);

            assertCanReferToUsingConceptualType(hyponymy, "inter", potentialReferent);
        });
    }

    @Test
    public void canReferToUsingConceptualType_suffixRefersToConceptualTypeOfInterface() throws Exception {
        testContract(hyponymy -> {
            final var interfaces = new ITypeBinding[] { type("Inter") };
            final var referentType = type("Sub", interfaces);
            final var potentialReferent = potentialReferent(referentType);

            assertCanReferToUsingConceptualType(hyponymy, "myInter", potentialReferent);
        });
    }

    @Test
    public void canReferToUsingConceptualType_idRefersToConceptualTypeOfInterfaceOfInterface() throws Exception {
        testContract(hyponymy -> {
            final var superInterfaces = new ITypeBinding[] { type("Root") };
            final var interfaces = new ITypeBinding[] { type("Super", superInterfaces) };
            final var referentType = type("Sub", interfaces);
            final var potentialReferent = potentialReferent(referentType);

            assertCanReferToUsingConceptualType(hyponymy, "root", potentialReferent);
        });
    }

    @Test
    public void canReferToUsingConceptualType_suffixRefersToConceptualTypeOfInterfaceOfInterface() throws Exception {
        testContract(hyponymy -> {
            final var superInterfaces = new ITypeBinding[] { type("Root") };
            final var interfaces = new ITypeBinding[] { type("Super", superInterfaces) };
            final var referentType = type("Sub", interfaces);
            final var potentialReferent = potentialReferent(referentType);

            assertCanReferToUsingConceptualType(hyponymy, "myRoot", potentialReferent);
        });
    }

    @Test
    public void canReferToUsingConceptualType_idRefersToConceptualTypeOfInterfaceOfSuperType() throws Exception {
        testContract(hyponymy -> {
            final var superInterfaces = new ITypeBinding[] { type("Root") };
            final var superType = type("Super", superInterfaces);
            final var referentType = type("Sub", superType);
            final var potentialReferent = potentialReferent(referentType);

            assertCanReferToUsingConceptualType(hyponymy, "root", potentialReferent);
        });
    }

    @Test
    public void canReferToUsingConceptualType_suffixRefersToConceptualTypeOfInterfaceOfSuperType() throws Exception {
        testContract(hyponymy -> {
            final var superInterfaces = new ITypeBinding[] { type("Root") };
            final var superType = type("Super", superInterfaces);
            final var referentType = type("Sub", superType);
            final var potentialReferent = potentialReferent(referentType);

            assertCanReferToUsingConceptualType(hyponymy, "myRoot", potentialReferent);
        });
    }

    // TODO: What about root/myRoot matching MyType extends DocumentRoot? I guess
    // that would be a combination of Hyponymy and FauxHyponymy ...

    protected void assertCannotReferToUsingConceptualType(final Hyponymy hyponymy,
            final String idFromDefiniteExpression, final Referent potentialReferent) {
        assertThat(canReferToUsingConceptualType(hyponymy, idFromDefiniteExpression, potentialReferent, false))
                .isFalse();
    }

    protected void assertCanReferToUsingConceptualType(final Hyponymy hyponymy, final String idFromDefiniteExpression,
            final Referent potentialReferent) {
        assertThat(canReferToUsingConceptualType(hyponymy, idFromDefiniteExpression, potentialReferent, true)).isTrue();
    }

    protected boolean canReferToUsingConceptualType(final Hyponymy hyponymy, final String idFromDefiniteExpression,
            final Referent potentialReferent, final boolean expectedReturnValue) {
        final var scope = scope();
        // TODO: (1) Feels a bit redundant for a non-upstream mock to first mock and
        // then actually invoke (with an assertion) - maybe change non-upstream mocks to
        // record interactions that are for mocking, see
        // ...provider.mock.InteractionRecording - but this is necessary in case the
        // mock invokes itself or other objects invoke the mock ... maybe change this in
        // the JUnit API and make it sensitive to the StubInfo - when that is equal to
        // the test class or so
        // doReturn(expectedReturnValue).when(hyponymy).canReferToUsingConceptualType(idFromDefiniteExpression,
        // potentialReferent, scope);
        return hyponymy.canReferToUsingConceptualType(idFromDefiniteExpression, potentialReferent, scope);
    }

    // TODO: Implement all cases mentioned in ConceptReferentializationStrategy
    // class JavaDoc + add transitive cases
    @Test
    @Ignore
    public void getFeaturesRemainingInIdentifierIfItCanReferUsingConceptualType() throws Exception {
        testContract(hyponymy -> {
            // TODO: Method is not yet implemented:
            // hyponymy.getFeaturesRemainingInIdentifierIfItCanReferUsingConceptualType(idFromDefiniteExpression,
            // potentialReferent, scope);
            fail("TODO: Do the actual stubbing/testing");
        });
    }

    protected CompilationUnit scope() {
        return scope;
    }

    protected Referent potentialReferent(final ITypeBinding referentType) {
        final var potentialReferent = mock(Referent.class);
        when(potentialReferent.resolveType(any())).thenReturn(referentType);
        return potentialReferent;
    }

    protected ITypeBinding type(final String name) {
        return type(name, (ITypeBinding) null);
    }

    protected ITypeBinding type(final String name, final ITypeBinding superType) {
        return type(name, superType, null);
    }

    protected ITypeBinding type(final String name, final ITypeBinding[] interfaces) {
        return type(name, null, interfaces);
    }

    protected ITypeBinding type(final String name, final ITypeBinding superType, final ITypeBinding[] interfaces) {
        final var type = mock(ITypeBinding.class);
        when(type.getName()).thenReturn(name);
        when(type.getErasure()).thenReturn(type);
        when(type.getSuperclass()).thenReturn(superType);
        when(type.getInterfaces()).thenReturn(interfaces != null ? interfaces : new ITypeBinding[0]);
        return type;
    }

    // TODO: Move the frameworky methods to java-contract (a dedicated artifact
    // java-contract-junit5 or so) and unit-test them
    protected void testContract(final Consumer<Hyponymy> test) throws Exception {
        final var hyponymy = proxyProvider();

        test.accept(hyponymy);
    }

    protected Hyponymy proxyProvider() throws NoSuchMethodException {
        final var anaphorsSpi = new PublicDom();
        final Function<ITypeBinding, Optional<ITypeBinding>> getSuperClass = PublicAstFunctions::getSuperClass;
        final Function<ITypeBinding, List<ITypeBinding>> getImplementedInterfaces = PublicAstFunctions::getImplementedInterfaces;

        final var providerInstance = new Hyponymy(anaphorsSpi, getSuperClass, getImplementedInterfaces);
        return proxyHyponymy(providerInstance);
    }

    public Hyponymy proxyHyponymy(final Hyponymy providerInstance) {
        return futureContractExtension.proxy(providerInstance);
    }
}
