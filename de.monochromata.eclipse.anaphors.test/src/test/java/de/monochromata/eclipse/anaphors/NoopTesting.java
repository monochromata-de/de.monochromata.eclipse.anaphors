package de.monochromata.eclipse.anaphors;

public interface NoopTesting {

    default void noop() {
    }

    default <T1> void noop(final T1 arg1) {
    }

    default <T1, T2> void noop(final T1 arg1, final T2 arg2) {
    }

}
