package de.monochromata.eclipse.anaphors.editor.simple.feature;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.eclipse.jface.text.Position;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class MultipleInterdependentProblems2xDA1ReTest extends AbstractSimpleFeatureEditorTest {

    @Override
    public void waitForAnaphoraRelations(final CognitiveEditor editor) {
        waitForAnaphoraRelations(editor, 2);
    }

    @Override
    public void assertAnaphoraRelations() {
        assertThat(getAddedAnaphoraRelations())
                .containsExactly(
                        new ImmutablePair<>(new Anaphora("CIC", "DA1Re", "RfRt", "helloString"),
                                new Anaphora("LVD", "DA1Re", "Rn", "helloString")),
                        new ImmutablePair<>(new Anaphora("CIC", "DA1Re", "RfRt", "helloString"),
                                new Anaphora("LVD", "DA1Re", "Rn", "helloString")));
    }

    @Override
    protected void manipulateEditorAndCheckAssertions(final ProblemCollectingChangeListener problemCollector,
            final CognitiveEditor editor) {
        super.manipulateEditorAndCheckAssertions(problemCollector, editor);
        waitForExpectedPositions(editor, getExpectedPositions());
        assertGivenPositions(editor, getExpectedPositions());
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple.feature;");
        builder.addLine("");
        builder.addLine("public class MultipleInterdependentProblems2xDA1Re {");
        builder.addLine("");
        builder.addLine("    public static void foo() {");
        builder.addLine("        final String helloString = new String(\"Hello\");");
        builder.addLine("        new String(\"Bello\");");
        builder.addLine("        System.err.println(helloString);");
        builder.addLine("        System.err.println(helloString);");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple.feature;");
        builder.addLine("");
        builder.addLine("public class MultipleInterdependentProblems2xDA1Re {");
        builder.addLine("");
        builder.addLine("    public static void foo() {");
        builder.addLine("        new String(\"Hello\");");
        builder.addLine("        new String(\"Bello\");");
        builder.addLine("        System.err.println(helloString);");
        builder.addLine("        System.err.println(helloString);");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    protected List<? extends Position> getExpectedPositions() {
        final Anaphora anaphora = new Anaphora("LVD", "DA1Re", "Rn", "helloString");
        final PositionForRelatedExpression relatedExpression = new PositionForRelatedExpression(158, 47);
        final PositionForAnaphor anaphor1 = new PositionForAnaphor(262, 11, anaphora);
        final PositionForAnaphor anaphor2 = new PositionForAnaphor(303, 11, anaphora);

        return asList(relatedExpression, anaphor1, anaphor2);
    }

}
