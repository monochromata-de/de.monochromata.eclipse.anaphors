package de.monochromata.eclipse.anaphors.editor.interactive;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class PD_IA2Mg_Rn_UpdatedTo_CIC_DA1Re_RtTest extends AbstractInteractiveDoubleAssertionEditorTest {

	@Override
	protected String getExpectedKindOfResolvedAnaphora() {
		return "PD-IA2Mg-Rn";
	}

	@Override
	protected String getExpectedIntermediateRefactoredSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("public class PD_IA2Mg_Rn_UpdatedTo_CIC_DA1Re_Rt {");
		builder.addLine("");
		builder.addLine("    public static void foo(A foo) {");
		builder.addLine("        System.err.println(foo.getB());");
		builder.addLine("    }");
		builder.addLine("");
		builder.addLine("    private static class A {");
		builder.addLine("        public B getB() {");
		builder.addLine("            return new B();");
		builder.addLine("        }");
		builder.addLine("    }");
		builder.addLine("");
		builder.addLine("    private static class B {");
		builder.addLine("    }");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected String getExpectedIntermediateUnderspecifiedRefactoredSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("public class PD_IA2Mg_Rn_UpdatedTo_CIC_DA1Re_Rt {");
		builder.addLine("");
		builder.addLine("    public static void foo(A foo) {");
		builder.addLine("        System.err.println(b);");
		builder.addLine("    }");
		builder.addLine("");
		builder.addLine("    private static class A {");
		builder.addLine("        public B getB() {");
		builder.addLine("            return new B();");
		builder.addLine("        }");
		builder.addLine("    }");
		builder.addLine("");
		builder.addLine("    private static class B {");
		builder.addLine("    }");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected List<Pair<? extends Annotation, ? extends Position>> getExpectedIntermediateAnnotations() {
		return emptyList();
	}

	@Override
	protected List<? extends Position> getExpectedIntermediatePositions() {
		final Anaphora anaphora = new Anaphora("PD", "IA2Mg", "Rn", "b");
		return asList(new PositionForAnaphor(176, 10, anaphora), new PositionForRelatedExpression(140, 5));
	}

	@Override
	protected void editFile(final IEditorPart editor) {
		getBot().activeEditor().toTextEditor().insertText(5, 7, "new B();\n       ");
		getBot().activeEditor().save();
	}

	@Override
	public String getExpectedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("public class PD_IA2Mg_Rn_UpdatedTo_CIC_DA1Re_Rt {");
		builder.addLine("");
		builder.addLine("    public static void foo(A foo) {");
		builder.addLine("        final B b = new B();");
		builder.addLine("        System.err.println(b);");
		builder.addLine("    }");
		builder.addLine("");
		builder.addLine("    private static class A {");
		builder.addLine("        public B getB() {");
		builder.addLine("            return new B();");
		builder.addLine("        }");
		builder.addLine("    }");
		builder.addLine("");
		builder.addLine("    private static class B {");
		builder.addLine("    }");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	public String getExpectedUnderspecifiedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
		builder.addLine("");
		builder.addLine("public class PD_IA2Mg_Rn_UpdatedTo_CIC_DA1Re_Rt {");
		builder.addLine("");
		builder.addLine("    public static void foo(A foo) {");
		builder.addLine("        new B();");
		builder.addLine("        System.err.println(b);");
		builder.addLine("    }");
		builder.addLine("");
		builder.addLine("    private static class A {");
		builder.addLine("        public B getB() {");
		builder.addLine("            return new B();");
		builder.addLine("        }");
		builder.addLine("    }");
		builder.addLine("");
		builder.addLine("    private static class B {");
		builder.addLine("    }");
		builder.addLine("");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected int getExpectedNumberOfAnaphoraRelationsAfterEdit() {
		return 1;
	}

	@Override
	protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
		return asList(
				new ImmutablePair<>(new Annotation("org.eclipse.jdt.ui.warning", false,
						"The method getB() from the type PD_IA2Mg_Rn_UpdatedTo_CIC_DA1Re_Rt.A is never used locally"),
						new Position(260, 6)),
				new ImmutablePair<>(new Annotation("org.eclipse.ui.workbench.texteditor.quickdiffUnchanged", false,
						"19 changed lines "), new Position(0, 355)));
	}

	@Override
	protected List<? extends Position> getExpectedPositions() {
		final Anaphora anaphora = new Anaphora("LVD", "DA1Re", "Rn", "b");
		return asList(new PositionForAnaphor(203, 1, anaphora), new PositionForRelatedExpression(154, 20));
	}

}
