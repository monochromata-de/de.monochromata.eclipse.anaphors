package de.monochromata.eclipse.anaphors.editor.interactive.feature.perspectivation;

import static java.util.Collections.emptyList;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy;
import de.monochromata.eclipse.anaphors.Activator;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.editor.interactive.AbstractInteractiveEditorTest;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;

public abstract class AbstractPerspectivationStrategyChangeTest extends AbstractInteractiveEditorTest {

    @Override
    protected void manipulateEditorAndCheckAssertions(final ProblemCollectingChangeListener problemCollector,
            final CognitiveEditor editor) {

        enableMarkOccurrences();
        waitForAnaphoraRelations(editor, 1);
        waitForProblemsToBeSolved(editor);
        assertRefactoringResults(editor, problemCollector);
        waitForSharedASTToBeReconciled(editor);
        waitForExpectedAnnotations(editor, getExpectedAnnotations());
        assertAnnotations(editor, getExpectedAnnotations());
        waitForExpectedPositions(editor, getExpectedPositions());
        assertGivenPositions(editor, getExpectedPositions());

        changePerspectivationStrategy();

        assertUnderspecifiedEventualSourceCode(editor, getExpectedUnderspecifiedRefactoredSourceCodeAfterChange());
    }

    @Override
    protected String getExpectedKindOfResolvedAnaphora() {
        return "CIC-IA2Mg-Rn";
    }

    @Override
    protected PerspectivationStrategy getPerspectivationStrategy() {
        return getInitialPerspectivationStrategy();
    }

    protected abstract PerspectivationStrategy getInitialPerspectivationStrategy();

    protected void changePerspectivationStrategy() {
        Activator.getDefault().setPerspectivationStrategy(getUpdatedPerspectivationStrategy());
    }

    protected abstract PerspectivationStrategy getUpdatedPerspectivationStrategy();

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature.perspectivation;");
        builder.addLine("");
        builder.addLine("public class " + getTestTypeName() + " {");
        builder.addLine("");
        builder.addLine("   public static void main(String[] args) {");
        builder.addLine("       final A a = new A();");
        builder.addLine("       System.err.println(a.getValue());");
        builder.addLine("   }");
        builder.addLine("");
        builder.addLine("   private static class A {");
        builder.addLine("       public Integer getValue() {");
        builder.addLine("           return 10;");
        builder.addLine("       }");
        builder.addLine("   }");
        builder.addLine("}");
        return builder.toString();
    }

    private String getTestTypeName() {
        final String testName = this.getClass().getSimpleName();
        final int endIndex = testName.length() - "Test".length();
        return testName.substring(0, endIndex);
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature.perspectivation;");
        builder.addLine("");
        builder.addLine("public class " + getTestTypeName() + " {");
        builder.addLine("");
        builder.addLine("   public static void main(String[] args) {");
        builder.addLine("       " + getInitialRelatedExpressionImage());
        builder.addLine("       System.err.println(" + getInitialAnaphorImage() + ");");
        builder.addLine("   }");
        builder.addLine("");
        builder.addLine("   private static class A {");
        builder.addLine("       public Integer getValue() {");
        builder.addLine("           return 10;");
        builder.addLine("       }");
        builder.addLine("   }");
        builder.addLine("}");
        return builder.toString();
    }

    protected abstract String getInitialRelatedExpressionImage();

    protected abstract String getInitialAnaphorImage();

    @Override
    protected void editFile(final IEditorPart editor) {
        throw new UnsupportedOperationException(
                "Perspectivation strategy update tests should not edit the file "
                        + "but merely change the perspectivation strategy.");
    }

    @Override
    protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
        return emptyList();
    }

    @Override
    protected List<? extends Position> getExpectedPositions() {
        final Anaphora anaphora = new Anaphora("LVD", "IA2Mg", "Rn", "value");
        return getExpectedPositions(anaphora);
    }

    protected abstract List<AbstractAnaphoraPositionForRepresentation> getExpectedPositions(final Anaphora anaphora);

    protected String getExpectedUnderspecifiedRefactoredSourceCodeAfterChange() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature.perspectivation;");
        builder.addLine("");
        builder.addLine("public class " + getTestTypeName() + " {");
        builder.addLine("");
        builder.addLine("   public static void main(String[] args) {");
        builder.addLine("       " + getRelatedExpressionImageAfterChange());
        builder.addLine("       System.err.println(" + getAnaphorImageAfterChange() + ");");
        builder.addLine("   }");
        builder.addLine("");
        builder.addLine("   private static class A {");
        builder.addLine("       public Integer getValue() {");
        builder.addLine("           return 10;");
        builder.addLine("       }");
        builder.addLine("   }");
        builder.addLine("}");
        return builder.toString();
    }

    protected abstract String getRelatedExpressionImageAfterChange();

    protected abstract String getAnaphorImageAfterChange();

}
