package de.monochromata.eclipse.anaphors.editor.interactive;

import static java.util.stream.Collectors.joining;
import static org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable.syncExec;

import java.util.List;
import java.util.stream.IntStream;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;

import de.monochromata.eclipse.anaphors.editor.AbstractCognitiveEditorTest;
import de.monochromata.eclipse.anaphors.editor.AbstractEditorTest.ProblemCollectingChangeListener;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;

/**
 * An abstract base class for simple tests of user interaction with
 * {@link CognitiveEditor} that enter a single anaphor.
 */
public abstract class AbstractInteractiveEditorTest extends AbstractCognitiveEditorTest
        implements MarkOccurrencesTesting {

    /**
     * Returns the kind of anaphor between the prefix "Enter" and the suffix "Test".
     */
    @Override
    protected String getExpectedKindOfResolvedAnaphora() {
        final String kindWithDescription = getKindWithDescription();
        final String[] parts = kindWithDescription.split("_");
        return IntStream.rangeClosed(0, 2).mapToObj(i -> parts[i]).collect(joining("-"));
    }

    protected String getKindWithDescription() {
        final String className = getClass().getSimpleName();
        final int prefixLength = "Enter".length();
        final int suffixLength = "Test".length();
        return className.substring(prefixLength, className.length() - suffixLength);
    }

    @Override
    protected void manipulateEditorAndCheckAssertions(final ProblemCollectingChangeListener problemCollector,
            final CognitiveEditor editor) {
        enableMarkOccurrences();
        editFile(editor);
        syncExec(() -> editor.doSave(null));
        waitForAnaphoraRelations(editor, 1);
        waitForProblemsToBeSolved(editor);
        assertRefactoringResults(editor, problemCollector);
        waitForSharedASTToBeReconciled(editor);
        waitForExpectedAnnotations(editor, getExpectedAnnotations());
        assertAnnotations(editor, getExpectedAnnotations());
        waitForExpectedPositions(editor, getExpectedPositions());
        assertGivenPositions(editor, getExpectedPositions());
    }

    protected abstract void editFile(IEditorPart editor);

    protected abstract List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations();

    protected abstract List<? extends Position> getExpectedPositions();

}
