package de.monochromata.eclipse.anaphors.editor;

import static de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPosition.MEMBER_TRACKING_CATEGORY;
import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static de.monochromata.eclipse.anaphors.position.KnownProblemPosition.KNOWN_PROBLEM_CATEGORY;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.DefaultPositionUpdater;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentListener;
import org.junit.Test;

import de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPosition;
import de.monochromata.eclipse.anaphors.position.AnaphoraPositionUpdater;
import de.monochromata.eclipse.anaphors.position.KnownProblemPositionUpdater;

public class AnaphoraDocumentProviderTest implements AnaphoraDocumentProviderTesting {

    private final IDocument document = new Document();

    {
        AnaphoraDocumentProvider.configure(document, anaphoras -> {
        });
    }

    @Test
    public void anaphoraPositionCategoryIsAdded() {
        positionCategoryIsAdded(ANAPHORA_CATEGORY, document);
    }

    @Test
    public void anaphoraPpositionUpdaterIsAdded() {
        assertPositionUpdaterType(AnaphoraPositionUpdater.class, document);
    }

    @Test
    public void knownProblemPositionCategoryIsAdded() {
        positionCategoryIsAdded(KNOWN_PROBLEM_CATEGORY, document);
    }

    @Test
    public void knownProblemPositionUpdaterIsAdded() {
        assertPositionUpdaterType(KnownProblemPositionUpdater.class, document);
    }

    @Test
    public void memberTrackingPositionCategoryIsAdded() {
        positionCategoryIsAdded(MEMBER_TRACKING_CATEGORY, document);
    }

    @Test
    public void memberTrackingPositionUpdaterIsAdded() {
        assertPositionUpdaterType(DefaultPositionUpdater.class, document);
    }

    @Test
    public void memberTrackingPositionsAreUpdatedOnEdits() throws Exception {
        document.set("The quick brown fox");
        final MemberTrackingPosition position = new MemberTrackingPosition(16, 3, null);
        document.addPosition(MEMBER_TRACKING_CATEGORY, position);

        document.replace(4, 6, "");

        assertThat(position.offset).isEqualTo(10);
        assertThat(position.length).isEqualTo(3);
    }

    @Test
    public void memberTrackingPositionsAreUpdatedAfterDocumentAboutToBeChangedEvent() throws Exception {
        final AtomicReference<Pair<Integer, Integer>> offsetAndLengthDuringAboutToBeChangedEvent = new AtomicReference<>();
        document.set("The quick brown fox");
        final MemberTrackingPosition position = new MemberTrackingPosition(16, 3, null);
        document.addPosition(MEMBER_TRACKING_CATEGORY, position);
        document.addDocumentListener(new IDocumentListener() {
            @Override
            public void documentAboutToBeChanged(final DocumentEvent event) {
                offsetAndLengthDuringAboutToBeChangedEvent.set(new ImmutablePair<>(position.offset, position.length));
            }

            @Override
            public void documentChanged(final DocumentEvent event) {
            }
        });

        document.replace(4, 6, "");

        assertThat(offsetAndLengthDuringAboutToBeChangedEvent.get()).isEqualTo(new ImmutablePair<>(16, 3));

        assertThat(position.offset).isEqualTo(10);
        assertThat(position.length).isEqualTo(3);
    }

    @Test
    public void memberTrackingPositionsAreUpdatedBeforeDocumentChangedEvent() throws Exception {
        final AtomicReference<Pair<Integer, Integer>> offsetAndLengthAfterDocumentChangedEvent = new AtomicReference<>();
        document.set("The quick brown fox");
        final MemberTrackingPosition position = new MemberTrackingPosition(16, 3, null);
        document.addPosition(MEMBER_TRACKING_CATEGORY, position);
        document.addDocumentListener(new IDocumentListener() {
            @Override
            public void documentAboutToBeChanged(final DocumentEvent event) {
            }

            @Override
            public void documentChanged(final DocumentEvent event) {
                offsetAndLengthAfterDocumentChangedEvent.set(new ImmutablePair<>(position.offset, position.length));
            }
        });

        document.replace(4, 6, "");

        assertThat(offsetAndLengthAfterDocumentChangedEvent.get()).isEqualTo(new ImmutablePair<>(10, 3));

        assertThat(position.offset).isEqualTo(10);
        assertThat(position.length).isEqualTo(3);
    }

    @Test
    public void memberTrackingPositionsWithLength0AreRetained() throws Exception {
        document.set("The quick brown fox");
        final MemberTrackingPosition position = new MemberTrackingPosition(16, 3, null);
        document.addPosition(MEMBER_TRACKING_CATEGORY, position);

        document.replace(16, 3, "");

        assertThat(position.offset).isEqualTo(16);
        assertThat(position.length).isEqualTo(0);
        assertThat(position.isDeleted).isFalse();
    }

    @Test
    public void memberTrackingPositionsMayBeRemoved() throws Exception {
        document.set("The quick brown fox");
        final MemberTrackingPosition position = new MemberTrackingPosition(10, 5, null);
        document.addPosition(MEMBER_TRACKING_CATEGORY, position);

        document.replace(9, 7, "");

        assertThat(position.isDeleted).isTrue();
    }

}
