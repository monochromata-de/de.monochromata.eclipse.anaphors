package de.monochromata.eclipse.anaphors.editor.interactive.feature;

import static java.util.Arrays.asList;
import static org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable.syncExec;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.editor.interactive.AbstractInteractiveEditorTest;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class RemoveAmbiguityMarkerWhenAnaphorIsReplacedByLocalVariableReferenceTest
		extends AbstractInteractiveEditorTest {

	@Override
	protected String getExpectedKindOfResolvedAnaphora() {
		return "LVD-DA1Re-Rn";
	}

	@Override
	protected void manipulateEditorAndCheckAssertions(final ProblemCollectingChangeListener problemCollector,
			final CognitiveEditor editor) {
		enableMarkOccurrences();
		waitForExpectedAnnotations(editor, getExpectedIntermediateAnnotations());
		assertAnnotations(editor, getExpectedIntermediateAnnotations());

		editFile(editor);
		syncExec(() -> editor.doSave(null));

		waitForSharedASTToBeReconciled(editor);
		waitForAllAnnotations(editor, getExpectedAnnotations());
		assertAllAnnotations(editor, getExpectedAnnotations());
		waitForRemovalOfPositionsOfBacklistedTypes(editor, PositionForRelatedExpression.class,
				PositionForAnaphor.class);
		assertPositionsDoNotHaveType(editor, PositionForRelatedExpression.class, PositionForAnaphor.class);
	}

	protected List<Pair<? extends Annotation, ? extends Position>> getExpectedIntermediateAnnotations() {
		return asList(new ImmutablePair<>(new Annotation("org.eclipse.jdt.ui.error", false,
				"Ambiguous reference (more than 1 potential referents found):\n"
						+ "final Integer foo at 5:27 (PD-DA1Re-Rt)\n" + "final Integer bar = 0; at 6:8 (LVD-DA1Re-Rt)"),
				new Position(259, 7)));
	}

	@Override
	protected void editFile(final IEditorPart editor) {
		deleteTextOnLine(5);
		deleteTextOnLine(4);
		getBot().activeEditor().toTextEditor().insertText(4, 0, "public static void foo(final Integer integer) {\n");
		getBot().activeEditor().save();
	}

	@Override
	public String getExpectedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature;");
		builder.addLine("");
		builder.addLine("public class RemoveAmbiguityMarkerWhenAnaphorIsReplacedByLocalVariableReference {");
		builder.addLine("");
		builder.addLine("    public static void foo(final Integer integer) {");
		builder.addLine("        System.err.println(integer);");
		builder.addLine("    }");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	public String getExpectedUnderspecifiedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature;");
		builder.addLine("");
		builder.addLine("public class RemoveAmbiguityMarkerWhenAnaphorIsReplacedByLocalVariableReference {");
		builder.addLine("");
		builder.addLine("    public static void foo(final Integer integer) {");
		builder.addLine("        System.err.println(integer);");
		builder.addLine("    }");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
		return asList(new ImmutablePair<>(
				new Annotation("org.eclipse.ui.workbench.texteditor.quickdiffUnchanged", false, "9 changed lines "),
				new Position(0, 247)));
	}

	@Override
	protected List<? extends Position> getExpectedPositions() {
		final Anaphora anaphora = new Anaphora("LVD", "DA1Re", "Rn", "integer");
		final PositionForRelatedExpression relatedExpression = new PositionForRelatedExpression(157, 47);
		final PositionForAnaphor anaphor = new PositionForAnaphor(261, 11, anaphora);
		return asList(relatedExpression, anaphor);
	}

}
