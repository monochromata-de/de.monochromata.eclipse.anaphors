/**
 * Tests for
 * {@link de.monochromata.eclipse.anaphors.editor.CognitiveEditor}
 * that test interactive functionality of the editor.
 * 
 * @author monochromata
 *
 */
package de.monochromata.eclipse.anaphors.editor.interactive;