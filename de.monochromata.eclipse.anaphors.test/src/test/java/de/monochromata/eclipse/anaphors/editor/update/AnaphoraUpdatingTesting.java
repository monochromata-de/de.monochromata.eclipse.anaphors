package de.monochromata.eclipse.anaphors.editor.update;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;

import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.anaphors.ast.strategy.AnaphorResolutionStrategy;

public interface AnaphoraUpdatingTesting extends InvocationRejection {

	default Pair mockAnaphora(final String anaphor) {
		final AST ast = AST.newAST(AST.JLS8);

		final RelatedExpressionPart relatedExpressionPart = mock(RelatedExpressionPart.class);
		final RelatedExpressionStrategy relatedExpressionStrategy = mock(RelatedExpressionStrategy.class);
		final PublicRelatedExpression relatedExpression = new PublicRelatedExpression(false, () -> null,
				createAstNode("relatedExpression", 0, 10, ast), relatedExpressionStrategy, null, null, null, null, null,
				null);

		final AnaphorPart anaphorPart = mock(AnaphorPart.class);
		final AnaphorResolutionStrategy anaphorResolutionStrategy = mock(AnaphorResolutionStrategy.class);
		final ReferentializationStrategy referentializationStrategy = mock(ReferentializationStrategy.class);

		when(relatedExpressionPart.getRelatedExpression()).thenReturn(relatedExpression);
		when(relatedExpressionStrategy.getKind()).thenReturn("reKind");
		when(anaphorPart.getAnaphor()).thenReturn(anaphor);
		when(anaphorPart.getAnaphorExpression()).thenReturn(createAstNode("anaphor", 10, 10, ast));
		when(anaphorPart.getAnaphorResolutionStrategy()).thenReturn(anaphorResolutionStrategy);
		when(anaphorResolutionStrategy.getKind()).thenReturn("arKind");
		when(anaphorPart.getReferentializationStrategy()).thenReturn(referentializationStrategy);
		when(referentializationStrategy.getKind()).thenReturn("refKind");
		return new ImmutablePair(relatedExpressionPart, anaphorPart);
	}

	default SimpleName createAstNode(final String identifier, final int startPosition, final int length,
			final AST ast) {
		final SimpleName relatedExpressionNode = ast.newSimpleName(identifier);
		relatedExpressionNode.setSourceRange(startPosition, length);
		return relatedExpressionNode;
	}
}
