package de.monochromata.eclipse.anaphors.position;

import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static de.monochromata.eclipse.anaphors.position.AnaphoraPositionCreationTesting.addPositionForRelatedExpression;
import static de.monochromata.eclipse.anaphors.position.AnaphoraPositionCreationTesting.createAndLinkAndAddPositionForAnaphor;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.junit.Before;
import org.junit.Test;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.AnaphoraTesting;
import de.monochromata.eclipse.persp.PerspectivationPosition;

public class AnaphoraPositionUpdaterTest implements AnaphoraTesting, AnaphoraPositionCreationTesting {

	private final List<List<Anaphora>> deletionEvents = new ArrayList<>();
	private IDocument document;
	private AnaphoraPositionUpdater<AbstractAnaphoraPositionForRepresentation> updater;

	@Before
	public void createDocument() {
		document = new Document("The quick brown fox jumps over the lazy dog.");
		document.addPositionCategory(ANAPHORA_CATEGORY);
		updater = new AnaphoraPositionUpdater<>(anaphoras -> deletionEvents.add(anaphoras));
		document.addPositionUpdater(updater);
	}

	@Test
	public void removedEventIsSentWhenAnaphorIsRemoved() throws Exception {
		final Anaphora anaphora = createAnaphora("anaphor");
		final PositionForAnaphor position = new PositionForAnaphor((PerspectivationPosition) null, anaphora);

		updater.deletePositionIfNotYetDeleted(position, document, null);

		assertThat(deletionEvents).containsExactly(singletonList(anaphora));
		assertAnaphoraPositions(document, emptyList());
	}

	@Test
	public void removedEventIsSentOnlyForRemovedAnaphorOfTwoAnaphorsOfARelatedExpression() throws Exception {
		final PositionForRelatedExpression positionForRelatedExpression = addPositionForRelatedExpression(document,
				new PerspectivationPosition(0, 10));
		final Anaphora anaphora1 = createAnaphora("anaphor1");
		final PositionForAnaphor anaphorPosition1 = createAndLinkAndAddPositionForAnaphor(document,
				positionForRelatedExpression, anaphora1, new PerspectivationPosition(20, 3));
		final Anaphora anaphora2 = createAnaphora("anaphor2");
		final PositionForAnaphor anaphorPosition2 = createAndLinkAndAddPositionForAnaphor(document,
				positionForRelatedExpression, anaphora2, new PerspectivationPosition(30, 3));

		updater.deletePositionIfNotYetDeleted(anaphorPosition1, document, null);

		assertThat(deletionEvents).containsExactly(singletonList(anaphora1));

		assertAnaphoraPositions(document, asList(positionForRelatedExpression, anaphorPosition2));
	}

	@Test
	public void removedEventIsSentForAllAnaphorsRelatedToRemovedRelatedExpression() throws Exception {
		final PositionForRelatedExpression positionForRelatedExpression = addPositionForRelatedExpression(document,
				new PerspectivationPosition(0, 10));
		final Anaphora anaphora1 = createAnaphora("anaphor1");
		final PositionForAnaphor anaphorPosition1 = createAndLinkAndAddPositionForAnaphor(document,
				positionForRelatedExpression, anaphora1, new PerspectivationPosition(20, 3));
		final Anaphora anaphora2 = createAnaphora("anaphor2");
		final PositionForAnaphor anaphorPosition2 = createAndLinkAndAddPositionForAnaphor(document,
				positionForRelatedExpression, anaphora2, new PerspectivationPosition(30, 3));

		updater.deletePositionIfNotYetDeleted(positionForRelatedExpression, document, null);

		assertThat(deletionEvents).containsExactly(asList(anaphora1, anaphora2));
		assertAnaphoraPositions(document, emptyList());
	}

}
