package de.monochromata.eclipse.anaphors.editor.occurrences;

import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.core.runtime.jobs.Job.SHORT;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.AnnotationModel;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.junit.Test;

import de.monochromata.eclipse.anaphors.NoopTesting;

public class RemoveResolutionOccurrencesJobTest implements NoopTesting {

    private final Object lock = new Object();
    private final IAnnotationModel annotationModel = new AnnotationModel();

    @Test
    public void isSystemJob() {
        final RemoveResolutionOccurrencesJob job = new RemoveResolutionOccurrencesJob(null, null, null, null);

        assertThat(job.isSystem()).isTrue();
    }

    @Test
    public void hasShortPriority() {
        final AtomicReference<Pair<Job, Integer>> priorizedJob = new AtomicReference<>();
        new RemoveResolutionOccurrencesJob(null, null, null, null, this::noop,
                (job, priority) -> priorizedJob.set(new ImmutablePair<>(job, priority)));

        assertThat(priorizedJob.get().getRight()).isEqualTo(SHORT);
    }

    @Test
    public void removesExistingAnnotationsFromTheAnnotationModel() {
        final Annotation existingAnnotation = new Annotation(false);
        annotationModel.addAnnotation(existingAnnotation, new Position(0, 10));
        final RemoveResolutionOccurrencesJob job = new RemoveResolutionOccurrencesJob(
                new Annotation[] { existingAnnotation },
                lock, annotationModel, this::noop);

        job.run(null);

        assertThat(annotationModel.getPosition(existingAnnotation)).isNull();
    }

    @Test
    public void setsExistingAnnotationsToEmptyArray() {
        final AtomicBoolean resetInvoked = new AtomicBoolean(false);
        final RemoveResolutionOccurrencesJob job = new RemoveResolutionOccurrencesJob(null, lock, annotationModel,
                () -> resetInvoked.set(true),
                this::noop, this::noop);

        job.run(null);

        assertThat(resetInvoked.get()).isTrue();
    }

}
