package de.monochromata.eclipse.anaphors.editor.interactive;

import static java.util.Arrays.asList;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class EnterCIC_DA1Re_HyFx_withAmbiguityIntroducedByEditTest
        extends AbstractAnaphoraRemovingTest {

    @Override
    public void assertAnaphoraRelations() {
        // There should be no anaphora relations eventually. Their absence is asserted
        // indirectly via the absence of anaphora positions.
    }

    @Override
    protected String getExpectedIntermediateRefactoredSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
        builder.addLine("");
        builder.addLine("import javax.swing.JButton;");
        builder.addLine("import javax.swing.JPanel;");
        builder.addLine("");
        builder.addLine("public class EnterCIC_DA1Re_HyFx_withAmbiguityIntroducedByEdit {");
        builder.addLine("");
        builder.addLine("   public static void main(String[] args) {");
        builder.addLine("       new JPanel();");
        builder.addLine("       final JButton button = new JButton(\"start\");");
        builder.addLine("   System.err.println(button);");
        builder.addLine("   }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected String getExpectedIntermediateUnderspecifiedRefactoredSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
        builder.addLine("");
        builder.addLine("import javax.swing.JButton;");
        builder.addLine("");
        builder.addLine("public class EnterCIC_DA1Re_HyFx_withAmbiguityIntroducedByEdit {");
        builder.addLine("");
        builder.addLine("   public static void main(String[] args) {");
        builder.addLine("       new JPanel();");
        builder.addLine("       new JButton(\"start\");");
        builder.addLine("   System.err.println(button);");
        builder.addLine("   }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected List<? extends Position> getExpectedIntermediatePositions() {
        final Anaphora anaphora = new Anaphora("LVD", "DA1Re", "Rn", "button");
        final PositionForRelatedExpression positionForRelatedExpression = new PositionForRelatedExpression(244, 44);
        final PositionForAnaphor positionForAnaphor = new PositionForAnaphor(310, 6, anaphora);
        return asList(positionForRelatedExpression, positionForAnaphor);
    }

    @Override
    protected void editFile(final IEditorPart editor) {
        getBot().activeEditor().toTextEditor().insertText(8, 25, "new JButton(\"stop\");\n");
        getBot().activeEditor().save();
    }

    @Override
    protected void waitForAnaphoraRelationsAfterEdit(final CognitiveEditor editor) {
        // Wait for anaphora relations, because the relation will be removed
        // by the refactoring, not by the edit.
        waitForAnaphoraRelations(editor, 0);
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
        builder.addLine("");
        builder.addLine("import javax.swing.JButton;");
        builder.addLine("import javax.swing.JPanel;");
        builder.addLine("");
        builder.addLine("public class EnterCIC_DA1Re_HyFx_withAmbiguityIntroducedByEdit {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		new JPanel();");
        builder.addLine("		new JButton(\"start\");");
        builder.addLine("		new JButton(\"stop\");");
        builder.addLine("	System.err.println(button);");
        builder.addLine("	}");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
        builder.addLine("");
        builder.addLine("import javax.swing.JButton;");
        builder.addLine("");
        builder.addLine("public class EnterCIC_DA1Re_HyFx_withAmbiguityIntroducedByEdit {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		new JPanel();");
        builder.addLine("		new JButton(\"start\");");
        builder.addLine("		new JButton(\"stop\");");
        builder.addLine("	System.err.println(button);");
        builder.addLine("	}");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
        return asList(
                new ImmutablePair<>(new Annotation("org.eclipse.jdt.ui.error", false,
                        "Ambiguous reference (more than 1 potential referents found):\n"
                                + "new JButton(\"start\") at 10:2 (CIC-DA1Re-HyFx)\n"
                                + "new JButton(\"stop\") at 11:2 (CIC-DA1Re-HyFx)"),
                        new Position(309, 6)),
                new ImmutablePair<>(
                        new Annotation("org.eclipse.jdt.ui.error", false, "button cannot be resolved to a variable"),
                        new Position(309, 6)));
    }

}
