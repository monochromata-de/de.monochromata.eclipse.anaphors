package de.monochromata.eclipse.anaphors.position;

import static de.monochromata.eclipse.anaphors.position.ExistingPerspectivation.getExistingAnaphorPerspectivation;
import static de.monochromata.eclipse.anaphors.position.ExistingPerspectivation.getExistingRelatedExpressionPerspectivation;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Optional.empty;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Predicate;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicChainElement;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.junit.Test;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.DefaultAnaphorPart;
import de.monochromata.anaphors.ast.DefaultRelatedExpressionPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.eclipse.anaphors.MultipleOriginalAnaphoras;
import de.monochromata.eclipse.anaphors.OriginalAnaphora;
import de.monochromata.eclipse.anaphors.SingleOriginalAnaphora;
import de.monochromata.eclipse.persp.PerspectivationPosition;

public class ExistingPerspectivationTest {

	private static final Predicate<PerspectivationPosition> DO_NOT_CHECK_DOCUMENT = unused -> {
		throw new IllegalStateException();
	};

	private final PositionForRelatedExpression positionForRelatedExpression = new PositionForRelatedExpression(0, 1);
	private final Anaphora anaphora = new Anaphora("RelExp", "AnaRes", "Ref", "anaphor");
	private final PositionForAnaphor positionForAnaphor = new PositionForAnaphor(2, 1, anaphora);
	private final Optional<Triple<PositionForRelatedExpression, Anaphora, PositionForAnaphor>> originalAnaphora = Optional
			.of(new ImmutableTriple<>(positionForRelatedExpression, anaphora, positionForAnaphor));

	private final RelatedExpressionPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression> relatedExpressionPart = new DefaultRelatedExpressionPart<>(
			null);
	private final AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart = new DefaultAnaphorPart<>(
			null, null, null, null, null, null);

	@Test
	public void getExistingAnaphorPerspectivation_forUnknownTypeOfOriginalAnaphora_yieldsException() {
		final var originalAnaphora = new OriginalAnaphora() {
		};
		final var chainElement = new PublicChainElement(null, anaphorPart, originalAnaphora);

		assertThatThrownBy(() -> getExistingAnaphorPerspectivation(chainElement, DO_NOT_CHECK_DOCUMENT))
				.isInstanceOf(IllegalArgumentException.class).hasMessage("Unknown subtype of "
						+ OriginalAnaphora.class.getSimpleName() + ": " + originalAnaphora.getClass().getName());
	}

	@Test
	public void getExistingAnaphorPerspectivation_forNullOriginalAnaphora_yieldsException() {
		final var chainElement = new PublicChainElement(null, anaphorPart, null);

		assertThatThrownBy(() -> getExistingAnaphorPerspectivation(chainElement, DO_NOT_CHECK_DOCUMENT))
				.isInstanceOf(NullPointerException.class);
	}

	@Test
	public void getExistingAnaphorPerspectivation_forMultipleOriginalAnaphora_withoutOptionalOriginalAnaphoras_isFalse() {
		final var multipleOriginalAnaphoras = new MultipleOriginalAnaphoras(emptyMap());
		final var chainElement = new PublicChainElement(null, anaphorPart, multipleOriginalAnaphoras);

		assertThat(getExistingAnaphorPerspectivation(chainElement, DO_NOT_CHECK_DOCUMENT)).isNull();
	}

	@Test
	public void getExistingAnaphorPerspectivation_forMultipleOriginalAnaphora_withoutOriginalAnaphoras_isFalse() {
		final var multipleOriginalAnaphoras = new MultipleOriginalAnaphoras(Map.of(anaphorPart, empty()));
		final var chainElement = new PublicChainElement(null, anaphorPart, multipleOriginalAnaphoras);

		assertThat(getExistingAnaphorPerspectivation(chainElement, DO_NOT_CHECK_DOCUMENT)).isNull();
	}

	@Test
	public void getExistingAnaphorPerspectivation_forMultipleOriginalAnaphora_withPerspectivationPositionToBeRemoved_isFalse() {
		final var originalAnaphoras = new MultipleOriginalAnaphoras(Map.of(anaphorPart, originalAnaphora));
		final var chainElement = new PublicChainElement(null, anaphorPart, originalAnaphoras);
		final var positionsToRemove = List.of(positionForAnaphor.perspectivationPosition);

		assertThat(getExistingAnaphorPerspectivation(chainElement, positionsToRemove)).isNull();
	}

	@Test
	public void getExistingAnaphorPerspectivation_forMultipleOriginalAnaphora_withPerspectivationPositionToBeRetained_isTrue() {
		final var originalAnaphoras = new MultipleOriginalAnaphoras(Map.of(anaphorPart, originalAnaphora));
		final var chainElement = new PublicChainElement(null, anaphorPart, originalAnaphoras);

		assertThat(getExistingAnaphorPerspectivation(chainElement, emptyList()))
				.isSameAs(positionForAnaphor.perspectivationPosition);
	}

	@Test
	public void getExistingAnaphorPerspectivation_forSingleOriginalAnaphora_withoutOriginalAnaphora_isFalse() {
		final var chainElement = new PublicChainElement(null, anaphorPart, new SingleOriginalAnaphora(empty()));

		assertThat(getExistingAnaphorPerspectivation(chainElement, DO_NOT_CHECK_DOCUMENT)).isNull();
	}

	@Test
	public void getExistingAnaphorPerspectivation_forSingleOriginalAnaphora_withPerspectivationPositionToBeRemoved_isFalse() {
		final var chainElement = new PublicChainElement(null, anaphorPart,
				new SingleOriginalAnaphora(originalAnaphora));
		final var positionsToRemove = List.of(positionForAnaphor.perspectivationPosition);

		assertThat(getExistingAnaphorPerspectivation(chainElement, positionsToRemove)).isNull();
	}

	@Test
	public void isAnaphorAnaphorAlreadyPerspectivized_forSingleOriginalAnaphora_withPerspectivationPositionToBeRetained_isTrue() {
		final var chainElement = new PublicChainElement(null, anaphorPart,
				new SingleOriginalAnaphora(originalAnaphora));

		assertThat(getExistingAnaphorPerspectivation(chainElement, emptyList()))
				.isSameAs(positionForAnaphor.perspectivationPosition);
	}

	@Test
	public void isRelatedExpressionAlreadyPerspectivized_forMultipleOriginalAnaphora_withoutOptionalOriginalAnaphoras_isFalse() {
		final var multipleOriginalAnaphoras = new MultipleOriginalAnaphoras(emptyMap());
		final var anaphorElement = new PublicChainElement(null, anaphorPart, multipleOriginalAnaphoras);
		final var relatedExpressionElement = new PublicChainElement(anaphorElement, relatedExpressionPart);

		assertThat(getExistingRelatedExpressionPerspectivation(relatedExpressionElement, DO_NOT_CHECK_DOCUMENT))
				.isNull();
	}

	@Test
	public void isRelatedExpressionAlreadyPerspectivized_forMultipleOriginalAnaphora_withoutOriginalAnaphoras_isFalse() {
		final var multipleOriginalAnaphoras = new MultipleOriginalAnaphoras(Map.of(anaphorPart, empty()));
		final var anaphorElement = new PublicChainElement(null, anaphorPart, multipleOriginalAnaphoras);
		final var relatedExpressionElement = new PublicChainElement(anaphorElement, relatedExpressionPart);

		assertThat(getExistingRelatedExpressionPerspectivation(relatedExpressionElement, DO_NOT_CHECK_DOCUMENT))
				.isNull();
	}

	@Test
	public void isRelatedExpressionAlreadyPerspectivized_forMultipleOriginalAnaphora_withPerspectivationPositionToBeRemoved_isFalse() {
		final var originalAnaphoras = new MultipleOriginalAnaphoras(Map.of(anaphorPart, originalAnaphora));
		final var anaphorElement = new PublicChainElement(null, anaphorPart, originalAnaphoras);
		final var relatedExpressionElement = new PublicChainElement(anaphorElement, relatedExpressionPart);
		final var positionsToRemove = List.of(positionForRelatedExpression.perspectivationPosition);

		assertThat(getExistingRelatedExpressionPerspectivation(relatedExpressionElement, positionsToRemove)).isNull();
	}

	@Test
	public void isRelatedExpressionAlreadyPerspectivized_forMultipleOriginalAnaphora_withoutCorrespondingAnaphorChainElement_yieldsException() {
		final var chainElement = new PublicChainElement(emptyList(), relatedExpressionPart);

		assertThatThrownBy(() -> getExistingRelatedExpressionPerspectivation(chainElement, DO_NOT_CHECK_DOCUMENT))
				.isInstanceOf(NoSuchElementException.class).hasMessage("No value present");
	}

	@Test
	public void isRelatedExpressionAlreadyPerspectivized_forMultipleOriginalAnaphora_withPerspectivationPositionToBeRetained_isTrue() {
		final var originalAnaphora = new ImmutableTriple<>(positionForRelatedExpression, anaphora, positionForAnaphor);
		final var originalAnaphoras = new MultipleOriginalAnaphoras(Map.of(anaphorPart, Optional.of(originalAnaphora)));
		final var anaphorElement = new PublicChainElement(null, anaphorPart, originalAnaphoras);
		final var relatedExpressionElement = new PublicChainElement(anaphorElement, relatedExpressionPart);

		assertThat(getExistingRelatedExpressionPerspectivation(relatedExpressionElement, emptyList()))
				.isSameAs(positionForRelatedExpression.perspectivationPosition);
	}

	@Test
	public void isRelatedExpressionAlreadyPerspectivized_forSingleOriginalAnaphora_withoutOriginalAnaphora_isFalse() {
		final var anaphorElement = new PublicChainElement(null, anaphorPart, new SingleOriginalAnaphora(empty()));
		final var relatedExpressionElement = new PublicChainElement(anaphorElement, relatedExpressionPart);

		assertThat(getExistingRelatedExpressionPerspectivation(relatedExpressionElement, DO_NOT_CHECK_DOCUMENT))
				.isNull();
	}

	@Test
	public void isRelatedExpressionAlreadyPerspectivized_forSingleOriginalAnaphora_withPerspectivationPositionToBeRemoved_isFalse() {
		final var anaphorElement = new PublicChainElement(null, anaphorPart,
				new SingleOriginalAnaphora(originalAnaphora));
		final var relatedExpressionElement = new PublicChainElement(anaphorElement, relatedExpressionPart);
		final var positionsToRemove = List.of(positionForRelatedExpression.perspectivationPosition);

		assertThat(getExistingRelatedExpressionPerspectivation(relatedExpressionElement, positionsToRemove)).isNull();
	}

	@Test
	public void isRelatedExpressionAlreadyPerspectivized_forSingleOriginalAnaphora_withPerspectivationPositionToBeRetained_isTrue() {
		final var anaphorElement = new PublicChainElement(null, anaphorPart,
				new SingleOriginalAnaphora(originalAnaphora));
		final var relatedExpressionElement = new PublicChainElement(anaphorElement, relatedExpressionPart);

		assertThat(getExistingRelatedExpressionPerspectivation(relatedExpressionElement, emptyList()))
				.isSameAs(positionForRelatedExpression.perspectivationPosition);
	}

}
