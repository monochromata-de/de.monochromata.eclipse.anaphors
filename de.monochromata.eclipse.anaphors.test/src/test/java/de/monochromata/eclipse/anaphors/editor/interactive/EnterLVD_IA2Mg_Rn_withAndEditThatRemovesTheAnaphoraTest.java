package de.monochromata.eclipse.anaphors.editor.interactive;

import static java.util.Arrays.asList;
import static org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable.syncExec;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;

import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.persp.PerspectivationPosition;

public class EnterLVD_IA2Mg_Rn_withAndEditThatRemovesTheAnaphoraTest
        extends AbstractInteractiveEditorTest {

    @Override
    protected void manipulateEditorAndCheckAssertions(final ProblemCollectingChangeListener problemCollector,
            final CognitiveEditor editor) {
        enableMarkOccurrences();
        waitForAnaphoraRelations(editor, 1);
        waitForProblemsToBeSolved(editor);

        editFile(editor);
        syncExec(() -> editor.doSave(null));
        waitForAnaphoraRelations(editor, 0);
        waitForSharedASTToBeReconciled(editor);
        waitForExpectedAnnotations(editor, getExpectedAnnotations());
        assertEventualSourceCode(editor);
        assertUnderspecifiedEventualSourceCode(editor);
        assertAnnotations(editor, getExpectedAnnotations());
        waitForRemovalOfPositionsOfBacklistedTypes(editor, PerspectivationPosition.class,
                AbstractAnaphoraPositionForRepresentation.class);
        assertPositionsDoNotHaveType(editor, PerspectivationPosition.class,
                AbstractAnaphoraPositionForRepresentation.class);
    }

    @Override
    protected void editFile(final IEditorPart editor) {
        // Note that this edit introduces a syntax error but the anaphora should still
        // be de-realized.
        getBot().activeEditor().toTextEditor().insertText(8, 30, "s");
        getBot().activeEditor().save();
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
        builder.addLine("");
        builder.addLine("import javax.swing.JTextArea;");
        builder.addLine("");
        builder.addLine("public class EnterLVD_IA2Mg_Rn_withAndEditThatRemovesTheAnaphora {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		final JTextArea textArea = new JTextArea();");
        builder.addLine("		System.err.println(lineCounts);");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
        builder.addLine("");
        builder.addLine("import javax.swing.JTextArea;");
        builder.addLine("");
        builder.addLine("public class EnterLVD_IA2Mg_Rn_withAndEditThatRemovesTheAnaphora {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		final JTextArea textArea = new JTextArea();");
        builder.addLine("		System.err.println(lineCounts);");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
        return asList(
                new ImmutablePair<>(new Annotation("org.eclipse.jdt.ui.error", false,
                        "lineCounts cannot be resolved to a variable"),
                        new Position(270, 10)),
                new ImmutablePair<>(new Annotation("org.eclipse.ui.workbench.texteditor.quickdiffUnchanged", false,
                        "13 changed lines "),
                        new Position(0, 289)));
    }

    @Override
    protected List<? extends Position> getExpectedPositions() {
        throw new UnsupportedOperationException("assertPositionsDoNotHaveType is used instead of assertGivenPositions");
    }

}
