package de.monochromata.eclipse.anaphors.editor.simple;

import static de.monochromata.anaphors.ast.reference.strategy.concept.Hyponymy.Hy_KIND;
import static de.monochromata.anaphors.ast.reference.strategy.feature.FeatureRecurrence.Rf_KIND_PREFIX;
import static de.monochromata.anaphors.ast.relatedexp.strategy.ClassInstanceCreationStrategy.CIC_KIND;
import static de.monochromata.anaphors.ast.relatedexp.strategy.LocalVariableDeclarationStrategy.LVD_KIND;
import static de.monochromata.anaphors.ast.relatedexp.strategy.MethodInvocationStrategy.MI_KIND;
import static de.monochromata.anaphors.ast.relatedexp.strategy.ParameterDeclarationStrategy.PD_KIND;
import static java.util.stream.Collectors.toList;

import java.lang.reflect.Field;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.After;
import org.junit.Before;

import de.monochromata.Strategy;
import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.ast.ASTBasedAnaphorResolution;
import de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy;
import de.monochromata.anaphors.ast.reference.strategy.feature.FeatureRecurrence;
import de.monochromata.anaphors.ast.relatedexp.strategy.ClassInstanceCreationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalVariableDeclarationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.MethodInvocationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.ParameterDeclarationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.anaphors.ast.strategy.AnaphorResolutionStrategy;
import de.monochromata.contract.environment.junit.FutureJunitContractExtension;
import de.monochromata.eclipse.anaphors.Activator;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;

public class CIC_DA1Re_Hy_SimpleTest extends AbstractSimpleEditorTest {

    private static final FutureJunitContractExtension futureContractExtension = new FutureJunitContractExtension(
            CIC_DA1Re_Hy_SimpleTest.class);
    private ParameterDeclarationStrategy pdStrategy;
    private LocalVariableDeclarationStrategy lvdStrategy;
    private ClassInstanceCreationStrategy cicStrategy;
    private MethodInvocationStrategy miStrategy;
    private List<RelatedExpressionStrategy> relatedExpressionStrategies;
    private List<AnaphorResolutionStrategy> resolutionStrategies;
    private List<ReferentializationStrategy> referentializationStrategies;

    @Before
    public void proxyStrategies() throws Exception {
        /*
         * proxyRelatedExpressionStrategies(); proxyResolutionStrategies();
         * proxyReferentializationStrategies();
         */
    }

    protected void proxyRelatedExpressionStrategies() throws Exception {
        pdStrategy = getStrategy("getPdStrategy");
        final var proxiedPdStrategy = proxyStrategy(pdStrategy);
        setFieldInActivator("pdStrategy", proxiedPdStrategy);

        lvdStrategy = getStrategy("getLvdStrategy");
        final var proxiedLvdStrategy = proxyStrategy(lvdStrategy);
        setFieldInActivator("lvdStrategy", proxiedLvdStrategy);

        cicStrategy = getStrategy("getCicStrategy");
        final var proxiedCicStrategy = proxyStrategy(cicStrategy);
        setFieldInActivator("cicStrategy", proxiedCicStrategy);

        miStrategy = getStrategy("getMiStrategy");
        final var proxiedMiStrategy = proxyStrategy(miStrategy);
        setFieldInActivator("miStrategy", proxiedMiStrategy);

        relatedExpressionStrategies = getStrategies("getRelatedExpressionStrategies");
        proxyRelatedExpressionStrategies(proxiedPdStrategy, proxiedLvdStrategy, proxiedCicStrategy, proxiedMiStrategy);
    }

    protected void proxyRelatedExpressionStrategies(final ParameterDeclarationStrategy proxiedPdStrategy,
            final LocalVariableDeclarationStrategy proxiedLvdStrategy,
            final ClassInstanceCreationStrategy proxiedCicStrategy, final MethodInvocationStrategy proxiedMiStrategy)
            throws Exception {
        // TODO: Do not capture invocations outside of test methods?
        // TODO: Or only start capturing invocations at a certain point?
        final var proxiedStrategies = relatedExpressionStrategies.stream().map(strategy -> switch (strategy.getKind()) {
        case PD_KIND -> proxiedPdStrategy;
        case LVD_KIND -> proxiedLvdStrategy;
        case CIC_KIND -> proxiedCicStrategy;
        case MI_KIND -> proxiedMiStrategy;
        default -> throw new IllegalArgumentException("Unknown strategy " + strategy.getKind());
        }).collect(toList());

        setFieldInActivator("relatedExpressionStrategies", proxiedStrategies);
        setFieldInAnaphorResolution("relatedExpressionStrategies", proxiedStrategies);
    }

    protected void proxyResolutionStrategies() throws Exception {
        resolutionStrategies = getStrategies("getAnaphorResolutionStrategies");
        proxyStrategies("resolutionStrategies", resolutionStrategies);
    }

    protected void proxyReferentializationStrategies() throws Exception {
        referentializationStrategies = getStrategies("getReferentializationStrategies");
        final var proxiedStrategies = proxyStrategies("referentializationStrategies", referentializationStrategies);

        final var proxiedHyStrategy = getStrategy(proxiedStrategies, Hy_KIND);
        final var rfHyStrategy = getStrategy(referentializationStrategies, Rf_KIND_PREFIX + Hy_KIND);
        setField(rfHyStrategy, proxiedHyStrategy, FeatureRecurrence.class.getDeclaredField("delegate"));
    }

    public <S extends Strategy> S getStrategy(final List<S> strategies, final String kind) {
        return strategies.stream().filter(strategy -> strategy.getKind().equals(kind)).findAny().get();
    }

    protected <T extends Strategy> List<T> proxyStrategies(final String fieldName, final List<T> strategies)
            throws Exception {
        final var proxiedStrategies = strategies.stream().map(this::proxyStrategy).collect(toList());
        setFieldInActivator(fieldName, proxiedStrategies);
        setFieldInAnaphorResolution(fieldName, proxiedStrategies);
        return proxiedStrategies;
    }

    protected <T extends Strategy> T getStrategy(final String accessorName) throws Exception {
        return getField(accessorName);
    }

    protected <T extends Strategy> List<T> getStrategies(final String accessorName) throws Exception {
        return getField(accessorName);
    }

    @SuppressWarnings({ "unchecked" })
    protected <T> T getField(final String accessorName) throws Exception {
        final var accessor = Activator.class.getDeclaredMethod(accessorName);
        accessor.setAccessible(true);
        return (T) accessor.invoke(Activator.getDefault());
    }

    @SuppressWarnings("unchecked")
    protected <T extends Strategy> T proxyStrategy(final T strategy) {
        return futureContractExtension.proxy(strategy);
    }

    protected void setFieldInActivator(final String fieldName, final Object value) throws Exception {
        final var instance = Activator.getDefault();
        final var field = Activator.class.getDeclaredField(fieldName);
        setField(instance, value, field);
    }

    protected void setFieldInAnaphorResolution(final String fieldName, final Object value) throws Exception {
        final var instance = Activator.getDefault().getAnaphorResolution();
        final var field = ASTBasedAnaphorResolution.class.getDeclaredField(fieldName);
        setField(instance, value, field);
    }

    protected void setField(final Object instance, final Object value, final Field field)
            throws IllegalAccessException {
        field.setAccessible(true);
        field.set(instance, value);
    }

    @After
    public void recordHyponymyPact() {
        // futureContractExtension.save();
    }

    @After
    // TODO: Move into the framework - inkl. resetting the fields
    public void unproxyStrategies() throws Exception {
        /*
         * setFieldInActivator("pdStrategy", pdStrategy);
         * setFieldInActivator("lvdStrategy", lvdStrategy);
         * setFieldInActivator("cicStrategy", cicStrategy);
         * setFieldInActivator("miStrategy", miStrategy);
         * 
         * setFieldInActivator("relatedExpressionStrategies",
         * relatedExpressionStrategies);
         * setFieldInAnaphorResolution("relatedExpressionStrategies",
         * relatedExpressionStrategies);
         * 
         * setFieldInActivator("resolutionStrategies", resolutionStrategies);
         * setFieldInAnaphorResolution("resolutionStrategies", resolutionStrategies);
         * 
         * setFieldInActivator("referentializationStrategies",
         * referentializationStrategies);
         * setFieldInAnaphorResolution("referentializationStrategies",
         * referentializationStrategies);
         * 
         * final var hyStrategy = getStrategy(referentializationStrategies, Hy_KIND);
         * final var rfHyStrategy = getStrategy(referentializationStrategies,
         * Rf_KIND_PREFIX + Hy_KIND); setField(rfHyStrategy, hyStrategy,
         * FeatureRecurrence.class.getDeclaredField("delegate"));
         */
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
        builder.addLine("");
        builder.addLine("public class CIC_DA1Re_Hy_Simple {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		final Sword weapon = new Sword();");
        builder.addLine("		System.err.println(weapon);");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("   private static class Sword implements Weapon {");
        builder.addLine("   }");
        builder.addLine("");
        builder.addLine("   private static interface Weapon{");
        builder.addLine("   }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
        builder.addLine("");
        builder.addLine("public class CIC_DA1Re_Hy_Simple {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		new Sword();");
        builder.addLine("		System.err.println(weapon);");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("	private static class Sword implements Weapon {");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("   private static interface Weapon{");
        builder.addLine("   }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected Pair<Anaphora, Anaphora> getExpectedResolvedAndRealizedAnaphora() {
        return new ImmutablePair<>(new Anaphora("CIC", "DA1Re", "Hy", "weapon"),
                new Anaphora("LVD", "DA1Re", "Rn", "weapon"));
    }

}
