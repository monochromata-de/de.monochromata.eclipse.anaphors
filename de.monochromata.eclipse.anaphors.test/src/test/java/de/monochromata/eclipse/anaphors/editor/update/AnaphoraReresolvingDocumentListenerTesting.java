package de.monochromata.eclipse.anaphors.editor.update;

import static de.monochromata.eclipse.anaphors.position.AnaphoraPositionCreation.addPosition;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapPositionExceptions;

import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.internal.corext.refactoring.code.AnaphoraPositionCreationForRefactoring;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.position.AnaphoraPositionCreationTesting;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.persp.PerspectivationPosition;

public interface AnaphoraReresolvingDocumentListenerTesting extends AnaphoraPositionCreationTesting {

	default PositionForAnaphor addPositionForAnaphor(final IDocument document, final Anaphora anaphora,
			final PerspectivationPosition perspectivationPosition) {
		final PositionForAnaphor position = AnaphoraPositionCreationForRefactoring.createPositionForAnaphor(anaphora,
				perspectivationPosition);
		wrapPositionExceptions(() -> addPosition(document, position));
		return position;
	}

	default AnaphoraReresolvingDocumentListener createTestee(final CompilationUnit compilationUnit,
			final AtomicReference<Pair<Integer, Integer>> offsetAndLengthFuture) {
		return new AnaphoraReresolvingDocumentListener(() -> (ICompilationUnit) compilationUnit.getJavaElement(),
				(offsetAndLength, originDocument) -> offsetAndLengthFuture.set(offsetAndLength), (unused0, unused1) -> {
				});
	}

	default void notifyListener(final int eventOffset, final int eventLength,
			final AnaphoraReresolvingDocumentListener listener, final IDocument document) {
		listener.documentChanged(new DocumentEvent(document, eventOffset, eventLength, ""));
	}

}
