package de.monochromata.eclipse.anaphors.contract;

import static com.fasterxml.jackson.databind.SerializationFeature.FAIL_ON_EMPTY_BEANS;
import static de.monochromata.eclipse.anaphors.contract.mixins.Mixins.addMixins;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.monochromata.contract.io.IOConfig;

public interface ContractTesting {

    IOConfig IO_CONFIG = new IOConfig() {
        {
            addTypeValidationCustomizer(builder -> builder.allowIfSubType("[Ljava.lang.Object;")
                    .allowIfSubType("[Lorg.eclipse.jdt.core.dom.").allowIfSubType("org.eclipse.jdt.core.dom."));
        }

        @Override
        public void customizeObjectMapper(final ObjectMapper objectMapper) {
            objectMapper.configure(FAIL_ON_EMPTY_BEANS, false);
            addMixins(objectMapper);
        }

    };

}
