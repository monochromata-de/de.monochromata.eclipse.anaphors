package de.monochromata.eclipse.anaphors.editor;

import static java.lang.System.currentTimeMillis;

import java.util.function.Supplier;

import de.monochromata.eclipse.anaphors.editor.AbstractCognitiveEditorTest.ProblemListenerThatAbortsWaitingWhenTestCanProceed;

public interface WaitingDuringTesting {

    int WAIT_FOR_CHANGE_TIMEOUT = 10;

    /**
     * Provides a means of waiting up to a given number of seconds. Waiting may be
     * interrupted if it is permissible to proceed earlier.
     *
     * @param seconds Number of seconds to wait if not interrupted earlier.
     * @see ProblemListenerThatAbortsWaitingWhenTestCanProceed
     */
    default void waitSecondsIfNotNotifiedBefore(final String goal, final int seconds, final Supplier<Boolean> predicate,
            final Object waitee) {
        final long startMs = currentTimeMillis();
        final long endMs = startMs + (seconds * 1000);
        try {
            do {
                waitSeconds(endMs - currentTimeMillis(), waitee);
            } while (!predicate.get() && currentTimeMillis() < endMs);
        } catch (final InterruptedException ie) {
            ie.printStackTrace();
            // Just continue when waiting has been aborted.
        }
    }

    default void waitSeconds(final long millisecondsToWait, final Object waitee) throws InterruptedException {
        if (millisecondsToWait <= 0) {
            return;
        }
        synchronized (waitee) {
            waitee.wait(millisecondsToWait);
        }
    }

}
