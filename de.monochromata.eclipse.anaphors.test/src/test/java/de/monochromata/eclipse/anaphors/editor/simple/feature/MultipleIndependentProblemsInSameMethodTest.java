package de.monochromata.eclipse.anaphors.editor.simple.feature;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.eclipse.jface.text.Position;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class MultipleIndependentProblemsInSameMethodTest extends AbstractSimpleFeatureEditorTest {

    @Override
    public void waitForAnaphoraRelations(final CognitiveEditor editor) {
        waitForAnaphoraRelations(editor, 2);
    }

    @Override
    public void assertAnaphoraRelations() {
        assertThat(getAddedAnaphoraRelations())
                .containsExactly(
                        new ImmutablePair<>(new Anaphora("LVD", "IA2Mg", "Rn", "lineCount"),
                                new Anaphora("LVD", "IA2Mg", "Rn", "lineCount")),
                        new ImmutablePair<>(new Anaphora("CIC", "DA1Re", "RfRt", "helloString"),
                                new Anaphora("LVD", "DA1Re", "Rn", "helloString")));
    }

    @Override
    protected void manipulateEditorAndCheckAssertions(final ProblemCollectingChangeListener problemCollector,
            final CognitiveEditor editor) {
        super.manipulateEditorAndCheckAssertions(problemCollector, editor);
        waitForExpectedPositions(editor, getExpectedPositions());
        assertGivenPositions(editor, getExpectedPositions());
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple.feature;");
        builder.addLine("");
        builder.addLine("import javax.swing.JTextArea;");
        builder.addLine("");
        builder.addLine("public class MultipleIndependentProblemsInSameMethod {");
        builder.addLine("");
        builder.addLine("    public static void foo() {");
        builder.addLine("        final JTextArea jTextArea = new JTextArea();");
        builder.addLine("        jTextArea.setText(\"line count=\" + jTextArea.getLineCount());");
        builder.addLine("        final String helloString = new String(\"Hello\");");
        builder.addLine("        new String(\"Bello\");");
        builder.addLine("        System.err.println(helloString);");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple.feature;");
        builder.addLine("");
        builder.addLine("import javax.swing.JTextArea;");
        builder.addLine("");
        builder.addLine("public class MultipleIndependentProblemsInSameMethod {");
        builder.addLine("");
        builder.addLine("    public static void foo() {");
        builder.addLine("        final JTextArea jTextArea = new JTextArea();");
        builder.addLine("        jTextArea.setText(\"line count=\" + lineCount);");
        builder.addLine("        new String(\"Hello\");");
        builder.addLine("        new String(\"Bello\");");
        builder.addLine("        System.err.println(helloString);");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    protected List<? extends Position> getExpectedPositions() {
        final Anaphora lineCountAnaphora = new Anaphora("LVD", "IA2Mg", "Rn", "lineCount");
        final PositionForRelatedExpression lineCountRelatedExpression = new PositionForRelatedExpression(191, 44);
        final PositionForAnaphor lineCountAnaphor = new PositionForAnaphor(278, 24, lineCountAnaphora);

        final Anaphora helloStringAnaphora = new Anaphora("LVD", "DA1Re", "Rn", "helloString");
        final PositionForRelatedExpression helloStringRelatedExpression = new PositionForRelatedExpression(313, 47);
        final PositionForAnaphor helloStringAnaphor = new PositionForAnaphor(417, 11, helloStringAnaphora);

        return asList(lineCountAnaphor, lineCountRelatedExpression, helloStringRelatedExpression, helloStringAnaphor);
    }

}
