package de.monochromata.eclipse.anaphors.editor.interactive;

import java.lang.reflect.Method;

/**
 * Stores information about a throwable that triggered screenshot capture and a
 * method in which the throwable was raised.
 */
public class ScreenshotTrigger {

	private final Method method;
	private final Throwable throwable;

	/**
	 * Create a new ScreenshotTrigger.
	 * 
	 * @param method
	 *            The method in which the throwable to raised.
	 * @param throwable
	 *            The throwable that triggered the screenshot.
	 */
	public ScreenshotTrigger(Method method, Throwable throwable) {
		this.method = method;
		this.throwable = throwable;
	}

	/**
	 * Return the method in which the throwable was raised.
	 * 
	 * @return The method
	 */
	public Method getMethod() {
		return method;
	}

	/**
	 * Return the throwable that triggered the screenshot.
	 * 
	 * @return The throwable
	 */
	public Throwable getThrowable() {
		return throwable;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((method == null) ? 0 : method.hashCode());
		result = prime * result + ((throwable == null) ? 0 : throwable.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScreenshotTrigger other = (ScreenshotTrigger) obj;
		if (method == null) {
			if (other.method != null)
				return false;
		} else if (!method.equals(other.method))
			return false;
		if (throwable == null) {
			if (other.throwable != null)
				return false;
		} else if (!throwable.equals(other.throwable))
			return false;
		return true;
	}

}