package de.monochromata.eclipse.anaphors.editor.interactive;

import static java.util.Arrays.asList;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class EnterCIC_DA1Re_HyFxTest extends AbstractInteractiveEditorTest {

    @Override
    protected void editFile(final IEditorPart editor) {
        getBot().activeEditor().toTextEditor().insertText(6, 21, "panel");
        getBot().activeEditor().save();
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
        builder.addLine("");
        builder.addLine("public class EnterCIC_DA1Re_HyFx {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		final JPanel panel = new JPanel();");
        builder.addLine("		System.err.println(panel);");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("	private static class JPanel {");
        builder.addLine("	}");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
        builder.addLine("");
        builder.addLine("public class EnterCIC_DA1Re_HyFx {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		new JPanel();");
        builder.addLine("		System.err.println(panel);");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("	private static class JPanel {");
        builder.addLine("	}");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
        return asList(
                new ImmutablePair<>(
                        new Annotation("org.eclipse.jdt.ui.occurrences", false, "Anaphor co-referent to panel"),
                        new Position(198, 5)),
                new ImmutablePair<>(
                        new Annotation("org.eclipse.jdt.ui.occurrences.write", false, "Related expression"),
                        new Position(142, 34)));
    }

    @Override
    protected List<? extends Position> getExpectedPositions() {
        final Anaphora anaphora = new Anaphora("LVD", "DA1Re", "Rn", "panel");
        return asList(new PositionForRelatedExpression(142, 34),
                new PositionForAnaphor(198, 5, anaphora));
    }

}
