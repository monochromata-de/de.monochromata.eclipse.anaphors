package de.monochromata.eclipse.anaphors.editor;

import static org.assertj.core.api.Assertions.assertThat;

public interface SourceCodeTesting {

    String getExpectedEventualSourceCode();

    String getActualEventualSourceCode(CognitiveEditor editor);

    String getExpectedUnderspecifiedEventualSourceCode();

    String getActualUnderspecifiedEventualSourceCode(CognitiveEditor editor);

    default void assertEventualSourceCode(final CognitiveEditor editor) {
        final String expectedRefactoredSourceCode = getExpectedEventualSourceCode();
        assertEventualSourceCode(editor, expectedRefactoredSourceCode);
    }

    default void assertEventualSourceCode(final CognitiveEditor editor,
            final String expectedRefactoredSourceCode) {
        assertThat(getActualEventualSourceCode(editor))
                .isEqualToIgnoringWhitespace(expectedRefactoredSourceCode);
    }

    default void assertUnderspecifiedEventualSourceCode(final CognitiveEditor editor) {
        final String expectedUnderspecifiedRefactoredSourceCode = getExpectedUnderspecifiedEventualSourceCode();
        assertUnderspecifiedEventualSourceCode(editor, expectedUnderspecifiedRefactoredSourceCode);
    }

    default void assertUnderspecifiedEventualSourceCode(final CognitiveEditor editor,
            final String expectedUnderspecifiedRefactoredSourceCode) {
        assertThat(getActualUnderspecifiedEventualSourceCode(editor))
                .isEqualToIgnoringWhitespace(expectedUnderspecifiedRefactoredSourceCode);
    }

}
