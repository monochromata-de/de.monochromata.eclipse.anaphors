package de.monochromata.eclipse.anaphors.editor.simple.feature;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.eclipse.jface.text.Position;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class MultipleInterdependentProblems_DA1Re_IA2MgTest extends AbstractSimpleFeatureEditorTest {

    @Override
    public void waitForAnaphoraRelations(final CognitiveEditor editor) {
        waitForAnaphoraRelations(editor, 2);
    }

    @Override
    public void assertAnaphoraRelations() {
        assertThat(getAddedAnaphoraRelations())
                .containsExactly(
                        new ImmutablePair<>(new Anaphora("CIC", "DA1Re", "RfRt", "jTextArea"),
                                new Anaphora("LVD", "DA1Re", "Rn", "jTextArea")),
                        new ImmutablePair<>(new Anaphora("CIC", "IA2Mg", "Rn", "lineCount"),
                                new Anaphora("LVD", "IA2Mg", "Rn", "lineCount")));
    }

    @Override
    protected void manipulateEditorAndCheckAssertions(final ProblemCollectingChangeListener problemCollector,
            final CognitiveEditor editor) {
        super.manipulateEditorAndCheckAssertions(problemCollector, editor);
        waitForExpectedPositions(editor, getExpectedPositions());
        assertGivenPositions(editor, getExpectedPositions());
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple.feature;");
        builder.addLine("");
        builder.addLine("import javax.swing.JTextArea;");
        builder.addLine("");
        builder.addLine("public class MultipleInterdependentProblems_DA1Re_IA2Mg {");
        builder.addLine("");
        builder.addLine("    public static void foo() {");
        builder.addLine("        final JTextArea jTextArea = new JTextArea();");
        builder.addLine("        jTextArea.setText(\"line count=\" + jTextArea.getLineCount());");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple.feature;");
        builder.addLine("");
        builder.addLine("import javax.swing.JTextArea;");
        builder.addLine("");
        builder.addLine("public class MultipleInterdependentProblems_DA1Re_IA2Mg {");
        builder.addLine("");
        builder.addLine("    public static void foo() {");
        builder.addLine("        new JTextArea();");
        builder.addLine("        jTextArea.setText(\"line count=\" + lineCount);");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    protected List<? extends Position> getExpectedPositions() {
        final PositionForRelatedExpression relatedExpression = new PositionForRelatedExpression(194, 44);
        final Anaphora anaphora1 = new Anaphora("LVD", "DA1Re", "Rn", "jTextArea");
        final PositionForAnaphor anaphor1 = new PositionForAnaphor(247, 9, anaphora1);
        final Anaphora anaphora2 = new Anaphora("LVD", "IA2Mg", "Rn", "lineCount");
        final PositionForAnaphor anaphor2 = new PositionForAnaphor(281, 24, anaphora2);

        return asList(relatedExpression, anaphor1, anaphor2);
    }

}
