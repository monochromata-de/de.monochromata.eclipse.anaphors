package de.monochromata.eclipse.anaphors.editor;

import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable.syncExec;
import static org.eclipse.ui.dialogs.IOverwriteQuery.ALL;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.FutureTask;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.ElementChangedEvent;
import org.eclipse.jdt.core.IElementChangedListener;
import org.eclipse.jdt.core.IJavaElementDelta;
import org.eclipse.jdt.core.compiler.IProblem;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.swtbot.eclipse.finder.SWTWorkbenchBot;
import org.eclipse.swtbot.eclipse.finder.widgets.SWTBotEclipseEditor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.IOverwriteQuery;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.internal.ide.filesystem.FileSystemStructureProvider;
import org.eclipse.ui.intro.IIntroManager;
import org.eclipse.ui.intro.IIntroPart;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.wizards.datatransfer.ImportOperation;
import org.junit.Rule;

import de.monochromata.eclipse.anaphors.annotation.AnnotationsTesting;
import de.monochromata.eclipse.anaphors.editor.config.TestConfig;
import de.monochromata.eclipse.anaphors.editor.interactive.CaptureScreenshotOnErrorOrFailure;
import de.monochromata.eclipse.anaphors.position.PositionsTesting;

/**
 * Abstract base class for tests of {@link CognitiveEditor}. Subclasses need to
 * be run as JUnit Plug-in tests.
 */
public abstract class AbstractEditorTest implements AnnotationsTesting, PositionsTesting, ProblemMarkersTesting,
        SelectedRangeTesting, SourceCodeTesting, SynchronousTesting, WaitingDuringTesting {

    @Rule
    public CaptureScreenshotOnErrorOrFailure screenshot = new CaptureScreenshotOnErrorOrFailure();

    protected IEditorPart editor;
    protected final SWTWorkbenchBot bot = new SWTWorkbenchBot();
    protected final TestConfig config;

    public AbstractEditorTest(final TestConfig config) {
        this.config = config;
    }

    protected SWTWorkbenchBot getBot() {
        return bot;
    }

    public String getResourcePath() {
        return "/" + getTestedType() + ".java";
    }

    protected String getTestedType() {
        final String testName = getClass().getName().replace('.', '/');
        return testName.substring(0, testName.length() - ("Test".length()));
    }

    protected void closeIntroIfExists() {
        final IIntroManager introManager = PlatformUI.getWorkbench().getIntroManager();
        final IIntroPart intro = introManager.getIntro();
        if (intro != null) {
            performInUIThread(() -> {
                introManager.closeIntro(intro);
            });
        }
    }

    protected void makeSureTestResourcesProjectExists()
            throws CoreException, InvocationTargetException, InterruptedException, IOException {
        final IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
        final IPath testResourcesPath = new Path(getTestResourcesProjectPath());
        final IProject testResourcesProject = workspaceRoot.getProject(getTestResourcesProjectName());
        if (!testResourcesProject.exists()) {
            importTestResourcesProject(testResourcesPath, testResourcesProject);
        }
    }

    protected void importTestResourcesProject(final IPath testResourcesPath, final IProject testResourcesProject)
            throws InvocationTargetException, InterruptedException {
        final File sourceDirectory = new File(getTestResourcesProjectRelativePath());
        assertThat(sourceDirectory)
                .describedAs("Directory for test resource project: " + getTestResourcesProjectRelativePath() + "="
                        + sourceDirectory.getAbsolutePath()
                        + " (Make sure to run the tests in a sibling directory of the test resources project)")
                .exists();

        final FileSystemStructureProvider fileSystemStructureProvider = new FileSystemStructureProvider();
        final IOverwriteQuery overwriteQuery = pathString -> ALL;
        final ImportOperation operation = new ImportOperation(testResourcesPath, sourceDirectory,
                fileSystemStructureProvider, overwriteQuery, Arrays.asList(new File[] { sourceDirectory }));
        performInUIThread(() -> {
            operation.setContext(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
        });
        operation.setCreateContainerStructure(false);
        operation.run(null);
        assertThat(operation.getStatus().isOK()).describedAs("test resources import operation status is OK").isTrue();
    }

    protected String getTestResourcesProjectName() {
        return config.languageLevel.testProjectName;
    }

    protected String getTestResourcesProjectPath() {
        return "/" + getTestResourcesProjectName();
    }

    protected String getTestResourcesProjectRelativePath() {
        return ".." + getTestResourcesProjectPath();
    }

    protected void showJavaPerspective() throws InterruptedException {
        final IWorkbench workbench = PlatformUI.getWorkbench();
        assertThat(workbench.isStarting()).describedAs("Workbench is still starting").isFalse();
        performInUIThread(() -> {
            try {
                final IWorkbenchWindow activeWorkbenchWindow = workbench.getActiveWorkbenchWindow();
                assertThat(activeWorkbenchWindow).describedAs("active workbench window").isNotNull();
                final String perspectiveId = JavaUI.ID_PERSPECTIVE;
                workbench.showPerspective(perspectiveId, activeWorkbenchWindow);
            } catch (final Exception e) {
                throw new RuntimeException("Unexpected exception", e);
            }
        });
    }

    protected void setEditorAreaVisible() {
        final IWorkbench workbench = PlatformUI.getWorkbench();
        performInUIThread(() -> {
            final IWorkbenchWindow activeWorkbenchWindow = workbench.getActiveWorkbenchWindow();
            final IWorkbenchPage page = activeWorkbenchWindow.getActivePage();
            page.setEditorAreaVisible(true);
        });
    }

    protected void openEditor(final IEditorInput input) throws Exception {
        final IWorkbench workbench = PlatformUI.getWorkbench();
        final FutureTask<IEditorPart> task = new FutureTask<>(() -> {
            final IWorkbenchWindow activeWorkbenchWindow = workbench.getActiveWorkbenchWindow();
            final IWorkbenchPage page = activeWorkbenchWindow.getActivePage();
            return IDE.openEditor(page, input, CognitiveEditor.COGNITIVE_EDITOR_ID);
        });
        performInUIThread(task);
        editor = task.get();
    }

    protected abstract String getEditorId();

    protected IEditorPart getEditor() {
        return editor;
    }

    protected void closeEditor() {
        if (editor == null) {
            return;
        }
        final IWorkbench workbench = PlatformUI.getWorkbench();
        performInUIThread(() -> {
            final IWorkbenchWindow activeWorkbenchWindow = workbench.getActiveWorkbenchWindow();
            final IWorkbenchPage page = activeWorkbenchWindow.getActivePage();
            final boolean saveIsFalse = false;
            page.closeEditor(editor, saveIsFalse);
            editor = null;
        });
    }

    protected void performInUIThread(final Runnable runnable) {
        if (PlatformUI.getWorkbench().getDisplay().getThread() == Thread.currentThread()) {
            throw new IllegalStateException(
                    "The test is executed in the UI thread. Configure it to not run in the UI thread."
                            + "E.g. in the Run Configuration > Test tab: uncheck `Run in UI thread`.");
        }
        PlatformUI.getWorkbench().getDisplay().syncExec(runnable);
    }

    private FileStoreEditorInput getFileStoreEditorInput() throws URISyntaxException {
        final URL resourceURL = getClass().getClassLoader().getResource(getResourcePath());
        assertThat(resourceURL).isNotNull();

        final IFileStore fileStore = EFS.getLocalFileSystem().getStore(resourceURL.toURI());
        final FileStoreEditorInput input = new FileStoreEditorInput(fileStore);
        return input;
    }

    protected FileEditorInput getFileEditorInput() throws URISyntaxException {
        final IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
        final Path resourcePath = new Path(getTestResourcesProjectPath() + "/src/main/java" + getResourcePath());
        final IFile resourceFile = workspaceRoot.getFile(resourcePath);
        assertThat(resourceFile.exists()).describedAs("Workspace resource exists").isTrue();
        return new FileEditorInput(resourceFile);
    }

    @Override
    public String getActualEventualSourceCode(final CognitiveEditor editor) {
        final ISourceViewer viewer = editor.getViewer();
        final IDocument document = viewer.getDocument();
        final String actualRefactoredSourceCode = document.get();
        return actualRefactoredSourceCode;
    }

    @Override
    public String getActualUnderspecifiedEventualSourceCode(final CognitiveEditor editor) {
        return syncExec(() -> editor.getViewer().getTextWidget().getText());
    }

    protected void deleteTextOnLine(final int zeroBasedLineIndex) {
        final SWTBotEclipseEditor textEditor = getBot().activeEditor().toTextEditor();
        final String line = textEditor.getTextOnLine(zeroBasedLineIndex);
        final String lineSeparator = System.getProperty("line.separator");
        final String textToSelect = line + lineSeparator;
        textEditor.selectRange(zeroBasedLineIndex, 0, textToSelect.length());
        textEditor.insertText("");
    }

    public static class ProblemCollectingChangeListener implements IElementChangedListener {

        private IProblem[] problems;

        @Override
        public void elementChanged(final ElementChangedEvent event) {
            final IJavaElementDelta delta = event.getDelta();
            final CompilationUnit compilationUnitAST = delta.getCompilationUnitAST();
            if (compilationUnitAST != null) {
                problems = compilationUnitAST.getProblems();
            }
        }

        /**
         * Return the list of problems, or null.
         *
         * @return Only non-null, if there has been at least one delta contained in an
         *         {@link ElementChangedEvent} that provided a {@link CompilationUnit}.
         */
        public List<IProblem> getProblems() {
            return problems == null ? null : Arrays.asList(problems);
        }
    }
}
