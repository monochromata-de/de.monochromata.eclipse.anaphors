package de.monochromata.eclipse.anaphors.editor.simple;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.compiler.IProblem;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;

public class MI_IA1Mr_Rt_QualifiedWithUnresolvedMethodIsNotResolved_SimpleTest
        extends AbstractSimpleEditorTest {

    private static final String[] expectedProblems = new String[] {
            "The method create() is undefined for the type MI_IA1Mr_Rt_QualifiedWithUnresolvedMethodIsNotResolved_Simple",
            "foo cannot be resolved to a variable",
            "The method make() from the type MI_IA1Mr_Rt_QualifiedWithUnresolvedMethodIsNotResolved_Simple is never used locally",
            "The method createFoo() from the type MI_IA1Mr_Rt_QualifiedWithUnresolvedMethodIsNotResolved_Simple.ObjectSession is never used locally"
    };

    @Override
    public void waitForAnaphoraRelations(final CognitiveEditor editor) {
        // Wait for anaphora resolution - it should report that it resolved 0 anaphoras
        // during 1 resolution job
        waitForAnaphoraRelations(editor, () -> addedAnaphoras.size() == 1 && addedAnaphoras.get(0).isEmpty());
    }

    @Override
    protected void waitForProblemsToBeSolved(final CognitiveEditor editor) {
        waitForProblems(editor, expectedProblems.length);
    }

    @Override
    protected int getLineToAddLinebreakTo(final CognitiveEditor editor) {
        // Return a fixed line instead of the line of the anaphor, because no anaphor is
        // present.
        return 2;
    }

    @Override
    protected void assertRefactoringResults(final CognitiveEditor editor,
            final List<IProblem> problems) {
        assertEventualSourceCode(editor);
        assertUnderspecifiedEventualSourceCode(editor);
        assertProblemMarkers(editor, problems, expectedProblems);
        // Do not assert anaphora relations
    }

    @Override
    protected Pair<Anaphora, Anaphora> getExpectedResolvedAndRealizedAnaphora() {
        throw new UnsupportedOperationException("No anaphora relations are expected.");
    }

    @Override
    protected boolean hasAnaphoraBeenReResolved() {
        // initially, no resolution will have been triggered, only the edit triggers a
        // resolution
        return addedAnaphoras.size() == 1;
    }

    @Override
    protected void assertReResolvedAnaphoraRelations() {
        assertThat(getNumberOfAnaphoraRelations()).isEqualTo(0);
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
        builder.addLine("");
        builder.addLine("public class MI_IA1Mr_Rt_QualifiedWithUnresolvedMethodIsNotResolved_Simple {");
        builder.addLine("");
        builder.addLine("    public static void main() {");
        builder.addLine("        create()");
        builder.addLine("            .createFoo();");
        builder.addLine("        System.err.println(foo);");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static ObjectSession make() {");
        builder.addLine("        return new ObjectSession();");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static class ObjectSession {");
        builder.addLine("        public Foo createFoo() {");
        builder.addLine("            return new Foo();");
        builder.addLine("        }");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static class Foo {");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
        builder.addLine("");
        builder.addLine("public class MI_IA1Mr_Rt_QualifiedWithUnresolvedMethodIsNotResolved_Simple {");
        builder.addLine("");
        builder.addLine("    public static void main() {");
        builder.addLine("        create()");
        builder.addLine("            .createFoo();");
        builder.addLine("        System.err.println(foo);");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static ObjectSession make() {");
        builder.addLine("        return new ObjectSession();");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static class ObjectSession {");
        builder.addLine("        public Foo createFoo() {");
        builder.addLine("            return new Foo();");
        builder.addLine("        }");
        builder.addLine("    }");
        builder.addLine("");
        builder.addLine("    private static class Foo {");
        builder.addLine("    }");
        builder.addLine("}");
        return builder.toString();
    }

}
