package de.monochromata.eclipse.anaphors.editor.occurrences;

import static de.monochromata.eclipse.anaphors.editor.occurrences.ResolutionOccurrencesUpdating.SECONDS_TO_DISPLAY_RESOLUTION_OCCURRENCES;
import static de.monochromata.eclipse.anaphors.editor.occurrences.ResolutionOccurrencesUpdating.updateResolutionOccurrences;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.AnnotationModel;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.junit.Test;

import de.monochromata.eclipse.anaphors.NoopTesting;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class ResolutionOccurrencesUpdatingTest implements ResolutionOccurrencesUpdatingTesting, NoopTesting {

    private final Object lock = new Object();
    private final IAnnotationModel annotationModel = new AnnotationModel();

    @Test
    public void addedAnnotationsAreReturned() {
        final Annotation[] addedAnnotations = updateResolutionOccurrences(lock, annotationModel, new Annotation[0],
                singletonList(new PositionForRelatedExpression(11, 2)), this::noop);

        assertThat(addedAnnotations).hasSize(1);
    }

    @Test
    public void returnedAnnotationsAreAddedToTheModel() {
        final Annotation[] addedAnnotations = updateResolutionOccurrences(lock, annotationModel, new Annotation[0],
                singletonList(new PositionForRelatedExpression(11, 2)), this::noop);

        assertThat(annotationModel.getPosition(addedAnnotations[0])).isEqualTo(new Position(11, 2));
    }

    @Test
    public void existingOccurrencesAreNotReplacedByEmptyListOfNewOnes() {
        final Annotation existingAnnotation = createAndAddAnnotation(annotationModel);

        updateResolutionOccurrences(lock, annotationModel, new Annotation[] { existingAnnotation },
                emptyList(), this::noop);

        assertThat(annotationModel.getPosition(existingAnnotation)).isNotNull();
    }

    @Test
    public void existingOccurrencesAreReplacedByNewOnes() {
        final Annotation existingAnnotation = createAndAddAnnotation(annotationModel);

        updateResolutionOccurrences(lock, annotationModel, new Annotation[] { existingAnnotation },
                singletonList(new PositionForRelatedExpression(11, 2)), this::noop);

        assertThat(annotationModel.getPosition(existingAnnotation)).isNull();
    }

    @Test
    public void addOccurrencesForAllAddedOrUpdatedAnaphors() {
        final Annotation[] updatedAnnotations = updateResolutionOccurrences(lock, annotationModel,
                new Annotation[0],
                asList(new PositionForRelatedExpression(10, 1),
                        new PositionForRelatedExpression(30, 3)),
                this::noop);

        assertThat(updatedAnnotations).hasSize(2);
    }

    /**
     * A related expression might be referred to by multiple anaphors. Even in this
     * case it should be represented by a single occurrence only.
     */
    @Test
    public void addOnlyOneOccurrenceForARelatedExpressionIncludedMultipleTimes() {
        final Annotation[] updatedAnnotations = updateResolutionOccurrences(lock, annotationModel,
                new Annotation[0],
                asList(new PositionForRelatedExpression(10, 1),
                        new PositionForRelatedExpression(10, 1)),
                this::noop);

        assertThat(updatedAnnotations).hasSize(1);
    }

    @Test
    public void schedulesAJobToRemoveTheAddedOccurrences() {
        final AtomicReference<Pair<RemoveResolutionOccurrencesJob, Long>> scheduledJob = new AtomicReference<>();

        final Annotation[] addedAnnotations = updateResolutionOccurrences(lock, annotationModel, new Annotation[0],
                singletonList(new PositionForRelatedExpression(10, 1)), this::noop,
                (job, delayInMs) -> scheduledJob.set(new ImmutablePair<>(job, delayInMs)));

        assertThat(scheduledJob.get().getRight()).isEqualTo(SECONDS_TO_DISPLAY_RESOLUTION_OCCURRENCES * 1000l);
        assertThat(scheduledJob.get().getLeft().getAnnotationsToRemove()).isSameAs(addedAnnotations);
    }

}
