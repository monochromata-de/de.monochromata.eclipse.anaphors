package de.monochromata.eclipse.anaphors.editor.update;

import static de.monochromata.eclipse.anaphors.editor.update.AnaphoraComparing.isEqualRaw;
import static org.assertj.core.api.Assertions.assertThat;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.junit.Test;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;

public class AnaphoraComparingTest {

	@Test
	public void anAnaphoraWithRelatedExpressionAtDifferentPositionIsNotEqual() {
		assertThat(isEqualRaw(new ImmutableTriple<>(new Position(0, 10), null, null),
				new ImmutableTriple<>(new Position(1, 10), null, null))).isFalse();
	}

	@Test
	public void anAnaphoraWithDifferentKindIsNotEqual() {
		final Position identicalRelatedExpressionPosition = new Position(0, 10);
		assertThat(isEqualRaw(
				new ImmutableTriple<>(identicalRelatedExpressionPosition,
						new ImmutablePair<>("kindOfRelatedExpression-kindOfAnaphorResolution-kindOfReferentialization",
								"anaphor"),
						null),
				new ImmutableTriple<>(identicalRelatedExpressionPosition,
						new ImmutablePair<>(
								"otherKindOfRelatedExpression-kindOfAnaphorResolution-kindOfReferentialization",
								"anaphor"),
						null))).isFalse();
	}

	@Test
	public void anAnaphoraWithDifferentAnaphorIsNotEqual() {
		final Position identicalRelatedExpressionPosition = new Position(0, 10);
		assertThat(isEqualRaw(
				new ImmutableTriple<>(identicalRelatedExpressionPosition,
						new ImmutablePair<>("kindOfRelatedExpression-kindOfAnaphorResolution-kindOfReferentialization",
								"anaphor"),
						null),
				new ImmutableTriple<>(identicalRelatedExpressionPosition,
						new ImmutablePair<>("kindOfRelatedExpression-kindOfAnaphorResolution-kindOfReferentialization",
								"differentAnaphor"),
						null))).isFalse();
	}

	@Test
	public void anAnaphoraWithAnaphorAtDifferentPositionIsNotEqual() {
		final Position identicalRelatedExpressionPosition = new Position(0, 10);
		final Pair<String, String> identicalKindAndAnaphor = new ImmutablePair<>(
				"kindOfRelatedExpression-kindOfAnaphorResolution-kindOfReferentialization", "anaphor");
		assertThat(isEqualRaw(
				new ImmutableTriple<>(identicalRelatedExpressionPosition, identicalKindAndAnaphor, new Position(20, 3)),
				new ImmutableTriple<>(identicalRelatedExpressionPosition, identicalKindAndAnaphor,
						new Position(21, 3)))).isFalse();
	}

	@Test
	public void anAnaphoraWithSameRelatedExpressionPositionKindAndAnaphorPositionIsEqual() {
		final Anaphora anaphora = new Anaphora("a", "b", "c", "d");
		assertThat(isEqualRaw(
				new ImmutableTriple<>(new Position(0, 10),
						new ImmutablePair<>("kindOfRE-kindOfAnaRes-kindOfRef", "anaphor"),
						new PositionForAnaphor(20, 3, anaphora)),
				new ImmutableTriple<>(new Position(0, 10),
						new ImmutablePair<>("kindOfRE-kindOfAnaRes-kindOfRef", "anaphor"), new Position(20, 3))))
								.isTrue();
	}

}
