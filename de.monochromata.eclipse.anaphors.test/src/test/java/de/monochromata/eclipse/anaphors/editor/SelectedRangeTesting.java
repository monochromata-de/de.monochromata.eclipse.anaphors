package de.monochromata.eclipse.anaphors.editor;

import static org.assertj.core.api.Assertions.assertThat;

import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.IEditorPart;

import de.monochromata.eclipse.anaphors.swt.SWTAccess;

public interface SelectedRangeTesting {

    default void assertSelectedRange(final int selectionOffset, final int selectionLength,
            final IEditorPart editor) {
        assertSelectedRange(selectionOffset, selectionLength, (CognitiveEditor) editor);
    }

    default void assertSelectedRange(final int selectionOffset, final int selectionLength,
            final CognitiveEditor editor) {
        final ISourceViewer viewer = editor.getViewer();
        final Point selectedRange = SWTAccess.supplySync(viewer.getTextWidget(), viewer::getSelectedRange);
        assertThat(selectedRange).isEqualTo(new Point(selectionOffset, selectionLength));
    }

}
