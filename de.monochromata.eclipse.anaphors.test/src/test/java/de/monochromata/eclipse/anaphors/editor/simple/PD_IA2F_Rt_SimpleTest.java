package de.monochromata.eclipse.anaphors.editor.simple;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;

public class PD_IA2F_Rt_SimpleTest extends AbstractSimpleEditorTest {

	@Override
	public String getExpectedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
		builder.addLine("");
		builder.addLine("public class PD_IA2F_Rt_Simple {");
		builder.addLine("");
		builder.addLine("	public static void main(String[] args) {");
		builder.addLine("		foo(new A());");
		builder.addLine("	}");
		builder.addLine("");
		builder.addLine("	public static void foo(A foo) {");
		builder.addLine("		System.err.println(foo.Field);");
		builder.addLine("	}");
		builder.addLine("");
		builder.addLine("	private static class A {");
		builder.addLine("		public Integer Field;");
		builder.addLine("	}");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	public String getExpectedUnderspecifiedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
		builder.addLine("");
		builder.addLine("public class PD_IA2F_Rt_Simple {");
		builder.addLine("");
		builder.addLine("	public static void main(String[] args) {");
		builder.addLine("		foo(new A());");
		builder.addLine("	}");
		builder.addLine("");
		builder.addLine("	public static void foo(A foo) {");
		builder.addLine("		System.err.println(field);");
		builder.addLine("	}");
		builder.addLine("");
		builder.addLine("	private static class A {");
		builder.addLine("		public Integer Field;");
		builder.addLine("	}");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	protected Pair<Anaphora, Anaphora> getExpectedResolvedAndRealizedAnaphora() {
		return new ImmutablePair<>(new Anaphora("PD", "IA2F", "Rt", "Integer"),
				new Anaphora("PD", "IA2F", "Rn", "field"));
	}

}
