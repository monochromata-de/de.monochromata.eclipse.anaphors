package de.monochromata.eclipse.anaphors.editor.simple;

import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadLocationException;
import static de.monochromata.eclipse.position.PositionQuerying.getPositions;
import static java.util.Arrays.stream;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.AbstractCognitiveEditorTest;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.editor.config.TestConfig;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;

/**
 * An abstract base class for a simple test that opens a file that contains a
 * single anaphor and asserts whether the anaphor is resolved correctly.
 */
public abstract class AbstractSimpleEditorTest extends AbstractCognitiveEditorTest {

    private static final Pattern KIND_FROM_CLASS_NAME_PATTERN = Pattern
            .compile("^(?<RelExpKind>[0-9a-zA-Z]+)_(?<AnaResKind>[0-9a-zA-Z]+)_(?<RefKind>[0-9a-zA-Z]+)_.*$");

    public AbstractSimpleEditorTest() {
        super();
    }

    public AbstractSimpleEditorTest(final TestConfig config) {
        super(config);
    }

    /**
     * @return The expected kind of anaphor using
     *         {@link #KIND_FROM_CLASS_NAME_PATTERN}
     */
    @Override
    protected String getExpectedKindOfResolvedAnaphora() {
        final String className = getClass().getSimpleName();
        final Matcher matcher = KIND_FROM_CLASS_NAME_PATTERN.matcher(className);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Cannot extract expected kind of anaphora from class name " + className);
        }
        return matcher.group("RelExpKind") + "-" + matcher.group("AnaResKind") + "-" + matcher.group("RefKind");
    }

    @Override
    protected void manipulateEditorAndCheckAssertions(final ProblemCollectingChangeListener problemCollector,
            final CognitiveEditor editor) {
        checkAssertions(problemCollector, editor);
        testReResolution(editor);
    }

    protected void testReResolution(final CognitiveEditor editor) {
        addAnEmptyLineToTriggerReResolution(editor);
        waitForAnaphoraRelations(editor, this::hasAnaphoraBeenReResolved);
        assertReResolvedAnaphoraRelations();
        // If re-resolution failed, an exception will be thrown that makes the test
        // fail.
    }

    protected void checkAssertions(final ProblemCollectingChangeListener problemCollector,
            final CognitiveEditor editor) {
        waitForAnaphoraRelations(editor);
        waitForProblemsToBeSolved(editor);
        assertRefactoringResults(editor, problemCollector);
    }

    protected void addAnEmptyLineToTriggerReResolution(final CognitiveEditor editor) {
        final int lineWithAnaphor = getLineToAddLinebreakTo(editor);
        getBot().activeEditor().toTextEditor().insertText(lineWithAnaphor, 0, "\n");
        getBot().activeEditor().save();
    }

    protected int getLineToAddLinebreakTo(final CognitiveEditor editor) {
        return getLineThatContainsTheAnaphor(editor);
    }

    protected int getLineThatContainsTheAnaphor(final CognitiveEditor editor) {
        final IDocument document = editor.getDocumentProvider().getDocument(editor.getEditorInput());
        final Position anaphorPosition = stream(getPositions(ANAPHORA_CATEGORY, document))
                .filter(position -> position instanceof PositionForAnaphor)
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("No anaphor found"));
        return wrapBadLocationException(() -> document.getLineOfOffset(anaphorPosition.offset));
    }

    protected boolean hasAnaphoraBeenReResolved() {
        return addedAnaphoras.size() == getNumberOfAnaphorasAfterReResolution();
    }

    protected void assertReResolvedAnaphoraRelations() {
        assertThat(addedAnaphoras).hasSize(getNumberOfAnaphorasAfterReResolution());
        assertAnaphoraRelations();
    }

    protected int getNumberOfAnaphorasAfterReResolution() {
        return 2;
    }

    @Override
    protected void assertAnaphoraRelation() {
        super.assertAnaphoraRelation();
        assertThat(getOnlyAddedAnaphoraRelation())
                .isEqualTo(getExpectedResolvedAndRealizedAnaphora());
    }

    protected abstract Pair<Anaphora, Anaphora> getExpectedResolvedAndRealizedAnaphora();

}
