package de.monochromata.eclipse.anaphors.editor.config;

public class TestConfig {

    public final LanguageLevel languageLevel;

    public TestConfig(final LanguageLevel languageLevel) {
        this.languageLevel = languageLevel;
    }

}
