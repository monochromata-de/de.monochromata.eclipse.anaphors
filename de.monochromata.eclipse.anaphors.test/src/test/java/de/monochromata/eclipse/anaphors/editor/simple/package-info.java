/**
 * Test cases for
 * {@link de.monochromata.eclipse.anaphors.editor.CognitiveEditor}
 * that test each combination of related expression strategy, anaphor resolution
 * strategy and referentialization strategy by loading a Java file containing a
 * single anaphor.
 */
package de.monochromata.eclipse.anaphors.editor.simple;