package de.monochromata.eclipse.anaphors.editor.simple.feature;

import java.util.List;

import org.eclipse.jdt.core.compiler.IProblem;

import de.monochromata.eclipse.anaphors.editor.AbstractCognitiveEditorTest;
import de.monochromata.eclipse.anaphors.editor.AbstractEditorTest.ProblemCollectingChangeListener;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;

public abstract class AbstractSimpleFeatureEditorTest extends AbstractCognitiveEditorTest {

    @Override
    protected void manipulateEditorAndCheckAssertions(final ProblemCollectingChangeListener problemCollector,
            final CognitiveEditor editor) {
        waitForAnaphoraRelations(editor);
        waitForProblemsToBeSolved(editor);
        assertRefactoringResults(editor, problemCollector);
    }

    @Override
    protected void assertRefactoringResults(final CognitiveEditor editor, final List<IProblem> problems) {
        assertEventualSourceCode(editor);
        assertUnderspecifiedEventualSourceCode(editor);
        assertNoProblemMarkersPresent(editor, problems);
        // Do not wait for and do not assert anaphora relations
    }

    @Override
    protected String getExpectedKindOfResolvedAnaphora() {
        throw new UnsupportedOperationException();
    }

}
