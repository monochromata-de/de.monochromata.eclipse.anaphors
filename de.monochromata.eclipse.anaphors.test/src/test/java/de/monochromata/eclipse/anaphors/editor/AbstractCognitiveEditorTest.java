package de.monochromata.eclipse.anaphors.editor;

import static de.monochromata.eclipse.anaphors.editor.CognitiveEditor.COGNITIVE_EDITOR_ID;
import static de.monochromata.eclipse.anaphors.editor.config.LanguageLevel.JLS_8;
import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent.EventType.ABOUT_TO_REALIZE_ANAPHORS;
import static java.lang.Double.parseDouble;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jdt.core.ElementChangedEvent;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.compiler.IProblem;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.part.FileEditorInput;
import org.junit.After;
import org.junit.AssumptionViolatedException;
import org.junit.Before;
import org.junit.Test;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.perspectivation.strategy.PerspectivationStrategy;
import de.monochromata.eclipse.anaphors.Activator;
import de.monochromata.eclipse.anaphors.editor.config.TestConfig;
import de.monochromata.eclipse.anaphors.editor.update.job.ResolveAnaphorsJob;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent;
import de.monochromata.eclipse.anaphors.event.ResolveAnaphorsJobEvent;

public abstract class AbstractCognitiveEditorTest extends AbstractEditorTest implements AnaphoraInEditorTesting {

	protected final List<List<Pair<Anaphora, Anaphora>>> addedAnaphoras = new ArrayList<>();
	protected final List<List<Anaphora>> retainedAnaphoras = new ArrayList<>();
	protected final List<List<Triple<Anaphora, Anaphora, Anaphora>>> updatedAnaphoras = new ArrayList<>();
	protected final List<List<Anaphora>> removedAnaphoras = new ArrayList<>();
	protected final List<Exception> exceptionsDuringAnaphorResolution = new ArrayList<>();
	private ProblemCollectingChangeListener problemCollector;

	public AbstractCognitiveEditorTest() {
		super(new TestConfig(JLS_8));
	}

	public AbstractCognitiveEditorTest(final TestConfig config) {
		super(config);
	}

	@Override
	protected String getEditorId() {
		return COGNITIVE_EDITOR_ID;
	}

	@Override
	public List<List<Pair<Anaphora, Anaphora>>> getAddedAnaphoras() {
		return addedAnaphoras;
	}

	@Override
	public List<List<Anaphora>> getRetainedAnaphoras() {
		return retainedAnaphoras;
	}

	@Override
	public List<List<Triple<Anaphora, Anaphora, Anaphora>>> getUpdatedAnaphoras() {
		return updatedAnaphoras;
	}

	@Override
	public List<List<Anaphora>> getRemovedAnaphoras() {
		return removedAnaphoras;
	}

	protected void configurePerspectivationStrategy() {
		Activator.getDefault().setPerspectivationStrategy(getPerspectivationStrategy());
	}

	protected PerspectivationStrategy getPerspectivationStrategy() {
		return PerspectivationStrategy.underspecifyEverything();
	}

	@Before
	public void setUp() throws Exception {
		problemCollector = new ProblemCollectingChangeListener();
		closeIntroIfExists();
		showJavaPerspective();
		setEditorAreaVisible();
		makeSureTestResourcesProjectExists();
		configurePerspectivationStrategy();
	}

	@Test
	public void testEditor() throws Exception {
		Runnable stopListeningToAnaphorResolution = null;
		Runnable stopListeningToResolveAnaphorsJobs = null;
		try {
			requireMinimumJavaSpecVersion();
			final FileEditorInput fileEditorInput = getFileEditorInput();
			final IEditorInput input = fileEditorInput;// getFileStoreEditorInput();

			JavaCore.addElementChangedListener(problemCollector);

			openEditor(input);

			assertThat(getEditor()).isInstanceOf(CognitiveEditor.class);
			stopListeningToAnaphorResolution = listenToAnaphorResolution((CognitiveEditor) getEditor());
			stopListeningToResolveAnaphorsJobs = listenToResolveAnaphorsJobs((CognitiveEditor) getEditor());
			manipulateEditorAndCheckAssertions(problemCollector, (CognitiveEditor) getEditor());
			assertThat(exceptionsDuringAnaphorResolution).isEmpty();
		} catch (final Throwable throwable) {
			screenshot.captureScreenshot(this, "testEditor", throwable);
			throw throwable;
		} finally {
			if (stopListeningToAnaphorResolution != null) {
				stopListeningToAnaphorResolution.run();
			}
			if (stopListeningToResolveAnaphorsJobs != null) {
				stopListeningToResolveAnaphorsJobs.run();
			}
			JavaCore.removeElementChangedListener(problemCollector);
		}
	}

	protected void requireMinimumJavaSpecVersion() {
		final double specVersion = parseDouble(System.getProperty("java.specification.version"));
		if (specVersion < config.languageLevel.javaSpecVersion) {
			throw new AssumptionViolatedException("Test for Java version " + config.languageLevel.javaSpecVersion
					+ " disabled during run with Java version " + specVersion);
		}
	}

	protected abstract void manipulateEditorAndCheckAssertions(ProblemCollectingChangeListener problemCollector,
			CognitiveEditor editor);

	@After
	public void tearDown() {
		closeEditor();
		JavaCore.removeElementChangedListener(problemCollector);
		problemCollector = null;
	}

	protected Runnable listenToAnaphorResolution(final CognitiveEditor editor) {
		final Consumer<AnaphorResolutionEvent> listener = this::processAnaphorResolution;
		editor.getAnaphorResolutionNotifier().addListener(ABOUT_TO_REALIZE_ANAPHORS, listener);
		return () -> editor.getAnaphorResolutionNotifier().removeListener(ABOUT_TO_REALIZE_ANAPHORS, listener);
	}

	protected void processAnaphorResolution(final AnaphorResolutionEvent event) {
		addedAnaphoras.add(event.addedAnaphoras);
		retainedAnaphoras.add(event.retainedAnaphoras);
		updatedAnaphoras.add(event.updatedAnaphoras);
		removedAnaphoras.add(event.removedAnaphoras);
	}

	protected Runnable listenToResolveAnaphorsJobs(final CognitiveEditor editor) {
		final Consumer<Pair<Optional<ResolveAnaphorsJob>, Optional<Exception>>> listener = jobAndException -> jobAndException
				.getRight().ifPresent(exceptionsDuringAnaphorResolution::add);
		editor.getResolveAnaphorsJobNotifier().addListenerForAllEvents(ResolveAnaphorsJobEvent.class, listener);
		return () -> editor.getResolveAnaphorsJobNotifier().removeListenerForAllEvents(ResolveAnaphorsJobEvent.class,
				listener);
	}

	@Override
	public void assertAnaphoraRelations() {
		assertAnaphoraRelation();
	}

	protected void assertAnaphoraRelation() {
		assertThat(getNumberOfAnaphoraRelations()).isEqualTo(1);
		assertThat(getLatestAnaphoraRelation().getLeft().kind).isEqualTo(getExpectedKindOfResolvedAnaphora());
	}

	protected abstract String getExpectedKindOfResolvedAnaphora();

	protected void assertRefactoringResults(final IEditorPart editor,
			final ProblemCollectingChangeListener problemCollector) {
		final List<IProblem> problems = problemCollector.getProblems();
		assertRefactoringResults((CognitiveEditor) editor, problems);
	}

	protected void assertRefactoringResults(final CognitiveEditor editor, final List<IProblem> problems) {
		assertEventualSourceCode(editor);
		assertUnderspecifiedEventualSourceCode(editor);
		assertProblemMarkers(editor, problems);
		assertAnaphoraRelations();
	}

	protected void assertProblemMarkers(final CognitiveEditor editor, final List<IProblem> problems) {
		assertNoProblemMarkersPresent(editor, problems);
	}

	protected void waitForProblemsToBeSolved(final CognitiveEditor editor) {
		waitForProblems(editor, 0);
	}

	protected void waitForProblems(final CognitiveEditor editor, final int expectedNumberOfProblems) {
		// Initialize from annotations, but update from element changes because
		// annotation updates are slow and might get lost.
		final AtomicBoolean hasNoProblems = new AtomicBoolean(!hasProblems(editor, expectedNumberOfProblems));
		final Supplier<Boolean> condition = hasNoProblems::get;
		if (problemCollector.getProblems() == null || !condition.get()) {
			final Object waitee = new Object();
			final ProblemListenerThatAbortsWaitingWhenTestCanProceed waitingListener = new ProblemListenerThatAbortsWaitingWhenTestCanProceed(
					expectedNumberOfProblems, hasNoProblems, waitee);
			JavaCore.addElementChangedListener(waitingListener);
			waitSecondsIfNotNotifiedBefore("problems to be solved", WAIT_FOR_CHANGE_TIMEOUT, condition, waitee);
			JavaCore.removeElementChangedListener(waitingListener);
		}
	}

	private boolean hasProblems(final CognitiveEditor editor, final int expectedNumberOfProblems) {
		final IAnnotationModel annotationModel = editor.getDocumentProvider()
				.getAnnotationModel(editor.getEditorInput());
		return getNumberOfProblems(annotationModel) > expectedNumberOfProblems;
	}

	private int getNumberOfProblems(final IAnnotationModel annotationModel) {
		final Iterator<Annotation> iter = annotationModel.getAnnotationIterator();
		int numberOfProblems = 0;
		while (iter.hasNext()) {
			final Annotation next = iter.next();
			switch (next.getType()) {
			case "org.eclipse.ui.workbench.texteditor.task":
			case "org.eclipse.jdt.ui.info":
			case "org.eclipse.jdt.ui.warning":
			case "org.eclipse.jdt.ui.error":
				if (!next.isMarkedDeleted()) {
					numberOfProblems++;
				}
				break;
			}
		}
		return numberOfProblems;
	}

	public class ProblemListenerThatAbortsWaitingWhenTestCanProceed extends ProblemCollectingChangeListener {

		private final int expectedNumberOfProblems;
		private final AtomicBoolean hasNoProblems;
		private final Object waitee;

		public ProblemListenerThatAbortsWaitingWhenTestCanProceed(final int expectedNumberOfProblems,
				final AtomicBoolean hasNoProblems, final Object waitee) {
			this.expectedNumberOfProblems = expectedNumberOfProblems;
			this.hasNoProblems = hasNoProblems;
			this.waitee = waitee;
		}

		@Override
		public void elementChanged(final ElementChangedEvent event) {
			super.elementChanged(event);
			final List<IProblem> problems = getProblems();
			if (problems != null && problems.size() == expectedNumberOfProblems) {
				hasNoProblems.set(true);
				notifyWaitee(waitee);
			}
		}
	}

}
