package de.monochromata.eclipse.anaphors.editor.simple.feature;

import de.monochromata.eclipse.anaphors.editor.CodeBuilder;

public class IgnorePrivateFieldForIA2FTest extends AbstractSimpleFeatureEditorTest {

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple.feature;");
        builder.addLine("");
        builder.addLine("public class IgnorePrivateFieldForIA2F {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		foo(new A());");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("	public static void foo(A foo) {");
        builder.addLine("		System.err.println(foo.int0);");
        builder.addLine("	}");
        builder.addLine("}");
        builder.addLine("");
        builder.addLine("class A {");
        builder.addLine("    public Integer int0;");
        builder.addLine("");
        builder.addLine("    /**");
        builder.addLine("     * Does not cause ambiguity because the field is not visible");
        builder.addLine("     */");
        builder.addLine("    @SuppressWarnings(\"unused\")");
        builder.addLine("    private Integer int1;");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple.feature;");
        builder.addLine("");
        builder.addLine("public class IgnorePrivateFieldForIA2F {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		foo(new A());");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("	public static void foo(A foo) {");
        builder.addLine("		System.err.println(int0);");
        builder.addLine("	}");
        builder.addLine("}");
        builder.addLine("");
        builder.addLine("class A {");
        builder.addLine("    public Integer int0;");
        builder.addLine("");
        builder.addLine("    /**");
        builder.addLine("     * Does not cause ambiguity because the field is not visible");
        builder.addLine("     */");
        builder.addLine("    @SuppressWarnings(\"unused\")");
        builder.addLine("    private Integer int1;");
        builder.addLine("}");
        return builder.toString();
    }

}
