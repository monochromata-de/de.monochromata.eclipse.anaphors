package de.monochromata.eclipse.anaphors.editor.update;

import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionAroundASTNode.NodeType.Anaphor;
import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionAroundASTNode.NodeType.RelatedExpression;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionAroundASTNode;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionOnASTNode;
import de.monochromata.eclipse.anaphors.position.AnaphoraPositionAfterEndOfASTNode;
import de.monochromata.eclipse.anaphors.position.AnaphoraPositionBeforeStartOfASTNode;
import de.monochromata.eclipse.anaphors.position.AnaphoraPositionOnAnaphor;
import de.monochromata.eclipse.anaphors.position.AnaphoraPositionOnRelatedExpression;

@RunWith(JUnit4.class)
public class AstBasedAnaphoraUpdatingTest implements AstBasedAnaphoraUpdatingTesting {

    @Test
    public void missingPositionForRelatedExpressionYieldsException() throws CoreException {
        assertAnaphoraUpdate((newScope, preliminaryAnaphora) -> {
            assertThatThrownBy(() -> AstBasedAnaphoraUpdating.updateAnaphoraWithEqualNodes(emptyList(), newScope,
                    preliminaryAnaphora)).isInstanceOf(IllegalArgumentException.class).hasMessage(
                            "The given positions contain no position of type de.monochromata.eclipse.anaphors.position.AnaphoraPositionOnRelatedExpression");
        });
    }

    @Test
    public void missingPositionForAnaphorYieldsException() throws CoreException {
        assertAnaphoraUpdate((newScope, preliminaryAnaphora) -> {
            final ASTNode oldRelatedExpression = preliminaryAnaphora.getRelatedExpression().getRelatedExpression();
            final List<AbstractAnaphoraPositionOnASTNode> positions = singletonList(
                    new AnaphoraPositionOnRelatedExpression(oldRelatedExpression, 28, 19));

            assertThatThrownBy(() -> AstBasedAnaphoraUpdating.updateAnaphoraWithEqualNodes(positions, newScope,
                    preliminaryAnaphora)).isInstanceOf(IllegalArgumentException.class).hasMessage(
                            "The given positions contain no position of type de.monochromata.eclipse.anaphors.position.AnaphoraPositionOnAnaphor");
        });
    }

    @Test
    @Ignore // See #292
    public void newAnaphoraRelationHasEqualNodesFromNewAST() throws CoreException {
        assertAnaphoraUpdate((newScope, preliminaryAnaphora) -> {
            final ASTNode oldRelatedExpression = preliminaryAnaphora.getRelatedExpression().getRelatedExpression();
            final Expression oldAnaphor = preliminaryAnaphora.getAnaphorExpression();
            final List<AbstractAnaphoraPositionOnASTNode> positions = asList(
                    new AnaphoraPositionOnRelatedExpression(oldRelatedExpression, 28, 19),
                    new AnaphoraPositionOnAnaphor(oldAnaphor, 66, 9, null));

            final PublicAnaphora updatedAnaphora = AstBasedAnaphoraUpdating.updateAnaphoraWithEqualNodes(positions,
                    newScope, preliminaryAnaphora);
            assertUpdatedAnaphora(updatedAnaphora, 28, 19, "\"relatedExpression\"", 66, 9, "\"anaphor\"");
        });
    }

    @Test
    public void missingPositionBeforeStartOfRelatedExpressionYieldsException() throws CoreException {
        assertAnaphoraUpdate((newScope, preliminaryAnaphora) -> {
            assertThatThrownBy(() -> AstBasedAnaphoraUpdating.updateAnaphoraWithNodesInRange(emptyList(), newScope,
                    preliminaryAnaphora)).isInstanceOf(NullPointerException.class)
                            .hasMessage("The given positions contain no position with node type RelatedExpression");
        });
    }

    @Test
    public void missingPositionAfterEndOfRelatedExpressionYieldsException() throws CoreException {
        assertAnaphoraUpdate((newScope, preliminaryAnaphora) -> {
            final ASTNode oldRelatedExpression = preliminaryAnaphora.getRelatedExpression().getRelatedExpression();
            final List<AbstractAnaphoraPositionAroundASTNode> positions = singletonList(
                    new AnaphoraPositionBeforeStartOfASTNode(RelatedExpression, oldRelatedExpression, 28, 0));

            assertThatThrownBy(() -> AstBasedAnaphoraUpdating.updateAnaphoraWithNodesInRange(positions, newScope,
                    preliminaryAnaphora)).isInstanceOf(NullPointerException.class)
                            .hasMessage("The given positions contain no position with node type RelatedExpression");
        });
    }

    @Test
    public void missingPositionBeforeStartOfAnaphorYieldsException() throws CoreException {
        assertAnaphoraUpdate((newScope, preliminaryAnaphora) -> {
            final ASTNode oldRelatedExpression = preliminaryAnaphora.getRelatedExpression().getRelatedExpression();
            final AnaphoraPositionBeforeStartOfASTNode beforeStartOfRelatedExpression = new AnaphoraPositionBeforeStartOfASTNode(
                    RelatedExpression, oldRelatedExpression, 28, 0);
            final List<AbstractAnaphoraPositionAroundASTNode> positions = asList(beforeStartOfRelatedExpression,
                    new AnaphoraPositionAfterEndOfASTNode(RelatedExpression, oldRelatedExpression,
                            beforeStartOfRelatedExpression, 47, 0));

            assertThatThrownBy(() -> AstBasedAnaphoraUpdating.updateAnaphoraWithNodesInRange(positions, newScope,
                    preliminaryAnaphora)).isInstanceOf(NullPointerException.class)
                            .hasMessage("The given positions contain no position with node type Anaphor");
        });
    }

    @Test
    public void missingPositionAfterEndOfAnaphorYieldsException() throws CoreException {
        assertAnaphoraUpdate((newScope, preliminaryAnaphora) -> {
            final ASTNode oldRelatedExpression = preliminaryAnaphora.getRelatedExpression().getRelatedExpression();
            final Expression oldAnaphor = preliminaryAnaphora.getAnaphorExpression();
            final AnaphoraPositionBeforeStartOfASTNode beforeStartOfRelatedExpression = new AnaphoraPositionBeforeStartOfASTNode(
                    RelatedExpression, oldRelatedExpression, 28, 0);
            final List<AbstractAnaphoraPositionAroundASTNode> positions = asList(beforeStartOfRelatedExpression,
                    new AnaphoraPositionAfterEndOfASTNode(RelatedExpression, oldRelatedExpression,
                            beforeStartOfRelatedExpression, 47, 0),
                    new AnaphoraPositionBeforeStartOfASTNode(Anaphor, oldAnaphor, 66, 0));

            assertThatThrownBy(() -> AstBasedAnaphoraUpdating.updateAnaphoraWithNodesInRange(positions, newScope,
                    preliminaryAnaphora)).isInstanceOf(NullPointerException.class)
                            .hasMessage("The given positions contain no position with node type Anaphor");
        });
    }

    @Test
    @Ignore // See #292
    public void newAnaphoraRelationHasNodesFromGivenRangesFromNewAST() throws CoreException {
        assertAnaphoraUpdate((newScope, preliminaryAnaphora) -> {
            final ASTNode oldRelatedExpression = preliminaryAnaphora.getRelatedExpression().getRelatedExpression();
            final Expression oldAnaphor = preliminaryAnaphora.getAnaphorExpression();
            final AnaphoraPositionBeforeStartOfASTNode beforeStartOfRelatedExpression = new AnaphoraPositionBeforeStartOfASTNode(
                    RelatedExpression, oldRelatedExpression, 28, 0);
            final AnaphoraPositionBeforeStartOfASTNode beforeStartOfAnaphor = new AnaphoraPositionBeforeStartOfASTNode(
                    Anaphor, oldAnaphor, 66, 0);
            final List<AbstractAnaphoraPositionAroundASTNode> positions = asList(beforeStartOfRelatedExpression,
                    new AnaphoraPositionAfterEndOfASTNode(RelatedExpression, oldRelatedExpression,
                            beforeStartOfRelatedExpression, 47, 0),
                    beforeStartOfAnaphor,
                    new AnaphoraPositionAfterEndOfASTNode(Anaphor, oldAnaphor, beforeStartOfAnaphor, 75, 0));

            final PublicAnaphora updatedAnaphora = AstBasedAnaphoraUpdating.updateAnaphoraWithNodesInRange(positions,
                    newScope, preliminaryAnaphora);
            assertUpdatedAnaphora(updatedAnaphora, 28, 19, "\"relatedExpression\"", 66, 9, "\"anaphor\"");
        });
    }

    @Test
    @Ignore // See #292
    public void newAnaphoraRelationHasNodesFromGivenRangesFromNewModifiedAST() throws CoreException {
        assertAnaphoraUpdate("Foo", "class Foo { String relExp = \"relExpr\"; String anaphor = \"aNewAnaphor\"; }", 28,
                9, "\"anaphor\"", 56, 13, (newScope, preliminaryAnaphora) -> {
                    final ASTNode oldRelatedExpression = preliminaryAnaphora.getRelatedExpression()
                            .getRelatedExpression();
                    final Expression oldAnaphor = preliminaryAnaphora.getAnaphorExpression();
                    final AnaphoraPositionBeforeStartOfASTNode beforeStartOfRelatedExpression = new AnaphoraPositionBeforeStartOfASTNode(
                            RelatedExpression, oldRelatedExpression, 28, 0);
                    final AnaphoraPositionBeforeStartOfASTNode beforeStartOfAnaphor = new AnaphoraPositionBeforeStartOfASTNode(
                            Anaphor, oldAnaphor, 56, 0);
                    final List<AbstractAnaphoraPositionAroundASTNode> positionsAfterUpdate = asList(
                            beforeStartOfRelatedExpression,
                            new AnaphoraPositionAfterEndOfASTNode(RelatedExpression, oldRelatedExpression,
                                    beforeStartOfRelatedExpression, 37, 0),
                            beforeStartOfAnaphor,
                            new AnaphoraPositionAfterEndOfASTNode(Anaphor, oldAnaphor, beforeStartOfAnaphor, 69, 0));

                    final PublicAnaphora updatedAnaphora = AstBasedAnaphoraUpdating
                            .updateAnaphoraWithNodesInRange(positionsAfterUpdate, newScope, preliminaryAnaphora);
                    assertUpdatedAnaphora(updatedAnaphora, 28, 9, "\"relExpr\"", 56, 13, "\"aNewAnaphor\"");
                });
    }

    @Test
    public void additionalSpaceBeforeOrAfterTheAstNodesYieldsException() throws CoreException {
        assertAnaphoraUpdate("Foo",
                "class Foo { String relExp =  \"relatedExpression\"; String anaphor = \"anaphor\" ; }", 29, 19,
                "\"anaphor\"", 67, 9, (newScope, preliminaryAnaphora) -> {
                    final ASTNode oldRelatedExpression = preliminaryAnaphora.getRelatedExpression()
                            .getRelatedExpression();
                    final Expression oldAnaphor = preliminaryAnaphora.getAnaphorExpression();
                    final AnaphoraPositionBeforeStartOfASTNode beforeStartOfRelatedExpression = new AnaphoraPositionBeforeStartOfASTNode(
                            RelatedExpression, oldRelatedExpression, 28, 0);
                    final AnaphoraPositionBeforeStartOfASTNode beforeStartOfAnaphor = new AnaphoraPositionBeforeStartOfASTNode(
                            Anaphor, oldAnaphor, 67, 0);
                    final List<AbstractAnaphoraPositionAroundASTNode> positions = asList(beforeStartOfRelatedExpression,
                            new AnaphoraPositionAfterEndOfASTNode(RelatedExpression, oldRelatedExpression,
                                    beforeStartOfRelatedExpression, 48, 0),
                            beforeStartOfAnaphor,
                            new AnaphoraPositionAfterEndOfASTNode(Anaphor, oldAnaphor, beforeStartOfAnaphor, 77, 0));

                    assertThatThrownBy(() -> AstBasedAnaphoraUpdating.updateAnaphoraWithNodesInRange(positions,
                            newScope, preliminaryAnaphora)).isInstanceOf(NullPointerException.class).hasMessage(
                                    "Could not find a node with startPosition=28, length=20 in the given scope");
                });

    }

}
