package de.monochromata.eclipse.anaphors.editor.simple;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;

public class CIC_IA2Mg_Rt_SimpleTest extends AbstractSimpleEditorTest {

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
        builder.addLine("");
        builder.addLine("public class CIC_IA2Mg_Rt_Simple {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		final A a = new A();");
        builder.addLine("		System.err.println(a.getValue());");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("	private static class A {");
        builder.addLine("		public Integer getValue() {");
        builder.addLine("			return 10;");
        builder.addLine("		}");
        builder.addLine("	}");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
        builder.addLine("");
        builder.addLine("public class CIC_IA2Mg_Rt_Simple {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		new A();");
        builder.addLine("		System.err.println(value);");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("	private static class A {");
        builder.addLine("		public Integer getValue() {");
        builder.addLine("			return 10;");
        builder.addLine("		}");
        builder.addLine("	}");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected Pair<Anaphora, Anaphora> getExpectedResolvedAndRealizedAnaphora() {
        return new ImmutablePair<>(new Anaphora("CIC", "IA2Mg", "Rt", "integer"),
                new Anaphora("LVD", "IA2Mg", "Rn", "value"));
    }

}
