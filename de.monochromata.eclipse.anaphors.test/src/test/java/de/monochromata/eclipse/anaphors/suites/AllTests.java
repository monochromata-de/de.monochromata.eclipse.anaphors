package de.monochromata.eclipse.anaphors.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import de.monochromata.eclipse.anaphors.ast.reference.strategy.concept.HyponymyIntegrationTest;
import de.monochromata.eclipse.anaphors.editor.AmbiguityAnnotationHandlingTest;
import de.monochromata.eclipse.anaphors.editor.AnaphoraDocumentProviderTest;
import de.monochromata.eclipse.anaphors.editor.AnaphoraPartSelectionUpdaterTest;
import de.monochromata.eclipse.anaphors.editor.AnaphoraQueryingTest;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditorTest;
import de.monochromata.eclipse.anaphors.editor.SelectionRelativeToAnaphoraPartsTest;
import de.monochromata.eclipse.anaphors.editor.interactive.CaptureScreenshotOnErrorOrFailureTest;
import de.monochromata.eclipse.anaphors.editor.interactive.EnterCIC_DA1Re_HyFxTest;
import de.monochromata.eclipse.anaphors.editor.interactive.EnterCIC_DA1Re_HyFx_withAmbiguityAfterEditTest;
import de.monochromata.eclipse.anaphors.editor.interactive.EnterCIC_DA1Re_HyFx_withAmbiguityIntroducedByEditTest;
import de.monochromata.eclipse.anaphors.editor.interactive.EnterCIC_DA1Re_HyFx_withAmbiguityTest;
import de.monochromata.eclipse.anaphors.editor.interactive.EnterCIC_DA1Re_HyFx_withAnaphorRemovedLaterTest;
import de.monochromata.eclipse.anaphors.editor.interactive.EnterCIC_DA1Re_HyFx_withRelatedExpressionAndAnaphorRemovedLaterTest;
import de.monochromata.eclipse.anaphors.editor.interactive.EnterCIC_DA1Re_HyFx_withRelatedExpressionRemovedLaterTest;
import de.monochromata.eclipse.anaphors.editor.interactive.EnterCIC_DA1Re_RfRtTest;
import de.monochromata.eclipse.anaphors.editor.interactive.EnterCIC_DA1Re_RfRt_withAmbiguityResolvedAfterEditTest;
import de.monochromata.eclipse.anaphors.editor.interactive.EnterCIC_DA1Re_Rt_withAmbiguityRetainedAfterEditTest;
import de.monochromata.eclipse.anaphors.editor.interactive.EnterLVD_DA1Re_RnTest;
import de.monochromata.eclipse.anaphors.editor.interactive.EnterLVD_IA2Mg_RnTest;
import de.monochromata.eclipse.anaphors.editor.interactive.EnterLVD_IA2Mg_Rn_withAndEditThatRemovesTheAnaphoraTest;
import de.monochromata.eclipse.anaphors.editor.interactive.EnterPD_DA1Re_RnTest;
import de.monochromata.eclipse.anaphors.editor.interactive.LVD_IA2F_Rn_UpdatedTo_CIC_DA1Re_RtTest;
import de.monochromata.eclipse.anaphors.editor.interactive.LVD_IA2F_Rn_UpdatedTo_LVD_IA2Mg_RnTest;
import de.monochromata.eclipse.anaphors.editor.interactive.LVD_IA2F_Rn_UpdatedTo_MI_IA1Mr_RtTest;
import de.monochromata.eclipse.anaphors.editor.interactive.LVD_IA2F_Rn_UpdatedTo_Other_LVD_IA2F_RnTest;
import de.monochromata.eclipse.anaphors.editor.interactive.LVD_IA2Mg_Rn_UpdatedTo_CIC_DA1Re_RtTest;
import de.monochromata.eclipse.anaphors.editor.interactive.LVD_IA2Mg_Rn_UpdatedTo_LVD_IA2F_RnTest;
import de.monochromata.eclipse.anaphors.editor.interactive.LVD_IA2Mg_Rn_UpdatedTo_MI_IA1Mr_RtTest;
import de.monochromata.eclipse.anaphors.editor.interactive.LVD_IA2Mg_Rn_UpdatedTo_Other_LVD_IA2Mg_RnTest;
import de.monochromata.eclipse.anaphors.editor.interactive.PD_IA2F_Rn_UpdatedTo_CIC_DA1Re_RtTest;
import de.monochromata.eclipse.anaphors.editor.interactive.PD_IA2F_Rn_UpdatedTo_MI_IA1Mr_RtTest;
import de.monochromata.eclipse.anaphors.editor.interactive.PD_IA2F_Rn_UpdatedTo_PD_IA2Mg_RnTest;
import de.monochromata.eclipse.anaphors.editor.interactive.PD_IA2Mg_Rn_UpdatedTo_CIC_DA1Re_RtTest;
import de.monochromata.eclipse.anaphors.editor.interactive.PD_IA2Mg_Rn_UpdatedTo_MI_IA1Mr_RtTest;
import de.monochromata.eclipse.anaphors.editor.interactive.PD_IA2Mg_Rn_UpdatedTo_PD_IA2F_RnTest;
import de.monochromata.eclipse.anaphors.editor.interactive.feature.AnaphorSelectionCorrectionDA1ReTest;
import de.monochromata.eclipse.anaphors.editor.interactive.feature.AnaphorSelectionCorrectionIA2MgTest;
import de.monochromata.eclipse.anaphors.editor.interactive.feature.LineEndTest;
import de.monochromata.eclipse.anaphors.editor.interactive.feature.MemberUpdateDoesNotYieldExceptionTest;
import de.monochromata.eclipse.anaphors.editor.interactive.feature.MemberUpdateWithoutSaveDoesNotYieldExceptionTest;
import de.monochromata.eclipse.anaphors.editor.interactive.feature.RemoveAmbiguityMarkerWhenAnaphorIsReplacedByLocalVariableReferenceTest;
import de.monochromata.eclipse.anaphors.editor.interactive.feature.occurrences.MarkAnaphorOccurrencesTest;
import de.monochromata.eclipse.anaphors.editor.interactive.feature.occurrences.MarkNonAnaphorOccurrencesTest;
import de.monochromata.eclipse.anaphors.editor.interactive.feature.perspectivation.SwitchUnderspecifyEverythingToUnderspecifyNothingTest;
import de.monochromata.eclipse.anaphors.editor.interactive.feature.perspectivation.SwitchUnderspecifyEverythingToUnderspecifyRelatedExpressionsTest;
import de.monochromata.eclipse.anaphors.editor.interactive.feature.perspectivation.SwitchUnderspecifyNothingToUnderspecifyEverythingTest;
import de.monochromata.eclipse.anaphors.editor.interactive.feature.perspectivation.SwitchUnderspecifyNothingToUnderspecifyRelatedExpressionsTest;
import de.monochromata.eclipse.anaphors.editor.interactive.feature.perspectivation.SwitchUnderspecifyRelatedExpressionsToUnderspecifyEverythingTest;
import de.monochromata.eclipse.anaphors.editor.interactive.feature.perspectivation.SwitchUnderspecifyRelatedExpressionsToUnderspecifyNothingTest;
import de.monochromata.eclipse.anaphors.editor.interactive.feature.resolutionoccurrences.MarkOccurrencesForAddedAnaphorTest;
import de.monochromata.eclipse.anaphors.editor.interactive.feature.resolutionoccurrences.MarkOccurrencesForUpdatedAnaphorTest;
import de.monochromata.eclipse.anaphors.editor.interactive.feature.resolutionoccurrences.TurnOffResolutionOccurrencesTest;
import de.monochromata.eclipse.anaphors.editor.occurrences.AnaphoraOccurrencesFinderTest;
import de.monochromata.eclipse.anaphors.editor.occurrences.RemoveResolutionOccurrencesJobTest;
import de.monochromata.eclipse.anaphors.editor.occurrences.ResolutionOccurrencesUpdatingTest;
import de.monochromata.eclipse.anaphors.editor.simple.CIC_DA1Re_HyFx_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.CIC_DA1Re_HyFx_UnderspecifyNothing_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.CIC_DA1Re_HyFx_UnderspecifyRelatedExpressions_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.CIC_DA1Re_Hy_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.CIC_DA1Re_RfRt_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.CIC_DA1Re_Rt_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.CIC_IA2F_RfRt_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.CIC_IA2F_Rn_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.CIC_IA2F_Rt_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.CIC_IA2F_Rt_UnderspecifyNothing_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.CIC_IA2F_Rt_UnderspecifyRelatedExpressions_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.CIC_IA2Mg_RfRt_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.CIC_IA2Mg_Rn_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.CIC_IA2Mg_Rn_UnderspecifyNothing_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.CIC_IA2Mg_Rn_UnderspecifyRelatedExpressions_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.CIC_IA2Mg_Rt_GettersHaveNoParameters_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.CIC_IA2Mg_Rt_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.LVD_DA1Re_HyFx_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.LVD_DA1Re_RfRt_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.LVD_DA1Re_Rn_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.LVD_DA1Re_Rt_MIAlreadyAssignedToLocalVariable_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.LVD_DA1Re_Rt_MIQualifiedWithExpressionAndAlreadyAssignedToLocalVariable_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.LVD_DA1Re_Rt_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.LVD_IA2F_RfRt_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.LVD_IA2F_Rn_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.LVD_IA2F_Rt_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.LVD_IA2Mg_RfRt_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.LVD_IA2Mg_Rn_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.LVD_IA2Mg_Rt_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.MI_IA1Mr_HyFx_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.MI_IA1Mr_HyFx_WithGenericType_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.MI_IA1Mr_RfRt_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.MI_IA1Mr_Rn_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.MI_IA1Mr_Rn_WithNonMatchingArguments_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.MI_IA1Mr_Rt_QualifiedWithExpression_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.MI_IA1Mr_Rt_QualifiedWithTypeName_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.MI_IA1Mr_Rt_QualifiedWithUnresolvedMethodIsNotResolved_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.MI_IA1Mr_Rt_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.MI_IA1Mr_Rt_UnderspecifyNothing_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.MI_IA1Mr_Rt_UnderspecifyRelatedExpressions_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.PD_DA1Re_HyFx_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.PD_DA1Re_Rn_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.PD_DA1Re_Rt_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.PD_IA2F_Rn_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.PD_IA2F_Rt_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.PD_IA2Mg_Rn_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.PD_IA2Mg_Rt_SimpleTest;
import de.monochromata.eclipse.anaphors.editor.simple.WarmupBeforeTheFirstTest;
import de.monochromata.eclipse.anaphors.editor.simple.feature.AddImportForNewNonLocalTypeTest;
import de.monochromata.eclipse.anaphors.editor.simple.feature.AmbiguousAnaphorShouldNotBlockResolutionOfOtherAnaphorsTest;
import de.monochromata.eclipse.anaphors.editor.simple.feature.IgnorePrivateFieldForIA2FTest;
import de.monochromata.eclipse.anaphors.editor.simple.feature.IgnorePrivateGetterForIA2MgTest;
import de.monochromata.eclipse.anaphors.editor.simple.feature.MultipleIndependentProblemsInDifferentMethodsTest;
import de.monochromata.eclipse.anaphors.editor.simple.feature.MultipleIndependentProblemsInSameMethodTest;
import de.monochromata.eclipse.anaphors.editor.simple.feature.MultipleInterdependentProblems2xDA1ReTest;
import de.monochromata.eclipse.anaphors.editor.simple.feature.MultipleInterdependentProblems_DA1Re_IA2MgTest;
import de.monochromata.eclipse.anaphors.editor.simple.feature.ShortestAnaphorIsChosenFor2xDA1ReTest;
import de.monochromata.eclipse.anaphors.editor.simple.feature.java10.VarDisabledForJava10ProjectTest;
import de.monochromata.eclipse.anaphors.editor.simple.feature.java10.VarEnabledForJava10ProjectTest;
import de.monochromata.eclipse.anaphors.editor.simple.feature.java10.VarEnabledForJava8ProjectTest;
import de.monochromata.eclipse.anaphors.editor.update.AnaphoraComparingTest;
import de.monochromata.eclipse.anaphors.editor.update.AnaphoraReresolvingDocumentListenerTest;
import de.monochromata.eclipse.anaphors.editor.update.AnaphoraResolvingElementListenerTest;
import de.monochromata.eclipse.anaphors.editor.update.AnaphoraUpdatingTest;
import de.monochromata.eclipse.anaphors.editor.update.AstBasedAnaphoraUpdatingTest;
import de.monochromata.eclipse.anaphors.editor.update.MemberCollectionTest;
import de.monochromata.eclipse.anaphors.editor.update.TriggeringPositionNormalizationTest;
import de.monochromata.eclipse.anaphors.editor.update.UnderspecificationTest;
import de.monochromata.eclipse.anaphors.editor.update.job.ResolveAnaphorsJobCreationTest;
import de.monochromata.eclipse.anaphors.position.AnaphoraPositionCreationTest;
import de.monochromata.eclipse.anaphors.position.AnaphoraPositionUpdaterTest;
import de.monochromata.eclipse.anaphors.position.ExistingPerspectivationTest;
import de.monochromata.eclipse.anaphors.preferences.PreferencePageTest;
import de.monochromata.eclipse.anaphors.swt.DefaultASTNodeTrackerTest;
import de.monochromata.eclipse.anaphors.swt.DocumentOffsetDeterminationTest;

/**
 * All tests of {@link CognitiveEditor}.
 * <p>
 * Needs to be run as a JUnit Plug-in Test, but not in the UI thread.
 */
@RunWith(Suite.class)
@SuiteClasses({
        // Unit tests
        AmbiguityAnnotationHandlingTest.class, AnaphoraComparingTest.class, AnaphoraDocumentProviderTest.class,
        CognitiveEditorTest.class, AnaphoraOccurrencesFinderTest.class, AnaphoraPositionCreationTest.class,
        AnaphoraPositionUpdaterTest.class, AnaphoraPartSelectionUpdaterTest.class, AnaphoraQueryingTest.class,
        AnaphoraUpdatingTest.class, AstBasedAnaphoraUpdatingTest.class, DefaultASTNodeTrackerTest.class,
        DocumentOffsetDeterminationTest.class, ExistingPerspectivationTest.class, MemberCollectionTest.class,
        AnaphoraReresolvingDocumentListenerTest.class, AnaphoraResolvingElementListenerTest.class,
        PreferencePageTest.class, RemoveResolutionOccurrencesJobTest.class, ResolutionOccurrencesUpdatingTest.class,
        ResolveAnaphorsJobCreationTest.class, SelectionRelativeToAnaphoraPartsTest.class, UnderspecificationTest.class,
        TriggeringPositionNormalizationTest.class,

        // Unit tests for the test infrastructure
        CaptureScreenshotOnErrorOrFailureTest.class,

        // Integration tests for de.monochromata.anaphors
        HyponymyIntegrationTest.class,

        // Warm up so the tests run in time
        WarmupBeforeTheFirstTest.class,

        // Simple tests

        // -- anaphors related to class instance creation expressions
        /*
         * Not a sensible combination, because CIC doesn't have a name:
         * CIC_DA1Re_Rn_SimpleTest.class,
         */
        CIC_DA1Re_RfRt_SimpleTest.class, CIC_DA1Re_Rt_SimpleTest.class, CIC_DA1Re_Hy_SimpleTest.class,
        CIC_DA1Re_HyFx_SimpleTest.class, CIC_DA1Re_HyFx_UnderspecifyNothing_SimpleTest.class,
        CIC_DA1Re_HyFx_UnderspecifyRelatedExpressions_SimpleTest.class,

        CIC_IA2F_RfRt_SimpleTest.class, CIC_IA2F_Rn_SimpleTest.class, CIC_IA2F_Rt_SimpleTest.class,
        CIC_IA2F_Rt_UnderspecifyNothing_SimpleTest.class, CIC_IA2F_Rt_UnderspecifyRelatedExpressions_SimpleTest.class,
        CIC_IA2Mg_RfRt_SimpleTest.class, CIC_IA2Mg_Rn_SimpleTest.class,
        CIC_IA2Mg_Rn_UnderspecifyNothing_SimpleTest.class, CIC_IA2Mg_Rn_UnderspecifyRelatedExpressions_SimpleTest.class,
        CIC_IA2Mg_Rt_SimpleTest.class, CIC_IA2Mg_Rt_GettersHaveNoParameters_SimpleTest.class,

        // -- anaphors related to parameter- and local variable declarations
        PD_DA1Re_Rn_SimpleTest.class, PD_DA1Re_Rt_SimpleTest.class, PD_DA1Re_HyFx_SimpleTest.class,
        PD_IA2F_Rn_SimpleTest.class, PD_IA2F_Rt_SimpleTest.class, PD_IA2Mg_Rn_SimpleTest.class,
        PD_IA2Mg_Rt_SimpleTest.class, LVD_DA1Re_RfRt_SimpleTest.class, LVD_DA1Re_Rn_SimpleTest.class,
        LVD_DA1Re_Rt_SimpleTest.class, LVD_DA1Re_HyFx_SimpleTest.class, LVD_IA2F_RfRt_SimpleTest.class,
        LVD_IA2F_Rn_SimpleTest.class, LVD_IA2F_Rt_SimpleTest.class, LVD_IA2Mg_RfRt_SimpleTest.class,
        LVD_IA2Mg_Rn_SimpleTest.class, LVD_IA2Mg_Rt_SimpleTest.class,

        // -- anaphors related to method invocation expressions
        MI_IA1Mr_RfRt_SimpleTest.class, MI_IA1Mr_Rn_SimpleTest.class,
        MI_IA1Mr_Rn_WithNonMatchingArguments_SimpleTest.class,
        LVD_DA1Re_Rt_MIAlreadyAssignedToLocalVariable_SimpleTest.class,
        // MI_IA1Mr_Rt_AnchoringInQualifierNotInRightmostInvocation_SimpleTest.class,
        MI_IA1Mr_Rt_QualifiedWithExpression_SimpleTest.class,
        LVD_DA1Re_Rt_MIQualifiedWithExpressionAndAlreadyAssignedToLocalVariable_SimpleTest.class,
        MI_IA1Mr_Rt_QualifiedWithTypeName_SimpleTest.class,
        MI_IA1Mr_Rt_QualifiedWithUnresolvedMethodIsNotResolved_SimpleTest.class, MI_IA1Mr_HyFx_SimpleTest.class,
        MI_IA1Mr_HyFx_WithGenericType_SimpleTest.class, MI_IA1Mr_Rt_SimpleTest.class,
        MI_IA1Mr_Rt_UnderspecifyNothing_SimpleTest.class, MI_IA1Mr_Rt_UnderspecifyRelatedExpressions_SimpleTest.class,

        // Simple tests of features
        AddImportForNewNonLocalTypeTest.class, AmbiguousAnaphorShouldNotBlockResolutionOfOtherAnaphorsTest.class,
        IgnorePrivateFieldForIA2FTest.class, IgnorePrivateGetterForIA2MgTest.class,
        MultipleIndependentProblemsInDifferentMethodsTest.class, MultipleIndependentProblemsInSameMethodTest.class,
        MultipleInterdependentProblems2xDA1ReTest.class, MultipleInterdependentProblems_DA1Re_IA2MgTest.class,
        ShortestAnaphorIsChosenFor2xDA1ReTest.class,

        VarDisabledForJava10ProjectTest.class, VarEnabledForJava10ProjectTest.class,
        VarEnabledForJava8ProjectTest.class,

        // Interactive tests
        EnterCIC_DA1Re_RfRt_withAmbiguityResolvedAfterEditTest.class,
        EnterCIC_DA1Re_Rt_withAmbiguityRetainedAfterEditTest.class,
        EnterCIC_DA1Re_HyFx_withAmbiguityAfterEditTest.class,
        EnterCIC_DA1Re_HyFx_withAmbiguityIntroducedByEditTest.class, EnterCIC_DA1Re_HyFx_withAmbiguityTest.class,
        EnterCIC_DA1Re_HyFx_withAnaphorRemovedLaterTest.class,
        EnterCIC_DA1Re_HyFx_withRelatedExpressionAndAnaphorRemovedLaterTest.class,
        EnterCIC_DA1Re_HyFx_withRelatedExpressionRemovedLaterTest.class, EnterCIC_DA1Re_RfRtTest.class,
        EnterLVD_DA1Re_RnTest.class, EnterLVD_IA2Mg_RnTest.class,
        EnterLVD_IA2Mg_Rn_withAndEditThatRemovesTheAnaphoraTest.class, EnterCIC_DA1Re_HyFxTest.class,
        EnterPD_DA1Re_RnTest.class, LVD_IA2F_Rn_UpdatedTo_CIC_DA1Re_RtTest.class,
        LVD_IA2F_Rn_UpdatedTo_LVD_IA2Mg_RnTest.class, LVD_IA2F_Rn_UpdatedTo_MI_IA1Mr_RtTest.class,
        LVD_IA2F_Rn_UpdatedTo_Other_LVD_IA2F_RnTest.class, LVD_IA2Mg_Rn_UpdatedTo_CIC_DA1Re_RtTest.class,
        LVD_IA2Mg_Rn_UpdatedTo_LVD_IA2F_RnTest.class, LVD_IA2Mg_Rn_UpdatedTo_MI_IA1Mr_RtTest.class,
        LVD_IA2Mg_Rn_UpdatedTo_Other_LVD_IA2Mg_RnTest.class, PD_IA2F_Rn_UpdatedTo_CIC_DA1Re_RtTest.class,
        PD_IA2F_Rn_UpdatedTo_MI_IA1Mr_RtTest.class, PD_IA2F_Rn_UpdatedTo_PD_IA2Mg_RnTest.class,
        PD_IA2Mg_Rn_UpdatedTo_CIC_DA1Re_RtTest.class, PD_IA2Mg_Rn_UpdatedTo_MI_IA1Mr_RtTest.class,
        PD_IA2Mg_Rn_UpdatedTo_PD_IA2F_RnTest.class,

        // Interactive tests of features
        AnaphorSelectionCorrectionDA1ReTest.class, AnaphorSelectionCorrectionIA2MgTest.class, LineEndTest.class,
        MemberUpdateDoesNotYieldExceptionTest.class, MemberUpdateWithoutSaveDoesNotYieldExceptionTest.class,
        RemoveAmbiguityMarkerWhenAnaphorIsReplacedByLocalVariableReferenceTest.class,

        MarkAnaphorOccurrencesTest.class, MarkNonAnaphorOccurrencesTest.class,

        SwitchUnderspecifyEverythingToUnderspecifyNothingTest.class,
        SwitchUnderspecifyEverythingToUnderspecifyRelatedExpressionsTest.class,
        SwitchUnderspecifyNothingToUnderspecifyEverythingTest.class,
        SwitchUnderspecifyNothingToUnderspecifyRelatedExpressionsTest.class,
        SwitchUnderspecifyRelatedExpressionsToUnderspecifyNothingTest.class,
        SwitchUnderspecifyRelatedExpressionsToUnderspecifyEverythingTest.class,

        MarkOccurrencesForAddedAnaphorTest.class, MarkOccurrencesForUpdatedAnaphorTest.class,
        TurnOffResolutionOccurrencesTest.class })

public class AllTests {

}
