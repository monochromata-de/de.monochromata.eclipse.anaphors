package de.monochromata.eclipse.anaphors.editor.interactive;

import static java.util.Arrays.asList;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class EnterLVD_IA2Mg_RnTest extends AbstractInteractiveEditorTest {

    @Override
    protected void editFile(final IEditorPart editor) {
        getBot().activeEditor().toTextEditor().insertText(8, 21, "lineCount");
        getBot().activeEditor().save();
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
        builder.addLine("");
        builder.addLine("import javax.swing.JTextArea;");
        builder.addLine("");
        builder.addLine("public class EnterLVD_IA2Mg_Rn {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		final JTextArea textArea = new JTextArea();");
        builder.addLine("		System.err.println(textArea.getLineCount());");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive;");
        builder.addLine("");
        builder.addLine("import javax.swing.JTextArea;");
        builder.addLine("");
        builder.addLine("public class EnterLVD_IA2Mg_Rn {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		final JTextArea textArea = new JTextArea();");
        builder.addLine("		System.err.println(lineCount);");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
        return asList(
                new ImmutablePair<>(new Annotation("org.eclipse.jdt.ui.occurrences.write", false,
                        "Related expression"), new Position(171, 43)),
                new ImmutablePair<>(
                        new Annotation("org.eclipse.jdt.ui.occurrences", false, "Anaphor co-referent to lineCount"),
                        new Position(236, 23)));
    }

    @Override
    protected List<? extends Position> getExpectedPositions() {
        final Anaphora anaphora = new Anaphora("LVD", "IA2Mg", "Rn", "lineCount");
        return asList(new PositionForAnaphor(236, 23, anaphora),
                new PositionForRelatedExpression(171, 43));
    }

}
