package de.monochromata.eclipse.anaphors.editor.interactive.feature;

import static java.util.Arrays.asList;
import static org.eclipse.ui.texteditor.ITextEditorActionDefinitionIds.LINE_END;
import static org.eclipse.ui.texteditor.ITextEditorActionDefinitionIds.LINE_START;
import static org.eclipse.ui.texteditor.ITextEditorActionDefinitionIds.SELECT_LINE_END;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.editor.interactive.AbstractInteractiveEditorTest;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class LineEndTest extends AbstractInteractiveEditorTest {

    @Override
    protected String getExpectedKindOfResolvedAnaphora() {
        return "LVD-IA2Mg-Rn";
    }

    @Override
    protected void manipulateEditorAndCheckAssertions(final ProblemCollectingChangeListener problemCollector,
            final CognitiveEditor editor) {
        super.manipulateEditorAndCheckAssertions(problemCollector, editor);
        assertLineEndActions(editor);
    }

    protected void assertLineEndActions(final CognitiveEditor editor) {
        assertSelectedRange(246, 0, editor); // origin coordinates
        runSync(() -> editor.getAction(LINE_END).run(), editor);
        assertSelectedRange(259, 0, editor); // origin coordinates including ()
        runSync(() -> editor.getAction(LINE_START).run(), editor);
        assertSelectedRange(215, 0, editor); // origin coordinates including textArea.get
        runSync(() -> editor.getAction(SELECT_LINE_END).run(), editor);
        assertSelectedRange(215, 44, editor); // origin coordinates including textArea.get and ()
    }

    @Override
    protected void editFile(final IEditorPart editor) {
        getBot().activeEditor().toTextEditor().insertText(8, 21, "lineCount");
        getBot().activeEditor().save();
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature;");
        builder.addLine("");
        builder.addLine("import javax.swing.JTextArea;");
        builder.addLine("");
        builder.addLine("public class LineEnd {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		final JTextArea textArea = new JTextArea();");
        builder.addLine("		System.err.println(textArea.getLineCount());");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature;");
        builder.addLine("");
        builder.addLine("import javax.swing.JTextArea;");
        builder.addLine("");
        builder.addLine("public class LineEnd {");
        builder.addLine("");
        builder.addLine("	public static void main(String[] args) {");
        builder.addLine("		final JTextArea textArea = new JTextArea();");
        builder.addLine("		System.err.println(lineCount);");
        builder.addLine("	}");
        builder.addLine("");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
        return asList(
                new ImmutablePair<>(new Annotation("org.eclipse.jdt.ui.occurrences.write", false,
                        "Related expression"), new Position(169, 43)),
                new ImmutablePair<>(
                        new Annotation("org.eclipse.jdt.ui.occurrences", false, "Anaphor co-referent to lineCount"),
                        new Position(234, 23)));
    }

    @Override
    protected List<? extends Position> getExpectedPositions() {
        final Anaphora anaphora = new Anaphora("LVD", "IA2Mg", "Rn", "lineCount");
        return asList(new PositionForAnaphor(234, 23, anaphora),
                new PositionForRelatedExpression(169, 43));
    }

}
