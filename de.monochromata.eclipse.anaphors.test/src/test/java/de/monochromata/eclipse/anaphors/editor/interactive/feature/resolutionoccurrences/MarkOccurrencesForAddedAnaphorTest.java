package de.monochromata.eclipse.anaphors.editor.interactive.feature.resolutionoccurrences;

import static java.util.Arrays.asList;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ui.IEditorPart;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.interactive.AbstractInteractiveEditorTest;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class MarkOccurrencesForAddedAnaphorTest extends AbstractInteractiveEditorTest {

    @Override
    protected String getExpectedKindOfResolvedAnaphora() {
        return "CIC-IA2F-Rt";
    }

    @Override
    protected void editFile(final IEditorPart editor) {
        // Just resolve the anaphors initially
    }

    @Override
    public String getExpectedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature.resolutionoccurrences;");
        builder.addLine("");
        builder.addLine("public class MarkOccurrencesForAddedAnaphor {");
        builder.addLine("");
        builder.addLine("   public static void main(String[] args) {");
        builder.addLine("       final A a = new A();");
        builder.addLine("       System.err.println(a.int0);");
        builder.addLine("   }");
        builder.addLine("");
        builder.addLine("   private static class A {");
        builder.addLine("       public Integer int0;");
        builder.addLine("   }");
        builder.addLine("");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    public String getExpectedUnderspecifiedEventualSourceCode() {
        final CodeBuilder builder = new CodeBuilder();
        builder.addLine("package de.monochromata.eclipse.anaphors.editor.interactive.feature.resolutionoccurrences;");
        builder.addLine("");
        builder.addLine("public class MarkOccurrencesForAddedAnaphor {");
        builder.addLine("");
        builder.addLine("   public static void main(String[] args) {");
        builder.addLine("       new A();");
        builder.addLine("       System.err.println(int0);");
        builder.addLine("   }");
        builder.addLine("");
        builder.addLine("   private static class A {");
        builder.addLine("       public Integer int0;");
        builder.addLine("   }");
        builder.addLine("");
        builder.addLine("}");
        return builder.toString();
    }

    @Override
    protected List<Pair<? extends Annotation, ? extends Position>> getExpectedAnnotations() {
        return asList(
                new ImmutablePair<>(new Annotation("org.eclipse.jdt.ui.occurrences.write", false,
                        "Related expression"), new Position(183, 20)),
                new ImmutablePair<>(
                        new Annotation("org.eclipse.jdt.ui.occurrences", false, "Anaphor co-referent to int0"),
                        new Position(225, 6)));
    }

    @Override
    protected List<? extends Position> getExpectedPositions() {
        final Anaphora anaphora = new Anaphora("LVD", "IA2F", "Rn", "int0");
        return asList(new PositionForAnaphor(225, 6, anaphora),
                new PositionForRelatedExpression(183, 20));
    }
}
