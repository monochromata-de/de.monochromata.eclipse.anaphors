package de.monochromata.eclipse.anaphors.editor;

import java.lang.reflect.Field;

import de.monochromata.eclipse.anaphors.editor.CognitiveEditor.ErrorTickUpdater;

public interface CognitiveEditorTesting {

	default ErrorTickUpdater getErrorTickUpdater(final CognitiveEditor editor) throws Exception {
		final Field field = CognitiveEditor.class.getDeclaredField("fErrorTickUpdater");
		field.setAccessible(true);
		return (ErrorTickUpdater) field.get(editor);
	}

}
