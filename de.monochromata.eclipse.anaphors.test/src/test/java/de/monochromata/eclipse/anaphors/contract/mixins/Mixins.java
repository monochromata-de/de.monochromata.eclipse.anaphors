package de.monochromata.eclipse.anaphors.contract.mixins;

import java.util.List;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ChildListPropertyDescriptor;
import org.eclipse.jdt.core.dom.ChildPropertyDescriptor;
import org.eclipse.jdt.core.dom.rewrite.ListRewrite;
import org.eclipse.jdt.internal.core.dom.rewrite.RewriteEventStore;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Mixins {

    private static final List<Class<?>> typesToSerializeAsEmptyObject = List.of(AST.class,
            ChildPropertyDescriptor.class, ChildListPropertyDescriptor.class, ListRewrite.class,
            RewriteEventStore.class);

    public static void addMixins(final ObjectMapper objectMapper) {
        typesToSerializeAsEmptyObject.forEach(type -> objectMapper.addMixIn(type, ToEmptyObjectMixin.class));
    }

}
