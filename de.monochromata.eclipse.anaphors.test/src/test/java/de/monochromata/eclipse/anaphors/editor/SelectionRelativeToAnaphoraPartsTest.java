package de.monochromata.eclipse.anaphors.editor;

import static de.monochromata.eclipse.anaphors.editor.SelectionRelativeToAnaphoraPart.selectByClosestPosition;
import static de.monochromata.eclipse.anaphors.editor.SelectionRelativeToAnaphoraPart.Relation.AtBeginningOfAnaphoraPart;
import static de.monochromata.eclipse.anaphors.editor.SelectionRelativeToAnaphoraPart.Relation.AtEndOfAnaphoraPart;
import static de.monochromata.eclipse.anaphors.editor.SelectionRelativeToAnaphoraPart.Relation.InsideAnaphoraPart;
import static de.monochromata.eclipse.anaphors.editor.SelectionRelativeToAnaphoraPart.Relation.OutsideAnaphoraPart;
import static de.monochromata.eclipse.anaphors.editor.SelectionRelativeToAnaphoraPart.Relation.SelectionHasNonZeroLength;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.junit.Test;

public class SelectionRelativeToAnaphoraPartsTest implements SelectionRelativeToAnaphoraPartsTesting {

    // Cases not covering any of the two anaphora parts

    @Test
    public void createRelativeSelectionForNonEmptySelection() {
        assertSelectionRelativeToDefiniteExpression(12, 5, 10, 10, SelectionHasNonZeroLength, 2);
    }

    @Test
    public void createRelativeSelectionForSelectionBeforeAnaphoraPart() {
        assertSelectionRelativeToDefiniteExpression(10, 0, 20, 10, OutsideAnaphoraPart, -10);
    }

    @Test
    public void createRelativeSelectionForSelectionAfterEndOfAnaphoraPart() {
        assertSelectionRelativeToDefiniteExpression(21, 0, 10, 10, OutsideAnaphoraPart, 11);
    }

    // Selections covering the anaphora part position

    @Test
    public void createRelativeSelectionForSelectionAtAnaphoraPartOffset() {
        assertSelectionRelativeToDefiniteExpression(10, 0, 10, 10, AtBeginningOfAnaphoraPart, 0);
    }

    @Test
    public void createRelativeSelectionForSelectionInsideAnaphoraPart() {
        assertSelectionRelativeToDefiniteExpression(13, 0, 10, 10, InsideAnaphoraPart, 3);
    }

    @Test
    public void createRelativeSelectionForSelectionAtEndOfAnaphoraPart() {
        assertSelectionRelativeToDefiniteExpression(20, 0, 10, 10, AtEndOfAnaphoraPart, 10);
    }

    // Determining the closest relative selection out of multiple position pairs

    @Test
    public void missingPositionPairYieldsException() {
        assertThatThrownBy(() -> selectByClosestPosition(Stream.of()))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("empty stream");
    }

    @Test
    public void simplyReturnTheSelectionIfThereIsOnlyOne() {
        final Pair<Position, SelectionRelativeToAnaphoraPart> input = new ImmutablePair<>(
                new Position(0, 1),
                new SelectionRelativeToAnaphoraPart(OutsideAnaphoraPart, 0));

        assertThat(selectByClosestPosition(Stream.of(input))).isSameAs(input);
    }

    @Test
    public void returnTheClosestOfTwoSelectionsByPositiveOffset() {
        final Pair<Position, SelectionRelativeToAnaphoraPart> closer = new ImmutablePair<>(
                new Position(0, 1),
                new SelectionRelativeToAnaphoraPart(OutsideAnaphoraPart, 0));
        final Pair<Position, SelectionRelativeToAnaphoraPart> farther = new ImmutablePair<>(
                new Position(2, 1),
                new SelectionRelativeToAnaphoraPart(OutsideAnaphoraPart, 2));

        assertThat(selectByClosestPosition(Stream.of(farther, closer))).isSameAs(closer);
    }

    @Test
    public void returnTheClosestOfTwoSelectionsByNegativeOffset() {
        final Pair<Position, SelectionRelativeToAnaphoraPart> closer = new ImmutablePair<>(
                new Position(0, 1),
                new SelectionRelativeToAnaphoraPart(OutsideAnaphoraPart, -0));
        final Pair<Position, SelectionRelativeToAnaphoraPart> farther = new ImmutablePair<>(
                new Position(2, 1),
                new SelectionRelativeToAnaphoraPart(OutsideAnaphoraPart, -2));

        assertThat(selectByClosestPosition(Stream.of(farther, closer))).isSameAs(closer);
    }

    @Test
    public void returnTheClosestOfTwoSelectionsByPositiveDistanceToOffset() {
        final Pair<Position, SelectionRelativeToAnaphoraPart> farther = new ImmutablePair<>(
                new Position(1, 1),
                new SelectionRelativeToAnaphoraPart(OutsideAnaphoraPart, -1));
        final Pair<Position, SelectionRelativeToAnaphoraPart> closer = new ImmutablePair<>(
                new Position(3, 1),
                new SelectionRelativeToAnaphoraPart(OutsideAnaphoraPart, 1));

        assertThat(selectByClosestPosition(Stream.of(farther, closer))).isSameAs(closer);
    }

    @Test
    public void returnTheFirstOfTwoEquallyCloseSelectionsAndLogAWarning() {
        final Pair<Position, SelectionRelativeToAnaphoraPart> farther = new ImmutablePair<>(
                new Position(1, 1),
                new SelectionRelativeToAnaphoraPart(OutsideAnaphoraPart, 0));
        final Pair<Position, SelectionRelativeToAnaphoraPart> closer = new ImmutablePair<>(
                new Position(1, 1),
                new SelectionRelativeToAnaphoraPart(OutsideAnaphoraPart, 0));
        final List<String> logStatements = new ArrayList<>();

        assertThat(selectByClosestPosition(Stream.of(farther, closer), logStatements::add)).isSameAs(closer);
        assertThat(logStatements).containsExactly("choosing second of two equally close inputs: "
                + "(offset: 1, length: 1,OutsideAnaphoraPart:0), "
                + "(offset: 1, length: 1,OutsideAnaphoraPart:0)");
    }
}
