package de.monochromata.eclipse.anaphors;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jface.text.Document;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy;
import de.monochromata.anaphors.ast.reference.strategy.concept.TypeRecurrence;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.ClassInstanceCreationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.anaphors.ast.spi.AnaphoraResolutionSpi;
import de.monochromata.anaphors.ast.spi.AnaphorsSpi;
import de.monochromata.anaphors.ast.spi.RelatedExpressionsSpi;
import de.monochromata.anaphors.ast.strategy.AnaphorResolutionStrategy;
import de.monochromata.anaphors.ast.strategy.DA1ReStrategy;
import de.monochromata.anaphors.preferences.Preferences;
import de.monochromata.eclipse.anaphors.position.PositionsTesting;
import de.monochromata.function.ThrowingFunction;

public interface AnaphoraTesting extends PositionsTesting {

    default Anaphora createAnaphora(final String anaphor) {
        return new Anaphora("kindOfRelatedExpression", "kindOfAnaphorResolution", "kindOfReferenzialization", anaphor);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    default <O, T extends Throwable> O withNopProviders(final ThrowingFunction<ASTSpis, O, T> delegate) throws T {
        final var anaphorsSpi = mock(AnaphorsSpi.class);
        final var relatedExpressionsSpi = mock(RelatedExpressionsSpi.class);
        final var anaphoraResolutionSpi = mock(AnaphoraResolutionSpi.class);
        final var preferencesSpi = mock(Preferences.class);
        final ASTSpis spis = new ASTSpis<>(anaphorsSpi, relatedExpressionsSpi, anaphoraResolutionSpi, preferencesSpi);
        return delegate.apply(spis);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    default ASTBasedAnaphora createAstBasedAnaphora_CIC_DA1Re_Rt(final ASTNode relatedExpressionNode,
            final String anaphor, final Expression anaphorExpression) {
        return withNopProviders(spis -> {
            final var strategy = new ClassInstanceCreationStrategy(spis.relatedExpressionsSpi, spis.preferencesSpi);
            final RelatedExpression relatedExpression = PublicRelatedExpression.createForClassInstanceCreation(
                    (ClassInstanceCreation) relatedExpressionNode, strategy, spis.relatedExpressionsSpi);
            final DA1ReStrategy anaphorResolutionStrategy = new DA1ReStrategy<>(spis.anaphorsSpi,
                    spis.relatedExpressionsSpi, spis.anaphorResolutionSpi);
            final Referent referent = anaphorResolutionStrategy.createReferent(null, relatedExpression, null);
            final ASTBasedAnaphora astBasedAnaphora = anaphorResolutionStrategy.createAnaphora(null, anaphor,
                    anaphorExpression, relatedExpression, referent, new TypeRecurrence(spis.anaphorsSpi));
            return astBasedAnaphora;
        });
    }

    default Pair<Document, PublicAnaphora> createDocumentAndAnaphora(final String documentText,
            final int relatedExpressionStart, final String relatedExpressionCode, final int anaphorStart,
            final String anaphorCode) {
        final AST ast = AST.newAST(AST.JLS8);
        final StringLiteral relatedExpression = createStringLiteral(ast, relatedExpressionStart, relatedExpressionCode);
        final StringLiteral anaphorExpression = createStringLiteral(ast, anaphorStart, anaphorCode);

        final PublicAnaphora anaphora = mockAnaphora(null, relatedExpression, anaphorExpression,
                /* re-use as new rel.expr. */ relatedExpression, /* re-use as new anaphor */ anaphorCode,
                anaphorExpression);

        final Document document = new Document(documentText);
        final Pair<Document, PublicAnaphora> documentAndAnaphora = new ImmutablePair<>(document, anaphora);
        return documentAndAnaphora;
    }

    default StringLiteral createStringLiteral(final AST ast, final int startPosition, final String literalValue) {
        final StringLiteral anaphorExpression = ast.newStringLiteral();
        anaphorExpression.setLiteralValue(literalValue);
        anaphorExpression.setSourceRange(startPosition, 2 + literalValue.length());
        return anaphorExpression;
    }

    default PublicAnaphora mockAnaphora(final CompilationUnit newScope, final ASTNode oldRelatedExpression,
            final Expression oldAnaphor, final ASTNode newRelatedExpression, final String newAnaphor,
            final Expression newAnaphorExpression) {
        final RelatedExpressionStrategy<ASTNode, Type, IBinding, ITypeBinding, CompilationUnit, String, PublicRelatedExpression> relatedExpressionStrategy = mockRelatedExpressionStrategy();
        final ReferentializationStrategy<Expression, ITypeBinding, CompilationUnit, String, String> referentializationStrategy = mockReferentializationStrategy();
        final AnaphorResolutionStrategy<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> resolutionStrategy = mockResolutionStrategy(
                newScope, newRelatedExpression, newAnaphor, newAnaphorExpression, relatedExpressionStrategy,
                referentializationStrategy);

        return mockAnaphora(mockRelatedExpression(oldRelatedExpression), oldAnaphor, resolutionStrategy,
                relatedExpressionStrategy, referentializationStrategy);
    }

    default RelatedExpressionStrategy<ASTNode, Type, IBinding, ITypeBinding, CompilationUnit, String, PublicRelatedExpression> mockRelatedExpressionStrategy() {
        final RelatedExpressionStrategy<ASTNode, Type, IBinding, ITypeBinding, CompilationUnit, String, PublicRelatedExpression> relatedExpressionStrategy = mock(
                RelatedExpressionStrategy.class);
        when(relatedExpressionStrategy.getKind()).thenReturn("kindOfRE");
        return relatedExpressionStrategy;
    }

    default ReferentializationStrategy<Expression, ITypeBinding, CompilationUnit, String, String> mockReferentializationStrategy() {
        final ReferentializationStrategy<Expression, ITypeBinding, CompilationUnit, String, String> referentializationStrategy = mock(
                ReferentializationStrategy.class);
        when(referentializationStrategy.getKind()).thenReturn("kindOfRef");
        return referentializationStrategy;
    }

    @SuppressWarnings("unchecked")
    default PublicRelatedExpression mockRelatedExpression(final ASTNode relatedExpression) {
        final RelatedExpressionStrategy mockStrategy = mock(RelatedExpressionStrategy.class);
        final PublicRelatedExpression mockRelatedExpression = new PublicRelatedExpression(false, () -> null,
                relatedExpression, mockStrategy, null, unused -> null, unused -> null, (unused1, unused2) -> null,
                unused -> null, (unused1, unused2) -> false);
        return mockRelatedExpression;
    }

    @SuppressWarnings("unchecked")
    default AnaphorResolutionStrategy<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> mockResolutionStrategy(
            final CompilationUnit newScope, final ASTNode newRelatedExpression, final String newAnaphor,
            final Expression newAnaphorExpression,
            final RelatedExpressionStrategy<ASTNode, Type, IBinding, ITypeBinding, CompilationUnit, String, PublicRelatedExpression> relatedExpressionStrategy,
            final ReferentializationStrategy<Expression, ITypeBinding, CompilationUnit, String, String> referentializationStrategy) {
        final AnaphorResolutionStrategy<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> mockResolutionStrategy = mock(
                AnaphorResolutionStrategy.class);
        when(mockResolutionStrategy.getKind()).thenReturn("kindOfAR");
        return mockResolutionStrategy;
    }

    default PublicAnaphora mockAnaphora(final PublicRelatedExpression relatedExpression, final Expression anaphor,
            final AnaphorResolutionStrategy<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> resolutionStrategy,
            final RelatedExpressionStrategy<ASTNode, Type, IBinding, ITypeBinding, CompilationUnit, String, PublicRelatedExpression> relatedExpressionStrategy,
            final ReferentializationStrategy<Expression, ITypeBinding, CompilationUnit, String, String> referentializationStrategy) {
        @SuppressWarnings("unchecked")
        final PublicAnaphora mockAnaphora = mock(PublicAnaphora.class);
        when(mockAnaphora.getRelatedExpression()).thenReturn(relatedExpression);
        when(mockAnaphora.getAnaphor()).thenReturn(anaphor.toString());
        when(mockAnaphora.getAnaphorExpression()).thenReturn(anaphor);
        when(mockAnaphora.getAnaphorResolutionStrategy()).thenReturn(resolutionStrategy);
        when(mockAnaphora.getRelatedExpressionStrategy()).thenReturn(relatedExpressionStrategy);
        when(mockAnaphora.getReferentializationStrategy()).thenReturn(referentializationStrategy);
        return mockAnaphora;
    }

}
