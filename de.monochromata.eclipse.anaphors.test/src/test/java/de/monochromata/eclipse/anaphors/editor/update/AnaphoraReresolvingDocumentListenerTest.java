package de.monochromata.eclipse.anaphors.editor.update;

import static de.monochromata.eclipse.anaphors.position.AnaphoraPositionCreationTesting.addPositionForRelatedExpression;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.dom.anaphors.JavaModelTesting;
import org.eclipse.jdt.internal.corext.refactoring.change.DocumentTesting;
import org.junit.Test;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;
import de.monochromata.eclipse.persp.PerspectivationPosition;

public class AnaphoraReresolvingDocumentListenerTest
		implements DocumentTesting, JavaModelTesting, AnaphoraReresolvingDocumentListenerTesting {

	@Test
	public void theDisabledListenerDoesNotInvalidateMembers() throws Exception {
		assertDocument("class Foo { String bar = \"baroeo\"; }", (document, compilationUnit) -> {
			final PositionForRelatedExpression positionForRelatedExpression = addPositionForRelatedExpression(document,
					new PerspectivationPosition(12, 3));
			final PositionForAnaphor positionForAnaphor = addPositionForAnaphor(document,
					new Anaphora("RE", "AR", "REF", "anaphor"), new PerspectivationPosition(19, 3));
			final AtomicReference<Pair<Integer, Integer>> offsetAndLengthFuture = new AtomicReference<>();
			final AnaphoraReresolvingDocumentListener listener = createTestee(compilationUnit, offsetAndLengthFuture);

			listener.setEnabled(false);
			notifyListener(11, 1, listener, document);
			assertThat(offsetAndLengthFuture.get()).isNull();

			listener.setEnabled(true);
			notifyListener(11, 1, listener, document);
			assertThat(offsetAndLengthFuture.get()).isEqualTo(new ImmutablePair<>(11, 1));
		});
	}

}
