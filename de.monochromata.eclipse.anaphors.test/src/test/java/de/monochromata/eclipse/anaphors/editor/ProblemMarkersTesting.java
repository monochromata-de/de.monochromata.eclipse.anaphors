package de.monochromata.eclipse.anaphors.editor;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.eclipse.jdt.core.compiler.IProblem;

public interface ProblemMarkersTesting {

	default void assertProblemMarkers(final CognitiveEditor editor, final List<IProblem> problems,
			final String... expectedProblemMessages) {
		final List<String> problemMessages = problems.stream().map(IProblem::getMessage).collect(toList());
		assertThat(problemMessages).containsExactly(expectedProblemMessages);
	}

	default void assertNoProblemMarkersPresent(final CognitiveEditor editor, final List<IProblem> problems) {
		assertThat(problems).isEmpty();
	}

}
