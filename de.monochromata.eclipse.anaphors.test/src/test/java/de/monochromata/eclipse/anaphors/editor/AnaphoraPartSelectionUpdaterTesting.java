package de.monochromata.eclipse.anaphors.editor;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.internal.corext.refactoring.change.ChangeTesting;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Position;

import de.monochromata.function.ThrowingFunction;

public interface AnaphoraPartSelectionUpdaterTesting extends ChangeTesting {

    default <T extends Throwable> void assertSelectionUpdate(
            final SelectionRelativeToAnaphoraPart.Relation initialSelectionRelation,
            final int distanceToAnaphoraPart,
            final int newSelectionOffset, final int newSelectionLength,
            final Position position,
            final Consumer<IRegion> resultAsserter)
            throws T, Throwable {
        assertSelectionUpdate(initialSelectionRelation, distanceToAnaphoraPart,
                newSelectionOffset, newSelectionLength, position,
                in -> in, in -> in, resultAsserter);
    }

    default <T extends Throwable> void assertSelectionUpdate(
            final SelectionRelativeToAnaphoraPart.Relation initialSelectionRelation,
            final int distanceToAnaphoraPart,
            final int newSelectionOffset, final int newSelectionLength,
            final Position position,
            final ThrowingFunction<Integer, Integer, BadLocationException> toImageOffset,
            final ThrowingFunction<Integer, Integer, BadLocationException> toOriginOffset,
            final Consumer<IRegion> resultAsserter)
            throws T, Throwable {
        final SelectionRelativeToAnaphoraPart relativeSelection = new SelectionRelativeToAnaphoraPart(
                initialSelectionRelation, distanceToAnaphoraPart);
        final Pair<? extends Position, SelectionRelativeToAnaphoraPart> closestPositionAndSelection = new ImmutablePair<>(
                position, relativeSelection);
        assertSelectionUpdate(newSelectionOffset, newSelectionLength, closestPositionAndSelection,
                singletonList(closestPositionAndSelection), toImageOffset, toOriginOffset, resultAsserter);
    }

    default void assertSelectionUpdate(final int newSelectionOffset, final int newSelectionLength,
            final Pair<? extends Position, SelectionRelativeToAnaphoraPart> closestPositionAndSelection,
            final Pair<? extends Position, SelectionRelativeToAnaphoraPart> otherPositionAndSelection,
            final Consumer<IRegion> resultAsserter) {
        final AnaphoraPartSelectionUpdater updater = new AnaphoraPartSelectionUpdater(closestPositionAndSelection,
                asList(closestPositionAndSelection, otherPositionAndSelection), in -> in, in -> in);
        final IRegion correctedSelection = updater.correctSelection(newSelectionOffset, newSelectionLength);
        resultAsserter.accept(correctedSelection);
    }

    default void assertSelectionUpdate(final int newSelectionOffset, final int newSelectionLength,
            final Pair<? extends Position, SelectionRelativeToAnaphoraPart> closestPositionAndSelection,
            final List<Pair<? extends Position, SelectionRelativeToAnaphoraPart>> allPositionsAndSelections,
            final ThrowingFunction<Integer, Integer, BadLocationException> toImageOffset,
            final ThrowingFunction<Integer, Integer, BadLocationException> toOriginOffset,
            final Consumer<IRegion> resultAsserter) {
        final AnaphoraPartSelectionUpdater updater = new AnaphoraPartSelectionUpdater(closestPositionAndSelection,
                allPositionsAndSelections, toImageOffset, toOriginOffset);
        final IRegion correctedSelection = updater.correctSelection(newSelectionOffset, newSelectionLength);
        resultAsserter.accept(correctedSelection);
    }

    default int toImageOffset(final int originOffset, final int underspecifiedIndex1, final int underspecifiedIndex2) {
        if (originOffset <= underspecifiedIndex1) {
            return originOffset;
        } else if (originOffset == underspecifiedIndex2) {
            return originOffset - 1;
        } // else: implies originOffset > underspecifiedIndex2
        return originOffset - 2;
    }

    default int toOriginOffset(final int imageOffset, final int underspecifiedIndex1, final int underspecifiedIndex2) {
        if (imageOffset < underspecifiedIndex1) {
            return imageOffset;
        }
        return imageOffset + 2;
    }

}
