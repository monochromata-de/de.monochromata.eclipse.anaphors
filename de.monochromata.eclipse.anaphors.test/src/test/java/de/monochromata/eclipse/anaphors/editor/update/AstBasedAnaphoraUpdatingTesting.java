package de.monochromata.eclipse.anaphors.editor.update;

import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.jdt.core.dom.ASTNode.STRING_LITERAL;
import static org.eclipse.jdt.core.dom.anaphors.DomVisiting.requireNode;

import java.util.function.BiConsumer;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.anaphors.ASTTesting;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;

import de.monochromata.eclipse.anaphors.AnaphoraTesting;

public interface AstBasedAnaphoraUpdatingTesting extends AnaphoraTesting, ASTTesting {

	default void assertAnaphoraUpdate(final BiConsumer<CompilationUnit, PublicAnaphora> asserter) throws CoreException {
		assertAnaphoraUpdate("Foo",
				"class Foo { String relExp = \"relatedExpression\"; String anaphor = \"anaphor\"; }", 28, 19,
				"\"anaphor\"", 66, 9, asserter);
	}

	default void assertAnaphoraUpdate(final String typeName, final String source, final int newRelatedExpressionStart,
			final int newRelatedExpressionLength, final String newAnaphor, final int newAnaphorStart,
			final int newAnaphorLength, final BiConsumer<CompilationUnit, PublicAnaphora> asserter)
			throws CoreException {
		assertProjectAndCompilationUnit(typeName, source, (newScope, project) -> {
			final AST oldAst = AST.newAST(AST.JLS8);
			final StringLiteral oldRelatedExpression = createStringLiteral(oldAst, 0, "relatedExpression");
			final String oldAnaphor = "anaphor";
			final StringLiteral oldAnaphorExpression = createStringLiteral(oldAst, 20, oldAnaphor);
			final ASTNode newRelatedExpression = requireNode(newScope, STRING_LITERAL, newRelatedExpressionStart,
					newRelatedExpressionLength);
			final Expression newAnaphorExpression = (Expression) requireNode(newScope, STRING_LITERAL, newAnaphorStart,
					newAnaphorLength);
			final PublicAnaphora preliminaryAnaphora = mockAnaphora(newScope, oldRelatedExpression,
					oldAnaphorExpression, newRelatedExpression, newAnaphor, newAnaphorExpression);
			asserter.accept(newScope, preliminaryAnaphora);
		});
	}

	default void assertUpdatedAnaphora(final PublicAnaphora updatedAnaphora, final int relatedExpressionStart,
			final int relatedExpressionLength, final String relatedExpressionValue, final int anaphorStart,
			final int anaphorLength, final String anaphorValue) {
		assertThat(updatedAnaphora).isNotNull();
		final ASTNode relatedExpression = updatedAnaphora.getRelatedExpression().getRelatedExpression();
		assertUpdatedAstNode(relatedExpression, relatedExpressionStart, relatedExpressionLength,
				relatedExpressionValue);
		final Expression anaphor = updatedAnaphora.getAnaphorExpression();
		assertUpdatedAstNode(anaphor, anaphorStart, anaphorLength, anaphorValue);
	}

	default void assertUpdatedAstNode(final ASTNode astNode, final int start, final int length, final String value) {
		assertThat(astNode).isInstanceOf(StringLiteral.class);
		assertThat(astNode.getStartPosition()).isEqualTo(start);
		assertThat(astNode.getLength()).isEqualTo(length);
		assertThat(((StringLiteral) astNode).getEscapedValue()).isEqualTo(value);
	}

}
