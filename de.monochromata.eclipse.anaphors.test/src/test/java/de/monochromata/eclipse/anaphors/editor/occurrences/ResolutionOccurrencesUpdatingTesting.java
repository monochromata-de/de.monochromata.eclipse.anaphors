package de.monochromata.eclipse.anaphors.editor.occurrences;

import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;

public interface ResolutionOccurrencesUpdatingTesting {

    default Annotation createAndAddAnnotation(final IAnnotationModel annotationModel) {
        final Annotation annotation = new Annotation(false);
        annotationModel.addAnnotation(annotation, new Position(0, 10));
        return annotation;
    }

}
