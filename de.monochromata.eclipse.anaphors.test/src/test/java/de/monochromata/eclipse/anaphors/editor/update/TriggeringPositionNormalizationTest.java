package de.monochromata.eclipse.anaphors.editor.update;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.position.KnownProblemPosition;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;

public class TriggeringPositionNormalizationTest implements TriggeringPositionNormalizationTesting {

	private final Anaphora anaphora = new Anaphora("a", "b", "c", "d");
	private final KnownProblemPosition problemPosition1 = new KnownProblemPosition(10, 10, 2, "problem message");
	private final KnownProblemPosition problemPosition2 = new KnownProblemPosition(30, 10, 2, "problem message");
	private final List<KnownProblemPosition> oneProblemPosition = singletonList(problemPosition1);
	private final List<KnownProblemPosition> twoProblemPositions = asList(problemPosition1, problemPosition2);
	private final List<Pair<PositionForAnaphor, KnownProblemPosition>> overlaps = new ArrayList<>();

	@Test
	public void nonOverlappingProblemPositionsShouldBeRetained() {
		final List<KnownProblemPosition> result = invokeTestee(
				singletonList(new PositionForAnaphor(0, 10, anaphora)), oneProblemPosition, overlaps);

		assertThat(result).containsExactly(problemPosition1);
	}

	@Test
	public void noNotificationShouldBeSentForNonOverlappingProblemPositions() {
		invokeTestee(singletonList(new PositionForAnaphor(20, 3, anaphora)), oneProblemPosition, overlaps);

		assertThat(overlaps).isEmpty();
	}

	@Test
	public void problemPositionOverlappingAtStartShouldBeRemoved() {
		final List<KnownProblemPosition> result = invokeTestee(
				singletonList(new PositionForAnaphor(0, 11, anaphora)), oneProblemPosition, overlaps);

		assertThat(result).isEmpty();
	}

	@Test
	public void problemPositionOverlappingAtEndShouldBeRemoved() {
		final List<KnownProblemPosition> result = invokeTestee(
				singletonList(new PositionForAnaphor(19, 3, anaphora)), oneProblemPosition, overlaps);

		assertThat(result).isEmpty();
	}

	@Test
	public void notificationShouldBeSentForOverlappingProblemPosition() {
		final PositionForAnaphor anaphorPosition = new PositionForAnaphor(19, 3, anaphora);

		invokeTestee(singletonList(anaphorPosition), oneProblemPosition, overlaps);

		assertThat(overlaps).containsExactly(new ImmutablePair<>(anaphorPosition, problemPosition1));
	}

	@Test
	public void multipleOverlappingProblemPositionsShouldBeRemoved() {
		final PositionForAnaphor anaphorPosition = new PositionForAnaphor(0, 40, anaphora);

		final List<KnownProblemPosition> result = invokeTestee(singletonList(anaphorPosition), twoProblemPositions,
				overlaps);

		assertThat(result).isEmpty();
	}

	@Test
	public void notificationsShouldBeSendForMultipleOverlappingProblemPositions() {
		final PositionForAnaphor anaphorPosition = new PositionForAnaphor(0, 40, anaphora);

		final List<KnownProblemPosition> result = invokeTestee(singletonList(anaphorPosition), twoProblemPositions,
				overlaps);

		assertThat(overlaps).containsExactly(new ImmutablePair<>(anaphorPosition, problemPosition1),
				new ImmutablePair<>(anaphorPosition, problemPosition2));
	}

	@Test
	public void deletedPositionFromMemberShouldBeRetained() {
		final PositionForAnaphor anaphorPosition = new PositionForAnaphor(19, 3, anaphora);
		anaphorPosition.isDeleted = true;

		final List<KnownProblemPosition> result = invokeTestee(singletonList(anaphorPosition), oneProblemPosition,
				overlaps);

		assertThat(result).containsExactly(problemPosition1);
	}

	@Test
	public void noNotificationShouldBeSentForDeletedPositionFromMember() {
		final PositionForAnaphor anaphorPosition = new PositionForAnaphor(19, 3, anaphora);
		anaphorPosition.isDeleted = true;

		invokeTestee(singletonList(anaphorPosition), oneProblemPosition, overlaps);

		assertThat(overlaps).isEmpty();
	}

}
