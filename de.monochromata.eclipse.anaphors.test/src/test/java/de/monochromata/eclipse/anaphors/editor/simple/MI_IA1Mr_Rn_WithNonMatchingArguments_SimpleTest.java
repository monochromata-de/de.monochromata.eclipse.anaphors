package de.monochromata.eclipse.anaphors.editor.simple;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.compiler.IProblem;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.editor.CodeBuilder;
import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;

public class MI_IA1Mr_Rn_WithNonMatchingArguments_SimpleTest extends AbstractSimpleEditorTest {

	private static final List<String> expectedProblemMessages = List.of(
			"The method login(int) in the type MI_IA1Mr_Rn_WithNonMatchingArguments_Simple is not applicable for the arguments (String)",
			"login cannot be resolved to a variable");

	@Override
	public String getExpectedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
		builder.addLine("");
		builder.addLine("public class MI_IA1Mr_Rn_WithNonMatchingArguments_Simple {");
		builder.addLine("");
		builder.addLine("    public static void main() {");
		builder.addLine("        login(\"input\");");
		builder.addLine("        System.err.println(login);");
		builder.addLine("    }");
		builder.addLine("");
		builder.addLine("    private static ObjectSession login(final int input) {");
		builder.addLine("        return null;");
		builder.addLine("    }");
		builder.addLine("");
		builder.addLine("    private static class ObjectSession {");
		builder.addLine("    }");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	public String getExpectedUnderspecifiedEventualSourceCode() {
		final CodeBuilder builder = new CodeBuilder();
		builder.addLine("package de.monochromata.eclipse.anaphors.editor.simple;");
		builder.addLine("");
		builder.addLine("public class MI_IA1Mr_Rn_WithNonMatchingArguments_Simple {");
		builder.addLine("");
		builder.addLine("    public static void main() {");
		builder.addLine("        login(\"input\");");
		builder.addLine("        System.err.println(login);");
		builder.addLine("    }");
		builder.addLine("");
		builder.addLine("    private static ObjectSession login(final int input) {");
		builder.addLine("        return null;");
		builder.addLine("    }");
		builder.addLine("");
		builder.addLine("    private static class ObjectSession {");
		builder.addLine("    }");
		builder.addLine("}");
		return builder.toString();
	}

	@Override
	public void waitForAnaphoraRelations(final CognitiveEditor editor) {
		waitForAnaphoraRelations(editor, 0);
	}

	@Override
	public void assertAnaphoraRelations() {
		assertThat(getNumberOfAnaphoraRelations()).isEqualTo(0);
	}

	@Override
	protected void waitForProblemsToBeSolved(final CognitiveEditor editor) {
		waitForProblems(editor, expectedProblemMessages.size());
	}

	@Override
	protected void assertProblemMarkers(final CognitiveEditor editor, final List<IProblem> problems) {
		assertThat(problems.stream().map(IProblem::getMessage)).containsExactlyElementsOf(expectedProblemMessages);
	}

	@Override
	protected void testReResolution(final CognitiveEditor editor) {
		// Do not re-resolve since no anaphors could be resolved initially
	}

	@Override
	protected Pair<Anaphora, Anaphora> getExpectedResolvedAndRealizedAnaphora() {
		throw new UnsupportedOperationException("Not expecting any problems");
	}

}
