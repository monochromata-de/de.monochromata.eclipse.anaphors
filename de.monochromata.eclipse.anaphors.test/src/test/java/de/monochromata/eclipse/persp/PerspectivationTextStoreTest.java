package de.monochromata.eclipse.persp;

import static java.util.stream.IntStream.range;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jface.text.Document;
import org.junit.Test;

import de.monochromata.eclipse.persp.PerspectivationDocument;
import de.monochromata.eclipse.persp.PerspectivationDocumentManager;
import de.monochromata.eclipse.persp.PerspectivationTextStore;

public class PerspectivationTextStoreTest implements PerspectivationTextStoreTesting {

	final PerspectivationDocumentManager documentManager = new PerspectivationDocumentManager();

	@Override
	public PerspectivationDocumentManager documentManager() {
		return documentManager;
	}

	// Test set(String)

	@Test
	public void theMasterDocumentContainsTheSetTextAfterSet() throws Exception {
		final Triple<Document, PerspectivationDocument, PerspectivationTextStore> result = createStoreWith1ToLowerCase();

		final String newText = "Some new text";
		result.getRight().set(newText);

		assertThat(result.getLeft().get()).isEqualTo(newText);
	}

	// Test replace(int,int,String)

	@Test
	public void theReplacementTextIsAppliedToTheMasterDocument() throws Exception {
		final Triple<Document, PerspectivationDocument, PerspectivationTextStore> result = createStoreWith1ToLowerCase();

		result.getRight().replace(7, 2, "cked");
		assertThat(result.getLeft().get()).isEqualTo("The quicked brown fox\njumps over the lazy dog.");
	}

	// Test getLength()

	@Test
	public void getLengthReturnsTheMappingLength() throws Exception {
		final Triple<Document, PerspectivationDocument, PerspectivationTextStore> result = createStoreWith1ToLowerCase();
		assertThat(result.getRight().getLength()).isEqualTo(result.getMiddle().getLength());
	}

	// Test get()

	@Test
	public void getReflectsToLowerCasePosition() throws Exception {
		final Triple<Document, PerspectivationDocument, PerspectivationTextStore> result = createStoreWith1ToLowerCase();

		final String expectedText = "the quick brown fox\njumps over the lazy dog.";
		final String actualText = range(0, expectedText.length()).mapToObj(offset -> result.getRight().get(offset))
				.map(character -> character.toString()).collect(Collectors.joining());

		assertThat(actualText).isEqualTo(expectedText);
	}

	// Test get(int,int)

	@Test
	public void testGetReflectsToLowerCasePosition() throws Exception {
		final Triple<Document, PerspectivationDocument, PerspectivationTextStore> result = createStoreWith1ToLowerCase();

		final String expectedText = "the quick brown fox\njumps over the lazy dog.";
		final String actualText = result.getRight().get(0, expectedText.length());

		assertThat(actualText).isEqualTo(expectedText);
	}

	@Test
	public void testGetReflectsToLowerCasePositionWhenRequestingPartialText() throws Exception {
		final Consumer<PerspectivationDocument> slaveModifier = slave -> {
			try {
				slave.toLowerCase(31);
				slave.removeMasterDocumentRange(26, 5);
			} catch (final Exception e) {
				throw new IllegalStateException(e);
			}
		};
		final Triple<Document, PerspectivationDocument, PerspectivationTextStore> result = createAndModifyStore(
				"The quick brown fox\njumps over The lazy dog.", slaveModifier);

		final String expectedText = "the lazy dog.";
		final String actualText = result.getRight().get(26, 14);

		assertThat(actualText).isEqualTo(expectedText);
	}

	@Test
	public void testGetReflectsToLowerCasePositionWhenRequestingPartialTextFromDifferentFragments() throws Exception {
		final Consumer<PerspectivationDocument> slaveModifier = slave -> {
			try {
				slave.toLowerCase(31);
				slave.removeMasterDocumentRange(26, 5);
			} catch (final Exception e) {
				throw new IllegalStateException(e);
			}
		};
		final Triple<Document, PerspectivationDocument, PerspectivationTextStore> result = createAndModifyStore(
				"The quick brown fox\njumps over The lazy dog.", slaveModifier);

		final String expectedText = "jumps the lazy dog.";
		final String actualText = result.getRight().get(20, 19);

		assertThat(actualText).isEqualTo(expectedText);
	}

}
