package de.monochromata.eclipse.persp;

import static de.monochromata.eclipse.persp.HidePosition.HIDE_CATEGORY;
import static de.monochromata.eclipse.persp.ToLowerCasePosition.TO_LOWER_CASE_CATEGORY;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapPositionExceptions;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.projection.Fragment;
import org.eclipse.jface.text.projection.Segment;
import org.junit.Test;

import de.monochromata.eclipse.position.UncheckedBadLocationException;

public class PerspectivationDocumentTest implements PerspectivationDocumentTesting {

    final PerspectivationDocumentManager documentManager = new PerspectivationDocumentManager();

    @Override
    public PerspectivationDocumentManager documentManager() {
        return documentManager;
    }

    // Test toLowerCase(int)

    @Test
    public void toLowerCaseThrowsBadLocationExceptionForNegativeValue() throws Exception {
        final PerspectivationDocument image = createTestImageDocument();

        assertThatThrownBy(() -> image.toLowerCase(-1))
                .isInstanceOf(BadLocationException.class)
                .hasMessage("offset -1 < 0");
    }

    @Test
    public void toLowerCaseThrowsBadLocationExceptionForValueExceedingEndOfDocument() throws Exception {
        final PerspectivationDocument image = createTestImageDocument();

        assertThatThrownBy(() -> image.toLowerCase(44)).isInstanceOf(BadLocationException.class);
    }

    @Test
    public void toLowerCaseOnUpperCaseCharacterReturnsNonEmptyOptional() throws Exception {
        final PerspectivationDocument image = createTestImageDocument();

        assertThat(image.toLowerCase(0)).isNotEmpty();
    }

    @Test
    public void toLowerCaseOnUpperCaseCharacterAddsTransformation() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createTestOriginAndImageDocument();

        originAndImage.getRight().toLowerCase(0);

        assertThat(originAndImage.getLeft().getPositions(TO_LOWER_CASE_CATEGORY))
                .containsExactly(new ToLowerCasePosition(0));
    }

    @Test
    public void toLowerCaseOnLowerCaseCharacterReturnsEmptyOptional() throws Exception {
        final PerspectivationDocument image = createTestImageDocument();

        assertThat(image.toLowerCase(1)).isEmpty();
    }

    @Test
    public void toLowerCaseOnLowerCaseCharacterDoesNotAddTransformation() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createTestOriginAndImageDocument();
        originAndImage.getRight().toLowerCase(1);

        assertThat(originAndImage.getLeft().getPositions(TO_LOWER_CASE_CATEGORY)).isEmpty();
    }

    @Test
    public void toLowerCaseInvokedTwiceForSameOffsetReturnsEmptyOptionalOnSecondInvocation() throws Exception {
        final PerspectivationDocument image = createTestImageDocument();
        image.toLowerCase(0);

        assertThat(image.toLowerCase(0)).isEmpty();
    }

    @Test
    public void toLowerCaseRemainsEffectiveAfterTextIsAddedBeforeTheOffset() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createTestOriginAndImageDocument();
        originAndImage.getRight().toLowerCase(0);
        originAndImage.getRight().replace(0, 0, "ABC");

        assertThat(originAndImage.getLeft().getPositions(TO_LOWER_CASE_CATEGORY))
                .containsExactly(new ToLowerCasePosition(3));
    }

    // Test removeToLowerCase(int)

    @Test
    public void removeToLowerCaseThrowsBadLocationExceptionForNegativeValue() throws Exception {
        final PerspectivationDocument image = createTestImageDocument();

        assertThatThrownBy(() -> image.removeToLowerCase(-1))
                .isInstanceOf(IllegalStateException.class)
                .hasCauseInstanceOf(BadLocationException.class);
    }

    @Test
    public void removeToLowerCaseThrowsBadLocationExceptionForValueExceedingEndOfDocument() throws Exception {
        final PerspectivationDocument image = createTestImageDocument();

        assertThatThrownBy(() -> image.removeToLowerCase(44))
                .isInstanceOf(UncheckedBadLocationException.class)
                .hasCauseInstanceOf(BadLocationException.class);
    }

    @Test
    public void removeToLowerCaseOnTransformationReturnsTrue() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createTestOriginAndImageDocument();
        originAndImage.getRight().toLowerCase(0);
        assertThat(originAndImage.getLeft().getPositions(TO_LOWER_CASE_CATEGORY)).contains(new ToLowerCasePosition(0));

        assertThat(originAndImage.getRight().removeToLowerCase(0)).isTrue();
    }

    @Test
    public void removeToLowerCaseOnTransformationRemovesPosition() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createTestOriginAndImageDocument();
        originAndImage.getRight().toLowerCase(0);
        assertThat(originAndImage.getLeft().getPositions(TO_LOWER_CASE_CATEGORY)).contains(new ToLowerCasePosition(0));

        originAndImage.getRight().removeToLowerCase(0);

        assertThat(originAndImage.getLeft().getPositions(TO_LOWER_CASE_CATEGORY)).isEmpty();
    }

    @Test
    public void removeToLowerCaseInvokedTwiceForSameOffsetReturnsFalseOnSecondInvocation() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createTestOriginAndImageDocument();
        originAndImage.getRight().toLowerCase(0);
        assertThat(originAndImage.getLeft().getPositions(TO_LOWER_CASE_CATEGORY)).contains(new ToLowerCasePosition(0));
        originAndImage.getRight().removeToLowerCase(0);

        assertThat(originAndImage.getRight().removeToLowerCase(0)).isFalse();
    }

    // Test PerspectivationDocument.removeToLowerCase(DocumentEvent OriginEvent)

    @Test
    public void removeToLowerCaseWorksForOriginDocumentOffsetAfterRemovedOriginDocumentRange() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                "The quick brown fox\njumps over the lazy Dog.");
        originAndImage.getRight().removeMasterDocumentRange(0, 20);
        originAndImage.getRight().toLowerCase(20);

        final DocumentEvent originEvent = createOriginEvent(30, 14);
        originAndImage.getRight().removeToLowerCaseRelativeToOrigin(originEvent);
        assertThat(originAndImage.getLeft().getPositions(TO_LOWER_CASE_CATEGORY)).isEmpty();
    }

    @Test
    public void removeToLowerCaseWorksForOriginDocumentRangeCoveringRemovedOriginDocumentRange() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                "The quick brown fox\njumps over the lazy Dog.");
        originAndImage.getRight().removeMasterDocumentRange(26, 13);
        originAndImage.getRight().toLowerCase(27);
        assertThat(originAndImage.getLeft().getPositions(TO_LOWER_CASE_CATEGORY)).contains(new ToLowerCasePosition(40));

        final DocumentEvent OriginEvent = createOriginEvent(20, 24);
        originAndImage.getRight().removeToLowerCaseRelativeToOrigin(OriginEvent);
        assertThat(originAndImage.getLeft().getPositions(TO_LOWER_CASE_CATEGORY)).isEmpty();
    }

    // Test removeToLowerCase(Position)

    @Test
    public void nothingHappensIfRemoveToLowerCaseIsGivenADeletedPosition() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                "The quick brown fox\njumps over the lazy Dog.");
        final ToLowerCasePosition lowerCase = originAndImage.getRight().toLowerCase(0).get();
        assertThat(originAndImage.getLeft().getPositions(TO_LOWER_CASE_CATEGORY)).contains(lowerCase);
        // Fake: set the position to deleted
        lowerCase.isDeleted = true;

        originAndImage.getRight().removeToLowerCase(lowerCase);

        // The lowercasing is retained, because the ToLowerCase has already been marked
        // as deleted.
        assertThat(originAndImage.getRight().get()).isEqualTo("the quick brown fox\njumps over the lazy Dog.");
    }

    // Test inlineToLowerCaseIntoOriginDocument(List<Position>)

    @Test
    public void inlineToLowerCaseIntoOriginDocumentThrowsBadLocationException() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                "The quick brown fox\njumps over the lazy dog.");
        final int offsetInOrigin = originAndImage.getLeft().getLength();

        assertThatThrownBy(() -> originAndImage.getRight().inlineToLowerCaseIntoOriginDocument(
                new ToLowerCasePosition(offsetInOrigin)))
                        .isInstanceOf(UncheckedBadLocationException.class)
                        .hasCauseInstanceOf(BadLocationException.class);
    }

    @Test
    public void inlineToLowerCaseIntoOriginDocumentConvertsTheOriginDocument() throws Exception {
        final String initialText = "The quick brown fox\njumps over the lazy dog.";
        final String lowerCasedText = "the quick brown fox\njumps over the lazy dog.";
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(initialText);
        final ToLowerCasePosition position = new ToLowerCasePosition(0);
        originAndImage.getLeft().addPosition(TO_LOWER_CASE_CATEGORY, position);
        assertText(originAndImage, initialText, lowerCasedText);

        originAndImage.getRight().inlineToLowerCaseIntoOriginDocument(position);

        assertText(originAndImage, lowerCasedText, lowerCasedText);
    }

    @Test
    public void inlineToLowerCaseIntoOriginDocumentRemovesTheHidePosition() throws Exception {
        final String initialText = "The quick brown fox\njumps over the lazy dog.";
        final String lowerCasedText = "the quick brown fox\njumps over the lazy dog.";
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(initialText);
        final ToLowerCasePosition position = new ToLowerCasePosition(0);
        originAndImage.getLeft().addPosition(TO_LOWER_CASE_CATEGORY, position);
        assertText(originAndImage, initialText, lowerCasedText);

        originAndImage.getRight().inlineToLowerCaseIntoOriginDocument(position);

        assertThat(originAndImage.getLeft().getPositions(TO_LOWER_CASE_CATEGORY)).isEmpty();
    }

    @Test
    public void inlineToLowerCaseIntoOriginDocumentRemovesMultiplePositions() throws Exception {
        final String initialText = "The Quick Brown Fox\nJumps over the lazy dog.";
        final String lowerCasedText = "the quick brown fox\njumps over the lazy dog.";
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(initialText);
        final ToLowerCasePosition prePosition = new ToLowerCasePosition(0);
        final List<ToLowerCasePosition> coveredPositions = asList(new ToLowerCasePosition(10),
                new ToLowerCasePosition(4),
                new ToLowerCasePosition(16));
        final ToLowerCasePosition postPosition = new ToLowerCasePosition(20);
        final List<ToLowerCasePosition> allPositions = Stream
                .concat(Stream.of(prePosition, postPosition),
                        coveredPositions.stream())
                .collect(toList());

        allPositions.forEach(position -> wrapPositionExceptions(
                () -> originAndImage.getLeft().addPosition(TO_LOWER_CASE_CATEGORY, position)));
        assertText(originAndImage, initialText, lowerCasedText);

        originAndImage.getRight().inlineToLowerCaseIntoOriginDocument(coveredPositions);

        assertThat(originAndImage.getLeft().getPositions(TO_LOWER_CASE_CATEGORY)).containsExactly(prePosition,
                postPosition);
        assertText(originAndImage, "The quick brown fox\nJumps over the lazy dog.", lowerCasedText);
    }

    // Test PerspectivationDocument.getToLowerCases(...)

    @Test
    public void onlyIntersectingToLowerCasesAreReturned() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createTestOriginAndImageDocument();
        addToLowerCase(originAndImage.getLeft(), new ToLowerCasePosition(19));
        final List<ToLowerCasePosition> toLowerCasesToBeReturned = asList(new ToLowerCasePosition(20),
                new ToLowerCasePosition(21), new ToLowerCasePosition(25), new ToLowerCasePosition(29));
        addToLowerCases(originAndImage.getLeft(), toLowerCasesToBeReturned);
        addToLowerCase(originAndImage.getLeft(), new ToLowerCasePosition(30));

        assertThat(originAndImage.getRight().getToLowerCases(20, 10))
                .containsExactlyElementsOf(toLowerCasesToBeReturned);
    }

    @Test
    public void noToLowerCasesAreProvidedForEmptyRange() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createTestOriginAndImageDocument();
        addToLowerCase(originAndImage.getLeft(), new ToLowerCasePosition(0));

        assertThat(originAndImage.getRight().getToLowerCases(0, 0)).isEmpty();
    }

    // Test PerspectivationDocument.getToLowerCaseAt(...)

    @Test
    public void toLowerCaseIsAccessible() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createTestOriginAndImageDocument();
        addToLowerCase(originAndImage.getLeft(), new ToLowerCasePosition(9));

        assertThat(originAndImage.getRight().getToLowerCaseAt(9)).isTrue();
    }

    @Test
    public void toLowerCaseIsFalseOutsideOfToLowerCase() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createTestOriginAndImageDocument();
        addToLowerCase(originAndImage.getLeft(), new ToLowerCasePosition(0));

        assertThat(originAndImage.getRight().getToLowerCaseAt(1)).isFalse();
    }

    // Test whether ToLowerCases are removed (via events from the Origin
    // document) when text is set or replaced

    @Test
    public void thereAreNoToLowerCasesAfterSet() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createTestOriginAndImageDocument();
        originAndImage.getRight().toLowerCase(0);
        assertThat(originAndImage.getLeft().getPositions(TO_LOWER_CASE_CATEGORY)).isNotEmpty();

        originAndImage.getRight().set("Some new text");

        assertThat(originAndImage.getLeft().getPositions(TO_LOWER_CASE_CATEGORY)).isEmpty();
    }

    @Test
    public void toLowerCasesOverlappingTheReplacedRegionAreRemoved() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createTestOriginAndImageDocument();
        addToLowerCase(originAndImage.getLeft(), new ToLowerCasePosition(7));
        addToLowerCase(originAndImage.getLeft(), new ToLowerCasePosition(8));

        originAndImage.getRight().replace(7, 2, "cked");

        assertThat(originAndImage.getLeft().getPositions(TO_LOWER_CASE_CATEGORY)).isEmpty();
    }

    @Test
    public void toLowerCasesNotOverlappingTheReplacedRegionAreRetained() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createTestOriginAndImageDocument();
        final ToLowerCasePosition unaffectedToLowerCase = new ToLowerCasePosition(9);
        addToLowerCase(originAndImage.getLeft(), unaffectedToLowerCase);

        originAndImage.getRight().replace(7, 2, "cked");

        assertThat(originAndImage.getLeft().getPositions(TO_LOWER_CASE_CATEGORY))
                .containsExactly(unaffectedToLowerCase);
    }

    // Test hideOriginDocumentRange(int,int)

    @Test
    public void hideOriginDocumentRangeRemovesOriginDocumentRange() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                "The quick brown fox\njumps over the lazy dog.");

        originAndImage.getRight().hideOriginDocumentRange(4, 6);

        assertText(originAndImage, "The quick brown fox\njumps over the lazy dog.",
                "The brown fox\njumps over the lazy dog.");
    }

    @Test
    public void hideOriginDocumentRangeAddsHidePosition() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                "The quick brown fox\njumps over the lazy dog.");
        assertThat(originAndImage.getLeft().getPositions(HIDE_CATEGORY)).isEmpty();

        originAndImage.getRight().hideOriginDocumentRange(4, 6);

        assertThat(originAndImage.getLeft().getPositions(HIDE_CATEGORY))
                .containsExactly(new HidePosition(4, 6));
    }

    // Test removeHiddenRangesFromOriginDocument(int,int)

    @Test
    public void removeHiddenRangeFromOriginDocumentThrowsBadLocationException() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                "The quick brown fox\njumps over the lazy dog.");
        final int offsetInOrigin = originAndImage.getLeft().getLength();

        assertThatThrownBy(() -> originAndImage.getRight().removeHiddenRangeFromOriginDocument(
                new HidePosition(offsetInOrigin, 100)))
                        .isInstanceOf(UncheckedBadLocationException.class)
                        .hasCauseInstanceOf(BadLocationException.class);
    }

    @Test
    public void removeHiddenRangesFromOriginDocumentRemovesTheHiddenText() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                "The quick brown fox\njumps over the lazy dog.");
        final HidePosition position = new HidePosition(4, 6);
        originAndImage.getLeft().addPosition(HIDE_CATEGORY, position);
        originAndImage.getRight().removeMasterDocumentRange(4, 6);

        originAndImage.getRight().removeHiddenRangeFromOriginDocument(position);

        assertText(originAndImage, "The brown fox\njumps over the lazy dog.",
                "The brown fox\njumps over the lazy dog.");
    }

    @Test
    public void removeHiddenRangeFromOriginDocumentRemovesTheHidePosition() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                "The quick brown fox\njumps over the lazy dog.");
        final HidePosition position = new HidePosition(4, 6);
        originAndImage.getLeft().addPosition(HIDE_CATEGORY, position);
        originAndImage.getRight().removeMasterDocumentRange(4, 6);

        originAndImage.getRight().removeHiddenRangeFromOriginDocument(position);

        assertThat(originAndImage.getLeft().getPositions(HIDE_CATEGORY)).isEmpty();
    }

    @Test
    public void removeHiddenRangesFromOriginDocumentRemovesMultiplePositions() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                "The quick brown fox\njumps over the lazy dog.");
        final HidePosition prePosition = new HidePosition(0, 3);
        final List<HidePosition> coveredPositions = asList(new HidePosition(10, 6), new HidePosition(4, 6),
                new HidePosition(16, 3));
        final HidePosition postPosition = new HidePosition(20, 5);
        final List<HidePosition> allPositions = Stream
                .concat(Stream.of(prePosition, postPosition),
                        coveredPositions.stream())
                .collect(toList());

        allPositions.forEach(position -> wrapPositionExceptions(
                () -> originAndImage.getLeft().addPosition(HIDE_CATEGORY, position)));

        originAndImage.getRight().removeHiddenRangesFromOriginDocument(coveredPositions);

        assertThat(originAndImage.getLeft().getPositions(HIDE_CATEGORY)).containsExactly(prePosition, postPosition);
        assertText(originAndImage, "The \njumps over the lazy dog.",
                "The \njumps over the lazy dog.");
    }

    @Test
    public void checkBoundaryConditionsForRemoveHiddenRangesFromOriginDocument() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                "The quick brown fox\njumps over the lazy dog.");
        final HidePosition prePosition = new HidePosition(0, 3);
        final HidePosition coveredPosition = new HidePosition(4, 6);
        final HidePosition postPosition = new HidePosition(10, 6);
        final List<HidePosition> allPositions = asList(prePosition, coveredPosition, postPosition);

        allPositions.forEach(position -> wrapPositionExceptions(
                () -> originAndImage.getLeft().addPosition(HIDE_CATEGORY, position)));

        originAndImage.getRight().removeHiddenRangeFromOriginDocument(coveredPosition);

        assertThat(originAndImage.getLeft().getPositions(HIDE_CATEGORY)).containsExactly(prePosition, postPosition);
        assertText(originAndImage, "The brown fox\njumps over the lazy dog.",
                "The brown fox\njumps over the lazy dog.");
    }

    @Test
    public void hidePositionsAreCorrectlyUpdatedOnInsertions() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                "The quick brown fox\njumps over the lazy dog.");
        asList(/* before the addition */ new HidePosition(0, 3),
                /* before the start of the addition */ new HidePosition(0, 4),
                /* crossing the offset of the addition */ new HidePosition(0, 5),
                /* starting at the addition */ new HidePosition(4, 5),
                /* starting after the addition */ new HidePosition(6, 3))
                        .forEach(position -> wrapPositionExceptions(
                                () -> originAndImage.getLeft().addPosition(HIDE_CATEGORY, position)));

        originAndImage.getRight().replace(4, 0, "added ");

        assertThat(originAndImage.getLeft().getPositions(HIDE_CATEGORY)).containsOnly(
                /* before the addition */ new HidePosition(0, 3),
                /* before the start of the addition */ new HidePosition(0, 4),
                // The next example is debatable: an insertion on a hidden region that expands
                // the hidden region. The expand/collapse annotation mechanism will in such
                // cases expand the hidden region. During perspectivation, a re-perspectivation
                // might be triggered.
                /* crossing the offset of the addition */ new HidePosition(0, 11),
                /* starting at the addition */ new HidePosition(10, 5),
                /* starting after the addition */ new HidePosition(12, 3));
        assertText(originAndImage, "The added quick brown fox\njumps over the lazy dog.",
                "The added quick brown fox\njumps over the lazy dog.");
    }

    @Test
    public void hidePositionsAreCorrectlyUpdatedOnDeletions() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                "The quick brown fox\njumps over the lazy dog.");
        asList(/* before the deletion */ new HidePosition(0, 3),
                /* before the start of the deletion */ new HidePosition(0, 4),
                /* crossing the offset of the deletion */ new HidePosition(0, 5),
                /* starting at the deletion */ new HidePosition(4, 5),
                /* starting after the end of the deletion */ new HidePosition(9, 3))
                        .forEach(position -> wrapPositionExceptions(
                                () -> originAndImage.getLeft().addPosition(HIDE_CATEGORY, position)));

        originAndImage.getRight().replace(4, 5, "");

        assertThat(originAndImage.getLeft().getPositions(HIDE_CATEGORY)).containsOnly(
                /* before the deletion */ new HidePosition(0, 3),
                /* before the start of the deletion */ new HidePosition(0, 4),
                // The next two examples are debatable: an deletion on a hidden region that
                // shrinks the hidden region. The expand/collapse annotation mechanism will in
                // such cases expand the hidden region. During perspectivation, a
                // re-perspectivation might be triggered.
                /* crossing the offset of the deletion */ new HidePosition(0, 4),
                /* starting at the deletion */ new HidePosition(4, 0),
                /* starting after the end of the deletion */ new HidePosition(4, 3));
        assertText(originAndImage, "The  brown fox\njumps over the lazy dog.",
                "The  brown fox\njumps over the lazy dog.");
    }

    // Test removeHiddenRangeFromOriginDocument(Position)

    @Test
    public void nothingHappensIfRemoveHiddenRangesFromOriginDocumentIsGivenADeletedPosition() throws Exception {
        final String initialText = "The quick brown fox\njumps over the lazy dog.";
        final String shortenedText = "The brown fox\njumps over the lazy dog.";
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                initialText);
        final HidePosition hidePosition = originAndImage.getRight().hideOriginDocumentRange(4, 6);
        assertThat(originAndImage.getRight().get()).isEqualTo(shortenedText);
        // Fake: set the position to deleted
        hidePosition.isDeleted = true;

        originAndImage.getRight().removeHiddenRangeFromOriginDocument(hidePosition);

        assertThat(originAndImage.getLeft().getPositions(HIDE_CATEGORY)).contains(hidePosition);
        assertThat(originAndImage.getLeft().get()).isEqualTo(initialText);
        assertThat(originAndImage.getRight().get()).isEqualTo(shortenedText);
    }

    // Test addMasterDocumentRange(int,int)

    @Test
    public void addMasterDocumentRangeWorksAsInSupertypeIfThereAreNoHidePositionsInTheAddedRange() throws Exception {
        final String initialText = "The quick brown fox\njumps over the lazy dog.";
        final String shortenedText = "The brown fox\njumps over the lazy dog.";
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                initialText);
        final PerspectivationDocument imageDocument = originAndImage.getRight();
        imageDocument.removeMasterDocumentRange(4, 6);
        assertThat(imageDocument.get()).isEqualTo(shortenedText);

        imageDocument.addMasterDocumentRange(4, 6);

        assertThat(imageDocument.get()).isEqualTo(initialText);
    }

    @Test
    public void addMasterDocumentRangeIsIgnoredIfExactlyOnHidePosition() throws Exception {
        final String initialText = "The quick brown fox\njumps over the lazy dog.";
        final String shortenedText = "The brown fox\njumps over the lazy dog.";
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                initialText);
        final PerspectivationDocument imageDocument = originAndImage.getRight();
        imageDocument.hideOriginDocumentRange(4, 6);
        assertThat(imageDocument.get()).isEqualTo(shortenedText);

        imageDocument.addMasterDocumentRange(4, 6);

        assertThat(imageDocument.get()).isEqualTo(shortenedText);
    }

    @Test
    public void addMasterDocumentRangeSparesTwoContainedHidePositions() throws Exception {
        final String initialText = "The quick brown fox\njumps over the lazy dog.";
        final String shortenedText = "The brown fox\njumps over the lazy dog.";
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                initialText);
        final PerspectivationDocument imageDocument = originAndImage.getRight();
        imageDocument.hideOriginDocumentRange(4, 2);
        imageDocument.removeMasterDocumentRange(6, 2);
        imageDocument.hideOriginDocumentRange(8, 2);
        assertThat(imageDocument.get()).isEqualTo(shortenedText);

        imageDocument.addMasterDocumentRange(4, 6);

        assertThat(imageDocument.get()).isEqualTo("The icbrown fox\njumps over the lazy dog.");
    }

    @Test
    public void addMasterDocumentRangeSparesThreeContainedHidePositions() throws Exception {
        final String initialText = "The quick brown fox\njumps over the lazy dog.";
        final String shortenedText = "over the lazy dog.";
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                initialText);
        final PerspectivationDocument imageDocument = originAndImage.getRight();
        imageDocument.hideOriginDocumentRange(20, 6);
        imageDocument.hideOriginDocumentRange(0, 4);
        imageDocument.hideOriginDocumentRange(10, 6);
        imageDocument.removeMasterDocumentRange(4, 6);
        imageDocument.removeMasterDocumentRange(16, 4);
        assertThat(imageDocument.get()).isEqualTo(shortenedText);

        imageDocument.addMasterDocumentRange(0, 26);

        assertThat(imageDocument.get()).isEqualTo("quick fox\nover the lazy dog.");
    }

    @Test
    public void addMasterDocumentRangeSparesContainedHidePositionThatStartsBeforeRange() throws Exception {
        final String initialText = "The quick brown fox\njumps over the lazy dog.";
        final String shortenedText = "jumps over the lazy dog.";
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                initialText);
        final PerspectivationDocument imageDocument = originAndImage.getRight();
        imageDocument.hideOriginDocumentRange(0, 10);
        imageDocument.hideOriginDocumentRange(16, 4);
        imageDocument.removeMasterDocumentRange(10, 6);
        assertThat(imageDocument.get()).isEqualTo(shortenedText);

        imageDocument.addMasterDocumentRange(4, 20);

        assertThat(imageDocument.get()).isEqualTo("brown jumps over the lazy dog.");
    }

    @Test
    public void addMasterDocumentRangeAddsRangeBeforeFirstContainedHidePosition() throws Exception {
        final String initialText = "The quick brown fox\njumps over the lazy dog.";
        final String shortenedText = "jumps over the lazy dog.";
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                initialText);
        final PerspectivationDocument imageDocument = originAndImage.getRight();
        imageDocument.hideOriginDocumentRange(4, 6);
        imageDocument.hideOriginDocumentRange(16, 4);
        imageDocument.removeMasterDocumentRange(0, 4);
        imageDocument.removeMasterDocumentRange(10, 6);
        assertThat(imageDocument.get()).isEqualTo(shortenedText);

        imageDocument.addMasterDocumentRange(0, 20);

        assertThat(imageDocument.get()).isEqualTo("The brown jumps over the lazy dog.");
    }

    @Test
    public void addMasterDocumentRangeSparesContainedHidePositionThatEndsAfterRange() throws Exception {
        final String initialText = "The quick brown fox\njumps over the lazy dog.";
        final String shortenedText = "jumps over the lazy dog.";
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                initialText);
        final PerspectivationDocument imageDocument = originAndImage.getRight();
        imageDocument.hideOriginDocumentRange(0, 10);
        imageDocument.hideOriginDocumentRange(16, 4);
        imageDocument.removeMasterDocumentRange(10, 6);
        assertThat(imageDocument.get()).isEqualTo(shortenedText);

        imageDocument.addMasterDocumentRange(0, 22);

        assertThat(imageDocument.get()).isEqualTo("brown jumps over the lazy dog.");
    }

    @Test
    public void addMasterDocumentRangeAddsRangeAfterLastContainedHidePosition() throws Exception {
        final String initialText = "The quick brown fox\njumps over the lazy dog.";
        final String shortenedText = "fox\njumps over the lazy dog.";
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                initialText);
        final PerspectivationDocument imageDocument = originAndImage.getRight();
        imageDocument.hideOriginDocumentRange(0, 10);
        imageDocument.removeMasterDocumentRange(10, 6);
        assertThat(imageDocument.get()).isEqualTo(shortenedText);

        imageDocument.addMasterDocumentRange(0, 13);

        assertThat(imageDocument.get()).isEqualTo("brofox\njumps over the lazy dog.");
    }

    // General tests

    @Test
    public void createImageDocumentShowingFullOriginText() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                "The quick brown fox\njumps over the lazy dog.");

        assertText(originAndImage, "The quick brown fox\njumps over the lazy dog.",
                "The quick brown fox\njumps over the lazy dog.");
        assertSegmentAndFragmentLinkedWithEachOther(originAndImage, 0, 44);
    }

    @Test
    public void removeOriginDocumentRangeFromImageDocument() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                "The quick brown fox\njumps over the lazy dog.");
        originAndImage.getRight().removeMasterDocumentRange(26, 5);

        final String fragment1 = "The quick brown fox\njumps ";
        final String fragment2 = "the lazy dog.";
        final String segment1 = "The quick brown fox\njumps ";
        final String segment2 = "the lazy dog.";
        assertText(originAndImage, fragment1 + "over " + fragment2, segment1 + segment2);

        final Fragment firstExpectedFragment = new Fragment(0, 26);
        final Segment firstExpectedSegment = createSegment(0, 26, firstExpectedFragment, false, false);
        firstExpectedFragment.segment = firstExpectedSegment;

        final Fragment secondExpectedFragment = new Fragment(31, 13);
        final Segment secondExpectedSegment = createSegment(26, 13, secondExpectedFragment, false, false);
        secondExpectedFragment.segment = secondExpectedSegment;

        assertSegments(originAndImage, firstExpectedSegment, secondExpectedSegment);
        assertFragments(originAndImage, firstExpectedFragment, secondExpectedFragment);
    }

    @Test
    public void addTextToOriginDocumentButNotToImageDocument() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                "The quick brown fox\njumps over the lazy dog.");
        originAndImage.getLeft().replace(0, 0, "Now: ");
        originAndImage.getRight().removeMasterDocumentRange(0, 5);

        final String fragment1 = "The quick brown fox\njumps over the lazy dog.";
        final String segment1 = "The quick brown fox\njumps over the lazy dog.";
        assertText(originAndImage, "Now: " + fragment1, segment1);
        final Fragment expectedFragment = new Fragment(5, 44);
        final Segment expectedSegment = createSegment(0, 44, expectedFragment, false, false);
        expectedFragment.segment = expectedSegment;

        assertSegments(originAndImage, expectedSegment);
        assertFragments(originAndImage, expectedFragment);
    }

    @Test
    public void addTextToBothDocumentsViaImageDocument() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                "The quick brown fox\njumps over the lazy dog.");
        originAndImage.getRight().replace(0, 0, "Now: ");

        assertText(originAndImage, "Now: The quick brown fox\njumps over the lazy dog.");
        assertSegmentAndFragmentLinkedWithEachOther(originAndImage, 0, 49);
    }

    @Test
    public void removeNewlineInOriginAndImageAndUpdateLineInformation() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
                "The quick brown fox\njumps over the lazy dog.");

        assertLineCount(originAndImage, 2);

        originAndImage.getRight().replace(10, 29, "no line");

        assertText(originAndImage, "The quick no line dog.");
        assertLineCount(originAndImage, 1);

        assertSegmentAndFragmentLinkedWithEachOther(originAndImage, 0, 22);
    }

}
