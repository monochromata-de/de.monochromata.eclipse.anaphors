package de.monochromata.eclipse.persp;

import static de.monochromata.eclipse.persp.ToLowerCasePosition.TO_LOWER_CASE_CATEGORY;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPositionCategoryException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.projection.Fragment;
import org.eclipse.jface.text.projection.Segment;

public interface PerspectivationDocumentTesting {

    String INITIAL_TEXT = "The quick brown fox\njumps over the lazy dog.";

    String FRAGMENTS_CATEGORY_PREFIX = "__fragmentsCategory";
    String SEGMENTS_CATEGORY = "__segmentsCategory";

    PerspectivationDocumentManager documentManager();

    default PerspectivationDocument createTestImageDocument() throws Exception {
        return createTestImageDocument(INITIAL_TEXT);
    }

    default PerspectivationDocument createTestImageDocument(final String text) throws BadLocationException {
        final Pair<Document, PerspectivationDocument> masterAndSlave = createOriginAndImageDocument(text);
        return masterAndSlave.getRight();
    }

    default Document createTestOriginDocument() throws Exception {
        return createTestOriginDocument(INITIAL_TEXT);
    }

    default Document createTestOriginDocument(final String text) throws BadLocationException {
        final Pair<Document, PerspectivationDocument> masterAndSlave = createOriginAndImageDocument(text);
        return masterAndSlave.getLeft();
    }

    default Pair<Document, PerspectivationDocument> createTestOriginAndImageDocument()
            throws BadLocationException {
        return createOriginAndImageDocument(INITIAL_TEXT);
    }

    default Pair<Document, PerspectivationDocument> createOriginAndImageDocument(final String initialText)
            throws BadLocationException {
        final Document originDocument = createOriginDocument(initialText);
        final PerspectivationDocument imageDocument = createImageDocumentShowingFullOriginText(initialText,
                originDocument);
        return new ImmutablePair<>(originDocument, imageDocument);
    }

    default Document createOriginDocument(final String initialText) {
        final Document originDocument = new Document();
        originDocument.set(initialText);
        return originDocument;
    }

    default PerspectivationDocument createImageDocumentShowingFullOriginText(final String initialText,
            final Document originDocument) throws BadLocationException {
        final PerspectivationDocument imageDocument = documentManager()
                .createSlaveDocument(originDocument);
        imageDocument.addMasterDocumentRange(0, initialText.length());
        return imageDocument;
    }

    default void addToLowerCases(final IDocument document, final List<ToLowerCasePosition> replacements) {
        replacements.forEach(position -> addToLowerCase(document, position));
    }

    default void addToLowerCase(final IDocument document, final ToLowerCasePosition position) {
        try {
            document.addPosition(TO_LOWER_CASE_CATEGORY, position);
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    default void assertText(final Pair<Document, PerspectivationDocument> originAndImage, final String expectedText) {
        assertText(originAndImage, expectedText, expectedText);
    }

    default void assertText(final Pair<Document, PerspectivationDocument> originAndImage,
            final String textExpectedInOrigin, final String textExpectedInImage) {
        assertThat(originAndImage.getLeft().get()).isEqualTo(textExpectedInOrigin);
        assertThat(originAndImage.getRight().get()).isEqualTo(textExpectedInImage);
    }

    default void assertLineCount(final Pair<Document, PerspectivationDocument> originAndImage,
            final int expectedLineCount) {
        assertLineCount(originAndImage, expectedLineCount, expectedLineCount);
    }

    default void assertLineCount(final Pair<Document, PerspectivationDocument> originAndImage,
            final int lineCountExpectedInMaster, final int lineCountExpectedInSlave) {
        assertThat(originAndImage.getLeft().getNumberOfLines()).isEqualTo(lineCountExpectedInMaster);
        assertThat(originAndImage.getRight().getNumberOfLines()).isEqualTo(lineCountExpectedInSlave);
    }

    default void assertSegments(final Pair<Document, PerspectivationDocument> originAndImage,
            final Position... expectedPositions) throws Exception {
        assertPositions(originAndImage.getRight(), SEGMENTS_CATEGORY, expectedPositions);
    }

    default void assertFragments(final Pair<Document, PerspectivationDocument> originAndImage,
            final Position... expectedPositions) throws Exception {
        assertPositions(originAndImage.getLeft(), getFragmentsCategory(originAndImage.getLeft()), expectedPositions);
    }

    default String getFragmentsCategory(final IDocument document) {
        return Arrays
                .stream(document.getPositionCategories())
                .filter(category -> category.startsWith(FRAGMENTS_CATEGORY_PREFIX))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(
                        "No fragments category in " + asList(document.getPositionCategories())));
    }

    default void assertSegmentAndFragmentLinkedWithEachOther(
            final Pair<Document, PerspectivationDocument> originAndImage, final int offset, final int length)
            throws Exception {
        assertSegmentAndFragmentLinkedWithEachOther(originAndImage, offset, length, offset, length);
    }

    default void assertSegmentAndFragmentLinkedWithEachOther(
            final Pair<Document, PerspectivationDocument> originAndImage, final int fragmentOffset,
            final int fragmentLength, final int segmentOffset, final int segmentLength) throws Exception {
        final Fragment expectedFragment = new Fragment(fragmentOffset, fragmentLength);
        final Segment expectedSegment = createSegment(segmentOffset, segmentLength, expectedFragment, false, false);
        expectedFragment.segment = expectedSegment;

        assertSegments(originAndImage, expectedSegment);
        assertFragments(originAndImage, expectedFragment);
    }

    default void assertPositions(final IDocument document, final String positionCategory,
            final Position... expectedPositions) throws BadPositionCategoryException {
        assertThat(document.getPositionCategories()).contains(positionCategory);
        assertPositions(positionCategory, document.getPositions(positionCategory), expectedPositions);
    }

    default void assertPositions(final String positionCategory, final Position[] positions,
            final Position[] expectedPositions) {
        assertThat(positions)
                .as("number of positions of category " + positionCategory)
                .hasSameSizeAs(expectedPositions);
        IntStream.range(0, positions.length).forEach(i -> assertPosition(i, positions[i], expectedPositions[i]));
    }

    default void assertPosition(final int i, final Position position, final Position expectedPosition) {
        if (position instanceof Segment && expectedPosition instanceof Segment) {
            assertSegment(i, (Segment) position, (Segment) expectedPosition);
        } else if (position instanceof Fragment && expectedPosition instanceof Fragment) {
            assertFragment(i, (Fragment) position, (Fragment) expectedPosition);
        } else {
            throw new IllegalArgumentException("Unknown position types " + position.getClass().getName() + " and "
                    + expectedPosition.getClass().getName());
        }
    }

    default void assertSegment(final int i, final Segment position, final Segment expectedPosition) {
        assertThat(position).as("check segment[" + i + "]").isEqualTo(expectedPosition);
        assertThat(position.fragment).as("check segment[" + i + "].fragment").isEqualTo(expectedPosition.fragment);
        assertThat(position.isMarkedForStretch)
                .as("check segment[" + i + "].isMarkedForStretch")
                .isEqualTo(expectedPosition.isMarkedForStretch);
        assertThat(position.isMarkedForShift)
                .as("check segment[" + i + "].isMarkedForShift")
                .isEqualTo(expectedPosition.isMarkedForShift);
    }

    default void assertFragment(final int i, final Fragment position, final Fragment expectedPosition) {
        assertThat(position).as("check fragement[" + i + "]").isEqualTo(expectedPosition);
        assertThat(position.segment).as("check fragement[" + i + "].segment").isEqualTo(expectedPosition.segment);
    }

    default Segment createSegment(final int offset, final int length, final Fragment fragment,
            final boolean isMarkedForStretch, final boolean isMarkedForShift) {
        final Segment segment = new Segment(offset, length);
        segment.fragment = fragment;
        segment.isMarkedForStretch = isMarkedForStretch;
        segment.isMarkedForShift = isMarkedForShift;
        return segment;
    }

    default DocumentEvent createOriginEvent(final int offsetInOrigin, final int length) {
        final DocumentEvent originEvent = new DocumentEvent();
        originEvent.fOffset = offsetInOrigin;
        originEvent.fLength = length;
        return originEvent;
    }

}
