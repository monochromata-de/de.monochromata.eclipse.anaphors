package de.monochromata.eclipse.persp;

import static de.monochromata.eclipse.persp.HidePosition.HIDE_CATEGORY;
import static de.monochromata.eclipse.persp.PerspectivationPosition.PERSPECTIVATION_CATEGORY;
import static de.monochromata.eclipse.persp.ToLowerCasePosition.TO_LOWER_CASE_CATEGORY;
import static de.monochromata.eclipse.position.DoublyLinkedPosition.link;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.junit.Test;

import de.monochromata.anaphors.perspectivation.Perspectivation;
import de.monochromata.eclipse.anaphors.position.PositionsTesting;

public class PerspectivationPositionTest implements PositionsTesting {

	private final IDocument originDocument = new Document();
	private final PerspectivationDocumentManager manager = new PerspectivationDocumentManager();
	private final PerspectivationDocument document = manager.createSlaveDocument(originDocument);

	@Test
	public void isEnabledByDefault() {
		assertThat(new PerspectivationPosition(0, 10).isEnabled()).isTrue();
	}

	@Test
	public void setEnabledTogglesIsEnabled() throws BadLocationException {
		final PerspectivationPosition position = new PerspectivationPosition(0, 1);
		assertThat(position.isEnabled()).isTrue();

		position.setEnabled(false, document);

		assertThat(position.isEnabled()).isFalse();
	}

	@Test
	public void setEnabledPerformsPerspectivation() throws Exception {
		final String initialText = "The Quick Brown Fox Is Slow Today.";
		document.set(initialText);

		final PerspectivationPosition position = new PerspectivationPosition(4, 12, unused -> true,
				asList(new Perspectivation.ToLowerCase(6), new Perspectivation.Hide(0, 6)));

		position.notify(new DocumentEvent(), document);
		position.undoPerspectivation();

		assertThat(document.get()).isEqualTo("The Quick Brown Fox Is Slow Today.");

		final Field isEnabledField = PerspectivationPosition.class.getDeclaredField("isEnabled");
		isEnabledField.setAccessible(true);
		isEnabledField.set(position, false);

		position.setEnabled(true, document);

		assertThat(document.get()).isEqualTo("The brown Fox Is Slow Today.");
	}

	@Test
	public void setEnabledDoesNotPerformPerspectivationTwice() throws Exception {
		final String initialText = "The Quick Brown Fox Is Slow Today.";
		document.set(initialText);

		final PerspectivationPosition position = new PerspectivationPosition(4, 12, unused -> true,
				asList(new Perspectivation.ToLowerCase(6), new Perspectivation.Hide(0, 6)));

		position.notify(new DocumentEvent(), document);
		assertThat(document.get()).isEqualTo("The brown Fox Is Slow Today.");

		position.setEnabled(true, document);

		assertThat(document.get()).isEqualTo("The brown Fox Is Slow Today.");
	}

	@Test
	public void setEnabledUndoesPerspectivation() throws BadLocationException {
		final String initialText = "The Quick Brown Fox Is Slow Today.";
		document.set(initialText);

		final PerspectivationPosition position = new PerspectivationPosition(4, 12, unused -> true,
				asList(new Perspectivation.ToLowerCase(6), new Perspectivation.Hide(0, 6)));

		position.notify(new DocumentEvent(), document);
		assertThat(document.get()).isEqualTo("The brown Fox Is Slow Today.");
		position.setEnabled(false, document);

		assertThat(position.isEnabled()).isFalse();
		assertThat(document.get()).isEqualTo("The Quick Brown Fox Is Slow Today.");
	}

	@Test
	public void setEnabledDoesNotUndoPerspectivationTwice() throws BadLocationException {
		final String initialText = "The Quick Brown Fox Is Slow Today.";
		document.set(initialText);

		final PerspectivationPosition position = new PerspectivationPosition(4, 12, unused -> true,
				asList(new Perspectivation.ToLowerCase(6), new Perspectivation.Hide(0, 6)));

		position.notify(new DocumentEvent(), document);
		position.undoPerspectivation();

		assertThat(document.get()).isEqualTo("The Quick Brown Fox Is Slow Today.");
		assertThat(position.isEnabled()).isTrue();

		position.setEnabled(false, document);

		assertThat(position.isEnabled()).isFalse();
		assertThat(document.get()).isEqualTo("The Quick Brown Fox Is Slow Today.");
	}

	@Test
	public void conditionHasBeenMetReturnsFalseInitially() {
		assertThat(new PerspectivationPosition(0, 1).conditionHasBeenMet()).isFalse();
	}

	@Test
	public void conditionHasBeenMetReturnsTrueAfterNotificationOfDisabledPosition() throws BadLocationException {
		final PerspectivationPosition position = new PerspectivationPosition(event -> true, emptyList());
		position.setEnabled(false, document);

		position.notify(new DocumentEvent(), document);

		assertThat(position.conditionHasBeenMet()).isTrue();
	}

	@Test
	public void conditionHasBeenMetReturnsTrueAfterSuccessfulPerspectivation() throws BadLocationException {
		final PerspectivationPosition position = new PerspectivationPosition(unused -> true, emptyList());

		position.notify(new DocumentEvent(), document);

		assertThat(position.conditionHasBeenMet()).isTrue();
	}

	@Test
	public void isPerspectivisedReturnsTrueAfterFulfilledPerspectivationPredicate() throws Exception {
		final PerspectivationPosition position = new PerspectivationPosition(unused -> true, emptyList());
		assertThat(position.isPerspectivated()).isFalse();

		position.notify(new DocumentEvent(), document);

		assertThat(position.isPerspectivated()).isTrue();
	}

	@Test
	public void isPerspectivisedReturnsFalseAfterNonMatchingPerspectivationPredicate() throws Exception {
		final PerspectivationPosition position = new PerspectivationPosition(unused -> false, emptyList());
		assertThat(position.isPerspectivated()).isFalse();

		position.notify(new DocumentEvent(), document);

		assertThat(position.isPerspectivated()).isFalse();
	}

	@Test
	public void perspectivationIsPerformedIfConditionIsFulfilled() throws Exception {
		final AtomicBoolean performed = new AtomicBoolean(false);
		final PerspectivationPosition position = new PerspectivationPosition(unused -> true,
				new ListForTestingForEach<>(() -> performed.set(true)));

		position.notify(new DocumentEvent(), document);

		assertThat(performed.get()).isTrue();
	}

	@Test
	public void perspectivationIsNotPerformedIfConditionIsNotFulfilled() throws Exception {
		final AtomicBoolean performed = new AtomicBoolean(false);
		final PerspectivationPosition position = new PerspectivationPosition(unused -> false,
				new ListForTestingForEach<>(() -> performed.set(true)));

		position.notify(new DocumentEvent(), document);

		assertThat(performed.get()).isFalse();
	}

	@Test
	public void perspectivationIsNotPerformedIfPositionIsDisabled() throws Exception {
		final AtomicBoolean performed = new AtomicBoolean(false);
		final PerspectivationPosition position = new PerspectivationPosition(unused -> true,
				new ListForTestingForEach<>(() -> performed.set(true)));
		position.setEnabled(false, document);

		position.notify(new DocumentEvent(), document);

		assertThat(performed.get()).isFalse();
	}

	@Test
	public void conditionalPerspectivationIsNotCheckedOncePerspectivationHasBeenPerformed() throws Exception {
		final AtomicInteger perspectivationCount = new AtomicInteger(0);
		final PerspectivationPosition position = new PerspectivationPosition(unused -> true,
				new ListForTestingForEach<>(() -> perspectivationCount.incrementAndGet()));

		position.notify(new DocumentEvent(), document);
		assertThat(perspectivationCount.get()).isEqualTo(1);
		position.notify(new DocumentEvent(), document);

		assertThat(perspectivationCount.get()).isEqualTo(1);
	}

	@Test
	public void perspectivationPerformsToLowerCase() throws Exception {
		final String initialText = "The Quick Brown Fox Is Slow Today.";
		document.set(initialText);

		final PerspectivationPosition position = new PerspectivationPosition(4, 12, unused -> true,
				asList(new Perspectivation.ToLowerCase(6)));

		position.notify(new DocumentEvent(), document);
		assertThat(document.get()).isEqualTo("The Quick brown Fox Is Slow Today.");
	}

	@Test
	public void perspectivationSavesToLowerCasePositionInPerspectivationPosition() throws Exception {
		document.set("The Quick Brown Fox Is Slow Today.");
		final PerspectivationPosition position = new PerspectivationPosition(4, 12, unused -> true,
				asList(new Perspectivation.ToLowerCase(6)));

		position.notify(new DocumentEvent(), document);

		assertThat(position.getToLowerCasePositions()).containsExactly(new ToLowerCasePosition(10));
	}

	@Test
	public void perspectivationPerformsHide() throws Exception {
		final String initialText = "The Quick Brown Fox Is Slow Today.";
		document.set(initialText);

		final PerspectivationPosition position = new PerspectivationPosition(4, 12, unused -> true,
				asList(new Perspectivation.Hide(0, 6)));

		position.notify(new DocumentEvent(), document);
		assertThat(document.get()).isEqualTo("The Brown Fox Is Slow Today.");
	}

	@Test
	public void perspectivationSavesHidePositionInPerspectivationPosition() throws Exception {
		document.set("The Quick Brown Fox Is Slow Today.");
		final PerspectivationPosition position = new PerspectivationPosition(4, 12, unused -> true,
				asList(new Perspectivation.Hide(0, 6)));

		position.notify(new DocumentEvent(), document);

		assertThat(position.getHidePositions()).containsExactly(new HidePosition(4, 6));
	}

	@Test
	public void perspectivationPerformsToLowerCaseAndHide() throws Exception {
		final String initialText = "The Quick Brown Fox Is Slow Today.";
		document.set(initialText);

		final PerspectivationPosition position = new PerspectivationPosition(4, 12, unused -> true,
				asList(new Perspectivation.ToLowerCase(6), new Perspectivation.Hide(0, 6)));

		position.notify(new DocumentEvent(), document);
		assertThat(document.get()).isEqualTo("The brown Fox Is Slow Today.");
	}

	@Test
	public void perspectivationPerformsHideAndToLowerCase() throws Exception {
		final String initialText = "The Quick Brown Fox Is Slow Today.";
		document.set(initialText);

		final PerspectivationPosition position = new PerspectivationPosition(4, 12, unused -> true,
				asList(new Perspectivation.Hide(0, 6), new Perspectivation.ToLowerCase(6)));

		position.notify(new DocumentEvent(), document);
		assertThat(document.get()).isEqualTo("The brown Fox Is Slow Today.");
	}

	@Test
	public void undoDoesNothingIfNotYetPerspectivated() throws Exception {
		final PerspectivationPosition position = new PerspectivationPosition(null, null);

		// Should not throw a NullPointerException since no action should be
		// taken.
		position.undoPerspectivation();
	}

	/**
	 * This case is assumed due to a delay execution of setEnabled(false) that
	 * already modified this.isEnabled, but not yet isPerspectivated.
	 */
	@Test
	public void undoDoesNothingIfDisabledAfterPerspectivation() throws Exception {
		final String initialText = "The Quick Brown Fox Is Slow Today.";
		document.set(initialText);

		final PerspectivationPosition position = new PerspectivationPosition(4, 12, unused -> true,
				asList(new Perspectivation.ToLowerCase(6), new Perspectivation.Hide(0, 6)));

		position.notify(new DocumentEvent(), document);

		final Field isEnabledField = PerspectivationPosition.class.getDeclaredField("isEnabled");
		isEnabledField.setAccessible(true);
		isEnabledField.set(position, false);

		position.undoPerspectivation();

		assertThat(document.get()).isEqualTo("The brown Fox Is Slow Today.");
	}

	/**
	 * All perspectivations are removed, even those not added by the position
	 */
	@Test
	public void undoPerspectivationRemovesAllPerspectivations() throws Exception {
		final String initialText = "The Quick Brown Fox Is Slow Today.";
		document.set(initialText);

		final PerspectivationPosition position = new PerspectivationPosition(4, 12, unused -> true,
				asList(new Perspectivation.ToLowerCase(6), new Perspectivation.Hide(0, 6)));

		position.notify(new DocumentEvent(), document);
		position.undoPerspectivation();

		assertThat(document.get()).isEqualTo(initialText);
	}

	@Test
	public void undoRemovesToLowerCasePositionFromDocument() throws Exception {
		document.set("The Quick Brown Fox Is Slow Today.");
		final PerspectivationPosition position = new PerspectivationPosition(4, 12, unused -> true,
				asList(new Perspectivation.ToLowerCase(6)));
		position.notify(new DocumentEvent(), document);

		position.undoPerspectivation();

		assertThat(document.getMasterDocument().getPositions(TO_LOWER_CASE_CATEGORY)).isEmpty();
	}

	@Test
	public void undoRemovesHidePositionFromDocument() throws Exception {
		document.set("The Quick Brown Fox Is Slow Today.");
		final PerspectivationPosition position = new PerspectivationPosition(4, 12, unused -> true,
				asList(new Perspectivation.Hide(0, 6)));
		position.notify(new DocumentEvent(), document);

		position.undoPerspectivation();

		assertThat(document.getMasterDocument().getPositions(HIDE_CATEGORY)).isEmpty();
	}

	@Test
	public void undoRemovesToLowerCasePositionFromPerspectivationPosition() throws Exception {
		document.set("The Quick Brown Fox Is Slow Today.");
		final PerspectivationPosition position = new PerspectivationPosition(4, 12, unused -> true,
				asList(new Perspectivation.ToLowerCase(6)));
		position.notify(new DocumentEvent(), document);

		position.undoPerspectivation();

		assertThat(position.getToLowerCasePositions()).isEmpty();
	}

	@Test
	public void undoRemovesHidePositionFromPerspectivationPosition() throws Exception {
		document.set("The Quick Brown Fox Is Slow Today.");
		final PerspectivationPosition position = new PerspectivationPosition(4, 12, unused -> true,
				asList(new Perspectivation.Hide(0, 6)));
		position.notify(new DocumentEvent(), document);

		position.undoPerspectivation();

		assertThat(position.getHidePositions()).isEmpty();
	}

	@Test
	public void isPerspectivatedReturnsFalseAfterUndo() throws Exception {
		final String initialText = "The Quick Brown Fox Is Slow Today.";
		document.set(initialText);

		final PerspectivationPosition position = new PerspectivationPosition(4, 12, unused -> true, emptyList());

		position.notify(new DocumentEvent(), document);
		position.undoPerspectivation();

		assertThat(position.isPerspectivated()).isFalse();
	}

	@Test
	public void perspectivationCanBeRepeatedAfterUndo() throws Exception {
		final String initialText = "The Quick Brown Fox Is Slow Today.";
		document.set(initialText);

		final PerspectivationPosition position = new PerspectivationPosition(4, 12, unused -> true,
				asList(new Perspectivation.ToLowerCase(6), new Perspectivation.Hide(0, 6)));

		position.notify(new DocumentEvent(), document);
		assertThat(document.get()).isEqualTo("The brown Fox Is Slow Today.");

		position.undoPerspectivation();

		// Repeated perspectivation
		position.notify(new DocumentEvent(), document);
		assertThat(document.get()).isEqualTo("The brown Fox Is Slow Today.");
	}

	@Test
	public void documentRetainsToLowerCaseOfUnaffectedPositionOnDeletion() throws Exception {
		document.set("The Quick Brown Fox Is Slow Today.");
		final PerspectivationPosition position = new PerspectivationPosition(19, 5, event -> true,
				asList(new Perspectivation.ToLowerCase(0)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, position);
		position.notify(new DocumentEvent(document, 0, 0, ""), document);
		assertThat(document.getMasterDocument().getPositions(TO_LOWER_CASE_CATEGORY))
				.containsExactly(new ToLowerCasePosition(19));

		document.replace(4, 12, "");

		assertThat(document.getMasterDocument().getPositions(TO_LOWER_CASE_CATEGORY))
				.containsExactly(new ToLowerCasePosition(7));
	}

	// Removing to lower case and hidden document range for current position

	@Test
	public void documentRemovesToLowerCaseOfCurrentPositionOnDeletion() throws Exception {
		document.set("The Quick Brown Fox Is Slow Today.");
		final PerspectivationPosition position = new PerspectivationPosition(4, 12, event -> true,
				asList(new Perspectivation.ToLowerCase(6)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, position);
		position.notify(new DocumentEvent(document, 0, 0, ""), document);
		assertThat(document.getMasterDocument().getPositions(TO_LOWER_CASE_CATEGORY))
				.containsExactly(new ToLowerCasePosition(10));

		document.replace(4, 12, "");

		assertThat(document.getMasterDocument().getPositions(TO_LOWER_CASE_CATEGORY)).isEmpty();
	}

	/**
	 * It is assumed that hidden content was generated and is potentially invalid
	 * when the perspectivation is undone.
	 */
	@Test
	public void updaterRemovesHiddenDocumentRangeFromMasterDocumentForCurrentPositionOnDeletion() throws Exception {
		document.set("The Quick Brown Fox Is Slow Today.");
		final PerspectivationPosition position = new PerspectivationPosition(4, 12, event -> true,
				asList(new Perspectivation.Hide(0, 6)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, position);
		position.notify(new DocumentEvent(document, 0, 0, ""), document); // trigger application of perspectivations
		assertThat(originDocument.getPositions(HIDE_CATEGORY)).containsExactly(new HidePosition(4, 6));
		assertThat(originDocument.get()).isEqualTo("The Quick Brown Fox Is Slow Today.");
		assertThat(document.get()).isEqualTo("The Brown Fox Is Slow Today.");

		originDocument.replace(4, 12, "");

		assertThat(originDocument.getPositions(HIDE_CATEGORY)).isEmpty();
		assertThat(originDocument.get()).isEqualTo("The Fox Is Slow Today.");
		assertThat(document.get()).isEqualTo("The Fox Is Slow Today.");
	}

	// Removing to lower case and hidden document range for previous position

	/**
	 * It is assumed that hidden content was generated and is potentially invalid
	 * when the perspectivation is undone.
	 */
	@Test
	public void updaterInlinesToLowerCaseOfPreviousPositionIntoOriginDocumentOnDeletion() throws Exception {
		final String initialText = "The Quick Brown Fox Is Slow Today.";
		document.set(initialText);

		final PerspectivationPosition previousPosition = new PerspectivationPosition(4, 12, event -> true,
				asList(new Perspectivation.ToLowerCase(6)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, previousPosition);
		previousPosition.notify(new DocumentEvent(document, 0, 0, ""), document);

		final PerspectivationPosition currentPosition = new PerspectivationPosition(16, 3, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, currentPosition);
		link(previousPosition, currentPosition);

		assertThat(originDocument.getPositions(TO_LOWER_CASE_CATEGORY)).containsExactly(new ToLowerCasePosition(10));
		assertThat(originDocument.get()).isEqualTo(initialText);
		assertThat(document.get()).isEqualTo("The Quick brown Fox Is Slow Today.");

		document.replace(16, 3, "");

		assertThat(originDocument.getPositions(TO_LOWER_CASE_CATEGORY)).isEmpty();
		final String lowerCasedShortenedText = "The Quick brown  Is Slow Today.";
		assertThat(originDocument.get()).isEqualTo(lowerCasedShortenedText);
		assertThat(document.get()).isEqualTo(lowerCasedShortenedText);
	}

	@Test
	public void updaterDoesNotInlineToLowerCaseOfPreviousPositionIntoOriginDocumentOnDeletionIfThereAreSiblings()
			throws Exception {
		final String initialText = "The Quick Brown Fox Is Slow Today.";
		document.set(initialText);

		final PerspectivationPosition previousPosition = new PerspectivationPosition(4, 12, event -> true,
				asList(new Perspectivation.ToLowerCase(6)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, previousPosition);
		previousPosition.notify(new DocumentEvent(document, 0, 0, ""), document);

		final PerspectivationPosition currentPosition = new PerspectivationPosition(16, 3, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, currentPosition);
		link(previousPosition, currentPosition);

		final PerspectivationPosition siblingPosition = new PerspectivationPosition(20, 2, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, siblingPosition);
		link(previousPosition, siblingPosition);

		assertThat(originDocument.getPositions(TO_LOWER_CASE_CATEGORY)).containsExactly(new ToLowerCasePosition(10));
		assertThat(originDocument.get()).isEqualTo(initialText);
		assertThat(document.get()).isEqualTo("The Quick brown Fox Is Slow Today.");

		document.replace(16, 3, "");

		assertThat(originDocument.getPositions(TO_LOWER_CASE_CATEGORY)).containsExactly(new ToLowerCasePosition(10));
		assertThat(originDocument.get()).isEqualTo("The Quick Brown  Is Slow Today.");
		assertThat(document.get()).isEqualTo("The Quick brown  Is Slow Today.");
	}

	/**
	 * It is assumed that hidden content was generated and is potentially invalid
	 * when the perspectivation is undone.
	 */
	@Test
	public void updaterRemovesHiddenDocumentRangeFromMasterDocumentForPreviousPositionOnDeletion() throws Exception {
		document.set("The Quick Brown Fox Is Slow Today.");
		final int hideLength = 6;
		final PerspectivationPosition previousPosition = new PerspectivationPosition(4, 12, event -> true,
				asList(new Perspectivation.Hide(0, hideLength)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, previousPosition);

		final PerspectivationPosition currentPosition = new PerspectivationPosition(16, 3, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, currentPosition);
		link(previousPosition, currentPosition);

		previousPosition.notify(new DocumentEvent(document, 0, 0, ""), document);

		assertThat(originDocument.getPositions(HIDE_CATEGORY)).containsExactly(new HidePosition(4, hideLength));

		document.replace(16 - hideLength, 3, "");

		assertThat(originDocument.getPositions(HIDE_CATEGORY)).isEmpty();
		assertThat(originDocument.get()).isEqualTo("The Brown  Is Slow Today.");
		assertThat(document.get()).isEqualTo("The Brown  Is Slow Today.");
	}

	@Test
	public void updaterDoesNotRemoveHiddenDocumentRangeFromMasterDocumentForPreviousPositionOnDeletionIfThereAreSiblings()
			throws Exception {
		document.set("The Quick Brown Fox Is Slow Today.");
		final int hideLength = 6;
		final PerspectivationPosition previousPosition = new PerspectivationPosition(4, 12, event -> true,
				asList(new Perspectivation.Hide(0, hideLength)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, previousPosition);

		final PerspectivationPosition currentPosition = new PerspectivationPosition(16, 3, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, currentPosition);
		link(previousPosition, currentPosition);

		final PerspectivationPosition siblingPosition = new PerspectivationPosition(20, 2, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, siblingPosition);
		link(previousPosition, siblingPosition);

		previousPosition.notify(new DocumentEvent(document, 0, 0, ""), document);

		assertThat(originDocument.getPositions(HIDE_CATEGORY)).containsExactly(new HidePosition(4, hideLength));

		document.replace(16 - hideLength, 3, "");

		assertThat(originDocument.getPositions(HIDE_CATEGORY)).containsExactly(new HidePosition(4, hideLength));
		assertThat(originDocument.get()).isEqualTo("The Quick Brown  Is Slow Today.");
		assertThat(document.get()).isEqualTo("The Brown  Is Slow Today.");
	}

	@Test
	public void updaterRemovesPreviousPositionOnDeletion() throws Exception {
		document.set("The Quick Brown Fox Is Slow Today.");
		final PerspectivationPosition previousPosition = new PerspectivationPosition(4, 12, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, previousPosition);

		final PerspectivationPosition currentPosition = new PerspectivationPosition(16, 3, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, currentPosition);
		link(previousPosition, currentPosition);

		assertThat(originDocument.getPositions(PERSPECTIVATION_CATEGORY)).hasSize(2);

		document.replace(16, 3, "");

		assertThat(originDocument.getPositions(PERSPECTIVATION_CATEGORY)).isEmpty();
	}

	@Test
	public void updaterDoesNotRemovePreviousPositionOnDeletionIfThereAreSiblings() throws Exception {
		document.set("The Quick Brown Fox Is Slow Today.");
		final PerspectivationPosition previousPosition = new PerspectivationPosition(4, 12, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, previousPosition);

		final PerspectivationPosition currentPosition = new PerspectivationPosition(16, 3, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, currentPosition);
		link(previousPosition, currentPosition);

		final PerspectivationPosition siblingPosition = new PerspectivationPosition(20, 2, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, siblingPosition);
		link(previousPosition, siblingPosition);

		assertThat(originDocument.getPositions(PERSPECTIVATION_CATEGORY)).hasSize(3);

		document.replace(16, 3, "");

		assertThat(originDocument.getPositions(PERSPECTIVATION_CATEGORY)).containsExactly(previousPosition,
				siblingPosition);
	}

	// Removing to lower case and hidden document range for next position

	/**
	 * It is assumed that hidden content was generated and is potentially invalid
	 * when the perspectivation is undone.
	 */
	@Test
	public void updaterInlinesToLowerCaseOfNextPositionIntoOriginDocumentOnDeletion() throws Exception {
		final String initialText = "The Quick Brown Fox Is Slow Today.";
		document.set(initialText);

		final PerspectivationPosition currentPosition = new PerspectivationPosition(0, 3, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, currentPosition);

		final PerspectivationPosition nextPosition = new PerspectivationPosition(4, 12, event -> true,
				asList(new Perspectivation.ToLowerCase(6)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, nextPosition);
		nextPosition.notify(new DocumentEvent(document, 0, 0, ""), document);
		link(currentPosition, nextPosition);

		assertThat(originDocument.getPositions(TO_LOWER_CASE_CATEGORY)).containsExactly(new ToLowerCasePosition(10));
		assertThat(originDocument.get()).isEqualTo(initialText);
		assertThat(document.get()).isEqualTo("The Quick brown Fox Is Slow Today.");

		document.replace(0, 3, "");

		final String lowerCasedAndShortenedText = " Quick brown Fox Is Slow Today.";
		assertThat(originDocument.getPositions(TO_LOWER_CASE_CATEGORY)).isEmpty();
		assertThat(originDocument.get()).isEqualTo(lowerCasedAndShortenedText);
		assertThat(document.get()).isEqualTo(lowerCasedAndShortenedText);
	}

	@Test
	public void updaterInlinesToLowerCasesOfNextPositionsIntoOriginDocumentOnDeletion() throws Exception {
		final String initialText = "The Quick Brown Fox Is Slow Today.";
		document.set(initialText);

		final PerspectivationPosition currentPosition = new PerspectivationPosition(0, 3, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, currentPosition);

		final PerspectivationPosition nextPosition1 = new PerspectivationPosition(4, 6, event -> true,
				asList(new Perspectivation.ToLowerCase(0)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, nextPosition1);
		nextPosition1.notify(new DocumentEvent(document, 0, 0, ""), document);
		link(currentPosition, nextPosition1);

		final PerspectivationPosition nextPosition2 = new PerspectivationPosition(10, 6, event -> true,
				asList(new Perspectivation.ToLowerCase(0)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, nextPosition2);
		nextPosition2.notify(new DocumentEvent(document, 0, 0, ""), document);
		link(currentPosition, nextPosition2);

		assertThat(originDocument.getPositions(TO_LOWER_CASE_CATEGORY)).containsExactly(new ToLowerCasePosition(4),
				new ToLowerCasePosition(10));
		assertThat(originDocument.get()).isEqualTo(initialText);
		assertThat(document.get()).isEqualTo("The quick brown Fox Is Slow Today.");

		document.replace(0, 3, "");

		final String lowerCasedAndShortenedText = " quick brown Fox Is Slow Today.";
		assertThat(originDocument.getPositions(TO_LOWER_CASE_CATEGORY)).isEmpty();
		assertThat(originDocument.get()).isEqualTo(lowerCasedAndShortenedText);
		assertThat(document.get()).isEqualTo(lowerCasedAndShortenedText);
	}

	/**
	 * It is assumed that hidden content was generated and is potentially invalid
	 * when the perspectivation is undone.
	 */
	@Test
	public void updaterRemovesHiddenDocumentRangeFromMasterDocumentForNextPositionOnDeletion() throws Exception {
		document.set("The Quick Brown Fox Is Slow Today.");
		final PerspectivationPosition currentPosition = new PerspectivationPosition(4, 5, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, currentPosition);

		final int hideLength = 3;
		final PerspectivationPosition nextPosition = new PerspectivationPosition(16, 3, event -> true,
				asList(new Perspectivation.Hide(0, hideLength)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, nextPosition);
		link(currentPosition, nextPosition);
		nextPosition.notify(new DocumentEvent(document, 0, 0, ""), document);

		assertThat(originDocument.getPositions(HIDE_CATEGORY)).containsExactly(new HidePosition(16, hideLength));

		// This replacement triggers a registerPostNotificationReplace with the outdated
		// next position. However, the next position is updated before the post
		// notification replace is executed - then with the updated range.
		document.replace(4, 5, "");

		assertThat(originDocument.getPositions(HIDE_CATEGORY)).isEmpty();
		assertThat(originDocument.get()).isEqualTo("The  Brown  Is Slow Today.");
		assertThat(document.get()).isEqualTo("The  Brown  Is Slow Today.");
	}

	@Test
	public void updaterRemovesHiddenDocumentRangesFromMasterDocumentForNextPositionsOnDeletion() throws Exception {
		document.set("The Quick Brown Fox Is Slow Today.");
		final PerspectivationPosition currentPosition = new PerspectivationPosition(4, 5, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, currentPosition);

		final int hideLength1 = 3;
		final PerspectivationPosition nextPosition1 = new PerspectivationPosition(16, hideLength1, event -> true,
				asList(new Perspectivation.Hide(0, hideLength1)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, nextPosition1);
		link(currentPosition, nextPosition1);
		nextPosition1.notify(new DocumentEvent(document, 0, 0, ""), document);

		final int hideLength2 = 2;
		final PerspectivationPosition nextPosition2 = new PerspectivationPosition(20, hideLength2, event -> true,
				asList(new Perspectivation.Hide(0, hideLength2)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, nextPosition2);
		link(currentPosition, nextPosition2);
		nextPosition2.notify(new DocumentEvent(document, 0, 0, ""), document);

		assertThat(originDocument.getPositions(HIDE_CATEGORY)).containsExactly(new HidePosition(16, hideLength1),
				new HidePosition(20, hideLength2));

		// This replacement triggers a registerPostNotificationReplace with the outdated
		// next positions. However, the next positions are updated before the post
		// notification replace is executed - then with the updated range.
		document.replace(4, 5, "");

		assertThat(originDocument.getPositions(HIDE_CATEGORY)).isEmpty();
		assertThat(originDocument.get()).isEqualTo("The  Brown   Slow Today.");
		assertThat(document.get()).isEqualTo("The  Brown   Slow Today.");
	}

	@Test
	public void updaterRemovesBothCurrentAndNextPositionOnDeletionWithoutException() throws Exception {
		document.set("The Quick Brown Fox Is Slow Today.");
		final PerspectivationPosition currentPositionPosition = new PerspectivationPosition(4, 5, event -> true,
				emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, currentPositionPosition);

		final int hideLength = 3;
		final PerspectivationPosition nextPosition = new PerspectivationPosition(16, hideLength, event -> true,
				asList(new Perspectivation.Hide(0, hideLength)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, nextPosition);
		link(currentPositionPosition, nextPosition);
		nextPosition.notify(new DocumentEvent(document, 0, 0, ""), document);

		assertThat(originDocument.getPositions(HIDE_CATEGORY)).containsExactly(new HidePosition(16, hideLength));
		assertThat(originDocument.getPositions(PERSPECTIVATION_CATEGORY)).hasSize(2);

		document.replace(4, 15, "");

		assertThat(originDocument.getPositions(HIDE_CATEGORY)).isEmpty();
		assertThat(originDocument.getPositions(PERSPECTIVATION_CATEGORY)).isEmpty();
		assertThat(originDocument.get()).isEqualTo("The  Slow Today.");
		assertThat(document.get()).isEqualTo("The  Slow Today.");
	}

	@Test
	public void updaterRemovesBothCurrentAndNextPositionsOnDeletionWithoutException() throws Exception {
		document.set("The Quick Brown Fox Is Slow Today.");
		final PerspectivationPosition currentPositionPosition = new PerspectivationPosition(4, 5, event -> true,
				emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, currentPositionPosition);

		final int hideLength1 = 3;
		final PerspectivationPosition nextPosition1 = new PerspectivationPosition(16, hideLength1, event -> true,
				asList(new Perspectivation.Hide(0, hideLength1)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, nextPosition1);
		link(currentPositionPosition, nextPosition1);
		nextPosition1.notify(new DocumentEvent(document, 0, 0, ""), document);

		final int hideLength2 = 2;
		final PerspectivationPosition nextPosition2 = new PerspectivationPosition(20, hideLength2, event -> true,
				asList(new Perspectivation.Hide(0, hideLength2)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, nextPosition2);
		link(currentPositionPosition, nextPosition2);
		nextPosition2.notify(new DocumentEvent(document, 0, 0, ""), document);

		assertThat(originDocument.getPositions(HIDE_CATEGORY)).containsExactly(new HidePosition(16, hideLength1),
				new HidePosition(20, hideLength2));
		assertThat(originDocument.getPositions(PERSPECTIVATION_CATEGORY)).hasSize(3);

		document.replace(4, 18, "");

		assertThat(originDocument.getPositions(HIDE_CATEGORY)).isEmpty();
		assertThat(originDocument.getPositions(PERSPECTIVATION_CATEGORY)).isEmpty();
		assertThat(originDocument.get()).isEqualTo("The  Today.");
		assertThat(document.get()).isEqualTo("The  Today.");
	}

	@Test
	public void updaterRemovesNextPositionOnDeletion() throws Exception {
		document.set("The Quick Brown Fox Is Slow Today.");
		final PerspectivationPosition previousPosition = new PerspectivationPosition(4, 5, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, previousPosition);

		final PerspectivationPosition currentPosition = new PerspectivationPosition(16, 3, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, currentPosition);
		link(previousPosition, currentPosition);

		assertThat(originDocument.getPositions(PERSPECTIVATION_CATEGORY)).hasSize(2);

		document.replace(4, 5, "");

		assertThat(originDocument.getPositions(PERSPECTIVATION_CATEGORY)).isEmpty();
	}

	@Test
	public void updaterRemovesNextPositionsOnDeletion() throws Exception {
		document.set("The Quick Brown Fox Is Slow Today.");
		final PerspectivationPosition currentPosition = new PerspectivationPosition(4, 5, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, currentPosition);

		final PerspectivationPosition nextPosition1 = new PerspectivationPosition(16, 3, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, nextPosition1);
		link(currentPosition, nextPosition1);

		final PerspectivationPosition nextPosition2 = new PerspectivationPosition(20, 3, event -> true, emptyList());
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, nextPosition2);
		link(currentPosition, nextPosition2);

		assertThat(originDocument.getPositions(PERSPECTIVATION_CATEGORY)).hasSize(3);

		document.replace(4, 5, "");

		assertThat(originDocument.getPositions(PERSPECTIVATION_CATEGORY)).isEmpty();
	}

	private static class ListForTestingForEach<T> extends ArrayList<T> {
		private static final long serialVersionUID = 7919732490022687893L;
		private final Runnable runnableNotifiedOfForEach;

		public ListForTestingForEach(final Runnable runnableNotifiedOfForEach) {
			this.runnableNotifiedOfForEach = runnableNotifiedOfForEach;
		}

		@Override
		public void forEach(final Consumer<? super T> action) {
			runnableNotifiedOfForEach.run();
			super.forEach(action);
		}
	}

}
