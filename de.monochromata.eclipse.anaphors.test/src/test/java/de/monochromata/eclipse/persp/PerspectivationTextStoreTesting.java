package de.monochromata.eclipse.persp;

import java.util.function.Consumer;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;

import de.monochromata.eclipse.persp.PerspectivationDocument;
import de.monochromata.eclipse.persp.PerspectivationTextStore;

public interface PerspectivationTextStoreTesting extends PerspectivationDocumentTesting {

	default Triple<Document, PerspectivationDocument, PerspectivationTextStore> createStoreWith1ToLowerCase()
			throws BadLocationException {
		return createStoreWith1ToLowerCase("The quick brown fox\njumps over the lazy dog.", 0);
	}

	default Triple<Document, PerspectivationDocument, PerspectivationTextStore> createStoreWith1ToLowerCase(
			final String initialText, final int offsetToLowerCase) throws BadLocationException {
		final Consumer<PerspectivationDocument> slaveModifier = slave -> {
			try {
				slave.toLowerCase(offsetToLowerCase);
			} catch (final Exception e) {
				throw new IllegalStateException(e);
			}
		};
		return createAndModifyStore(initialText, slaveModifier);
	}

	default Triple<Document, PerspectivationDocument, PerspectivationTextStore> createAndModifyStore(
			final String initialText, final Consumer<PerspectivationDocument> slaveModifier)
			throws BadLocationException {
		final Pair<Document, PerspectivationDocument> masterAndSlave = createOriginAndImageDocument(initialText);

		final Document master = masterAndSlave.getLeft();
		final PerspectivationDocument slave = masterAndSlave.getRight();
		slaveModifier.accept(slave);

		final PerspectivationTextStore store = new PerspectivationTextStore(master, slave,
				slave.getDocumentInformationMapping());
		return new ImmutableTriple<>(master, slave, store);
	}

}
