package de.monochromata.eclipse.persp;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.DocumentEvent;
import org.junit.Test;

public class PerspectivationPositionNotifierTest implements PerspectivationDocumentTesting {

    final PerspectivationDocumentManager documentManager = new PerspectivationDocumentManager();

    @Override
    public PerspectivationDocumentManager documentManager() {
        return documentManager;
    }

    /**
     * This test ensures that when the position notifier is notified of a removal
     * that has already happened (i.e. the end of the range of the event exceeds the
     * length of the current document text) does not yield an exception.
     */
    @Test
    public void doesNotYieldAnExceptionWhenNotifiedOfRemoval() throws Exception {
        final Pair<Document, PerspectivationDocument> originAndImageDocument = createOriginAndImageDocument("Foo bar");
        final Document originDocument = originAndImageDocument.getLeft();
        final PerspectivationDocument imageDocument = originAndImageDocument.getRight();
        final DocumentEvent eventInformingAboutCompletedRemoval = new DocumentEvent(originDocument, 7, 10, "");
        final PerspectivationPositionNotifier notifier = new PerspectivationPositionNotifier(imageDocument);

        // Should not yield an exception
        notifier.documentChanged(eventInformingAboutCompletedRemoval);
    }

}
