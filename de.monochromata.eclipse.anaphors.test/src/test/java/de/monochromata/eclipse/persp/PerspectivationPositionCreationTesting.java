package de.monochromata.eclipse.persp;

import static de.monochromata.eclipse.persp.PerspectivationPositionCreation.createPerspectivationPositions;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;

public interface PerspectivationPositionCreationTesting extends PositionCreationTesting {

	default List<Pair<PerspectivationPosition, PerspectivationPosition>> collectForLinking(
			final PerspectivationPosition existingRelatedExpressionPerspectivation,
			final PerspectivationPosition existingAnaphorPerspectivation) {
		final List<Pair<PerspectivationPosition, PerspectivationPosition>> positionsToLink = new ArrayList<>();
		createPerspectivationPositions(relatedExpressionElement, Stream.builder(), positionsToLink,
				unused -> existingRelatedExpressionPerspectivation, unused -> relatedExpressionPerspectivationPosition,
				unused -> existingAnaphorPerspectivation, unused0 -> unused1 -> anaphorPerspectivationPosition);
		return positionsToLink;
	}

}
