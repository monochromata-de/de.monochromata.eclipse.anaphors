package de.monochromata.eclipse.persp;

import static de.monochromata.eclipse.persp.PerspectivationPositionCreation.perspectivize;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.dom.anaphors.PublicChainElement;
import org.junit.Test;

import de.monochromata.anaphors.perspectivation.strategy.PerspectivationConfiguration;

public class PerspectivationPositionCreationTest implements PerspectivationPositionCreationTesting {

	@Test
	public void configureAnaphorPerspectivation_doesNotLinkPositions() {
		final var perspectivationConfiguration = new PerspectivationConfiguration(true, true);

		PerspectivationPositionCreation.CONFIGURE_ANAPHOR_PERSPECTIVATION.apply(perspectivationConfiguration)
				.apply(anaphorPerspectivationPosition);

		assertNotLinkedToNext(relatedExpressionPerspectivationPosition);
		assertNotLinkedToPrevious(anaphorPerspectivationPosition);
	}

	@Test
	public void collectForAddition_unperspectivizedRelatedExpression() {
		final Stream.Builder<Pair<PublicChainElement, PerspectivationPosition>> positionsToAdd = Stream.builder();

		perspectivize(relatedExpressionElement, unused -> null, unused -> relatedExpressionPerspectivationPosition,
				positionsToAdd);

		assertThat(positionsToAdd.build()).containsExactly(
				new ImmutablePair<>(relatedExpressionElement, relatedExpressionPerspectivationPosition));
	}

	@Test
	public void doNotCollectForAddition_perspectivizedRelatedExpression() {
		final Stream.Builder<Pair<PublicChainElement, PerspectivationPosition>> positionsToAdd = Stream.builder();

		perspectivize(relatedExpressionElement, unused -> relatedExpressionPerspectivationPosition, unused -> {
			throw new UnsupportedOperationException();
		}, positionsToAdd);

		assertThat(positionsToAdd.build()).isEmpty();
	}

	@Test
	public void collectForAddition_unperspectivizedAnaphor() {
		final Stream.Builder<Pair<PublicChainElement, PerspectivationPosition>> positionsToAdd = Stream.builder();

		perspectivize(anaphorElement, unused -> null, unused -> anaphorPerspectivationPosition, positionsToAdd);

		assertThat(positionsToAdd.build())
				.containsExactly(new ImmutablePair<>(anaphorElement, anaphorPerspectivationPosition));
	}

	@Test
	public void doNotCollectForAddition_perspectivizedAnaphor() {
		final Stream.Builder<Pair<PublicChainElement, PerspectivationPosition>> positionsToAdd = Stream.builder();

		perspectivize(anaphorElement, unused -> anaphorPerspectivationPosition, unused -> {
			throw new UnsupportedOperationException();
		}, positionsToAdd);

		assertThat(positionsToAdd.build()).isEmpty();
	}

	@Test
	public void collectForLinking_unperspectivizedRelatedExpression_unperspectivizedAnaphor() {
		final var positionsToLink = collectForLinking(null, null);

		assertThat(positionsToLink).containsExactly(
				new ImmutablePair<>(relatedExpressionPerspectivationPosition, anaphorPerspectivationPosition));
	}

	/**
	 * This can happen when the related expression is re-perspectivized.
	 */
	@Test
	public void collectForLinking_unperspectivizedRelatedExpression_perspectivizedAnaphor() {
		final var positionsToLink = collectForLinking(null, anaphorPerspectivationPosition);

		assertThat(positionsToLink).containsExactly(
				new ImmutablePair<>(relatedExpressionPerspectivationPosition, anaphorPerspectivationPosition));
	}

	@Test
	public void collectForLinking_perspectivizedRelatedExpression_unperspectivizedAnaphor() {
		final var positionsToLink = collectForLinking(relatedExpressionPerspectivationPosition, null);

		assertThat(positionsToLink).containsExactly(
				new ImmutablePair<>(relatedExpressionPerspectivationPosition, anaphorPerspectivationPosition));
	}

	@Test
	public void doNotcollectForLinking_perspectivizedRelatedExpression_perspectivizedAnaphor() {
		final var positionsToLink = collectForLinking(relatedExpressionPerspectivationPosition,
				anaphorPerspectivationPosition);

		assertThat(positionsToLink).isEmpty();
	}

}
