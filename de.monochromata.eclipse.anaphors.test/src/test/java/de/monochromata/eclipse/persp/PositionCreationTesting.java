package de.monochromata.eclipse.persp;

import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicChainElement;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.DefaultAnaphorPart;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;
import de.monochromata.eclipse.position.DoublyLinkedPositionTesting;

public interface PositionCreationTesting extends DoublyLinkedPositionTesting {

	AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> anaphorPart = new DefaultAnaphorPart<>(
			null, null, null, null, null, null);
	PublicChainElement anaphorElement = new PublicChainElement(null, anaphorPart, null);
	PublicChainElement relatedExpressionElement = new PublicChainElement(List.of(anaphorElement), null);

	PerspectivationPosition relatedExpressionPerspectivationPosition = new PerspectivationPosition(0, 1);
	PerspectivationPosition anaphorPerspectivationPosition = new PerspectivationPosition(2, 1);

	Anaphora anaphora = new Anaphora("RelExp", "AnaRes", "Ref", "anaphor");
	PositionForRelatedExpression positionForRelatedExpression = new PositionForRelatedExpression(
			relatedExpressionPerspectivationPosition);
	PositionForAnaphor positionForAnaphor = new PositionForAnaphor(anaphorPerspectivationPosition, anaphora);

}
