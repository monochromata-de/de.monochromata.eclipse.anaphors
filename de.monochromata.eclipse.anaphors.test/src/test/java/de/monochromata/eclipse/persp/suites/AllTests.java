package de.monochromata.eclipse.persp.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import de.monochromata.eclipse.persp.ConditionCreationTest;
import de.monochromata.eclipse.persp.PerspectivationDocumentTest;
import de.monochromata.eclipse.persp.PerspectivationPositionCreationTest;
import de.monochromata.eclipse.persp.PerspectivationPositionNotifierTest;
import de.monochromata.eclipse.persp.PerspectivationPositionTest;
import de.monochromata.eclipse.persp.PerspectivationTextStoreTest;

/**
 * Needs to be run as a JUnit Plug-in Test, but not in the UI thread.
 */
@RunWith(Suite.class)
@SuiteClasses({
		// Unit tests
		ConditionCreationTest.class, PerspectivationDocumentTest.class, PerspectivationPositionCreationTest.class,
		PerspectivationPositionTest.class, PerspectivationPositionNotifierTest.class,
		PerspectivationTextStoreTest.class })
public class AllTests {

}
