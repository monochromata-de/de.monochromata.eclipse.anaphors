package de.monochromata.eclipse.persp;

import static de.monochromata.eclipse.persp.ConditionCreation.createCondition;
import static de.monochromata.eclipse.persp.ConditionCreation.getCondition;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.function.Predicate;

import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.text.edits.CopySourceEdit;
import org.eclipse.text.edits.CopyTargetEdit;
import org.eclipse.text.edits.DeleteEdit;
import org.eclipse.text.edits.InsertEdit;
import org.eclipse.text.edits.MoveSourceEdit;
import org.eclipse.text.edits.MoveTargetEdit;
import org.eclipse.text.edits.MultiTextEdit;
import org.junit.Test;

public class ConditionCreationTest {

	private final Document masterDocument = new Document();
	private final PerspectivationDocumentManager documentManager = new PerspectivationDocumentManager();
	private final PerspectivationDocument document = documentManager.createSlaveDocument(masterDocument);

	@Test
	public void getCondition_isLazy() {
		final var condition = getCondition(new MultiTextEdit());

		assertThat(condition).isNotNull();
	}

	@Test
	public void getCondition_throwsIllegalArgumentExceptionIfNoConditionCanBeExtracted() {
		assertThatThrownBy(() -> getCondition(new MultiTextEdit()).test(new DocumentEvent()))
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessage("Could not create condition for {MultiTextEdit} [0,0] [undefined]");
	}

	@Test
	public void createCondition_throwsIllegalArgumentExceptionIfNoConditionCanBeExtracted() {
		assertThatThrownBy(() -> createCondition(new MultiTextEdit())).isInstanceOf(IllegalArgumentException.class)
				.hasMessage("Could not create condition for {MultiTextEdit} [0,0] [undefined]");
	}

	/**
	 * Edits
	 *
	 * <pre>
	 * {@code {MultiTextEdit} [180,32] [undefined]
	 *   {MultiTextEdit} [180,0]
	 *   {MultiTextEdit} [180,32]
	 *     {CopySourceEdit} [180,3]
	 *     {CopyTargetEdit} [208,0]
	 *     {InsertEdit} [208,0] <<.int0
	 *     {DeleteEdit} [208,4]
	 * }
	 * </pre>
	 *
	 * yield events, the last of which shall be captured by the condition
	 *
	 * <pre>
	 * {@code
	 * event @208 -4 +"" at 0
	 * event @208 -0 +".int0" at 0
	 * event @208 -0 +"foo" at 0
	 * }
	 * </pre>
	 */
	@Test
	public void createCondition_fromCopyTargetEdit() {
		document.set("package de.monochromata.eclipse.anaphors.editor.simple;\n" + "\n"
				+ "public class LVD_IA2F_Rn_Simple {\n" + "\n" + "\tpublic static void main(String[] args) {\n"
				+ "\t\tfoo(new A());\n" + "\t}\n" + "\n" + "\tpublic static void foo(A foo) {\n"
				+ "\t\tSystem.err.println(Int0);\n" + "\t}\n" + "\n" + "\tprivate static class A {\n"
				+ "\t\tpublic Integer int0;\n" + "\t}\n" + "}\n" + "");

		final MultiTextEdit rootEdit = new MultiTextEdit(180, 32);
		final MultiTextEdit deRealizationEdit = new MultiTextEdit(180, 0);
		rootEdit.addChild(deRealizationEdit);
		final MultiTextEdit realizationEdit = new MultiTextEdit(180, 32);
		final CopySourceEdit copySource = new CopySourceEdit(180, 3);
		realizationEdit.addChild(copySource);
		realizationEdit.addChild(new CopyTargetEdit(208, copySource));
		realizationEdit.addChild(new InsertEdit(208, ".int0"));
		realizationEdit.addChild(new DeleteEdit(208, 4));
		rootEdit.addChild(realizationEdit);

		final Predicate<DocumentEvent> condition = createCondition(rootEdit);

		assertThat(condition.test(new DocumentEvent(masterDocument, 208, 0, "foo"))).isTrue();
		assertThat(condition.test(new DocumentEvent(masterDocument, 209, 0, "foo"))).isFalse();
		assertThat(condition.test(new DocumentEvent(masterDocument, 208, 1, "foo"))).isFalse();
		assertThat(condition.test(new DocumentEvent(masterDocument, 208, 0, "bar"))).isFalse();
	}

	/**
	 * Edits
	 *
	 * <pre>
	 * {@code {MultiTextEdit} [245,21] [undefined]
	 *   {MultiTextEdit} [245,0]
	 *   {MultiTextEdit} [245,21]
	 *     {InsertEdit} [245,0] <<final JButton startjButton =
	 *     {MoveTargetEdit} [245,0]
	 *     {InsertEdit} [245,0] <<;
	 *     {DeleteEdit} [245,21]
	 *       {MoveSourceEdit} [245,20]
	 * }
	 * </pre>
	 *
	 * yield events, the last of which shall be captured by the condition
	 *
	 * <pre>
	 * {@code
	 * event @218 -20 +"" at 1
	 * event @218 -1 +"" at 1
	 * event @218 -0 +";" at 1
	 * event @218 -0 +"new JButton("start")" at 1
	 * event @218 -0 +"final JButton startjButton = " at 1
	 * }
	 * </pre>
	 */
	@Test
	public void createCondition_fromInsertEdit() throws Exception {
		document.set("package de.monochromata.eclipse.anaphors.editor.interactive;\n" + "\n"
				+ "import javax.swing.JButton;\n" + "import javax.swing.JPanel;\n" + "\n"
				+ "public class EnterCIC_DA1Re_RfRt_withAmbiguityResolvedAfterEdit {\n" + "\n"
				+ "\tpublic static void main(String[] args) {\n" + "\t\tnew JPanel();\n"
				+ "\t\tnew JButton(\"start\");\n" + "\t\tnew JButton(\"stop\");\n"
				+ "\t\tSystem.err.println(jButton);\n" + "\t}\n" + "}\n" + "");
		document.removeMasterDocumentRange(90, 27); // collapse imports

		final MultiTextEdit rootEdit = new MultiTextEdit(245, 21);
		final MultiTextEdit deRealizationEdit = new MultiTextEdit(245, 0);
		rootEdit.addChild(deRealizationEdit);
		final MultiTextEdit realizationEdit = new MultiTextEdit(245, 21);
		final MoveSourceEdit moveSource = new MoveSourceEdit(245, 20);
		final DeleteEdit deleteEdit = new DeleteEdit(245, 21);
		deleteEdit.addChild(moveSource);
		realizationEdit.addChild(new InsertEdit(245, "final JButton startjButton = "));
		realizationEdit.addChild(new MoveTargetEdit(245, moveSource));
		realizationEdit.addChild(new InsertEdit(245, ";"));
		realizationEdit.addChild(deleteEdit);
		rootEdit.addChild(realizationEdit);

		final Predicate<DocumentEvent> condition = createCondition(rootEdit);

		assertThat(condition.test(new DocumentEvent(masterDocument, 245, 0, "final JButton startjButton = "))).isTrue();
		assertThat(condition.test(new DocumentEvent(masterDocument, 246, 0, "final JButton startjButton = ")))
				.isFalse();
		assertThat(condition.test(new DocumentEvent(masterDocument, 245, 1, "final JButton startjButton = ")))
				.isFalse();
		assertThat(condition.test(new DocumentEvent(masterDocument, 245, 0, "bar"))).isFalse();
	}

	@Test
	public void createCondition_ifImportsAreAdded() throws Exception {
		document.set("package de.monochromata.eclipse.anaphors.editor.simple.feature;\n" + "\n"
				+ "import java.util.concurrent.ExecutorService;\n" + "\n"
				+ "public class AddImportForNewNonLocalType {\n" + "\n"
				+ "    public static void run(final ExecutorService executorService) {\n"
				+ "        executorService.submit(null, new Integer(0));\n" + "        System.err.println(future);\n"
				+ "    }\n" + "}\n");

		final MultiTextEdit rootEdit = new MultiTextEdit(0, 310);
		final MultiTextEdit deRealizationEdit = new MultiTextEdit(0, 0);
		rootEdit.addChild(deRealizationEdit);

		final MultiTextEdit importEdit = new MultiTextEdit(109, 0);
		importEdit.addChild(new InsertEdit(109, "\n"));
		importEdit.addChild(new InsertEdit(109, "import java.util.concurrent.Future;"));
		rootEdit.addChild(importEdit);

		final MultiTextEdit realizationEdit = new MultiTextEdit(231, 79);
		final MoveSourceEdit moveSource = new MoveSourceEdit(231, 44);
		final DeleteEdit deleteEdit = new DeleteEdit(231, 45);
		deleteEdit.addChild(moveSource);
		realizationEdit.addChild(new InsertEdit(231, "final Future<Integer> future = "));
		realizationEdit.addChild(new MoveTargetEdit(231, moveSource));
		realizationEdit.addChild(new InsertEdit(231, ";"));
		realizationEdit.addChild(deleteEdit);
		realizationEdit.addChild(new InsertEdit(304, "future"));
		realizationEdit.addChild(new DeleteEdit(304, 6));
		rootEdit.addChild(realizationEdit);

		final Predicate<DocumentEvent> condition = createCondition(rootEdit);

		assertThat(condition.test(new DocumentEvent(masterDocument, 231, 0, "final Future<Integer> future = ")))
				.isTrue();
		assertThat(condition.test(new DocumentEvent(masterDocument, 232, 0, "final Future<Integer> future = ")))
				.isFalse();
		assertThat(condition.test(new DocumentEvent(masterDocument, 231, 1, "final Future<Integer> future = ")))
				.isFalse();
		assertThat(condition.test(new DocumentEvent(masterDocument, 231, 0, "bar"))).isFalse();
	}
}
