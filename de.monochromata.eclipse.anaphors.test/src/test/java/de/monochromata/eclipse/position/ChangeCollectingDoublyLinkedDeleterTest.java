package de.monochromata.eclipse.position;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

public class ChangeCollectingDoublyLinkedDeleterTest implements DoublyLinkedPositionTesting {

	private final List<TestableDoublyLinkedPosition> positionsToAdd = new ArrayList<>();
	private final List<TestableDoublyLinkedPosition> positionsToRemove = new ArrayList<>();
	private final List<Pair<TestableDoublyLinkedPosition, TestableDoublyLinkedPosition>> positionsToLink = new ArrayList<>();
	private final List<Pair<TestableDoublyLinkedPosition, TestableDoublyLinkedPosition>> positionsToUnlink = new ArrayList<>();
	private final ChangeCollectingDoublyLinkedDeleter<TestableDoublyLinkedPosition> deleter = new ChangeCollectingDoublyLinkedDeleter<>(
			positionsToAdd, positionsToRemove, positionsToLink, positionsToUnlink);

	@Test
	public void isUnlinked_returnsTrueForUnlinkedPosition() {
		assertThat(deleter.isUnlinked(linkedPosition())).isTrue();
	}

	@Test
	public void isUnlinked_returnsFalseForPositionWithPrevious() {
		final var positionWithPrevious = linkedPosition(linkedPosition(), emptyList());

		assertThat(deleter.isUnlinked(positionWithPrevious)).isFalse();
	}

	@Test
	public void isUnlinked_returnsFalseForPositionWithNext() {
		final var positionWithNext = linkedPosition(null, List.of(linkedPosition()));

		assertThat(deleter.isUnlinked(positionWithNext)).isFalse();
	}

	@Test
	public void isUnlinked_returnsTrueForPositionWithPreviousWhichIsToBeUnlinked() {
		final var previous = linkedPosition();
		final var positionWithPrevious = linkedPosition(previous, emptyList());
		positionsToUnlink.add(new ImmutablePair<>(previous, positionWithPrevious));

		assertThat(deleter.isUnlinked(positionWithPrevious)).isTrue();
	}

	@Test
	public void isUnlinked_returnsFalseForPositionWithNextWhichIsToBeUnlinked() {
		final var next = linkedPosition();
		final var positionWithNext = linkedPosition(null, List.of(next));
		positionsToUnlink.add(new ImmutablePair<>(positionWithNext, next));

		assertThat(deleter.isUnlinked(positionWithNext)).isTrue();
	}

	@Test
	public void isUnlinked_returnsFalseForPositionWithTwoNextOfWhichOneIsToBeUnlinked() {
		final var next1 = linkedPosition(10, 1);
		final var next2 = linkedPosition(20, 2);
		final var positionWithNext = linkedPosition(null, List.of(next1, next2));
		positionsToUnlink.add(new ImmutablePair<>(positionWithNext, next1));

		assertThat(deleter.isUnlinked(positionWithNext)).isFalse();
	}

	@Test
	public void isUnlinked_returnsFalseForPositionYetToBeLinkedToPreviousPosition() {
		final var previousToBeLinked = linkedPosition(10, 1);
		final var position = linkedPosition(20, 2);
		positionsToLink.add(new ImmutablePair<>(previousToBeLinked, position));

		assertThat(deleter.isUnlinked(position)).isFalse();
	}

	@Test
	public void isUnlinked_returnsFalseForPositionWhichIsYetToBeLinkedToNext() {
		final var nextToBeLinked = linkedPosition(10, 1);
		final var position = linkedPosition(20, 2);
		positionsToLink.add(new ImmutablePair<>(position, nextToBeLinked));

		assertThat(deleter.isUnlinked(position)).isFalse();
	}

	@Test
	@SuppressWarnings("unchecked")
	public void link_collectsLinkToBeCreated() {
		final var previous = linkedPosition(10, 1);
		final var next = linkedPosition(20, 2);
		final var expectedLink = new ImmutablePair<>(previous, next);
		assertThat(positionsToLink).doesNotContain(expectedLink);

		deleter.link(previous, next);

		assertThat(positionsToLink).contains(expectedLink);
	}

	/**
	 * This test also prevents the same link from being created twice.
	 */
	@Test
	public void link_yieldsAnExceptionForPositionWithTwoPreviousToBeLinked() {
		final var previous1 = linkedPosition(10, 1);
		final var previous2 = linkedPosition(20, 2);
		final var position = linkedPosition(30, 3);
		positionsToLink.add(new ImmutablePair<>(previous1, position));

		assertThatThrownBy(() -> deleter.link(previous2, position)).isInstanceOf(IllegalStateException.class)
				.hasMessage(
						"You attempted to link previous position offset: 20, length: 2 to position offset: 30, length: 3 "
								+ "but the latter is already to be linked to previous position offset: 10, length: 1. "
								+ "There can only be one previous position. "
								+ "This is either a design flaw or a temporal order is required for positions to be linked.");
	}

	@Test
	public void link_yieldsAnExceptionForExistingLinkToBeUnlinked() {
		final var previousToLink = linkedPosition(10, 1);
		final var previousToUnlink = linkedPosition(20, 2);
		final var position = linkedPosition(30, 3, previousToUnlink, emptyList());
		positionsToUnlink.add(new ImmutablePair<>(previousToUnlink, position));

		assertThatThrownBy(() -> deleter.link(previousToLink, position)).isInstanceOf(IllegalStateException.class)
				.hasMessage("You attempted to link position offset: 10, length: 1 to position offset: 30, length: 3 "
						+ "but the latter is already to be unlinked from position offset: 20, length: 2. "
						+ "This is either a design flaw or a temporal order is required for positions to be linked and unlinked.");
	}

	@Test
	public void link_rejectsLinkingOfPreviousPositionThatIsToBeRemoved() {
		final var previous = linkedPosition(10, 1);
		final var next = linkedPosition(20, 2);
		positionsToRemove.add(previous);

		assertThatThrownBy(() -> deleter.link(previous, next)).isInstanceOf(IllegalStateException.class)
				.hasMessage("You attempted to link position offset: 10, length: 1 to position offset: 20, length: 2 "
						+ "but the former is already to be deleted. "
						+ "This is either a design flaw or a temporal order is required for positions to be linked and deleted.");
	}

	@Test
	public void link_rejectsLinkingOfNextPositionThatIsToBeRemoved() {
		final var previous = linkedPosition(10, 1);
		final var next = linkedPosition(20, 2);
		positionsToRemove.add(next);

		assertThatThrownBy(() -> deleter.link(previous, next)).isInstanceOf(IllegalStateException.class)
				.hasMessage("You attempted to link position offset: 10, length: 1 to position offset: 20, length: 2 "
						+ "but the latter is already to be deleted. "
						+ "This is either a design flaw or a temporal order is required for positions to be linked and deleted.");
	}

	@Test
	public void unlink_collectsRemovalOfRealizedLink() {
		final var previous = linkedPosition(10, 1);
		final var next = linkedPosition(20, 2, previous, emptyList());
		assertThat(positionsToUnlink).isEmpty();

		deleter.unlink(previous, next);

		assertThat(positionsToUnlink).containsExactly(new ImmutablePair<>(previous, next));
	}

	@Test
	public void unlinks_yieldsExceptionIfLinkDoesNotExistYet() {
		final var previous = linkedPosition(10, 1);
		final var next = linkedPosition(20, 2);
		assertThat(positionsToUnlink).isEmpty();

		assertThatThrownBy(() -> deleter.unlink(previous, next)).isInstanceOf(IllegalStateException.class)
				.hasMessage("Cannot unlink offset: 10, length: 1 and offset: 20, length: 2. "
						+ "The positions are not yet linked.");
	}

	@Test
	public void unlink_doesNotCollectDuplicates() {
		final var previous = linkedPosition(10, 1);
		final var next = linkedPosition(20, 2, previous, emptyList());
		positionsToUnlink.add(new ImmutablePair<>(previous, next));

		assertThatThrownBy(() -> deleter.unlink(previous, next)).isInstanceOf(IllegalStateException.class)
				.hasMessage("Cannot unlink offset: 10, length: 1 and offset: 20, length: 2. "
						+ "The positions are to be unlinked already.");
	}

	@Test
	public void unlink_yieldsAnExceptionForLinkWhichIsToBeBothLinkedAndUnlinked() {
		final var previous = linkedPosition(10, 1);
		final var position = linkedPosition(20, 2, previous, emptyList());
		positionsToLink.add(new ImmutablePair<>(previous, position));

		assertThatThrownBy(() -> deleter.unlink(previous, position)).isInstanceOf(IllegalStateException.class)
				.hasMessage(
						"Positions offset: 10, length: 1 and offset: 20, length: 2 are to be linked and to be unlinked at the same time. "
								+ "This is either a design flaw or a temporal order is required for positions to be linked and unlinked.");
	}

	@Test
	public void deletePosition_worksInSimpleCase() {
		final var position = linkedPosition(10, 1);
		assertThat(positionsToRemove).isEmpty();

		deleter.deletePosition(position);

		assertThat(positionsToRemove).containsExactly(position);
	}

	@Test
	public void deletePosition_notifiesListener() {
		final var position = linkedPosition(10, 1);
		final List<TestableDoublyLinkedPosition> notifications = new ArrayList();
		deleter.addDeletionListener(notifications::add);

		deleter.deletePosition(position);

		assertThat(notifications).containsExactly(position);
	}

	@Test
	public void deletePosition_rejectsDuplicateDeletions() {
		final var position = linkedPosition(10, 1);
		positionsToRemove.add(position);

		assertThatThrownBy(() -> deleter.deletePosition(position)).isInstanceOf(IllegalStateException.class).hasMessage(
				"Cannot delete position offset: 10, length: 1 twice - the position is already to be deleted");
	}

	@Test
	public void deletePosition_rejectsDeletionOfPositionThatIsToBeAdded() {
		final var position = linkedPosition(10, 1);
		positionsToAdd.add(position);

		assertThatThrownBy(() -> deleter.deletePosition(position)).isInstanceOf(IllegalStateException.class)
				.hasMessage("Position offset: 10, length: 1 is to be added already and shall now be removed. "
						+ "This is either a design flaw or a temporal order is required for positions to be added and removed.");
	}

	@Test
	public void deletePosition_rejectsDeletionOfPositionThatIsPreviousToBeLinked() {
		final var previousToRemove = linkedPosition(10, 1);
		final var next = linkedPosition(20, 2);
		positionsToLink.add(new ImmutablePair<>(previousToRemove, next));

		assertThatThrownBy(() -> deleter.deletePosition(previousToRemove)).isInstanceOf(IllegalStateException.class)
				.hasMessage(
						"Position offset: 10, length: 1 is already to be linked to offset: 20, length: 2 and shall now be removed. "
								+ "This is either a design flaw or a temporal order is required for positions to be added and removed.");
	}

	@Test
	public void deletePosition_rejectsDeletionOfPositionThatIsNextToBeLinked() {
		final var previous = linkedPosition(10, 1);
		final var nextToRemove = linkedPosition(20, 2);
		positionsToLink.add(new ImmutablePair<>(previous, nextToRemove));

		assertThatThrownBy(() -> deleter.deletePosition(nextToRemove)).isInstanceOf(IllegalStateException.class)
				.hasMessage(
						"Position offset: 20, length: 2 is already to be linked to offset: 10, length: 1 and shall now be removed. "
								+ "This is either a design flaw or a temporal order is required for positions to be added and removed.");
	}
}
