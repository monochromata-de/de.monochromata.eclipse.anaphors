package de.monochromata.eclipse.position.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import de.monochromata.eclipse.position.ChangeCollectingDoublyLinkedDeleterTest;
import de.monochromata.eclipse.position.DoublyLinkedPositionTest;
import de.monochromata.eclipse.position.DoublyLinkedPositionUpdaterTest;
import de.monochromata.eclipse.position.GrowingPositionUpdaterTest;
import de.monochromata.eclipse.position.PositionDeleterTest;
import de.monochromata.eclipse.position.PositionDeletionTest;
import de.monochromata.eclipse.position.PositionQueryingTest;

/**
 * Needs to be run as a JUnit Plug-in Test, but not in the UI thread.
 */
@RunWith(Suite.class)
@SuiteClasses({
        // Unit tests
		ChangeCollectingDoublyLinkedDeleterTest.class,
        DoublyLinkedPositionTest.class,
        DoublyLinkedPositionUpdaterTest.class,
        GrowingPositionUpdaterTest.class,
        PositionDeleterTest.class,
        PositionDeletionTest.class,
        PositionQueryingTest.class })
public class AllTests {

}
