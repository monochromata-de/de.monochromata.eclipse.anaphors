package de.monochromata.eclipse.position;

import static de.monochromata.eclipse.position.PositionsTesting.TEST_POSITION_CATEGORY;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

import java.util.List;

import org.eclipse.jface.text.IDocument;

import de.monochromata.function.TriConsumer;

public interface DoublyLinkedPositionTesting extends DoublyLinkedPositionAssertions {

	default void maintainChain(final TestableDoublyLinkedPosition position) {
		maintainChainForTestCategory().accept(position, null, null);
	}

	default TriConsumer<TestableDoublyLinkedPosition, IDocument, Void> maintainChainForTestCategory() {
		return (position, document, unusedDeletionStrategy) -> DoublyLinkedPositionUpdater
				.<TestableDoublyLinkedPosition>maintainChainBeforeDeletion(TEST_POSITION_CATEGORY).accept(position, document);
	}

	default TestableDoublyLinkedPosition linkedPosition() {
		return linkedPosition(null, (TestableDoublyLinkedPosition) null);
	}

	default TestableDoublyLinkedPosition linkedPosition(final int offset, final int length) {
		return linkedPosition(offset, length, null, (TestableDoublyLinkedPosition) null);
	}

	default TestableDoublyLinkedPosition linkedPosition(final TestableDoublyLinkedPosition previous,
			final TestableDoublyLinkedPosition next) {
		return linkedPosition(previous, next == null ? emptyList() : singletonList(next));
	}

	default TestableDoublyLinkedPosition linkedPosition(final TestableDoublyLinkedPosition previous,
			final List<TestableDoublyLinkedPosition> next) {
		return linkedPosition(0, 0, previous, next);
	}

	default TestableDoublyLinkedPosition linkedPosition(final int offset, final int length,
			final TestableDoublyLinkedPosition previous, final TestableDoublyLinkedPosition next) {
		return linkedPosition(offset, length, previous, next == null ? emptyList() : singletonList(next));
	}

	default TestableDoublyLinkedPosition linkedPosition(final int offset, final int length,
			final TestableDoublyLinkedPosition previous, final List<TestableDoublyLinkedPosition> next) {
		final TestableDoublyLinkedPosition newPosition = new TestableDoublyLinkedPosition(offset, length, previous,
				next);
		if (previous != null) {
			previous.next.add(newPosition);
		}
		next.forEach(nextElement -> nextElement.previous = newPosition);
		return newPosition;
	}

}
