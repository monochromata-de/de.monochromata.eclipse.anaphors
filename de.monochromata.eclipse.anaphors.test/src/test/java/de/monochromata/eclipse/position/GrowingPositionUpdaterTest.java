package de.monochromata.eclipse.position;

import static org.assertj.core.api.Assertions.assertThat;

import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.Position;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class GrowingPositionUpdaterTest implements GrowingPositionUpdaterTesting {

    /**
     * {@literal String foo| = [ "rela|tedExpress|ion", "ana|phor" ];}
     */
    @Test
    public void correctPositionsAreAdded() throws Exception {
        assertCorrectPositions("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
                "\"relatedExpression\"", 36, "\"anaphor\"", unusued -> {
                }, 15, 34, 36, 45);
    }

    /**
     * {@literal String foo| = [ "rela|ted<+Foo>Express|ion", "an<+Bar>a|phor"
     *  ];}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForChangesOnTheNodes() throws Exception {
        assertCorrectPositions("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
                "\"relatedExpression\"", 36, "\"anaphor\"", document -> {
                    document.replace(23, 0, "Foo");
                    document.replace(39, 0, "Bar");
                }, 15, 37, 39, 51);
    }

    /**
     * {@literal String
     * <-foo+fooAndBar>| = [ "rela|tedExpress|ion", "ana|phor" ];
     */
    @Test
    public void positionsAreCorrectlyUpdatedForChangesBeforeTheStartOfTheNodes() throws Exception {
        assertCorrectPositions("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
                "\"relatedExpression\"", 36, "\"anaphor\"", document -> document.replace(7, 3, "fooAndBar"), 21, 40, 42,
                51);
    }

    /**
     * {@literal String foo| = [ <-">rela|tedExpress|ion", "ana|phor" ];}
     */
    @Test
    public void positionWithLengthDoesNotGetNegativeLengthOnRemoval() throws Exception {
        final GrowingPositionUpdater updater = new GrowingPositionUpdater(TEST_POSITION_CATEGORY);
        updater.setDeleteEmptyPositions(false);

        assertCorrectPositions("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
                "", 36, "\"anaphor\"", document -> document.replace(15, 1, ""), 15, 15, 35, 44, updater);
    }

    /**
     * {@literal String foo| = [ <-">rela|tedExpress|ion", "ana|phor" ];}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForRemovalAtTheStartOfTheNodes() throws Exception {
        assertCorrectPositions("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
                "\"relatedExpression\"", 36, "\"anaphor\"", document -> document.replace(15, 1, ""), 15, 33, 35, 44);
    }

    /**
     * {@literal String foo| = [<- "r>ela|tedExpress|ion", "ana|phor" ];}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForCrossingRemovalAtTheStartOfTheNodes() throws Exception {
        assertCorrectPositions("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
                "\"relatedExpression\"", 36, "\"anaphor\"", document -> document.replace(14, 3, ""), 14, 31, 33, 42);
    }

    /**
     * {@literal String foo| = [ <-"+"a", ">rela|tedExpress|ion", "ana|phor" ];}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForInsertionAtTheStartOfTheNodes() throws Exception {
        assertCorrectPositions("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
                "\"relatedExpression\"", 36, "\"anaphor\"", document -> document.replace(15, 1, "\"a\", \""), 15, 39,
                41, 50);
    }

    /**
     * {@literal ... new Fo|o(); field| ...}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForInsertionAtTheStartOfTheNodes2() throws Exception {
        assertCorrectPositions("... new Foo(); field ...", 4,
                "new Foo()", 15, "field", document -> document.replace(4, 0, "final Foo foo = "), 4, 4 + 9 + 16,
                15 + 16, 15 + 16 + 5);
    }

    /**
     * {@literal Foo foo ..|. bar ...}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForInsertionAtTheStartOfTheNodes3() throws Exception {
        assertCorrectPositions("Foo foo ... bar ...", 0,
                "Foo foo", 12, "bar", document -> {
                    document.replace(12, 3, "");
                    document.replace(12, 0, ".getBar()");
                    document.replace(12, 0, "foo");
                }, 0, 7, 12, 12 + 12);
    }

    /**
     * {@literal String foo| = [<- "rela+"a", ">|tedExpress|ion", "ana|phor" ];}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForCrossingInsertionAtTheStartOfTheNodes() throws Exception {
        assertCorrectPositions("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
                "\"relatedExpression\"", 36, "\"anaphor\"", document -> document.replace(14, 6, "\"a\", \""), 14, 34,
                36, 45);
    }

    /**
     * {@literal String foo| = [ "rela|tedExpress|ion<-">, "ana|phor" ];}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForRemovalAtTheEndOfTheNodes() throws Exception {
        assertCorrectPositions("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
                "\"relatedExpression\"", 36, "\"anaphor\"", document -> document.replace(33, 1, ""), 15, 33, 35, 44);
    }

    /**
     * {@literal String foo| = [ "rela|tedExpress|ion<-", >"ana|phor" ];}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForCrossingRemovalAtTheEndOfTheNodes() throws Exception {
        assertCorrectPositions("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
                "\"relatedExpression\"", 36, "\"anaphor\"", document -> document.replace(33, 3, ""), 15, 33, 33, 42);
    }

    /**
     * {@literal String foo| = [ "rela|tedExpress|ion<-"+" >, "ana|phor" ];}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForInsertionAtTheEndOfTheNodes() throws Exception {
        assertCorrectPositions("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
                "\"relatedExpression\"", 36, "\"anaphor\"", document -> document.replace(33, 1, "\" "), 15, 35, 37, 46);
    }

    /**
     * {@literal String foo| = [ "rela|tedExpress|io<-n",+foo> "ana|phor" ];}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForCrossingInsertionAtTheEndOfTheNodes() throws Exception {
        assertCorrectPositions("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
                "\"relatedExpression\"", 36, "\"anaphor\"", document -> document.replace(32, 3, "foo"), 15, 35, 36, 45);
    }

    /**
     * {@literal String foo| = [ "rela|tedExpress|ion", "ana|phor" ]
     * <-;+something>}
     */
    @Test
    public void positionsAreNotUpdatedForChangesAfterTheEndOfTheNodes() throws Exception {
        assertCorrectPositions("String foo = [ \"relatedExpression\", \"anaphor\" ];", 15,
                "\"relatedExpression\"", 36, "\"anaphor\"", document -> document.replace(47, 1, "something"), 15, 34,
                36, 45);
    }

    /**
     * {@literal <-">relatedEx|pression",| "anaphor"| ];}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForRemovalAndNodeStartAt0() throws Exception {
        assertCorrectPositions("\"relatedExpression\", \"anaphor\" ];", 0,
                "\"relatedExpression\"", 21, "\"anaphor\"", document -> document.replace(0, 1, ""), 0, 18, 20, 29);
    }

    /**
     * {@literal <-"r>elatedEx|pression",| "anaphor"| ];}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForCrossingRemovalAndNodeStartAt0() throws Exception {
        assertCorrectPositions("\"relatedExpression\", \"anaphor\" ];", 0,
                "\"relatedExpression\"", 21, "\"anaphor\"", document -> document.replace(0, 2, ""), 0, 17, 19, 28);
    }

    /**
     * {@literal <+Add>"relatedEx|pression",| "anaphor"| ];}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForInsertionAndNodeStartAt0() throws Exception {
        assertCorrectPositions("\"relatedExpression\", \"anaphor\" ];", 0,
                "\"relatedExpression\"", 21, "\"anaphor\"", document -> document.replace(0, 0, "Add"), 0, 22, 24, 33);
        // TODO: Currently (0,19) (24,9)
    }

    /**
     * {@literal <-"re+Add>latedEx|pression",| "anaphor"| ];}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForCrossingInsertionAndNodeStartAt0() throws Exception {
        assertCorrectPositions("\"relatedExpression\", \"anaphor\" ];", 0,
                "\"relatedExpression\"", 21, "\"anaphor\"", document -> document.replace(0, 3, "Add"), 0, 19, 21, 30);
    }

    /**
     * {@literal String foo| = [ "rela|tedExpress|ion", "ana|phor<-">}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForRemovalAndNodeEndAtEndOfDocument() throws Exception {
        assertCorrectPositions("String foo = [ \"relatedExpression\", \"anaphor\"", 15,
                "\"relatedExpression\"", 36, "\"anaphor\"", document -> document.replace(44, 1, ""), 15, 34, 36, 44);
    }

    /**
     * {@literal String foo| = [ "rela|tedExpress|ion", "ana|pho<-r">}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForCrossingRemovalAndNodeEndAtEndOfDocument()
            throws Exception {
        assertCorrectPositions("String foo = [ \"relatedExpression\", \"anaphor\"", 15,
                "\"relatedExpression\"", 36, "\"anaphor\"", document -> document.replace(43, 2, ""), 15, 34, 36, 43);
    }

    /**
     * {@literal String foo| = [ "rela|tedExpress|ion", "ana|phor"<+Foo>}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForInsertionAndNodeEndAtEndOfDocument() throws Exception {
        assertCorrectPositions("String foo = [ \"relatedExpression\", \"anaphor\"", 15,
                "\"relatedExpression\"", 36, "\"anaphor\"", document -> document.replace(45, 0, "Foo"), 15, 34, 36, 48);
    }

    /**
     * {@literal String foo| = [ "rela|tedExpress|ion", "ana|ph<+Foo>or"}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForCrossingInsertionAndNodeEndAtEndOfDocument()
            throws Exception {
        assertCorrectPositions("String foo = [ \"relatedExpression\", \"anaphor\"", 15,
                "\"relatedExpression\"", 36, "\"anaphor\"", document -> document.replace(42, 0, "Foo"), 15, 34, 36, 48);
        // TODO: Why is this insertion "crossing"?
    }

    /*
     * TODO: @Test public void
     * positionsAreCorrectlyUpdatedForCrossingInsertionAndNodeEndAtEndOfDocument ()
     * throws Exception { assertCorrectPositions(context,
     * "String foo = [ \"relatedExpression\", \"anaphor\"", 15,
     * "\"relatedExpression\"", 36, "\"anaphor\"", document -> document.replace(42,
     * 3, "Foo"), 15, 34, 36, 48); }
     */

    /**
     * {@literal String foo| = [
     * <1:-"rela|tedExpress|ion"><2:+"RELATEDEXPRESSIONSFOOBAR">, "ana|phor"}
     */
    @Test
    public void positionsAreCorrectlyUpdatedForTwoStepReplacement() throws Exception {
        // Refactorings may introduce multiple modifications: a removal and then
        // a separate insertion
        assertCorrectPositions("String foo = [ \"relatedExpression\", \"anaphor\"", 15,
                "\"relatedExpression\"", 36, "\"anaphor\"", document -> {
                    document.replace(15, 19, "");
                    document.replace(15, 0, "\"RELATEDEXPRESSIONSFOOBAR\"");
                }, 15, 41, 43, 52);
    }

    /**
     * {@literal String foo|<- = [ "rela|tedExpress|ion",> "ana|phor"}
     */
    @Test
    public void deleteEmptyPositionsIfDeletionsAreTurnedOn() throws Exception {
        final Document document = createConfiguredDocument("String foo = [ \"relatedExpression\", \"anaphor\"",
                POSITION_UPDATER_THAT_DELETES_EMPTY_POSITIONS);
        addPosition(15, "\"relatedExpression\"", document);
        addPosition(36, "\"anaphor\"", document);

        document.replace(10, 25, "");

        assertPositions(document, new Position(11, 20 - 11));
    }

    /**
     * {@literal String foo| = [ <-"rela|tedExpress|ion">, "ana|phor"}
     */
    @Test
    public void deleteEmptyPositionsIfDeletionMatchesPositionExactlyIfDeletionsAreTurnedOn() throws Exception {
        final Document document = createConfiguredDocument("String foo = [ \"relatedExpression\", \"anaphor\"",
                POSITION_UPDATER_THAT_DELETES_EMPTY_POSITIONS);
        addPosition(15, "\"relatedExpression\"", document);
        addPosition(36, "\"anaphor\"", document);

        document.replace(15, 19, "");

        assertPositions(document, new Position(17, 26 - 17));
    }

    /**
     * {@literal String foo|<- = [ "rela|tedExpress|ion",> "ana|phor"}
     */
    @Test
    public void doNotDeleteEmptyPositionsIfDeletionsAreTurnedOff() throws Exception {
        final Document document = createConfiguredDocument("String foo = [ \"relatedExpression\", \"anaphor\"",
                POSITION_UPDATER_THAT_DOES_NOT_DELETE_EMPTY_POSITIONS);
        addPosition(15, "\"relatedExpression\"", document);
        addPosition(36, "\"anaphor\"", document);

        document.replace(10, 25, "");

        assertThat(document.getPositions(TEST_POSITION_CATEGORY)).hasSize(2);
    }

    /**
     * {@literal String foo|<- = [ "rela|tedExpress|ion",> "ana|phor"}
     */
    @Test
    public void limitLengthToMinimumOfZeroAfterDeletionsIfDeletionsAreTurnedOff() throws Exception {
        assertCorrectPositions("String foo = [ \"relatedExpression\", \"anaphor\"", 15,
                "\"relatedExpression\"", 36, "\"anaphor\"", document -> {
                    document.replace(10, 25, "");
                }, 10, 10, 11, 20, POSITION_UPDATER_THAT_DOES_NOT_DELETE_EMPTY_POSITIONS);
    }
}
