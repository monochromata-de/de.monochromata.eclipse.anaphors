package de.monochromata.eclipse.position;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPositionCategoryException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;

import de.monochromata.eclipse.position.PositionExceptionWrapping.BadLocationThrowingConsumer;

public interface GrowingPositionUpdaterTesting extends PositionsTesting {

    GrowingPositionUpdater POSITION_UPDATER_THAT_DELETES_EMPTY_POSITIONS = new GrowingPositionUpdater(
            TEST_POSITION_CATEGORY, true);
    GrowingPositionUpdater POSITION_UPDATER_THAT_DOES_NOT_DELETE_EMPTY_POSITIONS = new GrowingPositionUpdater(
            TEST_POSITION_CATEGORY, false);

    default void assertCorrectPositions(final String documentText,
            final int relatedExpressionStart, final String relatedExpressionCode, final int anaphorStart,
            final String anaphorCode, final BadLocationThrowingConsumer<IDocument> modification,
            final int expectedOffsetBeforeStartOfRelatedExpression, final int expectedOffsetAfterEndOfRelatedExpression,
            final int expectedOffsetBeforeStartOfAnaphor, final int expectedOffsetAfterEndOfAnaphor) throws Exception {
        assertCorrectPositions(documentText, relatedExpressionStart, relatedExpressionCode, anaphorStart, anaphorCode,
                modification, expectedOffsetBeforeStartOfRelatedExpression, expectedOffsetAfterEndOfRelatedExpression,
                expectedOffsetBeforeStartOfAnaphor, expectedOffsetAfterEndOfAnaphor,
                POSITION_UPDATER_THAT_DOES_NOT_DELETE_EMPTY_POSITIONS);
    }

    default void assertCorrectPositions(final String documentText, final int relatedExpressionStart,
            final String relatedExpressionCode, final int anaphorStart, final String anaphorCode,
            final BadLocationThrowingConsumer<IDocument> modification,
            final int expectedOffsetBeforeStartOfRelatedExpression, final int expectedOffsetAfterEndOfRelatedExpression,
            final int expectedOffsetBeforeStartOfAnaphor, final int expectedOffsetAfterEndOfAnaphor,
            final GrowingPositionUpdater updater) throws BadLocationException, BadPositionCategoryException, Exception {
        final Document document = createConfiguredDocument(documentText, updater);

        addPosition(relatedExpressionStart, relatedExpressionCode, document);
        addPosition(anaphorStart, anaphorCode, document);

        modification.accept(document);

        assertCorrectPositions(document, expectedOffsetBeforeStartOfRelatedExpression,
                expectedOffsetAfterEndOfRelatedExpression, expectedOffsetBeforeStartOfAnaphor,
                expectedOffsetAfterEndOfAnaphor);
    }

}
