package de.monochromata.eclipse.position;

import static de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPositionCreation.createMemberTrackingPosition;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapPositionExceptions;
import static de.monochromata.eclipse.position.PositionQuerying.getCoveredPositions;
import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.dom.anaphors.JavaModelTesting;
import org.eclipse.jdt.internal.corext.refactoring.change.DocumentTesting;
import org.eclipse.jface.text.Position;
import org.junit.Test;

import de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPosition;

public class PositionDeletionTest implements DocumentTesting, JavaModelTesting {

    String SOURCE_WITH_2_MEMBERS = "public class Foo { "
            + "public static void foo() { new Integer(1); System.err.println(integer); }"
            + "public void bar() { System.err.println(bar); }" + " }";

    String TEST_POSITION_CATEGORY = "testCategory";

    @Test
    public void deletesPositionsCoveredBySourceRangesAndRetainsOthers() throws Exception {
        assertDocument(SOURCE_WITH_2_MEMBERS, (originDocument, compilationUnit) -> {
            originDocument.addPositionCategory(TEST_POSITION_CATEGORY);

            final List<Position> deletedPositions = new ArrayList<>();
            final IMethod foo = findMethod("foo", compilationUnit);
            final Position positionInFooToBeRetained = new Position(foo.getSourceRange().getOffset() + 3,
                    1);
            wrapPositionExceptions(() -> originDocument.addPosition(TEST_POSITION_CATEGORY, positionInFooToBeRetained));

            final IMethod bar = findMethod("bar", compilationUnit);
            final MemberTrackingPosition trackingPosition = createMemberTrackingPosition(bar)
                    .orElseThrow(NullPointerException::new);
            final Position positionInBarToBeDeleted = new Position(
                    bar.getSourceRange().getOffset() + 10, 2);
            wrapPositionExceptions(() -> originDocument.addPosition(TEST_POSITION_CATEGORY, positionInBarToBeDeleted));

            final PositionDeleter positionDeleter = (position, document, deletionStrategy) -> deletedPositions
                    .add(position);
            final Stream<Position> coveredPositions = getCoveredPositions(
                    asList(requireNonNull(trackingPosition)), TEST_POSITION_CATEGORY,
                    originDocument);
            positionDeleter.deletePositions(coveredPositions, originDocument, null);

            assertThat(deletedPositions).containsExactly(positionInBarToBeDeleted);
        });
    }

}
