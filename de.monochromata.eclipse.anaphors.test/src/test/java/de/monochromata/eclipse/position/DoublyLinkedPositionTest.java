package de.monochromata.eclipse.position;

import static de.monochromata.eclipse.position.DoublyLinkedPosition.link;
import static de.monochromata.eclipse.position.DoublyLinkedPosition.unlink;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class DoublyLinkedPositionTest implements DoublyLinkedPositionTesting {

	@Test
	public void isFirstReturnsTrue() {
		assertThat(linkedPosition().isFirst()).isTrue();
	}

	@Test
	public void isFirstReturnsFalse() {
		assertThat(linkedPosition(linkedPosition(), emptyList()).isFirst()).isFalse();
	}

	@Test
	public void isLastReturnsTrue() {
		assertThat(linkedPosition().isLast()).isTrue();
	}

	@Test
	public void isLastReturnFalse() {
		assertThat(linkedPosition(null, linkedPosition()).isLast()).isFalse();
	}

	@Test
	public void getPositionBeforePreviousDoesNotYieldExceptionOnMissingPrevious() {
		assertThat(linkedPosition().getPositionBeforePrevious()).isNull();
	}

	@Test
	public void getPositionBeforePreviousReturnsNull() {
		assertThat(linkedPosition(linkedPosition(), emptyList()).getPositionBeforePrevious()).isNull();
	}

	@Test
	public void getPositionBeforePreviousReturnsPosition() {
		final TestableDoublyLinkedPosition initial = linkedPosition();
		assertThat(linkedPosition(linkedPosition(initial, emptyList()), emptyList()).getPositionBeforePrevious())
				.isSameAs(initial);
	}

	@Test
	public void hasPositionBeforePreviousDoesNotYieldExceptionOnMissingPrevious() {
		assertThat(linkedPosition().hasPositionBeforePrevious()).isFalse();
	}

	@Test
	public void hasPositionBeforePreviousReturnsFalse() {
		assertThat(linkedPosition(linkedPosition(), emptyList()).hasPositionBeforePrevious()).isFalse();
	}

	@Test
	public void hasPositionBeforePreviousReturnsTrue() {
		assertThat(
				linkedPosition(linkedPosition(linkedPosition(), emptyList()), emptyList()).hasPositionBeforePrevious())
						.isTrue();
	}

	@Test
	public void getPositionAfterNextDoesNotYieldExceptionOnMissingNext() {
		assertThat(linkedPosition().getPositionsAfterNext()).isEmpty();
	}

	@Test
	public void getPositionAfterNextReturnsEmpty() {
		assertThat(linkedPosition(null, linkedPosition()).getPositionsAfterNext()).isEmpty();
	}

	@Test
	public void getPositionAfterNextReturnsSinglePosition() {
		final TestableDoublyLinkedPosition last = linkedPosition();
		assertThat(linkedPosition(null, linkedPosition(null, last)).getPositionsAfterNext()).containsExactly(last);
	}

	@Test
	public void getPositionAfterNextReturnsMultiplePositions() {
		final TestableDoublyLinkedPosition last1 = linkedPosition();
		final TestableDoublyLinkedPosition last2 = linkedPosition();
		assertThat(linkedPosition(null, linkedPosition(null, asList(last1, last2))).getPositionsAfterNext())
				.containsExactly(last1, last2);
	}

	@Test
	public void hasPositionAfterNextDoesNotYieldExceptionOnMissingNext() {
		assertThat(linkedPosition().hasPositionsAfterNext()).isFalse();
	}

	@Test
	public void hasPositionAfterNextReturnsFalse() {
		assertThat(linkedPosition(null, linkedPosition()).hasPositionsAfterNext()).isFalse();
	}

	@Test
	public void hasPositionAfterNextReturnsTrue() {
		assertThat(linkedPosition(null, linkedPosition(null, linkedPosition())).hasPositionsAfterNext()).isTrue();
	}

	@Test
	public void link_works() {
		final TestableDoublyLinkedPosition position1 = linkedPosition();
		final TestableDoublyLinkedPosition position2 = linkedPosition();

		link(position1, position2);

		assertThat(position1.getNext()).containsExactly(position2);
		assertThat(position2.getPrevious()).isSameAs(position1);
	}

	@Test
	public void link_isIdempotent() {
		final TestableDoublyLinkedPosition position1 = linkedPosition();
		final TestableDoublyLinkedPosition position2 = linkedPosition();

		link(position1, position2);
		link(position1, position2);

		assertThat(position1.getNext()).containsExactly(position2);
		assertThat(position2.getPrevious()).isSameAs(position1);
	}

	@Test
	public void unlink_worksForLinkedPositions() {
		final TestableDoublyLinkedPosition position1 = linkedPosition();
		final TestableDoublyLinkedPosition position2 = linkedPosition();

		link(position1, position2);
		unlink(position1, position2);

		assertNotLinkedToNext(position1);
		assertNotLinkedToPrevious(position2);
	}

	@Test
	public void unlink_worksForMissingForwardLink() {
		final TestableDoublyLinkedPosition position1 = linkedPosition();
		final TestableDoublyLinkedPosition position2 = linkedPosition();

		link(position1, position2);
		position1.removeNext(position2);

		// Should not throw an exception
		unlink(position1, position2);

		assertNotLinkedToNext(position1);
		assertNotLinkedToPrevious(position2);
	}

	@Test
	public void unlink_worksForMissingBackwardLink() {
		final TestableDoublyLinkedPosition position1 = linkedPosition();
		final TestableDoublyLinkedPosition position2 = linkedPosition();

		link(position1, position2);
		position2.setPrevious(null);

		// Should not throw an exception
		unlink(position1, position2);

		assertNotLinkedToNext(position1);
		assertNotLinkedToPrevious(position2);
	}

	@Test
	public void unlink_worksForUnlinkedPositions() {
		final TestableDoublyLinkedPosition position1 = linkedPosition();
		final TestableDoublyLinkedPosition position2 = linkedPosition();

		// Should not throw an exception
		unlink(position1, position2);

		assertNotLinkedToNext(position1);
		assertNotLinkedToPrevious(position2);
	}
}
