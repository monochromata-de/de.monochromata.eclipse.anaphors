package de.monochromata.eclipse.position;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

public interface DoublyLinkedPositionAssertions {

	default void assertPosition(final TestableDoublyLinkedPosition position,
			final TestableDoublyLinkedPosition expectedPrevious, final TestableDoublyLinkedPosition expectedNext,
			final boolean expectedIsDeleted) {
		assertPosition(position, expectedPrevious, expectedNext == null ? emptyList() : singletonList(expectedNext),
				expectedIsDeleted);
	}

	default void assertPosition(final TestableDoublyLinkedPosition position,
			final TestableDoublyLinkedPosition expectedPrevious, final List<TestableDoublyLinkedPosition> expectedNext,
			final boolean expectedIsDeleted) {
		assertThat(position.getPrevious()).isSameAs(expectedPrevious);
		assertThat(position.getNext()).containsExactlyElementsOf(expectedNext);
		assertThat(position.isDeleted()).isEqualTo(expectedIsDeleted);
	}

	default <P extends DoublyLinkedPosition<P>> void assertLinked(final List<Pair<P, P>> positions) {
		positions.forEach(this::assertLinked);
	}

	default <P extends DoublyLinkedPosition<P>> void assertLinked(final Pair<P, P> positions) {
		assertLinked(positions.getLeft(), positions.getRight());
	}

	default <P extends DoublyLinkedPosition<P>> void assertLinked(final P previous, final P next) {
		assertLinkedToNextOnly(previous, next);
		assertLinkedToPreviousOnly(next, previous);
	}

	default void assertLinkedToPreviousOnly(final DoublyLinkedPosition<?> next,
			final DoublyLinkedPosition<?> previous) {
		assertLinkedToPrevious(next, previous);
		assertNotLinkedToNext(next);
	}

	default <P extends DoublyLinkedPosition<P>> void assertLinkedToNextOnly(final P previous, final P next) {
		assertNotLinkedToPrevious(previous);
		assertLinkedToNext(previous, next);
	}

	/**
	 * Alias for {@link #assertNotLinked(List)}
	 */
	default void assertUnlinked(final List<? extends DoublyLinkedPosition<?>> positions) {
		assertNotLinked(positions);
	}

	default void assertNotLinked(final List<? extends DoublyLinkedPosition<?>> positions) {
		positions.forEach(this::assertNotLinked);
	}

	default void assertNotLinked(final DoublyLinkedPosition<?> position) {
		assertNotLinkedToPrevious(position);
		assertNotLinkedToNext(position);
	}

	default <P extends DoublyLinkedPosition<P>> void assertLinkedToNext(final P previous, final P next) {
		assertThat(previous.getNext()).containsExactly(next);
	}

	default void assertNotLinkedToNext(final DoublyLinkedPosition<?> position) {
		assertThat(position.getNext()).isEmpty();
	}

	default void assertLinkedToPrevious(final DoublyLinkedPosition<?> next, final DoublyLinkedPosition<?> previous) {
		assertThat(next.getPrevious()).isSameAs(previous);
	}

	default void assertNotLinkedToPrevious(final DoublyLinkedPosition<?> position) {
		assertThat(position.getPrevious()).isNull();
	}

}
