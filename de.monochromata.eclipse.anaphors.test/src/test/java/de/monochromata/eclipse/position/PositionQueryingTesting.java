package de.monochromata.eclipse.position;

import static org.mockito.Mockito.when;

import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMember;
import org.eclipse.jdt.core.ISourceRange;
import org.eclipse.jdt.core.ISourceReference;
import org.eclipse.jdt.core.JavaModelException;

public interface PositionQueryingTesting {

	default void stubSourceReferenceToReturnSourceRange(final ISourceRange sourceRange,
			final ISourceReference sourceReference) throws JavaModelException {
		when(sourceReference.exists()).thenReturn(true);
		when(sourceReference.getSourceRange()).thenReturn(sourceRange);
	}

	default void stubSourceReferenceToReturnParentButNoSourceReference(final IJavaElement parent, final IMember member)
			throws JavaModelException {
		when(member.exists()).thenReturn(false);
		when(member.getSourceRange()).thenReturn(null);
		when(member.getParent()).thenReturn(parent);
	}

}
