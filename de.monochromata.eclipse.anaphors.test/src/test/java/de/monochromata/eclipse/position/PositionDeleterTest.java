package de.monochromata.eclipse.position;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.atomic.AtomicBoolean;

import org.eclipse.jface.text.Position;
import org.junit.Test;

public class PositionDeleterTest {

    private final AtomicBoolean deleteInvoked = new AtomicBoolean(false);
    private final PositionDeleter deleter = (position, document, deletionStrategy) -> deleteInvoked.set(true);

    @Test
    public void doDeleteNotYetDeletedPositions() {
        final Position position = new Position(10, 20);
        deleter.deletePositionIfNotYetDeleted(position, null, null);

        assertThat(deleteInvoked.get()).isTrue();
    }

    @Test
    public void doNotDeleteAlreadyDeletedPositionAgain() {
        final Position position = new Position(10, 20);
        position.delete();
        deleter.deletePositionIfNotYetDeleted(position, null, null);

        assertThat(deleteInvoked.get()).isFalse();
    }
}
