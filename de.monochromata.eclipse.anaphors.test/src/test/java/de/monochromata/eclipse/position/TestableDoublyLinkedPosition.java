package de.monochromata.eclipse.position;

import static java.util.Collections.singletonList;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.Position;

public class TestableDoublyLinkedPosition extends Position
        implements DoublyLinkedPosition<TestableDoublyLinkedPosition> {

    public TestableDoublyLinkedPosition previous;
    public List<TestableDoublyLinkedPosition> next;

    public TestableDoublyLinkedPosition(final TestableDoublyLinkedPosition previous,
            final TestableDoublyLinkedPosition next) {
        this(0, 0, previous, singletonList(next));
    }

    public TestableDoublyLinkedPosition(final TestableDoublyLinkedPosition previous,
            final List<TestableDoublyLinkedPosition> next) {
        this(0, 0, previous, next);
    }

    public TestableDoublyLinkedPosition(final int offset, final int length, final TestableDoublyLinkedPosition previous,
            final TestableDoublyLinkedPosition next) {
        this(offset, length, previous, singletonList(next));
    }

    public TestableDoublyLinkedPosition(final int offset, final int length, final TestableDoublyLinkedPosition previous,
            final List<TestableDoublyLinkedPosition> next) {
        super(offset, length);
        this.previous = previous;
        this.next = new ArrayList<>(next);
    }

    @Override
    public TestableDoublyLinkedPosition getPrevious() {
        return previous;
    }

    @Override
    public void setPrevious(final TestableDoublyLinkedPosition previous) {
        this.previous = previous;
    }

    @Override
    public List<TestableDoublyLinkedPosition> getNext() {
        return next;
    }

    @Override
    public void addNext(final TestableDoublyLinkedPosition next) {
        this.next.add(next);
    }

    @Override
    public void addNext(final List<TestableDoublyLinkedPosition> next) {
        this.next.addAll(next);
    }

    @Override
    public void removeNext(final TestableDoublyLinkedPosition next) {
        this.next.remove(next);
    }

    @Override
    public void removeNext(final List<TestableDoublyLinkedPosition> next) {
        this.next.removeAll(next);
    }

}
