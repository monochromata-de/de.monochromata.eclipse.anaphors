package de.monochromata.eclipse.position;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.junit.Test;

import de.monochromata.function.TriConsumer;

public class DoublyLinkedPositionUpdaterTest implements DoublyLinkedPositionTesting, PositionsTesting {

	@Test
	public void informsPreDeletionListener() throws Exception {
		final AtomicInteger invocationCounter = new AtomicInteger();
		final TriConsumer<TestableDoublyLinkedPosition, IDocument, Void> preDeleteListener = (unused0, unused1,
				unused2) -> invocationCounter.incrementAndGet();
		final DoublyLinkedPositionUpdater<TestableDoublyLinkedPosition, Void> updater = new DoublyLinkedPositionUpdater<>(
				TEST_POSITION_CATEGORY, preDeleteListener, null);
		final Document document = createConfiguredDocument("Hello world!", updater);
		addPosition(linkedPosition(6, "world".length()), document);
		assertThat(invocationCounter.get()).isEqualTo(0);

		document.replace(6, "world".length(), "");

		assertThat(invocationCounter.get()).isEqualTo(1);
	}

	@Test
	public void informsPreDeletionListenerBeforeAPositionIsDeleted() throws Exception {
		// The position delivered to the listener should not have been deleted yet.
		final Document document = createConfiguredDocument("Hello world!",
				new DoublyLinkedPositionUpdater<>(TEST_POSITION_CATEGORY,
						(position, unused, deletionStrategy) -> assertThat(((Position) position).isDeleted()).isFalse(),
						null));
		addPosition(linkedPosition(6, "world".length()), document);

		document.replace(6, "world".length(), "");
	}

	@Test
	public void maintainChainYieldsNoExceptionIfThePositionIsNotLinkedToOthers() {
		maintainChain(linkedPosition());
	}

	@Test
	public void maintainChainDeletesNextElementIfFirstPositionIsDeletedAndTheTreeHasDepth2With1Leaf() throws Exception {
		final Document document = createConfiguredDocument("Hello world!",
				new DoublyLinkedPositionUpdater<>(TEST_POSITION_CATEGORY, maintainChainForTestCategory(), null));

		final TestableDoublyLinkedPosition firstPosition = linkedPosition(0, 5);
		final TestableDoublyLinkedPosition lastPosition = linkedPosition(6, 5, firstPosition, emptyList());
		addPosition(firstPosition, document);
		addPosition(lastPosition, document);

		document.replace(0, 5, "");

		assertThat(document.getPositions(TEST_POSITION_CATEGORY)).isEmpty();
		assertPosition(firstPosition, null, emptyList(), true);
		assertPosition(lastPosition, null, emptyList(), true);
	}

	@Test
	public void maintainChainDeletesNextElementIfFirstPositionIsDeletedAndTheTreeHasDepth2With2Leaves()
			throws Exception {
		final Document document = createConfiguredDocument("Hello world beams!",
				new DoublyLinkedPositionUpdater<>(TEST_POSITION_CATEGORY, maintainChainForTestCategory(), null));

		final TestableDoublyLinkedPosition firstPosition = linkedPosition(0, 5);
		final TestableDoublyLinkedPosition lastPosition1 = linkedPosition(6, 5, firstPosition, emptyList());
		final TestableDoublyLinkedPosition lastPosition2 = linkedPosition(12, 5, firstPosition, emptyList());
		addPosition(firstPosition, document);
		addPosition(lastPosition1, document);
		addPosition(lastPosition2, document);

		document.replace(0, 5, "");

		assertThat(document.getPositions(TEST_POSITION_CATEGORY)).isEmpty();
		assertPosition(firstPosition, null, emptyList(), true);
		assertPosition(lastPosition1, null, emptyList(), true);
		assertPosition(lastPosition2, null, emptyList(), true);
	}

	@Test
	public void deletePositionDeletesNextElementIfFirstPositionIsDeletedAndTheTreeHasDepth2With1Leaf()
			throws Exception {
		final DoublyLinkedPositionUpdater<TestableDoublyLinkedPosition, Void> updater = new DoublyLinkedPositionUpdater<>(
				TEST_POSITION_CATEGORY, maintainChainForTestCategory(), null);
		final Document document = createConfiguredDocument("Hello world!", updater);

		final TestableDoublyLinkedPosition firstPosition = linkedPosition(0, 5);
		final TestableDoublyLinkedPosition lastPosition = linkedPosition(6, 5, firstPosition, emptyList());
		addPosition(firstPosition, document);
		addPosition(lastPosition, document);

		updater.deletePosition(firstPosition, document);

		assertThat(document.getPositions(TEST_POSITION_CATEGORY)).isEmpty();
		assertPosition(firstPosition, null, emptyList(), true);
		assertPosition(lastPosition, null, emptyList(), true);
	}

	@Test
	public void deletePositionDeletesNextElementIfFirstPositionIsDeletedAndTheTreeHasDepth2With2Leaves()
			throws Exception {
		final DoublyLinkedPositionUpdater<TestableDoublyLinkedPosition, Void> updater = new DoublyLinkedPositionUpdater<>(
				TEST_POSITION_CATEGORY, maintainChainForTestCategory(), null);
		final Document document = createConfiguredDocument("Hello world beams!", updater);

		final TestableDoublyLinkedPosition firstPosition = linkedPosition(0, 5);
		final TestableDoublyLinkedPosition lastPosition1 = linkedPosition(6, 5, firstPosition, emptyList());
		final TestableDoublyLinkedPosition lastPosition2 = linkedPosition(12, 5, firstPosition, emptyList());
		addPosition(firstPosition, document);
		addPosition(lastPosition1, document);
		addPosition(lastPosition2, document);

		updater.deletePosition(firstPosition, document);

		assertThat(document.getPositions(TEST_POSITION_CATEGORY)).isEmpty();
		assertPosition(firstPosition, null, emptyList(), true);
		assertPosition(lastPosition1, null, emptyList(), true);
		assertPosition(lastPosition2, null, emptyList(), true);
	}

	@Test
	public void maintainChainUnlinksNextElementIfFirstPositionIsDeletedAndTheTreeHasDepth3With1IntermediateNode() {
		final TestableDoublyLinkedPosition firstPosition = linkedPosition();
		final TestableDoublyLinkedPosition secondPosition = linkedPosition(firstPosition, emptyList());
		final TestableDoublyLinkedPosition lastPosition = linkedPosition(secondPosition, emptyList());

		maintainChain(firstPosition);

		// isDeleted would be set to true by updater, not by listener
		assertPosition(firstPosition, null, emptyList(), false);
		assertPosition(secondPosition, null, lastPosition, false);
		assertPosition(lastPosition, secondPosition, emptyList(), false);
	}

	@Test
	public void maintainChainUnlinksNextElementsIfFirstPositionIsDeletedAndTheTreeHasDepth3With2IntermediateNodes() {
		final TestableDoublyLinkedPosition firstPosition = linkedPosition();
		final TestableDoublyLinkedPosition secondPosition1 = linkedPosition(firstPosition, emptyList());
		final TestableDoublyLinkedPosition secondPosition2 = linkedPosition(firstPosition, emptyList());
		final TestableDoublyLinkedPosition lastPosition = linkedPosition(secondPosition1, emptyList());

		maintainChain(firstPosition);

		// isDeleted would be set to true by updater, not by listener
		assertPosition(firstPosition, null, emptyList(), false);
		assertPosition(secondPosition1, null, lastPosition, false);
		assertPosition(secondPosition2, null, emptyList(), false);
		assertPosition(lastPosition, secondPosition1, emptyList(), false);
	}

	@Test
	public void maintainChainDeletesPreviousElementIfLastPositionIsDeletedAndTheTreeHasDepth2With1Leaf()
			throws Exception {
		final Document document = createConfiguredDocument("Hello world!",
				new DoublyLinkedPositionUpdater<>(TEST_POSITION_CATEGORY, maintainChainForTestCategory(), null));

		final TestableDoublyLinkedPosition firstPosition = linkedPosition(0, 5);
		final TestableDoublyLinkedPosition lastPosition = linkedPosition(6, 5, firstPosition, emptyList());
		addPosition(firstPosition, document);
		addPosition(lastPosition, document);

		document.replace(6, 5, "");

		assertThat(document.getPositions(TEST_POSITION_CATEGORY)).isEmpty();
		assertPosition(firstPosition, null, emptyList(), true);
		assertPosition(lastPosition, null, emptyList(), true);
	}

	@Test
	public void maintainChainRetainsPreviousElementIfOneLeafIsDeletedAndTheTreeHasDepth2With2Leaves() throws Exception {
		final Document document = createConfiguredDocument("Hello world beams!",
				new DoublyLinkedPositionUpdater<>(TEST_POSITION_CATEGORY, maintainChainForTestCategory(), null));

		final TestableDoublyLinkedPosition firstPosition = linkedPosition(0, 5);
		final TestableDoublyLinkedPosition lastPosition1 = linkedPosition(6, 5, firstPosition, emptyList());
		final TestableDoublyLinkedPosition lastPosition2 = linkedPosition(12, 5, firstPosition, emptyList());
		addPosition(firstPosition, document);
		addPosition(lastPosition1, document);
		addPosition(lastPosition2, document);

		document.replace(6, 5, "");

		assertThat(document.getPositions(TEST_POSITION_CATEGORY)).containsExactly(firstPosition, lastPosition2);
		assertPosition(firstPosition, null, lastPosition2, false);
		assertPosition(lastPosition1, null, emptyList(), true);
		assertPosition(lastPosition2, firstPosition, emptyList(), false);
	}

	@Test
	public void deletePositionDeletesPreviousElementIfLastPositionIsDeletedAndTheTreeHasDepth2With1Leaf()
			throws Exception {
		final DoublyLinkedPositionUpdater<TestableDoublyLinkedPosition, Void> updater = new DoublyLinkedPositionUpdater<>(
				TEST_POSITION_CATEGORY, maintainChainForTestCategory(), null);
		final Document document = createConfiguredDocument("Hello world!", updater);

		final TestableDoublyLinkedPosition firstPosition = linkedPosition(0, 5);
		final TestableDoublyLinkedPosition lastPosition = linkedPosition(6, 5, firstPosition, emptyList());
		addPosition(firstPosition, document);
		addPosition(lastPosition, document);

		updater.deletePosition(lastPosition, document);

		assertThat(document.getPositions(TEST_POSITION_CATEGORY)).isEmpty();
		assertPosition(firstPosition, null, emptyList(), true);
		assertPosition(lastPosition, null, emptyList(), true);
	}

	@Test
	public void deletePositionRetainsPreviousElementIfOneLeafIsDeletedAndTheTreeHasDepth2With2Leaves()
			throws Exception {
		final DoublyLinkedPositionUpdater<TestableDoublyLinkedPosition, Void> updater = new DoublyLinkedPositionUpdater<>(
				TEST_POSITION_CATEGORY, maintainChainForTestCategory(), null);
		final Document document = createConfiguredDocument("Hello world beams!", updater);

		final TestableDoublyLinkedPosition firstPosition = linkedPosition(0, 5);
		final TestableDoublyLinkedPosition lastPosition1 = linkedPosition(6, 5, firstPosition, emptyList());
		final TestableDoublyLinkedPosition lastPosition2 = linkedPosition(12, 5, firstPosition, emptyList());
		addPosition(firstPosition, document);
		addPosition(lastPosition1, document);
		addPosition(lastPosition2, document);

		updater.deletePosition(lastPosition1, document);

		assertThat(document.getPositions(TEST_POSITION_CATEGORY)).containsExactly(firstPosition, lastPosition2);
		assertPosition(firstPosition, null, lastPosition2, false);
		assertPosition(lastPosition1, null, emptyList(), true);
		assertPosition(lastPosition2, firstPosition, emptyList(), false);
	}

	@Test
	public void maintainChainUnlinksPreviousElementIfLastPositionIsDeletedAndTheTreeHasDepth3With1Leaf() {
		final TestableDoublyLinkedPosition firstPosition = linkedPosition();
		final TestableDoublyLinkedPosition secondPosition = linkedPosition(firstPosition, emptyList());
		final TestableDoublyLinkedPosition lastPosition = linkedPosition(secondPosition, emptyList());

		maintainChain(lastPosition);

		assertPosition(firstPosition, null, secondPosition, false);
		assertPosition(secondPosition, firstPosition, emptyList(), false);
		// isDeleted would be set to true by updater, not by listener
		assertPosition(lastPosition, null, emptyList(), false);
	}

	@Test
	public void maintainChainUnlinksParentOfDeletedLeafIfOneLeafIsDeletedAndTheTreeHasDepth3With2Leaves() {
		final TestableDoublyLinkedPosition firstPosition = linkedPosition();
		final TestableDoublyLinkedPosition secondPosition = linkedPosition(firstPosition, emptyList());
		final TestableDoublyLinkedPosition lastPosition1 = linkedPosition(secondPosition, emptyList());
		final TestableDoublyLinkedPosition lastPosition2 = linkedPosition(secondPosition, emptyList());

		maintainChain(lastPosition1);

		assertPosition(firstPosition, null, secondPosition, false);
		assertPosition(secondPosition, firstPosition, lastPosition2, false);
		// isDeleted would be set to true by updater, not by listener
		assertPosition(lastPosition1, null, emptyList(), false);
		assertPosition(lastPosition2, secondPosition, emptyList(), false);
	}

	@Test
	public void maintainChainRemovesIntermediatePositionFromChainWhenTreeHasDepth3And1IntermediatePosition() {
		final TestableDoublyLinkedPosition firstPosition = linkedPosition();
		final TestableDoublyLinkedPosition secondPosition = linkedPosition(firstPosition, emptyList());
		final TestableDoublyLinkedPosition lastPosition = linkedPosition(secondPosition, emptyList());

		maintainChain(secondPosition);

		assertPosition(firstPosition, null, lastPosition, false);
		// isDeleted would be set to true by updater, not by listener
		assertPosition(secondPosition, null, emptyList(), false);
		assertPosition(lastPosition, firstPosition, emptyList(), false);
	}

	@Test
	public void maintainChainRemovesIntermediatePositionFromChainWhenTreeHasDepth3And1IntermediatePositionAnd2Leaves() {
		final TestableDoublyLinkedPosition firstPosition = linkedPosition();
		final TestableDoublyLinkedPosition secondPosition = linkedPosition(firstPosition, emptyList());
		final TestableDoublyLinkedPosition lastPosition1 = linkedPosition(10, 1, secondPosition, emptyList());
		final TestableDoublyLinkedPosition lastPosition2 = linkedPosition(20, 2, secondPosition, emptyList());

		maintainChain(secondPosition);

		assertPosition(firstPosition, null, asList(lastPosition1, lastPosition2), false);
		// isDeleted would be set to true by updater, not by listener
		assertPosition(secondPosition, null, emptyList(), false);
		assertPosition(lastPosition1, firstPosition, emptyList(), false);
		assertPosition(lastPosition2, firstPosition, emptyList(), false);
	}

	@Test
	public void maintainChainRemovesDeletedIntermediatePositionFromChainWhenTreeHasDepth3And2IntermediatePositionsWith1LeafEach() {
		final TestableDoublyLinkedPosition firstPosition = linkedPosition();
		final TestableDoublyLinkedPosition secondPosition1 = linkedPosition(10, 1, firstPosition, emptyList());
		final TestableDoublyLinkedPosition secondPosition2 = linkedPosition(20, 2, firstPosition, emptyList());
		final TestableDoublyLinkedPosition lastPosition1 = linkedPosition(110, 1, secondPosition1, emptyList());
		final TestableDoublyLinkedPosition lastPosition2 = linkedPosition(120, 2, secondPosition2, emptyList());

		maintainChain(secondPosition1);

		assertPosition(firstPosition, null, asList(secondPosition2, lastPosition1), false);
		// isDeleted would be set to true by updater, not by listener
		assertPosition(secondPosition1, null, emptyList(), false);
		assertPosition(secondPosition2, firstPosition, lastPosition2, false);
		assertPosition(lastPosition1, firstPosition, emptyList(), false);
		assertPosition(lastPosition2, secondPosition2, emptyList(), false);
	}

}
