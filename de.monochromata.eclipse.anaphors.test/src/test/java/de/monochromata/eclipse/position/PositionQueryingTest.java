package de.monochromata.eclipse.position;

import static org.assertj.core.api.Assertions.assertThat;

import org.eclipse.jface.text.Position;
import org.junit.Test;

import de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPosition;

public class PositionQueryingTest implements PositionQueryingTesting {

    @Test
    public void getCoveredPositionsMustNotReturnDuplicatePositions() {
        final Position position0 = new Position(0, 10);
        final Position position1 = new Position(0, 10);
        final MemberTrackingPosition trackingPosition = new MemberTrackingPosition(0, 10, null);

        assertThat(
                PositionQuerying.getCoveredPositions(trackingPosition, new Position[] { position0, position1 }))
                        .containsExactly(position0);
    }

}
