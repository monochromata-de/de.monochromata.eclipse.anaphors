package de.monochromata.eclipse.position;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPositionCategoryException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IPositionUpdater;
import org.eclipse.jface.text.Position;

public interface PositionsTesting {

    String TEST_POSITION_CATEGORY = PositionsTesting.class.getPackage().getName();

    default void addPosition(final int start, final String code, final Document document)
            throws BadLocationException, BadPositionCategoryException {
        final Position position = new Position(start, code.length());
        addPosition(position, document);
    }

    default void addPosition(final Position position, final Document document)
            throws BadLocationException, BadPositionCategoryException {
        document.addPosition(TEST_POSITION_CATEGORY, position);
    }

    default Document createConfiguredDocument(final String documentText, final IPositionUpdater positionUpdater) {
        final Document document = new Document(documentText);
        document.addPositionCategory(TEST_POSITION_CATEGORY);
        document.addPositionUpdater(positionUpdater);
        return document;
    }

    default void assertCorrectPositions(final Document document,
            final int expectedOffsetBeforeStartOfRelatedExpression, final int expectedOffsetAfterEndOfRelatedExpression,
            final int expectedOffsetBeforeStartOfAnaphor, final int expectedOffsetAfterEndOfAnaphor) throws Exception {
        assertPositions(document,
                new Position(expectedOffsetBeforeStartOfRelatedExpression,
                        expectedOffsetAfterEndOfRelatedExpression - expectedOffsetBeforeStartOfRelatedExpression),
                new Position(expectedOffsetBeforeStartOfAnaphor,
                        expectedOffsetAfterEndOfAnaphor - expectedOffsetBeforeStartOfAnaphor));
    }

    default <T extends Position> void assertPositions(final IDocument document, final T... expectedPositions)
            throws Exception {
        assertPositions(document, TEST_POSITION_CATEGORY, asList(expectedPositions));
    }

    default <T extends Position> void assertPositions(final IDocument document, final String positionsCategory,
            final T... expectedPositions) throws Exception {
        assertPositions(document, positionsCategory, asList(expectedPositions));
    }

    default <T extends Position> void assertPositions(final IDocument document, final List<T> expectedPositions)
            throws BadPositionCategoryException {
        assertPositions(document, TEST_POSITION_CATEGORY, expectedPositions);
    }

    default <T extends Position> void assertPositions(final IDocument document, final String positionsCategory,
            final List<T> expectedPositions) throws BadPositionCategoryException {
        final List<Position> positions = asList(document.getPositions(positionsCategory));
        assertPositions(positions, expectedPositions);
    }

    default <T extends Position> void assertGivenPositions(final IDocument document, final List<T> expectedPositions)
            throws BadPositionCategoryException {
        final List<Position> positions = asList(document.getPositions(TEST_POSITION_CATEGORY));
        assertGivenPositions(positions, expectedPositions);
    }

    default <T extends Position> void assertPositions(final List<Position> positions, final List<T> expectedPositions) {
        assertThat(positions).containsExactlyElementsOf(expectedPositions);
    }

    default <T extends Position> void assertGivenPositions(final List<Position> positions,
            final List<T> expectedPositions) {
        assertThat(positions).containsAll(expectedPositions);
    }

}
