package de.monochromata.contract.environment.junit;

import static java.util.Arrays.stream;
import static java.util.Optional.empty;
import static java.util.stream.Collectors.joining;

import java.lang.reflect.Method;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Stream;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.IPackageBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpressionsCollector;
import org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteFlattener;
import org.eclipse.jdt.internal.corext.dom.HierarchicalASTVisitor;
import org.eclipse.jdt.internal.ui.javaeditor.PositionCollectorCore;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.monochromata.anaphors.ast.reference.strategy.concept.Hyponymy;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.preferences.Preferences;
import de.monochromata.contract.config.Configuration;
import de.monochromata.contract.environment.direct.consumer.Instantiation;
import de.monochromata.contract.environment.direct.consumer.Persistence;
import de.monochromata.contract.execution.ExecutionContext;
import de.monochromata.contract.execution.RecordingContainerExecution;
import de.monochromata.contract.transformation.ReturnValueTransformation;
import de.monochromata.eclipse.anaphors.Activator;
import de.monochromata.eclipse.anaphors.contract.ContractTesting;
import de.monochromata.eclipse.anaphors.preferences.AnaphorsPreferences;

// TODO: Move to java-contract
public class FutureJunitContractExtension {

    private static Pattern MockitoMockNamePattern = Pattern
            .compile("^(?<nameOfMockedType>.+\\..+)\\$MockitoMock\\$.+$");

    private static final List<RecordingContainerExecution<?>> executionsToSave = new ArrayList<>();

    private static final List<ReturnValueTransformation> recordingTransformations = List.of();
    private static final List<Predicate<Class<?>>> providerTypesToRedefineDuringCapture = List.of(
            sourceType -> sourceType.getPackageName().startsWith(AST.class.getPackageName())
                    && !sourceType.getName().equals("org.eclipse.jdt.core.dom.ASTNode$NodeList")
                    && !sourceType.getName().contains("$MockitoMock$"),
            /*
             * sourceType -> ASTNode.class.isAssignableFrom(sourceType), sourceType ->
             * sourceType.equals(ASTRewrite.class),
             */
            // TODO: Add hint redefine to exception thrown when following type name is not
            // here:
            sourceType -> sourceType.equals(RelatedExpression.class),
            sourceType -> sourceType.getName().equals(PublicRelatedExpressionsCollector.class.getName() + "$Visitor"));
    private static final List<UnaryOperator<Class<?>>> proxyTypeTranslations = List.of(
            // TODO: Is this translation still required?
            sourceType -> sourceType.getName().equals("org.eclipse.jdt.core.dom.TypeBinding") ? ITypeBinding.class
                    : sourceType,
            sourceType -> sourceType.getName().equals("org.eclipse.jdt.core.dom.MethodBinding") ? IMethodBinding.class
                    : sourceType,
            sourceType -> sourceType.getName().equals("org.eclipse.jdt.core.dom.PackageBinding") ? IPackageBinding.class
                    : sourceType,
            sourceType -> sourceType.getName().equals("org.eclipse.jdt.core.dom.VariableBinding")
                    ? IVariableBinding.class
                    : sourceType,
            // TODO: This translation might not be correct because ASTNode$NodeList adds
            // methods not defined by AbstractList/List
            sourceType -> sourceType.getName().equals("org.eclipse.jdt.core.dom.ASTNode$NodeList") ? AbstractList.class
                    : sourceType,
            sourceType -> sourceType.getName().equals("org.eclipse.jdt.core.dom.NodeFinder$NodeFinderVisitor")
                    ? ASTVisitor.class
                    : sourceType,
            sourceType -> sourceType.getName()
                    .equals("org.eclipse.jdt.internal.corext.dom.ScopeAnalyzer$ScopeAnalyzerVisitor")
                            ? HierarchicalASTVisitor.class
                            : sourceType,
            sourceType -> sourceType.getName()
                    .equals("org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteFormatter$ExtendedFlattener")
                            ? ASTRewriteFlattener.class
                            : sourceType,
            sourceType -> sourceType.getName()
                    .equals("org.eclipse.jdt.internal.ui.javaeditor.OverrideIndicatorManager$1") ? ASTVisitor.class
                            : sourceType,
            sourceType -> sourceType.getName()
                    .equals("org.eclipse.jdt.internal.ui.javaeditor.SemanticHighlightingReconciler$PositionCollector")
                            ? PositionCollectorCore.class
                            : sourceType,
            sourceType -> sourceType.getName().equals("java.util.AbstractList$Itr") ? Iterator.class : sourceType,
            sourceType -> sourceType.getName().equals("java.util.ArrayList$Itr") ? Iterator.class : sourceType,
            sourceType -> sourceType.getName().equals("java.util.Arrays$ArrayList") ? List.class : sourceType,
            sourceType -> sourceType.getName().equals("java.util.Collections$EmptyList") ? List.class : sourceType,
            sourceType -> sourceType.getName().equals("java.util.Collections$UnmodifiableSet") ? Set.class : sourceType,
            sourceType -> sourceType.getName().equals("java.util.Collections$UnmodifiableRandomAccessList") ? List.class
                    : sourceType,
            sourceType -> sourceType.getName().equals("java.util.stream.Collectors$CollectorImpl") ? Collector.class
                    : sourceType,
            sourceType -> sourceType.getName().equals("java.util.stream.ReferencePipeline$Head") ? Stream.class
                    : sourceType,
            sourceType -> sourceType.getName().equals("java.util.stream.ReferencePipeline$1") ? Stream.class
                    : sourceType,
            sourceType -> sourceType.getName().equals("java.util.stream.ReferencePipeline$2") ? Stream.class
                    : sourceType,
            sourceType -> sourceType.getName().equals("java.util.stream.ReferencePipeline$3") ? Stream.class
                    : sourceType);

    private static final List<UnaryOperator<Class<?>>> recordingTypeTranslations = List
            .of(excludeMatchingTypesFromPact(typeName -> MockitoMockNamePattern.matcher(typeName).matches(),
                    Optional.of("org.mockito.internal."), sourceType -> {
                        throw new IllegalArgumentException("Could not translate " + sourceType
                                + ": it has no super class and implements no or more than one interfaces.");
                    }),
                    recursivelyExcludeMatchingTypesFromPact(typeName -> typeName.startsWith("org.eclipse"),
                            Optional.empty(), unused -> Object.class),
                    sourceType -> sourceType.equals(Activator.HyponymyUnderDevelopment.class) ? Hyponymy.class
                            : sourceType,
                    sourceType -> sourceType.equals(AnaphorsPreferences.class) ? Preferences.class : sourceType,
                    sourceType -> sourceType.getName().equals("java.util.Collections$UnmodifiableRandomAccessList")
                            ? List.class
                            : sourceType);
    private final Class<?> testClass;
    private final Configuration config;
    private final ExecutionContext executionContext;

    public FutureJunitContractExtension(final Class<?> testClass) {
        this.testClass = testClass;
        this.config = new Configuration(ContractTesting.IO_CONFIG, recordingTransformations,
                providerTypesToRedefineDuringCapture, proxyTypeTranslations, recordingTypeTranslations,
                interactionDescriptionContribution(testClass));
        this.executionContext = ExecutionContext.of(config);
    }

    // TODO: Move to java-contract as default recording type translation?
    // TODO: Add JavaDoc for handlerForJavaLangObject
    // TODO: Further abstract interfaceToExclude
    private static UnaryOperator<Class<?>> recursivelyExcludeMatchingTypesFromPact(final Predicate<String> predicate,
            final Optional<String> interfaceToExclude, final UnaryOperator<Class<?>> handlerForJavaLangObject) {
        return sourceType -> {
            final var translation = excludeMatchingTypesFromPact(predicate, interfaceToExclude,
                    handlerForJavaLangObject);
            var currentType = sourceType;
            var translatedType = sourceType;
            while (!currentType.equals(Object.class)
                    && ((translatedType = translation.apply(currentType)) != currentType)) {
                currentType = translatedType;
            }
            return translatedType;
        };
    }

    // TODO: Move to java-contract as default recording type translation?
    // TODO: Add JavaDoc for handlerForJavaLangObject
    // TODO: Furthher abstract interfaceToExclude
    private static UnaryOperator<Class<?>> excludeMatchingTypesFromPact(final Predicate<String> predicate,
            final Optional<String> interfaceToExclude, final UnaryOperator<Class<?>> handlerForJavaLangObject) {
        return sourceType -> {
            if (predicate.test(sourceType.getName())) {
                final var superclass = sourceType.getSuperclass();
                if (superclass != null && !superclass.equals(Object.class)) {
                    return superclass;
                }
                final var interfaces = filterInterfaces(sourceType, interfaceToExclude);
                if (interfaces.length == 1) {
                    return interfaces[0];
                }
                return handlerForJavaLangObject.apply(sourceType);
            }
            return sourceType;
        };
    }

    private static Class[] filterInterfaces(final Class<?> sourceType, final Optional<String> interfaceToExclude) {
        final var interfaces = Stream.of(sourceType.getInterfaces());
        if (interfaceToExclude.isPresent()) {
            return interfaces.filter(type -> !type.getName().startsWith("org.mockito.internal.")).toArray(Class[]::new);
        }
        return interfaces.toArray(Class[]::new);
    }

    public <S, T extends S> S proxy(final T providerInstance) {
        return pushProxy(
                // TODO: Maybe remove the ExecutionContext parameter from the method and create
                // the execution context in the method body
                Instantiation.objectProxy(providerInstance, "anaphors", "eclipse-anaphors", executionContext));
    }

    // TODO: Use (potentially to-be-added) type information to combine withMock,
    // withSpy, withProxy, ... into with(...) - well, use state that does that ...
    protected static <T> T pushProxy(final RecordingContainerExecution<T> execution) {
        executionsToSave.add(execution);
        return execution.augmentedProviderInstance;
    }

    // TODO: Move to java-contract
    // TODO: See https://gitlab.com/monochromata-de/java-contract/-/issues/73
    // TODO: Support configuration to enable shortened consumer IDs
    // TODO: The configuration should also apply to provider IDs
    protected static Supplier<String> interactionDescriptionContribution(final Class<?> testClass) {
        return () -> {
            for (final StackTraceElement element : Thread.currentThread().getStackTrace()) {
                final var testMethodName = getJunitAnnotatedMethodName(testClass, element);
                if (testMethodName.isPresent()) {
                    return getShortenedName(testClass) + "." + testMethodName.get();
                }
            }
            return testClass.getName();
        };
    }

    public static String getShortenedName(final Class clazz) {
        final var packageName = clazz.getPackageName();
        final var shortenedPackageName = shortenPackageName(packageName);
        final var fullyQualifiedName = clazz.getName();
        return fullyQualifiedName.replace(packageName, shortenedPackageName);
    }

    protected static String shortenPackageName(final String packageName) {
        return stream(packageName.split("\\.")).map(element -> element.subSequence(0, 1)).collect(joining("."));
    }

    protected static Optional<String> getJunitAnnotatedMethodName(final Class testClass,
            final StackTraceElement stackTraceElement) {
        if (stackTraceElement.getClassName().equals(testClass.getName())) {
            final var testMethod = stream(testClass.getDeclaredMethods())
                    .filter(isJunitAnnotatedMethod(stackTraceElement.getMethodName())).findFirst();
            return testMethod.map(Method::getName);
        }
        return empty();
    }

    protected static Predicate<Method> isJunitAnnotatedMethod(final String soughtMethodName) {
        return method -> method.getParameterCount() == 0 && method.getName().equals(soughtMethodName)
                && hasJunitAnnotation(method);
    }

    public static boolean hasJunitAnnotation(final Method method) {
        return method.isAnnotationPresent(Test.class) || method.isAnnotationPresent(Before.class)
                || method.isAnnotationPresent(BeforeClass.class) || method.isAnnotationPresent(After.class)
                || method.isAnnotationPresent(AfterClass.class);
    }

    public void save() {
        Persistence.savePact(executionsToSave, executionContext);
    }

}
