package de.monochromata.function.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import de.monochromata.function.LackOfMethodReferenceIdentityTest;

@RunWith(Suite.class)
@SuiteClasses({
        // Unit tests
        LackOfMethodReferenceIdentityTest.class })
public class AllTests {

}
