package de.monochromata.function;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import org.junit.Test;

public class LackOfMethodReferenceIdentityTest {

    @Test
    public void twoReferencesToTheSameStaticMethodAreNotIdentical() {
        assertThat((Function<String, Integer>) Integer::parseInt)
                .isNotSameAs((Function<String, Integer>) Integer::parseInt);
    }

    @Test
    public void twoReferencesToTheSameInstanceMethodAreNotIdentical() {
        final Integer integer = new Integer(2);
        assertThat((Supplier<Integer>) integer::intValue)
                .isNotSameAs((Supplier<Integer>) integer::intValue);
    }

    @Test
    public void aReferenceToAStaticMethodCanNotBeAddedAndRemovedFromASet() {
        final List<Function<String, Integer>> listeners = new ArrayList<>();
        assertThat(listeners).hasSize(0);

        listeners.add(Integer::parseInt);
        assertThat(listeners).hasSize(1);

        // Remove is ineffective
        listeners.remove((Function<String, Integer>) Integer::parseInt);
        assertThat(listeners).hasSize(1);
    }

    @Test
    public void aReferenceToAStaticMethodCanBeAddedAndRemovedFromASetIfStored() {
        final Function<String, Integer> listener = Integer::parseInt;
        final List<Function<String, Integer>> listeners = new ArrayList<>();
        assertThat(listeners).hasSize(0);

        listeners.add(listener);
        assertThat(listeners).hasSize(1);

        listeners.remove(listener);
        assertThat(listeners).isEmpty();
    }

    @Test
    public void aReferenceToAnInstanceMethodCanNotBeAddedAndRemovedFromASet() {
        final Integer integer = new Integer(2);
        final List<Supplier<Integer>> listeners = new ArrayList<>();
        assertThat(listeners).hasSize(0);

        listeners.add(integer::intValue);
        assertThat(listeners).hasSize(1);

        // Remove is ineffective
        listeners.remove((Supplier<Integer>) integer::intValue);
        assertThat(listeners).hasSize(1);
    }

    @Test
    public void aReferenceToAnInstanceMethodCanBeAddedAndRemovedFromASetIfStored() {
        final Integer integer = new Integer(2);
        final Supplier<Integer> listener = integer::intValue;
        final List<Supplier<Integer>> listeners = new ArrayList<>();
        assertThat(listeners).hasSize(0);

        listeners.add(listener);
        assertThat(listeners).hasSize(1);

        listeners.remove(listener);
        assertThat(listeners).isEmpty();
    }

}
