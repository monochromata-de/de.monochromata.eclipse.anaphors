package org.eclipse.jdt.core.dom.anaphors;

import org.junit.Test;

public class PublicRelatedExpressionsCollectorTest implements PublicRelatedExpressionsCollectorTesting {

    @Test
    public void collectRelatedExpressionBeforeAnaphor() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { Integer.valueOf(0); System.err.println(integer); } }", "integer",
                relatedExpressions -> assertRelatedExpressionCollected(relatedExpressions, "Integer.valueOf(0)"));
    }

    @Test
    public void doNotcollectRelatedExpressionAfterStartPositionOfAnaphor() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { System.err.println(integer); Integer.valueOf(0); } }", "integer",
                this::assertNoRelatedExpressionsCollected);
    }

    @Test
    public void doNotCollectLocalVariableDeclarationFragmentWhoseInitializerContainsTheAnaphor() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo {\n"
                        + "void foo() { final Bar bar = new Bar(integer); }\n"
                        + "static class Bar { Integer barry; Bar(Integer barry) { this.barry = barry; } }\n"
                        + "}",
                "integer",
                this::assertNoRelatedExpressionsCollected);
    }

    @Test
    public void doNotCollectRelatedExpressionFromNonEnclosingLambdaExpression() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { \n"
                        + "java.util.function.Function<String,Integer> function = unused -> Integer.valueOf(1);\n"
                        + "System.err.println(integer); } }",
                "integer",
                // The lambda expression itself is collected, but not its children.
                assertRelatedExpressionsCollected(
                        "java.util.function.Function<String,Integer> function=unused -> Integer.valueOf(1);\n",
                        "function=unused -> Integer.valueOf(1)"));
    }

    @Test
    public void collectRelatedExpressionFromEnclosingLambdaExpression() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo {\n"
                        + "  void foo() {\n"
                        + "    java.util.function.Function<String,Integer> function = unused -> {\n"
                        + "      Integer.valueOf(0);\n"
                        + "      return integer;\n"
                        + "    };\n"
                        + "  }\n"
                        + "}",
                "integer",
                assertRelatedExpressionsCollected("unused", "Integer.valueOf(0)"));
    }

    @Test
    public void doNotCollectRelatedExpressionFromNonEnclosingBlock() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { \n"
                        + "{ Integer.valueOf(0); }\n"
                        + "System.err.println(integer); } }",
                "integer",
                this::assertNoRelatedExpressionsCollected);
    }

    @Test
    public void collectRelatedExpressionFromEnclosingBlock() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { \n"
                        + "{ Integer.valueOf(0); System.err.println(integer); }\n"
                        + "} }",
                "integer",
                assertRelatedExpressionsCollected("Integer.valueOf(0)"));
    }

    @Test
    public void doNotCollectRelatedExpressionFromNonEnclosingDoStatement() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { \n"
                        + "do { Integer.valueOf(0); } while (Integer.valueOf(1) != null);\n"
                        + "System.err.println(integer); } }",
                "integer",
                this::assertNoRelatedExpressionsCollected);
    }

    @Test
    public void collectRelatedExpressionFromEnclosingDoStatement() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { \n"
                        + "do { Integer.valueOf(0); System.err.println(integer); } while (Integer.valueOf(1) != null);\n"
                        + "} }",
                "integer",
                assertRelatedExpressionsCollected("Integer.valueOf(0)"));
    }

    @Test
    public void doNotCollectRelatedExpressionFromNonEnclosingEnhancedForStatement() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { \n"
                        + "for(final Integer i: java.util.Collections.<Integer>emptyList()) { Integer.valueOf(0); }\n"
                        + "System.err.println(integer); } }",
                "integer",
                this::assertNoRelatedExpressionsCollected);
    }

    @Test
    public void collectRelatedExpressionFromEnclosingEnhancedForStatement() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { \n"
                        + "for(final Integer i: java.util.Collections.<Integer>emptyList()) { Integer.valueOf(0); System.err.println(integer); }\n"
                        + "} }",
                "integer",
                assertRelatedExpressionsCollected("final Integer i",
                		"java.util.Collections.<Integer>emptyList()",
                		"Integer.valueOf(0)"));
    }

    @Test
    public void doNotCollectRelatedExpressionFromNonEnclosingForStatement() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { \n"
                        + "for(final int i=Integer.valueOf(1);i<10;i++) { Integer.valueOf(0); }\n"
                        + "System.err.println(integer); } }",
                "integer",
                this::assertNoRelatedExpressionsCollected);
    }

    @Test
    public void collectRelatedExpressionFromEnclosingForStatement() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { \n"
                        + "for(final int i=Integer.valueOf(1);i<10;i++) { Integer.valueOf(0); System.err.println(integer); }\n"
                        + "} }",
                "integer",
                assertRelatedExpressionsCollected(/* "final int i=new Integer(1)", */"Integer.valueOf(0)"));
    }

    @Test
    public void doNotCollectRelatedExpressionFromNonEnclosingIfStatement() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { \n"
                        + "if(0<Integer.valueOf(1)) { Integer.valueOf(0); }\n"
                        + "System.err.println(integer); } }",
                "integer",
                this::assertNoRelatedExpressionsCollected);
    }

    @Test
    public void collectRelatedExpressionFromEnclosingIfStatement() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { \n"
                        + "if(0<Integer.valueOf(1)) { Integer.valueOf(0); System.err.println(integer); }\n"
                        + "} }",
                "integer",
                assertRelatedExpressionsCollected("Integer.valueOf(1)", "Integer.valueOf(0)"));
    }

    @Test
    public void doNotCollectRelatedExpressionFromNonEnclosingSwitchStatement() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { \n"
                        + "switch(\"foo\") { case \"foo\": Integer.valueOf(0); }\n"
                        + "System.err.println(integer); } }",
                "integer",
                this::assertNoRelatedExpressionsCollected);
    }

    @Test
    public void collectRelatedExpressionFromEnclosingSwitchStatement() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { \n"
                        + "switch(\"foo\") { case \"foo\": Integer.valueOf(0); System.err.println(integer); }\n"
                        + "} }",
                "integer",
                assertRelatedExpressionsCollected("Integer.valueOf(0)"));
    }

    @Test
    public void doNotCollectRelatedExpressionFromNonEnclosingTryStatement() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { \n"
                        + "try { Integer.valueOf(0); } catch (final RuntimeException e) { Integer.valueOf(1); } finally { Integer.valueOf(2); }\n"
                        + "System.err.println(integer); } }",
                "integer",
                this::assertNoRelatedExpressionsCollected);
    }

    @Test
    public void collectRelatedExpressionFromEnclosingTryStatement() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { \n"
                        + "try { Integer.valueOf(0); } catch (final RuntimeException e) { Integer.valueOf(1); System.err.println(integer); } finally { Integer.valueOf(2); }\n"
                        + "} }",
                "integer",
                assertRelatedExpressionsCollected("final RuntimeException e", "Integer.valueOf(1)"));
    }

    @Test
    public void doNotCollectRelatedExpressionFromNonEnclosingTypeDeclarationStatement() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { \n"
                        + "@SuppressWarnings({\"unused\"}) class Bar { void bar() { Integer.valueOf(0); } }\n"
                        + "System.err.println(integer); } }",
                "integer",
                this::assertNoRelatedExpressionsCollected);
    }

    @Test
    public void collectRelatedExpressionFromEnclosingTypeDeclarationStatement() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { \n"
                        + "@SuppressWarnings({\"unused\"}) class Bar { void bar() { Integer.valueOf(0); System.err.println(integer); } }\n"
                        + "} }",
                "integer",
                assertRelatedExpressionsCollected("Integer.valueOf(0)"));
    }

    @Test
    public void doNotCollectRelatedExpressionFromNonEnclosingWhileStatement() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { \n"
                        + "while(0 < Integer.valueOf(1)) { Integer.valueOf(0); }\n"
                        + "System.err.println(integer); } }",
                "integer",
                this::assertNoRelatedExpressionsCollected);
    }

    @Test
    public void collectRelatedExpressionFromEnclosingWhileStatement() throws Exception {
        assertRelatedExpressionsCollection("Foo",
                "class Foo { void foo() { \n"
                        + "while(0 < Integer.valueOf(1)) { Integer.valueOf(0); System.err.println(integer); }\n"
                        + "} }",
                "integer",
                assertRelatedExpressionsCollected("Integer.valueOf(1)", "Integer.valueOf(0)"));
    }

}
