package org.eclipse.jdt.core.dom.anaphors.transform;

import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.jdt.core.dom.anaphors.DomVisiting.findNode;
import static org.eclipse.jdt.core.dom.anaphors.check.CallChainCheckTesting.MAX_DEPTH_OF_CALL_CHAIN_ANALYSIS_IN_TEST;
import static org.eclipse.jdt.core.dom.anaphors.transform.FileBufferTesting.createAndCloseFileBufferFor;

import java.util.List;
import java.util.function.Function;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SimpleType;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.ASTTesting;
import org.eclipse.jdt.core.dom.anaphors.AstParsing;
import org.eclipse.jdt.core.dom.anaphors.DomTraversal;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphoraResolution;
import org.eclipse.jdt.core.dom.anaphors.PublicDom;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpressions;
import org.eclipse.jdt.core.dom.anaphors.check.CallChainCheck;
import org.eclipse.jdt.core.dom.anaphors.check.Invocation;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;

import de.monochromata.anaphors.ast.reference.strategy.concept.TypeRecurrence;
import de.monochromata.anaphors.ast.relatedexp.strategy.ClassInstanceCreationStrategy;
import de.monochromata.anaphors.ast.strategy.DA1ReStrategy;
import de.monochromata.eclipse.anaphors.Activator;
import de.monochromata.eclipse.anaphors.AnaphoraTesting;
import de.monochromata.eclipse.anaphors.editor.update.AstBasedAnaphoraUpdating;
import de.monochromata.eclipse.anaphors.position.AnaphoraPositionUpdater;
import de.monochromata.eclipse.anaphors.preferences.AnaphorsPreferences;
import de.monochromata.eclipse.persp.PerspectivationPosition;

public interface ASTTransformationTesting extends ASTTesting, AnaphoraTesting {

    default void installAnaphoraPositionCategory(final CompilationUnit scope) {
        final IDocument document = AstBasedAnaphoraUpdating.getDocumentForPositionsTracking(scope);
        document.addPositionCategory(ANAPHORA_CATEGORY);
        document.addPositionUpdater(new AnaphoraPositionUpdater(anaphoras -> {
        }));
    }

    default void assertAnaphoraUpdateWorks(final Function<PublicAnaphora, PublicAnaphora> anaphoraUpdater)
            throws CoreException {
        assertProjectAndCompilationUnit("Foo",
                "class Foo { void foo() { Long l = new Long(5l); bar(); } void bar() { System.err.println(Long); } }",
                (oldScope, project) -> {
                    createAndCloseFileBufferFor(oldScope, () -> {
                        // Initialize anaphor resolution to fetching it
                        Activator.getDefault().getAnaphorResolution();

                        final Expression oldRelatedExpression = findLongCICRelatedExpression(oldScope);
                        final Expression oldAnaphorExpression = findLongInPrintlnAnaphor(oldScope);
                        final String oldAnaphor = oldAnaphorExpression.toString();

                        final PublicAnaphora preliminaryAnaphora = createPreliminaryAnaphora(oldScope,
                                oldRelatedExpression, oldAnaphor, oldAnaphorExpression);

                        final PublicAnaphora updatedAnaphora = anaphoraUpdater.apply(preliminaryAnaphora);

                        assertThatRelatedExpressionAndAnaphorHaveBeenReplaced(oldScope, updatedAnaphora, "new Long(5l)",
                                "System.err.println(Long)");
                        final CompilationUnit newScope = DomTraversal
                                .getCompilationUnit(updatedAnaphora.getAnaphorExpression());
                        assertCompilationUnitSource(newScope,
                                "class Foo { void foo() { Long l = new Long(5l); bar(l); } void bar(Long l) { System.err.println(Long); } }");
                    });
                });
    }

    default void assertThatRelatedExpressionAndAnaphorHaveBeenReplaced(final CompilationUnit oldScope,
            final PublicAnaphora updatedAnaphora, final String expectedNewRelatedExpressionString,
            final String expectedNewAnaphorParentString) {
        final ASTNode newRelatedExpression = updatedAnaphora.getRelatedExpression().getRelatedExpression();
        final Expression newAnaphor = updatedAnaphora.getAnaphorExpression();
        assertThat(DomTraversal.getCompilationUnit(newRelatedExpression)).isNotSameAs(oldScope);
        assertThat(newRelatedExpression.toString()).isEqualTo(expectedNewRelatedExpressionString);
        assertThat(DomTraversal.getCompilationUnit(newAnaphor)).isNotSameAs(oldScope);
        assertThat(newAnaphor.getParent().toString()).isEqualTo(expectedNewAnaphorParentString);
    }

    default PublicAnaphora createPreliminaryAnaphora(final CompilationUnit oldScope,
            final Expression oldRelatedExpression, final String oldAnaphor, final Expression oldAnaphorExpression) {
        final PublicDom anaphorsSpi = Activator.getDefault().getAnaphorsSpi();
        final PublicRelatedExpressions relatedExpressionsSpi = Activator.getDefault().getRelatedExpressionsSpi();
        final PublicAnaphoraResolution anaphoraResolutionSpi = Activator.getDefault().getAnaphoraResolutionSpi();
        final AnaphorsPreferences preferencesSpi = Activator.getDefault().getPreferencesSpi();
        final DA1ReStrategy<ASTNode, Expression, Type, IBinding, IVariableBinding, IVariableBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression, PublicAnaphora> resolutionStrategy = new DA1ReStrategy<>(
                anaphorsSpi, relatedExpressionsSpi, anaphoraResolutionSpi);
        final ClassInstanceCreationStrategy<ASTNode, Expression, Type, IBinding, IMethodBinding, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition, PublicRelatedExpression, PublicAnaphora> relatedExpressionStrategy = new ClassInstanceCreationStrategy<>(
                relatedExpressionsSpi, preferencesSpi);
        final List<PublicRelatedExpression> potentialRelatedExpressions = singletonList(
                PublicRelatedExpression.create(oldRelatedExpression));
        final TypeRecurrence<ASTNode, Expression, ITypeBinding, CompilationUnit, String, String, DocumentEvent, PerspectivationPosition> referentializationStrategy = new TypeRecurrence<>(
                anaphorsSpi);

        final List<PublicAnaphora> preliminaryAnaphoraRelations = resolutionStrategy.generatePotentialAnaphora(oldScope,
                oldAnaphor, oldAnaphorExpression, potentialRelatedExpressions,
                singletonList(referentializationStrategy));
        assertThat(preliminaryAnaphoraRelations).hasSize(1);
        final PublicAnaphora preliminaryAnaphora = preliminaryAnaphoraRelations.get(0);
        return preliminaryAnaphora;
    }

    default NodeReplacingASTTransformation createAstTransformation(final CompilationUnit oldScope,
            final Expression oldRelatedExpression, final Expression oldAnaphor) {
        final List<Invocation> callChain = CallChainCheck.check(oldRelatedExpression, oldAnaphor, oldScope,
                MAX_DEPTH_OF_CALL_CHAIN_ANALYSIS_IN_TEST);
        final PassAlongCallChain delegate = new PassAlongCallChain(callChain, oldRelatedExpression, oldAnaphor,
                oldScope, typeRoot -> AstParsing.parse(typeRoot, true));
        final NodeReplacingASTTransformation astTransformation = new NodeReplacingASTTransformation(oldScope, delegate);
        return astTransformation;
    }

    default Expression findLongCICRelatedExpression(final CompilationUnit oldScope) {
        return (Expression) findNode(oldScope,
                node -> node instanceof ClassInstanceCreation && ((SimpleType) ((ClassInstanceCreation) node).getType())
                        .getName().getFullyQualifiedName().equals("Long"));
    }

    default Expression findLongInPrintlnAnaphor(final CompilationUnit oldScope) {
        return (Expression) findNode(oldScope,
                node -> node instanceof SimpleName && ((SimpleName) node).getIdentifier().equals("Long")
                        && node.getParent() instanceof MethodInvocation
                        && ((MethodInvocation) node.getParent()).getName().getIdentifier().equals("println"));
    }

}
