package org.eclipse.jdt.core.dom.anaphors.transform;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.anaphors.DomTraversal;
import org.junit.Test;

public class NodeReplacingASTTransformationTest implements NodeReplacingASTTransformationTesting {

	@Test
	public void nodeReplacementWorks() throws CoreException {
		assertAnaphoraUpdateWorks(preliminaryAnaphora -> {
			final Expression oldRelatedExpression = (Expression) preliminaryAnaphora.getRelatedExpression()
					.getRelatedExpression();
			final Expression oldAnaphor = preliminaryAnaphora.getAnaphorExpression();
			final CompilationUnit oldScope = DomTraversal.getCompilationUnit(oldAnaphor);
			installAnaphoraPositionCategory(oldScope);
			final NodeReplacingASTTransformation astTransformation = createAstTransformation(oldScope,
					oldRelatedExpression, oldAnaphor);
			return astTransformation.perform(preliminaryAnaphora);
		});
	}

	// TODO: Add a test for failure scenario

}
