package org.eclipse.jdt.core.dom.anaphors;

import static java.util.Arrays.stream;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.lang.reflect.Method;
import java.util.Set;

import org.eclipse.jdt.core.ISourceRange;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.SourceRange;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SimpleType;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.StructuralPropertyDescriptor;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;

import de.monochromata.anaphors.ast.feature.Feature;

public interface PublicRelatedExpressionsTesting {

	default PublicRelatedExpression mockRelatedExpression() {
		final ITypeBinding typeBinding = mock(ITypeBinding.class);
		when(typeBinding.getName()).thenReturn("TypeName");
		when(typeBinding.getErasure()).thenReturn(typeBinding);

		return new PublicRelatedExpression(false, () -> null, null, null, null, null, null, null, unused -> typeBinding,
				null);
	}

	default void assertNumberLiteralFeatures(final AST ast, final String numberLiteralString,
			final String... featureStrings) {
		final var numberLiteral = ast.newNumberLiteral(numberLiteralString);

		assertAstNodeYieldsFeatures(numberLiteral, featureStrings);
	}

	default void assertCharacterLiteralFeatures(final AST ast, final char charLiteral, final String... featureStrings) {
		final var literal = ast.newCharacterLiteral();
		literal.setCharValue(charLiteral);

		assertAstNodeYieldsFeatures(literal, featureStrings);
	}

	default void assertStringLiteralYieldsFeatures(final AST ast, final String literalString,
			final String... featureStrings) {
		final StringLiteral literal = ast.newStringLiteral();
		literal.setLiteralValue(literalString);
		assertAstNodeYieldsFeatures(literal, featureStrings);
	}

	default void assertStringLiteralYieldsFeatures(final AST ast, final String literalString,
			final Set<Feature<String>> features, final String... featureStrings) {
		final StringLiteral literal = ast.newStringLiteral();
		literal.setLiteralValue(literalString);
		getFeatures(literal, features);
	}

	@SuppressWarnings("unchecked")
	default void assertAstNodeYieldsFeatures(final ASTNode node, final String... featureStrings) {
		final Set<Feature<String>> features = mock(Set.class);

		getFeatures(node, features);

		stream(featureStrings).forEach(featureString -> verify(features).add(new DomFeature(featureString)));
		verifyNoMoreInteractions(features);
	}

	default void getFeatures(final ASTNode node, final Set<Feature<String>> features) {
		new PublicRelatedExpressions().getFeatures(node, features);
	}

	default VariableDeclarationStatement createVariableDeclarationStatement(final AST ast) throws Exception {

		final SimpleName typeNameInStatement = ast.newSimpleName("Integer");
		typeNameInStatement.setSourceRange(0, 7);

		final SimpleType typeInStatement = ast.newSimpleType(typeNameInStatement);
		typeInStatement.setSourceRange(0, 7);

		final SimpleName typeNameInCic = ast.newSimpleName("Integer");
		typeNameInCic.setSourceRange(22, 7);

		final SimpleType typeInCic = ast.newSimpleType(typeNameInCic);
		typeInCic.setSourceRange(22, 7);

		final ClassInstanceCreation cic = ast.newClassInstanceCreation();
		cic.setType(typeInCic);
		cic.setSourceRange(18, 13);

		final SimpleName name = ast.newSimpleName("integer");
		name.setSourceRange(9, 7);

		final VariableDeclarationFragment fragment = ast.newVariableDeclarationFragment();
		fragment.setName(name);
		fragment.setInitializer(cic);
		fragment.setSourceRange(9, 23);

		final VariableDeclarationStatement relatedExpression = ast.newVariableDeclarationStatement(fragment);
		relatedExpression.setType(typeInStatement);
		relatedExpression.setSourceRange(0, 32);
		setCompilationUnitParentWithSource(ast, relatedExpression, "Integer integer = new Integer();",
				new SourceRange(0, 32));

		return relatedExpression;
	}

	default SingleVariableDeclaration createSingleVariableDeclaration(final AST ast) throws Exception {
		final SimpleName typeName = ast.newSimpleName("TypeName");
		typeName.setSourceRange(10, 8);

		final SimpleType type = ast.newSimpleType(typeName);
		type.setSourceRange(10, 8);

		final SimpleName name = ast.newSimpleName("name");
		name.setSourceRange(19, 4);

		final SingleVariableDeclaration relatedExpression = ast.newSingleVariableDeclaration();
		relatedExpression.setType(type);
		relatedExpression.setName(name);
		relatedExpression.setSourceRange(10, 13);
		setCompilationUnitParentWithSource(ast, relatedExpression, "TypeName name", new SourceRange(10, 13));
		return relatedExpression;
	}

	default void setCompilationUnitParentWithSource(final AST ast, final ASTNode relatedExpression, final String source,
			final SourceRange sourceRange) throws Exception {
		final CompilationUnit compilationUnit = createCompilationUnitWithSource(ast, source, sourceRange);
		final Method setParentMethod = ASTNode.class.getDeclaredMethod("setParent", ASTNode.class,
				StructuralPropertyDescriptor.class);
		setParentMethod.setAccessible(true);
		setParentMethod.invoke(relatedExpression, compilationUnit, null);
	}

	@SuppressWarnings("restriction")
	default CompilationUnit createCompilationUnitWithSource(final AST ast, final String source,
			final SourceRange sourceRange) throws Exception {
		final org.eclipse.jdt.internal.core.CompilationUnit typeRoot = new CompilationUnitWithFixedSource(source,
				sourceRange);
		final CompilationUnit compilationUnit = ast.newCompilationUnit();
		final Method setTypeRootMethod = CompilationUnit.class.getDeclaredMethod("setTypeRoot", ITypeRoot.class);
		setTypeRootMethod.setAccessible(true);
		setTypeRootMethod.invoke(compilationUnit, typeRoot);
		return compilationUnit;
	}

	@SuppressWarnings("restriction")
	class CompilationUnitWithFixedSource extends org.eclipse.jdt.internal.core.CompilationUnit {

		private final String source;
		private final ISourceRange sourceRange;

		public CompilationUnitWithFixedSource(final String source, final ISourceRange sourceRange) {
			super(null, null, null);
			this.source = source;
			this.sourceRange = sourceRange;
		}

		@Override
		public String getSource() throws JavaModelException {
			return source;
		}

		@Override
		public ISourceRange getSourceRange() throws JavaModelException {
			return sourceRange;
		}

	}

}
