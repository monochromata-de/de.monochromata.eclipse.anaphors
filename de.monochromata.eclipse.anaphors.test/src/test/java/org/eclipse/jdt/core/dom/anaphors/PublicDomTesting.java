package org.eclipse.jdt.core.dom.anaphors;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ITypeBinding;

import de.monochromata.anaphors.ast.feature.FeatureContainer;
import de.monochromata.anaphors.ast.reference.Referent;

public interface PublicDomTesting {

	@SuppressWarnings("unchecked")
	default FeatureContainer<String> testGetFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentNameTemplate(
			final String id, final String referentName, final boolean caseSensitive) {
		final Referent<ITypeBinding, CompilationUnit, String, String> referent = mock(Referent.class);
		when(referent.hasName()).thenReturn(true);
		when(referent.getName()).thenReturn(referentName);

		final PublicDom dom = new PublicDom();
		return dom.getFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentName(id, referent, caseSensitive);
	}

}
