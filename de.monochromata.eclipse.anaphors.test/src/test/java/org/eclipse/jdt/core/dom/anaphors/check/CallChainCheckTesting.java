package org.eclipse.jdt.core.dom.anaphors.check;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.function.Consumer;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.NameQualifiedType;
import org.eclipse.jdt.core.dom.QualifiedType;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SimpleType;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.ASTTesting;

public interface CallChainCheckTesting extends ASTTesting {

	int MAX_DEPTH_OF_CALL_CHAIN_ANALYSIS_IN_TEST = 2;

	default void assertCheckReturnsEmptyList(final String typeName, final String source) throws CoreException {
		assertCheckReturnsList(typeName, source, callChain -> assertThatCallChainContains(callChain, emptyList()));
	}

	default void assertCheckReturnsList(final String typeName, final String source,
			final Consumer<List<? extends Invocation>> asserter) throws CoreException {
		assertStringLiteralExpressions(typeName, source, (compilationUnit, expressions) -> {
			final List<? extends Invocation> callChain = CallChainCheck.check(expressions.getLeft(),
					expressions.getRight(), compilationUnit, MAX_DEPTH_OF_CALL_CHAIN_ANALYSIS_IN_TEST);
			asserter.accept(callChain);
		});
	}

	default void assertThatCallChainContains(final List<? extends Invocation> callChain, final String... identifiers) {
		assertThatCallChainContains(callChain, asList(identifiers));
	}

	default void assertThatCallChainContains(final List<? extends Invocation> callChain,
			final List<String> identifiers) {
		final List<String> identifiersFromCallChain = getIdentifiers(callChain);
		assertThat(identifiersFromCallChain).containsExactlyElementsOf(identifiers);
	}

	default List<String> getIdentifiers(final List<? extends Invocation> callChain) {
		return callChain.stream().map(this::getIdentifier).collect(toList());
	}

	default String getIdentifier(final Invocation invocation) {
		if (invocation.isMethodInvocation()) {
			return invocation.getMethodInvocation().getName().getIdentifier();
		} else if (invocation.isConstructorInvocation()) {
			return "this";
		} else if (invocation.isClassInstanceCreation()) {
			return getIdentifier(invocation.getClassInstanceCreation().getType());
		} else {
			throw new IllegalArgumentException("Unknown kind of invocation: " + invocation);
		}
	}

	default String getIdentifier(final Type type) {
		if (type instanceof SimpleType) {
			return getIdentifier(((SimpleType) type).getName());
		} else if (type instanceof QualifiedType) {
			return getIdentifier(((QualifiedType) type).getQualifier(), ((QualifiedType) type).getName());
		} else if (type instanceof NameQualifiedType) {
			return getIdentifier(((NameQualifiedType) type).getQualifier(), ((NameQualifiedType) type).getName());
		} else {
			throw new IllegalArgumentException("Unexpected subtype of Type: " + type.getClass().getName());
		}
	}

	default String getIdentifier(final Name name) {
		return name.getFullyQualifiedName();
	}

	default String getIdentifier(final Name qualifier, final SimpleName name) {
		return getIdentifier(qualifier) + "." + getIdentifier(name);
	}

	default String getIdentifier(final Type qualifier, final SimpleName name) {
		return getIdentifier(qualifier) + "." + getIdentifier(name);
	}

}
