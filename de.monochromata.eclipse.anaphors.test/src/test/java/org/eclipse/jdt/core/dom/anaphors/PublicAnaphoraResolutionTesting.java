package org.eclipse.jdt.core.dom.anaphors;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;

public interface PublicAnaphoraResolutionTesting {

    default QualifiedName createQualifiedName(final AST ast) {
        final SimpleName qualifier = ast.newSimpleName("qualifier");
        final SimpleName name = ast.newSimpleName("fieldName");
        final QualifiedName qualifiedName = ast.newQualifiedName(qualifier, name);
        return qualifiedName;
    }

    default FieldAccess createFieldAccess(final AST ast) {
        final FieldAccess fieldAccess = ast.newFieldAccess();
        fieldAccess.setExpression(ast.newSimpleName("expression"));
        fieldAccess.setName(ast.newSimpleName("fieldName"));
        return fieldAccess;
    }

}
