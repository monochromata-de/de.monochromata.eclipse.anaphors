package org.eclipse.jdt.core.dom.anaphors.check;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;

public class CallChainCheckTest implements CallChainCheckTesting {

	// TODO: Add a test that fails it the given nodes are not part of
	// invocables, i.e. not parameter declarations

	@Test
	public void checkReturnsEmptyListIfThereIsNoCallChainBetweenTheInvocablesContainingTheExpressions()
			throws CoreException {
		assertCheckReturnsEmptyList("Foo",
				"class Foo { void foo() { String a = \"expression1\"; } void bar() { String b = \"expression2\"; } }");
	}

	@Test
	public void checkReturnsNonEmptyListIfTheFirstInvocableInvokesTheSecondInvocableAfterTheFirstExpression()
			throws CoreException {
		assertCheckReturnsList("Foo",
				"class Foo { void foo() { String a = \"expression1\"; bar(); } void bar() { String b = \"expression2\"; } }",
				callChain -> assertThatCallChainContains(callChain, "bar"));
	}

	@Test
	public void checkReturnsEmptyListIfTheFirstInvocableInvokesTheSecondInvocableBeforeTheFirstExpression()
			throws CoreException {
		assertCheckReturnsEmptyList("Foo",
				"class Foo { void foo() { bar(); String a = \"expression1\"; } void bar() { String b = \"expression2\"; } }");
	}

	@Test
	public void checkReturnsNonEmptyListIfTheMaximumDepthOfTheCallChainIsReached() throws CoreException {
		assertCheckReturnsList("Foo",
				"class Foo { void foo() { String a = \"expression1\"; bar(); }\n"
						+ "void bar() { String b = \"expression0\"; jizz(); }\n"
						+ "void jizz() { String c = \"expression2\"; } }",
				callChain -> assertThatCallChainContains(callChain, "bar", "jizz"));
	}

	@Test
	public void checkReturnsEmptyListIfTheMaximumDepthOfTheCallChainIsExceededWithoutAMatch() throws CoreException {
		assertCheckReturnsEmptyList("Foo",
				"class Foo { void foo() { String a = \"expression1\"; bar(); }\n"
						+ "void bar() { String b = \"expression-1\"; jizz(); }\n"
						+ "void jizz() { String c = \"expression0\"; jazz(); }\n"
						+ "void jazz() { String d = \"expression2\"; } }");
	}

	// TODO: Recursion should not be detected by the name merely as methods
	// might be overloaded
	// TODO: There should be more examples of recursion, including indirect
	// recursion

	@Test
	public void checkReturnsNonEmptyListIfThereIsRecursionInTheCallChain() throws CoreException {
		assertCheckReturnsList("Foo",
				"class Foo { void foo() { String a = \"expression1\"; bar(); }\n"
						+ "void bar() { String b = \"expression2\"; bar(); } }",
				callChain -> assertThatCallChainContains(callChain, "bar", "bar"));
		// TODO: Use the correct assertion:
		// callChain -> assertThatCallChainContains(callChain, "bar")
	}

	@Test
	public void checkReturnsNonEmptyListIfTheUpstreamInvocableCallsTheDownstreamInvocableAtMultiplePositions()
			throws CoreException {
		assertCheckReturnsList("Foo",
				"class Foo { void foo() { String a = \"expression1\"; bar(); bar(); }\n"
						+ "void bar() { String b = \"expression2\"; } }",
				callChain -> assertThatCallChainContains(callChain, "bar"));
	}

	@Test
	public void checkReturnsNonEmptyListForTheFirstShortestChainIfThereAreMultipleChains() throws CoreException {
		assertCheckReturnsList("Foo",
				"class Foo { void foo() { String a = \"expression1\"; bar(); jizz(); }\n"
						+ "void bar() { String b = \"expression0\"; jizz(); }"
						+ "void jizz() { String c = \"expression2\"; } }",
				callChain -> assertThatCallChainContains(callChain, "jizz"));
	}

	@Test
	public void checkReturnsNonEmptyListForCallChainContainingAConstructorInvocation() throws CoreException {
		assertCheckReturnsList("Foo",
				"class Foo { Foo() { this(\"expression1\"); } Foo(String i) { String a = i+\"expression2\"; } }",
				callChain -> assertThatCallChainContains(callChain, "this"));
	}

	@Test
	public void checkReturnsNonEmptyListForCallChainContainingAClassInstanceCreation() throws CoreException {
		assertCheckReturnsList("Foo",
				"class Foo { { String a = \"expression1\"; new Foo(); } Foo() { String b = \"expression2\"; } }",
				callChain -> assertThatCallChainContains(callChain, "Foo"));
	}

	@Test
	public void checkCurrentlyIgnoresBodyDeclarationsInOtherCompilationUnits() throws CoreException {
		// TODO: currently Long(5l) cannot be analyzed further because it is
		// declared in another CompilationUnit/ClassFile
		// TODO: the invocation of Long(5l) should not yield exceptions. To
		// assert it, add a new test for CallChainsCollection
		assertCheckReturnsList("Foo",
				"class Foo { void foo() { String a = \"expression1\"; new Long(5l); bar(); } void bar() { String b = \"expression2\"; } }",
				callChain -> assertThatCallChainContains(callChain, "bar"));
	}

	// TODO: Add tests for call chain analysis for complex expressions, i.e. a
	// method argument being created by a method invocation

}
