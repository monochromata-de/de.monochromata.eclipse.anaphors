package org.eclipse.jdt.core.dom.anaphors.transform;

import static org.eclipse.jdt.core.dom.anaphors.check.CallChainCheckTesting.MAX_DEPTH_OF_CALL_CHAIN_ANALYSIS_IN_TEST;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.anaphors.AstParsing;
import org.eclipse.jdt.core.dom.anaphors.check.CallChainCheck;
import org.eclipse.jdt.core.dom.anaphors.check.Invocation;

public interface PassAlongCallChainTesting extends TransformationTesting {

	default void assertPassAlongCallChainSucceeds(final String typeName, final String inputSource,
			final String transformedSource) throws CoreException {
		assertTransformationSucceeds(typeName, inputSource, transformedSource, this::createTransformation);
	}

	default PassAlongCallChain createTransformation(final CompilationUnit scope,
			final Pair<Expression, Expression> expressions) {
		final Expression upstreamExpression = expressions.getLeft();
		final Expression downstreamExpression = expressions.getRight();
		final List<Invocation> callChain = CallChainCheck.check(upstreamExpression, downstreamExpression, scope,
				MAX_DEPTH_OF_CALL_CHAIN_ANALYSIS_IN_TEST);
		return new PassAlongCallChain(callChain, upstreamExpression, downstreamExpression, scope,
				typeRoot -> AstParsing.parse(typeRoot, true));
	}

}
