package org.eclipse.jdt.core.dom.anaphors.check;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;

public class SameInvocableCheckTest implements SameInvocableCheckTesting {

	// TODO: Use Cucumber instead of plain JUnit?
	// TODO: Add a test that fails if the given nodes are not part of
	// invocables, i.e. not parameter declarations

	@Test
	public void partOfSameInvocableReturnsTrueWithinStaticInitializer() throws CoreException {
		assertStringLiteralsExpression1AndExpression2AreInSameInvocable("Foo",
				"class Foo { static { final String a = \"expression1\"; final String b = \"expression2\"; } }");
	}

	@Test
	public void partOfSameInvocableReturnsTrueWithinInstanceInitializer() throws CoreException {
		assertStringLiteralsExpression1AndExpression2AreInSameInvocable("Foo",
				"class Foo { { final String a = \"expression1\"; final String b = \"expression2\"; } }");
	}

	@Test
	public void partOfSameInvocableReturnsTrueWithinConstructor() throws CoreException {
		assertStringLiteralsExpression1AndExpression2AreInSameInvocable("Foo",
				"class Foo { Foo() { final String a = \"expression1\"; final String b = \"expression2\"; } }");
	}

	@Test
	public void partOfSameInvocableReturnsTrueWithinMethodBody() throws CoreException {
		assertStringLiteralsExpression1AndExpression2AreInSameInvocable("Foo",
				"class Foo { void foo() { final String a = \"expression1\"; final String b = \"expression2\"; } }");
	}

	@Test
	public void partOfSameInvocableReturnsTrueWithinStaticMethodBody() throws CoreException {
		assertStringLiteralsExpression1AndExpression2AreInSameInvocable("Foo",
				"class Foo { static void foo() { final String a = \"expression1\"; final String b = \"expression2\"; } }");
	}

	@Test
	public void partOfSameInvocableReturnsTrueWithinDefaultMethodBody() throws CoreException {
		assertStringLiteralsExpression1AndExpression2AreInSameInvocable("Foo",
				"interface Foo { default void foo() { final String a = \"expression1\"; final String b = \"expression2\"; } }");
	}

	/**
	 * expression1 is in a block, the other expression2 follows after the block.
	 * <p>
	 * TODO: Actually, expression1 is not reachable from expression2 and would
	 * need to be moved out of the block - if there are no side-effects.
	 */
	@Test
	public void partOfSameInvocableReturnsTrueWithBlockInMethodBody() throws CoreException {
		assertStringLiteralsExpression1AndExpression2AreInSameInvocable("Foo",
				"class Foo { void foo() { { final String a = \"expression1\"; } final String b = \"expression2\"; } }");
	}

	// TODO: Handle the special case that expression1 is not effectively final
	// and appears in a method body above a lambda expression in the same method
	// body. The lambda expression contains expression 2. How to proceed in that
	// case? Would it be possible to add a parameter in that case? Can that
	// actually happen? Or will such a reference be ambiguous and would it
	// always be possible to resolve the ambiguity by adding a feature to the
	// anaphor that disambiguates the anaphora relation?

	@Test
	public void partOfSameInvocableReturnsTrueForExpression1InMethodBodyAndExpression2InLambdaExpressionLaterInThatBody()
			throws CoreException {
		assertStringLiteralsExpression1AndExpression2AreInSameInvocable("Foo",
				"class Foo { void foo() { final String a = \"expression1\"; final Supplier<String> s = () -> \"expression2\"; } }");
	}

	/**
	 * TODO: How would that case be resolved correctly, not by introducing a
	 * parameter at least, maybe by re-ordering the code. But only if there are
	 * no side-effects. I.e. there would need to be an analysis of side-effects.
	 */
	@Test
	public void partOfSameInvocableReturnsFalseForExpression1InMethodBodyAndExpression2InLambdaExpressionEarlierInThatBody()
			throws CoreException {
		assertStringLiteralsExpression1AndExpression2AreInSameInvocable("Foo",
				"class Foo { void foo() { final Supplier<String> s = () -> \"expression2\"; final String a = \"expression1\"; } }");
	}

	@Test
	public void partOfSameInvocableReturnsFalseForDifferentInvocables() throws CoreException {
		assertStringLiteralsExpression1AndExpression2AreNotInSameInvocable("Foo",
				"class Foo { void foo() { final String a = \"expression1\"; } void bar() { final String b = \"expression2\"; } }");
	}

}
