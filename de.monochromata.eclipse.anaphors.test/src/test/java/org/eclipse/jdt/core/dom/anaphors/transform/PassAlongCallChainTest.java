package org.eclipse.jdt.core.dom.anaphors.transform;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;

public class PassAlongCallChainTest implements PassAlongCallChainTesting {

	@Test
	public void successfullyAppliedTransformationForPassAlongCallChain() throws CoreException {
		// TODO: Have the transformation add a final modifier to the new
		// parameter?
		assertPassAlongCallChainSucceeds("Foo",
				"class Foo { void foo() { String a = \"expression1\"; bar(); } void bar() { System.err.println(\"expression2\"); } }",
				"class Foo { void foo() { String a = \"expression1\"; bar(a); } void bar(String a) { System.err.println(\"expression2\"); } }");
	}

}
