package org.eclipse.jdt.core.dom.anaphors;

import static java.util.Arrays.stream;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Predicate;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IInitializer;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IParent;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.CompilationUnit;

public interface JavaModelTesting {

    default IMethod findMethod(final String methodName, final CompilationUnit compilationUnit) {
        return findNodeOrThrowException(compilationUnit, IMethod.class,
                node -> node.getElementName().equals(methodName));
    }

    default IInitializer findInitializer(final CompilationUnit compilationUnit) {
        return findNodeOrThrowException(compilationUnit, IInitializer.class, unused -> true);
    }

    default IField findField(final String fieldName, final CompilationUnit compilationUnit) {
        return findNodeOrThrowException(compilationUnit, IField.class,
                field -> field.getElementName().equals(fieldName));
    }

    default <T extends IJavaElement> T findNodeOrThrowException(final CompilationUnit compilationUnit,
            final Class<T> type,
            final Predicate<T> predicate) {
        return findNode(compilationUnit, type, predicate).orElseThrow(() -> new NoSuchElementException());
    }

    default <T extends IJavaElement> Optional<T> findNode(final CompilationUnit compilationUnit, final Class<T> type,
            final Predicate<T> predicate) {
        return findNode((ICompilationUnit) compilationUnit.getJavaElement(), type, predicate);
    }

    default <T extends IJavaElement> Optional<T> findNode(final IJavaElement[] elements, final Class<T> type,
            final Predicate<T> predicate) {
        return stream(elements)
                .map(element -> findNode(element, type, predicate))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .findFirst();
    }

    @SuppressWarnings("unchecked")
    default <T extends IJavaElement> Optional<T> findNode(final IJavaElement element, final Class<T> type,
            final Predicate<T> predicate) {
        if (type.isInstance(element) && predicate.test((T) element)) {
            return Optional.of((T) element);
        }
        if (element instanceof IParent) {
            try {
                return findNode(((IParent) element).getChildren(), type, predicate);
            } catch (final JavaModelException e) {
                throw new IllegalStateException(e);
            }
        }
        return Optional.empty();
    }

}
