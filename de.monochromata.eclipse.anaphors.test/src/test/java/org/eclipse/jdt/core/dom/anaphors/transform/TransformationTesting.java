package org.eclipse.jdt.core.dom.anaphors.transform;

import java.util.function.BiFunction;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.anaphors.ASTTesting;

public interface TransformationTesting extends ASTTesting {

	default void assertTransformationSucceeds(final String typeName, final String inputSource,
			final String transformedSource,
			final BiFunction<CompilationUnit, Pair<Expression, Expression>, Transformation> transformationFactory)
			throws CoreException {
		assertProjectAndStringLiteralExpressions(typeName, inputSource, (scope, projectAndExpressions) -> {
			final IJavaProject project = projectAndExpressions.getLeft();
			final StringLiteral upstreamExpression = projectAndExpressions.getMiddle();
			final StringLiteral downstreamExpression = projectAndExpressions.getRight();
			final Pair<Expression, Expression> expressions = new ImmutablePair<>(upstreamExpression,
					downstreamExpression);

			final Transformation transformation = transformationFactory.apply(scope, expressions);
			final CompilationUnit newScope = transformation.perform();

			assertCompilationUnitSource(newScope, transformedSource);
			assertJavaFileContents(project, typeName, transformedSource);
		});
	}

}
