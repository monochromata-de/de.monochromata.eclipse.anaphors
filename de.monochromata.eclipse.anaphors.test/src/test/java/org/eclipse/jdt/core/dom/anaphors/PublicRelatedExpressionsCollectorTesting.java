package org.eclipse.jdt.core.dom.anaphors;

import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.jdt.core.compiler.IProblem.UnresolvedVariable;
import static org.eclipse.jdt.core.dom.anaphors.DomVisiting.findSimpleName;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.compiler.IProblem;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.SimpleName;

import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.eclipse.anaphors.Activator;

public interface PublicRelatedExpressionsCollectorTesting extends ASTTesting {

	default void assertNoRelatedExpressionsCollected(final List<PublicRelatedExpression> relatedExpressions) {
		assertThat(relatedExpressions).isEmpty();
	}

	default Consumer<List<PublicRelatedExpression>> assertRelatedExpressionsCollected(
			final String... expectedRelatedExpressionStrings) {
		return relatedExpressions -> {
			assertThat(relatedExpressions).hasSize(expectedRelatedExpressionStrings.length);
			final List<String> relatedExpressionStrings = relatedExpressions.stream()
					.map(RelatedExpression::getRelatedExpression).map(Object::toString).collect(toList());
			assertThat(relatedExpressionStrings).isEqualTo(asList(expectedRelatedExpressionStrings));
		};
	}

	default void assertRelatedExpressionCollected(final List<PublicRelatedExpression> relatedExpressions,
			final String relatedExpressionString) {
		assertThat(relatedExpressions).hasSize(1);
		assertThat(relatedExpressions.get(0).getRelatedExpression().toString()).isEqualTo(relatedExpressionString);
	}

	default void assertRelatedExpressionsCollection(final String typeName, final String source,
			final String definiteExpressionIdentifier,
			final Consumer<List<PublicRelatedExpression>> relatedExpressionsAsserter) throws CoreException {
		assertProjectAndCompilationUnit(typeName, source, (scope, project) -> {
			hasNoProblemsBesidesTheAnaphor(scope);
			final PublicRelatedExpressionsCollector collector = createCollector();
			final SimpleName definiteExpression = findSimpleName(scope, definiteExpressionIdentifier);
			final List<PublicRelatedExpression> potentialRelatedExpressions = collector.traverse(definiteExpression,
					scope);
			relatedExpressionsAsserter.accept(potentialRelatedExpressions);
		});
	}

	default void hasNoProblemsBesidesTheAnaphor(final CompilationUnit scope) {
		final Stream<IProblem> problemsBesidesAnaphor = stream(scope.getProblems())
				.filter(problem -> problem.getID() != UnresolvedVariable);
		assertThat(problemsBesidesAnaphor).isEmpty();
	}

	default PublicRelatedExpressionsCollector createCollector() {
		return Activator.getDefault().createRelatedExpressionsCollector();
	}
}
