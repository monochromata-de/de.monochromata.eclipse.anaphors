package org.eclipse.jdt.core.dom.anaphors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.eclipse.jdt.core.dom.AST.JLS10;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.junit.Test;

public class PublicAnaphoraResolutionTest implements PublicAnaphoraResolutionTesting {

    private final AST ast = AST.newAST(JLS10);
    private final PublicAnaphoraResolution publicAnaphoraResolution = new PublicAnaphoraResolution();

    @Test
    public void couldBeAPreviousRealizationReturnsTrueForFieldAccess() {
        assertThat(publicAnaphoraResolution.couldBeAPreviousRealization(ast.newFieldAccess())).isTrue();
    }

    @Test
    public void couldBeAPreviousRealizationReturnsTrueForQualifiedName() {
        final QualifiedName qualifiedName = createQualifiedName(ast);

        assertThat(publicAnaphoraResolution.couldBeAPreviousRealization(qualifiedName)).isTrue();
    }

    @Test
    public void couldBeAPreviousRealizationReturnsFalseForOtherNode() {
        assertThat(publicAnaphoraResolution.couldBeAPreviousRealization(ast.newAssignment())).isFalse();
    }

    @Test
    public void getIdFromPreviousRealizationWorksForFieldAccess() {
        final FieldAccess fieldAccess = createFieldAccess(ast);

        assertThat(publicAnaphoraResolution.getIdFromPreviousRealization(fieldAccess)).isEqualTo("fieldName");
    }

    @Test
    public void getIdFromPreviousRealizationWorksForQualifiedName() {
        final QualifiedName qualifiedName = createQualifiedName(ast);

        assertThat(publicAnaphoraResolution.getIdFromPreviousRealization(qualifiedName)).isEqualTo("fieldName");
    }

    @Test
    public void getIdFromPreviousRealizationYieldsExceptionOnOtherNode() {
        assertThatThrownBy(() -> publicAnaphoraResolution.getIdFromPreviousRealization(ast.newAssignment()))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Unknown type of AST node: " + Assignment.class.getName());
    }

}
