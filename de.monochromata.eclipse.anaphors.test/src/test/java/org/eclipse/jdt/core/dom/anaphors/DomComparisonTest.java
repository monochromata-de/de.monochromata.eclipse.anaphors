package org.eclipse.jdt.core.dom.anaphors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.jdt.core.dom.AST.JLS8;
import static org.eclipse.jdt.core.dom.anaphors.DomComparison.compare;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.CastExpression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.junit.Test;

public class DomComparisonTest {

	private final AST ast = AST.newAST(JLS8);

	@Test
	public void compareTwoExpressionsThatAreEqualSimpleNames() {
		// Happens e.g. the two MethodInvocations are compared that have SimpleNames as
		// expressions.
		final boolean result = compare(ast.newSimpleName("foo"), ast.newSimpleName("foo"));

		assertThat(result).isTrue();
	}

	@Test
	public void compareTwoExpressionsThatAreDifferentSimpleNames() {
		// Happens e.g. the two MethodInvocations are compared that have SimpleNames as
		// expressions.
		final boolean result = compare(ast.newSimpleName("foo"), ast.newSimpleName("bar"));

		assertThat(result).isFalse();
	}

	@Test
	public void compareTwoExpressionsThatAreNullLiterals() {
		final boolean result = compare(ast.newNullLiteral(), ast.newNullLiteral());
		assertThat(result).isTrue();
	}

	// Comparison based on node type

	@Test
	public void stringLiteralDoesNotEqualNumberLiteral() {
		final boolean result = compare(ast.newStringLiteral(), ast.newNumberLiteral());

		assertThat(result).isFalse();
	}

	// Comparison based on SimplePropertyDescriptor

	@Test
	public void numberLiteralsWithoutValueAreEqual() {
		final boolean result = compare(ast.newNumberLiteral(), ast.newNumberLiteral());

		assertThat(result).isTrue();
	}

	@Test
	public void numberLiteralWithoutValueDoesNotEqualNumberLiteralWithValue() {
		final boolean result = compare(ast.newNumberLiteral(), ast.newNumberLiteral("1l"));

		assertThat(result).isFalse();
	}

	@Test
	public void numberLiteralsWithDifferentValuesAreNotEqual() {
		final boolean result = compare(ast.newNumberLiteral("0l"), ast.newNumberLiteral("1l"));

		assertThat(result).isFalse();
	}

	@Test
	public void numberLiteralsWithEqualValuesAreEqual() {
		final boolean result = compare(ast.newNumberLiteral("0l"), ast.newNumberLiteral("0l"));

		assertThat(result).isTrue();
	}

	// Comparison based on ChildPropertyDescriptor

	@Test
	public void childPropertiesWithoutValueMatch() {
		final boolean result = compare(ast.newCastExpression(), ast.newCastExpression());

		assertThat(result).isTrue();
	}

	@Test
	public void childPropertyWithoutValueDoesNotMatchChildPropertyWithValue() {
		final CastExpression cast1 = ast.newCastExpression();
		final CastExpression cast2 = ast.newCastExpression();
		cast2.setExpression(ast.newNumberLiteral("0l"));
		final boolean result = compare(cast1, cast2);

		assertThat(result).isFalse();
	}

	@Test
	public void childPropertiesWithDifferentValueDoNotMatch() {
		final CastExpression cast1 = ast.newCastExpression();
		cast1.setExpression(ast.newNumberLiteral("0l"));
		final CastExpression cast2 = ast.newCastExpression();
		cast2.setExpression(ast.newNumberLiteral("1l"));
		final boolean result = compare(cast1, cast2);

		assertThat(result).isFalse();
	}

	@Test
	public void childPropertiesWithEqualValueMatch() {
		final CastExpression cast1 = ast.newCastExpression();
		cast1.setExpression(ast.newNumberLiteral("0l"));
		final CastExpression cast2 = ast.newCastExpression();
		cast2.setExpression(ast.newNumberLiteral("0l"));
		final boolean result = compare(cast1, cast2);

		assertThat(result).isTrue();
	}

	// Comparison based on ChildListPropertyDescriptor

	@Test
	public void childListPropertiesWithoutValueMatch() {
		final boolean result = compare(ast.newMethodInvocation(), ast.newMethodInvocation());

		assertThat(result).isTrue();
	}

	@Test
	public void childListPropertyWithoutValueDoesNotMatchChildListPropertyWithValue() {
		final MethodInvocation invocation1 = ast.newMethodInvocation();
		final MethodInvocation invocation2 = ast.newMethodInvocation();
		invocation2.arguments().add(ast.newNumberLiteral("0"));

		final boolean result = compare(invocation1, invocation2);

		assertThat(result).isFalse();
	}

	@Test
	public void childListPropertiesWithDifferentValuesDoNotMatch() {
		final MethodInvocation invocation1 = ast.newMethodInvocation();
		invocation1.arguments().add(ast.newNumberLiteral("0"));
		final MethodInvocation invocation2 = ast.newMethodInvocation();
		invocation2.arguments().add(ast.newNumberLiteral("1"));

		final boolean result = compare(invocation1, invocation2);

		assertThat(result).isFalse();
	}

	@Test
	public void childListPropertiesWithDifferentTypesOfValuesDoNotMatch() {
		final MethodInvocation invocation1 = ast.newMethodInvocation();
		invocation1.arguments().add(ast.newStringLiteral());
		final MethodInvocation invocation2 = ast.newMethodInvocation();
		invocation2.arguments().add(ast.newNumberLiteral("1"));

		final boolean result = compare(invocation1, invocation2);

		assertThat(result).isFalse();
	}

	@Test
	public void childListPropertiesWithEqualValuesMatch() {
		final MethodInvocation invocation1 = ast.newMethodInvocation();
		invocation1.arguments().add(ast.newNumberLiteral("0"));
		final MethodInvocation invocation2 = ast.newMethodInvocation();
		invocation2.arguments().add(ast.newNumberLiteral("0"));

		final boolean result = compare(invocation1, invocation2);

		assertThat(result).isTrue();
	}
}
