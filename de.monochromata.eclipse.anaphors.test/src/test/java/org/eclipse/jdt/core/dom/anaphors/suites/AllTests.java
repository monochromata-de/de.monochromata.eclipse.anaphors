package org.eclipse.jdt.core.dom.anaphors.suites;

import org.eclipse.jdt.core.dom.anaphors.DomComparisonTest;
import org.eclipse.jdt.core.dom.anaphors.DomVisitingTest;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphoraResolutionTest;
import org.eclipse.jdt.core.dom.anaphors.PublicDomTest;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpressionsCollectorTest;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpressionsTest;
import org.eclipse.jdt.core.dom.anaphors.PublicTransformationsTest;
import org.eclipse.jdt.core.dom.anaphors.check.CallChainCheckTest;
import org.eclipse.jdt.core.dom.anaphors.check.SameInvocableCheckTest;
import org.eclipse.jdt.core.dom.anaphors.transform.NodeReplacingASTTransformationTest;
import org.eclipse.jdt.core.dom.anaphors.transform.PassAlongCallChainTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * The suite containing all tests for the JDT DOM implementation of anaphors.
 *
 * <p>
 * Must be run as a JUnit Plug-in Test. TODO: Really?
 * </p>
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ CallChainCheckTest.class, DomComparisonTest.class, DomVisitingTest.class,
		PublicAnaphoraResolutionTest.class, PublicDomTest.class, PublicRelatedExpressionsCollectorTest.class,
		PublicRelatedExpressionsTest.class, PublicTransformationsTest.class, SameInvocableCheckTest.class,
		PassAlongCallChainTest.class, NodeReplacingASTTransformationTest.class })
public class AllTests {

}
