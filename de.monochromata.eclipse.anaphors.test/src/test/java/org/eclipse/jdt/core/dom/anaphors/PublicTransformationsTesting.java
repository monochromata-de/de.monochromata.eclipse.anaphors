package org.eclipse.jdt.core.dom.anaphors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.jdt.core.dom.anaphors.check.CallChainCheckTesting.MAX_DEPTH_OF_CALL_CHAIN_ANALYSIS_IN_TEST;

import java.util.function.Consumer;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.dom.anaphors.transform.ASTTransformationTesting;

import de.monochromata.anaphors.ast.transform.ASTTransformation;

public interface PublicTransformationsTesting extends ASTTransformationTesting {

	default void assertPassAlongCallChainReturnsNull(final String typeName, final String source) throws CoreException {
		assertPassAlongCallChainResult(typeName, source, transformation -> assertThat(transformation).isNull());
	}

	@SuppressWarnings("rawtypes")
	default void assertPassAlongCallChainResult(final String typeName, final String source,
			final Consumer<ASTTransformation> asserter) throws CoreException {
		assertStringLiteralExpressions(typeName, source, (compilationUnit, expressions) -> {
			final ASTTransformation transformation = new PublicTransformations(MAX_DEPTH_OF_CALL_CHAIN_ANALYSIS_IN_TEST,
					typeRoot -> AstParsing.parse(typeRoot, true)).passAlongCallChain(expressions.getLeft(),
							expressions.getRight(), compilationUnit);
			asserter.accept(transformation);
		});
	}

}
