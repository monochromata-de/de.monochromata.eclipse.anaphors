package org.eclipse.jdt.core.dom.anaphors;

import static java.util.stream.Collectors.joining;
import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.jdt.core.dom.anaphors.DomVisiting.findStringLiteral;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SimpleType;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.launching.IVMInstall;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jdt.launching.LibraryLocation;

public interface ASTTesting {

	default void assertSourceRange(final ASTNode astNode, final int startPosition, final int length) {
		assertThat(astNode.getStartPosition()).isEqualTo(startPosition);
		assertThat(astNode.getLength()).isEqualTo(length);
	}

	default void assertStringLiteralExpressions(final String typeName, final String source,
			final BiConsumer<CompilationUnit, Pair<StringLiteral, StringLiteral>> asserter) throws CoreException {
		assertProjectAndStringLiteralExpressions(typeName, source, (scope, projectAndStringLiterals) -> asserter.accept(
				scope, new ImmutablePair<>(projectAndStringLiterals.getMiddle(), projectAndStringLiterals.getRight())));
	}

	default void assertProjectAndStringLiteralExpressions(final String typeName, final String source,
			final BiConsumer<CompilationUnit, Triple<IJavaProject, StringLiteral, StringLiteral>> asserter)
			throws CoreException {
		assertProjectAndCompilationUnit(typeName, source, (scope, project) -> {
			final StringLiteral expression1 = findStringLiteral(scope, "expression1");
			final StringLiteral expression2 = findStringLiteral(scope, "expression2");
			assertThat(expression1).isNotNull();
			assertThat(expression2).isNotNull();
			asserter.accept(scope, new ImmutableTriple<>(project, expression1, expression2));
		});
	}

	default Function<Void, Function<Pair<IJavaProject, IPackageFragment>, CompilationUnit>> getCompilationUnit(
			final String typeName, final String source) {
		return unused -> (projectAndDefaultPackage) -> parse(projectAndDefaultPackage.getLeft(),
				projectAndDefaultPackage.getRight(), typeName, source);
	}

	default Function<Function<Pair<IJavaProject, IPackageFragment>, CompilationUnit>, Void> assertProjectAndCompilationUnit(
			final BiConsumer<CompilationUnit, IJavaProject> asserter) {
		return compilationUnitFactory -> {
			try {
				withJavaProject((project, defaultPackage) -> {
					final var compilationUnit = compilationUnitFactory
							.apply(new ImmutablePair<>(project, defaultPackage));
					asserter.accept(compilationUnit, project);
				});
			} catch (final CoreException e) {
				throw new RuntimeException(e);
			}
			return null;
		};
	}

	/**
	 * @deprecated Use a composition of {@link #getCompilationUnit(String, String)}
	 *             and {@link #assertProjectAndCompilationUnit(BiConsumer)} instead.
	 */
	@Deprecated
	default void assertProjectAndCompilationUnit(final String typeName, final String source,
			final BiConsumer<CompilationUnit, IJavaProject> asserter) throws CoreException {
		withJavaProject((project, defaultPackage) -> {
			final CompilationUnit compilationUnit = parse(project, defaultPackage, typeName, source);
			asserter.accept(compilationUnit, project);
		});
	}

	default CompilationUnit parse(final IJavaProject javaProject, final IPackageFragment defaultPackage,
			final String typeName, final String source) {
		try {
			final var type = getTypeModel(javaProject, defaultPackage, typeName, source);
			return AstParsing.parse(type.getTypeRoot(), true);
		} catch (final JavaModelException e) {
			throw new RuntimeException(e);
		}
	}

	default IType getTypeModel(final IJavaProject javaProject, final IPackageFragment defaultPackage,
			final String typeName, final String source) throws JavaModelException {
		createCompilationUnit(defaultPackage, typeName, source);
		return javaProject.findType(typeName);
	}

	default void createCompilationUnit(final IPackageFragment defaultPackage, final String typeName,
			final String source) throws JavaModelException {
		defaultPackage.createCompilationUnit(createFileName(typeName), source, true, null);
	}

	default void createJavaSourceFile(final IJavaProject javaProject, final String fileName, final String source) {
		try {
			final IFile sourceFile = javaProject.getProject().getFile(fileName);
			sourceFile.create(new ByteArrayInputStream(source.getBytes("UTF-8")), true, null);
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	default void assertCompilationUnitSource(final CompilationUnit scope, final String expectedString) {
		try {
			assertThat(scope.getTypeRoot().getSource()).isEqualToIgnoringWhitespace(expectedString);
		} catch (final JavaModelException jme) {
			throw new IllegalStateException(jme);
		}
	}

	default void assertJavaFileContents(final IJavaProject javaProject, final String typeName,
			final String expectedContents) {
		final String contents = getJavaFileContents(javaProject, typeName);
		assertThat(contents).isEqualTo(expectedContents);
	}

	default String getJavaFileContents(final IJavaProject javaProject, final String typeName) {
		final String fileName = "src/" + createFileName(typeName);
		final IFile file = javaProject.getProject().getFile(fileName);
		return getContents(file);
	}

	default String getContents(final IFile file) {
		try (final BufferedReader reader = new BufferedReader(new InputStreamReader(file.getContents(), "UTF-8"))) {
			return reader.lines().collect(joining("\n"));
		} catch (final IOException ioe) {
			throw new UncheckedIOException(ioe);
		} catch (final CoreException ce) {
			throw new IllegalStateException(ce);
		}
	}

	default String createFileName(final String typeName) {
		return typeName + ".java";
	}

	default Pair<IJavaProject, IPackageFragment> createJavaProject(final IProject project) {
		try {
			// See
			// https://sdqweb.ipd.kit.edu/wiki/JDT_Tutorial:_Creating_Eclipse_Java_Projects_Programmatically
			initializeProject(project);
			return createJavaProject0(project);
		} catch (final CoreException e) {
			throw new RuntimeException(e);
		}
	}

	default void initializeProject(final IProject project) throws CoreException {
		project.open(null);
		final IProjectDescription description = project.getDescription();
		description.setNatureIds(new String[] { JavaCore.NATURE_ID });
		project.setDescription(description, null);
	}

	default IProject createEmptyProject() throws CoreException {
		final IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		final IProject project = workspaceRoot.getProject("emptyProject");
		project.create(null);
		return project;
	}

	default Pair<IJavaProject, IPackageFragment> createJavaProject0(final IProject project) throws CoreException {
		final IJavaProject javaProject = JavaCore.create(project);
		final IPackageFragment defaultPackage = setup(project, javaProject);
		return new ImmutablePair<>(javaProject, defaultPackage);
	}

	default IPackageFragment setup(final IProject project, final IJavaProject javaProject) throws CoreException {
		addOutputFolder(project, javaProject);
		addJreLibraryToClasspath(javaProject);
		final IFolder sourceFolder = addSourceFolder(project, javaProject);
		return createDefaultPackage(javaProject, sourceFolder);
	}

	default void addOutputFolder(final IProject project, final IJavaProject javaProject) throws CoreException {
		final IFolder binFolder = project.getFolder("bin");
		binFolder.create(false, true, null);
		javaProject.setOutputLocation(binFolder.getFullPath(), null);
	}

	default void addJreLibraryToClasspath(final IJavaProject javaProject) throws JavaModelException {
		final List<IClasspathEntry> entries = new ArrayList<>();
		final IVMInstall vmInstall = JavaRuntime.getDefaultVMInstall();
		final LibraryLocation[] locations = JavaRuntime.getLibraryLocations(vmInstall);
		for (final LibraryLocation element : locations) {
			entries.add(JavaCore.newLibraryEntry(element.getSystemLibraryPath(), null, null));
		}
		javaProject.setRawClasspath(entries.toArray(new IClasspathEntry[entries.size()]), null);
	}

	default IFolder addSourceFolder(final IProject project, final IJavaProject javaProject) throws CoreException {
		final IFolder sourceFolder = project.getFolder("src");
		sourceFolder.create(false, true, null);
		final IPackageFragmentRoot root = javaProject.getPackageFragmentRoot(sourceFolder);
		final IClasspathEntry[] oldEntries = javaProject.getRawClasspath();
		final IClasspathEntry[] newEntries = new IClasspathEntry[oldEntries.length + 1];
		System.arraycopy(oldEntries, 0, newEntries, 0, oldEntries.length);
		newEntries[oldEntries.length] = JavaCore.newSourceEntry(root.getPath());
		javaProject.setRawClasspath(newEntries, null);
		return sourceFolder;
	}

	default IPackageFragment createDefaultPackage(final IJavaProject javaProject, final IFolder sourceFolder)
			throws JavaModelException {
		return javaProject.getPackageFragmentRoot(sourceFolder).createPackageFragment("", false, null);
	}

	default void delete(final IJavaProject javaProject) {
		delete(javaProject.getProject());
	}

	default void delete(final IProject project) {
		try {
			project.delete(true, true, null);
		} catch (final CoreException e) {
			throw new RuntimeException(e);
		}
	}

	default void withJavaProject(final BiConsumer<IJavaProject, IPackageFragment> projectConsumer)
			throws CoreException {
		final IProject project = createEmptyProject();
		try {
			final Pair<IJavaProject, IPackageFragment> javaProjectAndDefaultPackage = createJavaProject(project);
			projectConsumer.accept(javaProjectAndDefaultPackage.getLeft(), javaProjectAndDefaultPackage.getRight());
		} finally {
			if (project != null) {
				delete(project);
			} else {
				delete(createEmptyProject());
			}
		}
	}

	default SingleVariableDeclaration createSingleVariableDeclaration(final AST ast, final String typeName,
			final String variableName, final int startPosition) {
		final int typeNameLength = typeName.length();
		final SimpleName typeNameNode = createSimpleName(ast, typeName, startPosition);
		final SimpleType type = createSimpleType(ast, startPosition, typeNameLength, typeNameNode);

		final SimpleName name = createSimpleName(ast, variableName, startPosition + 1 + typeNameLength);

		return createSingleVariableDeclaration(ast, variableName, startPosition, typeNameLength, type, name);
	}

	default SingleVariableDeclaration createSingleVariableDeclaration(final AST ast, final String variableName,
			final int startPosition, final int typeNameLength, final SimpleType type, final SimpleName name) {
		final SingleVariableDeclaration variableDeclaration = ast.newSingleVariableDeclaration();
		variableDeclaration.setType(type);
		variableDeclaration.setName(name);
		variableDeclaration.setSourceRange(startPosition, startPosition + typeNameLength + 1 + variableName.length());
		return variableDeclaration;
	}

	default ClassInstanceCreation createClassInstanceCreation(final AST ast, final String typeName,
			final int startPosition) {
		final int length = typeName.length();

		final SimpleName cicTypeName = createSimpleName(ast, typeName, startPosition);
		final SimpleType cicType = createSimpleType(ast, startPosition, length, cicTypeName);

		return createClassInstanceCreation(ast, startPosition, cicType);
	}

	default ClassInstanceCreation createClassInstanceCreation(final AST ast, final int startPosition,
			final SimpleType cicType) {
		final int length = "new ".length() + cicType.getLength() + "()".length();

		final ClassInstanceCreation cic = ast.newClassInstanceCreation();
		cic.setType(cicType);
		cic.setSourceRange(startPosition, length);
		return cic;
	}

	default SimpleType createSimpleType(final AST ast, final int startPosition, final int length,
			final SimpleName cicTypeName) {
		final SimpleType cicType = ast.newSimpleType(cicTypeName);
		cicType.setSourceRange(startPosition, length);
		return cicType;
	}

	default SimpleName createSimpleName(final AST ast, final String identifier, final int startPosition) {
		final SimpleName name = ast.newSimpleName(identifier);
		name.setSourceRange(startPosition, identifier.length());
		return name;
	}

}
