package org.eclipse.jdt.core.dom.anaphors.check;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.function.Consumer;

import org.assertj.core.api.AbstractBooleanAssert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.dom.anaphors.ASTTesting;

public interface SameInvocableCheckTesting extends ASTTesting {

	default void assertStringLiteralsExpression1AndExpression2AreInSameInvocable(final String typeName,
			final String source) throws CoreException {
		assertStringLiteralsExpression1AndExpression2(typeName, source, assertion -> assertion.isTrue());
	}

	default void assertStringLiteralsExpression1AndExpression2AreNotInSameInvocable(final String typeName,
			final String source) throws CoreException {
		assertStringLiteralsExpression1AndExpression2(typeName, source, assertion -> assertion.isFalse());
	}

	default void assertStringLiteralsExpression1AndExpression2(final String typeName, final String source,
			final Consumer<AbstractBooleanAssert<?>> assertionConsumer) throws CoreException {
		assertStringLiteralExpressions(typeName, source, (compilationUnit, expressions) -> {
			final AbstractBooleanAssert<?> assertion = assertThat(
					SameInvocableCheck.partOfSameInvocable(expressions.getLeft(), expressions.getRight()));
			assertionConsumer.accept(assertion);
		});
	}

}
