package org.eclipse.jdt.core.dom.anaphors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Iterator;

import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.junit.Test;

import de.monochromata.anaphors.ast.feature.Feature;
import de.monochromata.anaphors.ast.feature.FeatureContainer;
import de.monochromata.anaphors.ast.reference.Referent;

public class PublicDomTest implements PublicDomTesting {

	// Testing nameOfReferentMatchesConceptualTypeOfIdentifier(...)

	@Test
	public void testNameOfReferentMatchesConceptualTypeOfIdentifier_doesNotMatch() {

		final String id = "myName";
		final String referentName = "referentName";
		final boolean caseSensitive = false;
		final boolean matches = false;

		testNameOfReferentMatchesConceptualTypeOfIdentifierTemplate(id, referentName, caseSensitive, matches);
	}

	@Test
	public void testNameOfReferentMatchesConceptualTypeOfIdentifier_doesNotMatchCaseSensitively() {

		final String id = "myName";
		final String referentName = "myname";
		final boolean caseSensitive = true;
		final boolean matches = false;

		testNameOfReferentMatchesConceptualTypeOfIdentifierTemplate(id, referentName, caseSensitive, matches);
	}

	@Test
	public void testNameOfReferentMatchesConceptualTypeOfIdentifier_equality() {

		final String id = "myName";
		final String referentName = "myname";
		final boolean caseSensitive = false;
		final boolean matches = true;

		testNameOfReferentMatchesConceptualTypeOfIdentifierTemplate(id, referentName, caseSensitive, matches);
	}

	@Test
	public void testNameOfReferentMatchesConceptualTypeOfIdentifier_caseSensitiveEquality() {

		final String id = "myName";
		final String referentName = "myName";
		final boolean caseSensitive = true;
		final boolean matches = true;

		testNameOfReferentMatchesConceptualTypeOfIdentifierTemplate(id, referentName, caseSensitive, matches);
	}

	@Test
	public void testNameOfReferentMatchesConceptualTypeOfIdentifier_matchesWithPrefix() {

		final String id = "greenName";
		final String referentName = "name";
		final boolean caseSensitive = false;
		final boolean matches = true;

		testNameOfReferentMatchesConceptualTypeOfIdentifierTemplate(id, referentName, caseSensitive, matches);
	}

	@Test
	public void testNameOfReferentMatchesConceptualTypeOfIdentifier_matchesCasesSensitivelyWithFeaturePrefix() {

		final String id = "greenPartName";
		final String referentName = "partName";
		final boolean caseSensitive = true;
		final boolean matches = true;

		testNameOfReferentMatchesConceptualTypeOfIdentifierTemplate(id, referentName, caseSensitive, matches);
	}

	@Test
	public void testNameOfReferentMatchesConceptualTypeOfIdentifier_doesNotMatchCasesSensitivelyWithNonFeaturePrefix() {

		// There should not be a match if it is not clear whether the conceptual
		// type of the referent overlaps with the conceptual type of part of the
		// features of the id.

		final String id = "featureConceptualType";
		final String referentName = "eptualType";
		final boolean caseSensitive = true;
		final boolean matches = false;

		testNameOfReferentMatchesConceptualTypeOfIdentifierTemplate(id, referentName, caseSensitive, matches);
	}

	@SuppressWarnings("unchecked")
	protected void testNameOfReferentMatchesConceptualTypeOfIdentifierTemplate(final String id,
			final String referentName, final boolean caseSensitive, final boolean matches) {
		final Referent<ITypeBinding, CompilationUnit, String, String> referent = mock(Referent.class);
		when(referent.hasName()).thenReturn(true);
		when(referent.getName()).thenReturn(referentName);

		final PublicDom dom = new PublicDom();
		assertThat(dom.nameOfReferentMatchesConceptualTypeOfIdentifier(referent, id, caseSensitive)).isEqualTo(matches);
	}

	// Testing
	// nameOfIdentifierEqualsFauxHyponymOfSimpleNameOfTypeBinding(...)

	@Test
	public void testNameOfIdentifierEqualsFauxHyponymOfSimpleNameOfTypeBinding_matchesWith1CharacterPrefix() {
		testNameOfIdentifierEqualsFauxHyponymOfSimpleNameOfTypeBinding("panel", "JPanel", false, true);
	}

	@Test
	public void testNameOfIdentifierEqualsFauxHyponymOfSimpleNameOfTypeBinding_matchesWithLongPrefix() {
		testNameOfIdentifierEqualsFauxHyponymOfSimpleNameOfTypeBinding("schwert", "MetallSchwert", false, true);
	}

	protected void testNameOfIdentifierEqualsFauxHyponymOfSimpleNameOfTypeBinding(final String id,
			final String typeName, final boolean caseSensitive, final boolean matches) {
		// TODO: Add further tests for caseSensitive=true
		final ITypeBinding type = mock(ITypeBinding.class);
		when(type.getErasure()).thenReturn(type);
		when(type.getName()).thenReturn(typeName);

		final PublicDom dom = new PublicDom();
		assertThat(dom.nameOfIdentifierEqualsFauxHyponymOfSimpleNameOfTypeBinding(id, type, caseSensitive))
				.isEqualTo(matches);
	}

	// Testing
	// getFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentName(...)

	@Test
	public void testGetFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentName_doesNotMatch() {

		final String id = "myName";
		final String referentName = "referentName";
		final boolean caseSensitive = false;

		assertThat(testGetFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentNameTemplate(id, referentName,
				caseSensitive)).isNull();
	}

	@Test
	public void testGetFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentName_doesNotMatchCaseSensitively() {

		final String id = "myName";
		final String referentName = "myname";
		final boolean caseSensitive = true;

		assertThat(testGetFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentNameTemplate(id, referentName,
				caseSensitive)).isNull();
	}

	@Test
	public void testGetFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentName_equality() {

		final String id = "myName";
		final String referentName = "myname";
		final boolean caseSensitive = false;

		final FeatureContainer<String> actualFeatures = testGetFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentNameTemplate(
				id, referentName, caseSensitive);

		assertThat(actualFeatures).isNotNull();
		assertThat(actualFeatures.getFeatures()).isEmpty();
	}

	@Test
	public void testGetFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentName_caseSensitiveEquality() {

		final String id = "myName";
		final String referentName = "myName";
		final boolean caseSensitive = true;

		final FeatureContainer<String> actualFeatures = testGetFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentNameTemplate(
				id, referentName, caseSensitive);

		assertThat(actualFeatures).isNotNull();
		assertThat(actualFeatures.getFeatures()).isEmpty();
	}

	@Test
	public void testGetFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentName_matchesWithPrefix() {

		final String id = "greenName";
		final String referentName = "name";
		final boolean caseSensitive = false;

		final FeatureContainer<String> actualFeatures = testGetFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentNameTemplate(
				id, referentName, caseSensitive);

		assertThat(actualFeatures).isNotNull();
		assertThat(actualFeatures.getFeatures()).hasSize(1);
		final Feature<String> feature1 = actualFeatures.getFeatures().iterator().next();
		assertThat(feature1).isEqualTo(new DomFeature("green"));
	}

	@Test
	public void testGetFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentName_matchesCasesSensitivelyWithFeaturePrefix() {

		final String id = "greenPartName";
		final String referentName = "partName";
		final boolean caseSensitive = true;

		final FeatureContainer<String> actualFeatures = testGetFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentNameTemplate(
				id, referentName, caseSensitive);

		assertThat(actualFeatures).isNotNull();
		assertThat(actualFeatures.getFeatures()).hasSize(1);
		final Feature<String> feature1 = actualFeatures.getFeatures().iterator().next();
		assertThat(feature1).isEqualTo(new DomFeature("green"));
	}

	@Test
	public void testGetFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentName_doesNotMatchCasesSensitivelyWithNonFeaturePrefix() {

		// There should not be a match if it is not clear whether the conceptual
		// type of the referent overlaps with the conceptual type of part of the
		// features of the id.

		final String id = "featureConceptualType";
		final String referentName = "eptualType";
		final boolean caseSensitive = true;

		final FeatureContainer<String> actualFeatures = testGetFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentNameTemplate(
				id, referentName, caseSensitive);

		assertThat(actualFeatures).isNull();
	}

	@Test
	public void testGetFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentName_matchesCasesSensitivelyWithMultipleFeatures() {

		final String id = "smallGreenPartName";
		final String referentName = "partName";
		final boolean caseSensitive = true;

		final FeatureContainer<String> actualFeatures = testGetFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentNameTemplate(
				id, referentName, caseSensitive);

		assertThat(actualFeatures).isNotNull();
		assertThat(actualFeatures.getFeatures()).hasSize(2);
		final Iterator<Feature<String>> iterator = actualFeatures.getFeatures().iterator();
		final Feature<String> feature1 = iterator.next();
		final Feature<String> feature2 = iterator.next();
		assertThat(feature1).isEqualTo(new DomFeature("small"));
		assertThat(feature2).isEqualTo(new DomFeature("green"));
	}

}
