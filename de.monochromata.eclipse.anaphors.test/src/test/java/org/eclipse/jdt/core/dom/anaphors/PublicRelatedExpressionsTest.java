package org.eclipse.jdt.core.dom.anaphors;

import static de.monochromata.anaphors.ast.relatedexp.strategy.LocalTempVariableContents.ANCHOR;
import static de.monochromata.anaphors.ast.relatedexp.strategy.LocalTempVariableContents.REFERENT;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.LambdaExpression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.junit.Test;

import de.monochromata.anaphors.ast.feature.Feature;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalTempVariableContents;

public class PublicRelatedExpressionsTest implements PublicRelatedExpressionsTesting {

	private final PublicRelatedExpressions spi = new PublicRelatedExpressions();
	private final AST ast = AST.newAST(AST.JLS8);

	// Test getFeatures()

	@Test
	public void getFeatures_forDecimalIntegerLiteralWithoutTypeSuffix() {
		assertNumberLiteralFeatures(ast, "10", "10");
	}

	@Test
	public void getFeatures_forDecimalIntegerLiteralWitLowerCaseTypeSuffix() {
		assertNumberLiteralFeatures(ast, "10l", "10l");
	}

	@Test
	public void getFeatures_forDecimalIntegerLiteralWitUpperCaseTypeSuffix() {
		assertNumberLiteralFeatures(ast, "10L", "10L");
	}

	@Test
	public void getFeatures_forDecimalIntegerLiteralWithUnderscores() {
		assertNumberLiteralFeatures(ast, "10_20_30", "10_20_30");
	}

	@Test
	public void getFeatures_forDecimalIntegerLiteralWithUnderscoresInARow() {
		assertNumberLiteralFeatures(ast, "10__100", "10__100");
	}

	@Test
	public void getFeatures_forHexadecimalIntegerLiteral() {
		assertNumberLiteralFeatures(ast, "0x0123f", "0x0123f");
	}

	@Test
	public void getFeatures_forOctalIntegerLiteral() {
		assertNumberLiteralFeatures(ast, "01234", "01234");
	}

	@Test
	public void getFeatures_forLowerCaseBinaryIntegerLiteral() {
		assertNumberLiteralFeatures(ast, "0b10010", "0b10010");
	}

	@Test
	public void getFeatures_forUpperCaseBinaryIntegerLiteral() {
		assertNumberLiteralFeatures(ast, "0B10010", "0B10010");
	}

	@Test
	public void getFeatures_forDecimalFloatingPointLiteral() {
		// While anaphors i.e. identifiers cannot contain "." it also does not make
		// sense to split the floating point number into "1" and "9". Let's see what
		// ideas come up in case this becomes problematic.
		assertNumberLiteralFeatures(ast, "1.9", "1.9");
	}

	@Test
	public void getFeatures_forDecimalFloatingPointLiteralWithoutLeadingDigits() {
		assertNumberLiteralFeatures(ast, ".9", ".9");
	}

	@Test
	public void getFeatures_forDecimalFloatingPointLiteralWithExponent() {
		assertNumberLiteralFeatures(ast, "1.9e3", "1.9e3");
	}

	@Test
	public void getFeatures_forDecimalFloatingPointLiteralPositiveExponent() {
		assertNumberLiteralFeatures(ast, "1.9E+2", "1.9E+2");
	}

	@Test
	public void getFeatures_forDecimalFloatingPointLiteralWithNegativeExponent() {
		assertNumberLiteralFeatures(ast, "1.9e-12", "1.9e-12");
	}

	@Test
	public void getFeatures_forDecimalFloatingPointLiteralWithLowerCaseFloatSuffix() {
		assertNumberLiteralFeatures(ast, "1.9f", "1.9f");
	}

	@Test
	public void getFeatures_forDecimalFloatingPointLiteralWithUpperCaseFloatSuffix() {
		assertNumberLiteralFeatures(ast, "1.9F", "1.9F");
	}

	@Test
	public void getFeatures_forDecimalFloatingPointLiteralWithLowerCaseDoubleSuffix() {
		assertNumberLiteralFeatures(ast, "1.9d", "1.9d");
	}

	@Test
	public void getFeatures_forDecimalFloatingPointLiteralWithUpperCaseDoubleSuffix() {
		assertNumberLiteralFeatures(ast, "1.933D", "1.933D");
	}

	@Test
	public void getFeatures_forHexadecimalFloatingPointLiteral() {
		assertNumberLiteralFeatures(ast, "0xAE.9p-3D", "0xAE.9p-3D");
	}

	@Test
	public void getFeatures_forTrueBooleanLiteral() {
		final var literal = ast.newBooleanLiteral(true);
		assertAstNodeYieldsFeatures(literal, "true");
	}

	@Test
	public void getFeatures_forFalseBooleanLiteral() {
		final var literal = ast.newBooleanLiteral(false);
		assertAstNodeYieldsFeatures(literal, "false");
	}

	@Test
	public void getFeatures_forCharacterLiteral() {
		assertCharacterLiteralFeatures(ast, 'a', "a");
	}

	@Test
	public void getFeatures_forEscapeSequenceCharacterLiteral() {
		assertCharacterLiteralFeatures(ast, '\t', "\t");
	}

	@Test
	public void getFeatures_forOctalEscapeCharacterLiteral() {
		// Note that this is an octal escape, not a unicode escape as unicode escapes
		// are processed very early.
		assertCharacterLiteralFeatures(ast, '\0', "\0");
	}

	@Test
	public void getFeatures_forSingleWordStringLiteral() {
		assertStringLiteralYieldsFeatures(ast, "feature", "feature");
	}

	@Test
	public void getFeatures_forUmlautStringLiteral() {
		assertStringLiteralYieldsFeatures(ast, "aufräumen", "aufräumen");
	}

	@Test
	public void getFeatures_forWordWithTrailingQuestionMark() {
		assertStringLiteralYieldsFeatures(ast, "wohin?", "wohin");
	}

	@Test
	@SuppressWarnings("unchecked")
	public void getFeatures_forMultiWordStringLiteral() {
		final Set<Feature<String>> features = mock(Set.class);

		assertStringLiteralYieldsFeatures(ast, "mehrere Worte", features);

		verify(features).add(new DomFeature("mehrere"));
		verify(features).add(new DomFeature("worte"));
		verifyNoMoreInteractions(features);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void getFeatures_forEmptyStringLiteral() {
		final Set<Feature<String>> features = mock(Set.class);

		assertStringLiteralYieldsFeatures(ast, "", features);

		verifyNoInteractions(features);
	}

	@Test
	public void getFeatures_forNullLiteral() {
		final var nullLiteral = ast.newNullLiteral();

		assertAstNodeYieldsFeatures(nullLiteral, "null");
	}

	@Test
	public void getFeatures_forSimpleName() {
		final var simpleName = ast.newSimpleName("myName");

		assertAstNodeYieldsFeatures(simpleName, "my", "name");
	}

	@Test
	public void getFeatures_forCICArguments() {
		final var cic = ast.newClassInstanceCreation();
		cic.arguments().add(ast.newSimpleName("hello"));
		cic.arguments().add(ast.newSimpleName("world"));

		assertAstNodeYieldsFeatures(cic, "hello", "world");
	}

	@Test
	public void getFeatures_forCICInCIC() {
		final var innerCIC = ast.newClassInstanceCreation();
		// is added because it is part of an argument to the related expression
		innerCIC.setType(ast.newSimpleType(ast.newSimpleName("World")));
		innerCIC.arguments().add(ast.newSimpleName("hello"));

		final var outerCIC = ast.newClassInstanceCreation();
		// is not added because it is part of the type of the related expression
		outerCIC.setType(ast.newSimpleType(ast.newSimpleName("Foo")));
		outerCIC.arguments().add(innerCIC);

		assertAstNodeYieldsFeatures(outerCIC, "hello", "world");
	}

	@Test
	public void getFeatures_forLamdaExpressionInCICArgument() {
		final var lambdaParameter = ast.newVariableDeclarationFragment();
		lambdaParameter.setName(ast.newSimpleName("lambdaParameter"));
		final var lambda = ast.newLambdaExpression();
		lambda.parameters().add(lambdaParameter);
		lambda.setBody(ast.newQualifiedName(ast.newSimpleName("lambdaParameter"), ast.newSimpleName("foo")));

		final var cic = ast.newClassInstanceCreation();
		// is not added because it is part of the type of the related expression
		cic.setType(ast.newSimpleType(ast.newSimpleName("Type")));
		cic.arguments().add(lambda);

		final Set<Feature<String>> features = mock(Set.class);

		getFeatures(cic, features);

		verify(features, times(2)).add(new DomFeature("lambda"));
		verify(features, times(2)).add(new DomFeature("parameter"));
		verify(features).add(new DomFeature("foo"));
		verifyNoMoreInteractions(features);
	}

	@Test
	public void getFeatures_forCICWithAnonymousClassDeclaration() {
		final var variableDeclarationFragment = ast.newVariableDeclarationFragment();
		variableDeclarationFragment.setName(ast.newSimpleName("foo"));
		final var barLiteral = ast.newStringLiteral();
		barLiteral.setLiteralValue("bar");
		variableDeclarationFragment.setInitializer(barLiteral);

		final var fieldDeclaration = ast.newFieldDeclaration(variableDeclarationFragment);
		fieldDeclaration.setType(ast.newSimpleType(ast.newSimpleName("TypeName")));

		final var anonymousClassDeclaration = ast.newAnonymousClassDeclaration();
		anonymousClassDeclaration.bodyDeclarations().add(fieldDeclaration);

		final var cic = ast.newClassInstanceCreation();
		cic.setAnonymousClassDeclaration(anonymousClassDeclaration);

		assertAstNodeYieldsFeatures(cic, "type", "name", "foo", "bar");
	}

	@Test
	public void getFeatures_forVariableDeclarationFragmentInitializer() {
		final var initializer = ast.newStringLiteral();
		initializer.setLiteralValue("initializer");

		final var fragment = ast.newVariableDeclarationFragment();
		fragment.setName(ast.newSimpleName("bar"));
		fragment.setInitializer(initializer);

		assertAstNodeYieldsFeatures(fragment, "initializer");
	}

	@Test
	public void getFeatures_forVariableDeclarationStatementInitializer() {
		final var initializer = ast.newStringLiteral();
		initializer.setLiteralValue("initializer");

		final var fragment = ast.newVariableDeclarationFragment();
		fragment.setName(ast.newSimpleName("bar"));
		fragment.setInitializer(initializer);

		final var lvd = ast.newVariableDeclarationStatement(fragment);
		lvd.setType(ast.newSimpleType(ast.newSimpleName("Foo")));

		assertAstNodeYieldsFeatures(lvd, "initializer");
	}

	@Test
	public void getFeatures_forSingleVariableDeclaration() {
		final var pd = ast.newSingleVariableDeclaration();
		pd.setType(ast.newSimpleType(ast.newSimpleName("Foo")));
		pd.setName(ast.newSimpleName("bar"));

		assertAstNodeYieldsFeatures(pd); // No features expected
	}

	@Test
	public void getFeatures_forMethodInvocation() {
		final var mi = ast.newMethodInvocation();
		mi.setExpression(ast.newSimpleName("expression"));
		mi.setName(ast.newSimpleName("bar"));
		mi.arguments().add(ast.newSimpleName("argument"));

		// Ignore expression and method name
		assertAstNodeYieldsFeatures(mi, "argument");
	}

	// Test guessTempName()

	@Test
	public void guessTempNameForSingleDirectAnaphor() {
		final List<Pair<LocalTempVariableContents, String>> variableContentsAndAnaphors = singletonList(
				new ImmutablePair<>(REFERENT, "anaphor"));

		assertThat(spi.guessTempName(null, variableContentsAndAnaphors, null)).isEqualTo("anaphor");
	}

	@Test
	public void guessTempNameForSingleIndirectAnaphor() {
		final PublicRelatedExpression relatedExpression = mockRelatedExpression();
		final List<Pair<LocalTempVariableContents, String>> variableContentsAndAnaphors = singletonList(
				new ImmutablePair<>(ANCHOR, "anaphor"));

		assertThat(spi.guessTempName(relatedExpression, variableContentsAndAnaphors, null)).isEqualTo("typeName");
	}

	@Test
	public void guessTempNameForTwoIndirectAnaphorsDoesNotConsiderAnaphorsButTheSingleRelatedExpressionOnly() {
		final PublicRelatedExpression relatedExpression = mockRelatedExpression();
		final List<Pair<LocalTempVariableContents, String>> variableContentsAndAnaphors = asList(
				new ImmutablePair<>(ANCHOR, "anaphor1"), new ImmutablePair<>(ANCHOR, "anaphor2"));

		assertThat(spi.guessTempName(relatedExpression, variableContentsAndAnaphors, null)).isEqualTo("typeName");
	}

	@Test
	public void guessTempNameForDirectAndIndirectAnaphorChoosesShorterFromDirectAnaphor() {
		final PublicRelatedExpression relatedExpression = mockRelatedExpression();
		final List<Pair<LocalTempVariableContents, String>> variableContentsAndAnaphors = asList(
				new ImmutablePair<>(REFERENT, "shorter"), new ImmutablePair<>(ANCHOR, "anaphor2"));

		assertThat(spi.guessTempName(relatedExpression, variableContentsAndAnaphors, null)).isEqualTo("shorter");
	}

	@Test
	public void guessTempNameForDirectAndIndirectAnaphorChoosesShorterFromIndirectAnaphor() {
		final PublicRelatedExpression relatedExpression = mockRelatedExpression();
		final List<Pair<LocalTempVariableContents, String>> variableContentsAndAnaphors = asList(
				new ImmutablePair<>(REFERENT, "veryVeryVeryVeryLong"), new ImmutablePair<>(ANCHOR, "anaphor2"));

		assertThat(spi.guessTempName(relatedExpression, variableContentsAndAnaphors, null)).isEqualTo("typeName");
	}

	@Test
	public void guessTempNameAmongTwoDirectAnaphorsChoosesShorterOne() {
		final List<Pair<LocalTempVariableContents, String>> variableContentsAndAnaphors = asList(
				new ImmutablePair<>(REFERENT, "veryVeryVeryVeryLong"), new ImmutablePair<>(REFERENT, "shorter"));

		assertThat(spi.guessTempName(null, variableContentsAndAnaphors, null)).isEqualTo("shorter");
	}

	@Test
	public void isParameterDeclaration_returnsFalseForMethodDeclaration() {
		final MethodDeclaration target = ast.newMethodDeclaration();
		target.parameters().add(ast.newSingleVariableDeclaration());

		assertThat(spi.isParameterDeclaration(target)).isFalse();
	}

	@Test
	public void isParameterDeclaration_returnsTrueForSVDInMethodDeclaration() {
		final SingleVariableDeclaration svd = ast.newSingleVariableDeclaration();
		final MethodDeclaration md = ast.newMethodDeclaration();
		md.parameters().add(svd);

		assertThat(spi.isParameterDeclaration(svd)).isTrue();
	}

	@Test
	public void isParameterDeclaration_returnsFalseForLambdaExpression() {
		final LambdaExpression target = ast.newLambdaExpression();
		target.parameters().add(ast.newSingleVariableDeclaration());

		assertThat(spi.isParameterDeclaration(target)).isFalse();
	}

	@Test
	public void isParameterDeclaration_returnsTrueForSVDInLambdaExpression() {
		final SingleVariableDeclaration target = ast.newSingleVariableDeclaration();
		final LambdaExpression le = ast.newLambdaExpression();
		le.parameters().add(target);

		assertThat(spi.isParameterDeclaration(target)).isTrue();
	}

	@Test
	public void isLocalVariableDeclaration_returnsFalseForMethodDeclaration() {
		final MethodDeclaration target = ast.newMethodDeclaration();
		target.parameters().add(ast.newSingleVariableDeclaration());

		assertThat(spi.isLocalVariableDeclaration(target)).isFalse();
	}

	@Test
	public void isLocalVariableDeclaration_returnsFalseForSVDInMethodDeclaration() {
		final SingleVariableDeclaration svd = ast.newSingleVariableDeclaration();
		final MethodDeclaration md = ast.newMethodDeclaration();
		md.parameters().add(svd);

		assertThat(spi.isLocalVariableDeclaration(svd)).isFalse();
	}

	@Test
	public void isLocalVariableDeclaration_returnsFalseForLambdaExpression() {
		final LambdaExpression target = ast.newLambdaExpression();
		target.parameters().add(ast.newSingleVariableDeclaration());

		assertThat(spi.isLocalVariableDeclaration(target)).isFalse();
	}

	@Test
	public void isLocalVariableDeclaration_returnsFalseForSVDInLambdaExpression() {
		final SingleVariableDeclaration target = ast.newSingleVariableDeclaration();
		final LambdaExpression le = ast.newLambdaExpression();
		le.parameters().add(target);

		assertThat(spi.isLocalVariableDeclaration(target)).isFalse();
	}

	@Test
	public void isLocalVariableDeclaration_returnsTrueForVDFInLambdaExpression() {
		final VariableDeclarationFragment target = ast.newVariableDeclarationFragment();
		final LambdaExpression le = ast.newLambdaExpression();
		le.parameters().add(target);

		assertThat(spi.isLocalVariableDeclaration(target)).isTrue();
	}

	@Test
	public void isLocalVariableDeclaration_returnsTrueForVDFInVDE() {
		final VariableDeclarationFragment target = ast.newVariableDeclarationFragment();
		ast.newVariableDeclarationExpression(target);

		assertThat(spi.isLocalVariableDeclaration(target)).isTrue();
	}

	@Test
	public void isLocalVariableDeclaration_returnsFalseForVDEwithSingleVDF() {
		final VariableDeclarationExpression target = ast
				.newVariableDeclarationExpression(ast.newVariableDeclarationFragment());

		assertThat(spi.isLocalVariableDeclaration(target)).isFalse();
	}

	@Test
	public void isLocalVariableDeclaration_returnsFalseForVDEwithMultipleVDF() {
		final VariableDeclarationExpression target = ast
				.newVariableDeclarationExpression(ast.newVariableDeclarationFragment());
		target.fragments().add(ast.newVariableDeclarationFragment());

		assertThat(spi.isLocalVariableDeclaration(target)).isFalse();
	}

	@Test
	public void isLocalVariableDeclaration_returnsTrueForVDFInVDS() {
		final VariableDeclarationFragment target = ast.newVariableDeclarationFragment();
		ast.newVariableDeclarationStatement(target);

		assertThat(spi.isLocalVariableDeclaration(target)).isTrue();
	}

	@Test
	public void isLocalVariableDeclaration_returnsTrueForVDSwithSingleVDF() {
		final VariableDeclarationStatement target = ast
				.newVariableDeclarationStatement(ast.newVariableDeclarationFragment());

		assertThat(spi.isLocalVariableDeclaration(target)).isTrue();
	}

	@Test
	public void isLocalVariableDeclaration_returnsFalseForVDSwithMultipleVDF() {
		final VariableDeclarationStatement target = ast
				.newVariableDeclarationStatement(ast.newVariableDeclarationFragment());
		target.fragments().add(ast.newVariableDeclarationFragment());

		assertThat(spi.isLocalVariableDeclaration(target)).isFalse();
	}

	@Test
	public void hasInitializerThrowsIllegalArgumentExceptionForStringLiteral() {
		final StringLiteral stringLiteral = ast.newStringLiteral();
		assertThatThrownBy(() -> spi.hasInitializer(stringLiteral, ast.newNumberLiteral()))
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessage("Unexpected node type: " + stringLiteral.getClass().getName());
	}

	@Test
	public void hasInitializerReturnsFalseForMethodDeclaration() {
		final MethodDeclaration target = ast.newMethodDeclaration();
		target.parameters().add(ast.newSingleVariableDeclaration());

		assertThat(spi.hasInitializer(target, ast.newStringLiteral())).isFalse();
	}

	@Test
	public void hasInitializerReturnsFalseForSVDInMethodDeclaration() {
		final SingleVariableDeclaration svd = ast.newSingleVariableDeclaration();
		final MethodDeclaration md = ast.newMethodDeclaration();
		md.parameters().add(svd);

		assertThat(spi.hasInitializer(svd, ast.newStringLiteral())).isFalse();
	}

	@Test
	public void hasInitializerReturnsFalseForLambdaExpression() {
		final LambdaExpression target = ast.newLambdaExpression();
		target.parameters().add(ast.newSingleVariableDeclaration());

		assertThat(spi.hasInitializer(target, ast.newStringLiteral())).isFalse();
	}

	@Test
	public void hasInitializerReturnsFalseForSVDInLambdaExpression() {
		final SingleVariableDeclaration target = ast.newSingleVariableDeclaration();
		final LambdaExpression le = ast.newLambdaExpression();
		le.parameters().add(target);

		assertThat(spi.hasInitializer(target, ast.newStringLiteral())).isFalse();
	}

	@Test
	public void hasInitializerReturnsFalseForVDFInLambdaExpression() {
		final VariableDeclarationFragment target = ast.newVariableDeclarationFragment();
		final LambdaExpression le = ast.newLambdaExpression();
		le.parameters().add(target);

		assertThat(spi.hasInitializer(target, ast.newStringLiteral())).isFalse();
	}

	@Test
	public void hasInitializerReturnsReturnsFalseForVDFInVDEWithOtherInitializer() {
		final VariableDeclarationFragment target = ast.newVariableDeclarationFragment();
		target.setInitializer(ast.newStringLiteral());
		ast.newVariableDeclarationExpression(target);

		assertThat(spi.hasInitializer(target, ast.newStringLiteral())).isFalse();
	}

	@Test
	public void hasInitializerReturnsReturnsTrueForVDFInVDEWithSameInitializer() {
		final StringLiteral initializer = ast.newStringLiteral();
		final VariableDeclarationFragment target = ast.newVariableDeclarationFragment();
		target.setInitializer(initializer);
		ast.newVariableDeclarationExpression(target);

		assertThat(spi.hasInitializer(target, initializer)).isTrue();
	}

	@Test
	public void hasInitializerReturnsFalseForVDFInVDSWithOtherInitializer() {
		final VariableDeclarationFragment target = ast.newVariableDeclarationFragment();
		target.setInitializer(ast.newStringLiteral());
		ast.newVariableDeclarationStatement(target);

		assertThat(spi.hasInitializer(target, ast.newStringLiteral())).isFalse();
	}

	@Test
	public void hasInitializerReturnsTrueForVDFInVDSWithSameInitializer() {
		final StringLiteral initializer = ast.newStringLiteral();
		final VariableDeclarationFragment target = ast.newVariableDeclarationFragment();
		target.setInitializer(initializer);
		ast.newVariableDeclarationStatement(target);

		assertThat(spi.hasInitializer(target, initializer)).isTrue();
	}

	@Test
	public void hasInitializerReturnsFalseForVDSwithSingleVDFWithOtherInitializer() {
		final VariableDeclarationFragment vdf = ast.newVariableDeclarationFragment();
		vdf.setInitializer(ast.newStringLiteral());
		final VariableDeclarationStatement target = ast.newVariableDeclarationStatement(vdf);

		assertThat(spi.hasInitializer(target, ast.newStringLiteral())).isFalse();
	}

	@Test
	public void hasInitializerReturnsTrueForVDSwithSingleVDFWithSameInitializer() {
		final StringLiteral initializer = ast.newStringLiteral();
		final VariableDeclarationFragment vdf = ast.newVariableDeclarationFragment();
		vdf.setInitializer(initializer);
		final VariableDeclarationStatement target = ast.newVariableDeclarationStatement(vdf);

		assertThat(spi.hasInitializer(target, initializer)).isTrue();
	}

	@Test
	public void hasInitializerReturnsFalseForVDSwithMultipleVDF() {
		final StringLiteral initializer = ast.newStringLiteral();
		final VariableDeclarationFragment vdf1 = ast.newVariableDeclarationFragment();
		vdf1.setInitializer(initializer);
		final VariableDeclarationStatement target = ast.newVariableDeclarationStatement(vdf1);
		target.fragments().add(ast.newVariableDeclarationFragment());

		assertThat(spi.hasInitializer(target, initializer)).isFalse();
	}

	@Test
	public void getLocalDeclarationIdentifierThrowsIllegalArgumentExceptionForMethodDeclaration() {
		final MethodDeclaration target = ast.newMethodDeclaration();
		target.parameters().add(ast.newSingleVariableDeclaration());

		assertThatThrownBy(() -> spi.getLocalDeclarationIdentifier(target)).isInstanceOf(IllegalArgumentException.class)
				.hasMessage("Unexpected node type: " + target.getClass().getName());
	}

	@Test
	public void getLocalDeclarationIdentifierReturnsIdentifierForSVDInMethodDeclaration() {
		final String identifier = "identifier";
		final SimpleName name = ast.newSimpleName(identifier);
		final SingleVariableDeclaration svd = ast.newSingleVariableDeclaration();
		svd.setName(name);
		final MethodDeclaration md = ast.newMethodDeclaration();
		md.parameters().add(svd);

		assertThat(spi.getLocalDeclarationIdentifier(svd)).isSameAs(identifier);
	}

	@Test
	public void getLocalDeclarationIdentifierThrowsIllegalArgumentExceptionForLambdaExpression() {
		final LambdaExpression target = ast.newLambdaExpression();
		target.parameters().add(ast.newSingleVariableDeclaration());

		assertThatThrownBy(() -> spi.getLocalDeclarationIdentifier(target)).isInstanceOf(IllegalArgumentException.class)
				.hasMessage("Unexpected node type: " + target.getClass().getName());
	}

	@Test
	public void getLocalDeclarationIdentifierReturnsIdentifierForSVDInLambdaExpression() {
		final String identifier = "identifier";
		final SimpleName name = ast.newSimpleName(identifier);
		final SingleVariableDeclaration target = ast.newSingleVariableDeclaration();
		target.setName(name);
		final LambdaExpression le = ast.newLambdaExpression();
		le.parameters().add(target);

		assertThat(spi.getLocalDeclarationIdentifier(target)).isSameAs(identifier);
	}

	@Test
	public void getLocalDeclarationIdentifierReturnsIdentifierForVDFInLambdaExpression() {
		final String identifier = "identifier";
		final SimpleName name = ast.newSimpleName(identifier);
		final VariableDeclarationFragment target = ast.newVariableDeclarationFragment();
		target.setName(name);
		final LambdaExpression le = ast.newLambdaExpression();
		le.parameters().add(target);

		assertThat(spi.getLocalDeclarationIdentifier(target)).isSameAs(identifier);
	}

	@Test
	public void getLocalDeclarationIdentifierReturnsIdentifierForVDFInVDE() {
		final String identifier = "identifier";
		final SimpleName name = ast.newSimpleName(identifier);
		final VariableDeclarationFragment target = ast.newVariableDeclarationFragment();
		target.setName(name);
		ast.newVariableDeclarationExpression(target);

		assertThat(spi.getLocalDeclarationIdentifier(target)).isSameAs(identifier);
	}

	@Test
	public void getLocalDeclarationIdentifierThrowsIllegalArgumentExceptionForVDE() {
		final VariableDeclarationExpression target = ast
				.newVariableDeclarationExpression(ast.newVariableDeclarationFragment());

		assertThatThrownBy(() -> spi.getLocalDeclarationIdentifier(target)).isInstanceOf(IllegalArgumentException.class)
				.hasMessage("Unexpected node type: " + target.getClass().getName());
	}

	@Test
	public void getLocalDeclarationIdentifierReturnsIdentifierForVDFInVDS() {
		final String identifier = "identifier";
		final SimpleName name = ast.newSimpleName(identifier);
		final VariableDeclarationFragment target = ast.newVariableDeclarationFragment();
		target.setName(name);
		ast.newVariableDeclarationStatement(target);

		assertThat(spi.getLocalDeclarationIdentifier(target)).isSameAs(identifier);
	}

	@Test
	public void getLocalDeclarationIdentifierReturnsIdentifierForVDSwithSingleVDF() {
		final String identifier = "identifier";
		final SimpleName name = ast.newSimpleName(identifier);
		final VariableDeclarationFragment vdf = ast.newVariableDeclarationFragment();
		vdf.setName(name);
		final VariableDeclarationStatement target = ast.newVariableDeclarationStatement(vdf);

		assertThat(spi.getLocalDeclarationIdentifier(target)).isSameAs(identifier);
	}

	@Test
	public void getLocalDeclarationIdentifierThrowsIllegalArgumentExceptionForVDSwithMultipleVDF() {
		final VariableDeclarationStatement target = ast
				.newVariableDeclarationStatement(ast.newVariableDeclarationFragment());
		target.fragments().add(ast.newVariableDeclarationFragment());

		assertThatThrownBy(() -> spi.getLocalDeclarationIdentifier(target)).isInstanceOf(IllegalArgumentException.class)
				.hasMessage("Cannot obtain variable identifier from " + target.getClass().getName()
						+ " with multiple variable declaration fragments");
	}

}
