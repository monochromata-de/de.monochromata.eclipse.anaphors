package org.eclipse.jdt.core.dom.anaphors.transform;

import static org.eclipse.core.runtime.Assert.isTrue;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.core.filebuffers.FileBuffers;
import org.eclipse.core.filebuffers.LocationKind;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.CompilationUnit;

public interface FileBufferTesting {

	static void createAndCloseFileBufferFor(final CompilationUnit scope, final Runnable runnable) {
		final Pair<IPath, LocationKind> locationAndKind = getLocationAndKind(scope);
		try {
			createFileBuffer(scope, locationAndKind);
			becomeWorkingCopy(scope);
			try {
				runnable.run();
			} finally {
				releaseWorkingCopy(scope);
				closeFileBuffer(scope, locationAndKind);
			}
		} catch (final CoreException e) {
			throw new IllegalStateException(e);
		}
	}

	static void becomeWorkingCopy(final CompilationUnit scope) throws JavaModelException {
		final ICompilationUnit cu = (ICompilationUnit) scope.getTypeRoot();
		cu.becomeWorkingCopy(null);
		isTrue(cu.isWorkingCopy(), cu + " could not be turned into a working copy");
	}

	static void releaseWorkingCopy(final CompilationUnit scope) throws JavaModelException {
		final ICompilationUnit cu = (ICompilationUnit) scope.getTypeRoot();
		cu.discardWorkingCopy();
		isTrue(!cu.isWorkingCopy(), "Working copy for " + cu + " could not be discarded");
		// Unlike CompilationUnitChange.releaseDocument(...) we do not take care
		// of re-conciliation and consistency.
	}

	static void createFileBuffer(final CompilationUnit scope, final Pair<IPath, LocationKind> locationAndKind)
			throws CoreException {
		FileBuffers.getTextFileBufferManager().connect(locationAndKind.getLeft(), locationAndKind.getRight(), null);
	}

	static Pair<IPath, LocationKind> getLocationAndKind(final CompilationUnit scope) {
		final IPath location = getLocation(scope);
		final LocationKind locationKind = getLocationKind(location);
		final Pair<IPath, LocationKind> locationAndKind = new ImmutablePair<>(location, locationKind);
		return locationAndKind;
	}

	static void closeFileBuffer(final CompilationUnit scope, final Pair<IPath, LocationKind> locationAndKind)
			throws CoreException {
		FileBuffers.getTextFileBufferManager().disconnect(locationAndKind.getLeft(), locationAndKind.getRight(), null);
	}

	static IPath getLocation(final CompilationUnit scope) {
		final IPath location = scope.getTypeRoot().getPath();
		return location;
	}

	static LocationKind getLocationKind(final IPath location) {
		// TODO: Which location kind to choose if not IFile? See the JavaDoc of
		// getTextFileBuffer(...) and LocationKind
		return location instanceof IFile ? LocationKind.IFILE : LocationKind.NORMALIZE;
	}

}
