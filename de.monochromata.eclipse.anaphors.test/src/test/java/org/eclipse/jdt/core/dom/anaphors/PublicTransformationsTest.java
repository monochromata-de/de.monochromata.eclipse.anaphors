package org.eclipse.jdt.core.dom.anaphors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.jdt.core.dom.anaphors.check.CallChainCheckTesting.MAX_DEPTH_OF_CALL_CHAIN_ANALYSIS_IN_TEST;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.junit.Test;

import de.monochromata.anaphors.ast.transform.ASTTransformation;

public class PublicTransformationsTest implements PublicTransformationsTesting {

	@Test
	public void nullTransformationForPassAlongCallChain() throws CoreException {
		assertPassAlongCallChainReturnsNull("Foo",
				"class Foo { void foo() { String a = \"expression1\"; } void bar() { String b = \"expression2\"; } }");
	}

	@Test
	public void successfullyAppliedTransformationForPassAlongCallChain() throws CoreException {
		assertAnaphoraUpdateWorks(preliminaryAnaphora -> {
			final Expression oldRelatedExpression = (Expression) preliminaryAnaphora.getRelatedExpression()
					.getRelatedExpression();
			final Expression oldAnaphor = preliminaryAnaphora.getAnaphorExpression();
			final CompilationUnit oldScope = DomTraversal.getCompilationUnit(oldAnaphor);
			installAnaphoraPositionCategory(oldScope);
			final ASTTransformation<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> transformation = new PublicTransformations(
					MAX_DEPTH_OF_CALL_CHAIN_ANALYSIS_IN_TEST, typeRoot -> AstParsing.parse(typeRoot, true))
							.passAlongCallChain(oldRelatedExpression, oldAnaphor, oldScope);
			assertThat(transformation).isNotNull();
			return transformation.perform(preliminaryAnaphora);
		});
	}

	// TODO: Add a test in which local variable needs to be introduced first
	// instead of re-using the existing local variable that the upstream
	// expression initializes

	// TODO: Add a test in which the upstream expression is not an expression
	// but a statement, e.g. an existing local-variable declaration

	// TODO: Add another test that shows that the downstream expression does not
	// need to refer (DA1Re) to the newly introduced parameter

	/**
	 * TODO
	 *
	 * @Test public void successfullyAppliedTransformationForPassAlongCallChain() {
	 *       transformationSucceeds("Foo", "class Foo { void foo() { Foo f = new
	 *       Foo(); bar(); } void bar() { System.err.println(foo); } }" , "class Foo
	 *       { void foo() { Foo f = new Foo(); bar(f); } void bar(final Foo foo) {
	 *       System.err.println(foo); } }" ); }
	 */

	/**
	 * TODO
	 *
	 * @Test public void successfullyAppliedTransformationForPassAlongCallChain() {
	 *       transformationSucceeds("Foo", "class Foo { void foo() { new Foo();
	 *       bar(); } void bar() { System.err.println(foo); } }" , "class Foo { void
	 *       foo() { final Foo foo = new Foo(); bar(foo); } void bar(final Foo foo)
	 *       { System.err.println(foo); } }" ); }
	 */

	/**
	 * TODO: This test can only fail for call chains with more than 1 element
	 * because otherwise there can hardly be a name clash.
	 *
	 * @Test public void transformationForPassAlongCallChainFailsDueToNameClash() {
	 *       transformationFails("Foo", "class Foo { void foo() { Foo f = new Foo();
	 *       bar(); } void bar() { System.err.println(foo); } }" ); }
	 */

}
