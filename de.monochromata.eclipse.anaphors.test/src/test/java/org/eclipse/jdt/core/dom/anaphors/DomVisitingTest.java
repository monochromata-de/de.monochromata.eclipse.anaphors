package org.eclipse.jdt.core.dom.anaphors;

import static org.assertj.core.api.Assertions.assertThat;

import org.eclipse.jdt.core.dom.AST;
import org.junit.Test;

public class DomVisitingTest {

    private final AST ast = AST.newAST(AST.JLS_Latest, false);

    @Test
    public void collectSimpleNames_returnsGivenSimpleName() {
        final var simpleName = ast.newSimpleName("id");

        final var result = DomVisiting.collectSimpleNamesAndLiterals(simpleName);

        assertThat(result).containsExactly(simpleName);
    }

    @Test
    public void collectSimpleNames_returnsGivenStringLiteral() {
        final var stringLiteral = ast.newStringLiteral();

        final var result = DomVisiting.collectSimpleNamesAndLiterals(stringLiteral);

        assertThat(result).containsExactly(stringLiteral);
    }

    @Test
    public void collectSimpleNames_returnsNumberLiteral() {
        final var numberLiteral = ast.newNumberLiteral();

        final var result = DomVisiting.collectSimpleNamesAndLiterals(numberLiteral);

        assertThat(result).containsExactly(numberLiteral);
    }

    @Test
    public void collectSimpleNames_returnsBooleanLiteral() {
        final var booleanLiteral = ast.newBooleanLiteral(true);

        final var result = DomVisiting.collectSimpleNamesAndLiterals(booleanLiteral);

        assertThat(result).containsExactly(booleanLiteral);
    }

    @Test
    public void collectSimpleNames_returnsCharacterLiteral() {
        final var characterLiteral = ast.newCharacterLiteral();

        final var result = DomVisiting.collectSimpleNamesAndLiterals(characterLiteral);

        assertThat(result).containsExactly(characterLiteral);
    }

    @Test
    public void collectSimpleNames_returnsNullLiteral() {
        final var nullLiteral = ast.newNullLiteral();

        final var result = DomVisiting.collectSimpleNamesAndLiterals(nullLiteral);

        assertThat(result).containsExactly(nullLiteral);
    }

    @Test
    public void collectSimpleNames_returnsEmptyStream() {
        final var block = ast.newBlock();

        final var result = DomVisiting.collectSimpleNamesAndLiterals(block);

        assertThat(result).isEmpty();
    }

    @Test
    public void collectSimpleNames_returnsSimpleNamesOneLevelBelow() {
        final var simpleName = ast.newSimpleName("id");
        final var cast = ast.newCastExpression();
        cast.setExpression(simpleName);

        final var result = DomVisiting.collectSimpleNamesAndLiterals(cast);

        assertThat(result).containsExactly(simpleName);
    }

    @Test
    public void collectSimpleNames_returnsSimpleNamesFromLevelTwoAndThree() {
        final var typeName = ast.newSimpleName("TypeName");
        final var methodName = ast.newSimpleName("MethodName");

        final var methodInvocation = ast.newMethodInvocation();
        methodInvocation.setName(methodName);

        final var cic = ast.newClassInstanceCreation();
        cic.setType(ast.newSimpleType(typeName));
        cic.arguments().add(methodInvocation);

        final var result = DomVisiting.collectSimpleNamesAndLiterals(cic);

        assertThat(result).containsExactly(typeName, methodName);
    }

}
