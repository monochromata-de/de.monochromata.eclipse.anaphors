package org.eclipse.jdt.internal.corext.refactoring.change;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jdt.core.dom.anaphors.PublicChainElement;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.anaphors.ast.chain.Chaining;
import de.monochromata.eclipse.anaphors.SingleOriginalAnaphora;

public interface ChainMergingTesting {

	default List<PublicChainElement> mockChain(final String nameSuffix) {
		return singletonList(Chaining.toChain(mock(RelatedExpressionPart.class, "RE-" + nameSuffix),
				mock(AnaphorPart.class, "AN-" + nameSuffix), null, PublicChainElement::new));
	}

	default void assertStatusAndPotentialAnaphoras(
			final Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>> statusAndPotentialAnaphoras,
			final boolean isOk, final List<PublicChainElement> potentialAnaphoras) {
		assertThat(statusAndPotentialAnaphoras.getLeft().isOK()).isEqualTo(isOk);
		assertThat(statusAndPotentialAnaphoras.getRight()).isEqualTo(potentialAnaphoras);
	}

}
