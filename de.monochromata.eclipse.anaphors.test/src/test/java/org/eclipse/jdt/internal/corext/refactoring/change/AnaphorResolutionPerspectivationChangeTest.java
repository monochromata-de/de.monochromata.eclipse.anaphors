package org.eclipse.jdt.internal.corext.refactoring.change;

import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionPerspectivationEvent.AboutToStartDocumentRewritingForAnaphorResolution;
import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionPerspectivationEvent.CompletedDocumentRewritingForAnaphorResolution;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.jface.text.Position;
import org.junit.Test;

import de.monochromata.eclipse.anaphors.event.AnaphorResolutionPerspectivationEvent;

public class AnaphorResolutionPerspectivationChangeTest implements AnaphorResolutionPerspectivationChangeTesting {

    @Test
    public void isValidReturnsOk() throws Exception {
        final AnaphorResolutionPerspectivationChange change = new AnaphorResolutionPerspectivationChange(null, null,
                (unusedEvent, unusedPositions) -> {
                });
        assertThat(change.isValid(null).isOK()).isTrue();
    }

    @Test
    public void performSendsTheGivenEvent() throws Throwable {
        final AtomicReference<AnaphorResolutionPerspectivationEvent> listener = new AtomicReference<>();
        assertChangeResult(CompletedDocumentRewritingForAnaphorResolution, null,
                (event, positions) -> listener.set(event),
                unused -> assertThat(listener.get()).isSameAs(CompletedDocumentRewritingForAnaphorResolution));
    }

    @Test
    public void performSendsTheGivenPositions() throws Throwable {
        final List<? extends Position> positions = emptyList();
        final AtomicReference<List<? extends Position>> listener = new AtomicReference<>();
        assertChangeResult(CompletedDocumentRewritingForAnaphorResolution, positions,
                (event, receivedPositions) -> listener.set(receivedPositions),
                unused -> assertThat(listener.get()).isSameAs(positions));
    }

    @Test
    public void performReturnsOppositeUndoChangeForAboutToStart() throws Throwable {
        assertChangeResult(AboutToStartDocumentRewritingForAnaphorResolution, null,
                this::noop, undoChange -> assertThat(undoChange.getEventToFire())
                        .isSameAs(CompletedDocumentRewritingForAnaphorResolution));
    }

    @Test
    public void performReturnsOppositeUndoChangeForCompleted() throws Throwable {
        assertChangeResult(CompletedDocumentRewritingForAnaphorResolution, null,
                this::noop, undoChange -> assertThat(undoChange.getEventToFire())
                        .isSameAs(AboutToStartDocumentRewritingForAnaphorResolution));
    }

}
