package org.eclipse.jdt.internal.corext.refactoring.code;

import static org.assertj.core.api.Assertions.assertThat;

import org.eclipse.text.edits.DeleteEdit;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.ReplaceEdit;
import org.eclipse.text.edits.TextEdit;

public interface TextEditTesting {

    default void assertReplaceEdit(final MultiTextEdit parentEdit, final int childIndex, final int offset,
            final int length, final String text) {
        assertEdit(parentEdit, childIndex, ReplaceEdit.class, offset, length);
        assertThat(((ReplaceEdit) parentEdit.getChildren()[childIndex]).getText()).isEqualTo(text);
    }

    default void assertDeleteEdit(final MultiTextEdit parentEdit, final int childIndex, final int offset,
            final int length) {
        assertEdit(parentEdit, childIndex, DeleteEdit.class, offset, length);
    }

    default void assertEdit(final MultiTextEdit parentEdit, final int childIndex, final Class<? extends TextEdit> type,
            final int offset,
            final int length) {
        assertThat(parentEdit.getChildren()[childIndex]).isInstanceOf(type);
        assertThat(parentEdit.getChildren()[childIndex].getOffset()).isEqualTo(offset);
        assertThat(parentEdit.getChildren()[childIndex].getLength()).isEqualTo(length);
    }

}
