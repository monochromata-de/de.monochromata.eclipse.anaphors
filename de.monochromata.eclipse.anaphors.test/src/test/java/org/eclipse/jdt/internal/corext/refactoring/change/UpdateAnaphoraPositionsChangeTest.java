package org.eclipse.jdt.internal.corext.refactoring.change;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;

public class UpdateAnaphoraPositionsChangeTest extends AbstractAnaphoraPositionsTest {

	@Test
	public void perform_returnsUndoChangeWithSwappedPositions() throws Throwable {
		final List<AbstractAnaphoraPositionForRepresentation> positionsToAdd = new ArrayList<>();
		final List<AbstractAnaphoraPositionForRepresentation> positionsToRemove = new ArrayList<>();
		final UpdateAnaphoraPositionsChange change = new UpdateAnaphoraPositionsChange(document, positionsToAdd,
				emptyList(), positionsToRemove, emptyList());

		assertChangeResult(change, (final UpdateAnaphoraPositionsChange undoChange) -> {
			assertThat(undoChange.positionsToAdd).isSameAs(positionsToRemove);
			assertThat(undoChange.positionsToRemove).isSameAs(positionsToAdd);
		});
	}

	@Test
	public void perform_returnsUndoChangeWithSwappedListsForLinking() throws Throwable {
		final List<Pair<AbstractAnaphoraPositionForRepresentation, AbstractAnaphoraPositionForRepresentation>> positionsToLink = new ArrayList<>();
		final List<Pair<AbstractAnaphoraPositionForRepresentation, AbstractAnaphoraPositionForRepresentation>> positionsToUnlink = new ArrayList<>();
		final UpdateAnaphoraPositionsChange change = new UpdateAnaphoraPositionsChange(document, emptyList(),
				positionsToLink, emptyList(), positionsToUnlink);
		assertChangeResult(change, (final UpdateAnaphoraPositionsChange undoChange) -> {
			assertThat(undoChange.positionsToLink).isSameAs(positionsToUnlink);
			assertThat(undoChange.positionsToUnlink).isSameAs(positionsToLink);
		});
	}

}
