package org.eclipse.jdt.internal.corext.refactoring.code;

import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.jdt.internal.corext.refactoring.code.AnaphoraPositionCreationForRefactoring.loadOrCreatePosition;

import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Test;

import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;

public class AnaphoraPositionCreationForRefactoringTest implements AnaphoraPositionCreationForRefactoringTesting {

	@Test
	public void collectForAddition_noExistingRelatedExpression() {
		final Stream.Builder<AbstractAnaphoraPositionForRepresentation> positionsToAdd = Stream.builder();

		loadOrCreatePosition(relatedExpressionElement, unused -> null, unused -> positionForRelatedExpression,
				positionsToAdd);

		assertThat(positionsToAdd.build()).containsExactly(positionForRelatedExpression);
	}

	@Test
	public void doNotCollectForAddition_existingRelatedExpression() {
		final Stream.Builder<AbstractAnaphoraPositionForRepresentation> positionsToAdd = Stream.builder();

		loadOrCreatePosition(relatedExpressionElement, unused -> positionForRelatedExpression, unused -> {
			throw new UnsupportedOperationException();
		}, positionsToAdd);

		assertThat(positionsToAdd.build()).isEmpty();
	}

	@Test
	public void collectForAddition_noExistingAnaphor() {
		final Stream.Builder<AbstractAnaphoraPositionForRepresentation> positionsToAdd = Stream.builder();

		loadOrCreatePosition(anaphorElement, unused -> null, unused -> positionForAnaphor, positionsToAdd);

		assertThat(positionsToAdd.build()).containsExactly(positionForAnaphor);
	}

	@Test
	public void doNotCollectForAddition_existingAnaphor() {
		final Stream.Builder<AbstractAnaphoraPositionForRepresentation> positionsToAdd = Stream.builder();

		loadOrCreatePosition(anaphorElement, unused -> positionForAnaphor, unused -> {
			throw new UnsupportedOperationException();
		}, positionsToAdd);

		assertThat(positionsToAdd.build()).isEmpty();
	}

	@Test
	public void collectForLinking_noExistingRelatedExpression_noExistingAnaphor() {
		final var positionsToLink = collectForLinking(null, null);

		assertThat(positionsToLink)
				.containsExactly(new ImmutablePair<>(positionForRelatedExpression, positionForAnaphor));
	}

	/**
	 * This can happen when the related expression is re-resolved.
	 */
	@Test
	public void collectForLinking_noExistingRelatedExpression_existingAnaphor() {
		final var positionsToLink = collectForLinking(null, positionForAnaphor);

		assertThat(positionsToLink)
				.containsExactly(new ImmutablePair<>(positionForRelatedExpression, positionForAnaphor));
	}

	@Test
	public void collectForLinking_existingRelatedExpression_noExistingAnaphor() {
		final var positionsToLink = collectForLinking(positionForRelatedExpression, null);

		assertThat(positionsToLink)
				.containsExactly(new ImmutablePair<>(positionForRelatedExpression, positionForAnaphor));
	}

	@Test
	public void doNotcollectForLinking_existingRelatedExpression_existingAnaphor() {
		final var positionsToLink = collectForLinking(positionForRelatedExpression, positionForAnaphor);

		assertThat(positionsToLink).isEmpty();
	}

}
