package org.eclipse.jdt.internal.corext.refactoring.change;

import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static java.util.Collections.emptyList;

import java.util.Collections;

import org.junit.Test;

import de.monochromata.eclipse.anaphors.NoopTesting;

public class UpdatePositionsChangeTest extends AbstractAnaphoraPositionsTest
		implements UpdatePositionsChangeTesting, NoopTesting {

	@Test
	public void isValid_returnsOk() throws Exception {
		final var change = new UpdatePositionsChange(document, "", "", emptyList(), emptyList());
		assertStatusOk(change.isValid(null));
	}

	@Test
	public void perform_removesPositionsToRemove() throws Throwable {
		final var positions = createAndAddPositions(document, anaphora);
		final var change = new UpdatePositionsChange(document, "positionDescription", ANAPHORA_CATEGORY, emptyList(),
				positions);
		assertChangeResult(change, () -> assertAnaphoraPositions(document, Collections.emptyList()));
	}

	@Test
	public void perform_addsPositionsToAdd() throws Throwable {
		final var positions = createDefaultAnaphoraPositions(anaphora);
		final var change = new UpdatePositionsChange(document, "positionDescription", ANAPHORA_CATEGORY, positions,
				emptyList());
		assertChangeResult(change, () -> assertPositions(document, positions));
	}

	@Test
	public void undoChange_addsPositionsToRemove() throws Throwable {
		final var positions = createAndAddPositions(document, anaphora);
		final var change = new UpdatePositionsChange(document, "positionDescription", ANAPHORA_CATEGORY, emptyList(),
				positions);

		assertChangeResult(change, (undoChange) -> {
			assertStatusOk(undoChange.isValid(null));
			undoChange.perform(null);
			assertPositions(document, positions);
		});
	}

	@Test
	public void undoChange_removesPositionsToAdd() throws Throwable {
		final var positions = createDefaultAnaphoraPositions(anaphora);
		final var change = new UpdatePositionsChange(document, "positionDescription", ANAPHORA_CATEGORY, positions,
				emptyList());
		assertChangeResult(change, (undoChange) -> {
			assertStatusOk(undoChange.isValid(null));
			assertAnaphoraPositions(document, positions);

			undoChange.perform(null);

			assertAnaphoraPositions(document, emptyList());
		});
	}

}
