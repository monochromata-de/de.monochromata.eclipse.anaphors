package org.eclipse.jdt.internal.corext.refactoring.change;

import java.util.List;
import java.util.function.BiConsumer;

import de.monochromata.eclipse.anaphors.NoopTesting;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionProgressEvent;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.position.PositionExceptionWrapping.BadPositionCategoryThrowingConsumer;

public interface AnaphorResolutionProgressChangeTesting extends ChangeTesting, NoopTesting {

	default <T extends Throwable> void assertChangeResult(final AnaphorResolutionProgressEvent eventToFire,
			final List<AbstractAnaphoraPositionForRepresentation> positions,
			final BiConsumer<AnaphorResolutionProgressEvent, List<? extends AbstractAnaphoraPositionForRepresentation>> anaphorResolutionProgessNotifier,
			final BadPositionCategoryThrowingConsumer<AnaphorResolutionProgressChange, T> resultAsserter)
			throws T, Throwable {
		final AnaphorResolutionProgressChange change = new AnaphorResolutionProgressChange(eventToFire, positions,
				anaphorResolutionProgessNotifier);
		assertChangeResult(change, undoChange -> resultAsserter.accept((AnaphorResolutionProgressChange) undoChange));
	}

}
