package org.eclipse.jdt.internal.corext.refactoring.change;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.Optional.empty;
import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.jdt.internal.corext.refactoring.code.ChainMerging.mergeStatusAndPotentialChains;
import static org.eclipse.ltk.core.refactoring.RefactoringStatus.createErrorStatus;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jdt.core.dom.anaphors.PublicChainElement;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.junit.Test;

import de.monochromata.eclipse.anaphors.SingleOriginalAnaphora;

public class ChainMergingTest implements ChainMergingTesting {

	private final List<PublicChainElement> potentialAnaphoras1 = mockChain("1");
	private final List<PublicChainElement> potentialAnaphoras2 = mockChain("2");
	private final List<PublicChainElement> potentialAnaphoras3 = mockChain("3");

	@Test
	public void doNotMergeIfFirstListContainsSingleFailedChain() {
		final List<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> result = mergeStatusAndPotentialChains(
				singletonList(new ImmutableTriple<>(createErrorStatus("Error"), empty(), potentialAnaphoras1)),
				singletonList(new ImmutableTriple<>(new RefactoringStatus(), empty(), potentialAnaphoras2)));

		assertThat(result).hasSize(2);
		assertStatusAndPotentialAnaphoras(result.get(0), true, potentialAnaphoras2);
		assertStatusAndPotentialAnaphoras(result.get(1), false, potentialAnaphoras1);
	}

	@Test
	public void doNotMergeIfSecondListContainsSingleFailedChain() {
		final List<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> result = mergeStatusAndPotentialChains(
				singletonList(new ImmutableTriple<>(new RefactoringStatus(), empty(), potentialAnaphoras1)),
				singletonList(new ImmutableTriple<>(createErrorStatus("Error"), empty(), potentialAnaphoras2)));

		assertThat(result).hasSize(2);
		assertStatusAndPotentialAnaphoras(result.get(0), true, potentialAnaphoras1);
		assertStatusAndPotentialAnaphoras(result.get(1), false, potentialAnaphoras2);
	}

	@Test
	public void doNotMergeIfBothChainsContainSingleFailedChain() {
		final List<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> result = mergeStatusAndPotentialChains(
				singletonList(new ImmutableTriple<>(createErrorStatus("Error"), empty(), potentialAnaphoras1)),
				singletonList(new ImmutableTriple<>(createErrorStatus("Error"), empty(), potentialAnaphoras2)));

		assertThat(result).hasSize(2);
		assertStatusAndPotentialAnaphoras(result.get(0), false, potentialAnaphoras1);
		assertStatusAndPotentialAnaphoras(result.get(1), false, potentialAnaphoras2);
	}

	@Test
	public void mergeIfBothChainsDoNotContainASingleFailedChain() {
		final List<PublicChainElement> mergedChains = mockChain("result");

		final List<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> result = mergeStatusAndPotentialChains(
				singletonList(new ImmutableTriple<>(new RefactoringStatus(), empty(), potentialAnaphoras1)),
				singletonList(new ImmutableTriple<>(new RefactoringStatus(), empty(), potentialAnaphoras2)),
				(chains1, chains2) -> mergedChains);

		assertThat(result).hasSize(1);
		assertStatusAndPotentialAnaphoras(result.get(0), true, mergedChains);
	}

	@Test
	public void mergedChainsAreRetainedWhenFurtherErroneousChainAppears() {
		final List<PublicChainElement> mergedChains = mockChain("result");

		final List<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> result = mergeStatusAndPotentialChains(
				singletonList(new ImmutableTriple<>(createErrorStatus("Error"), empty(), potentialAnaphoras1)),
				asList(new ImmutableTriple<>(new RefactoringStatus(), empty(), potentialAnaphoras2),
						new ImmutableTriple<>(new RefactoringStatus(), empty(), potentialAnaphoras3)),
				(chains1, chains2) -> mergedChains);

		assertThat(result).hasSize(2);
		assertStatusAndPotentialAnaphoras(result.get(0), true, mergedChains);
		assertStatusAndPotentialAnaphoras(result.get(1), false, potentialAnaphoras1);
	}

	@Test
	public void mergeIfThereIsAnErroneousChainConcatenatedWithAnOkOne() {
		final List<PublicChainElement> mergedChains = mockChain("result");

		final List<Triple<RefactoringStatus, Optional<SingleOriginalAnaphora>, List<PublicChainElement>>> result = mergeStatusAndPotentialChains(
				singletonList(new ImmutableTriple<>(new RefactoringStatus(), empty(), potentialAnaphoras1)),
				asList(new ImmutableTriple<>(createErrorStatus("Error"), empty(), potentialAnaphoras2),
						new ImmutableTriple<>(new RefactoringStatus(), empty(), potentialAnaphoras3)),
				(chains1, chains2) -> mergedChains);

		assertThat(result).hasSize(2);
		assertStatusAndPotentialAnaphoras(result.get(0), true, mergedChains);
		assertStatusAndPotentialAnaphoras(result.get(1), false, potentialAnaphoras2);
	}

}
