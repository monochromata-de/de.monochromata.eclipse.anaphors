package org.eclipse.jdt.internal.corext.refactoring.change;

import static de.monochromata.eclipse.anaphors.editor.update.AstBasedAnaphoraUpdating.getDocumentForPositionsTracking;
import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static org.eclipse.core.filebuffers.LocationKind.IFILE;

import java.util.function.Function;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.core.filebuffers.FileBuffers;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.anaphors.ASTTesting;
import org.eclipse.jface.text.IDocument;

import de.monochromata.eclipse.anaphors.position.AnaphoraPositionUpdater;
import de.monochromata.eclipse.persp.PerspectivationDocument;
import de.monochromata.eclipse.persp.PerspectivationDocumentManager;
import de.monochromata.function.ThrowingBiConsumer;
import de.monochromata.function.ThrowingConsumer;

public interface DocumentTesting extends ASTTesting {

	String INITIAL_SOURCE = /* offsets 0- 9 */ "public cla" +
	/* offsets 10-19 */ "ss Foo { p" +
	/* offsets 20-29 */ "ublic stat" +
	/* offsets 30-39 */ "ic void fo" +
	/* offsets 40-49 */ "o() { new " +
	/* offsets 50-59 */ "Integer(1)" +
	/* offsets 60-69 */ "; System.e" +
	/* offsets 70-79 */ "rr.println" +
	/* offsets 80-89 */ "(integer);" +
	/* offsets 90-93 */ " } }";

	default Function<Void, Function<Pair<IJavaProject, IPackageFragment>, CompilationUnit>> getDefaultCompilationUnit() {
		return getCompilationUnit("Foo", INITIAL_SOURCE);
	}

	/**
	 * @deprecated Use {@link #getCompilationUnit(String, String)} instead.
	 */
	@Deprecated
	default void assertDocument(final String source,
			final ThrowingBiConsumer<IDocument, CompilationUnit, CoreException> asserter) throws CoreException {
		assertDocument("Foo", source, asserter);
	}

	default ThrowingConsumer<ThrowingBiConsumer<IDocument, CompilationUnit, CoreException>, CoreException> assertDocument(
			final String typeName, final String source) {
		return asserter -> assertDocument(typeName, source, asserter);
	}

	default Function<Function<Pair<IJavaProject, IPackageFragment>, CompilationUnit>, Void> assertDocument(
			final ThrowingBiConsumer<IDocument, CompilationUnit, CoreException> asserter) throws CoreException {
		return assertProjectAndCompilationUnit((compilationUnit, unused) -> {
			final var pm = new NullProgressMonitor();
			final var compilationUnitName = compilationUnit.getJavaElement().getElementName();
			final var documentPath = new Path("/emptyProject/src/" + compilationUnitName);
			try {
				FileBuffers.getTextFileBufferManager().connect(documentPath, IFILE, pm);
				final IDocument originDocument = getDocumentForPositionsTracking(compilationUnit);
				// TODO: Try to remove the image document because it is not used
				final PerspectivationDocument imageDocument = new PerspectivationDocumentManager()
						.createSlaveDocument(originDocument);
				originDocument.addPositionCategory(ANAPHORA_CATEGORY);
				originDocument.addPositionUpdater(new AnaphoraPositionUpdater(anaphoras -> {
				}));

				asserter.accept(originDocument, compilationUnit);
			} catch (final CoreException e) {
				throw new RuntimeException(e.getMessage(), e);
			} finally {
				try {
					FileBuffers.getTextFileBufferManager().disconnect(documentPath, IFILE, pm);
				} catch (final CoreException e) {
					throw new RuntimeException(e.getMessage(), e);
				}
			}
		});
	}

	/**
	 * @deprecated Use {@link #assertDocument(ThrowingBiConsumer)} instead.
	 */
	@Deprecated
	default void assertDocument(final String typeName, final String source,
			final ThrowingBiConsumer<IDocument, CompilationUnit, CoreException> asserter) throws CoreException {
		assertProjectAndCompilationUnit(typeName, source, (compilationUnit, project) -> {
			final NullProgressMonitor pm = new NullProgressMonitor();
			final Path documentPath = new Path("/emptyProject/src/" + typeName + ".java");
			try {
				FileBuffers.getTextFileBufferManager().connect(documentPath, IFILE, pm);
				final IDocument originDocument = getDocumentForPositionsTracking(compilationUnit);
				final PerspectivationDocument imageDocument = new PerspectivationDocumentManager()
						.createSlaveDocument(originDocument);
				originDocument.addPositionCategory(ANAPHORA_CATEGORY);
				originDocument.addPositionUpdater(new AnaphoraPositionUpdater(anaphoras -> {
				}));

				asserter.accept(originDocument, compilationUnit);
			} catch (final CoreException e) {
				throw new RuntimeException(e.getMessage(), e);
			} finally {
				try {
					FileBuffers.getTextFileBufferManager().disconnect(documentPath, IFILE, pm);
				} catch (final CoreException e) {
					throw new RuntimeException(e.getMessage(), e);
				}
			}
		});
	}

}
