package org.eclipse.jdt.internal.corext.refactoring.change;

import static de.monochromata.eclipse.persp.HidePosition.HIDE_CATEGORY;
import static de.monochromata.eclipse.persp.PerspectivationPosition.PERSPECTIVATION_CATEGORY;
import static de.monochromata.eclipse.persp.ToLowerCasePosition.TO_LOWER_CASE_CATEGORY;

import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;

public interface PerspectivationPositionsChangeTesting extends ChangeTesting {

    default IDocument createDocument() {
        final IDocument document = new Document();
        document.set("Foo bar");
        document.addPositionCategory(PERSPECTIVATION_CATEGORY);
        document.addPositionCategory(TO_LOWER_CASE_CATEGORY);
        document.addPositionCategory(HIDE_CATEGORY);
        return document;
    }

}
