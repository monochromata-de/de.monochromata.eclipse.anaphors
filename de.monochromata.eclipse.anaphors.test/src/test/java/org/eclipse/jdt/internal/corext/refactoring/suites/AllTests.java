package org.eclipse.jdt.internal.corext.refactoring.suites;

import org.eclipse.jdt.internal.corext.refactoring.change.AnaphorResolutionEventChangeTest;
import org.eclipse.jdt.internal.corext.refactoring.change.AnaphorResolutionPerspectivationChangeTest;
import org.eclipse.jdt.internal.corext.refactoring.change.AnaphorResolutionProgressChangeTest;
import org.eclipse.jdt.internal.corext.refactoring.change.AnnotationUpdateChangeTest;
import org.eclipse.jdt.internal.corext.refactoring.change.ChainMergingTest;
import org.eclipse.jdt.internal.corext.refactoring.change.ConfigureAnaphoraReresolutionChangeTest;
import org.eclipse.jdt.internal.corext.refactoring.change.ConfigureDeletionOfEmptyGrowingPositionChangeTest;
import org.eclipse.jdt.internal.corext.refactoring.change.UpdateAnaphoraPositionsChangeTest;
import org.eclipse.jdt.internal.corext.refactoring.change.UpdateDoublyLinkedPositionsChangeTest;
import org.eclipse.jdt.internal.corext.refactoring.change.UpdatePerspectivationPositionsChangeTest;
import org.eclipse.jdt.internal.corext.refactoring.change.UpdatePositionsChangeTest;
import org.eclipse.jdt.internal.corext.refactoring.code.AnaphoraPositionCreationForRefactoringTest;
import org.eclipse.jdt.internal.corext.refactoring.code.DeRealizeAnaphorsTest;
import org.eclipse.jdt.internal.corext.refactoring.code.ResolveAnaphorsRefactoringTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AnaphoraPositionCreationForRefactoringTest.class, AnaphorResolutionPerspectivationChangeTest.class,
		AnaphorResolutionEventChangeTest.class, AnaphorResolutionProgressChangeTest.class,
		AnnotationUpdateChangeTest.class, ChainMergingTest.class,
		ConfigureDeletionOfEmptyGrowingPositionChangeTest.class, ConfigureAnaphoraReresolutionChangeTest.class,
		DeRealizeAnaphorsTest.class, ResolveAnaphorsRefactoringTest.class, UpdateAnaphoraPositionsChangeTest.class,
		UpdateDoublyLinkedPositionsChangeTest.class, UpdatePerspectivationPositionsChangeTest.class,
		UpdatePositionsChangeTest.class })
public class AllTests {
}
