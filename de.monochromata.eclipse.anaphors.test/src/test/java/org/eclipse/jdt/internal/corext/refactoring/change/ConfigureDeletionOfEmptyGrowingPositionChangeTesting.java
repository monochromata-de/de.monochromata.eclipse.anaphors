package org.eclipse.jdt.internal.corext.refactoring.change;

import org.eclipse.jface.text.Document;

import de.monochromata.eclipse.position.GrowingPositionUpdater;

public interface ConfigureDeletionOfEmptyGrowingPositionChangeTesting extends ChangeTesting {

    default void addCategoryAndUpdater(final String category, final GrowingPositionUpdater updater,
            final Document document) {
        document.addPositionCategory(category);
        document.addPositionUpdater(updater);
    }

}
