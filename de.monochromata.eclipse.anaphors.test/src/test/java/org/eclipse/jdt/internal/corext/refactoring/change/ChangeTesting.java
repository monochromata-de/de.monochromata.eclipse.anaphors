package org.eclipse.jdt.internal.corext.refactoring.change;

import static de.monochromata.eclipse.anaphors.editor.update.AstBasedAnaphoraUpdating.getDocumentForPositionsTracking;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.function.BiConsumer;
import java.util.function.Function;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.internal.corext.refactoring.code.RefactoringTesting;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.PerformChangeOperation;

import de.monochromata.eclipse.anaphors.AnaphoraTesting;
import de.monochromata.eclipse.position.PositionExceptionWrapping.BadPositionCategoryThrowingConsumer;
import de.monochromata.eclipse.position.PositionExceptionWrapping.BadPositionCategoryThrowingRunnable;
import de.monochromata.function.ThrowingBiConsumer;
import de.monochromata.function.ThrowingBiFunction;

public interface ChangeTesting extends AnaphoraTesting, RefactoringTesting, DocumentTesting {

	default <C extends Change, T extends Throwable> void assertChangeResult(final C change,
			final BadPositionCategoryThrowingRunnable<T> resultAsserter) throws T, Throwable {
		assertChangeResult(change, unusedUndoChange -> resultAsserter.run());
	}

	default <C extends Change, T extends Throwable> void assertChangeResult(final C change,
			final BadPositionCategoryThrowingConsumer<C, T> resultAsserter) throws T, Throwable {
		withNopProviders(spis -> {
			final C undoChange = performChange(change);
			resultAsserter.accept(undoChange);
			return null;
		});
	}

	default Function<Function<Pair<IJavaProject, IPackageFragment>, CompilationUnit>, Void> assertChangeResult(
			final ThrowingBiFunction<IDocument, CompilationUnit, Change, CoreException> changeFactory,
			final BiConsumer<IDocument, PerformChangeOperation> resultsAsserter) throws Exception {
		return assertChange(changeFactory, (compilationUnit, change) -> {
			final NullProgressMonitor pm = new NullProgressMonitor();
			try {
				final IDocument document = getDocumentForPositionsTracking(compilationUnit);
				assertThat(document.get()).isEqualTo(INITIAL_SOURCE);

				final PerformChangeOperation operation = new PerformChangeOperation(change);
				withNopProviders(spis -> {
					operation.run(pm);
					return null;
				});
				assertThat(operation.changeExecuted()).isTrue();

				resultsAsserter.accept(document, operation);
			} catch (final CoreException e) {
				throw new RuntimeException(e.getMessage(), e);
			}
		});
	}

	default Function<Function<Pair<IJavaProject, IPackageFragment>, CompilationUnit>, Void> assertChange(
			final ThrowingBiFunction<IDocument, CompilationUnit, Change, CoreException> changeFactory,
			final ThrowingBiConsumer<CompilationUnit, Change, CoreException> asserter) throws Exception {
		return assertDocument((originDocument, compilationUnit) -> {
			final Change change = changeFactory.apply(originDocument, compilationUnit);
			asserter.accept(compilationUnit, change);
		});
	}

	default <C extends Change> C performChange(final C change) throws CoreException {
		change.initializeValidationData(null);
		assertThat(change.isValid(null).isOK()).isTrue();
		return (C) change.perform(null);
	}

}
