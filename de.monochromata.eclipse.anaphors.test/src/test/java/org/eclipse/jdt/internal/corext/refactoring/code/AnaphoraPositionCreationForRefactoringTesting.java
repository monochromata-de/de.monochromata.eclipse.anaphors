package org.eclipse.jdt.internal.corext.refactoring.code;

import static org.eclipse.jdt.internal.corext.refactoring.code.AnaphoraPositionCreationForRefactoring.loadOrCreatePositions;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.internal.corext.refactoring.change.AnaphoraPositionsTesting;

import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;
import de.monochromata.eclipse.persp.PositionCreationTesting;
import de.monochromata.eclipse.position.DoublyLinkedPositionTesting;

public interface AnaphoraPositionCreationForRefactoringTesting
		extends PositionCreationTesting, AnaphoraPositionsTesting, DoublyLinkedPositionTesting {

	default List<Pair<AbstractAnaphoraPositionForRepresentation, AbstractAnaphoraPositionForRepresentation>> collectForLinking(
			final PositionForRelatedExpression existingRelatedExpressionPosition,
			final PositionForAnaphor existingAnaphorPosition) {
		final List<Pair<AbstractAnaphoraPositionForRepresentation, AbstractAnaphoraPositionForRepresentation>> positionsToLink = new ArrayList<>();
		loadOrCreatePositions(relatedExpressionElement, Stream.builder(), positionsToLink,
				unused -> existingRelatedExpressionPosition, unused -> positionForRelatedExpression,
				unused -> existingAnaphorPosition, unused -> positionForAnaphor);
		return positionsToLink;
	}

}
