package org.eclipse.jdt.internal.corext.refactoring.change;

import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.ltk.core.refactoring.RefactoringStatus.ERROR;

import org.eclipse.jface.text.DefaultPositionUpdater;
import org.eclipse.jface.text.Document;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.junit.Test;

import de.monochromata.eclipse.position.GrowingPositionUpdater;

public class ConfigureDeletionOfEmptyGrowingPositionChangeTest
        implements ConfigureDeletionOfEmptyGrowingPositionChangeTesting {

    private static final String TEST_POSITION_CATEGORY = "testPositionCategory";

    @Test
    public void isValidReturnsErrorIfSoughtPositionCategoryIsNotFoundInDocument() throws Exception {
        final Document document = new Document();
        final ConfigureDeletionOfEmptyGrowingPositionsChange change = new ConfigureDeletionOfEmptyGrowingPositionsChange(
                document, TEST_POSITION_CATEGORY, false);

        final RefactoringStatus status = change.isValid(null);

        assertThat(status.getSeverity()).isEqualTo(ERROR);
        assertThat(status.getMessageMatchingSeverity(ERROR))
                .isEqualTo("The document does not contain the position category " + TEST_POSITION_CATEGORY);
    }

    @Test
    public void isValidReturnsErrorIfThereIsNoGrowingPositionUpdater() throws Exception {
        final Document document = new Document();
        document.addPositionCategory(TEST_POSITION_CATEGORY);
        document.addPositionUpdater(new DefaultPositionUpdater(TEST_POSITION_CATEGORY));
        final ConfigureDeletionOfEmptyGrowingPositionsChange change = new ConfigureDeletionOfEmptyGrowingPositionsChange(
                document, TEST_POSITION_CATEGORY, false);

        final RefactoringStatus status = change.isValid(null);

        assertThat(status.getSeverity()).isEqualTo(ERROR);
        assertThat(status.getMessageMatchingSeverity(ERROR))
                .isEqualTo(
                        "The document contains no " + GrowingPositionUpdater.class.getName() + " for position category "
                                + TEST_POSITION_CATEGORY);
    }

    @Test
    public void isValidReturnsErrorIfNoUpdaterIsNotFoundInDocumentForTheSoughtCategory() throws Exception {
        final Document document = new Document();
        document.addPositionCategory(TEST_POSITION_CATEGORY);
        final ConfigureDeletionOfEmptyGrowingPositionsChange change = new ConfigureDeletionOfEmptyGrowingPositionsChange(
                document, TEST_POSITION_CATEGORY, false);

        final RefactoringStatus status = change.isValid(null);

        assertThat(status.getSeverity()).isEqualTo(ERROR);
        assertThat(status.getMessageMatchingSeverity(ERROR))
                .isEqualTo(
                        "The document contains no " + GrowingPositionUpdater.class.getName() + " for position category "
                                + TEST_POSITION_CATEGORY);
    }

    @Test
    public void isValidReturnsOkForValidSetup() throws Exception {
        final Document document = new Document();
        document.addPositionCategory(TEST_POSITION_CATEGORY);
        document.addPositionUpdater(new GrowingPositionUpdater(TEST_POSITION_CATEGORY));
        final ConfigureDeletionOfEmptyGrowingPositionsChange change = new ConfigureDeletionOfEmptyGrowingPositionsChange(
                document, TEST_POSITION_CATEGORY, false);

        final RefactoringStatus status = change.isValid(null);

        assertThat(status.isOK()).isTrue();
    }

    @Test
    public void getNameReturnsCorrectResult() {
        final ConfigureDeletionOfEmptyGrowingPositionsChange change = new ConfigureDeletionOfEmptyGrowingPositionsChange(
                null, null, false);

        assertThat(change.getName()).isEqualTo("Configure deletion of empty growing positions");
    }

    @Test
    public void getModifiedElementReturnsNull() {
        final ConfigureDeletionOfEmptyGrowingPositionsChange change = new ConfigureDeletionOfEmptyGrowingPositionsChange(
                null, null, false);

        assertThat(change.getModifiedElement()).isNull();
    }

    @Test
    public void performConfiguresDeletionOfEmptyPositions() throws Exception {
        final GrowingPositionUpdater updaterToConfigure = new GrowingPositionUpdater(TEST_POSITION_CATEGORY);

        final Document document = new Document();
        addCategoryAndUpdater(TEST_POSITION_CATEGORY, updaterToConfigure, document);

        final ConfigureDeletionOfEmptyGrowingPositionsChange change = new ConfigureDeletionOfEmptyGrowingPositionsChange(
                document, TEST_POSITION_CATEGORY, false);

        assertThat(updaterToConfigure.isDeleteEmptyPositions()).isTrue();

        performChange(change);

        assertThat(updaterToConfigure.isDeleteEmptyPositions()).isFalse();
    }

    @Test
    public void performOnlyConfiguresUpdaterForNamedCategory() throws Exception {
        final GrowingPositionUpdater updaterToConfigure = new GrowingPositionUpdater(TEST_POSITION_CATEGORY);
        final GrowingPositionUpdater otherUpdater = new GrowingPositionUpdater("other category");

        final Document document = new Document();
        addCategoryAndUpdater(TEST_POSITION_CATEGORY, updaterToConfigure, document);
        addCategoryAndUpdater("other category", otherUpdater, document);

        final ConfigureDeletionOfEmptyGrowingPositionsChange change = new ConfigureDeletionOfEmptyGrowingPositionsChange(
                document, TEST_POSITION_CATEGORY, false);

        assertThat(otherUpdater.isDeleteEmptyPositions()).isTrue();

        performChange(change);

        assertThat(otherUpdater.isDeleteEmptyPositions()).isTrue();
    }

    @Test
    public void performReturnsUndoChange() throws Exception {
        final GrowingPositionUpdater updaterToConfigure = new GrowingPositionUpdater(TEST_POSITION_CATEGORY);

        final Document document = new Document();
        addCategoryAndUpdater(TEST_POSITION_CATEGORY, updaterToConfigure, document);

        final ConfigureDeletionOfEmptyGrowingPositionsChange change = new ConfigureDeletionOfEmptyGrowingPositionsChange(
                document, TEST_POSITION_CATEGORY, false);

        assertThat(updaterToConfigure.isDeleteEmptyPositions()).isTrue();

        final Change undoChange = performChange(change);

        assertThat(undoChange)
                .isInstanceOf(ConfigureDeletionOfEmptyGrowingPositionsChange.class);
        assertThat(((ConfigureDeletionOfEmptyGrowingPositionsChange) undoChange).isEnableDeletion())
                .isEqualTo(true);
    }
}
