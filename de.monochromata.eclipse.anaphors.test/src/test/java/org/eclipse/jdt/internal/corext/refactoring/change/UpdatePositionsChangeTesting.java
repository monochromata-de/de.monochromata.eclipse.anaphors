package org.eclipse.jdt.internal.corext.refactoring.change;

import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapBadLocationException;

import java.util.List;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.anaphors.position.AnaphoraPositionCreation;
import de.monochromata.eclipse.position.DoublyLinkedPositionTesting;
import de.monochromata.eclipse.position.PositionExceptionWrapping;

public interface UpdatePositionsChangeTesting
		extends ChangeTesting, AnaphoraPositionsTesting, DoublyLinkedPositionTesting {

	default List<AbstractAnaphoraPositionForRepresentation> createAndAddPositions(final IDocument document,
			final Anaphora anaphora) {
		final List<AbstractAnaphoraPositionForRepresentation> positions = createDefaultAnaphoraPositions(anaphora);
		addPositions(document, positions);
		return positions;
	}

	default void addPositions(final IDocument document,
			final List<AbstractAnaphoraPositionForRepresentation> positions) {
		positions.forEach(position -> addPosition(document, position));
	}

	default void addPosition(final IDocument document, final Position annotationPosition) {
		wrapBadLocationException(document, unused -> PositionExceptionWrapping.wrapBadPositionCategoryException(
				() -> AnaphoraPositionCreation.addPosition(document, (AbstractAnaphoraPosition) annotationPosition)));
	}

}
