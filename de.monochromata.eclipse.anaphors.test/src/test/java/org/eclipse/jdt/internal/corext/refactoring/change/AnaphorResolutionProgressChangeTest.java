package org.eclipse.jdt.internal.corext.refactoring.change;

import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionProgressEvent.AboutToUndoAnaphoraAddedOrChanged;
import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionProgressEvent.AnaphoraAddedOrChanged;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import org.junit.Test;

import de.monochromata.eclipse.anaphors.event.AnaphorResolutionProgressEvent;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;

public class AnaphorResolutionProgressChangeTest implements AnaphorResolutionProgressChangeTesting {

	@Test
	public void isValidReturnsOk() throws Exception {
		final AnaphorResolutionProgressChange change = new AnaphorResolutionProgressChange(null, null, null);
		assertThat(change.isValid(null).isOK()).isTrue();
	}

	@Test
	public void performSendsTheGivenEvent() throws Throwable {
		final AtomicReference<AnaphorResolutionProgressEvent> listener = new AtomicReference<>();
		assertChangeResult(AnaphoraAddedOrChanged, null, (event, positions) -> listener.set(event),
				unused -> assertThat(listener.get()).isSameAs(AnaphoraAddedOrChanged));
	}

	@Test
	public void performSendsTheGivenPositions() throws Throwable {
		final List<AbstractAnaphoraPositionForRepresentation> positions = emptyList();
		final AtomicReference<List<? extends AbstractAnaphoraPositionForRepresentation>> listener = new AtomicReference<>();
		assertChangeResult(AnaphoraAddedOrChanged, positions,
				(unused, receivedPositions) -> listener.set(receivedPositions),
				unused -> assertThat(listener.get()).isSameAs(positions));
	}

	@Test
	public void performReturnsOppositeUndoChangeForAnaphoraAddedOrChanged() throws Throwable {
		assertChangeResult(AnaphoraAddedOrChanged, null, this::noop,
				undoChange -> assertThat(undoChange.getEventToFire()).isSameAs(AboutToUndoAnaphoraAddedOrChanged));
	}

	@Test
	public void performReturnsOppositeUndoChangeForAboutToUndoAnaphoraAddedOrChanged() throws Throwable {
		assertChangeResult(AboutToUndoAnaphoraAddedOrChanged, null, this::noop,
				undoChange -> assertThat(undoChange.getEventToFire()).isSameAs(AnaphoraAddedOrChanged));
	}

}
