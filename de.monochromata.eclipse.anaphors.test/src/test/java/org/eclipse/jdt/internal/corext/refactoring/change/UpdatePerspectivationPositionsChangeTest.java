package org.eclipse.jdt.internal.corext.refactoring.change;

import static de.monochromata.eclipse.persp.PerspectivationPosition.PERSPECTIVATION_CATEGORY;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.junit.Test;

import de.monochromata.anaphors.perspectivation.Perspectivation;
import de.monochromata.eclipse.persp.PerspectivationPosition;

public class UpdatePerspectivationPositionsChangeTest implements PerspectivationPositionsChangeTesting {

	@Test
	public void perform_addsPositionsToAdd() throws Exception {
		final IDocument document = createDocument();
		final List<PerspectivationPosition> perspectivationPositions = asList(
				new PerspectivationPosition(0, 1, null, emptyList()),
				new PerspectivationPosition(2, 1, null, emptyList()));
		final UpdatePerspectivationPositionsChange change = new UpdatePerspectivationPositionsChange(Runnable::run,
				document, perspectivationPositions, emptyList(), emptyList(), emptyList());

		performChange(change);

		assertThat(document.getPositions(PERSPECTIVATION_CATEGORY)).containsExactlyElementsOf(perspectivationPositions);
	}

	@Test
	public void perform_linksPositionsToLink() throws Exception {
		final var document = createDocument();
		final var leftPosition = new PerspectivationPosition(0, 1, null, emptyList());
		final var rightPosition = new PerspectivationPosition(2, 1, null, emptyList());
		final List<Pair<PerspectivationPosition, PerspectivationPosition>> positionsToLink = List
				.of(new ImmutablePair<>(leftPosition, rightPosition));

		final var change = new UpdatePerspectivationPositionsChange(Runnable::run, document, emptyList(),
				positionsToLink, emptyList(), emptyList());
		performChange(change);

		assertThat(document.getPositions(PERSPECTIVATION_CATEGORY)).isEmpty();
		assertPositionsAreLinked(leftPosition, rightPosition);
	}

	@Test
	public void perform_removesPositionsToRemove() throws Exception {
		final PerspectivationPosition relatedExpressionPosition = new PerspectivationPosition(0, 3, null, emptyList());
		final PerspectivationPosition anaphorPosition = new PerspectivationPosition(4, 3, null, emptyList());

		final IDocument document = createDocument();
		document.addPosition(PERSPECTIVATION_CATEGORY, relatedExpressionPosition);

		final UpdatePerspectivationPositionsChange change = new UpdatePerspectivationPositionsChange(Runnable::run,
				document, emptyList(), emptyList(), asList(relatedExpressionPosition, anaphorPosition), emptyList());

		performChange(change);

		assertThat(document.getPositions(PERSPECTIVATION_CATEGORY)).isEmpty();
	}

	@Test
	public void perform_undoesPerspectivationOfPositionsToRemove() throws Exception {
		final AtomicInteger undoCount = new AtomicInteger();
		final PerspectivationPosition relatedExpressionPosition = new UndoCountingPerspectivationPosition(0, 3, null,
				emptyList(), undoCount);
		final PerspectivationPosition anaphorPosition = new UndoCountingPerspectivationPosition(4, 3, null, emptyList(),
				undoCount);

		final IDocument document = createDocument();
		document.addPosition(PERSPECTIVATION_CATEGORY, relatedExpressionPosition);

		final UpdatePerspectivationPositionsChange change = new UpdatePerspectivationPositionsChange(Runnable::run,
				document, emptyList(), emptyList(), asList(relatedExpressionPosition, anaphorPosition), emptyList());

		performChange(change);

		assertThat(undoCount.get()).isEqualTo(2);
	}

	@Test
	public void perform_returnsUndoChangeWithSwappedPositions() throws Throwable {
		final IDocument document = createDocument();
		final List<PerspectivationPosition> positionsToAdd = new ArrayList<>();
		final List<PerspectivationPosition> positionsToRemove = new ArrayList<>();
		final UpdatePerspectivationPositionsChange change = new UpdatePerspectivationPositionsChange(Runnable::run,
				document, positionsToAdd, emptyList(), positionsToRemove, emptyList());
		assertChangeResult(change, (final UpdatePerspectivationPositionsChange undoChange) -> {
			assertThat(undoChange.positionsToAdd).isSameAs(positionsToRemove);
			assertThat(undoChange.positionsToRemove).isSameAs(positionsToAdd);
		});
	}

	@Test
	public void perform_returnsUndoChangeWithSwappedListsForLinking() throws Throwable {
		final IDocument document = createDocument();
		final List<Pair<PerspectivationPosition, PerspectivationPosition>> positionsToLink = new ArrayList<>();
		final List<Pair<PerspectivationPosition, PerspectivationPosition>> positionsToUnlink = new ArrayList<>();
		final UpdatePerspectivationPositionsChange change = new UpdatePerspectivationPositionsChange(Runnable::run,
				document, emptyList(), positionsToLink, emptyList(), positionsToUnlink);

		assertChangeResult(change, (final UpdatePerspectivationPositionsChange undoChange) -> {
			assertThat(undoChange.positionsToLink).isSameAs(positionsToUnlink);
			assertThat(undoChange.positionsToUnlink).isSameAs(positionsToLink);
		});
	}

	private static class UndoCountingPerspectivationPosition extends PerspectivationPosition {

		private final AtomicInteger counter;

		public UndoCountingPerspectivationPosition(final int offset, final int length,
				final Predicate<DocumentEvent> condition, final List<Perspectivation> perspectivations,
				final AtomicInteger counter) {
			super(offset, length, condition, perspectivations);
			this.counter = counter;
		}

		@Override
		public void undoPerspectivation() throws BadLocationException {
			counter.incrementAndGet();
			super.undoPerspectivation();
		}

	}

}
