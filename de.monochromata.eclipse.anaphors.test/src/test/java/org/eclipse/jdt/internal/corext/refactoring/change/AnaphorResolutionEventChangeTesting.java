package org.eclipse.jdt.internal.corext.refactoring.change;

import java.util.function.BiConsumer;

import de.monochromata.eclipse.anaphors.NoopTesting;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent;
import de.monochromata.eclipse.position.PositionExceptionWrapping.BadPositionCategoryThrowingConsumer;

public interface AnaphorResolutionEventChangeTesting extends ChangeTesting, NoopTesting {

	default <T extends Throwable> void assertChangeResult(final AnaphorResolutionEvent.EventType eventTypeToFire,
			final AnaphorResolutionEvent eventToFire,
			final BiConsumer<AnaphorResolutionEvent.EventType, AnaphorResolutionEvent> anaphorResolutionNotifier,
			final BadPositionCategoryThrowingConsumer<AnaphorResolutionEventChange, T> resultAsserter)
			throws T, Throwable {
		final AnaphorResolutionEventChange change = new AnaphorResolutionEventChange(eventTypeToFire, eventToFire,
				anaphorResolutionNotifier);
		assertChangeResult(change, undoChange -> resultAsserter.accept(undoChange));
	}

}
