package org.eclipse.jdt.internal.corext.refactoring.change;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.ltk.core.refactoring.Change;
import org.junit.Test;

import de.monochromata.eclipse.anaphors.editor.update.InvocationRejection;

public class AnnotationUpdateChangeTest implements ChangeTesting, InvocationRejection {

    @Test
    public void isValidReturnsOk() throws Exception {
        final AnnotationUpdateChange change = new AnnotationUpdateChange(this::ensureNotInvoked, this::ensureNotInvoked,
                emptyList(), emptyList());
        assertStatusOk(change.isValid(null));
    }

    @Test
    public void performRemovesAnnotationsToRemove() throws Exception {
        final Annotation annotationToRemove = new Annotation("type", false, "text");
        final List<Annotation> removedAnnotations = new ArrayList<>();
        final AnnotationUpdateChange change = new AnnotationUpdateChange(removedAnnotations::add,
                this::ensureNotInvoked, asList(new ImmutablePair<>(annotationToRemove, null)),
                emptyList());

        change.perform(null);

        assertThat(removedAnnotations).containsExactly(annotationToRemove);
    }

    @Test
    public void performAddsAnnotationsToAdd() throws Exception {
        final ImmutablePair<Annotation, Position> annotationToAdd = new ImmutablePair<>(
                new Annotation("type", false, "text"),
                new Position(0, 10));
        final List<Pair<Annotation, Position>> addedAnnotations = new ArrayList<>();
        final AnnotationUpdateChange change = new AnnotationUpdateChange(this::ensureNotInvoked,
                (annotation, position) -> addedAnnotations.add(new ImmutablePair<>(annotation, position)),
                emptyList(), asList(annotationToAdd));

        change.perform(null);

        assertThat(addedAnnotations).containsExactly(annotationToAdd);
    }

    @Test
    public void undoChangeAddsAnnotationsToBeRemovedInitially() throws Exception {
        final ImmutablePair<Annotation, Position> annotationToRemove = new ImmutablePair<>(
                new Annotation("type", false, "text"),
                new Position(0, 10));
        final List<Pair<Annotation, Position>> addedAnnotations = new ArrayList<>();
        final AnnotationUpdateChange change = new AnnotationUpdateChange(unused -> {
        }, (annotation, position) -> addedAnnotations.add(new ImmutablePair<>(annotation, position)),
                asList(annotationToRemove), emptyList());

        final Change undoChange = change.perform(null);
        assertThat(addedAnnotations.isEmpty());

        undoChange.perform(null);
        assertThat(addedAnnotations).containsExactly(annotationToRemove);
    }

    @Test
    public void undoChangeRemovesAnnotationsToBeAddedInitially() throws Exception {
        final Annotation annotationToAdd = new Annotation("type", false, "text");
        final List<Annotation> removedAnnotations = new ArrayList<>();
        final AnnotationUpdateChange change = new AnnotationUpdateChange(removedAnnotations::add,
                (unused0, unused1) -> {
                }, emptyList(),
                asList(new ImmutablePair<>(annotationToAdd, new Position(0, 10))));

        final Change undoChange = change.perform(null);
        assertThat(removedAnnotations).isEmpty();

        undoChange.perform(null);
        assertThat(removedAnnotations).containsExactly(annotationToAdd);
    }

}
