package org.eclipse.jdt.internal.corext.refactoring.change;

import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicChainElement;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.eclipse.anaphors.position.AnaphoraPositionUpdater;
import de.monochromata.eclipse.persp.PerspectivationPosition;

public class AbstractAnaphoraPositionsTest implements AnaphoraPositionsTesting {

	protected final IDocument document = new Document(" relatedExpression  anaphor");
	protected final Anaphora anaphora = new Anaphora("CIC", "DA1", "Rt", "integer");
	protected final Pair<PerspectivationPosition, PerspectivationPosition> perspectivationPositions = createPerspectivationPositions();

	protected final Pair<PublicChainElement, PublicChainElement> existingChainElements = createChainElements();
	protected final Pair<PublicChainElement, PublicChainElement> newChainElements = createChainElements();

	protected final List<PublicChainElement> singleNewChainRoot = singletonList(newChainElements.getLeft());
	protected final List<PublicChainElement> newAndExistingChainRoots = asList(existingChainElements.getLeft(),
			newChainElements.getLeft());
	protected final List<PublicChainElement> chainRootWithOldAndNewAnaphor = singletonList(
			createChainWithOldAndNewAnaphorElements(existingChainElements.getRight(), newChainElements.getRight()));

	protected final Map<AnaphorPart<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora>, Anaphora> anaphoraByAnaphorPart = new HashMap<>();
	protected final Map<PublicChainElement, PerspectivationPosition> perspectivationPositionsByElement = new HashMap<>();

	{
		document.addPositionCategory(ANAPHORA_CATEGORY);
		document.addPositionUpdater(new AnaphoraPositionUpdater(anaphoras -> {
		}));

		anaphoraByAnaphorPart.put(newChainElements.getRight().anaphor, anaphora);

		perspectivationPositionsByElement.put(newChainElements.getLeft(), perspectivationPositions.getLeft());
		perspectivationPositionsByElement.put(newChainElements.getRight(), perspectivationPositions.getRight());
	}

}
