package org.eclipse.jdt.internal.corext.refactoring.code;

import static de.monochromata.eclipse.anaphors.editor.AnaphoraDocumentProvider.configure;
import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static de.monochromata.eclipse.persp.HidePosition.HIDE_CATEGORY;
import static de.monochromata.eclipse.persp.PerspectivationPosition.PERSPECTIVATION_CATEGORY;
import static de.monochromata.eclipse.persp.ToLowerCasePosition.TO_LOWER_CASE_CATEGORY;
import static de.monochromata.eclipse.position.DoublyLinkedPosition.link;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.Optional.empty;
import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.jdt.internal.corext.refactoring.code.DeRealizeAnaphors.deRealize;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.jface.text.Document;
import org.eclipse.text.edits.MultiTextEdit;
import org.junit.Before;
import org.junit.Test;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.perspectivation.Perspectivation;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;
import de.monochromata.eclipse.persp.HidePosition;
import de.monochromata.eclipse.persp.PerspectivationDocument;
import de.monochromata.eclipse.persp.PerspectivationDocumentManager;
import de.monochromata.eclipse.persp.PerspectivationDocumentTesting;
import de.monochromata.eclipse.persp.PerspectivationPosition;
import de.monochromata.eclipse.persp.ToLowerCasePosition;
import de.monochromata.eclipse.position.ChangeCollectingDoublyLinkedDeleter;

public class DeRealizeAnaphorsTest implements PerspectivationDocumentTesting, TextEditTesting {

	private final List<List<Anaphora>> deletedAnaphoras = new ArrayList<>();
	private final PerspectivationDocumentManager perspectivationDocumentManager = new PerspectivationDocumentManager();
	private Document originDocument;
	private PerspectivationDocument imageDocument;
	private final MultiTextEdit derealizationEdit = new MultiTextEdit();

	private final List<PerspectivationPosition> perspectivationPositionsToAdd = new ArrayList<>();
	private final List<PerspectivationPosition> perspectivationPositionsToRemove = new ArrayList<>();
	private final List<Pair<PerspectivationPosition, PerspectivationPosition>> perspectivationPositionsToLink = new ArrayList<>();
	private final List<Pair<PerspectivationPosition, PerspectivationPosition>> perspectivationPositionsToUnlink = new ArrayList<>();
	private final ChangeCollectingDoublyLinkedDeleter<PerspectivationPosition> maintainPerspectivation = new ChangeCollectingDoublyLinkedDeleter<>(
			perspectivationPositionsToAdd, perspectivationPositionsToRemove, perspectivationPositionsToLink,
			perspectivationPositionsToUnlink);

	private final List<AbstractAnaphoraPositionForRepresentation> anaphoraPositionsToAdd = new ArrayList<>();
	private final List<AbstractAnaphoraPositionForRepresentation> anaphoraPositionsToRemove = new ArrayList<>();
	private final List<Pair<AbstractAnaphoraPositionForRepresentation, AbstractAnaphoraPositionForRepresentation>> anaphoraPositionsToLink = new ArrayList<>();
	private final List<Pair<AbstractAnaphoraPositionForRepresentation, AbstractAnaphoraPositionForRepresentation>> anaphoraPositionsToUnlink = new ArrayList<>();
	private final ChangeCollectingDoublyLinkedDeleter<AbstractAnaphoraPositionForRepresentation> maintainAnaphoras = new ChangeCollectingDoublyLinkedDeleter<>(
			anaphoraPositionsToAdd, anaphoraPositionsToRemove, anaphoraPositionsToLink, anaphoraPositionsToUnlink);

	@Before
	public void configureDocument() throws Exception {
		final Pair<Document, PerspectivationDocument> originAndImage = createOriginAndImageDocument(
				"... relatedExpression ... anaphor ...");
		originDocument = originAndImage.getLeft();
		configure(originDocument, anaphoras -> deletedAnaphoras.add(anaphoras));
		imageDocument = originAndImage.getRight();
	}

	@Override
	public PerspectivationDocumentManager documentManager() {
		return perspectivationDocumentManager;
	}

	@Test
	public void deRealizeAnaphoraWithoutPerspectivations() throws Exception {
		final PositionForRelatedExpression relatedExpressionPosition = new PositionForRelatedExpression(4, 17);
		originDocument.addPosition(ANAPHORA_CATEGORY, relatedExpressionPosition);
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, relatedExpressionPosition.perspectivationPosition);

		final var anaphora = new Anaphora("relExpKind", "arKind", "refKind", "anaphor");
		final var anaphorPosition = new PositionForAnaphor(26, 7, anaphora);
		originDocument.addPosition(ANAPHORA_CATEGORY, anaphorPosition);
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, anaphorPosition.perspectivationPosition);
		link(relatedExpressionPosition, anaphorPosition);
		link(relatedExpressionPosition.perspectivationPosition, anaphorPosition.perspectivationPosition);

		final Triple<Anaphora, PositionForAnaphor, Optional<String>> anaphoraToDelete = new ImmutableTriple<>(anaphora,
				anaphorPosition, empty());
		final var allPerspectivationPositions = List.of(relatedExpressionPosition.perspectivationPosition,
				anaphorPosition.perspectivationPosition);
		final var allAnaphoraPositions = List.of(relatedExpressionPosition, anaphorPosition);

		deRealize(singletonList(anaphoraToDelete), originDocument, Optional.of(derealizationEdit),
				maintainPerspectivation, perspectivationPositionsToRemove, maintainAnaphoras,
				anaphoraPositionsToRemove);

		assertThat(derealizationEdit.getChildren()).isEmpty();
		assertThat(perspectivationPositionsToRemove).containsExactlyElementsOf(allPerspectivationPositions);
		assertThat(anaphoraPositionsToRemove).containsExactlyElementsOf(allAnaphoraPositions);
		// The anaphora position updater is not used, hence no events about deleted
		// positions are received from it.
		assertThat(deletedAnaphoras).isEmpty();
		assertThat(originDocument.getPositions(PERSPECTIVATION_CATEGORY))
				.containsExactlyElementsOf(allPerspectivationPositions);
		assertThat(originDocument.getPositions(ANAPHORA_CATEGORY)).containsExactlyElementsOf(allAnaphoraPositions);

		assertThat(originDocument.get()).isEqualTo("... relatedExpression ... anaphor ...");
		assertThat(imageDocument.get()).isEqualTo("... relatedExpression ... anaphor ...");
	}

	@Test
	public void deRealizeAnaphorWithToLowerCaseAndHideWithoutTextEdits() throws Exception {
		final PositionForRelatedExpression relatedExpressionPosition = new PositionForRelatedExpression(4, 17);
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, relatedExpressionPosition.perspectivationPosition);
		originDocument.addPosition(ANAPHORA_CATEGORY, relatedExpressionPosition);

		final Anaphora anaphora = new Anaphora("relExpKind", "arKind", "refKind", "anaphor");
		final List<Perspectivation> perspectivations = asList(new Perspectivation.Hide(0, 21),
				new Perspectivation.ToLowerCase(21), new Perspectivation.Hide(28, 2));
		final PerspectivationPosition perspectivationPosition = new PerspectivationPosition(26, 7, all -> true,
				perspectivations);
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, perspectivationPosition);
		final PositionForAnaphor anaphorPosition = new PositionForAnaphor(26, 7, perspectivationPosition, anaphora);
		originDocument.addPosition(ANAPHORA_CATEGORY, anaphorPosition);
		link(relatedExpressionPosition, anaphorPosition);
		link(relatedExpressionPosition.perspectivationPosition, anaphorPosition.perspectivationPosition);

		originDocument.replace(26, 7, "relatedExpression.getAnaphor()");
		assertThat(originDocument.get()).isEqualTo("... relatedExpression ... relatedExpression.getAnaphor() ...");
		assertThat(imageDocument.get()).isEqualTo("... relatedExpression ... anaphor ...");

		final Triple<Anaphora, PositionForAnaphor, Optional<String>> anaphoraToDelete = new ImmutableTriple<>(anaphora,
				anaphorPosition, empty());
		final var allPerspectivationPositions = List.of(relatedExpressionPosition.perspectivationPosition,
				anaphorPosition.perspectivationPosition);
		final var allAnaphoraPositions = List.of(relatedExpressionPosition, anaphorPosition);

		deRealize(singletonList(anaphoraToDelete), originDocument, Optional.empty(), maintainPerspectivation,
				perspectivationPositionsToRemove, maintainAnaphoras, anaphoraPositionsToRemove);

		assertThat(derealizationEdit.getChildren()).hasSize(0);

		assertThat(perspectivationPositionsToRemove).containsExactlyElementsOf(allPerspectivationPositions);
		assertThat(anaphoraPositionsToRemove).containsExactlyElementsOf(allAnaphoraPositions);
		// The anaphora position updater is not used, hence no events about deleted
		// positions are received from it.
		assertThat(deletedAnaphoras).isEmpty();
		assertThat(originDocument.getPositions(TO_LOWER_CASE_CATEGORY)).containsExactly(new ToLowerCasePosition(47));
		assertThat(originDocument.getPositions(HIDE_CATEGORY)).containsExactly(new HidePosition(26, 21),
				new HidePosition(54, 2));
		assertThat(originDocument.getPositions(PERSPECTIVATION_CATEGORY))
				.containsExactlyElementsOf(allPerspectivationPositions);
		assertThat(originDocument.getPositions(ANAPHORA_CATEGORY)).containsExactlyElementsOf(allAnaphoraPositions);

		// The documents should not have been modified yet because the
		// ToLowerCasePosition and HidePosition instances are still available and no
		// text edits have been recorded.
		assertThat(originDocument.get()).isEqualTo("... relatedExpression ... relatedExpression.getAnaphor() ...");
		assertThat(imageDocument.get()).isEqualTo("... relatedExpression ... anaphor ...");
	}

	@Test
	public void deRealizeAnaphorWithToLowerCaseAndHide() throws Exception {
		final PositionForRelatedExpression relatedExpressionPosition = new PositionForRelatedExpression(4, 17);
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, relatedExpressionPosition.perspectivationPosition);
		originDocument.addPosition(ANAPHORA_CATEGORY, relatedExpressionPosition);

		final Anaphora anaphora = new Anaphora("relExpKind", "arKind", "refKind", "anaphor");
		final List<Perspectivation> perspectivations = asList(new Perspectivation.Hide(0, 21),
				new Perspectivation.ToLowerCase(21), new Perspectivation.Hide(28, 2));
		final PerspectivationPosition perspectivationPosition = new PerspectivationPosition(26, 7, all -> true,
				perspectivations);
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, perspectivationPosition);
		final PositionForAnaphor anaphorPosition = new PositionForAnaphor(26, 7, perspectivationPosition, anaphora);
		originDocument.addPosition(ANAPHORA_CATEGORY, anaphorPosition);
		link(relatedExpressionPosition, anaphorPosition);
		link(relatedExpressionPosition.perspectivationPosition, anaphorPosition.perspectivationPosition);

		originDocument.replace(26, 7, "relatedExpression.getAnaphor()");
		assertThat(originDocument.get()).isEqualTo("... relatedExpression ... relatedExpression.getAnaphor() ...");
		assertThat(imageDocument.get()).isEqualTo("... relatedExpression ... anaphor ...");

		final Triple<Anaphora, PositionForAnaphor, Optional<String>> anaphoraToDelete = new ImmutableTriple<>(anaphora,
				anaphorPosition, empty());
		final var allPerspectivationPositions = List.of(relatedExpressionPosition.perspectivationPosition,
				anaphorPosition.perspectivationPosition);
		final var allAnaphoraPositions = List.of(relatedExpressionPosition, anaphorPosition);

		deRealize(singletonList(anaphoraToDelete), originDocument, Optional.of(derealizationEdit),
				maintainPerspectivation, perspectivationPositionsToRemove, maintainAnaphoras,
				anaphoraPositionsToRemove);

		assertThat(derealizationEdit.getChildren()).hasSize(3);
		assertDeleteEdit(derealizationEdit, 0, 26, 21);
		assertReplaceEdit(derealizationEdit, 1, 47, 1, "a");
		assertDeleteEdit(derealizationEdit, 2, 54, 2);

		assertThat(perspectivationPositionsToRemove).containsExactlyElementsOf(allPerspectivationPositions);
		assertThat(anaphoraPositionsToRemove).containsExactlyElementsOf(allAnaphoraPositions);
		// The anaphora position updater is not used, hence no events about deleted
		// positions are received from it.
		assertThat(deletedAnaphoras).isEmpty();
		assertThat(originDocument.getPositions(TO_LOWER_CASE_CATEGORY)).containsExactly(new ToLowerCasePosition(47));
		assertThat(originDocument.getPositions(HIDE_CATEGORY)).containsExactly(new HidePosition(26, 21),
				new HidePosition(54, 2));
		assertThat(originDocument.getPositions(PERSPECTIVATION_CATEGORY))
				.containsExactlyElementsOf(allPerspectivationPositions);
		assertThat(originDocument.getPositions(ANAPHORA_CATEGORY)).containsExactlyElementsOf(allAnaphoraPositions);

		// The documents should not have been modified yet because the
		// ToLowerCasePosition and HidePosition instances are still available and the
		// text edits are applied only later.
		assertThat(originDocument.get()).isEqualTo("... relatedExpression ... relatedExpression.getAnaphor() ...");
		assertThat(imageDocument.get()).isEqualTo("... relatedExpression ... anaphor ...");
	}

	@Test
	public void deRealizeRelatedExpressionWithToLowerCaseAndHide() throws Exception {
		final PerspectivationPosition rePerspectivationPosition = new PerspectivationPosition(4, 17, all -> true,
				singletonList(new Perspectivation.Hide(0, 16)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, rePerspectivationPosition);
		final PositionForRelatedExpression relatedExpressionPosition = new PositionForRelatedExpression(4, 17,
				rePerspectivationPosition);
		originDocument.addPosition(ANAPHORA_CATEGORY, relatedExpressionPosition);

		final Anaphora anaphora = new Anaphora("relExpKind", "arKind", "refKind", "anaphor");
		final PerspectivationPosition anaphorPerspectivationPosition = new PerspectivationPosition(26, 7);
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, anaphorPerspectivationPosition);
		link(rePerspectivationPosition, anaphorPerspectivationPosition);
		final PositionForAnaphor anaphorPosition = new PositionForAnaphor(26, 7, anaphorPerspectivationPosition,
				anaphora);
		originDocument.addPosition(ANAPHORA_CATEGORY, anaphorPosition);
		link(relatedExpressionPosition, anaphorPosition);

		originDocument.replace(4, 0, "final Foo foo = ");
		assertThat(originDocument.get()).isEqualTo("... final Foo foo = relatedExpression ... anaphor ...");
		assertThat(imageDocument.get()).isEqualTo("... relatedExpression ... anaphor ...");

		final Triple<Anaphora, PositionForAnaphor, Optional<String>> anaphoraToDelete = new ImmutableTriple<>(anaphora,
				anaphorPosition, empty());
		final var allPerspectivationPositions = List.of(rePerspectivationPosition, anaphorPerspectivationPosition);
		final var allAnaphoraPositions = List.of(relatedExpressionPosition, anaphorPosition);

		deRealize(singletonList(anaphoraToDelete), originDocument, Optional.of(derealizationEdit),
				maintainPerspectivation, perspectivationPositionsToRemove, maintainAnaphoras,
				anaphoraPositionsToRemove);

		assertThat(derealizationEdit.getChildren()).hasSize(1);
		assertDeleteEdit(derealizationEdit, 0, 4, 16);

		assertThat(perspectivationPositionsToRemove).containsExactlyElementsOf(allPerspectivationPositions);
		assertThat(anaphoraPositionsToRemove).containsExactlyElementsOf(allAnaphoraPositions);
		// The anaphora position updater is not used, hence no events about deleted
		// positions are received from it.
		assertThat(deletedAnaphoras).isEmpty();
		assertThat(originDocument.getPositions(PERSPECTIVATION_CATEGORY))
				.containsExactlyElementsOf(allPerspectivationPositions);
		assertThat(originDocument.getPositions(ANAPHORA_CATEGORY)).containsExactlyElementsOf(allAnaphoraPositions);
	}

	@Test
	public void deRealizeRelatedExpressionWhenItsTwoAnaphorsAreDeleted() throws Exception {
		imageDocument.set("... relatedExpression ... anaphor1 ... anaphor2 ...");

		final PerspectivationPosition rePerspectivationPosition = new PerspectivationPosition(4, 17,
				event -> event.getOffset() == 4 && event.getLength() == 0,
				singletonList(new Perspectivation.Hide(0, 16)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, rePerspectivationPosition);
		final PositionForRelatedExpression relatedExpressionPosition = new PositionForRelatedExpression(4, 17,
				rePerspectivationPosition);
		originDocument.addPosition(ANAPHORA_CATEGORY, relatedExpressionPosition);

		final Anaphora anaphora1 = new Anaphora("relExpKind", "arKind", "refKind", "anaphor1");
		final PerspectivationPosition anaphor1PerspectivationPosition = new PerspectivationPosition(26, 7,
				event -> event.getOffset() == 4 && event.getLength() == 0, asList(new Perspectivation.Hide(0, 7),
						new Perspectivation.ToLowerCase(7), new Perspectivation.Hide(15, 2)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, anaphor1PerspectivationPosition);
		link(rePerspectivationPosition, anaphor1PerspectivationPosition);
		final PositionForAnaphor anaphor1Position = new PositionForAnaphor(26, 7, anaphor1PerspectivationPosition,
				anaphora1);
		originDocument.addPosition(ANAPHORA_CATEGORY, anaphor1Position);
		link(relatedExpressionPosition, anaphor1Position);

		final Anaphora anaphora2 = new Anaphora("relExpKind", "arKind", "refKind", "anaphor2");
		final PerspectivationPosition anaphor2PerspectivationPosition = new PerspectivationPosition(39, 7,
				event -> event.getOffset() == 4 && event.getLength() == 0, asList(new Perspectivation.Hide(0, 7),
						new Perspectivation.ToLowerCase(7), new Perspectivation.Hide(15, 2)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, anaphor2PerspectivationPosition);
		link(rePerspectivationPosition, anaphor2PerspectivationPosition);
		final PositionForAnaphor anaphor2Position = new PositionForAnaphor(39, 7, anaphor2PerspectivationPosition,
				anaphora2);
		originDocument.addPosition(ANAPHORA_CATEGORY, anaphor2Position);
		link(relatedExpressionPosition, anaphor2Position);

		originDocument.replace(39, 8, "foo.getAnaphor2()");
		originDocument.replace(26, 8, "foo.getAnaphor1()");
		originDocument.replace(4, 0, "final Foo foo = ");
		assertThat(originDocument.get())
				.isEqualTo("... final Foo foo = relatedExpression ... foo.getAnaphor1() ... foo.getAnaphor2() ...");
		assertThat(imageDocument.get()).isEqualTo("... relatedExpression ... anaphor1 ... anaphor2 ...");

		final List<Triple<Anaphora, PositionForAnaphor, Optional<String>>> anaphorasToDelete = asList(
				new ImmutableTriple<>(anaphora1, anaphor1Position, empty()),
				new ImmutableTriple<>(anaphora2, anaphor2Position, empty()));
		final var allPerspectivationPositions = List.of(rePerspectivationPosition, anaphor1PerspectivationPosition,
				anaphor2PerspectivationPosition);
		final var allAnaphoraPositions = List.of(relatedExpressionPosition, anaphor1Position, anaphor2Position);

		deRealize(anaphorasToDelete, originDocument, Optional.of(derealizationEdit), maintainPerspectivation,
				perspectivationPositionsToRemove, maintainAnaphoras, anaphoraPositionsToRemove);

		assertThat(derealizationEdit.getChildren()).hasSize(7);
		assertDeleteEdit(derealizationEdit, 0, 4, 16);
		assertDeleteEdit(derealizationEdit, 1, 42, 7);
		assertReplaceEdit(derealizationEdit, 2, 49, 1, "a");
		assertDeleteEdit(derealizationEdit, 3, 57, 2);
		assertDeleteEdit(derealizationEdit, 4, 64, 7);
		assertReplaceEdit(derealizationEdit, 5, 71, 1, "a");
		assertDeleteEdit(derealizationEdit, 6, 79, 2);

		assertThat(perspectivationPositionsToRemove).containsExactly(anaphor1PerspectivationPosition,
				rePerspectivationPosition, anaphor2PerspectivationPosition);
		assertThat(anaphoraPositionsToRemove).containsExactly(anaphor1Position, relatedExpressionPosition,
				anaphor2Position);
		// The anaphora position updater is not used, hence no events about deleted
		// positions are received from it.
		assertThat(deletedAnaphoras).isEmpty();
		assertThat(originDocument.getPositions(PERSPECTIVATION_CATEGORY))
				.containsExactlyElementsOf(allPerspectivationPositions);
		assertThat(originDocument.getPositions(ANAPHORA_CATEGORY)).containsExactlyElementsOf(allAnaphoraPositions);
	}

	@Test
	public void doNotDeRealizeRelatedExpressionWhenNotAllOfItsAnaphorsAreDeleted() throws Exception {
		imageDocument.set("... relatedExpression ... anaphor1 ... anaphor2 ...");

		final PerspectivationPosition rePerspectivationPosition = new PerspectivationPosition(4, 17,
				event -> event.getOffset() == 4 && event.getLength() == 0,
				singletonList(new Perspectivation.Hide(0, 16)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, rePerspectivationPosition);
		final PositionForRelatedExpression relatedExpressionPosition = new PositionForRelatedExpression(4, 17,
				rePerspectivationPosition);
		originDocument.addPosition(ANAPHORA_CATEGORY, relatedExpressionPosition);

		final Anaphora anaphora1 = new Anaphora("relExpKind", "arKind", "refKind", "anaphor1");
		final PerspectivationPosition anaphor1PerspectivationPosition = new PerspectivationPosition(26, 7,
				event -> event.getOffset() == 4 && event.getLength() == 0, asList(new Perspectivation.Hide(0, 7),
						new Perspectivation.ToLowerCase(7), new Perspectivation.Hide(15, 2)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, anaphor1PerspectivationPosition);
		link(rePerspectivationPosition, anaphor1PerspectivationPosition);
		final PositionForAnaphor anaphor1Position = new PositionForAnaphor(26, 7, anaphor1PerspectivationPosition,
				anaphora1);
		originDocument.addPosition(ANAPHORA_CATEGORY, anaphor1Position);
		link(relatedExpressionPosition, anaphor1Position);

		final Anaphora anaphora2 = new Anaphora("relExpKind", "arKind", "refKind", "anaphor2");
		final PerspectivationPosition anaphor2PerspectivationPosition = new PerspectivationPosition(39, 7,
				event -> event.getOffset() == 4 && event.getLength() == 0, asList(new Perspectivation.Hide(0, 7),
						new Perspectivation.ToLowerCase(7), new Perspectivation.Hide(15, 2)));
		originDocument.addPosition(PERSPECTIVATION_CATEGORY, anaphor2PerspectivationPosition);
		link(rePerspectivationPosition, anaphor2PerspectivationPosition);
		final PositionForAnaphor anaphor2Position = new PositionForAnaphor(39, 7, anaphor2PerspectivationPosition,
				anaphora2);
		originDocument.addPosition(ANAPHORA_CATEGORY, anaphor2Position);
		link(relatedExpressionPosition, anaphor2Position);

		originDocument.replace(39, 8, "foo.getAnaphor2()");
		originDocument.replace(26, 8, "foo.getAnaphor1()");
		originDocument.replace(4, 0, "final Foo foo = ");
		assertThat(originDocument.get())
				.isEqualTo("... final Foo foo = relatedExpression ... foo.getAnaphor1() ... foo.getAnaphor2() ...");
		assertThat(imageDocument.get()).isEqualTo("... relatedExpression ... anaphor1 ... anaphor2 ...");

		final List<Triple<Anaphora, PositionForAnaphor, Optional<String>>> anaphorasToDelete = asList(
				new ImmutableTriple<>(anaphora1, anaphor1Position, empty()));
		final var allPerspectivationPositions = List.of(rePerspectivationPosition, anaphor1PerspectivationPosition,
				anaphor2PerspectivationPosition);
		final var allAnaphoraPositions = List.of(relatedExpressionPosition, anaphor1Position, anaphor2Position);

		deRealize(anaphorasToDelete, originDocument, Optional.of(derealizationEdit), maintainPerspectivation,
				perspectivationPositionsToRemove, maintainAnaphoras, anaphoraPositionsToRemove);

		assertThat(derealizationEdit.getChildren()).hasSize(3);
		assertDeleteEdit(derealizationEdit, 0, 42, 7);
		assertReplaceEdit(derealizationEdit, 1, 49, 1, "a");
		assertDeleteEdit(derealizationEdit, 2, 57, 2);

		assertThat(perspectivationPositionsToRemove).containsExactly(anaphor1PerspectivationPosition);
		assertThat(anaphoraPositionsToRemove).containsExactly(anaphor1Position);
		// The anaphora position updater is not used, hence no events about deleted
		// positions are received from it.
		assertThat(deletedAnaphoras).isEmpty();
		assertThat(originDocument.getPositions(PERSPECTIVATION_CATEGORY))
				.containsExactlyElementsOf(allPerspectivationPositions);
		assertThat(originDocument.getPositions(ANAPHORA_CATEGORY)).containsExactlyElementsOf(allAnaphoraPositions);
	}
}
