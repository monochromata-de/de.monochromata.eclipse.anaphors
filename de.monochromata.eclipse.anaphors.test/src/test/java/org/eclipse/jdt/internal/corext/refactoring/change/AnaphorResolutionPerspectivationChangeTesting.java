package org.eclipse.jdt.internal.corext.refactoring.change;

import java.util.List;
import java.util.function.BiConsumer;

import org.eclipse.jface.text.Position;

import de.monochromata.eclipse.anaphors.NoopTesting;
import de.monochromata.eclipse.anaphors.event.AnaphorResolutionPerspectivationEvent;
import de.monochromata.eclipse.position.PositionExceptionWrapping.BadPositionCategoryThrowingConsumer;

public interface AnaphorResolutionPerspectivationChangeTesting extends ChangeTesting, NoopTesting {

    default <T extends Throwable> void assertChangeResult(
            final AnaphorResolutionPerspectivationEvent event,
            final List<? extends Position> positions,
            final BiConsumer<AnaphorResolutionPerspectivationEvent, List<? extends Position>> anaphorResolutionPerspectivationNotifier,
            final BadPositionCategoryThrowingConsumer<AnaphorResolutionPerspectivationChange, T> resultAsserter)
            throws T, Throwable {
        final AnaphorResolutionPerspectivationChange change = new AnaphorResolutionPerspectivationChange(event,
                positions,
                anaphorResolutionPerspectivationNotifier);
        assertChangeResult(change,
                undoChange -> resultAsserter.accept((AnaphorResolutionPerspectivationChange) undoChange));
    }

}
