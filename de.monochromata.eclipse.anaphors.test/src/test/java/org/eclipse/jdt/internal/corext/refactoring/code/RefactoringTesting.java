package org.eclipse.jdt.internal.corext.refactoring.code;

import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.ltk.core.refactoring.RefactoringStatus.OK;

import org.eclipse.ltk.core.refactoring.RefactoringStatus;

public interface RefactoringTesting {

	default void assertRefactoringStatus(final RefactoringStatus status, final int expectedSeverity,
			final String expectedMessage) {
		assertStatusSeverity(status, expectedSeverity);
		assertThat(status.getMessageMatchingSeverity(expectedSeverity)).isEqualTo(expectedMessage);
	}

	default void assertStatusOk(final RefactoringStatus status) {
		assertStatusSeverity(status, OK);
	}

	default void assertStatusSeverity(final RefactoringStatus status, final int expectedSeverity) {
		assertThat(status.getSeverity()).isEqualTo(expectedSeverity);
	}

}
