package org.eclipse.jdt.internal.corext.refactoring.code;

import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static de.monochromata.eclipse.persp.HidePosition.HIDE_CATEGORY;
import static de.monochromata.eclipse.persp.PerspectivationPosition.PERSPECTIVATION_CATEGORY;
import static de.monochromata.eclipse.position.DoublyLinkedPosition.link;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.junit.Test;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.eclipse.anaphors.annotation.AnnotationsTesting;
import de.monochromata.eclipse.anaphors.editor.update.AnaphoraReresolvingDocumentListener;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;
import de.monochromata.eclipse.anaphors.position.PositionsTesting;
import de.monochromata.eclipse.persp.HidePosition;
import de.monochromata.eclipse.persp.PerspectivationPosition;
import de.monochromata.eclipse.position.DoublyLinkedPosition;

public class ResolveAnaphorsRefactoringTest
		implements ResolveAnaphorsRefactoringTesting, AnnotationsTesting, PositionsTesting {

	@Test
	public void createsASyntheticCompositeChange() throws Exception {
		getDefaultCompilationUnit().andThen(assertRefactoringChange((unused, change) -> {
			assertThat(change).isInstanceOf(CompositeChange.class);
			assertThat(((CompositeChange) change).isSynthetic()).isTrue();
		})).apply(null);
	}

	@Test
	public void modifiesTheDocument() throws Exception {
		getDefaultCompilationUnit().andThen(assertRefactoringResults(document -> {
			assertThat(document.get()).isEqualTo(
					"public class Foo { public static void foo() { final var integer = new Integer(1); System.err.println(integer); } }");
		})).apply(null);
	}

	// It is not tested whether ToLowerCasePosition is added, because the default
	// test source does not contain an anaphor that yields a ToLowerCasePosition.
	@Test
	public void addsHidePositions() throws Exception {
		getDefaultCompilationUnit().andThen(assertRefactoringResults(
				document -> assertPositions(document, HIDE_CATEGORY, new HidePosition(46, 20)))).apply(null);
	}

	@Test
	public void addsPerspectivationPositions() throws Exception {
		getDefaultCompilationUnit().andThen(assertRefactoringResults(document -> assertPositions(document,
				PERSPECTIVATION_CATEGORY, new PerspectivationPosition(46, 35, unused -> false, emptyList()),
				new PerspectivationPosition(101, 7, unused -> false, emptyList())))).apply(null);
	}

	@Test
	public void addedPerspectivationPositionsAreLinked() throws Exception {
		getDefaultCompilationUnit().andThen(assertRefactoringResults(document -> {
			final var positions = document.getPositions(PERSPECTIVATION_CATEGORY);
			assertThat(positions).hasSize(2);
			assertPositionsAreLinked((DoublyLinkedPosition) positions[0], (DoublyLinkedPosition) positions[1]);
		})).apply(null);
	}

	@Test
	public void addsPerspectivationPositions_forTwoAnaphorsForSameRelatedExpression() throws Exception {
		getCompilationUnit("FooBar",
				"public class FooBar { public static void foo() { new FooBar(); System.err.println(fooBar+\" \"+bar); } }")
						.andThen(assertRefactoringResults(
								createRefactoringForProblemPositions(List.of(new Position(82, 6), new Position(93, 3))),
								document -> assertPositions(document, PERSPECTIVATION_CATEGORY,
										new PerspectivationPosition(49, 29, unused -> false, emptyList()),
										new PerspectivationPosition(98, 3, unused -> false, emptyList()),
										new PerspectivationPosition(106, 3, unused -> false, emptyList()))))
						.apply(null);
	}

	@Test
	public void reUsesPerspectivationPositions_forSecondAnaphorForSameRelatedExpression() throws Exception {
		// These existing anaphora and positions are actually incorrect - they are not
		// realized in the source code. But they work for simulating an existing
		// anaphora.
		final var fakeAnaphora = new Anaphora("CIC", "DA1Re", "Rt", "fooBar");
		final var fakeRelatedExpressionPosition = new PositionForRelatedExpression(49, 12);
		final var fakeExistingAnaphorPosition = new PositionForAnaphor(82, 6, fakeAnaphora);
		link(fakeRelatedExpressionPosition, fakeExistingAnaphorPosition);
		link(fakeRelatedExpressionPosition.perspectivationPosition,
				fakeExistingAnaphorPosition.perspectivationPosition);
		getCompilationUnit("FooBar",
				"public class FooBar { public static void foo() { new FooBar(); System.err.println(fooBar+\" \"+bar); } }")
						.andThen(assertRefactoringResults(
								createRefactoring(
										List.of(fakeRelatedExpressionPosition.perspectivationPosition,
												fakeExistingAnaphorPosition.perspectivationPosition),
										List.of(fakeRelatedExpressionPosition), List.of(fakeExistingAnaphorPosition),
										List.of(new Position(93, 3))),
								document -> assertPositions(document, PERSPECTIVATION_CATEGORY,
										// relatedExpressionPosition.perspectivationPosition has been updated, not added
										// again
										new PerspectivationPosition(49, 29, unused -> false, emptyList()),
										// existingAnaphorPosition.perspectivationPosition has been updated, not added
										// again
										new PerspectivationPosition(98, 6, unused -> false, emptyList()),
										new PerspectivationPosition(109, 3, unused -> false, emptyList()))))
						.apply(null);
	}

	@Test
	public void linksPerspectivationPositions_forTwoAnaphorsForSameRelatedExpression() throws Exception {
		getCompilationUnit("FooBar",
				"public class FooBar { public static void foo() { new FooBar(); System.err.println(fooBar+\" \"+bar); } }")
						.andThen(assertRefactoringResults(
								createRefactoringForProblemPositions(List.of(new Position(82, 6), new Position(93, 3))),
								document -> {
									final var positions = document.getPositions(PERSPECTIVATION_CATEGORY);
									assertThat(positions).hasSize(3);
									assertPositionsAreLinked((DoublyLinkedPosition) positions[0],
											(DoublyLinkedPosition) positions[1], (DoublyLinkedPosition) positions[2]);
								}))
						.apply(null);
	}

	@Test
	public void linksToReUsedPerspectivationPositions_forSecondAnaphorForSameRelatedExpression() throws Exception {
		// These existing anaphora and positions are actually incorrect - they are not
		// realized in the source code. But they work for simulating an existing
		// anaphora.
		final var fakeAnaphora = new Anaphora("CIC", "DA1Re", "Rt", "fooBar");
		final var fakeRelatedExpressionPosition = new PositionForRelatedExpression(49, 12);
		final var fakeAnaphorPosition = new PositionForAnaphor(82, 6, fakeAnaphora);
		link(fakeRelatedExpressionPosition, fakeAnaphorPosition);
		link(fakeRelatedExpressionPosition.perspectivationPosition, fakeAnaphorPosition.perspectivationPosition);
		getCompilationUnit("FooBar",
				"public class FooBar { public static void foo() { new FooBar(); System.err.println(fooBar+\" \"+bar); } }")
						.andThen(assertRefactoringResults(createRefactoring(
								List.of(fakeRelatedExpressionPosition.perspectivationPosition,
										fakeAnaphorPosition.perspectivationPosition),
								List.of(fakeRelatedExpressionPosition), List.of(fakeAnaphorPosition),
								List.of(new Position(93, 3))), document -> {
									final var positions = document.getPositions(PERSPECTIVATION_CATEGORY);
									assertThat(positions).hasSize(3);
									assertPositionsAreLinked((DoublyLinkedPosition) positions[0],
											(DoublyLinkedPosition) positions[1], (DoublyLinkedPosition) positions[2]);
								}))
						.apply(null);
	}

	@Test
	public void addsAnaphoraRepresentationPositions() throws Exception {
		getDefaultCompilationUnit().andThen(assertRefactoringResults(document -> {
			final Anaphora anaphora = new Anaphora("LVD", "DA1Re", "Rn", "integer");
			assertPositions(document, new PositionForRelatedExpression(46, 35),
					new PositionForAnaphor(101, 7, anaphora));
		})).apply(null);
	}

	@Test
	public void addedAnaphoraPresentationPositionsAreLinked() throws Exception {
		getDefaultCompilationUnit().andThen(assertRefactoringResults(document -> {
			final Position[] positions = document.getPositions(ANAPHORA_CATEGORY);
			assertThat(positions).hasSize(2);
			assertPositionsAreLinked((DoublyLinkedPosition) positions[0], (DoublyLinkedPosition) positions[1]);
		})).apply(null);
	}

	@Test
	public void addsAnaphoraPositions_forTwoAnaphorsForSameRelatedExpression() throws Exception {
		getCompilationUnit("FooBar",
				"public class FooBar { public static void foo() { new FooBar(); System.err.println(fooBar+\" \"+bar); } }")
						.andThen(assertRefactoringResults(
								createRefactoringForProblemPositions(List.of(new Position(82, 6), new Position(93, 3))),
								document -> assertPositions(document, ANAPHORA_CATEGORY,
										new PositionForRelatedExpression(49, 29),
										new PositionForAnaphor(98, 3, new Anaphora("LVD", "DA1Re", "Rn", "bar")),
										new PositionForAnaphor(106, 3, new Anaphora("LVD", "DA1Re", "Rn", "bar")))))
						.apply(null);
	}

	@Test
	public void reUsesAnaphoraPositions_forSecondAnaphorForSameRelatedExpression() throws Exception {
		// These existing anaphora and positions are actually incorrect - they are not
		// realized in the source code. But they work for simulating an existing
		// anaphora.
		final var fakeAnaphora = new Anaphora("CIC", "DA1Re", "Rt", "fooBar");
		final var fakeRelatedExpressionPosition = new PositionForRelatedExpression(49, 12);
		final var fakeAnaphorPosition = new PositionForAnaphor(82, 6, fakeAnaphora);
		link(fakeRelatedExpressionPosition, fakeAnaphorPosition);
		link(fakeRelatedExpressionPosition.perspectivationPosition, fakeAnaphorPosition.perspectivationPosition);
		getCompilationUnit("FooBar",
				"public class FooBar { public static void foo() { new FooBar(); System.err.println(fooBar+\" \"+bar); } }")
						.andThen(assertRefactoringResults(
								createRefactoring(
										List.of(fakeRelatedExpressionPosition.perspectivationPosition,
												fakeAnaphorPosition.perspectivationPosition),
										List.of(fakeRelatedExpressionPosition), List.of(fakeAnaphorPosition),
										List.of(new Position(93, 3))),
								document -> assertPositions(document, ANAPHORA_CATEGORY,
										// relatedExpressionPosition has been updated, not added
										// again
										new PositionForRelatedExpression(49, 29),
										// existingAnaphorPosition has been updated, not added
										// again
										new PositionForAnaphor(98, 6, new Anaphora("CIC", "DA1Re", "Rt", "fooBar")),
										new PositionForAnaphor(109, 3, new Anaphora("LVD", "DA1Re", "Rn", "bar")))))
						.apply(null);
	}

	@Test
	public void linksAnaphoraPositions_forTwoAnaphorsForSameRelatedExpression() throws Exception {
		getCompilationUnit("FooBar",
				"public class FooBar { public static void foo() { new FooBar(); System.err.println(fooBar+\" \"+bar); } }")
						.andThen(assertRefactoringResults(
								createRefactoringForProblemPositions(List.of(new Position(82, 6), new Position(93, 3))),
								document -> {
									final var positions = document.getPositions(ANAPHORA_CATEGORY);
									assertThat(positions).hasSize(3);
									assertPositionsAreLinked((DoublyLinkedPosition) positions[0],
											(DoublyLinkedPosition) positions[1], (DoublyLinkedPosition) positions[2]);
								}))
						.apply(null);
	}

	@Test
	public void linksToReUsedAnaphoraPositions_forSecondAnaphorForSameRelatedExpression() throws Exception {
		// These existing anaphora and positions are actually incorrect - they are not
		// realized in the source code. But they work for simulating an existing
		// anaphora.
		final var fakeAnaphora = new Anaphora("CIC", "DA1Re", "Rt", "fooBar");
		final var fakeRelatedExpressionPosition = new PositionForRelatedExpression(49, 12);
		final var fakeAnaphorPosition = new PositionForAnaphor(82, 6, fakeAnaphora);
		link(fakeRelatedExpressionPosition, fakeAnaphorPosition);
		link(fakeRelatedExpressionPosition.perspectivationPosition, fakeAnaphorPosition.perspectivationPosition);
		getCompilationUnit("FooBar",
				"public class FooBar { public static void foo() { new FooBar(); System.err.println(fooBar+\" \"+bar); } }")
						.andThen(assertRefactoringResults(createRefactoring(
								List.of(fakeRelatedExpressionPosition.perspectivationPosition,
										fakeAnaphorPosition.perspectivationPosition),
								List.of(fakeRelatedExpressionPosition), List.of(fakeAnaphorPosition),
								List.of(new Position(93, 3))), document -> {
									final var positions = document.getPositions(ANAPHORA_CATEGORY);
									assertThat(positions).hasSize(3);
									assertPositionsAreLinked((DoublyLinkedPosition) positions[0],
											(DoublyLinkedPosition) positions[1], (DoublyLinkedPosition) positions[2]);
								}))
						.apply(null);
	}

	@Test
	public void memberInvalidationIsDiabledDuringRefactoring() throws Exception {
		final List<Boolean> enableArgs = new ArrayList<>();
		final AtomicReference<AnaphoraReresolvingDocumentListener> listenerFuture = new AtomicReference<>();
		final BiFunction<IDocument, ICompilationUnit, AnaphoraReresolvingDocumentListener> listenerFactory = (document,
				cu) -> {
			listenerFuture.set(new AnaphoraReresolvingDocumentListener(() -> cu, this::noop, this::noop) {
				@Override
				public void setEnabled(final boolean enabled) {
					enableArgs.add(enabled);
					super.setEnabled(enabled);
				}

			});
			return listenerFuture.get();
		};
		getDefaultCompilationUnit().andThen(assertRefactoringResults(listenerFactory,
				unused -> assertThat(enableArgs).containsExactly(false, true))).apply(null);
	}

	@Test
	public void membersWouldBeInvalidatedIfTheListenerWereNotTurnedOff() throws Exception {
		final List<Pair<Integer, Integer>> offsetsAndLengths = new ArrayList<>();
		final BiFunction<IDocument, ICompilationUnit, AnaphoraReresolvingDocumentListener> listenerFactory = (document,
				cu) -> new AnaphoraReresolvingDocumentListener(() -> cu,
						(offsetAndLength, originDocument) -> offsetsAndLengths.add(offsetAndLength),
						(unused0, unused1) -> {
						}) {
					@Override
					public void setEnabled(final boolean enabled) {
						// Do not turn the listener off
					}
				};

		getDefaultCompilationUnit()
				.andThen(assertRefactoringResults(listenerFactory,
						unused -> assertThat(offsetsAndLengths).containsExactly(new ImmutablePair<>(81, 7),
								new ImmutablePair<>(81, 0), new ImmutablePair<>(46, 14), new ImmutablePair<>(46, 1),
								new ImmutablePair<>(46, 0), new ImmutablePair<>(46, 0), new ImmutablePair<>(46, 0))))
				.apply(null);
	}

	@Test
	public void memberInvalidationIsEnabledAgainAfterRefactoring() throws Exception {
		final AtomicReference<AnaphoraReresolvingDocumentListener> listenerFuture = new AtomicReference<>();
		final BiFunction<IDocument, ICompilationUnit, AnaphoraReresolvingDocumentListener> listenerFactory = (document,
				cu) -> {
			listenerFuture.set(new AnaphoraReresolvingDocumentListener(() -> cu, this::noop, this::noop));
			return listenerFuture.get();
		};
		getDefaultCompilationUnit().andThen(assertRefactoringResults(listenerFactory,
				unused -> assertThat(listenerFuture.get().isEnabled()).isTrue())).apply(null);
	}

	@Test
	public void undoYieldsOriginalDocument() throws Exception {
		getDefaultCompilationUnit()
				.andThen(assertResultsOfUndoneRefactoring(document -> assertThat(document.get()).isEqualTo(
						"public class Foo { public static void foo() { new Integer(1); System.err.println(integer); } }")))
				.apply(null);
	}

	@Test
	public void undoRemovesAnaphoraPositions() throws Exception {
		getDefaultCompilationUnit().andThen(
				assertResultsOfUndoneRefactoring(document -> assertPositions(document, ANAPHORA_CATEGORY, emptyList())))
				.apply(null);
	}

	@Test
	public void undoRemovesPerspectivationPositions() throws Exception {
		getDefaultCompilationUnit().andThen(assertResultsOfUndoneRefactoring(
				document -> assertPositions(document, PERSPECTIVATION_CATEGORY, emptyList()))).apply(null);
	}

	// It is not tested whether ToLowerCasePosition is remove, because the default
	// test source does not contain an anaphor that yields a ToLowerCasePosition.
	@Test
	public void undoRemovesHidePositions() throws Exception {
		getDefaultCompilationUnit().andThen(
				assertResultsOfUndoneRefactoring(document -> assertPositions(document, HIDE_CATEGORY, emptyList())))
				.apply(null);
	}

}
