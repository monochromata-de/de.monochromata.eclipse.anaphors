package org.eclipse.jdt.internal.corext.refactoring.code;

import static de.monochromata.eclipse.anaphors.editor.update.AstBasedAnaphoraUpdating.getDocumentForPositionsTracking;
import static de.monochromata.eclipse.anaphors.editor.update.MemberTrackingPositionCreation.createMemberTrackingPosition;
import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static de.monochromata.eclipse.persp.PerspectivationPosition.PERSPECTIVATION_CATEGORY;
import static de.monochromata.eclipse.position.PositionExceptionWrapping.wrapPositionExceptions;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Stream.concat;
import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.jface.viewers.StructuredSelection.EMPTY;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMember;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.internal.corext.refactoring.change.ChangeTesting;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.AnnotationModel;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CheckConditionsOperation;
import org.eclipse.ltk.core.refactoring.CreateChangeOperation;
import org.eclipse.ltk.core.refactoring.PerformChangeOperation;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.swt.widgets.Display;

import de.monochromata.eclipse.anaphors.NoopTesting;
import de.monochromata.eclipse.anaphors.editor.update.AnaphoraReresolvingDocumentListener;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;
import de.monochromata.eclipse.persp.PerspectivationPosition;
import de.monochromata.function.ThrowingBiConsumer;
import de.monochromata.function.ThrowingBiFunction;
import de.monochromata.function.ThrowingConsumer;
import de.monochromata.function.ThrowingFunction;

public interface ResolveAnaphorsRefactoringTesting extends RefactoringTesting, ChangeTesting, NoopTesting {

	default <T extends Throwable> Function<Function<Pair<IJavaProject, IPackageFragment>, CompilationUnit>, Void> assertRefactoringResults(
			final ThrowingConsumer<IDocument, T> resultsAsserter) throws Exception {
		return assertRefactoringResults(this::createRefactoringForDefaultProblem, resultsAsserter);
	}

	default <T extends Throwable> Function<Function<Pair<IJavaProject, IPackageFragment>, CompilationUnit>, Void> assertRefactoringResults(
			final ThrowingBiFunction<IDocument, CompilationUnit, ThrowingFunction<BiFunction<IDocument, ICompilationUnit, AnaphoraReresolvingDocumentListener>, ResolveAnaphorsRefactoring, CoreException>, CoreException> createRefactoring,
			final ThrowingConsumer<IDocument, T> resultsAsserter) throws Exception {
		return assertRefactoringResults(createRefactoring,
				(document, cu) -> new AnaphoraReresolvingDocumentListener(() -> cu, this::noop, this::noop),
				resultsAsserter);
	}

	default <T extends Throwable> Function<Function<Pair<IJavaProject, IPackageFragment>, CompilationUnit>, Void> assertRefactoringResults(
			final BiFunction<IDocument, ICompilationUnit, AnaphoraReresolvingDocumentListener> anaphoraReresolvingListenerFactory,
			final ThrowingConsumer<IDocument, T> resultsAsserter) throws Exception {
		return assertRefactoringResults(this::createRefactoringForDefaultProblem, anaphoraReresolvingListenerFactory,
				resultsAsserter);
	}

	default <T extends Throwable> Function<Function<Pair<IJavaProject, IPackageFragment>, CompilationUnit>, Void> assertRefactoringResults(
			final ThrowingBiFunction<IDocument, CompilationUnit, ThrowingFunction<BiFunction<IDocument, ICompilationUnit, AnaphoraReresolvingDocumentListener>, ResolveAnaphorsRefactoring, CoreException>, CoreException> createRefactoring,
			final BiFunction<IDocument, ICompilationUnit, AnaphoraReresolvingDocumentListener> anaphoraReresolvingListenerFactory,
			final ThrowingConsumer<IDocument, T> resultsAsserter) throws Exception {
		return assertRefactoringResultsInternal(createRefactoring, anaphoraReresolvingListenerFactory,
				(undoChange, document) -> resultsAsserter.accept(document));
	}

	default <T extends Throwable> Function<Function<Pair<IJavaProject, IPackageFragment>, CompilationUnit>, Void> assertResultsOfUndoneRefactoring(
			final ThrowingConsumer<IDocument, T> resultsAsserter) throws Exception {
		return assertRefactoringResultsInternal((undoChange, document) -> {
			final NullProgressMonitor pm = new NullProgressMonitor();
			undoChange.initializeValidationData(pm);
			assertThat(undoChange.isValid(pm).isOK()).isTrue();
			undoChange.perform(pm);
			resultsAsserter.accept(document);
		});
	}

	default <T extends Throwable> Function<Function<Pair<IJavaProject, IPackageFragment>, CompilationUnit>, Void> assertRefactoringResultsInternal(
			final ThrowingBiConsumer<Change, IDocument, T> resultsAsserter) throws Exception {
		return assertRefactoringResultsInternal(
				(document, cu) -> new AnaphoraReresolvingDocumentListener(() -> cu, this::noop, this::noop),
				resultsAsserter);
	}

	default <T extends Throwable> Function<Function<Pair<IJavaProject, IPackageFragment>, CompilationUnit>, Void> assertRefactoringResultsInternal(
			final BiFunction<IDocument, ICompilationUnit, AnaphoraReresolvingDocumentListener> anaphoraReresolvingListenerFactory,
			final ThrowingBiConsumer<Change, IDocument, T> resultsAsserter) throws Exception {
		return assertRefactoringResultsInternal(this::createRefactoringForDefaultProblem,
				anaphoraReresolvingListenerFactory, resultsAsserter);
	}

	default <T extends Throwable> Function<Function<Pair<IJavaProject, IPackageFragment>, CompilationUnit>, Void> assertRefactoringResultsInternal(
			final ThrowingBiFunction<IDocument, CompilationUnit, ThrowingFunction<BiFunction<IDocument, ICompilationUnit, AnaphoraReresolvingDocumentListener>, ResolveAnaphorsRefactoring, CoreException>, CoreException> createRefactoring,
			final BiFunction<IDocument, ICompilationUnit, AnaphoraReresolvingDocumentListener> anaphoraReresolvingListenerFactory,
			final ThrowingBiConsumer<Change, IDocument, T> resultsAsserter) throws Exception {
		return assertRefactoringChange(createRefactoring, anaphoraReresolvingListenerFactory,
				(compilationUnit, change) -> {
					final NullProgressMonitor pm = new NullProgressMonitor();
					try {
						final IDocument document = getDocumentForPositionsTracking(compilationUnit);

						final PerformChangeOperation operation = new PerformChangeOperation(change);
						operation.run(pm);
						assertThat(operation.getConditionCheckingStatus()).isNull();
						assertThat(operation.getValidationStatus().toString()).startsWith("<OK");
						assertThat(operation.changeExecutionFailed()).isFalse();
						assertThat(operation.changeExecuted()).isTrue();

						resultsAsserter.accept(operation.getUndoChange(), document);
					} catch (final Throwable e) {
						throw new RuntimeException(e.getMessage(), e);
					}
				});
	}

	default Function<Function<Pair<IJavaProject, IPackageFragment>, CompilationUnit>, Void> assertRefactoringChange(
			final BiConsumer<CompilationUnit, Change> asserter) throws Exception {
		return assertRefactoringChange(
				(document, cu) -> new AnaphoraReresolvingDocumentListener(() -> cu, this::noop, this::noop), asserter);
	}

	default Function<Function<Pair<IJavaProject, IPackageFragment>, CompilationUnit>, Void> assertRefactoringChange(
			final BiFunction<IDocument, ICompilationUnit, AnaphoraReresolvingDocumentListener> anaphoraReresolvingListenerFactory,
			final BiConsumer<CompilationUnit, Change> asserter) throws Exception {
		return assertRefactoringChange(this::createRefactoringForDefaultProblem, anaphoraReresolvingListenerFactory,
				asserter);
	}

	default Function<Function<Pair<IJavaProject, IPackageFragment>, CompilationUnit>, Void> assertRefactoringChange(
			final ThrowingBiFunction<IDocument, CompilationUnit, ThrowingFunction<BiFunction<IDocument, ICompilationUnit, AnaphoraReresolvingDocumentListener>, ResolveAnaphorsRefactoring, CoreException>, CoreException> createRefactoring,
			final BiFunction<IDocument, ICompilationUnit, AnaphoraReresolvingDocumentListener> anaphoraReresolvingListenerFactory,
			final BiConsumer<CompilationUnit, Change> asserter) throws Exception {
		return assertChange((document, compilationUnit) -> {
			final var refactoring = createRefactoring.apply(document, compilationUnit)
					.apply(anaphoraReresolvingListenerFactory);
			return createChange(refactoring);
		}, (compilationUnit, change) -> asserter.accept(compilationUnit, change));
	}

	default ThrowingFunction<BiFunction<IDocument, ICompilationUnit, AnaphoraReresolvingDocumentListener>, ResolveAnaphorsRefactoring, CoreException> createRefactoringForDefaultProblem(
			final IDocument document, final CompilationUnit compilationUnit) throws CoreException {
		return createRefactoringForProblemPositions(singletonList(new Position(81, 7))).apply(document,
				compilationUnit);
	}

	default ThrowingBiFunction<IDocument, CompilationUnit, ThrowingFunction<BiFunction<IDocument, ICompilationUnit, AnaphoraReresolvingDocumentListener>, ResolveAnaphorsRefactoring, CoreException>, CoreException> createRefactoringForProblemPositions(
			final List<Position> problemPositions) throws CoreException {
		return createRefactoring(emptyList(), problemPositions);
	}

	default ThrowingBiFunction<IDocument, CompilationUnit, ThrowingFunction<BiFunction<IDocument, ICompilationUnit, AnaphoraReresolvingDocumentListener>, ResolveAnaphorsRefactoring, CoreException>, CoreException> createRefactoring(
			final List<PositionForAnaphor> anaphorPositions, final List<? extends Position> problemPositions) {
		return createRefactoring(emptyList(), emptyList(), anaphorPositions, problemPositions);
	}

	default ThrowingBiFunction<IDocument, CompilationUnit, ThrowingFunction<BiFunction<IDocument, ICompilationUnit, AnaphoraReresolvingDocumentListener>, ResolveAnaphorsRefactoring, CoreException>, CoreException> createRefactoring(
			final List<PerspectivationPosition> existingPerspectivationPositions,
			final List<PositionForRelatedExpression> existingRelatedExpressionPositions,
			final List<PositionForAnaphor> existingAnaphorPositions, final List<? extends Position> problemPositions) {
		return (document, compilationUnit) -> anaphoraReresolvingListenerFactory -> {
			existingPerspectivationPositions.forEach(
					position -> wrapPositionExceptions(() -> document.addPosition(PERSPECTIVATION_CATEGORY, position)));
			concat(existingRelatedExpressionPositions.stream(), existingAnaphorPositions.stream()).forEach(
					position -> wrapPositionExceptions(() -> document.addPosition(ANAPHORA_CATEGORY, position)));
			final var anaphoraReresolvingListener = anaphoraReresolvingListenerFactory.apply(document,
					(ICompilationUnit) compilationUnit.getTypeRoot());
			document.addDocumentListener(anaphoraReresolvingListener);
			final var method = (IMember) ((IType) compilationUnit.getTypeRoot().getChildren()[0]).getChildren()[0];
			final var trackingPosition = createMemberTrackingPosition(method).orElseThrow(NullPointerException::new);
			final var annotationModel = new AnnotationModel();
			return new ResolveAnaphorsRefactoring(Display.getDefault(), document, annotationModel, () -> EMPTY,
					this::noop, compilationUnit, anaphoraReresolvingListener, this::noop, this::noop, this::noop,
					trackingPosition, existingAnaphorPositions, problemPositions);
		};
	}

	default Change createChange(final ResolveAnaphorsRefactoring refactoring) throws CoreException {
		final var workspace = ResourcesPlugin.getWorkspace();
		final var checkConditionsOperation = new CheckConditionsOperation(refactoring,
				CheckConditionsOperation.ALL_CONDITIONS);
		final var createChangeOperation = new CreateChangeOperation(checkConditionsOperation, RefactoringStatus.FATAL);
		workspace.run(createChangeOperation, null);
		assertThat(checkConditionsOperation.getStatus().getSeverity()).isEqualTo(RefactoringStatus.OK);
		return createChangeOperation.getChange();
	}

}
