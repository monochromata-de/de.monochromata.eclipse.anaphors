package org.eclipse.jdt.internal.corext.refactoring.change;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.anaphors.ASTTesting;
import org.eclipse.jdt.core.dom.anaphors.PublicAnaphora;
import org.eclipse.jdt.core.dom.anaphors.PublicChainElement;
import org.eclipse.jdt.core.dom.anaphors.PublicRelatedExpression;

import de.monochromata.anaphors.Anaphora;
import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.DefaultDirectAnaphora;
import de.monochromata.eclipse.anaphors.AnaphoraTesting;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;
import de.monochromata.eclipse.persp.PerspectivationPosition;

public interface AnaphoraPositionsTesting extends ASTTesting, ChangeTesting, AnaphoraTesting {

	default List<AbstractAnaphoraPositionForRepresentation> createDefaultAnaphoraPositions(final Anaphora anaphora) {
		final PositionForRelatedExpression relatedExpressionPosition = new PositionForRelatedExpression(1, 17);
		final PositionForAnaphor anaphorPosition = new PositionForAnaphor(20, 7, anaphora);
		return asList(relatedExpressionPosition, anaphorPosition);
	}

	default Pair<PerspectivationPosition, PerspectivationPosition> createPerspectivationPositions() {
		return new ImmutablePair<>(
				new PerspectivationPosition(1, "relatedExpression".length(), unused -> true, emptyList()),
				new PerspectivationPosition(20, "anaphor".length(), unused -> true, emptyList()));
	}

	default Pair<PublicChainElement, PublicChainElement> createChainElements() {
		final ASTBasedAnaphora<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> astBasedAnaphora = new DefaultDirectAnaphora<>(
				null, null, false);
		final List<PublicChainElement> nextElements = new ArrayList<>();
		final PublicChainElement relatedExpressionElement = new PublicChainElement(nextElements, astBasedAnaphora);
		final PublicChainElement anaphorElement = new PublicChainElement(relatedExpressionElement, astBasedAnaphora,
				null);
		nextElements.add(anaphorElement);
		return new ImmutablePair<>(relatedExpressionElement, anaphorElement);
	}

	default PublicChainElement createChainWithOldAndNewAnaphorElements(final PublicChainElement existingAnaphorElement,
			final PublicChainElement newAnaphorElement) {
		final ASTBasedAnaphora<ASTNode, Expression, Type, IBinding, ITypeBinding, CompilationUnit, String, String, PublicRelatedExpression, PublicAnaphora> astBasedAnaphora = new DefaultDirectAnaphora<>(
				null, null, false);
		final List<PublicChainElement> nextElements = new ArrayList<>();
		final PublicChainElement relatedExpressionElement = new PublicChainElement(nextElements, astBasedAnaphora);
		nextElements.add(existingAnaphorElement);
		nextElements.add(newAnaphorElement);
		return relatedExpressionElement;
	}

}
