package org.eclipse.jdt.internal.corext.refactoring.change;

import static org.assertj.core.api.Assertions.assertThat;

import org.eclipse.ltk.core.refactoring.Change;
import org.junit.Test;

import de.monochromata.eclipse.anaphors.editor.CognitiveEditor;
import de.monochromata.eclipse.anaphors.editor.update.AnaphoraReresolvingDocumentListener;

public class ConfigureAnaphoraReresolutionChangeTest implements ChangeTesting {

    @Test
    public void isValidAlwaysReturnsOk() throws Exception {
        final ConfigureAnaphoraReresolutionChange change = new ConfigureAnaphoraReresolutionChange(null, false);
        assertThat(change.isValid(null).isOK()).isTrue();
    }

    @Test
    public void getNameReturnsCorrectResult() throws Exception {
        assertThat(new ConfigureAnaphoraReresolutionChange(null, false).getName())
                .isEqualTo("Configure member invalidation");
    }

    @Test
    public void getModifiedElementReturnsNull() throws Exception {
        assertThat(new ConfigureAnaphoraReresolutionChange(null, false).getModifiedElement()).isNull();
    }

    @Test
    @SuppressWarnings("rawtypes")
    public void performEnablesInvalidation() throws Exception {
        final AnaphoraReresolvingDocumentListener listener = new AnaphoraReresolvingDocumentListener(
                (CognitiveEditor) null, null, null);
        listener.setEnabled(false);
        final ConfigureAnaphoraReresolutionChange change = new ConfigureAnaphoraReresolutionChange(listener, true);
        assertThat(listener.isEnabled()).isFalse();

        performChange(change);

        assertThat(listener.isEnabled()).isTrue();
    }

    @Test
    public void performDisablesInvalidation() throws Exception {
        final AnaphoraReresolvingDocumentListener listener = new AnaphoraReresolvingDocumentListener(
                (CognitiveEditor) null, null, null);
        final ConfigureAnaphoraReresolutionChange change = new ConfigureAnaphoraReresolutionChange(listener, false);
        assertThat(listener.isEnabled()).isTrue();

        performChange(change);

        assertThat(listener.isEnabled()).isFalse();
    }

    @Test
    public void performReturnsUndoChange() throws Exception {
        final AnaphoraReresolvingDocumentListener listener = new AnaphoraReresolvingDocumentListener(
                (CognitiveEditor) null, null, null);
        final ConfigureAnaphoraReresolutionChange change = new ConfigureAnaphoraReresolutionChange(listener, false);
        assertThat(listener.isEnabled()).isTrue();

        final Change undoChange = performChange(change);

        assertThat(listener.isEnabled()).isFalse();

        performChange(undoChange);

        assertThat(listener.isEnabled()).isTrue();
    }

}