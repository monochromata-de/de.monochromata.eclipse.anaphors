package org.eclipse.jdt.internal.corext.refactoring.change;

import static de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPosition.ANAPHORA_CATEGORY;
import static de.monochromata.eclipse.position.DoublyLinkedPosition.link;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import de.monochromata.eclipse.anaphors.NoopTesting;
import de.monochromata.eclipse.anaphors.position.AbstractAnaphoraPositionForRepresentation;
import de.monochromata.eclipse.anaphors.position.PositionForAnaphor;
import de.monochromata.eclipse.anaphors.position.PositionForRelatedExpression;

public class UpdateDoublyLinkedPositionsChangeTest extends AbstractAnaphoraPositionsTest
		implements UpdatePositionsChangeTesting, NoopTesting {

	@Test
	public void perform_linksPositionsToLink() throws Throwable {
		final List<AbstractAnaphoraPositionForRepresentation> positions = createDefaultAnaphoraPositions(anaphora);
		final List<Pair<PositionForRelatedExpression, PositionForAnaphor>> positionsToLink = singletonList(
				new ImmutablePair<>((PositionForRelatedExpression) positions.get(0),
						(PositionForAnaphor) positions.get(1)));
		final UpdateDoublyLinkedPositionsChange change = new UpdateDoublyLinkedPositionsChange(document,
				"positionDescription", ANAPHORA_CATEGORY, positions, positionsToLink, emptyList(), emptyList());
		assertNotLinked(positions);

		assertChangeResult(change, () -> assertPositions(document, positions));

		assertLinked(positions.get(0), positions.get(1));
	}

	@Test
	public void perform_unlinksPositionsToUnlink() throws Throwable {
		final List<AbstractAnaphoraPositionForRepresentation> positions = createDefaultAnaphoraPositions(anaphora);
		link(positions.get(0), positions.get(1));

		final List<Pair<PositionForRelatedExpression, PositionForAnaphor>> positionsToUnlink = singletonList(
				new ImmutablePair<>((PositionForRelatedExpression) positions.get(0),
						(PositionForAnaphor) positions.get(1)));
		final UpdateDoublyLinkedPositionsChange change = new UpdateDoublyLinkedPositionsChange(document,
				"positionDescription", ANAPHORA_CATEGORY, positions, emptyList(), emptyList(), positionsToUnlink);

		assertChangeResult(change, () -> assertPositions(document, positions));

		assertNotLinked(positions);
	}

	@Test
	public void undoChange_addsPositionsToRemove() throws Throwable {
		final List<AbstractAnaphoraPositionForRepresentation> positions = createAndAddPositions(document, anaphora);
		final UpdateDoublyLinkedPositionsChange change = new UpdateDoublyLinkedPositionsChange(document,
				"positionDescription", ANAPHORA_CATEGORY, emptyList(), emptyList(), positions, emptyList());

		assertChangeResult(change, (undoChange) -> {
			assertStatusOk(undoChange.isValid(null));
			undoChange.perform(null);
			assertPositions(document, positions);
		});
	}

	@Test
	public void undoChange_removesPositionsToAdd() throws Throwable {
		final List<AbstractAnaphoraPositionForRepresentation> positions = createDefaultAnaphoraPositions(anaphora);
		final UpdateDoublyLinkedPositionsChange change = new UpdateDoublyLinkedPositionsChange(document,
				"positionDescription", ANAPHORA_CATEGORY, positions, emptyList(), emptyList(), emptyList());
		assertChangeResult(change, (undoChange) -> {
			assertStatusOk(undoChange.isValid(null));
			assertAnaphoraPositions(document, positions);

			undoChange.perform(null);

			assertAnaphoraPositions(document, emptyList());
		});
	}

	@Test
	public void undoChange_unlinksPositionsToLink() throws Throwable {
		final List<AbstractAnaphoraPositionForRepresentation> positions = createDefaultAnaphoraPositions(anaphora);
		final List<Pair<PositionForRelatedExpression, PositionForAnaphor>> positionsToLink = singletonList(
				new ImmutablePair<>((PositionForRelatedExpression) positions.get(0),
						(PositionForAnaphor) positions.get(1)));
		final UpdateDoublyLinkedPositionsChange change = new UpdateDoublyLinkedPositionsChange(document,
				"positionDescription", ANAPHORA_CATEGORY, positions, positionsToLink, emptyList(), emptyList());

		assertChangeResult(change, (undoChange) -> {
			assertStatusOk(undoChange.isValid(null));
			assertLinked(positions.get(0), positions.get(1));

			undoChange.perform(null);

			assertNotLinked(positions);
		});
	}

	@Test
	public void undoChange_linksPositionsToUnlink() throws Throwable {
		final List<AbstractAnaphoraPositionForRepresentation> positions = createAndAddPositions(document, anaphora);
		final List<Pair<AbstractAnaphoraPositionForRepresentation, AbstractAnaphoraPositionForRepresentation>> positionsToUnlink = singletonList(
				new ImmutablePair<>(positions.get(0), positions.get(1)));
		link(positionsToUnlink.get(0));
		final UpdateDoublyLinkedPositionsChange change = new UpdateDoublyLinkedPositionsChange(document,
				"positionDescription", ANAPHORA_CATEGORY, emptyList(), emptyList(), positions, positionsToUnlink);

		assertChangeResult(change, (undoChange) -> {
			assertStatusOk(undoChange.isValid(null));

			undoChange.perform(null);

			assertLinked(positionsToUnlink);
		});
	}

}
