package org.eclipse.jdt.internal.corext.refactoring.change;

import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent.EventType.ABOUT_TO_REALIZE_ANAPHORS;
import static de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent.EventType.ABOUT_TO_UNDO_ANAPHOR_REALIZATION;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.atomic.AtomicReference;

import org.junit.Test;

import de.monochromata.eclipse.anaphors.event.AnaphorResolutionEvent;

public class AnaphorResolutionEventChangeTest implements AnaphorResolutionEventChangeTesting {

	@Test
	public void isValid_returnsOk() throws Exception {
		final var change = new AnaphorResolutionEventChange(null, null, null);

		assertThat(change.isValid(null).isOK()).isTrue();
	}

	@Test
	public void perform_sendsTheGivenEvent() throws Throwable {
		final var event = new AnaphorResolutionEvent(null, null, null, null);
		final AtomicReference<AnaphorResolutionEvent> listener = new AtomicReference<>();

		assertChangeResult(ABOUT_TO_REALIZE_ANAPHORS, event, (eventType, event0) -> listener.set(event0),
				unused -> assertThat(listener.get()).isSameAs(event));
	}

	@Test
	public void perform_sendOppositeUndoChangeForAnaphorResolution() throws Throwable {
		final var event = new AnaphorResolutionEvent(null, null, null, null);

		assertChangeResult(ABOUT_TO_REALIZE_ANAPHORS, event, this::noop,
				undoChange -> assertThat(undoChange.getEventTypeToFire().equals(ABOUT_TO_UNDO_ANAPHOR_REALIZATION)));
	}

	@Test
	public void perform_sendsOppositionUndoChangeForUndoAnaphorResolution() throws Throwable {
		final var event = new AnaphorResolutionEvent(null, null, null, null);

		assertChangeResult(ABOUT_TO_UNDO_ANAPHOR_REALIZATION, event, this::noop,
				undoChange -> assertThat(undoChange.getEventTypeToFire().equals(ABOUT_TO_REALIZE_ANAPHORS)));
	}

}
