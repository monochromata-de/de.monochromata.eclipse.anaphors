package de.monochromata.eclipse.anaphors.editor.simple;

public class WarmupBeforeTheFirst {

	public static void main(String[] args) {
		new A("foo");
		new A("bar");
		System.err.println(fooA);
	}

	private static class A {
		public A(String feature) {
		}
	}
}
