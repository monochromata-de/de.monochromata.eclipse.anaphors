package de.monochromata.eclipse.anaphors.editor.simple.feature;

import javax.swing.JTextArea;

public class MultipleIndependentProblemsInSameMethod {

    public static void foo() {
        final JTextArea jTextArea = new JTextArea();
        jTextArea.setText("line count=" + lineCount);
        new String("Hello");
        new String("Bello");
        System.err.println(helloString);
    }
}
