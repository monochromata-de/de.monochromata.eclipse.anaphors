package de.monochromata.eclipse.anaphors.editor.simple;

public class LVD_DA1Re_Rt_MIQualifiedWithExpressionAndAlreadyAssignedToLocalVariable_Simple {

    public static void main() {
        final Foo result = create().createFoo();
        System.err.println(foo);
    }

    private static ObjectSession create() {
        return new ObjectSession();
    }

    private static class ObjectSession {
        public Foo createFoo() {
            return new Foo();
        }
    }

    private static class Foo {
    }
}
