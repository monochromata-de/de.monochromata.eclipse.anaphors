package de.monochromata.eclipse.anaphors.editor.simple;

public class CIC_IA2Mg_Rt_GettersHaveNoParameters_Simple {

    public static void main(String[] args) {
        new A();
        System.err.println(integer);
    }

    private static class A {
        public Integer getValue() {
            return 10;
        }
        // This does not count as getter because it has a parameter
        @SuppressWarnings("unused")
        public Integer getValue(final boolean really) {
            if(really) {
                return getValue();
            }
            return -1;
        }
    }
}
