/**
 * Java source files to be loaded to start interaction tests.
 * 
 * @author monochromata
 */
package de.monochromata.eclipse.anaphors.editor.interactive;