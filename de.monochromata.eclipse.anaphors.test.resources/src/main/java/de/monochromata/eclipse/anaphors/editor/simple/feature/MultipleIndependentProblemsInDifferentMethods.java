package de.monochromata.eclipse.anaphors.editor.simple.feature;

import javax.swing.JTextArea;

public class MultipleIndependentProblemsInDifferentMethods {

    public static void foo() {
        final JTextArea jTextArea = new JTextArea();
        jTextArea.setText("line count=" + lineCount);
    }

    public static void bar() {
        new String("Hello");
        new String("Bello");
        System.err.println(helloString);
    }
}
