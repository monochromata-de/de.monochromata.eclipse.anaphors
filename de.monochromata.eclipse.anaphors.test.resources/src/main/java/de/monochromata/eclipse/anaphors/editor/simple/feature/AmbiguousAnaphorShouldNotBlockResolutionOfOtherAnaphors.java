package de.monochromata.eclipse.anaphors.editor.simple.feature;

public class AmbiguousAnaphorShouldNotBlockResolutionOfOtherAnaphors {

    public static void foo() {
        new String("Hello");
        System.err.println(helloString);
        System.err.println(belloString);
    }
}
