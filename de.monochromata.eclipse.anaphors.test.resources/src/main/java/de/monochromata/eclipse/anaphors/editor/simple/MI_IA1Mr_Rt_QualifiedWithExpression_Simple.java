package de.monochromata.eclipse.anaphors.editor.simple;

public class MI_IA1Mr_Rt_QualifiedWithExpression_Simple {

    public static void main() {
        create()
            .createFoo();
        System.err.println(foo);
    }

    private static ObjectSession create() {
        return new ObjectSession();
    }

    private static class ObjectSession {
        public Foo createFoo() {
            return new Foo();
        }
    }

    private static class Foo {
    }
}
