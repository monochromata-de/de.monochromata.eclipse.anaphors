package de.monochromata.eclipse.anaphors.editor.simple.feature;

public class IgnorePrivateFieldForIA2F {

	public static void main(String[] args) {
		foo(new A());
	}

	public static void foo(A foo) {
		System.err.println(Integer);
	}
}

class A {
    public Integer int0;

    /**
     * Does not cause ambiguity because the field is not visible
     */
    @SuppressWarnings("unused")
    private Integer int1;
}
