package de.monochromata.eclipse.anaphors.editor.simple;

public class MI_IA1Mr_Rt_QualifiedWithTypeName_Simple {

    public static void main() {
        MI_IA1Mr_Rt_QualifiedWithTypeName_Simple.create();
        System.err.println(objectSession);
    }

    private static ObjectSession create() {
        return new ObjectSession();
    }

    private static class ObjectSession {
    }
}
