package de.monochromata.eclipse.anaphors.editor.simple;

public class MI_IA1Mr_Rt_AnchoringInQualifierNotInRightmostInvocation_Simple {

    public static void main() {
        create().foo();
        System.err.println(objectSession);
    }

    private static ObjectSession create() {
        return new ObjectSession();
    }

    private static class ObjectSession {
        public boolean foo() {
            return false;
        }
    }
}
