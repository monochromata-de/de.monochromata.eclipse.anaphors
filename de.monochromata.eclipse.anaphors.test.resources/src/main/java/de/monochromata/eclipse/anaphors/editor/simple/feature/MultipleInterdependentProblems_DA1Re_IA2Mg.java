package de.monochromata.eclipse.anaphors.editor.simple.feature;

import javax.swing.JTextArea;

public class MultipleInterdependentProblems_DA1Re_IA2Mg {

    public static void foo() {
        new JTextArea();
        jTextArea.setText("line count=" + lineCount);
    }
}
