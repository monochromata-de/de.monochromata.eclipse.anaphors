package de.monochromata.eclipse.anaphors.editor.simple;

public class PD_IA2Mg_Rn_Simple {

	public static void main(String[] args) {
		foo(new A());
	}

	public static void foo(A foo) {
		System.err.println(value);
	}

	private static class A {
		public Integer getValue() {
			return 10;
		}
	}
}
