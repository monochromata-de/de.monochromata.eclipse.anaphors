package de.monochromata.eclipse.anaphors.editor.simple;

public class LVD_IA2Mg_RfRt_Simple {

	public static void foo() {
		A foo = new A("bar");
		System.err.println(barInteger);
	}

	private static class A {
		public A(final String input) {}
		
		public Integer getValue() {
			return 10;
		}
	}
}
