package de.monochromata.eclipse.anaphors.editor.simple.feature;

public class IgnorePrivateGetterForIA2Mg {

    public static void main(String[] args) {
        foo(new B());
    }

    public static void foo(B foo) {
        System.err.println(integer);
    }
}

class B {
    public Integer getValue() {
        return 10;
    }

    /**
     * Does not cause ambiguity because the getter is not visible
     */
    @SuppressWarnings("unused")
    private Integer getOtherValue() {
        return 11;
    }
}
