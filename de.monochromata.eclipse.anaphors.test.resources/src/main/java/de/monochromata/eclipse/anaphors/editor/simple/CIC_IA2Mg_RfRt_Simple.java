package de.monochromata.eclipse.anaphors.editor.simple;

public class CIC_IA2Mg_RfRt_Simple {

	public static void main(String[] args) {
		new A("foo");
		System.err.println(fooInteger);
	}

	private static class A {
		public A(final String input) {}
		
		public Integer getValue() {
			return 10;
		}
	}
}
