# An Eclipse plug-in for anaphors in Java source code

[![pipeline status](https://gitlab.com/monochromata-de/de.monochromata.eclipse.anaphors/badges/master/pipeline.svg)](https://gitlab.com/monochromata-de/de.monochromata.eclipse.anaphors/commits/master)

* The project is available from a Maven repository at https://monochromata.de/maven/releases/de.monochromata.eclipse.anaphors/ .
* The Maven site is available at https://monochromata.de/maven/sites/de.monochromata.eclipse.anaphors/ .
* An Eclipse p2 repository is available at https://monochromata.de/eclipse/sites/de.monochromata.eclipse.anaphors/ .

## Resources 

* https://www.eclipsecon.org/2012/sites/eclipsecon.org.2012/files/How%20To%20Train%20the%20JDT%20Dragon%20combined.pdf

## License

EPL 2.0